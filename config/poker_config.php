<?php
return [
    'admin' => [
        'super_admin_id' => 1,
        'super_admins' => [1]
    ],
    'paginate' => [
        'per_page' => 100
    ],

    's3Bucket' => [
        'AWS_ACCESS_KEY_ID' => env('AWS_ACCESS_KEY_ID', 'AKIATTVEXHWZZMFHTTNA'),
        'AWS_SECRET_ACCESS_KEY' => env('AWS_SECRET_ACCESS_KEY', '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'),
        'AWS_DEFAULT_REGION' => env('AWS_DEFAULT_REGION', 'ap-south-1'),
        'AWS_BUCKET' => env('AWS_BUCKET', 'devbaazi-upload')
    ],
    'CDNs3Bucket' => [
        'AWS_CDN_ACCESS_KEY_ID' => env('AWS_CDN_ACCESS_KEY_ID', 'AKIA27QXR2TZL5LDEUV5'),
        'AWS_CDN_SECRET_ACCESS_KEY' => env('AWS_CDN_SECRET_ACCESS_KEY', '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'),
        'AWS_CDN_DEFAULT_REGION' => env('AWS_CDN_DEFAULT_REGION', 'ap-south-1'),
        'AWS_CDN_BUCKET' => env('AWS_CDN_BUCKET', 'baazi-poker-web-cdn')
    ],
    'imageUrl' => [
        'REWARD_IMAGE_FOLDER' => env('REWARD_IMAGE_FOLDER', 'rewards/test-images/'),
        'CDN_URL' => env('CDN_URL', 'https://web.pokerbaazicdn.com/')
    ],
    "mail" => [
        'from' => [
            'support' => env('MAIL_FROM_SUPPORT', 'support@pokerbaazi.com'),
            'analytics' => env('MAIL_FROM_ANALYTICS', 'analytics@pokerbaazi.com'),
            'ankit_singla' => env('MAIL_FROM_ANALYTICS', 'Ankit.singla@pokerbaazi.com')
        ],
        "to" => [
            'support' => env('MAIL_FROM_SUPPORT', 'support@pokerbaazi.com')
        ]
    ],
    "payment" => [
        'PAYTM'=>[
            'paytm_status_url'=>env('PAYTM_STATUS_URL'),
            'mid'=>env('PAYTM_MID')
        ],
        'CASHFREE'=>[
            'cashfree_status_url'=>env('CASHFREE_STATUS_URL'),
            'cashfree_id'=>env('CASHFREE_ID'),
            'cashfree_sec'=>env('CASHFREE_SEC'),
        ],
        'PAYU'=>[
            'payu_stat_url'=>env('PAYU_STATUS_CHECK_URL'),
            'payu_merchant_key'=>env('PAYU_MERCHANT_KEY'),
            'payu_auth_key'=>env('PAYU_AUTH_KEY'),
        ],
    ],
    "director_mail_id" => [
        'emails' =>explode('|', env('ADMIN_MAIL_ID'))
    ],
    "push_notification" => [
        "send_url" => env('PUSH_NOTIFICATION_SEND_URL',"http://pokerchronicles.com:3000/notification/send")
    ],
    "cohort_report_mail_to" => [
        'emails' =>explode("|",env('COHORT_REPORT_MAIL_TO'))
    ],
    "server_ip" => [
        'ip' =>explode("|",env('SERVER_IP','::1'))
    ],
	 "pbdailystats_report_mail_to" => [
        'emails' =>explode("|",env('PB_DAILY_STATE_MAIL_TO'))
    ],
	"testpbdailystats_report_mail_to" => [
        'emails' =>explode("|",env('TEST_PB_DAILY_STATE_MAIL_TO'))
    ],
    "daily_marketing_aut_mail" => [
        'emails' =>explode("|",env('DAILY_MARKETING_AUT_MAIL'))
    ],
	"landing_page_automation_report_mail_to" => [
        'emails' =>explode("|",env('LANDING_PAGE_REPORT_AUTOMATION_MAIL_TO'))
    ],
	"weekly_report_mail_to" => [
        'emails' =>explode("|",env('WEEKLY_REPORT_MAIL_TO'))
    ],
    'inVoid' => [
        'authKey' => env('INVOID_AUTH_KEY'),
        'numberBasedApiUrl' => env('INVOID_NUMBER_BASED_URL', 'https://gc.invoid.co/v3'),
        'fileBasedApiUrl' => env('INVOID_FILE_BASED_URL', 'https://ic.invoid.co/v2'),
        'uniqueUrlForAadhaarKycApiUrl' => env('INVOID_UNIQUE_URL_GENERATE_BASED_URL', 'https://init-aadhaar.invoid.co'),
    ],

    'ageLimit'=>env('AgeLimit'),
    'reasonCategoryId' => [
        'reason_category_id' => 2
    ],
    "poker_partner_automation_mail" => [
        'emails' =>explode("|",env('POKER_PARTNER_AUTOMATION_MAIL_TO'))
    ],
    "poker_partner" => [
        'partner_id' => env('POKER_PARTNER_ID')
    ],
    "transaction_type_id" => [
        'debit_id' => env('POKER_PATNER_TRANSACTION_TYPE_ID_FOR_DEBIT'),
        'credit_id' => env('POKER_PATNER_TRANSACTION_TYPE_ID_FOR_CREDIT')
    ],
    "kycMatchPercentage" => [
        'fullMatchPercentage' => env('KYC_FULL_MATCH_PERCENTAGE'),
        'partialMatchPercentage' => env('KYC_PARTIAL_MATCH_PERCENTAGE')
    ]

];
