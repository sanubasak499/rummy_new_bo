<?php
return [
    'game' => [
        'IS_REGISTER_GAME' => 1,
        'TOURNAMENT_BREAK_TIME' => 5,
        'BREAK_INTERVAL_TIME' => 55,
        'PAID_OPTION' => 1,
        'ALLOW_OFFLINE_PLAYERS' => 1,
        'TOURNAMENT_STATUS' => 0,
        'TOURNAMENT_LOBBY_ID' => 2,
        'MINIGAMES_ID' => 1,
        'DEFAULT_SCHEDULED_TOURNAMET_ID' => 4,
        'TOURNAMENT_LOBBY_MENU_ID_OMAHA' => 6,
        'TOURNAMENT_LOBBY_MENU_ID_TEXTA' => 5,
        'CURRENT_LEVEL' => 1,
        'STAKE_LEVELS' => 1,
        'TIME_BANK_INTERVAL_TIME' => 10,
        'SITEOUT_TIME'=>10,

        
        'ADMIN_USER_ID'=>10001,
        'INTERNAL_USER_ID'=>10002,
        'INTERNAL_ADMIN_ID'=>10004,
    ],
];