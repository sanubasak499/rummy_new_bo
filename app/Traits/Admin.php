<?php

namespace App\Traits;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use App\Models\Role;
use App\Models\CustomAdminPermission;
use App\Models\Permission;
use App\Models\Module;
use App\Models\RolePermission;
use DB;
use Auth;

/**
 * @property  \Illuminate\Database\Eloquent\Collection  roles
 */
trait Admin
{
    /**
     * Return default Admin Role.
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
    public function custom_permissions()
    {
        return $this->hasMany(CustomAdminPermission::class, 'admin_id');
    }

    public function loadCustomPermissions(){
        if (!$this->relationLoaded('custom_permissions')) {
            $this->load('custom_permissions.permission_details');
        }   
    }

    public function role_permission(){
        return $this->hasMany(RolePermission::class, 'role_id');
    }

    public function loadRolePermissions(){
        if (!$this->relationLoaded('role_permission')) {
            $this->load('role_permission');
        }        
    }

    private function loadRolesRelations()
    {
        if (!$this->relationLoaded('role')) {
            $this->load('role');
        }
    }

    private function loadPermissionsRelations()
    {
        $this->loadRolesRelations();
        
        if ($this->role && !$this->role->relationLoaded('permissions')) {
            $this->role->load('permissions');
        }
    }

    // custom authentications then check by passing view or edit as access and module name as $module 
    public function hasPermission($access,$module){
        return $this->hasModulePermission($access,$module);
    }
    // check module edit permission on behalf of module name
    public function hasEditPermission($module){
        return $this->hasModulePermission('edit',$module);
    }

    // check view permission on behalf of module name
    public function hasViewPermission($module){
        return $this->hasModulePermission('view',$module);
    }

    // check permission on priority basis if custom permission exist for particular module then give more priority to that
    private function hasModulePermission($access, $module){
        if($this->checkSuperAdmin()){
            return true;
        }
        if($this->allAccess){
            return $this->allAccess->where('module_key', $module)->where("$access", 1)->first() ? true : false;
        }
        
        if($canAccess = $this->hasCustomPermission($access, $module)){
            return $canAccess->{$access} == 1;
        }
        $canAccess = $this->hasPermissionByRole($access, $module) ? true : false;
        return $canAccess;
    }
    
    // check custom permission exist for specific module
    private function hasCustomPermission($access, $module){
        $admin_id = $this->id;
        
        $canAccess = Module::join('bo_admin_customized_admin_user_permissions as cp', function($join) use ($admin_id, $access){
            $join->on('bo_admin_modules.id', '=', 'cp.module_id')
                ->where('cp.admin_id', $admin_id);
            })
            ->where('module_key', $module)->first();
        
        return $canAccess;
    }

    // check role permission on behalf of module with access
    private function hasPermissionByRole($access, $module){
        $role_id = $this->role_id;
        $canAccess = Module::join('bo_admin_roles_permissions as rp', function($join) use ($role_id, $access){
            $join->on('bo_admin_modules.id', '=', 'rp.module_id')
                ->where('rp.role_id', $role_id)
                ->when($access == "view", function($query){
                    return $query->where('rp.view', 1);
                })
                ->when($access == "edit", function($query){
                    return $query->where("rp.edit",1);
                });
            })
            ->where('module_key', $module)->first();
        
        return $canAccess ? true : false;
    }

    public function getAllPermissions(){
        if(!$this->allAccess){
            $this->allAccess = $this->loadFinalPermissions();
        }
        return $this->allAccess;
    }

    private function loadFinalPermissions(){
        $this->loadPermissionByRole();
        $this->loadPermissionByCustom();
        $accessByRoles = $this->accessByRoles->keyBy('module_id');
        $accessByCustom = $this->accessByCustom->keyBy('module_id');
        $allAccess = $accessByRoles->replace($accessByCustom);
        return $allAccess;
    }

    private function loadPermissionByRole(){
        if(!$this->accessByRoles){
            $this->accessByRoles = $this->getRolePermissions();
        }
        return $this->accessByRoles;
    }
    private function loadPermissionByCustom(){
        if(!$this->accessByCustom){
            $this->accessByCustom = $this->getCustomPermissions();
        }
        return $this->accessByCustom;
    }

    public function getCustomPermissions(){
        $user = $this;
        $accessByCustom = Module::select('menu_id', 'module_key', 'module_id', 'view', 'edit')
        ->join('bo_admin_customized_admin_user_permissions as cp', function($join) use ($user){
            $join->on('bo_admin_modules.id', '=', 'cp.module_id')
                ->where('cp.admin_id', $user->id);
        })
        ->get();
        return $accessByCustom;
    }

    public function getRolePermissions(){
        $user = $this;
        $accessByRoles = Module::select('menu_id', 'module_key', 'module_id', 'view', 'edit')
            ->join('bo_admin_roles_permissions as rp', function($join) use ($user){
                $join->on('bo_admin_modules.id', '=', 'rp.module_id')
                    ->where('rp.role_id', $user->role_id);
            })
            ->get();
        return $accessByRoles;
    }
    public function checkSuperAdmin(){
        return in_array($this->id, config('poker_config.admin.super_admins'));
    }   
    
    public function menuPermission($access, $menu_id){
        if($this->checkSuperAdmin()){
            return true;
        }
        $this->getAllPermissions();
        return $this->allAccess->where('menu_id', $menu_id)->where("$access", 1)->first() ? true : false;
    
    }
}
