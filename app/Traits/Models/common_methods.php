<?php

namespace App\Traits\Models;

use App\Models\PaymentTransaction;
use App\Models\PromoCampaign;
use App\Models\MasterTransactionHistoryRewardcoins;
use App\Models\helpdesk\MasterTransactionHistory;
use App\Models\UserPoint;
use App\Models\Tournament;
use App\Models\WithdrawalCriteria;
use App\Models\PlayerLedger;
use App\Models\User;
use App\Models\WithdrawTransactionHistory;
use DB;

/**
 * 
 */
trait common_methods
{
  // Function to get payment records of a payment from payment_transaction table
  public function getPaymentDetailsWC($paymentRefNo)
  {
    $transStatus = array('103', '125');
    // DB::enableQueryLog();
    //Get userid, amount,promo code, deposit time of payment by passing reference number
    $paymentDetailsResult = PaymentTransaction::select('USER_ID', 'INTERNAL_REFERENCE_NO', 'PAYMENT_TRANSACTION_AMOUNT', 'PROMO_CODE', 'UPDATED_DATE', 'PAYMENT_TRANSACTION_CREATED_ON')
      ->when(!empty($paymentRefNo), function ($query) use ($paymentRefNo) {
        return $query->where('INTERNAL_REFERENCE_NO', $paymentRefNo);
      })
      ->when(!empty($transStatus), function ($query) use ($transStatus) {
        return $query->whereIn('PAYMENT_TRANSACTION_STATUS', $transStatus);
      })->first();
    // $queriesss = DB::getQueryLog();
    // dd($queriesss);
    return $paymentDetailsResult;
  }

  // Function to get COINS_REQUIRED of a particular deposit code
  public function getCoinsRequired($codeUsed)
  {
    //Get previous withdrawal criteria data
    // DB::enableQueryLog();
    $codeCoinsResult = PromoCampaign::select('COINS_REQUIRED', 'MAXIMUM_DEPOSIT_AMOUNT', 'PROMO_CAMPAIGN_ID')
      ->with('getCoinsRequired:PROMO_CAMPAIGN_ID,P_PROMO_CHIPS,P_PROMO_VALUE')
      ->when(!empty($codeUsed), function ($query) use ($codeUsed) {
        return $query->where('PROMO_CAMPAIGN_CODE', $codeUsed);
      })
      ->first();
    // $queriesss = end(DB::getQueryLog());
    // dd($queriesss);
    return $codeCoinsResult;
  }

  // Function to get coins accumulated between 2 dates
  public function getAccCoinsBetweenDates($userId, $startDate, $endDate)
  {
    // DB::enableQueryLog();
    $accCoinsResult = MasterTransactionHistoryRewardcoins::select(DB::Raw('IFNULL(SUM(TRANSACTION_AMOUNT),0) as ACC_REWARD_COINS'))
      ->when(!empty($userId), function ($query) use ($userId) {
        return $query->where('USER_ID', $userId);
      })
      ->whereBetween('TRANSACTION_DATE', [$startDate, $endDate])
      ->first();
    // $queriesss = DB::getQueryLog();
    // dd($queriesss);
    return $accCoinsResult;
  }

  public function getUsercurrentBalance($user_id, $coin_type_id)
  {
    // DB::enableQueryLog();
    return $userCurrentBal = UserPoint::select('USER_DEPOSIT_BALANCE', 'USER_WIN_BALANCE', 'USER_TOT_BALANCE', 'USER_PROMO_BALANCE')->where(['USER_ID' => $user_id, 'COIN_TYPE_ID' => $coin_type_id])->first();
    // $queriesss = DB::getQueryLog();
    // dd($queriesss);
  }

  // Function to get currentBalance of a user on cash tables
  public function getCurrentBalanceOnCash($userId)
  {

    // DB::enableQueryLog();
    $currBalOnCashResult = DB::select(DB::raw("SELECT tmp1.INTERNAL_REFERENCE_NO, tmp2.CLOSING_TOT_BALANCE AS BAL FROM(SELECT DISTINCT(INTERNAL_REFERENCE_NO) 
    AS INTERNAL_REFERENCE_NO FROM master_transaction_history WHERE USER_ID=$userId AND TRANSACTION_TYPE_ID=31 AND TRANSACTION_DATE >= ADDDATE(NOW() , 
    INTERVAL -10 hour) AND INTERNAL_REFERENCE_NO NOT IN(SELECT INTERNAL_REFERENCE_NO FROM master_transaction_history  WHERE USER_ID = $userId AND 
    TRANSACTION_TYPE_ID IN (71) AND TRANSACTION_DATE >= ADDDATE(NOW() , INTERVAL -10 hour))) tmp1 INNER JOIN(SELECT pwt.GAME_TRANSACTION_ID, 
    pwt.INTERNAL_REFERENCE_NO, pwt.CLOSING_TOT_BALANCE FROM poker_wallet_transaction pwt INNER JOIN(SELECT INTERNAL_REFERENCE_NO,MAX(GAME_TRANSACTION_ID)
     AS GAME_TRANSACTION_ID FROM poker_wallet_transaction WHERE USER_ID=$userId AND CREATED >= adddate(now(), interval -10 hour) GROUP BY 
     INTERNAL_REFERENCE_NO) t1 ON pwt.GAME_TRANSACTION_ID=t1.GAME_TRANSACTION_ID WHERE pwt.USER_ID=$userId AND CREATED >= adddate(now(), interval -10 hour) 
     ORDER BY pwt.GAME_TRANSACTION_ID DESC) tmp2 ON tmp1.INTERNAL_REFERENCE_NO = tmp2.INTERNAL_REFERENCE_NO WHERE tmp2.CLOSING_TOT_BALANCE > 0"));
    // $queriesss = DB::getQueryLog();
    // dd($queriesss);
    if (!empty($currBalOnCashResult)) {
      $totBuyInOnCash = 0;
      $reNoArray = "";
      foreach ($currBalOnCashResult as $currBalOnCash) {
        $reNoArray = $reNoArray . "'" . $currBalOnCash['INTERNAL_REFERENCE_NO'] . "',";
        $totBuyInOnCash = $totBuyInOnCash + $currBalOnCash['BAL'];
      }
      $reNoArray = rtrim($reNoArray, ", ");
      //  DB::enableQueryLog();
      $getPromoBuyInResult = MasterTransactionHistory::select(DB::Raw('IFNULL(SUM(TRANSACTION_AMOUNT),0) AS PROMO_BUYIN'))
        ->when(!empty($reNoArray), function ($query) use ($reNoArray) {
          return $query->whereIn('INTERNAL_REFERENCE_NO', $reNoArray);
        })
        ->when(!empty($userId), function ($query) use ($userId) {
          return $query->where('USER_ID', $userId);
        })
        ->where('BALANCE_TYPE_ID', 2)
        ->first();
      // $queriesss = DB::getQueryLog();
      // dd($queriesss);
      $buyInToBeConsideredOnCash = 0;
      if (($totBuyInOnCash - $getPromoBuyInResult->PROMO_BUYIN) < 0) {
        $buyInToBeConsideredOnCash = 0;
      } else {
        $buyInToBeConsideredOnCash = $totBuyInOnCash - $getPromoBuyInResult->PROMO_BUYIN;
      }
    } else {

      $buyInToBeConsideredOnCash = 0;
    }
    // print_r('$buyInToBeConsideredOnCash: '.$buyInToBeConsideredOnCash);exit;
    return $buyInToBeConsideredOnCash;
  }

  // Function to get total buy-ins made by user in upcoming tournaments
  public function getCurrentBalanceOnTour($userId)
  {
    $currBalOnTourResult = Tournament::from('tournament as t')
      ->select(DB::Raw('IFNULL(SUM(t.BUYIN+t.ENTRY_FEE),0) AS TOT_TOUR_BUYIN'))
      ->leftJoin('tournament_user_ticket as tu', 'tu.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
      ->where('tu.USER_ID', $userId)
      ->where('t.IS_ACTIVE', 1)
      ->where('tu.GENERATED_TICK_MODE', 1)
      ->where('t.TOURNAMENT_START_TIME', '>=', date('Y-m-d') . ' 00:00:00')
      ->first();
    return $currBalOnTourResult->TOT_TOUR_BUYIN;
  }

  // Function to update previous data in to withdrwal_criteria table
  public function updatePreviousWithdrawalCriteria($userId, $withdrawalCriteriaId, $coinsMadeSinceLastDeposit)
  {
    // DB::enableQueryLog();
    return $updatePreviousWithdrawalCriteria = WithdrawalCriteria::where(['USER_ID' => $userId, 'WITHDRAWAL_CRITERIA_ID' => $withdrawalCriteriaId])
      ->update(['COINS_MADE_SINCE_LAST_DEPOSIT' => $coinsMadeSinceLastDeposit]);
    // $queriesss = DB::getQueryLog();
    // dd($queriesss);
  }

  // Function to insert data in to withdrwal_criteria table
  public function insertIntoWithdrawalCriteria(
    $userId,
    $balAtTheTimeOfDeposit,
    $depositAmount,
    $depositDate,
    $codeUsed,
    $codeCoins,
    $coinsMadeSinceLastDeposit,
    $amountToBeKept,
    $coinsToBeMade,
    $coinsStartDate,
    $comment
  ) {

    //  DB::enableQueryLog();
    date_default_timezone_set('Asia/Kolkata');
    $currentDate = date('Y-m-d H:i:s');

    DB::enableQueryLog();
    $WithdrawalCriteria = new WithdrawalCriteria();
    $WithdrawalCriteria->USER_ID          = $userId;
    $WithdrawalCriteria->BAL_AT_DEPOSIT   = $balAtTheTimeOfDeposit;
    $WithdrawalCriteria->DEPOSIT_AMOUNT   = $depositAmount;
    $WithdrawalCriteria->DEPOSIT_DATE     = $depositDate;
    $WithdrawalCriteria->CODE_USED        = $codeUsed;
    $WithdrawalCriteria->CODE_COINS       = $codeCoins;
    $WithdrawalCriteria->COINS_MADE_SINCE_LAST_DEPOSIT  = $coinsMadeSinceLastDeposit;
    $WithdrawalCriteria->AMOUNT_TO_BE_KEPT   = $amountToBeKept;
    $WithdrawalCriteria->COINS_TO_BE_MADE    = $coinsToBeMade;
    $WithdrawalCriteria->COINS_START_DATE    = $coinsStartDate;
    $WithdrawalCriteria->COMMENT             = $comment;
    $WithdrawalCriteria->CREATED_DATE        = $currentDate;

    $resIns = $WithdrawalCriteria->save();
    return $resIns;
    // $queriesss = DB::getQueryLog();
    // dd($queriesss);
  }

  // Function to get last entry of a user from withdrawal_criteria table
  public function getPreviousWithdrawalCriteria($userId)
  {
    //Get previous withdrawal criteria data
    // DB::enableQueryLog();
    $previousCriteriaResult = WithdrawalCriteria::select('AMOUNT_TO_BE_KEPT', 'COINS_TO_BE_MADE', 'COINS_START_DATE', 'WITHDRAWAL_CRITERIA_ID')
      ->when(!empty($userId), function ($query) use ($userId) {
        return $query->where('USER_ID', $userId);
      })
      ->orderBy('DEPOSIT_DATE', 'DESC')
      ->first();
    // $queriesss = DB::getQueryLog();
    // dd($queriesss);
    return $previousCriteriaResult;
  }

  public function checkReferenceAlreadyExistsInPlayerLedger($referenceNumber)
  {
    // DB::enableQueryLog();
    return $previousCriteriaResult = PlayerLedger::select('USER_ID')
      ->where('INTERNAL_REFERENCE_NO', $referenceNumber)
      ->first();
  }

  // Function to get last entry of a user from player ledger table
  public function getPreviousPlayerLedgerData($userId)
  {
    $previousCriteriaResult = PlayerLedger::select('ACTION', 'TOTAL_DEPOSITS', 'TOTAL_WITHDRAWALS', 'TOTAL_TAXABLE_WITHDRAWALS', 'ELIGIBLE_WITHDRAWAL_WITHOUT_TAX', 'EXEMPTION_10K', 'TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX')
      ->where('USER_ID', $userId)
      ->orderBy('PLAYER_LEDGER_ID', 'DESC')
      ->first();
    return $previousCriteriaResult;
  }

  // Function to insert data in to withdrwal_criteria table
  public function insertIntoPlayerLedger($userId, $action, $transactionAmount, $referenceNumber, $totalDeposits, $totalWithdrawals, $totalTaxableWithdrawals, $eligibleWithdrawalWithoutTax, $tenKExemption, $totalEligibleWithdrawalWithoutTax, $paymentTransactionCreatedOn)
  {
    date_default_timezone_set('Asia/Kolkata');
    $currentDate = date('Y-m-d H:i:s');
    // DB::enableQueryLog();
    $insertPlayerLedgerData = new PlayerLedger();
    $insertPlayerLedgerData->USER_ID                                  = $userId;
    $insertPlayerLedgerData->ACTION                                   = $action;
    $insertPlayerLedgerData->TRANSACTION_AMOUNT                       = $transactionAmount;
    $insertPlayerLedgerData->INTERNAL_REFERENCE_NO                    = $referenceNumber;
    $insertPlayerLedgerData->TOTAL_DEPOSITS                           = $totalDeposits;
    $insertPlayerLedgerData->TOTAL_WITHDRAWALS                        = $totalWithdrawals;
    $insertPlayerLedgerData->TOTAL_TAXABLE_WITHDRAWALS                = $totalTaxableWithdrawals;
    $insertPlayerLedgerData->ELIGIBLE_WITHDRAWAL_WITHOUT_TAX          = $eligibleWithdrawalWithoutTax;
    $insertPlayerLedgerData->EXEMPTION_10K                            = $tenKExemption;
    $insertPlayerLedgerData->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX    = $totalEligibleWithdrawalWithoutTax;
    $insertPlayerLedgerData->CREATED_DATE                             = $currentDate;
    $insertPlayerLedgerData->PAYMENT_TRANSACTION_CREATED_ON           = $paymentTransactionCreatedOn;

    return $insertPlayerLedgerData->save();
    //  $queriesss = DB::getQueryLog();
    //    dd($queriesss);

  }

  // Function to get withdrawal records from payment_transaction table
  public function getWithdrawalDetails($referenceNo, $withdrawalStatus)
  {
    //Get userid, reference number, amount of withdrawal by passing reference number
    $transStatusId = array('203', '112');
    $withdrawalDetailsResult = PaymentTransaction::select('USER_ID', 'INTERNAL_REFERENCE_NO', 'PAYMENT_TRANSACTION_AMOUNT', 'PAYMENT_TRANSACTION_CREATED_ON')
      ->where('INTERNAL_REFERENCE_NO', $referenceNo)
      ->when($withdrawalStatus == "NOT_REVERT_REJECTED", function ($query) use ($transStatusId) {
        return $query->whereNotIn('PAYMENT_TRANSACTION_STATUS', $transStatusId);
      })
      ->first();
    return $withdrawalDetailsResult;
  }

  // Function to get all entries of a user from player ledger table after the withdrawal revert reference number
  public function getPreviousPlayerLedgerDataForRevert($userId, $referenceNumber)
  {

    $referenceLedgerResult = PlayerLedger::select('PLAYER_LEDGER_ID')
      ->where('USER_ID', $userId)
      ->where('INTERNAL_REFERENCE_NO', $referenceNumber)
      ->first();
    //print_r ($referenceLedgerResult);exit;

    if (!empty($referenceLedgerResult->PLAYER_LEDGER_ID)) {
      $previousLedgerResult = PlayerLedger::select('PLAYER_LEDGER_ID', 'ACTION', 'TRANSACTION_AMOUNT', 'INTERNAL_REFERENCE_NO')
        ->where('USER_ID', $userId)
        ->where('PLAYER_LEDGER_ID', $referenceLedgerResult->PLAYER_LEDGER_ID)
        ->orderBy('PLAYER_LEDGER_ID', 'ASC')
        ->get();

      //print_r ($previousLedgerResult);exit;
      return $previousLedgerResult;
    } else {
      return "REF_NOT_FOUND";
    }
  }

  // Function to delete a particular player ledger id
  public function deletePlayerLedgerEntry($userId, $playerLedgerID)
  {
    return $deletedRows = PlayerLedger::where(['USER_ID' => $userId, 'PLAYER_LEDGER_ID' => $playerLedgerID])->delete();
  }

  // assign tournament ticket based in promo code
  public function executeProcedureTicket($tourId, $playerId)
  {
    $tour_ticket_in = DB::select('call sp_create_user_tournament_ticket_external("AAA","' . $tourId . '","' . $playerId . '",@out)', [
      $tourId,
      $playerId,
    ]);
    $tour_ticket_out = DB::select(DB::raw("select @out as 'out'"));
    $out = $tour_ticket_out[0]->out;
    return $out;
  }

  // assign tournament ticket based in promo code
  public function executeProcedure($tourId, $playerId)
  {
    $tour_ticket_in = DB::select("call sp_tournament_registration(?,?,@out1,@out2)", [
      $tourId,
      $playerId,
    ]);
    $tour_ticket_out = DB::select(DB::raw("select @out1 as 'out',@out2 as 'out2'"));
    $out1 = $tour_ticket_out[0]->out;
    $out2 = $tour_ticket_out[0]->out2;
    $out = ['chip' => $out1, 'ref_no' => $out2];
    return $out;
  }

  public function getUserDetailsFromUserId($userId)
  {

    $getUserDetails = User::select('USERNAME', 'EMAIL_ID', 'CONTACT', 'FIRSTNAME', 'LASTNAME', 'ADDRESS')
      ->where('USER_ID', $userId)
      ->first();
    return $getUserDetails;
  }

  public function getUserDetailsFromUserName($userName)
  {

    $getUserDetails = User::select('USER_ID', 'EMAIL_ID', 'CONTACT')
      ->where('USERNAME', $userName)
      ->first();
    return $getUserDetails;
  }

  public function getTournamentDetailsById($tourId)
  {
    $getTourDetails = Tournament::select('TOURNAMENT_NAME', 'BUYIN', 'ENTRY_FEE', 'TOURNAMENT_START_TIME', 'REGISTER_START_TIME', 'REGISTER_END_TIME')
      ->where('TOURNAMENT_ID', $tourId)
      ->first();
    return $getTourDetails;
  }

  //update withdraw crieteria after payment approve

  public function updateWithdrawalCriteria($paymentRefNo)
  {
    // Get userid, amount,promo code, deposit time of payment by passing reference number
    $paymentDetailsResult = $this->getPaymentDetailsWC($paymentRefNo);
    if (!empty($paymentDetailsResult)) {
      $userId = $paymentDetailsResult->USER_ID;
      $depositAmount = $paymentDetailsResult->PAYMENT_TRANSACTION_AMOUNT;
      $codeUsed = $paymentDetailsResult->PROMO_CODE;
      $depositDate = $paymentDetailsResult->UPDATED_DATE;
      $comment = "";

      //Get previous withdrawal criteria data
      //define in Traits/Models/common_method 
      $previousCriteriaResult = $this->getPreviousWithdrawalCriteria($userId);

      if (!empty($previousCriteriaResult)) { // Case: if there are previous records of criteria

        //activity entry
        // $data = array('Case' => ' if there are previous records of criteria', 'data' => $previousCriteriaResult, 'userId' => $userId);
        // $action = "Approve payment Withdrawal Criteria -" . $userId;
        // $this->insertAdminActivity($data, $action);

        $previousATK = $previousCriteriaResult->AMOUNT_TO_BE_KEPT;
        $previousCoinsToBeMade = $previousCriteriaResult->COINS_TO_BE_MADE;
        $previousCoinsStartDate = $previousCriteriaResult->COINS_START_DATE;
        $withdrawalCriteriaId = $previousCriteriaResult->WITHDRAWAL_CRITERIA_ID;


        $codeCoins = 0; // Initialise to 0
        $maxDepositAmount = 0; // Initialise to 0
        $depositAmountToBeConsidered = 0;
        $codeCoinsToBeConsidered = 0;

        //Get coins required for that code
        // Function to get COINS_REQUIRED of a particular deposit code
        //define in Traits/Models/common_method 
        $codeCoinsResult = $this->getCoinsRequired($codeUsed);

        if (!empty($codeCoinsResult) && $codeCoinsResult->COINS_REQUIRED != NULL && $codeCoinsResult->COINS_REQUIRED != "") {

          $codeCoins = $codeCoinsResult->COINS_REQUIRED;
          $maxDepositAmount = $codeCoinsResult->MAXIMUM_DEPOSIT_AMOUNT;
          $bonusChipsFlag = $codeCoinsResult->P_PROMO_CHIPS;
          $bonusPercentage = $codeCoinsResult->P_PROMO_VALUE;
          $depositAmountToBeConsidered = $depositAmount;
          $codeCoinsToBeConsidered = $codeCoins;
          if ($depositAmount > $maxDepositAmount) {
            $depositAmountToBeConsidered = $maxDepositAmount;
          }
          if ($bonusChipsFlag == 1) {
            $bonusAmount = round(($bonusPercentage / 100) * $depositAmountToBeConsidered);
            if ($bonusAmount < $codeCoins && $bonusAmount != 0) {
              $codeCoinsToBeConsidered = $bonusAmount;
            }
          }
        }

        if ($codeCoinsToBeConsidered == 0) {
          $depositAmountToBeConsidered = 0;
        }

        $newATK = 0; // Initialise to 0
        $newCoinsToBeMade = 0; // Initialise to 0
        $newCoinsStartDate = $depositDate;

        // Get Coins made since previousCoinsStartDate
        // Function to get coins accumulated between 2 dates
        //define in Traits/Models/common_method 
        $accCoinsResult = $this->getAccCoinsBetweenDates($userId, $previousCoinsStartDate, $depositDate);
        $coinsMadeSinceLastDeposit = $accCoinsResult->ACC_REWARD_COINS;

        // Get Total Balance of user including cash tables balance and future tournaments buy-ins
        $coinType = "1";
        // $cashBalanceResult = $this->getUserBalance($userId,$coinType);
        //define in Traits/Models/common_method 
        $cashBalanceResult = $this->getUsercurrentBalance($userId, $coinType);

        $totalCashBalance = $cashBalanceResult->USER_DEPOSIT_BALANCE + $cashBalanceResult->USER_WIN_BALANCE - $depositAmount;
        //define in Traits/Models/common_method 
        $currentBalanceOnCash = $this->getCurrentBalanceOnCash($userId);
        //define in Traits/Models/common_method
        $currentBalanceOnTour = $this->getCurrentBalanceOnTour($userId);

        $balAtTheTimeOfDeposit = $totalCashBalance + $currentBalanceOnCash + $currentBalanceOnTour;

        // Calculate new amount to be kept

        if ($codeUsed != NULL && $codeUsed != "") { // If there is a code used
          //activity entry
          // $data = array('Case' => ' If there is a code used', 'data' => $codeUsed, 'userId' => $userId);
          // $action = "Approve payment Withdrawal Criteria -" . $userId;
          // $this->insertAdminActivity($data, $action);
          if (($coinsMadeSinceLastDeposit >= $previousCoinsToBeMade) || ($balAtTheTimeOfDeposit < (0.1 * $previousATK))) {
            // If the user has made the coins OR almost lost amount
            $newATK = $depositAmountToBeConsidered;
            $comment = $comment . "New 'AMOUNT TO BE KEPT' has been changed to the 'CURR. DEPOSIT AMOUNT' as you have 'COMPLETED' the 'COINS CRITERIA' or you almost 'LOST' your 'PREVIOUS AMOUNT TO BE KEPT' as there was a 'CODE USED'.";
          } else {

            // If the user hasnt made the coins and hasnt lost deposit amount also
            $ratio = 1 - ($coinsMadeSinceLastDeposit / $previousCoinsToBeMade);
            if (($ratio * $previousATK) > $balAtTheTimeOfDeposit) {
              // If the user has made coins in ratio to his balance
              $newATK = $balAtTheTimeOfDeposit + $depositAmountToBeConsidered;
              $comment = $comment . "New 'AMOUNT TO BE KEPT' has been changed to the 'CURR. DEPOSIT AMOUNT + BAL AT THE TIME OF DEPOSIT' as you have 'NOT COMPLETED' the 'COINS CRITERIA' and 'YOU HAVE NOT LOST THE PREVIOUS AMOUNT TO BE KEPT' but your 'BAL AT THE TIME OF DEPOSIT' was 'LESS THAN' the '(RATIO OF COINS MADE x PREVIOUS AMOUNT TO BE KEPT)' as there was a 'CODE USED'.";
            } else {
              // If the user hasnt made coins in ratio to his balance
              $newATK = ($ratio * $previousATK) + $depositAmountToBeConsidered;
              $comment = $comment . "New 'AMOUNT TO BE KEPT' has been changed to the 'CURR. DEPOSIT AMOUNT + (RATIO OF COINS MADE x PREVIOUS AMOUNT TO BE KEPT)' as you have 'NOT COMPLETED' the 'COINS CRITERIA' and 'YOU HAVE NOT LOST THE PREVIOUS AMOUNT TO BE KEPT' but your 'BAL AT THE TIME OF DEPOSIT' was 'GREATER THAN' the '(RATIO OF COINS MADE x PREVIOUS AMOUNT TO BE KEPT)' as there was a 'CODE USED'.";
            }
          }
        } else { //If no code used
          if (($coinsMadeSinceLastDeposit >= $previousCoinsToBeMade) || ($balAtTheTimeOfDeposit < (0.1 * $previousATK))) {
            // If the user has made the coins OR almost lost amount
            $newATK = 0;
            $comment = $comment . "New 'AMOUNT TO BE KEPT' has been changed to '0' as you have 'COMPLETED' the 'COINS CRITERIA' or you almost 'LOST' your 'PREVIOUS AMOUNT TO BE KEPT' and there was 'NO CODE USED'.";
            //activity entry
            // $data = array('Case' => ' If there is no promo code used', 'COMMENT' => $comment, 'userId' => $userId);
            // $action = "Approve payment Withdrawal Criteria -" . $userId;
            // $this->insertAdminActivity($data, $action);
          } else {

            // If the user hasnt made the coins and hasnt lost deposit amount also
            //basant
            //echo $coinsMadeSinceLastDeposit.'---'.$previousCoinsToBeMade; exit;

            $ratio = 1 - ($coinsMadeSinceLastDeposit / $previousCoinsToBeMade);

            if (($ratio * $previousATK) > $balAtTheTimeOfDeposit) {
              // If the user has made coins in ratio to his balance
              $newATK = $balAtTheTimeOfDeposit;
              $comment = $comment . "New 'AMOUNT TO BE KEPT' has been changed to 'BAL AT THE TIME OF DEPOSIT' as you have 'NOT COMPLETED' the 'COINS CRITERIA' and 'YOU HAVE NOT LOST THE PREVIOUS AMOUNT TO BE KEPT' but your 'BAL AT THE TIME OF DEPOSIT' was 'LESS THAN' the '(RATIO OF COINS MADE x PREVIOUS AMOUNT TO BE KEPT)' and there was 'NO CODE USED'.";
            } else {
              // If the user hasnt made coins in ratio to his balance
              $newATK = $ratio * $previousATK;
              $comment = $comment . "New 'AMOUNT TO BE KEPT' has been changed to '(RATIO OF COINS MADE x PREVIOUS AMOUNT TO BE KEPT)' as you have 'NOT COMPLETED' the 'COINS CRITERIA' and 'YOU HAVE NOT LOST THE PREVIOUS AMOUNT TO BE KEPT' but your 'BAL AT THE TIME OF DEPOSIT' was 'GREATER THAN' the '(RATIO OF COINS MADE x PREVIOUS AMOUNT TO BE KEPT)' and there was 'NO CODE USED'.";
            }
            //activity entry
            // $data = array('Case' => ' If there is no previous record', 'COMMENT' => $comment, 'userId' => $userId);
            // $action = "Approve payment Withdrawal Criteria-" . $userId;
            // $this->insertAdminActivity($data, $action);
          }
        }

        // Calculate new coins to be made
        if (($coinsMadeSinceLastDeposit >= $previousCoinsToBeMade) || ($balAtTheTimeOfDeposit < (0.1 * $previousATK))) {
          // 			// If the user has made the coins OR almost lost amount
          if ($codeUsed == NULL || $codeUsed == "") {
            // If no code used in current deposit the new coins to made is set as 0
            $newCoinsToBeMade = 0;
            $comment = $comment . " <---> New 'COINS TO BE MADE' has been changed to '0' as you have 'COMPLETED' the 'COINS CRITERIA' or you almost 'LOST' your 'PREVIOUS AMOUNT TO BE KEPT' and there was 'NO CODE USED'.";
          } else {
            // If code is used in current deposit the new coins to made is set as current code coins
            $newCoinsToBeMade = $codeCoinsToBeConsidered;
            $comment = $comment . " <---> New 'COINS TO BE MADE' has been changed to 'COINS OF CURR. CODE' as you have 'COMPLETED' the 'COINS CRITERIA' or you almost 'LOST' your 'PREVIOUS AMOUNT TO BE KEPT' and there was a 'CODE USED'.";
          }
          //activity entry
          // $data = array('Case' => ' If there usermade some coin', 'COMMENT' => $comment, 'userId' => $userId);
          // $action = "Approve payment Withdrawal Criteria-" . $userId;
          // $this->insertAdminActivity($data, $action);
        } else {
          // 			// If the user hasnt made the coins and hasnt lost deposit amount also
          $newCoinsToBeMade = ($previousCoinsToBeMade - $coinsMadeSinceLastDeposit) + $codeCoinsToBeConsidered;
          $comment = $comment . " <---> New 'COINS TO BE MADE' has been changed to '(PREVIOUS COINS TO BE MADE - COINS MADE SINCE LAST DEPOSIT) + COINS OF CURR. CODE' as you have 'NOT COMPLETED' the 'COINS CRITERIA' and you have 'NOT LOST' your 'PREVIOUS AMOUNT TO BE KEPT'.";
        }
        //define in Traits/Models/common_method 
        $updatePreviousWithdrawalCriteria = $this->updatePreviousWithdrawalCriteria($userId, $withdrawalCriteriaId, $coinsMadeSinceLastDeposit);
        //define in Traits/Models/common_method 
        $insertIntoWithdrawalCriteria = $this->insertIntoWithdrawalCriteria($userId, $balAtTheTimeOfDeposit, $depositAmount, $depositDate, $codeUsed, $codeCoinsToBeConsidered, $coinsMadeSinceLastDeposit, $newATK, $newCoinsToBeMade, $newCoinsStartDate, $comment);
        //activity entry
        $data = array('insertIntoWithdrawalCriteria' => $insertIntoWithdrawalCriteria, 'COMMENT' => $comment);
        $action = "Withdrawal Criteria while payment approve";
        $this->insertAdminActivity($data, $action);
      } else { // Case: if there was no previous criteria

        if ($codeUsed != NULL && $codeUsed != "") { // If there is a code used


          $codeCoins = 0;
          $maxDepositAmount = 0;
          $depositAmountToBeConsidered = 0;
          $codeCoinsToBeConsidered = 0;
          //Get coins required for that code
          $codeCoinsResult = $this->getCoinsRequired($codeUsed);

          if (!empty($codeCoinsResult) && $codeCoinsResult->COINS_REQUIRED != NULL && $codeCoinsResult->COINS_REQUIRED != "") {
            $codeCoins = $codeCoinsResult->COINS_REQUIRED;
            $maxDepositAmount = $codeCoinsResult->MAXIMUM_DEPOSIT_AMOUNT;
            $bonusChipsFlag = $codeCoinsResult->P_PROMO_CHIPS;
            $bonusPercentage = $codeCoinsResult->P_PROMO_VALUE;
            $depositAmountToBeConsidered = $depositAmount;
            $codeCoinsToBeConsidered = $codeCoins;
            if ($depositAmount > $maxDepositAmount) {
              $depositAmountToBeConsidered = $maxDepositAmount;
            }
            if ($bonusChipsFlag == 1) {
              $bonusAmount = round(($bonusPercentage / 100) * $depositAmountToBeConsidered);
              if ($bonusAmount < $codeCoins && $bonusAmount != 0) {
                $codeCoinsToBeConsidered = $bonusAmount;
              }
            }
          }

          if ($codeCoinsToBeConsidered == 0) {
            $depositAmountToBeConsidered = 0;
          }

          $coinsMadeSinceLastDeposit = 0;

          // Get Total Balance of user including cash tables balance and future tournaments buy-ins
          $coinType = "1";
          // $cashBalanceResult = $this->getUserBalance($userId,$coinType);
          //define in Traits/Models/common_method 
          $cashBalanceResult = $this->getUsercurrentBalance($userId, $coinType);
          $totalCashBalance = $cashBalanceResult->USER_DEPOSIT_BALANCE + $cashBalanceResult->USER_WIN_BALANCE - $depositAmount;
          //define in Traits/Models/common_method 
          $currentBalanceOnCash = $this->getCurrentBalanceOnCash($userId);
          //define in Traits/Models/common_method 
          $currentBalanceOnTour = $this->getCurrentBalanceOnTour($userId);
          $balAtTheTimeOfDeposit = $totalCashBalance + $currentBalanceOnCash + $currentBalanceOnTour;
          $newATK = $depositAmountToBeConsidered;
          $newCoinsToBeMade = $codeCoinsToBeConsidered;
          $newCoinsStartDate = $depositDate;
          $comment = $comment . "New 'AMOUNT TO BE KEPT' has been changed to the 'CURR. DEPOSIT AMOUNT' as there was 'NO PREVIOUS WITHDRAWAL CRITERIA' and there was a 'CODE USED'.";
          $comment = $comment . " <---> New 'COINS TO MADE' has been changed to the 'CURR. CODE COINS' as there was 'NO PREVIOUS WITHDRAWAL CRITERIA' and there was a 'CODE USED'.";
          $insertIntoWithdrawalCriteria = $this->insertIntoWithdrawalCriteria($userId, $balAtTheTimeOfDeposit, $depositAmount, $depositDate, $codeUsed, $codeCoinsToBeConsidered, $coinsMadeSinceLastDeposit, $newATK, $newCoinsToBeMade, $newCoinsStartDate, $comment);
          //activity entry
          $data = array('insertIntoWithdrawalCriteria' => $insertIntoWithdrawalCriteria, 'COMMENT' => $comment);
          $action = "Withdrawal Criteria while payment approve";
          $this->insertAdminActivity($data, $action);
        } else { //If no code used
          // Do nothing as no previous record of criteria and no code also used
          //$comment = $comment."No previous criteria and no code used at the time of deposit so no new criteria inserted.";
          // print_r('$comment - '.$comment.'<br/>');
        }
      }
    } else {
      // No successful payments found for that refernce number
      $comment = $comment . "Payment was not successfull.";
      //activity entry
      $data = array('Case' => 'If there is no Payment was successfull', 'comment' => $comment);
      $action = "Withdrawal Criteria while payment approve";
      $this->insertAdminActivity($data, $action);
      // print_r('$comment - '.$comment.'<br/>');
    }
    //exit;

  }

  //player ledger start-------------------------------------

  // Function to get update player ledger table on a successfull deposit or withdrawal
  public function updatePlayerLedger($referenceNumber, $action)
  {

    if ($action == "DEPOSIT") {

      // Check if reference number already exists then skip
      //define in Traits/Models/common_method 
      $refRes = $this->checkReferenceAlreadyExistsInPlayerLedger($referenceNumber);

      if (!empty($refRes)) {
        return "";
      }

      // Get userid, amount,promo code, deposit time of payment by passing reference number
      //define in Traits/Models/common_method 
      $paymentDetailsResult = $this->getPaymentDetailsWC($referenceNumber);

      if (!empty($paymentDetailsResult->INTERNAL_REFERENCE_NO)) {
        // If records exists for that successful payment
        $userId = $paymentDetailsResult->USER_ID;
        $depositAmount = $paymentDetailsResult->PAYMENT_TRANSACTION_AMOUNT;
        $paymentTransactionCreatedOn = $paymentDetailsResult->PAYMENT_TRANSACTION_CREATED_ON;
        //Get previous player ledger data
        //define in Traits/Models/common_method 
        $previousLedgerResult = $this->getPreviousPlayerLedgerData($userId);

        // print_r($previousLedgerResult);exit;

        if (!empty($previousLedgerResult)) {
          // Case: if there are previous records of player ledger
          $newTotalDeposits = $depositAmount + $previousLedgerResult->TOTAL_DEPOSITS;
          $newTotalWithdrawals = $previousLedgerResult->TOTAL_WITHDRAWALS;
          $newTotalTaxableWithdrawals = $previousLedgerResult->TOTAL_TAXABLE_WITHDRAWALS;

          $newEligibleWithdrawalWithoutTax = $newTotalDeposits - $newTotalWithdrawals + $newTotalTaxableWithdrawals;
          if (($newTotalDeposits - $newTotalWithdrawals + $newTotalTaxableWithdrawals) < 0) {
            $newEligibleWithdrawalWithoutTax = 0;
          }
          $newTenKExemption = $previousLedgerResult->EXEMPTION_10K;
          // if (round($newTotalTaxableWithdrawals) == 0) {
          // 	$newTenKExemption = 10000;
          // }

          $newTotalEligibleWithdrawalWithoutTax = $newEligibleWithdrawalWithoutTax + $newTenKExemption;
        } else {
          // Case: if there was no previous ledger details
          $newTotalDeposits = $depositAmount;
          $newTotalWithdrawals = 0;
          $newTotalTaxableWithdrawals = 0;
          $newEligibleWithdrawalWithoutTax = $depositAmount;
          $newTenKExemption = 10000;
          $newTotalEligibleWithdrawalWithoutTax = $newEligibleWithdrawalWithoutTax + $newTenKExemption;
        }
        //define in Traits/Models/common_method 
        $this->insertIntoPlayerLedger($userId, $action, $depositAmount, $referenceNumber, $newTotalDeposits, $newTotalWithdrawals, $newTotalTaxableWithdrawals, $newEligibleWithdrawalWithoutTax, $newTenKExemption, $newTotalEligibleWithdrawalWithoutTax, $paymentTransactionCreatedOn);

        return "PLAYER_LEDGER_INSERTED";
      } else {
        // No successful payments found for that reference number
        return "REF_NOT_FOUND";
      }
    } else if ($action == "WITHDRAWAL") {

      // Check if reference number already exists then skip
      if (!empty($this->checkReferenceAlreadyExistsInPlayerLedger($referenceNumber))) {
        return "";
      }
      // Get details of withdrawal by passing reference number
      //define in Traits/Models/common_method 
      $withdrawalDetailsResult = $this->getWithdrawalDetails($referenceNumber, "NOT_REVERT_REJECTED");
      if (!empty($withdrawalDetailsResult)) {
        // If records exists for that withdrawal

        $userId = $withdrawalDetailsResult->USER_ID;
        $withdrawAmount = $withdrawalDetailsResult->PAYMENT_TRANSACTION_AMOUNT;
        $paymentTransactionCreatedOn = $withdrawalDetailsResult->PAYMENT_TRANSACTION_CREATED_ON;

        //Get previous player ledger data
        $previousLedgerResult = $this->getPreviousPlayerLedgerData($userId);

        if (!empty($previousLedgerResult)) {
          // Case: if there are previous records of player ledger

          $newTotalDeposits = $previousLedgerResult->TOTAL_DEPOSITS;
          $newTotalWithdrawals = $withdrawAmount + $previousLedgerResult->TOTAL_WITHDRAWALS;

          $newTotalTaxableWithdrawals = $previousLedgerResult->TOTAL_TAXABLE_WITHDRAWALS; // Initialising

          if (
            $newTotalTaxableWithdrawals <= 10000 &&
            $previousLedgerResult->EXEMPTION_10K > 0 &&
            $previousLedgerResult->EXEMPTION_10K <= 10000 &&
            ($previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX - $withdrawAmount) <= $previousLedgerResult->EXEMPTION_10K &&
            ($previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX - $withdrawAmount) > 0
          ) {
            // Middle Condition of TenK Exemption
            $newTenKExemption = $previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX - $withdrawAmount;
            $newTotalTaxableWithdrawals = 10000 - $newTenKExemption;
          } else {
            if ($previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX <= $withdrawAmount) {
              $newTotalTaxableWithdrawals = $withdrawAmount - $previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX + $previousLedgerResult->TOTAL_TAXABLE_WITHDRAWALS + $previousLedgerResult->EXEMPTION_10K;
            }
            $newTenKExemption = 0;
            if ($withdrawAmount <= round($previousLedgerResult->ELIGIBLE_WITHDRAWAL_WITHOUT_TAX)) {
              $newTenKExemption = round($previousLedgerResult->EXEMPTION_10K);
            }
            if (round($newTotalTaxableWithdrawals) == 0) {
              $newTenKExemption = 10000;
            }
          }

          $newEligibleWithdrawalWithoutTax = $newTotalDeposits - $newTotalWithdrawals + $newTotalTaxableWithdrawals;
          if (($newTotalDeposits - $newTotalWithdrawals + $newTotalTaxableWithdrawals) < 0) {
            $newEligibleWithdrawalWithoutTax = 0;
          }

          $newTotalEligibleWithdrawalWithoutTax = $newEligibleWithdrawalWithoutTax + $newTenKExemption;
          //define in Traits/Models/common_method 

          $this->insertIntoPlayerLedger($userId, $action, $withdrawAmount, $referenceNumber, $newTotalDeposits, $newTotalWithdrawals, $newTotalTaxableWithdrawals, $newEligibleWithdrawalWithoutTax, $newTenKExemption, $newTotalEligibleWithdrawalWithoutTax, $paymentTransactionCreatedOn);

          $taxableAmount = 0;
          if ($withdrawAmount > $previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX) {
            $taxableAmount = $withdrawAmount - $previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX;
          }

          return $taxableAmount;
        } else {
          // Case: if there was no previous ledger details
          // Ideally this should not be possible, because no player is allowed to withdraw without depositing and if he has deposited then there should be an entry for him in player_ledger table
          // Yet to decide on how to handle this
          return "NO_LEDGER_FOUND";
        }
      } else {
        // No successful withdrawals found for that reference number
        return "REF_NOT_FOUND";
      }
    } else if ($action == "REVERT_REJECT") {

      // Get details of withdrawal by passing reference number
      //define in Traits/Models/common_method 
      $withdrawalDetailsResult = $this->getWithdrawalDetails($referenceNumber, "INCLUDING_REVERT_REJECTED");

      if (!empty($withdrawalDetailsResult)) {
        // If records exists for that withdrawal
        $userId = $withdrawalDetailsResult->USER_ID;

        $withdrawAmount = $withdrawalDetailsResult->PAYMENT_TRANSACTION_AMOUNT;

        //Get previous player ledger data
        //define in Traits/Models/common_method 
        $previousLedgerResult = $this->getPreviousPlayerLedgerDataForRevert($userId, $referenceNumber);
        if (!empty($previousLedgerResult) && $previousLedgerResult != "REF_NOT_FOUND") {

          // 1. We will need to delete the entries including and after this entry in player ledger table
          // 2. Will need to insert entries in player ledger table again sequentially excluding the reverted or rejected one

          // echo "Going to Deleting all player ledger entries";
          // print_r("<br/>");
          // 1. Deleting all player ledger entries

          foreach ($previousLedgerResult as $previousLedgerEntryDelete) {
            // Delete player ledger Entry
            //define in Traits/Models/common_method 

            $this->deletePlayerLedgerEntry($userId, $previousLedgerEntryDelete->PLAYER_LEDGER_ID);
          }
          //   	echo "Re-Insert entries in player ledger table again sequentially excluding the reverted or rejected one";
          // print_r("<br/>");
          // 2. Re-Insert entries in player ledger table again sequentially excluding the reverted or rejected one
          foreach ($previousLedgerResult as $previousLedgerEntryReInsert) {
            // echo "$previousLedgerEntryReInsert->INTERNAL_REFERENCE_NO".'----'.$referenceNumber;exit;
            if ($previousLedgerEntryReInsert->INTERNAL_REFERENCE_NO != $referenceNumber) {
              $reInsertPlayerLedger = $this->updatePlayerLedger($previousLedgerEntryReInsert->INTERNAL_REFERENCE_NO, $previousLedgerEntryReInsert->ACTION);
            }
          }
        } else {
          // Case: if there was no previous ledger details
          // Ideally this should not be possible, because no player is allowed to withdraw without depositing and if he has deposited then there should be an entry for him in player_ledger table
          // Yet to decide on how to handle this
          return "NO_LEDGER_FOUND";
        }
      } else {
        // No withdrawals found for that reference number
        return "REF_NOT_FOUND";
      }
    } else {
      // Wrong Action Passed
      return "INCORRECT_ACTION";
    }
  }


  // get user total balance
  public function getUserTotalDeposit($userid)
  {
    return $totalDeposit = PaymentTransaction::where('USER_ID',$userid)
		->whereIn('PAYMENT_TRANSACTION_STATUS',[103,125])
		->whereIn('TRANSACTION_TYPE_ID',[8,61,62,83,111])
		->sum('PAYMENT_TRANSACTION_AMOUNT');
  }

  // get user total balance
  public function checkUserFirstDepositBonusCode($userid)
  {
    $query = PaymentTransaction::query();
    $query->from(app(PaymentTransaction::class)->getTable() . " as pt");
    $query->join('promo_campaign as pc', 'pc.PROMO_CAMPAIGN_CODE', '=', 'pt.PROMO_CODE');
    $query->whereIn('pt.PAYMENT_TRANSACTION_STATUS',[103,125]);
    $query->where('pt.USER_ID',$userid);
    $query->whereIn('pt.TRANSACTION_TYPE_ID',[61,62,83]);
    $query->whereIn('pc.PROMO_CAMPAIGN_TYPE_ID',[6,8]);
    return $fisrDepositRes = $query->count('pt.USER_ID');
    // print_r($query->toSql());exit;
  }
  // get user total balance
  public function getUserTotalWithdraw($userid)
  {

    return $totalDeposit = PaymentTransaction::where('USER_ID',$userid)
		->where('PAYMENT_TRANSACTION_STATUS',208)
		->where('TRANSACTION_TYPE_ID',10)
		->sum('PAYMENT_TRANSACTION_AMOUNT');

    
  }
}
