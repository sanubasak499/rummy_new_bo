<?php

namespace App\Traits\Models;

use App\Models\CoinType;
use Illuminate\Http\Request;
use DB;

use App\Models\TournamentLevelStructure;
use App\Models\TournamentLimit;
use App\Models\TournamentServer;
use App\Models\Tournament;
use App\Models\TournamentBlindStructure;
use App\Models\DefaultPrizeStructureTypes;
use App\Models\MiniGameType;
use App\Models\TournamentRegistration;
use App\Models\TournamentSubTabCategory;
use App\Models\TournamentUsersRank;
use App\Models\TournamentWinnerLevel;
use App\Models\TournamentWinnerTransaction;
use Carbon\Carbon;

/**
 * Common Game/Tournament Queries
 * 
 * In this file given all the Game/Tournament
 * related Database Queries
 * 
 * Traits CommonGameQueries
 * 
 * @category    Manage Tournament
 * @package     Tournament
 * @copyright   PokerBaazi
 * @author      Nitin Sharma<nitin.sharma@moonshinetechnology.com>
 * Created At:  03/02/2020
 */

trait CommonGameQueries
{

    protected $filterTournamentQueryObj;

    /**
     * getTournaments function is used for the get all Tournaments 
     * @param array|int $params[NOT_TOURNAMENT_TYPE_ID] to find where TOURNAMENT_TYPE_ID not in or not equal to
     * @param array|int $params[TOURNAMENT_TYPE_ID] to find where TOURNAMENT_TYPE_ID in or equal to 
     * @param { "greaterThenNow"|"lessThenNow" } $params[TOURNAMENT_START_TIME],
     *        TOURNAMENT_START_TIME set greaterThenNow to find tournament after now
     *        TOURNAMENT_START_TIME set lessThenNow to find tournament before now
     *        TOURNAMENT_START_TIME set date("Y-m-d H:i:s") to get tournaments after specific time
     * @param int [IS_ACTIVE=1]
     * @param array|string [$select=null]
     * @return Tournament[] array of Tournament class objects 
     */
    public function getTournaments($params, $select = null, $returnQueryObject = false)
    {

        $select = $select ?? ["TOURNAMENT_ID", "TOURNAMENT_NAME", "TOURNAMENT_START_TIME", DB::raw('BUYIN+ENTRY_FEE AS TOT_BUYIN')];
        $IS_ACTIVE =  $params['IS_ACTIVE'] ?? [1];
        $IS_ACTIVE = is_array($IS_ACTIVE) ? $IS_ACTIVE : [$IS_ACTIVE];

        $query = Tournament::query();
        $query->select($select);

        $query->whereIn("IS_ACTIVE", $IS_ACTIVE);

        if (array_key_exists('NOT_TOURNAMENT_TYPE_ID', $params)) {
            if (is_array($params['NOT_TOURNAMENT_TYPE_ID'])) {
                $query->whereNotIn('TOURNAMENT_TYPE_ID', $params['NOT_TOURNAMENT_TYPE_ID']);
            } else {
                $query->where('TOURNAMENT_TYPE_ID', "!=", $params['NOT_TOURNAMENT_TYPE_ID']);
            }
        }

        if (array_key_exists('TOURNAMENT_TYPE_ID', $params)) {
            if (is_array($params['TOURNAMENT_TYPE_ID'])) {
                $query->whereIn('TOURNAMENT_TYPE_ID', $params['TOURNAMENT_TYPE_ID']);
            } else {
                $query->where('TOURNAMENT_TYPE_ID', $params['TOURNAMENT_TYPE_ID']);
            }
        }

        if (array_key_exists('TOURNAMENT_START_TIME', $params)) {
            if ($params['TOURNAMENT_START_TIME'] == "greaterThenNow") {
                $query->where('TOURNAMENT_START_TIME', ">=", DB::raw('NOW()'));
            } elseif ($params['TOURNAMENT_START_TIME'] == "lessThenNow") {
                $query->where('TOURNAMENT_START_TIME', "<", DB::raw('NOW()'));
            } else {
                $query->where('TOURNAMENT_START_TIME', ">=", $params['TOURNAMENT_START_TIME']);
            }
        }

        if ($returnQueryObject) {
            return $query;
        }

        return $query->get();
    }

    /**
     * getTournamentInfo function is used for the get Tournaments Info
     * @param int $params[TOURNAMENT_ID]
     * @param array|string [$select=null]
     * @return (object)Tournament [Tournament class objects]
     */
    public function getTournamentInfo($params, $select = null, $returnQueryObject = false)
    {
        \extract($params);
        $query = Tournament::query();
        if ($select) $query->select($select);
        if ($returnQueryObject) {
            return $query;
        }
        return $TOURNAMENT_ID ? $query->findOrFail($TOURNAMENT_ID) : $query->where($params)->first();
    }

    /**
     * getTournamentBlindInfo function is used for the get all Tournament blind
     * @param array|int $params[BLIND_STRUCTURE_ID]
     * @param array|int $params[TOURNAMENT_LEVEL] 
     * @example $params[TOURNAMENT_LEVEL] = ["TOURNAMENT_LEVEL", "=", 11] or 
     *          ["TOURNAMENT_LEVEL", ">", 11] or 
     *          ["TOURNAMENT_LEVEL", ">=", 11] or
     *          default if TOURNAMENT_LEVEL int then 
     *          it will treats as == condition like ["TOURNAMENT_LEVEL", "=", 11]
     * @param array|string $select [$select=null]
     * @return TournamentLevelStructure[] array of TournamentLevelStructure class objects 
     */
    public function getTournamentBlindInfo($params = [], $select = null)
    {
        // $request = app(Request::class);
        \extract($params);
        $select = $select ?? ["SMALL_BLIND", "BIG_BLIND", "TOURNAMENT_LEVEL", "ANTE"];
        $query = TournamentLevelStructure::query();
        $query->select($select);

        /**
         * check if BLIND_STRUCTURE_ID is array,
         * then check wherein value else check where
         */
        is_array($BLIND_STRUCTURE_ID) ? $query->whereIn('BLIND_STRUCTURE_ID', $BLIND_STRUCTURE_ID) : $query->where('BLIND_STRUCTURE_ID', $BLIND_STRUCTURE_ID);

        if (!empty($TOURNAMENT_LEVEL)) {
            is_array($TOURNAMENT_LEVEL) ? $query->where([$TOURNAMENT_LEVEL]) : $query->where('TOURNAMENT_LEVEL', $TOURNAMENT_LEVEL);
        }

        return $query->get();
    }

    /**
     * getTournamentBlindLevelCount function is used for the get all 
     * @param int [$params]
     * @example { BLIND_STRUCTURE_ID }
     * 
     * @param array|string [$select=null]
     * @return int count of rows where conditions $params
     */
    public function getTournamentBlindLevelCount($params, $select = null)
    {
        // $request = app(Request::class);

        $select = DB::raw('count(*) as cnt');
        $result = $this->getTournamentBlindInfo($params, $select);
        return count($result) > 0 ? $result->first()->cnt : 0;
    }

    /**
     * @return TournamentLimit[] array of TournamentLimit class objects
     */
    public function getTournamentLimits()
    {
        return TournamentLimit::select("TOURNAMENT_LIMIT_ID", "TOURNAMENT_LIMIT_NAME", "DESCRIPTION")->get();
    }

    /**
     * @return { TournamentServer[] } array of TournamentServer class objects
     */
    public function getTournamentServers()
    {
        return TournamentServer::get();
    }

    /**
     * @param int [STATUS=1]
     * @param array|string [$select=null]
     * @return TournamentBlindStructure[] array of TournamentBlindStructure class objects
     */
    public function getBlindStructure($params = [], $select = null)
    {

        $select = $select ?? ["BLIND_STRUCTURE_ID", "STATUS", "DESCRIPTION"];
        $STATUS =  $params['STATUS'] ?? 1;

        $query = TournamentBlindStructure::query();
        $query->select($select);
        $query->where('STATUS', $STATUS);
        return $query->get();
    }

    /**
     * @param int [STATUS=1]
     * @param array|string [$select=null]
     * @return DefaultPrizeStructureTypes[] array of DefaultPrizeStructureTypes class objects
     */
    public function getMultiplePrizeStructures($params = [], $select = null)
    {

        $select = $select ?? ["PRIZE_STRUCTURE_TYPE_ID", "DESCRIPTION"];
        $STATUS =  $params['STATUS'] ?? 1;

        // default_prize_structure_types table name
        $query = DefaultPrizeStructureTypes::query();
        $query->select($select);
        $query->where('STATUS', $STATUS);
        return $query->get();
    }

    /**
     * @param int [TOURNAMENT_LEVEL]
     * @param int [$BLIND_STRUCTURE_ID]
     * @return int [SMALL_BLIND]
     */
    public function getSmallBlindByLevel($TOURNAMENT_LEVEL, $BLIND_STRUCTURE_ID)
    {

        $select = "SMALL_BLIND";
        $params = [
            'BLIND_STRUCTURE_ID' => $BLIND_STRUCTURE_ID,
            'TOURNAMENT_LEVEL' => $TOURNAMENT_LEVEL,
        ];

        return $this->getTournamentBlindInfo($params, $select)->first()->SMALL_BLIND;
    }

    /**
     * @param int [TOURNAMENT_LEVEL]
     * @param int [$BLIND_STRUCTURE_ID]
     * @return int [BIG_BLIND]
     */
    public function getBigBlindByLevel($TOURNAMENT_LEVEL, $BLIND_STRUCTURE_ID)
    {

        $select = "BIG_BLIND";
        $params = [
            'BLIND_STRUCTURE_ID' => $BLIND_STRUCTURE_ID,
            'TOURNAMENT_LEVEL' => $TOURNAMENT_LEVEL,
        ];

        return $this->getTournamentBlindInfo($params, $select)->first()->BIG_BLIND;
    }

    /**
     * @param int [TOURNAMENT_LEVEL]
     * @param int [$BLIND_STRUCTURE_ID]
     * @return TournamentLevelStructure[] array of TournamentLevelStructure object
     */
    public function getBlindStructuresAfterLevel($TOURNAMENT_LEVEL, $BLIND_STRUCTURE_ID)
    {
        $select = ["SMALL_BLIND", "BIG_BLIND", "ANTE", "TIME_BANK"];
        $params = [
            'BLIND_STRUCTURE_ID' => $BLIND_STRUCTURE_ID,
            'TOURNAMENT_LEVEL' => ["TOURNAMENT_LEVEL", ">=", $TOURNAMENT_LEVEL],
        ];
        return $this->getTournamentBlindInfo($params, $select);
    }

    /**
     * getParentTournamentTicketValue method is used for the get
     * specific tournament buyin + entry fee mean ticket value
     * 
     * @param int [$parentTournamentId]
     * @return int [BUYIN + ENTRY_FEE]
     */
    public function getParentTournamentTicketValue($parentTournamentId)
    {
        $select = DB::raw('BUYIN+ENTRY_FEE AS TOT_BUYIN');
        $params = [
            'TOURNAMENT_ID' => $parentTournamentId,
        ];
        return $this->getTournamentInfo($params, $select)->TOT_BUYIN ?? 0;
    }

    public function calPercentage($val1, $val2, $precision)
    {
        $res = round(($val1 / 100) * $val2, $precision);
        return $res;
    }

    /**
     * get All the game Types
     * 
     * @param array $params
     * @param array|string $select
     * @return MiniGameType[]
     */
    public function getTournamentGameTypes($params = [], $select = null)
    {
        $select = $select ?? ["MINIGAMES_ID", "MINIGAMES_TYPE_ID", "GAME_DESCRIPTION", "MINIGAMES_TYPE_NAME"];
        $params['STATUS'] = $params['STATUS'] ?? 1;

        $query = MiniGameType::query();
        $query->select($select);
        $query->where($params);

        return $query->get();
    }

    /**
     * get All the game Types
     * 
     * @param array $params
     * @param array|string $select
     * @return CoinType[]
     */
    public function getAllCurrencyTypes($params = [], $select = null)
    {
        $select = $select ?? ["NAME", "COIN_TYPE_ID"];
        $params['STATUS'] = $params['STATUS'] ?? 1;

        $query = CoinType::query();
        $query->select($select);
        $query->where($params);

        return $query->get();
    }


    /**
     * Filter Tournaments
     * 
     * This query can be use globally contains many of the condition,
     * but can be add more and select field can be choosen by yourself
     * because this function return QueryBuilder Object
     * 
     * @param array $params
     * @param array $optional [] 
     * 
     * @return QueryBuilder 
     */
    public function filterTournamentConditionalQuery($params, $optional = [])
    {
        $select = $optional['select'] ?? false;
        $tournamentRegistrationsCount = $optional['tournamentRegistrationsCount'] ?? false;
        $isDefaultTournamentTypeId = $optional['isDefaultTournamentTypeId'] ?? true;

        $this->filterTournamentQueryObj = Tournament::query();
        $this->filterTournamentQueryObj->from(app(Tournament::class)->getTable() . " AS t");

        if (!empty($select)) {

            $select = $select !== true ? $select : [
                "t.TOURNAMENT_ID",
                "t.TOURNAMENT_NAME",
                "t.TOURNAMENT_STATUS",
                "t.BUYIN",
                "t.ENTRY_FEE",
                "t.IS_ACTIVE",
                "m.MINIGAMES_TYPE_NAME",
                "t.TOURNAMENT_START_TIME",
                "t.TOURNAMENT_END_TIME",
            ];

            $this->filterTournamentQueryObj->select($select);
        }
        if ($tournamentRegistrationsCount) {
            $this->filterTournamentQueryObj->addSelect(DB::raw("(select count(*) from `tournament_registration` AS `tr` where `t`.`TOURNAMENT_ID` = `tr`.`TOURNAMENT_ID` ) as `tournamentRegistrationsCount`"));
        }

        $this->filterTournamentQueryObj->join('minigames_type AS m', 'm.MINIGAMES_TYPE_ID', '=', 't.MINI_GAME_TYPE_ID');
        $this->filterTournamentQueryObj->join('tournament_type AS tt', 'tt.TOURNAMENT_TYPE_ID', '=', 't.TOURNAMENT_TYPE_ID');
        $this->filterTournamentQueryObj->leftJoin('tournament_limit AS tl', 'tl.TOURNAMENT_LIMIT_ID', '=', 't.TOURNAMENT_LIMIT_ID');

        $this->filterTournamentQueryObj->when(!empty($params['TOURNAMENT_NAME']), function ($query) use ($params) {
            return $query->where('t.TOURNAMENT_NAME', 'LIKE', "%{$params['TOURNAMENT_NAME']}%");
        })
            ->when(!empty($params['MINI_GAME_TYPE_ID']), function ($query) use ($params) {
                return $query->where('t.MINI_GAME_TYPE_ID', $params['MINI_GAME_TYPE_ID']);
            })
            ->when(!empty($params['TOURNAMENT_STATUS']) || ($params['TOURNAMENT_STATUS'] ?? null) === "0", function ($query) use ($params) {
                $TOURNAMENT_STATUS = \explode(',', $params['TOURNAMENT_STATUS']);
                return $query->whereIn('t.TOURNAMENT_STATUS', $TOURNAMENT_STATUS);
            })
            ->when(!empty($params['BUYIN']), function ($query) use ($params) {
                return $query->where('t.BUYIN', $params['BUYIN']);
            })
            ->when(!empty($params['COIN_TYPE_ID']), function ($query) use ($params) {
                return $query->where('t.COIN_TYPE_ID', $params['COIN_TYPE_ID']);
            })
            ->when(!empty($params['IS_ACTIVE']) || ($params['IS_ACTIVE'] ?? null) === "0", function ($query) use ($params) {
                return $query->where('t.IS_ACTIVE', $params['IS_ACTIVE']);
            })
            ->when(!empty($params['date_from']), function ($query) use ($params) {
                $date_from = Carbon::parse($params['date_from']);
                return $query->where('t.TOURNAMENT_START_TIME', ">=", $date_from);
            })
            ->when(!empty($params['date_to']), function ($query) use ($params) {
                $date_to = Carbon::parse($params['date_to']);
                return $query->where('t.TOURNAMENT_START_TIME', "<=", $date_to);
            });

        if (!empty($params['TOURNAMENT_TYPE_ID']) && count(array_filter($params['TOURNAMENT_TYPE_ID'])) > 0) {
            $this->filterTournamentQueryObj->whereIn('t.TOURNAMENT_TYPE_ID', array_filter($params['TOURNAMENT_TYPE_ID']));
        } elseif ($isDefaultTournamentTypeId) {
            $this->filterTournamentQueryObj->whereIn('t.TOURNAMENT_TYPE_ID', [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12]);
        }
        return $this->filterTournamentQueryObj;
    }

    public function getTournamentWinnersData($TOURNAMENT_ID, $select = null, $returnQueryObject = false)
    {
        $select = $select ?? ["tw.RANK", "u.USER_ID", "u.USERNAME", "u.EMAIL_ID", "u.CONTACT", "tw.PRIZE_VALUE", "tw.WINNER_PERCENTAGE"];
        $tournamentWinnerTransactions = TournamentWinnerTransaction::query();
        $tournamentWinnerTransactions->from(app(TournamentWinnerTransaction::class)->getTable() . " As tw");

        $tournamentWinnerTransactions->select($select);
        $tournamentWinnerTransactions->leftJoin('user as u', "tw.USER_ID", '=', 'u.USER_ID');
        $tournamentWinnerTransactions->where("tw.TOURNAMENT_ID", $TOURNAMENT_ID);
        $tournamentWinnerTransactions->orderBy("tw.RANK", 'asc');

        if ($returnQueryObject) {
            return $tournamentWinnerTransactions;
        }
        return $tournamentWinnerTransactions->get();
    }

    public function getTournamentAllUsersRanks($TOURNAMENT_ID, $select = null, $returnQueryObject = false)
    {
        $select = $select ?? ["tr.RANK", "u.USER_ID", "u.USERNAME", "u.EMAIL_ID", "u.CONTACT", "tw.PRIZE_VALUE", "tw.WINNER_PERCENTAGE"];
        $tournamentAllUsersRanks = TournamentUsersRank::query();
        $tournamentAllUsersRanks->from(app(TournamentUsersRank::class)->getTable() . " As tr");

        $tournamentAllUsersRanks->select($select);
        $tournamentAllUsersRanks->leftJoin('tournament_winners_transaction as tw', function ($join) {
            return $join->on("tw.TOURNAMENT_ID", "=", "tr.TOURNAMENT_ID")
                ->on("tw.USER_ID", "=", "tr.USER_ID");
        });
        $tournamentAllUsersRanks->leftJoin("user as u", "tr.USER_ID", "=", "u.USER_ID");
        $tournamentAllUsersRanks->where("tr.TOURNAMENT_ID", $TOURNAMENT_ID);
        $tournamentAllUsersRanks->orderBy("tr.RANK", "asc");

        if ($returnQueryObject) {
            return $tournamentAllUsersRanks;
        }
        return $tournamentAllUsersRanks->get();
    }

    public function getTournamentRegisteredUsers($TOURNAMENT_ID, $select = null, $returnQueryObject = false)
    {
        $select = $select ?? ["t.USER_ID", "t.PLAYER_NAME", "u.EMAIL_ID", "u.CONTACT", "t.REGISTERED_DATE", "tut.TICKET_SOURCE", "tut.TICKET_REASON"];
        $tournamentRegisteredUsers = TournamentRegistration::query();
        $tournamentRegisteredUsers->from(app(TournamentRegistration::class)->getTable() . " As t");

        $tournamentRegisteredUsers->select($select);
        $tournamentRegisteredUsers->selectRaw("SUM( CASE WHEN mth.TRANSACTION_TYPE_ID = 101 THEN 1 ELSE 0 END ) AS total_rebuy_user, 
            SUM( CASE WHEN mth.TRANSACTION_TYPE_ID = 103 THEN 1 ELSE 0 END ) AS total_addon_user ");

        $tournamentRegisteredUsers->join('user as u', "u.USER_ID", '=', 't.USER_ID');
        $tournamentRegisteredUsers->join('tournament_user_ticket as tut', function ($join) use ($TOURNAMENT_ID) {
            return $join->on("tut.USER_ID", '=', 't.USER_ID')
                ->on('tut.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID');
        });
        $tournamentRegisteredUsers->leftJoin('master_transaction_history as mth', function ($join) use ($TOURNAMENT_ID) {
            return $join->on("mth.USER_ID", '=', 'tut.USER_ID')
                ->on("mth.INTERNAL_REFERENCE_NO", '=', 'tut.INTERNAL_REFERENCE_NO')
                ->whereIn('mth.TRANSACTION_TYPE_ID', [101, 103]);
        });

        $tournamentRegisteredUsers->where("t.TOURNAMENT_ID", $TOURNAMENT_ID);

        $tournamentRegisteredUsers->groupBy('u.USER_ID');

        if ($returnQueryObject) {
            return $tournamentRegisteredUsers;
        }
        return $tournamentRegisteredUsers->get();
    }

    public function getTournamentModelObject($tournament)
    {
        if ((!$tournament instanceof Tournament) && is_numeric($tournament)) {
            $tournament = $this->getTournamentInfo(["TOURNAMENT_ID" => $tournament]);
        }

        if ($tournament instanceof Tournament) {
            return $tournament;
        } else {
            return "Tournament Not found, Please check params";
        }
    }

    /**
     * @param mixed $tournament the tournament param instanceof Tournament Model
     *      or numberic id of tournament by which can select from database
     */
    public function getTournamentBlindStructureInfo($TOURNAMENT_ID, $select = null, $returnQueryObject = false)
    {
        $select = $select ?? ["tl.LEVEL_PERIOD", "tls.TOURNAMENT_LEVEL"];

        $query = Tournament::query();
        $query->from(app(Tournament::class)->getTable() . " AS t");
        $query->select($select);
        $query->join("tournament_level_structure as tls", function ($join) {
            return $join->on("tls.SMALL_BLIND", "=", "t.SMALL_BLIND")
                ->on("tls.BIG_BLIND", "=", "t.BIG_BLIND")
                ->on("tls.BLIND_STRUCTURE_ID", "=", "t.BLIND_STRUCTURE_ID");
        });
        $query->join("tournament_levels as tl", function ($join) {
            return $join->on("tl.SMALL_BLIND", "=", "t.SMALL_BLIND")
                ->on("tl.BIG_BLIND", "=", "t.BIG_BLIND")
                ->on("tl.TOURNAMENT_ID", "=", "t.TOURNAMENT_ID");
        });
        $query->where("t.TOURNAMENT_ID", $TOURNAMENT_ID);

        if ($returnQueryObject) {
            return $query;
        }
        return $query->first($TOURNAMENT_ID);
    }

    public function getTournamentWinnerLevel($TOURNAMENT_ID, $select = null, $returnQueryObject = false)
    {

        $select = $select ?? ["twl.RANK", "twl.PRIZE_VALUE", "twl.PRIZE_TYPE", "twl.WINNER_PERCENTAGE", "twl.PRIZE_TOURNAMENT_NAME"];

        $query = TournamentWinnerLevel::query();
        $query->from(app(TournamentWinnerLevel::class)->getTable() . " AS twl");
        $query->select($select);
        $query->where("twl.TOURNAMENT_ID", $TOURNAMENT_ID);
        $query->orderBy("twl.RANK", 'asc');
        if ($returnQueryObject) {
            return $query;
        }
        return $query->get();
    }
    public function getTournamentSubTabCategory($select = null, $returnQueryObject = false)
    {

        $select = $select ?? ["PK_TOURNAMENT_SUBTAB_CATEGORY_ID", "CATEGORY_DESC"];

        $query = TournamentSubTabCategory::query();
        $query->select($select);
        if ($returnQueryObject) {
            return $query;
        }
        return $query->get();
    }
}
