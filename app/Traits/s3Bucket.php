<?php

namespace App\Traits;

/**
 * 
 */
trait s3Bucket
{
    function s3FileUpload($file, $filePath, $allowedFormats, $hasFile)
    {
        $fileUploadUrl = "";
        $bucket = config('poker_config.s3Bucket.AWS_BUCKET');
        if ($hasFile) {

            $randNum = rand(100, 999);
            $randAlpha = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), -1);
            $name = date('mdYHis') . $randNum . $randAlpha . '.' . $file->getClientOriginalExtension();
            $filePath = $filePath . $name;
            $fileExtension =$file->getClientOriginalExtension();
            if (!in_array($fileExtension,$allowedFormats,true)) {
                #Invalid Format
               return (object)['status'=>400, 'message'=>'invalid file format'];
            } else {
                $s3 = \Storage::disk('s3')->getDriver();
                $s3->put($filePath, file_get_contents($file), array('ACL' => 'public-read'));
                $fileUploadUrl = 'https://' . $bucket . '.s3.amazonaws.com/' . $filePath;
                return (object)['status'=>200, 'message'=>'success', 'url'=>$fileUploadUrl];
            }
        }
    }
}
