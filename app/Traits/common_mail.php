<?php

namespace App\Traits;

use App\Mail\CommonMail;
use Illuminate\Support\Facades\Mail;

/**
 * 
 */
trait common_mail
{
    public function sendMail($params){
        Mail::to($params['email'])->send(new CommonMail($params));
    }
    
}
