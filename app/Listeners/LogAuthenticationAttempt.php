<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Attempting;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogAuthenticationAttempt
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Attempting  $event
     * @return void
     */
    public function handle(Attempting $event)
    {
        $credentials = $event->credentials;
        unset($credentials['password']);
        $event->credentials = $credentials;
        
        $activity = [
            'action' => "Login Attempt",
            'data' => json_encode($event)
        ];
        \PokerBaazi::storeActivity($activity);
    }
}
