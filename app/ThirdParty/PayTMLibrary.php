<?php
namespace App\ThirdParty;



class PayTMLibrary{
    public function __construct()
    {
        include "encdec_paytm.php";
    }

    public function getChecksumFromArray($paytmParams){
       return  $checksum = getChecksumFromArray($paytmParams, "1iifM_31svgZgYeC");
    }
}
