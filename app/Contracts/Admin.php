<?php

namespace App\Contracts;

interface Admin
{
    public function role();
    public function hasPermission($access,$module);
    public function hasEditPermission($module);
    public function hasViewPermission($module);
    public function getAllPermissions();
    public function checkSuperAdmin();
    public function menuPermission($access, $menu_id);
}
