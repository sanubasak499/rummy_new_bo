<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CommonMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->fromMail = config('poker_config.mail.from.support');
        $this->mailArray = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $dataArray = $this->mailArray;
        if (!empty($dataArray['subject'])) {
            $subject = $dataArray['subject'];
        } else {
            $subject = "PokerBaazi";
        }

        return $this->from($this->fromMail)->subject($subject)->view('bo.mail.commonMail', ['mailData' => $dataArray]);
    }
}
