<?php

namespace App\PokerBaazi\Contracts;

interface Authentication {
    public function hasCustomPermissionOrFail($access,$module);
    public function hasPermission($access, $module);
    public function hasEditPermission($module);
    public function hasViewPermission($module);
    public function getAllPermissions();
    public function menuPermission($access, $menu_id);
}