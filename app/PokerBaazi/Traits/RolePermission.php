<?php

namespace App\PokerBaazi\Traits;

use App\Models\Menu;

/**
 * 
 */
trait RolePermission
{
    public function getRolePermissions($role_id){
        return  Menu::with('module')
                ->with(['children' => function($childrenQuery) use ($role_id){
                    return $childrenQuery
                    ->whereHas('module.rolePermission', function($query) use ($role_id){
                        return $query->where('role_id', $role_id);
                    })
                    ->with(['module.rolePermission' => function($query) use ($role_id){
                        return $query->where('role_id', $role_id);
                    }]);
                }])
                ->whereHas('module.rolePermission', function($query) use ($role_id){
                    return $query->where('role_id', $role_id);
                })
                ->with(['module.rolePermission' => function($query) use ($role_id){
                    return $query->where('role_id', $role_id);
                }])
                ->whereNull('parent_id')
                ->get();
    }
}
