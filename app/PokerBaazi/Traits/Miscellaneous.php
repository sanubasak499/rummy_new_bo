<?php

namespace App\PokerBaazi\Traits;

use App\Models\SiteJincNewsletter;

trait Miscellaneous
{
    public function getMailTemplate($tempId)
    {
        $mailTemplate = SiteJincNewsletter::find($tempId);
        if ($mailTemplate) {
            return $mailTemplate;
        }
    }
}
