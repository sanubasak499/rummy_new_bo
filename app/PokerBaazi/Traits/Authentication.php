<?php

namespace App\PokerBaazi\Traits;

/**
 * 
 */
trait Authentication
{
    public function hasPermission($access, $module){
        return $this->user->hasPermission($access,$module);
    }

    public function hasViewPermission($module){
        return $this->user->hasViewPermission($module);
    }

    public function hasEditPermission($module){
        return $this->user->hasEditPermission($module);
    }

    public function hasCustomPermissionOrFail($access,$module){
        if(!$this->user->hasPermission($access,$module)){
            return abort(4003, "You're not authenticated user.");
        }
    }
    public function menuPermission($access, $menu_id){
        return $this->user->menuPermission($access, $menu_id);
    }
    public function checkSuperAdmin(){
        return in_array($this->user->id, $this->superUsers);
    }
    public function getAllPermissions(){
        return $this->user->getAllPermissions();
    }
}
