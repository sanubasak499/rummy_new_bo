<?php

namespace App\PokerBaazi;

use App\Pokerbaazi\Contracts\Authentication as PokerBaaziAuthentication;
use App\Models\Menu;
use Auth;
use App\Http\Controllers\Controller;
use App\PokerBaazi\Traits\Authentication as PokerBaaziAuthTraits;
use App\PokerBaazi\Traits\Miscellaneous;
use App\PokerBaazi\Traits\MethodHelpers;
use App\Models\AdminActivityLog;
use Illuminate\Support\Facades\Log;
use DB;
use Hash;

/**
 * This file used for the PokerBaazi Facades 
 * 
 * Here defined all the method which need to 
 * access the out the app, can use anywhere with the 
 * help of \PokerBaazi Facade
 * 
 * @example \PokerBaazi::storeActivityWithParams($action, $data, $module_id=null)
 * 
 * Class PokerBaazi
 * 
 * @package     PokerBaazi
 * @copyright   PokerBaazi
 * @author      Nitin Sharma<nitin.sharma@moonshinetechnology.com>
 */

class PokerBaazi extends Controller implements PokerBaaziAuthentication {

    use PokerBaaziAuthTraits, Miscellaneous, MethodHelpers;
    
    public function __construct(){
        $this->user = Auth::user();
        $this->superUsers = config('poker_config.admin.super_admins');
    }

    public function storeActivity($activity){
        try {
            DB::transaction(function () use ($activity) {
                $activity = new AdminActivityLog($activity);
                $activity->save();
            }, 5);
        } catch (\Exception $e) {
            Log::error("Admin Activity Log: ".json_encode($activity));
        }
    }
    public function storeActivityWithParams($action, $data, $module_id=null){
        $activity = [
            'admin_id'=>\Auth::user()->id,
            'module_id'=>$module_id,
            'action' => $action,
            'data' =>  $data
        ];
        $this->storeActivity($activity);
    }
    public function model($name)
    {
        return app($name);
    }

    public function menu(){
        return Menu::with('children')->whereNull('parent_id')->where('status', 1)->orderBy('menu_order')->get();
    }

    public function completeMenuNameId(){
        return Menu::select('id', 'display_name')->get();
    } 
    public function checkTransactionPassword($transPwd) {
        return Hash::check($transPwd,Auth::user()->transcation_password) ? true : false;
    }
}