<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CollectionExport implements FromCollection, WithHeadings
{
    private $data;
    private $headings;
    use Exportable;

    public function __construct($data,$headings)
    {
        $this->data = $data;
        $this->headings  = $headings;
    }

    public function collection()
    {
        return collect([$this->data]);
    }

    public function headings(): array
    {
        return $this->headings;
    }

}