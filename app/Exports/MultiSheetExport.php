<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class MultiSheetExport implements WithMultipleSheets
{
    
    private $collection;
    private $arrays;
    use Exportable;

    public function __construct($arrays)
    {
        $this->arrays  = $arrays;
    }
    public function collection()
    {
        $this->collection;
    }

    public function sheets(): array
    {
		$sheets = [];
		for($i = 0; $i < count($this->arrays); $i++) {
			$sheets[] = new MultiCollectionExport($this->arrays[$i]);
		}
        return $sheets;
    }
}
