<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

class MultiCollectionExport implements FromArray, WithHeadings
{
    protected $arrays;
	use Exportable;
	
    public function __construct(array $arrays)
    {
        $this->arrays = $arrays;
    }
	public function array():array
    {
       return $this->arrays;
	}
	public function headings(): array
    {
		if($this->arrays !=[]){
			return array_keys($this->arrays[0]);
		}else{
			return [];
		}
	}
    
}