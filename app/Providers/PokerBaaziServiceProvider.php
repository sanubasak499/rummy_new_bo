<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Collection;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

use Auth;

use App\PokerBaazi\PokerBaazi;
use App\Facades\PokerBaazi as PokerBaaziFacade;

use App\Models\Role;
use App\Models\Menu;
use App\Policies\BasePolicy;
use Illuminate\Support\Str;

class PokerBaaziServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // Setting::class  => SettingPolicy::class,
    ];

    protected $gates = [
        'browse_admin'
    ];

    public function register()
    {
        $loader = AliasLoader::getInstance();
        $loader->alias('PokerBaazi', PokerBaaziFacade::class);

        $this->app->singleton('pokerbaazi', function () {
            return new PokerBaazi();
        });

        $this->app->singleton('PokerBaaziAuth', function () {
            return auth();
        });

        $this->loadHelpers();

        $this->customMacros();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Router $router, Dispatcher $event)
    {
        // $this->loadAuth();
    }

    public function customMacros(){
        Collection::macro('attributeTitleCase', function () {
            return $this->map(function ($value) {
                return str_replace("_"," ",Str::title($value));
            });
        });
    }

    /**
     * Load helpers.
     */
    protected function loadHelpers()
    {
        foreach (glob(app_path("/Helpers/*.php")) as $filename) {
            require_once $filename;
        }
    }

    public function loadAuth()
    {
        // DataType Policies

        // This try catch is necessary for the Package Auto-discovery
        // otherwise it will throw an error because no database
        // connection has been made yet.
        try {
            if (Schema::hasTable(app(Menu::class)->getTable())) {
                $dataType = app(Menu::class);
                $dataTypes = $dataType->select('name','policy', 'model')
                            ->whereNotNull('model')
                            ->where('model', '!=', "")
                            ->get();
                

                foreach ($dataTypes as $dataType) {
                    $policyClass = BasePolicy::class;
                    if (isset($dataType->policy) && $dataType->policy !== ''
                        && class_exists($dataType->policy)) {
                        $policyClass = $dataType->policy;
                    }else{
                        if(trim($dataType->model) != "" && class_exists($dataType->model)){
                            $this->policies[$dataType->model] = $policyClass;
                        }
                    }
                }
                $this->registerPolicies();
                // dd($this->policies());
                
                $this->gates = $dataType->select('name','policy', 'model')
                                ->whereNull('model')
                                ->where('model', '')
                                ->get();
                foreach ($this->gates as $gate) {
                    $gate = 'browse_'.$gate->name;
                    Gate::define($gate, function ($user) use ($gate) {
                        if($user->id == config('poker_config.admin.super_admin_id')){
                            return true;
                        }
                        
                        return $user->hasPermission($gate);
                    });
                }
            }
        } catch (\PDOException $e) {
            Log::error('No Database connection yet in VoyagerServiceProvider loadAuth()');
        }

    }
}
