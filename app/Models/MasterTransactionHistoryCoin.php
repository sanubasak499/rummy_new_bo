<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterTransactionHistoryCoin extends Model
{
    protected $table = 'master_transaction_history_coins';
	protected $primaryKey = 'MASTER_TRANSACTTION_ID';
}
