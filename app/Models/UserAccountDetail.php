<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAccountDetail extends Model
{

    protected $table = "user_account_details";
    protected $primaryKey = "USER_ACCOUNT_ID";

    protected $fillable = ['USER_ACCOUNT_ID', 'USER_ID', 'ACCOUNT_HOLDER_NAME', 'ACCOUNT_TYPE', 'ACCOUNT_NUMBER', 'MICR_CODE', 'IFSC_CODE', 'BANK_NAME', 'BRANCH_NAME', 'APPROVED_BY', 'ATTEMPT_COUNT', 'UPDATED_DATE'];

    public $timestamps = false;
    protected $guarded = [];
}
