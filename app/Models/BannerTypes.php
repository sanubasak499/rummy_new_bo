<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class BannerTypes extends Model
{
    protected $table="banner_types";
    protected $primaryKey ="BANNER_TYPE_ID";

    public $timestamps = false;

    public function BannerTypeResult(){  
		$BannerTypeResult = self::select('BANNER_TYPE_ID','BANNER_TYPE_DESC')
                                ->where('STATUS',1)
                                ->get(); 
                                return $BannerTypeResult;
	}

}
