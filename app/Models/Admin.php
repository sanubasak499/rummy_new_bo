<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Contracts\Admin as AdminContracts;
use App\Traits\Admin as AdminTraits;

class Admin extends Authenticatable implements AdminContracts
{
    use Notifiable, AdminTraits;

    protected $table="admin_user";

    protected $guard = 'admin';

    protected $appends = ['IsSuperAdmin', 'FULLNAME'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'username', 'email', 'password', 'transcation_password', 'mobile', 'role_id', 'fk_partner_id', 'account_status', 'created_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','transcation_password'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function partner(){
        return $this->belongsTo(Partner::class, 'fk_partner_id');
    }
    
    // for Super Admin
    public function getIsSuperAdminAttribute(){
        return $this->id == config('poker_config.admin.super_admin_id');
    }
    // for Super Admin
    public function getFULLNAMEAttribute(){
        return $this->firstname .' '.$this->lastname;
    }

}
