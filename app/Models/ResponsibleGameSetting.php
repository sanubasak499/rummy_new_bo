<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResponsibleGameSetting extends Model
{
	protected $table = 'responsible_game_settings';
    protected $primaryKey = 'RESPONSIBLE_GAME_SETTINGS_ID';
    public $timestamps = false;
    
    protected $fillable = ['USER_ID','CASH_FROM_DATE','CASH_TO_DATE','CASH_ACTIVE','CASH_MAX_BIG_BLIND','CASH_UPDATED_DATE','CASH_UPDATED_BY','OFC_FROM_DATE','OFC_TO_DATE','OFC_ACTIVE','OFC_MAX_POINT','OFC_UPDATED_DATE','OFC_UPDATED_BY','BREAK_FROM_DATE','BREAK_TO_DATE','BREAK_ACTIVE','BREAK_UPDATED_DATE','BREAK_UPDATED_BY','CREATED_DATE'];
}
