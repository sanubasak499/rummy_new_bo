<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdjustmentTransaction extends Model
{
    protected $table = 'adjustment_transaction';
    protected $primaryKey = 'ADJUSTMENT_TRANSACTION_ID';

    const CREATED_AT = 'ADJUSTMENT_CREATED_ON';
    
    const UPDATED_AT = 'UPDATED_DATE';

    protected $fillable = [
        'USER_ID','TRANSACTION_TYPE_ID','INTERNAL_REFERENCE_NO','ADJUSTMENT_CREATED_BY','ADJUSTMENT_CREATED_ON','ADJUSTMENT_AMOUNT','ADJUSTMENT_ACTION','ADJUSTMENT_COMMENT','COIN_TYPE_ID','REASON_ID'
    ];

}
