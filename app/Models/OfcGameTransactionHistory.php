<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfcGameTransactionHistory extends Model
{
    protected $table = "ofc_game_transaction_history";

    protected $primaryKey = 'GAME_TRANSACTION_ID';
}
