<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentWinnerTransaction extends Model
{
    protected $table = "tournament_winners_transaction";
}