<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RewardUserType extends Model
{
    protected $table = "reward_user_type";
	protected $primaryKey = "S_NO";
	const CREATED_AT = 'CREATED_DATE';
	const UPDATED_AT = 'UPDATED_DATE';
}
