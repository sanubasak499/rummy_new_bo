<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentUsersRank extends Model
{
    protected $table = "tournament_users_rank";
}