<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterTransactionHistoryRewardpoints extends Model
{
    protected $table = 'master_transaction_history_rewardpoints';
    protected $primaryKey = 'MASTER_TRANSACTTION_ID';
	public $timestamps = false;
}
