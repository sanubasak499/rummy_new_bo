<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignTournamentMapping extends Model
{
    protected $table='campaign_tournament_mapping';

    protected $primaryKey = 'CAMPAIGN_TOURNAMENT_ID';
	
	const CREATED_AT = 'CREATED_DATE';
    const UPDATED_AT = 'UPDATED_DATE';
	
}