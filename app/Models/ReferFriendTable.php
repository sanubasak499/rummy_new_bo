<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferFriendTable extends Model
{
    protected $table ="refer_friend_table";

    protected $primaryKey = 'RAF_REPORT_ID';

   const CREATED_AT = 'CREATED_DATE';
   const UPDATED_AT = 'UPDATED_DATE';
}
