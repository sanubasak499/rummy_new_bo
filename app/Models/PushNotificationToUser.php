<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class PushNotificationToUser extends Model
{
    protected $table = 'push_notifications_to_users';
    protected $fillable = [
    						'PUSH_NOTIFICATION_ID',
    						'USER_ID'
    					  ];
 	protected $primaryKey = 'NOTIFICATION_TO_USER_ID';

 	public $timestamps = false;
}
