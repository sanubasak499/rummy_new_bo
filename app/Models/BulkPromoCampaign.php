<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BulkPromoCampaign extends Model
{
    protected $table='bulk_promo_campaign';

    protected $primaryKey = 'BULK_PROMO_CAMPAIGN_ID';
	
	const CREATED_AT = 'CREATED_dATE';
    const UPDATED_AT = 'UPDATED_DATE';
	
}