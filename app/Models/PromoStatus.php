<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoStatus extends Model
{
    protected $table='promo_status';

    protected $primaryKey = 'PROMO_STATUS_ID';
	
	public function promo_campaign()
    {
        return $this->hasMany('App\PromoCampaign');
    }
	
}