<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminActivityLog extends Model
{
    protected $table = "bo_admin_activity_log";

    protected $fillable= ['admin_id','module_id','action','data','created_at','updated_at'];
}