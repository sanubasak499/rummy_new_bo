<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserKycDetails extends Model
{
    protected $table = 'USER_KYC_DETAILS';

    protected $primaryKey = 'USER_KYC_ID';
	
	public $timestamps=false;
	
	const CREATED_AT = 'CREATED_DATE';
    const UPDATED_AT = 'UPDATE_DATE';
}
