<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ActionReason extends Model
{
    protected $table = 'action_reasons';
    protected $primaryKey = 'ACTIONS_REASONS_ID';
    protected $fillable = ['reason_category_id','actions_reason','actions_reason_description','status','created_by','created_date','updated_by','update_date'];
    
    public $timestamps = false;

    public function categories(){
        $categoryIds=explode(',',$this->REASON_CATEGORY_ID);
        $this->reasonCategory = ReasonCategory::select('REASON_CATEGORY_NAME AS CATEGORY')->find($categoryIds);
        return $this->reasonCategory;
    }

    public function scopeNotDeleted($query){
    	return $query->where('STATUS','<>','2');
    }

    public function scopeActive($query){
        return $query->where('STATUS','=','1');
    }
}

