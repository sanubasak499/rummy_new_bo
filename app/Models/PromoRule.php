<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoRule extends Model
{
    protected $table='promo_rule';

    protected $primaryKey = 'PROMO_RULE_ID';

	const UPDATED_AT = 'UPDATED_DATE';
	public $timestamps=false;
}