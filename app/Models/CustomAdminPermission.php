<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomAdminPermission extends Model
{
    protected $table = "bo_admin_customized_admin_user_permissions";
    
    public function permission_details(){
        return $this->belongsTo(Permission::class, 'permission_id');
    }
}
