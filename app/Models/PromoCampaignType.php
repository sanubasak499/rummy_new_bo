<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoCampaignType extends Model
{
    protected $table='promo_campaign_type';

    protected $primaryKey = 'PROMO_CAMPAIGN_TYPE_ID';

	const UPDATED_AT = 'UPDATED_DATE';
	
	public function promo_campaign()
    {
        return $this->hasMany('App\PromoCampaignType');
    }
}