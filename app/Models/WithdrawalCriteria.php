<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WithdrawalCriteria extends Model
{
    protected $table='withdrawal_criteria';

    protected $primaryKey = 'WITHDRAWAL_CRITERIA_ID';

    const UPDATED_AT = 'UPDATED_DATE';
    const CREATED_AT = 'CREATED_DATE';
    
}