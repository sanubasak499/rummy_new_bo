<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tournament extends Model
{
	protected $table = "tournament";
	protected $primaryKey = "TOURNAMENT_ID";

	public $timestamps = false;

	protected $fillable = ["FK_PARENT_TOURNAMENT_ID", "TOURNAMENT_TYPE_ID", "MINIGAMES_ID", "MINI_GAME_TYPE_ID", "TOURNAMENT_LIMIT_ID", "COIN_TYPE_ID", "LOBBY_ID", "LOBBY_MENU_ID", "TOURNAMENT_NAME", "TOURNAMENT_DESC", "T_MIN_PLAYERS", "T_MAX_PLAYERS", "MIN_PLAYERS", "MAX_PLAYERS", "PLAYERS", "PLAYER_PER_TABLE", "TABLES", "MINBUYIN", "MAXBUYIN", "SMALL_BLIND", "BIG_BLIND", "BUYIN", "RAKE", "RAKE1", "RAKE_CAP", "RAKE_CAP1", "PLAYER_HAND_TIME", "EXTRA_TIME", "TIMER_BANK_TIME", "INITIAL_TIME_BANK", "TIME_BANK_INTERVAL_LEVEL", "TIME_BANK_INTERVAL_TIME", "FINAL_TABLE_TIME_BANK", "SITOUT_TIME", "ANTI_BANKING_TIME", "HIGH_LOW_LEVEL", "IS_GIVE_CHIPS", "STRADDLE", "TOURNAMENT_CHIPS", "ELIGIBILITY_ID", "ELIGIBILITY", "ENTRY_FEE", "TOURNAMENT_COMMISION", "IS_REGISTER_GAME", "REGISTER_START_TIME", "REGISTER_END_TIME", "TOURNAMENT_START_TIME", "TOURNAMENT_END_TIME", "TOURNAMENT_BREAK_TIME", "BREAK_INTERVAL_TIME", "DISCONNECT_TIME", "PAID_OPTION", "IS_REBUY", "REBUY_IN", "REBUY_ENTRY_FEE", "REBUY_COUNT", "REBUY_CHIPS", "REBUY_END_TIME", "IS_ADDON", "ADDON_AMOUNT", "ADDON_COUNT", "ADDON_CHIPS", "IS_WAITLIST", "IS_ACTIVE", "STAKE_LEVELS", "NO_OF_WINNERS", "REGISTRATED_PLAYER_COUNT", "TOURNAMENT_PLAYER_COUNT", "TOURNAMENT_STATUS", "CREATED_DATE", "CATEGORY", "LATE_REGISTRATION_ALLOW", "LATE_REGISTRATION_END_TIME", "LATE_REGISTRATION_FINISH_LEVEL", "ALLOW_OFFLINE_PLAYERS", "UNREGISTRATION_FEE", "CANCEL_REASON", "PRIZE_TYPE", "PRIZE_BALANCE_TYPE", "PRIZE_COIN_TYPE_ID", "TICKET_VALUE", "PRIZE_STRUCTURE_ID", "GUARENTIED_PRIZE", "LAYALTY_ELIGIBILITY_START_DATE", "LAYALTY_ELIGIBILITY_END_DATE", "UPDATED_DATE", "PARTNER_ID", "MASTER_ID", "REBUY_ELIGIBLE_CHIPS", "ADDON_ENTRY_FEE", "ADDON_BREAK_TIME", "TOTAL_ADDON_COUNT", "TOTAL_REBUY_COUNT", "STUCK_STATUS", "PPL_STATUS_ID", "IS_BLOCKED_TABLE", "BLOCKED_TABLE_KEY", "SERVER_ID", "HIGH_ROLLER_STATUS_ID", "DEPOSIT_BALANCE_ALLOW", "PROMO_BALANCE_ALLOW", "WIN_BALANCE_ALLOW", "ANONYMOUS_TABLE", "DOUBLE_REBUYIN", "ADDITIONAL_EXTRATIME", "ADDITIONAL_EXTRATIME_LEVEL_INTERVAL", "PLAYER_MAX_EXTRATIME", "RUN_IT_TWICE", "PASSWORD", "BLIND_STRUCTURE_ID", "PROMO_PLAYERS_COUNT", "EXCLUDE_PROMO_PLAYERS", "PRIZE_STRUCTURE_TYPE_ID", "FIXED_PRIZE", "STUCK_RESUME", "STUCK_TOURNAMENT_ID", "GRINDERS_SERIES_STATUS_ID", "TIMER_TOURNAMENT", "TIMER_TOURNAMENT_END_TIME", "RE_ENTRY_PLAYER_COUNT", "FINAL_PRIZE_AMOUNT", "LOBBY_DISPLAY_INTERVAL", "MULTIDAY_PLAYER_PERCENTAGE", "BOUNTY_AMOUNT", "BOUNTY_ENTRY_FEE", "PROGRESSIVE_BOUNTY_PERCENTAGE", "WIN_THE_BUTTON", "FANTASY_TYPE", "FANTASY_ROUND", "FANTASY_GROUP", "POINT_VALUE", "THRESHOLD_AMOUNT", "FINAL_TABLE_STOP", "UNREGISTRATION_ENDTIME", "PBBS_STATUS_ID", "PRIVATE_TABLE", "PMVS_STATUS_ID", "FEATURED", "IMPS_STATUS_ID"];

	/*
	|--------------------------------------------------------------------------
	| Author: Basanta
	| Purpose: Get all the tournament details from tournament table
	| Callers: 
	| Created Date: 
	| Modified Date: 
	| Modified By: 
	| Modified Details: 
	|--------------------------------------------------------------------------
	*/
	public function tournament()
	{
		$tournament = self::select(
			'TOURNAMENT_ID',
			'TOURNAMENT_NAME',
			'TOURNAMENT_START_TIME',
			DB::raw('DATE(TOURNAMENT_START_TIME)')
		)
			->where('TOURNAMENT_STATUS', 1)
			->where('IS_ACTIVE', 1)
			->where('TOURNAMENT_START_TIME', ">=", DB::raw('NOW()'))
			->whereIn('TOURNAMENT_TYPE_ID', array(1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12))
			->orderBy('TOURNAMENT_START_TIME', 'ASC')
			// ->toSql();
			->get();
		return $tournament;
	}

	public function chkUserAlreadyRegister($userId, $tournament_id)
	{
		$countResult = DB::table('tournament_user_ticket')->where("tournament_id", $tournament_id)->where("user_id", $userId)->count();
		return $countResult;
	}

	public function tournamentNames()
	{
		$tournamentNames = Tournament::select(DB::raw("IFNULL(TOURNAMENT_ID,0)"), "TOURNAMENT_NAME")
			->where('TOURNAMENT_STATUS', "1")
			->whereIn('TOURNAMENT_TYPE_ID', ['2', '3', '4', '12'])
			->whereIn('LOBBY_ID', ['2', '3', '5', '6'])
			->where('IS_ACTIVE', "1")
			->whereNull('TOURNAMENT_END_TIME')
			->orderBy("TOURNAMENT_START_TIME", "ASC")
			->get();
		return $tournamentNames;
	}

	public function tournamentDetailsWithServerDetails($TOURNAMENT_ID)
	{
		$TournamentResult = self::from(app(self::class)->getTable() . " as t")
			->select('t.TOURNAMENT_ID', 't.TOURNAMENT_NAME', 't.TOURNAMENT_START_TIME', 't.SERVER_ID', 't.TOURNAMENT_TYPE_ID', 'tr.SERVER_URL')
			->join('tournament_servers as tr', 'tr.SERVER_ID', '=', 't.SERVER_ID')
			->where('t.TOURNAMENT_ID', $TOURNAMENT_ID)
			->get();
		return $TournamentResult;
	}

	/**
	 * Author           : Brijesh Kumar
	 * Purpose          : To Get A list of Tornamenter for Live Entries DD(Drop-Down) 
	 * Code Refrence by : Mr.Nitesh - this function is replica of public function tournamentDetailsIsActiveOne
	 * Descrption       : This function return a list of tournament.
	 * Date             : 27-Dec-2019 15:52         * 
	 */
	public static function liveTournamentList()
	{
		return self::select('TOURNAMENT_ID', 'TOURNAMENT_NAME', 'TOURNAMENT_START_TIME', 'SERVER_ID', 'TOURNAMENT_TYPE_ID', DB::raw('BUYIN+ENTRY_FEE as TOURNAMENT_AMOUNT'))
			->where('IS_ACTIVE', 1)
			->where('TOURNAMENT_TYPE_ID', 12)
			->orderBy('TOURNAMENT_START_TIME', 'DESC')
			->limit(50)
			->get();
	}

	/**
	 * Author           : Brijesh Kumar
	 * Purpose          : To get a Tornamente 'BUYIN+ENTRY_FEE as TOURNAMENT_AMOUNT' 
	 * Date             : 27-Dec-2019 15:52         * 
	 */
	public static function getTournamentByeinPlusEntryFee($tournamentID)
	{
		return self::select(DB::raw('BUYIN+ENTRY_FEE as TOURNAMENT_AMOUNT'))->where('IS_ACTIVE', 1)->find($tournamentID);
	}

	public function tournamentDetailsIsActiveOne()
	{
		$TournamentResult =	self::select('TOURNAMENT_ID', 'TOURNAMENT_NAME', 'TOURNAMENT_START_TIME', 'SERVER_ID', 'TOURNAMENT_TYPE_ID')
			->where('IS_ACTIVE', 1)
			->where('TOURNAMENT_START_TIME', ">", DB::raw('NOW()'))
			->get();
		return $TournamentResult;
	}

	public function getAllUpcomingTournaments($date = null)
	{
		$date = $date ?? \DB::raw('DATE_SUB(NOW(), INTERVAL 1 HOUR)');
		$types = array('1', '2', '3', '4', '5', '7', '8', '10', '12');
		$status = array('1', '3');
		$lobby_id = array('2', '3', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16');
		$getallupcomingTournaments = Tournament::whereIn('TOURNAMENT_STATUS', $status)
			->whereIn('TOURNAMENT_TYPE_ID', $types)
			->whereIn('LOBBY_ID', $lobby_id)
			->where('IS_ACTIVE', '1')
			->where('TOURNAMENT_START_TIME', ">", $date)
			->orderBy('TOURNAMENT_START_TIME', 'ASC')
			->get();

		return $getallupcomingTournaments;
	}

	public function scopeTournamentDetailsById($query, $tournament_id)
	{
		return $query->where('TOURNAMENT_ID', '=', $tournament_id);
	}

	public function getTournamentStatusName($TOURNAMENT_STATUS = null)
	{
		$TOURNAMENT_STATUS = $TOURNAMENT_STATUS ?? $this->TOURNAMENT_STATUS;
		$statusList = ['Announced', 'Registering', 'Seating', 'Started', 'Breaking', 'Finished', 'Cancelled', 'Announced'];
		$statusString = '';

		if (!empty($TOURNAMENT_STATUS) || ($TOURNAMENT_STATUS ?? null) === 0) {
			if (array_key_exists($TOURNAMENT_STATUS, $statusList)) {
				$statusString = $statusList[$TOURNAMENT_STATUS];
			} else {
				$statusString = 'Announced';
			}
		}

		return $statusString;
	}
	
	public function getTournamentDisplayTabName($CATEGORY = null)
	{
		$CATEGORY = $CATEGORY ?? $this->CATEGORY;
		$dataList = ['1' => 'Normal Tab', '2' => 'Timer Tab', '3' => 'Specials Tab', '4' => 'BPT Tab', '9' => 'Freeroll'];

		$statusString = '';
		if (!empty($CATEGORY)) {
			if (array_key_exists($CATEGORY, $dataList)) {
				$statusString = $dataList[$CATEGORY];
			} else {
				$statusString = '';
			}
		}

		return $statusString;
	}
}
