<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrackingStatus extends Model
{
    protected $table = "tracking_status";
    protected $primaryKey = "TRACKING_STATUS_ID";
    public $timestamps = false;
}
