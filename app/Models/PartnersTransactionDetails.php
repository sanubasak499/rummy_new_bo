<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnersTransactionDetails extends Model
{
    protected $table = 'partners_transaction_details';
	protected $primaryKey = 'PARTNER_TRANSACTION_ID';
	public $timestamps = false;
}
