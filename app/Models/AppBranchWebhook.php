<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppBranchWebhook extends Model
{
    //
    protected $table="app_branch_event_tracking";

    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
   
}
