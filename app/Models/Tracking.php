<?php
/*
    |--------------------------------------------------------------------------
    | Class Name: Tracking
	| Author: Dharminder Singh
	| Created Date: Dec 20 2019
	| Modified Date: 
	| Modified By: 
	| Modified Details: 
	|--------------------------------------------------------------------------
*/
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    protected $table="tracking";
    protected $primaryKey = 'TRACKING_ID';
	public $timestamps = false;
}
