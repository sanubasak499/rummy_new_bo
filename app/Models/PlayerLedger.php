<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerLedger extends Model
{
	protected $table = "player_ledger";
	protected $primaryKey = "PLAYER_LEDGER_ID";
	public $timestamps = false;
	
}
