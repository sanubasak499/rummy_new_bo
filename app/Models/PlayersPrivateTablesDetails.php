<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayersPrivateTablesDetails extends Model
{
    protected $table = 'players_private_tables_details';

    protected $primaryKey = 'PK_PRIVATE_TABLES_DETAILS_ID';
	public $timestamps = false;
}
