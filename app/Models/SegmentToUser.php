<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SegmentToUser extends Model
{
    protected $table = 'segment_to_user';
    protected $primaryKey ="SEGMENT_USER_ID";

    public $timestamps = false;

    public function scopeUsersIdFromSegmentId($query, $segment_id){
    	$query->distinct('USER_ID')->whereIn('SEGMENT_ID',$segment_id);
    } 
}
