<?php

/*
    |--------------------------------------------------------------------------
    | Class Name: PokerPlay
	| Author: Dharminder Singh
	| Created Date: Dec 02 2020
	| Modified Date: 
	| Modified By: 
	| Modified Details: 
	|--------------------------------------------------------------------------
*/


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PokerPlay extends Model
{
    protected $table="poker_play";
    protected $primaryKey = 'PLAY_ID';
}
