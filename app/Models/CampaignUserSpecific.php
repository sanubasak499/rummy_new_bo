<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class CampaignUserSpecific extends Model
{
    protected $table='campaign_user_specific';

    protected $primaryKey = 'CAMPAIGN_USER_SPECIFIC_ID';
	
	const CREATED_AT = 'CREATED_ON';
	public $timestamps=false;
	
	public function chkUserAlreadyExist($userId, $promoCampaignId)
	{
		$countResult = DB::table('campaign_user_specific')->where("PROMO_CAMPAIGN_ID", $promoCampaignId)->where("USER_ID", $userId)->count();
		return $countResult;
	}
}