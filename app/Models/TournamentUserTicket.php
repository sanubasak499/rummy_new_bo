<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentUserTicket extends Model
{
    protected $table='tournament_user_ticket';

    protected $primaryKey = 'TOURNAMENT_USER_TICKET_ID';

	const UPDATED_AT = 'UPDATED';
	
	
}