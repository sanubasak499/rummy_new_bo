<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterTransactionHistory extends Model
{
    protected $table = 'master_transaction_history';
	protected $primaryKey = 'MASTER_TRANSACTTION_ID';
	public $timestamps = false;

	protected $fillable = [
        'USER_ID','BALANCE_TYPE_ID','TRANSACTION_STATUS_ID','TRANSACTION_TYPE_ID','TRANSACTION_AMOUNT','TRANSACTION_DATE','INTERNAL_REFERENCE_NO','CURRENT_TOT_BALANCE','CLOSING_TOT_BALANCE','PARTNER_ID'
    ];
	
	public function payment_transaction(){
		return $this->hasOne(PaymentTransaction::class, 'INTERNAL_REFERENCE_NO');
	}
}
