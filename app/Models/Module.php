<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RolePermission;

class Module extends Model
{
    protected $table="bo_admin_modules";

    protected $fillable = ['menu_id','module_key','display_name','description'];
    
    public function menu(){
        return $this->belongsTo(Menu::class, 'menu_id');
    }
    public function rolePermission(){
        return $this->hasMany(RolePermission::class, 'module_id');
    }
}
