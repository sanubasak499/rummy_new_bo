<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RewardCashbackLevel extends Model
{
    protected $table = 'reward_cashback_level';
    protected $primaryKey='LEVEL_ID';

    const UPDATED_AT = 'UPDATED_DATE';
}
