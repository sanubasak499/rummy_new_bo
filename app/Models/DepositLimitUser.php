<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepositLimitUser extends Model
{
    protected $table = 'deposit_limit_user';
    protected $primaryKey = 'DEPOSIT_LIMIT_USER_ID';
	const CREATED_AT = 'CREATED_DATE';
    const UPDATED_AT = 'UPDATED_ON';

    protected $fillable = ['DEP_LEVEL' , 'DEP_AMOUNT' , 'DEP_AMOUNT_PER_DAY' , 'TRANSACTIONS_PER_DAY' , 'DEP_AMOUNT_PER_WEEK','DEP_AMOUNT_PER_MONTH', 'TRANSACTIONS_PER_MONTH' , 'UPDATED_STATUS','UPDATED_ON','UPDATED_BY' ];
    
}
