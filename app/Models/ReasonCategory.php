<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReasonCategory extends Model
{
    protected $table = 'reason_category';
    protected $primaryKey = 'REASON_CATEGORY_ID';
    protected $fillable = [ 'reason_category_name' , 'reason_category_description' , 'created_by' , 'updated_by' , 'created_date','STATUS','update_date' ];

    public $timestamps = false;

    public function scopeActive($query){
    	return $query->where('STATUS',1);
    }

    public function scopeInactive($query){
    	return $query->where('STATUS',0);
    }

}
