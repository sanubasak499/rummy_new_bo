<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class BannerCategories extends Model
{
    protected $table="banner_categories";
    protected $primaryKey ="BANNER_CATEGORY_ID";

    public $timestamps = false;
   
    public function BannerCategoriesResult(){  
		$BannerCategoriesResult = self::select('BANNER_CATEGORY_ID','BANNER_CATEGORY_DESC')
                                        ->where('STATUS',1)
                                        ->get(); 
                                      return $BannerCategoriesResult;
	}

}
