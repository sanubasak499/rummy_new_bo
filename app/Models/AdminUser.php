<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminUser extends Model
{
    protected $table='admin_user';

    protected $primaryKey = 'ADMIN_USER_ID';
	
	const UPDATED_AT = 'UPDATED_DATE';
   
}