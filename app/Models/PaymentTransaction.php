<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
    protected  $table = 'payment_transaction';
	protected $primaryKey = 'PAYMENT_TRANSACTION_ID';
	public $timestamps = false;


	public function getTransactionStatus(){
		return $this->belongsTo(TransactionStatus::class,'PAYMENT_TRANSACTION_STATUS');
	}
	public function getPaymentProviderName(){
		return $this->belongsTo(PaymentProvider::class,'PAYMENT_PROVIDER_ID');
	}
	public function getUserNameFromUserId(){
    	return $this->belongsTo(User::class, 'USER_ID');
    }
}