<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "bo_admin_roles";
    protected $fillable = ['name', 'description'];
    protected $appends = ['Description50Char'];

    public function users()
    {
        $userModel = app(Admin::class);
        
        return $this->belongsToMany($userModel, 'admin_roles')
                    ->select(app($userModel)->getTable().'.*')
                    ->union($this->hasMany($userModel))->getQuery();
    }

    public function permissions()
    {
        return $this->belongsToMany(Module::class, RolePermission::class);
    }

    public function rolePermission(){
        return $this->hasMany(RolePermission::class, 'role_id');
    }
    public function getDescription50CharAttribute(){
        $string = strip_tags($this->description);
        
        if (strlen($string) > 50) {
            // truncate string
            $stringCut = substr($string, 0, 50);
            $endPoint = strrpos($stringCut, ' ');
            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...';
        }
        return $string;
    }
}
