<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
    protected $table = 'transaction_type';

    protected $primaryKey = 'TRANSACTION_TYPE_ID';
	public $timestamps = false;
}
