<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserAccountLogs extends Model
{
    protected $table = 'user_account_logs';
    protected $primaryKey = 'ACCOUNT_LOG_ID';
  
    public $timestamps = false;

}

