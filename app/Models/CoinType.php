<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoinType extends Model
{
    protected $table = 'coin_type';

    protected $primaryKey = 'COIN_TYPE_ID';
	public $timestamps = false;
}
