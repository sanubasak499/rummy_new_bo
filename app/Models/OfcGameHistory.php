<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfcGameHistory extends Model
{
    protected $table = 'ofc_game_history';

    protected $primaryKey = 'GAME_HISTORY_ID';

    public function tournament_tables(){
        return $this->belongsTo(TournamentTable::class, 'TOURNAMENT_TABLE_ID');
    }
}
