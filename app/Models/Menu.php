<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Route;

class Menu extends Model
{
    protected $table="bo_admin_menus";

    protected $fillable = ['parent_id','display_name','description','target','icon_class','url','route','parameters','menu_order','absolute_url','status'];

    public function parent(){
        return $this->belongsTo(Menu::class, 'parent_id')->with('parent');
    }

    public function children(){
        return $this->hasMany(Menu::class, 'parent_id')->with('children')->orderBy('menu_order');
    }
    public function childrenWithModule(){
        return $this->hasMany(Menu::class, 'parent_id')->with('children')->whereHas('module');
    }
    public function childrenWithModuleWithPermission(){
        return $this->hasMany(Menu::class, 'parent_id')->with('childrenWithModuleWithPermission')
        ->whereHas('module.rolePermission');
    }

    public function link()
    {
        return $this->prepareLink($this->route, $this->parameters, $this->url, $this->absolute_url);
    }

    protected function prepareLink($route, $parameters, $url, $absolute_url)
    {
        if (is_null($parameters) || trim($parameters) == "") {
            $parameters = [];
        }

        if (is_string($parameters)) {
            $parameters = json_decode($parameters, true);
        } elseif (is_array($parameters)) {
            $parameters = $parameters;
        } elseif (is_object($parameters)) {
            $parameters = json_decode(json_encode($parameters), true);
        }

        if (!is_null($route) || trim($route) == "") {
            
            if($absolute_url==0){
                if (!Route::has($route)) {
                    return '#';
                }
                return route($route, $parameters);
            }else{
                return url($url);
            }
        }

        return $url ?? "#";
    }

    public function getParametersAttribute()
    {
        return json_decode($this->attributes['parameters']);
    }

    public function setParametersAttribute($value)
    {
        if (is_array($value)) {
            $value = json_encode($value);
        }

        $this->attributes['parameters'] = $value;
    }

    public function setUrlAttribute($value)
    {
        if (is_null($value)) {
            $value = '';
        }

        $this->attributes['url'] = $value;
    }

    public function hasPermission($access){
        return \PokerBaazi::menuPermission($access, $this->id);
    }

    public function module(){
        return $this->hasOne(Module::class, 'menu_id');
    }
    public function modules(){
        return $this->hasMany(Module::class, 'menu_id');
    }
}
