<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentLevel extends Model
{
    protected $table = "tournament_levels";

    protected $fillable=["TOURNAMENT_ID", "MINIGAMES_TYPE_ID", "STAKE_LEVELS", "SMALL_BLIND", "BIG_BLIND", "ANTE", "LEVEL_BREAK_TIME", "TIME_BANK", "TOURNAMENT_LEVELS_ID", "LEVEL_PERIOD", "UPDATED_DATE"];

    public $timestamps = false;
}