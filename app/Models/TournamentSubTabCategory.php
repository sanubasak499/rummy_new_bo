<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;

class TournamentSubTabCategory extends Model
{
    // use Notifiable;
    protected $table = 'tournament_sub_tab_categories';
    protected $primaryKey = 'PK_TOURNAMENT_SUBTAB_CATEGORY_ID';
    
    public $timestamp = false;
}
