<?php

namespace App\Models\helpdesk;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\TransactionType;
use App\Models\TransactionStatus;
use App\Models\TournamentTable;
use App\Models\PokerWwalletTtransaction;
use App\Models\PaymentTransaction;

class MasterTransactionHistory extends Model
{
    //
    protected $table="master_transaction_history";

    public $timestamps = false;

    public function user(){
    	return $this->belongsTo(User::class, 'USER_ID');
    }
    public function transactionType(){
    	return $this->belongsTo(TransactionType::class, 'TRANSACTION_TYPE_ID');
    }

    public function transactionStatus(){
    	return $this->belongsTo(TransactionStatus::class, 'TRANSACTION_STATUS_ID');
    }
    public function pokerWwalletTtransaction(){
    	return $this->belongsTo(PokerWwalletTtransaction::class, 'ROOM_ID');
    }
    public function payment_transaction(){
		return $this->hasOne(PaymentTransaction::class, 'INTERNAL_REFERENCE_NO');
	}
    public function getPromoCode($INTERNAL_REFERENCE_NO){
		return PaymentTransaction::select('PROMO_CODE')->where('INTERNAL_REFERENCE_NO', $INTERNAL_REFERENCE_NO)->first();
    }

    public function gerReferFriendName($INTERNAL_REFERENCE_NO){
        return \DB::table('refer_friend_bonus as rf')
            ->select('rf.FRIEND_ID', 'u.USERNAME')
            ->join('user as u', 'refer_friend_bonus.FRIEND_ID' ,'=', 'u.USER_ID')
            ->where('rf.INTERNAL_REFERENCE_NO',$INTERNAL_REFERENCE_NO )
            ->first();
    }

    
    // public function getTournamentNameByRefNo($INTERNAL_REFERENCE_NO){
    //   return PokerWwalletTtransaction::select('TOURNAMENT_NAME')->where('INTERNAL_REFERENCE_NO', $INTERNAL_REFERENCE_NO)->first();
    //   }

    public function getTournamentNameByRefNo($INTERNAL_REFERENCE_NO){
      return \DB::table('tournament_tables')
          ->select('tournament_tables.TOURNAMENT_NAME')
          ->join('poker_wallet_transaction', 'poker_wallet_transaction.ROOM_ID' ,'=', 'tournament_tables.TOURNAMENT_TABLE_ID')
          ->where('poker_wallet_transaction.INTERNAL_REFERENCE_NO',$INTERNAL_REFERENCE_NO )
          ->first();
  }
    
}
