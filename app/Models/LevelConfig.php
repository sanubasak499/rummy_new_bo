<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LevelConfig extends Model
{
    protected $table = 'level_config';
    protected $primaryKey = 'LEVEL_CONFIG_ID';
}
