<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AppTypes extends Model
{
    protected $table="app_types";
    protected $primaryKey ="APP_TYPE_ID";

    public $timestamps = false;

    public function AppTypeResult(){  
		$AppTypeResult = self::select('APP_TYPE_ID','APP_TYPE_DESC')
                        ->where('STATUS',1)
                        ->get();
                        return $AppTypeResult;
	}
}
