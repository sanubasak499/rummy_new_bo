<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RewardTracking extends Model
{
    protected $table = "reward_tracking";
	protected $primaryKey = "REWARD_TRACKING_ID";
	public $timestamps = false;
}
