<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResponsibleGamingRequest extends Model
{
    protected $table = 'responsible_gaming_request';
    protected $primaryKey = 'REQUEST_ID';
    public $timestamps = false;
    
    protected $fillable = ['USER_ID','GAME_TYPE','CURRENT_LIMIT','REQUESTED_LIMIT','REASON','STATUS','STATUS_UPDATED_ON','STATUS_UPDATED_BY'];
}
