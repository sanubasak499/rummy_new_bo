<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AppBanners extends Model
{
    protected $table="app_banners";
    protected $primaryKey ="APP_BANNER_ID";

    public $timestamps = false;

    // App Banner Edit
    public  function appBannerEdit($APP_BANNER_ID)
	{  
        $AppBannerResult = self::from(app(self::class)->getTable()." as ab")
                            ->select('ab.APP_BANNER_ID','ab.APP_TYPE_ID','ab.BANNER_CATEGORY_ID','ab.BANNER_TYPE_ID','ab.BANNER_IMAGE_URL','ab.BANNER_LINK_BUTTON_TEXT',
                            'ab.BANNER_REDIRECTION_URL','ab.TOURNAMENT_ID','ab.TOURNAMENT_NAME','ab.SERVER_ID','ab.SERVER_URL','ab.STATUS','ab.START_TIME','ab.END_TIME',
                            'ab.CREATED_DATE','at.APP_TYPE_DESC','bc.BANNER_CATEGORY_DESC','bt.BANNER_TYPE_DESC','t.TOURNAMENT_NAME','ab.EXTERNAL_URL','ab.RELATIVE_URL','ab.SEGMENT_ID')
                            ->leftjoin('tournament as t', 't.TOURNAMENT_ID' ,'=', 'ab.TOURNAMENT_ID')
                            ->join('app_types as at', 'at.APP_TYPE_ID' ,'=', 'ab.APP_TYPE_ID')
                            ->join('banner_categories as bc', 'bc.BANNER_CATEGORY_ID' ,'=', 'ab.BANNER_CATEGORY_ID')
                            ->join('banner_types as bt', 'bt.BANNER_TYPE_ID' ,'=', 'ab.BANNER_TYPE_ID')
                            ->where('ab.APP_BANNER_ID',$APP_BANNER_ID )
                            ->get();
                            return $AppBannerResult;
    }
    
}
