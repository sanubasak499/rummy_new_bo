<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentProvider extends Model
{
    protected  $table = 'payment_provider';
	protected $primaryKey = 'PAYMENT_PROVIDER_ID';
	public $timestamps = false;


	
}