<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MiniGameType extends Model
{
    protected $table ="minigames_type";
    protected $primaryKey = 'MINIGAMES_TYPE_ID';
    
    public $timestamps=false;
}
