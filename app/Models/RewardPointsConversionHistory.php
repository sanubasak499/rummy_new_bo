<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RewardPointsConversionHistory extends Model
{
    protected $table = "reward_points_conversion_history";
	protected $primaryKey = "S_NO";
	const CREATED_AT = 'CREATED_DATE';
	const UPDATED_AT = 'UPDATED_DATE';
}
