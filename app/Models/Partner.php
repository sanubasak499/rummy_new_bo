<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $table='partner';

    protected $primaryKey = 'PARTNER_ID';

    protected $guarded =[];

    const CREATED_AT = 'CREATED_ON';
    const UPDATED_AT = 'UPDATED_DATE';
}
