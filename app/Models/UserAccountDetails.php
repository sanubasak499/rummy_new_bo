<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAccountDetails extends Model
{
	protected $table = 'user_account_details';

    protected $primaryKey = 'USER_ACCOUNT_ID';
	public $timestamps = false;
}
