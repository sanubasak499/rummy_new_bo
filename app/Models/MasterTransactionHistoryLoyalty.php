<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterTransactionHistoryLoyalty extends Model
{
    protected $table = 'master_transaction_history_loyalty';
	protected $primaryKey = 'MASTER_TRANSACTTION_ID';
}
