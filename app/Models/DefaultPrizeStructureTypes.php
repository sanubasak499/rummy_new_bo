<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DefaultPrizeStructureTypes extends Model
{
    protected $table = "default_prize_structure_types";
}