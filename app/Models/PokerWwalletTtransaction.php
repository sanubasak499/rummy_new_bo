<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PokerWwalletTtransaction extends Model
{
    protected $table = 'poker_wallet_transaction';

    protected $primaryKey = 'GAME_TRANSACTION_ID';
	public $timestamps = false;
}
