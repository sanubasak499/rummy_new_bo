<?php

namespace App\models;

use App\Models\SegmentType;
use App\Models\Tournament;
use Illuminate\Database\Eloquent\Model;

class PushNotification extends Model
{
    protected $table = 'push_notifications';
	protected $fillable = [ 'PUSH_NOTIFICATION_NAME', 'PUSH_NOTIFICATION_TITLE', 'NOTIFICATION_BODY', 'IMAGE_PATH', 'REDIRECTION_TYPE_ID', 
		'REDIRECTION_URL', 'IS_RELATIVE', 'IS_EXTERNAL', 'TOURNAMENT_ID', 'SENT_TO', 'SEGMENT_IDS', 'PUSHED_BY', 'PUSHED_DATE', 
		'CREATED_DATE', 'UPDATED_BY', 'UPDATE_DATE'];
    protected $primaryKey = 'PUSH_NOTIFICATION_ID';
    const CREATED_AT = 'CREATED_DATE';
	const UPDATED_AT = 'UPDATE_DATE';

	public function getDescriptionLimitAttribute($char=null){
		$char = $char ?? 50;
        $string = strip_tags($this->NOTIFICATION_BODY);

		if (strlen($string) > $char) {
            // truncate string
            $stringCut = substr($string, 0, $char);
            $endPoint = strrpos($stringCut, ' ');
            //if the string doesn't contain any space then it will cut without word basis.
			$string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...';
        }
        return $string;
    }

    public function getSegments($segmentIds=null,$select=null){
        $select = $select ?? ["SEGMENT_ID", "SEGMENT_TITLE"];
        $segmentIds = $segmentIds ?? $this->SEGMENT_IDS;
        $segmentIds = \is_array($segmentIds) ? $segmentIds : explode(',', $segmentIds);
        $this->segmentDetails = SegmentType::select($select)->find($segmentIds);
        return $this->segmentDetails;
    }
    public function getTournaments($tournamentIds=null,$select=null){
        $select = $select ?? ["TOURNAMENT_ID", "TOURNAMENT_NAME"];
        $tournamentIds = $tournamentIds ?? $this->TOURNAMENT_ID;
        $tournamentIds = \is_array($tournamentIds) ? $tournamentIds : explode(',', $tournamentIds);
        $this->tournamentDetails = Tournament::select($select)->find($tournamentIds);
        return $this->tournamentDetails;
    }
}
