<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RewardLevel extends Model
{
	protected $table = "rewards_level";
	protected $primaryKey = "LEVEL_ID";
	public $timestamps = false;
	
}
