<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class StaticBlock extends Model
{
    protected $table="static_block";
    protected $primaryKey ="STATIC_BLOCK_ID";

    public $timestamps = false;

}
