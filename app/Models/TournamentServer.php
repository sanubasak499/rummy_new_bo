<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentServer extends Model
{
    protected $table = "tournament_servers";
}