<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterTransactionHistoryBaazi extends Model
{
    protected $table = 'master_transaction_history_baazi';
	protected $primaryKey = 'MASTER_TRANSACTTION_ID';
}
