<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UserKycDetail extends Model
{
    protected $table = 'USER_KYC_DETAILS';
    protected $primaryKey = 'USER_KYC_ID';
//    public $timestamps = ["updated_at"];
    const UPDATED_AT = 'UPDATED_AT';
    protected $guarded = [];
}
