<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResponsibleGameLog extends Model
{
    protected $table = 'responsible_game_logs';
    protected $primaryKey = 'RESPONSIBLE_GAME_LOGS_ID';
    const CREATED_AT = 'CREATED_DATE';
    const UPDATED_AT = 'UPDATED_DATE';

    protected $fillable = ['USER_ID','REQUEST_ID','RESPONSIBLE_SETTINGS_TYPE','FROM_DATE','TO_DATE','STATUS','ACTION','VALUE','UPDATED_BY','UPDATED_DATE','CREATED_DATE'];
}
