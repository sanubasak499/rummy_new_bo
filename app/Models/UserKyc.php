<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserKyc extends Model
{
    protected $table = 'USER_KYC';

    protected $primaryKey = 'USER_KYC_ID';
	
	const CREATED_AT = 'CREATED_DATE';
    const UPDATED_AT = 'UPDATE_DATE';
}
