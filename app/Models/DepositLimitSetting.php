<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DepositLimitSetting extends Model
{
    protected $table = "deposit_limit_settings";
    protected $primaryKey = "DEPOSIT_LIMIT_ID";
    protected $fillable = ["DEPOSIT_LIMIT_ID", "DEP_LEVEL", "DEP_AMOUNT", "DEP_AMOUNT_PER_DAY", "TRANSACTIONS_PER_DAY", "DEP_AMOUNT_PER_WEEK", "TRANSACTIONS_PER_WEEK", "DEP_AMOUNT_PER_MONTH", "TRANSACTIONS_PER_MONTH", "CREATED_ON", "UPDATED_DATE", "CREATED_BY", "UPDATED_BY"];
    public $timestamps = false;
}
