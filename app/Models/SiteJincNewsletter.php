<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class SiteJincNewsletter extends Model
{
    // use Notifiable;
    protected $table = 'site_jinc_newsletter';

    protected $primaryKey = 'id';
    protected $guarded = [];
}
