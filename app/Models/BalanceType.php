<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BalanceType extends Model
{
    protected $table = 'balance_type';
    protected $primaryKey = 'BALANCE_TYPE_ID';
    const UPDATED_AT = 'UPDATED_DATE';

    public $timestamps = false;

    public function scopeFilterId($query, $ids=array()){
    	return $query->whereIn('BALANCE_TYPE_ID',$ids);
    }
}
