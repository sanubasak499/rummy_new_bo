<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemplateType extends Model
{
    protected $table = "bo_admin_template_types";
    protected $primaryKey="template_type_id";
}