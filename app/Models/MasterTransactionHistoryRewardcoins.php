<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterTransactionHistoryRewardcoins extends Model
{
	protected $table = 'master_transaction_history_rewardcoins';
    protected $primaryKey = 'MASTER_TRANSACTTION_ID';
	public $timestamps = false;
}
