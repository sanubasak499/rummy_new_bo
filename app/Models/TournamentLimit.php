<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentLimit extends Model
{
    protected $table = "tournament_limit";
}