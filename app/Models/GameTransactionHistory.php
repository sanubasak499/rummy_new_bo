<?php


/*
    |--------------------------------------------------------------------------
    | 
    | Class Name: GameTransactionHistory
	| Author: Dharminder Singh
	| Created Date: Dec 20 2019
	| Modified Date: 
	| Modified By: 
	| Modified Details: 
	|--------------------------------------------------------------------------
*/
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class GameTransactionHistory extends Model
{
    protected $table="game_transaction_history";
    protected $primaryKey = 'GAME_TRANSACTION_ID';
	public $timestamps = false;
}
