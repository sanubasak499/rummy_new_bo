<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentLevelStructure extends Model
{
    protected $table = "tournament_level_structure";
}