<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentTransactionPokercommission extends Model
{
    protected $table = 'payment_transaction_pokercommission';
    protected $primaryKey = 'PAYMENT_TRANSACTION_ID';
    const CREATED_AT = 'PAYMENT_TRANSACTION_CREATED_ON';
    const UPDATED_AT = 'UPDATED_DATE';

}
