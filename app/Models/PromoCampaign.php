<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoCampaign extends Model
{
    protected $table='promo_campaign';

    protected $primaryKey = 'PROMO_CAMPAIGN_ID';
	
	const CREATED_AT = 'CREATED_ON';
	public $timestamps=false;
	
	public function promo_campaign_type()
    {
        return $this->hasManyThrough('App\Models\PromoCampaignType','App\Models\PromoStatus');
    }
	public function promo_status()
    {
        return $this->hasManyThrough('App\Models\PromoStatus');
    }

    public function getCoinsRequired()
    {
        return $this->hasOne(PromoRule::class, 'PROMO_CAMPAIGN_ID');
    }
}