<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $table = "bo_admin_templates";
    protected $primaryKey="template_id";

    public function scopeActive($query){
        return $query->where('status', 1);
    }
}