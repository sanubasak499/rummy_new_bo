<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignToUser extends Model
{
    protected $table='campaign_to_user';

    protected $primaryKey = 'CAMPAIGN_TO_USER_ID';
	
    const UPDATED_AT = 'UPDATED_DATE';
    public $timestamps = false;
	
	public function scopeGetUniqueUser($query,$id){
		return $query->where('PROMO_CAMPAIGN_ID' ,'=',$id);
	}
}