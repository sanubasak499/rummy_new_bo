<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfcGameHandHistory extends Model
{
    //
    protected $table = 'ofc_game_hand_history';

    protected $primaryKey = 'HAND_HISTORY_ID';

    public function tournament(){
        return $this->belongsTo(Tournament::class, 'TOURNAMENT_ID');
    }
    public function user(){
        return $this->belongsTo(User::class, 'USER_ID');
    }
}
