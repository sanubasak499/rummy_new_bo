<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentBlindStructure extends Model
{
    protected $table = "tournament_blind_structure";
}