<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    // use Notifiable;

    protected $table = 'user';

    protected $primaryKey = 'USER_ID';

    protected $appends = ['FULLNAME'];

    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    const CREATED_AT = 'REGISTATION_TIMESTAMP';
    const UPDATED_AT = 'UPDATED_DATE';

    public function getFULLNAMEAttribute(){
        return $this->FIRSTNAME.' '.$this->LASTNAME;
    }


    public function partner(){
        return $this->belongsTo(Partner::class, 'PARTNER_ID');
    }
	
	public function user_kyc(){
        return $this->hasOne(UserKyc::class, 'USER_ID');
    }
	
	public function user_account_details(){
        return $this->hasOne(UserAccountDetails::class, 'USER_ID');
    }

    public function user_points(){
        return $this->hasMany(UserPoint::class, 'USER_ID');
    }
	
	public function refer_name(){
		return $this->belongsTo(self::class, 'REFERRED_BY');
	}

    public function getUserIdByUserName($username){
        $this->where('USERNAME', $username)->first()->USER_ID;
    }

    public function ofc_game_transaction_history(){
        return $this->hasMany(OfcGameTransactionHistory::class, 'USER_ID');
    }

    public function responsible_game_setting(){
        return $this->hasOne(ResponsibleGameSetting::class,'USER_ID');
    }
}
