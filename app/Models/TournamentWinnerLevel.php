<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentWinnerLevel extends Model
{
    protected $table = "tournament_winner_level";

    public $timestamps = false;
}