<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;

class TournamentRegistration extends Model
{
    // use Notifiable;
    protected $table = 'tournament_registration';
    protected $primaryKey = 'TOURNAMENT_REG_ID';
    const UPDATED_AT = 'UPDATED_DATE';
}
