<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SegmentType extends Model
{
    protected $table = 'segment_type';
    protected $primaryKey='SEGMENT_ID';

    const CREATED_AT = 'CREATED_DATE';
    const UPDATED_AT = 'UPDATE_DATE';
    
    public function scopeActive($query){
        return $query->where('STATUS','=','1');
    } 
}
