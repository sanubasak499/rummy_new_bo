<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WithdrawTransactionHistory extends Model
{

    protected $table = "withdraw_transaction_history";
    protected $primaryKey = "WITHDRAW_TRANSACTTION_ID";
    public $timestamps = false;

    protected $fillable = [
        'USER_ID', 'BALANCE_TYPE_ID','TRANSACTION_STATUS_ID', 'TRANSACTION_TYPE_ID', 'WITHDRAW_AMOUNT', 'WITHDRAW_TDS', 'WITHDRAW_FEE', 'FEE_WAIVED', 'PAID_AMOUNT', 'INTERNAL_REFERENCE_NO','WITHDRAW_TYPE','TRANSACTION_DATE','APPROVED_AT','GATEWAY','GATEWAY_STATUS'
    ];

    public function payment_transaction()
    {
        return $this->hasOne(PaymentTransaction::class, 'INTERNAL_REFERENCE_NO');
    }
}
