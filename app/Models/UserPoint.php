<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPoint extends Model
{
    protected $table ="user_points";

    protected $primaryKey = 'USER_POINTS_ID';

    // public $timestamps = false;

    public $timestamps = ["updated_at"];

    const UPDATED_AT = 'UPDATED_DATE';
	
	public function coin_type(){
		return $this->belongsTo(CoinType::class, 'COIN_TYPE_ID');
	}
}
