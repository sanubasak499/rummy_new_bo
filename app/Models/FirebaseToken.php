<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FirebaseToken extends Model
{
    protected $table = 'firebase_tokens';

    public function scopeTokenByUserId($query,$user_ids){
    	return $query->distinct('TOKEN')->whereIn('USER_ID',$user_ids);
    }
}
