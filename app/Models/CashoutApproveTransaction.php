<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashoutApproveTransaction extends Model
{
    protected $table = 'cashout_approve_transaction';

    protected $primaryKey = 'CASHOUT_TRANSACTTION_ID';
	public $timestamps = false;
}
