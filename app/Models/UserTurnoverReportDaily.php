<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTurnoverReportDaily extends Model
{
    protected $table='user_turnover_report_daily';

    protected $primaryKey = 'USER_TURNOVER_REPORT_ID';
	
	const UPDATED_AT = 'UPDATED_DATE';
	
}