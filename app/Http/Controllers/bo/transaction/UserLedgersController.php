<?php

namespace App\Http\Controllers\bo\transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PlayerLedger;
use Carbon\Carbon;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Author: Nitesh Kumar Jha
 * Purpose: Get ledgers results and  export excel  of users on the basis of user name ,email id, contect, internal referance no ,ACTION and CREATED_DATE
 * Created Date: 6-4-2020
 * Modify Date: 8-10-2020
 * Changes : remove unuse js , modify loop
 */

class UserLedgersController extends Controller
{

    public function index()
    {
        return view('bo.views.transaction.UserLedgers');
    }
    // Search User Ledgers report deatils 
    public function UserLedgerSearch(Request $request)
    {
        $perPage = config('poker_config.paginate.per_page');
        if (!$request->isMethod('post')) {
            return view('bo.views.transaction.UserLedgers');
        }
        $page = request()->page;

        $userLedgersResult = $this->getAllResult($request);
        $userLedgersResult = $userLedgersResult->paginate($perPage, ['*'], 'page', $page);
        $params = $request->all();
        $params['page'] = $page;

        return view('bo.views.transaction.UserLedgers', ['userLedgersResult' => $userLedgersResult, 'params' => $params,]);
    }

    public function getAllResult($request)
    {
        $userLedgersResult = PlayerLedger::from(app(PlayerLedger::class)->getTable() . " as pl")
            ->select('u.USERNAME', 'pl.ACTION', 'pl.TRANSACTION_AMOUNT', 'pl.INTERNAL_REFERENCE_NO', 'pl.PAYMENT_TRANSACTION_CREATED_ON', 'pl.TOTAL_DEPOSITS', 'pl.TOTAL_WITHDRAWALS', 'pl.TOTAL_TAXABLE_WITHDRAWALS', 'pl.ELIGIBLE_WITHDRAWAL_WITHOUT_TAX', 'pl.EXEMPTION_10K', 'pl.TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX')
            ->join('user as u', 'u.USER_ID', '=', 'pl.USER_ID')

            ->when($request->search_by == "USERNAME", function ($query) use ($request) {
                return $query->where('u.USERNAME', 'LIKE', "%{$request->search_by_value}%");
            })
            ->when($request->search_by == "EMAIL_ID", function ($query) use ($request) {
                return $query->where('u.EMAIL_ID', 'LIKE', "%{$request->search_by_value}%");
            })
            ->when($request->search_by == "CONTACT", function ($query) use ($request) {
                return $query->where('u.CONTACT', 'LIKE', "%{$request->search_by_value}%");
            })

            ->when(!empty($request->ACTION), function ($query) use ($request) {
                return $query->where('pl.ACTION', $request->ACTION);
            })

            ->when(!empty($request->TRANSACTION_AMOUNT), function ($query) use ($request) {
                return $query->where('pl.TRANSACTION_AMOUNT', $request->TRANSACTION_AMOUNT);
            })
            ->when(!empty($request->INTERNAL_REFERENCE_NO), function ($query) use ($request) {
                return $query->where('pl.INTERNAL_REFERENCE_NO', $request->INTERNAL_REFERENCE_NO);
            })

            ->when(!empty($request->START_DATE_TIME), function ($query) use ($request) {
                $dateFrom = Carbon::parse($request->START_DATE_TIME)->format('Y-m-d H:i:s');
                return $query->whereDate('pl.PAYMENT_TRANSACTION_CREATED_ON', ">=", $dateFrom);
            })
            ->when(!empty($request->END_DATE_TIME), function ($query) use ($request) {
                $dateTo = Carbon::parse($request->END_DATE_TIME)->format('Y-m-d H:i:s');
                return $query->whereDate('pl.PAYMENT_TRANSACTION_CREATED_ON', "<=", $dateTo);
            })
            ->orderBy('pl.PAYMENT_TRANSACTION_CREATED_ON', 'DESC');
        return $userLedgersResult;
    }

    //Export To Excel 
    public function exportExcel(Request $request)
    {
        $excelData =  $this->getAllResult($request)->get();

        $data[0] = array("Username" => "", "Action" => "", "Amount" => "", "Reference No" => "", "Transaction Date" => "", "Total Deposits" => "", "Total Withdrawals" => "", "Total Taxable Withdrawals" => "", "Eligible Withdrawa Without Tax" => "", "10K Exemption" => "", "Total Eligible Withdrawal Without Tax" => "");
        $name = "User-Ledger";
        $fileName = $name . ".xlsx";
        $headings = array_keys($data[0]);

        return Excel::download(new CollectionExport($excelData, $headings), $fileName);
    }
}
