<?php

namespace App\Http\Controllers\bo\transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ActionReason;
use App\Models\AdjustmentTransaction;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class AdjustmentTransactionsController extends Controller
{
	
    public function index(Request $request){

    	if($request->isMethod('post')){

    		$perPage = config('poker_config.paginate.per_page');
    		$page = request()->page;

    		// validating data
    		$arrVal = array('amount' => 'nullable|numeric');

    		if($request->search_by=='email')
    			$arrVal['search_by_value'] = 'nullable|email';
    		elseif($request->search_by=='contact_no')
    			$arrVal['search_by_value'] = 'nullable|numeric';
   		
    		$request->validate($arrVal);
    		// validation end
    		
    		// Fetching Data
    		$queryObj = AdjustmentTransaction::query()->from(app(AdjustmentTransaction::class)->getTable()." as adj");

    		if($request->has('excel')){

    			$headingofsheet = ['USERNAME','REFERENCE NO','BALANCE TYPE','COIN TYPE','AMOUNT','ADJUSTMENT','ADJUSTED BY','TRANSACTION DATE','REASON','COMMENT'];
                
    			$resultSets['dataSets'] = $this->fetchData($queryObj, $request)
                                               ->orderBy('adj.ADJUSTMENT_TRANSACTION_ID','DESC')
    									       ->get();

    			$fileName = urlencode("Adjustment-Transaction-Report_".date('d-m-Y').".xlsx");
    			return Excel::download(new CollectionExport($resultSets['dataSets'],$headingofsheet), $fileName);
    		}
    		else{
                $queryObj = $this->fetchData($queryObj, $request);
                $cloneQueryObj = clone $queryObj;
    			$resultSets['dataSets'] = $queryObj
                                               ->orderBy('adj.ADJUSTMENT_TRANSACTION_ID','DESC')
    									       ->paginate($perPage, ['*'], 'page', $page);
    		}
    		    		
    		$resultSets['params'] = $request->all();

    		
    		$totals = DB::select(
    		    $cloneQueryObj->select(
    		        DB::raw("sum(adj.ADJUSTMENT_AMOUNT) as totalAmount"),
    		        DB::raw("count(adj.INTERNAL_REFERENCE_NO) as totalCount")
    		    )->toSql(),
    		    $cloneQueryObj->getBindings()
    		);

    		$resultSets['totals'] = count($totals) > 0 ? $totals[0] : null;
            $resultSets['reasons'] = ActionReason::active()->whereIn('REASON_CATEGORY_ID',[1])->get();
    		// Fetching End
			return view('bo.views.transaction.index',$resultSets);
	   	}
    	else{
    		return view('bo.views.transaction.index',['reasons' =>ActionReason::active()->whereIn('REASON_CATEGORY_ID',[1])->get()]);
    	}
    	
    }

    private function fetchData($queryObj, $request){
   	    

        
    	$queryObj->select('user.USERNAME AS username','adj.INTERNAL_REFERENCE_NO AS Reference_No','tt.TRANSACTION_TYPE_NAME AS BALANCE_TYPE',DB::raw("CASE WHEN UPPER(ct.NAME)='REWARD COINS' THEN 'Reward Points' ELSE ct.NAME END AS COIN_TYPE"),'adj.ADJUSTMENT_AMOUNT AS AMOUNT',DB::raw(" CASE WHEN adj.ADJUSTMENT_ACTION='Subtract' THEN 'Deduct' ELSE 'Add' END AS ADJUST_TYPE"),'adj.ADJUSTMENT_CREATED_BY AS ADJUSTED_BY','adj.ADJUSTMENT_CREATED_ON','ar.ACTIONS_REASON AS REASON',DB::raw("CASE WHEN TRIM(adj.ADJUSTMENT_COMMENT)!='' THEN adj.ADJUSTMENT_COMMENT ELSE 'NA' END AS COMMENT"));

    	$queryObj->leftJoin('user', 'adj.USER_ID', '=', 'user.USER_ID');
    	$queryObj->leftJoin('transaction_type AS tt', 'adj.TRANSACTION_TYPE_ID' ,'=' ,'tt.TRANSACTION_TYPE_ID');
    	$queryObj->leftJoin('coin_type AS ct', 'adj.COIN_TYPE_ID' ,'=' ,'ct.COIN_TYPE_ID');
        $queryObj->leftJoin('action_reasons AS ar', 'adj.REASON_ID' ,'=' ,'ar.ACTIONS_REASONS_ID');

    	if($request->search_by_value!=''){

	    	if($request->search_by=='username'){
	    		$queryObj->where('user.USERNAME', '=' , $request->search_by_value);
	    	}
	    	else if($request->search_by=='email'){
	    		$queryObj->where('user.EMAIL_ID', '=' , $request->search_by_value);
	    	}
	    	else{
	    		$queryObj->where('user.CONTACT', '=' , $request->search_by_value);
	    	}

	    }	
	    
	    if($request->ref_no!=''){
	    	$queryObj->where('adj.INTERNAL_REFERENCE_NO', '=' ,$request->ref_no);
	    }

    	if($request->amount!=''){
    		$queryObj->where('adj.ADJUSTMENT_AMOUNT', '=' ,$request->amount);
    	}

        if($request->adjust_type == 1){
            $queryObj->where('adj.ADJUSTMENT_ACTION', '=', 'Add');
        }
        elseif ($request->adjust_type == 2) {
            $queryObj->where('adj.ADJUSTMENT_ACTION', '=', 'Subtract');
        }

    	if($request->date_from!='' || $request->date_to!=''){
    		$from = date('Y-m-d H:i:s', strtotime($request->date_from));
    		$to = date('Y-m-d H:i:s', strtotime($request->date_to)) ;
    		$queryObj->whereBetween('adj.ADJUSTMENT_CREATED_ON', [$from,$to] );
    	}

    	if($request->has('balance_type')){
    		$queryObj->whereIn('adj.TRANSACTION_TYPE_ID', $request->balance_type);
    	}

    	
    	if($request->has('reason')){
    		$queryObj->whereIn('adj.REASON_ID', $request->reason);
    	}

        if($request->has('coin_type')){
            $queryObj->whereIn('adj.COIN_TYPE_ID', $request->coin_type);
        }

    	
    	return $queryObj;
    }

}
