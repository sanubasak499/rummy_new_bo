<?php

namespace App\Http\Controllers\bo\games\poker\game;

use Illuminate\Http\Request;
use App\Http\Controllers\bo\games\poker\GameController;
use App\Models\User;
use App\Models\CoinType;
use App\Models\MiniGameType;
use App\Models\OfcGameHistory;
use App\Models\OfcGameTransactionHistory;
use App\Models\OfcGameHandHistory;

use DB;
use Carbon\Carbon;

class OFCGameController extends GameController
{
    public function __construct(){
        parent::__construct();
        $this->db = app(DB::class);
        $this->perPage = config('poker_config.paginate.per_page');
    }

    public function index(){
        $data=[];
        $data['coin_types'] = $this->getAllCurrencyTypes();
        $data['ofc_types'] = $this->getOFCTypes();
        
        return view('bo.views.games.poker.game.ofc.index',$data);
    }
    public function viewofcgames(Request $request){
        $data=[];
        $data['coin_types'] = $this->getAllCurrencyTypes();
        $data['ofc_types'] = $this->getOFCTypes();
        
        \DB::enableQueryLog();

        $data['params'] = $request->all();
        $data['page'] = request()->page;

        $searchData = $this->getSearchOfcGameData($data['params'], $data['page']);
        $data['searchGameData'] = $searchData['paginate'];
        $data['totalSearchData'] = $searchData['sumColumns'];
        
        $query = \DB::getQueryLog();
        // dd($query);
        
        return view('bo.views.games.poker.game.ofc.index',$data);
    }

    public function getAllCurrencyTypes(){
        return CoinType::select("NAME","COIN_TYPE_ID")
                ->whereIn('NAME', ['Cash', 'Baazi Chips'])
                ->where('STATUS',1)->get();
    }
    public function getOFCTypes(){
        return MiniGameType::select("MINIGAMES_ID","MINIGAMES_TYPE_ID","GAME_DESCRIPTION","MINIGAMES_TYPE_NAME")
        ->where('MINIGAMES_TYPE_ID', 16)
        ->where('STATUS',1)->get();
    }

    public function getSearchOfcGameData($params, $perPage=100, $page=null) {

        // PLAYER_ID or hand id not null
        $allPlayGroupIDs = $this->getAllPlayGroupIDs($params);
        
        $query =OfcGameHistory::query();
        $query->from(app(OfcGameHistory::class)->getTable()." as g");
        $query->leftJoin('tournament_tables as tt', 'tt.TOURNAMENT_TABLE_ID', '=', 'g.TOURNAMENT_TABLE_ID');
        $query->leftJoin('tournament as t', 't.TOURNAMENT_ID', '=', 'tt.TOURNAMENT_ID');
        
        // when $allPlayGroupIDs not null
        $query->when(count($allPlayGroupIDs)>0, function($q) use ($allPlayGroupIDs){
            return $q->whereIn('g.PLAY_GROUP_ID', $allPlayGroupIDs);
        });
        
        $query->when(!empty($params["TABLE_ID"]), function($q) use ($params){
            return $q->where('t.TOURNAMENT_NAME', $params["TABLE_ID"]);
        });
      
        $query->when(!empty($params["GAME_TYPE"]) && count($allPlayGroupIDs) == 0, function($q) use ($params){
            return $q->where('t.MINI_GAME_TYPE_ID', $params["GAME_TYPE"]);
        });
        $query->when(!empty($params["GAME_ID"]), function($q) use ($params){
            return $q->where('g.PLAY_GROUP_ID', $params["GAME_ID"]);
        });
        $query->when(!empty($params["CURRENCY_TYPE"]), function($q) use ($params){
            return $q->where('t.COIN_TYPE_ID', $params["CURRENCY_TYPE"]);
        });
        $query->when(!empty($params["FANTASY_TYPE"]), function($q) use ($params){
            return $q->where('g.FANTASY_TYPE', $params["FANTASY_TYPE"]);
        });
        $query->when(!empty($params["DEAD_HAND"]), function($q) use ($params){
            return $q->where('g.DEAD_HAND', $params["DEAD_HAND"]);
        });
        $query->when(!empty($params["FANTASY_GAME"]), function($q) use ($params){
            return $q->where('g.FANTASY_GAME', $params["FANTASY_GAME"]);
        });
        // $query->when(!empty($params["STAKE"]), function($q) use ($params){
        //     return $q->where('g.TOTAL_STAKE', $params["STAKE"]);
        // });
        $query->when(!empty($params['date_from']), function($q) use ($params){
            $date_from = Carbon::parse($params['date_from'])->format('Y-m-d H:i:s');
            return $q->whereDate('STARTED', ">=", $date_from);
        });
        $query->when(!empty($params['date_to']), function($q) use ($params){
            $date_to = Carbon::parse($params['date_to'])->format('Y-m-d H:i:s');
            return $q->whereDate('STARTED', "<=", $date_to);
        });
        $bindings = $query->getBindings();
        
        $data['sumColumns'] = DB::select(
                                    $query->selectRaw(" SUM(g.TOTAL_WIN) AS TOTAL_WIN , SUM(g.TOTAL_REVENUE) AS TOTAL_REVENUE , SUM(g.REAL_REVENUE) AS TOTAL_REAL_RAKE , SUM(g.BONUS_REVENUE) AS TOTAL_BONUS_RAKE , SUM(g.SERVICE_TAX) as TOTAL_SERVICE_TAX ")
                                ->toSql(), $bindings);
        $data['paginate'] =  $query->select("g.MINIGAMES_TYPE_ID","g.TOURNAMENT_TABLE_ID","g.PLAY_GROUP_ID","g.TOTAL_PLAYERS","g.TOTAL_WIN","g.TOTAL_REVENUE","g.APP_RAKE_PERCENTAGE","g.STARTED","g.ENDED","tt.TOURNAMENT_NAME","g.REAL_REVENUE","g.BONUS_REVENUE","g.SERVICE_TAX")
                                    ->selectRaw("g.GAME_HISTORY_ID")
                                    ->orderBy('g.GAME_HISTORY_ID','desc')
                                    ->paginate($this->perPage,['*'],'page',$page);
        return $data;
    }

    public function getAllPlayGroupIDs($params){
        $allPlayGroupIDs = [];
        if(!empty($params["PLAYER_ID"]) || !empty($params["HAND_ID"])){
            $transcationQ = OfcGameTransactionHistory::query();
            $transcationQ->from(app(OfcGameHistory::class)->getTable()." as p");
            $transcationQ->select("p.PLAY_GROUP_ID");
            $transcationQ->when(!empty($params["PLAYER_ID"]), function($q) use ($params){
                return $q->where('p.USER_ID',  app(User::class)->getUserIdByUserName($params["PLAYER_ID"]));
            });
            $transcationQ->when(!empty($params["HAND_ID"]), function($q) use ($params){
                return $q->where('p.INTERNAL_REFFERENCE_NO', $params["HAND_ID"]);
            });
            $transcationQ->when(!empty($params['date_from']), function($query) use ($params){
                $date_from = Carbon::parse($params['date_from']);
                return $query->whereDate('STARTED', ">=", $date_from);
            });
            $transcationQ->when(!empty($params['date_to']), function($query) use ($params){
                $date_to = Carbon::parse($params['date_to']);
                return $query->whereDate('STARTED', "<=", $date_to);
            });
            $allPlayGroupIDs = $transcationQ->pluck('PLAY_GROUP_ID');
        }
        return $allPlayGroupIDs;
    }


    public function ViewOfcDetail(Request $request, $play_game_id){
        
        // \DB::enableQueryLog();
        $data['ofGameHandHistory'] = OfcGameHandHistory::select('USER_ID','PLAY_GROUP_ID','TOURNAMENT_ID','INTERNAL_REFERENCE_NO','GAME_REFERENCE_NO','WIN','STAKE','REVENUE','RAKE','STREET1','STREET2','STREET3','STREET4','STREET5','DISCARD_CARD','PLAYER_DATA')
                            ->with('tournament:TOURNAMENT_ID,ANONYMOUS_TABLE,MAXBUYIN,POINT_VALUE,RAKE,RAKE1,RAKE_CAP,RAKE_CAP1')
                            ->with('user:USER_ID,USERNAME')
                            ->with(['user.ofc_game_transaction_history' => function($q) use ($play_game_id){
                                return $q->where('PLAY_GROUP_ID',$play_game_id);
                            }])
                            ->where('PLAY_GROUP_ID', $play_game_id)
                            ->get();
        $data['ofcGameHistory'] = OfcGameHistory::select("GAME_HISTORY_ID","PLAY_GROUP_ID","TOTAL_PLAYERS","TOTAL_WIN","TOTAL_REVENUE","APP_RAKE_PERCENTAGE","STARTED","ENDED")
                            ->where('PLAY_GROUP_ID', $play_game_id)
                            ->get();
        
        // $query = \DB::getQueryLog();
        
        return view('bo.views.games.poker.game.ofc.hand_detail',$data);
    }
}