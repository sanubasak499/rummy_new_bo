<?php

namespace App\Http\Controllers\bo\games\poker\tournament;

use Illuminate\Http\Request;
use App\Http\Controllers\bo\BaseController;
use Mail;
use DB;
use App\Models\Tournament;
use App\Models\User;
use App\Imports\UserImport;
use Maatwebsite\Excel\Importer;
use App\Traits\common_mail;
use App\Models\ActionReason;
use App\Models\TournamentUserTicket;
// use App\Facades\PokerBaazi;

class TournamentRegisterPlayerController extends BaseController
{
    //
    use common_mail;
    public function index()
    {
        $tournament['allUpcommingTour'] = app(Tournament::class)->getAllUpcomingTournaments();
        $tournament['action_reasons'] = $this->getReason();
        return view('bo.views.games.poker.tournament.ticketToUser', $tournament);
    }
    public function registerPlayer(Request $request)
    {
        $request->session()->forget('sucess');
        $ticketStatusFlagSucess = "";
        $ticketStatusResponse = array();
        $TourDetails = $request->all();
        $tour_id_array = $request['tour_id'];

        if (isset($TourDetails['autoreg'])) {
            $autoreg = $TourDetails['autoreg'];
        } else {
            $autoreg = "";
        }

        if (isset($TourDetails['tour_id'])) {
            $tour_id_array = $TourDetails['tour_id'];
        } else {
            return back()->with('error', 'Tournament name should not be empty.');
        }

        if (isset($TourDetails['reason'])) {
            if($this->matchReason($where=['ACTIONS_REASON'=>$TourDetails['reason']])){
                $ticket_reasion = $TourDetails['reason'];
            }else{
                return back()->with('error', 'Invalid Reason.');
            }
        } else {
            return back()->with('error', 'Reason should not be empty.');
        }

        if (isset($TourDetails['user_name'])) {
            $username_array = $TourDetails['user_name'];
        } else {
            return back()->with('error', 'Username should not be empty.');
        }
        $ticketStatusResponse = $this->tournamentRegister($tour_id_array, $username_array, $autoreg,$ticket_reasion);
        $tournament = app(Tournament::class)->getAllUpcomingTournaments();
        $action_reasons = $this->getReason();

        if ($ticketStatusResponse) {
            //track admin activity
            $action = "Tournament Ticket assign";
            $this->insertAdminActivity($ticketStatusResponse, $action);
            return view('bo.views.games.poker.tournament.ticketToUser', ['sucess' => $ticketStatusResponse, 'allUpcommingTour' => $tournament, 'action_reasons'=> $action_reasons]);
        }
    }

    public function registerplayerExcel(Request $request, Importer $importer)
    {
        try{
            $now = date("Y-m-d H:i:s");

            if (empty($request->tour_excel)) {
                return back()->with('error', 'Please upload an excel.');
            }
            if ($request->isMethod('get')) {
                $tournament = app(Tournament::class)->tournament();
                $action_reasons = $this->getReason();
                return view('bo.views.games.poker.tournament.ticketToUser', ['allUpcommingTour' => $tournament,'action_reasons'=> $action_reasons]);
            }
            if($request->file('tour_excel')->extension() == 'xlsx'){
                $autoreg = $request['autoreg'];
                $ticketStatusResponse = array();
                $importer->import($data = new UserImport, $request->tour_excel);
                $tourTicketData = $data->data;
                
                foreach ($tourTicketData as $tourTicketData) {
                    $tour_id = $tourTicketData['TOUR_ID'];
                    $user_name = $tourTicketData['USERNAME'];
                    $ticket_reasion = $tourTicketData['REASON'];
                    $ticket_source = 'ticket_assign_from_bo';
                    $getTournamentGameCode = $this->getTournamentGameTypeCode($tour_id);
                    if ($getTournamentGameCode) {
                        $tourCode = $getTournamentGameCode->ref_game_code;
                        $tourName = $getTournamentGameCode->tournament_name;
                        $tourBuyIn = $getTournamentGameCode->BUYIN;
                        $tourEntryFee = $getTournamentGameCode->ENTRY_FEE;
                        $tourStartTime = $getTournamentGameCode->TOURNAMENT_START_TIME;
                        $tourRegStartTime = $getTournamentGameCode->REGISTER_START_TIME;
                        $tourRegEndTime = $getTournamentGameCode->REGISTER_END_TIME;
                        
                        $user_id = getUserIdFromUsername($user_name);
                        $match_reason = $this->matchReason($where=['ACTIONS_REASON'=>$ticket_reasion]);

                        if ($user_id) {
                            if($match_reason){
                                // $alreadyAvailable = $this->chkUserAlreadyRegister($user_id, $tour_id);
                                $alreadyAvailable = app(Tournament::class)->chkUserAlreadyRegister($user_id, $tour_id);
                                if ($alreadyAvailable == 1) {
                                    $ticketStatusResponse[] = ['userId' => $user_id, 'userName' => $user_name, 'tournamentId' => $tour_id, 'tournamentName' => $tourName, 'ticketStatus' => '2','ticketReason'=>$ticket_reasion, 'ticketSource'=>$ticket_source];
                                } else {

                                    if ($autoreg == "on") {
                                        $procOne = DB::select("call sp_create_user_tournament_ticket_external('$tourCode','$tour_id','$user_id',@out1)");
                                        $this->updateReason($where=['USER_ID'=> $user_id, 'TOURNAMENT_ID'=> $tour_id], $data=['TICKET_REASON'=> $ticket_reasion,'TICKET_SOURCE'=> $ticket_source]);
                                        $outQuery = DB::raw("select @out1 as result");
                                        if ($outQuery) {
                                            $res = DB::select('call sp_tournament_registration(' . $tour_id . ',' . $user_id . ',@out1,@out2)');
                                        }
                                    } else {
                                        $res = DB::select("call sp_create_user_tournament_ticket('$tourCode','$tour_id','$user_id')");
                                        $this->updateReason($where=['USER_ID'=> $user_id, 'TOURNAMENT_ID'=> $tour_id], $data=['TICKET_REASON'=> $ticket_reasion,'TICKET_SOURCE'=> $ticket_source]);
                                    }
                                    //tour ticke mail to user
                                    $email = $this->getUserEmail($user_id);
                                    $params['email'] = $email;
                                    $params['username'] = $user_name;
                                    $params['tempId'] = 17;
                                    $params['subject'] = "Tournament Promotional Ticket";
                                    $params['tourName'] = $tourName;
                                    $params['tourBuyIn'] = $tourBuyIn;
                                    $params['tourEntryFee'] = $tourEntryFee;
                                    $params['tourStartTime'] = $tourStartTime;
                                    $params['tourRegStartTime'] = $tourRegStartTime;
                                    $params['tourRegEndTime'] = $tourRegEndTime;
                                    //send mail to user after sucessfull tour ticket assign
                                    if (!empty($email)) {
                                        $this->sendMail($params);
                                    }
                                    $ticketStatusResponse[] = ['userId' => $user_id, 'userName' => $user_name, 'tournamentId' => $tour_id, 'tournamentName' => $tourName, 'ticketStatus' => '1','ticketReason'=>$ticket_reasion, 'ticketSource'=>$ticket_source];
                                } 
                            } else {
                                $ticketStatusResponse[] = ['userId' => $user_id, 'userName' => $user_name, 'tournamentId' => $tour_id, 'tournamentName' => $tourName, 'ticketStatus' => '4','ticketReason'=>$ticket_reasion, 'ticketSource'=>$ticket_source];
                            }
                        } else {
                            $ticketStatusResponse[] = ['userId' => $user_id, 'userName' => $user_name, 'tournamentId' => $tour_id, 'tournamentName' => $tourName, 'ticketStatus' => '0','ticketReason'=>$ticket_reasion, 'ticketSource'=>$ticket_source];
                        }
                    } else {
                        $ticketStatusResponse[] = ['userId' => '-', 'userName' => '-', 'tournamentId' => $tour_id, 'tournamentName' => 'Does Not Exist', 'ticketStatus' => '3','ticketReason'=>$ticket_reasion, 'ticketSource'=>$ticket_source];
                    }
                }
                if ($ticketStatusResponse) {

                    //track admin activity
                    $action = "Tournament Ticket Bulk Assign";
                    $this->insertAdminActivity($ticketStatusResponse, $action);
                    $tournament = app(Tournament::class)->tournament();
                    $action_reasons = $this->getReason();
                    return view('bo.views.games.poker.tournament.ticketToUser', ['sucess' => $ticketStatusResponse, 'allUpcommingTour' => $tournament,'action_reasons'=> $action_reasons]);
                }
            } else {
                return back()->with('error', 'Only excel file accept');
            }
        }catch(\Exception $e){
            return back()->with('error', $e->getMessage());
        }   
    }



    public function tournamentRegister($tour_id_array, $username_array, $autoreg,$ticket_reasion)
    {
        foreach ($tour_id_array as $tour_id) {
            $getTournamentGameCode = $this->getTournamentGameTypeCode($tour_id);
            if ($getTournamentGameCode) {
                $tourCode = $getTournamentGameCode->ref_game_code;
                $tourName = $getTournamentGameCode->tournament_name;
                $tourBuyIn = $getTournamentGameCode->BUYIN;
                $tourEntryFee = $getTournamentGameCode->ENTRY_FEE;
                $tourStartTime = $getTournamentGameCode->TOURNAMENT_START_TIME;
                $tourRegStartTime = $getTournamentGameCode->REGISTER_START_TIME;
                $tourRegEndTime = $getTournamentGameCode->REGISTER_END_TIME;
                $ticket_source = 'ticket_assign_from_bo';
                foreach ($username_array as $user_name) {
                    $user_id = getUserIdFromUsername($user_name);

                    if ($user_id) {
                        // $alreadyAvailable = $this->chkUserAlreadyRegister($user_id, $tour_id);
                        $alreadyAvailable = app(Tournament::class)->chkUserAlreadyRegister($user_id, $tour_id);
                        if ($alreadyAvailable == 1) {
                            $ticketStatusResponse[] = ['userId' => $user_id, 'userName' => $user_name, 'tournamentId' => $tour_id, 'tournamentName' => $tourName, 'ticketStatus' => '2','ticketReason'=>$ticket_reasion, 'ticketSource'=>$ticket_source];
                        } else {

                            if ($autoreg == "on") {
                                $procOne = DB::select("call sp_create_user_tournament_ticket_external('$tourCode','$tour_id','$user_id',@out1)");
                                $this->updateReason($where=['USER_ID'=> $user_id, 'TOURNAMENT_ID'=> $tour_id], $data=['TICKET_REASON'=> $ticket_reasion,'TICKET_SOURCE'=> $ticket_source]);
                                $outQuery = DB::raw("select @out1 as result");
                                if ($outQuery) {
                                    $res = DB::select('call sp_tournament_registration(' . $tour_id . ',' . $user_id . ',@out1,@out2)');
                                }
                            } else {
                                $res = DB::select("call sp_create_user_tournament_ticket('$tourCode','$tour_id','$user_id')");
                                $this->updateReason($where=['USER_ID'=> $user_id, 'TOURNAMENT_ID'=> $tour_id], $data=['TICKET_REASON'=> $ticket_reasion,'TICKET_SOURCE'=> $ticket_source]);
                            }
                            $ticketStatusResponse[] = ['userId' => $user_id, 'userName' => $user_name, 'tournamentId' => $tour_id, 'tournamentName' => $tourName, 'ticketStatus' => '1','ticketReason'=>$ticket_reasion, 'ticketSource'=>$ticket_source];
                            $email = $this->getUserEmail($user_id);
                            $params['email'] = $email;
                            $params['username'] = $user_name;
                            $params['tempId'] = 17;
                            $params['subject'] = "Tournament Promotional Ticket";
                            $params['tourName'] = $tourName;
                            $params['tourBuyIn'] = $tourBuyIn;
                            $params['tourEntryFee'] = $tourEntryFee;
                            $params['tourStartTime'] = $tourStartTime;
                            $params['tourRegStartTime'] = $tourRegStartTime;
                            $params['tourRegEndTime'] = $tourRegEndTime;
                            //send mail to user after sucessfull tour ticket assign
                            if (!empty($email)) {
                                $this->sendMail($params);
                            }
                            // //track admin activity
                            // $action ="Tournament Ticket assign";
                            // $this->insertAdminActivity($params,$action);
                        }
                    } else {
                        $ticketStatusResponse[] = ['userId' => $user_id, 'userName' => $user_name, 'tournamentId' => $tour_id, 'tournamentName' => $tourName, 'ticketStatus' => '0','ticketReason'=>$ticket_reasion, 'ticketSource'=>$ticket_source];
                    }
                }
            } else {
                $ticketStatusResponse[] = ['userId' => '-', 'userName' => '-', 'tournamentId' => $tour_id, 'tournamentName' => 'Does Not Exist', 'ticketStatus' => '3','ticketReason'=>$ticket_reasion, 'ticketSource'=>$ticket_source];
            }
        }
        return  $ticketStatusResponse;
    }


    public function checkUserNameExist(Request $request)
    {
        $userNameDetails = $request->all();
        $username = $userNameDetails['username'];
        if ($user_id = getUserIdFromUsername($username)) {
            return "exist";
        } else {
            return "notexist";
        }
    }


    public function getUserEmail($userId)
    {
        $email = getEmailFromUserId($userId);
        return $email;
    }

    public function getTournamentGameTypeCode($tournament_id)
    {
        $refCode = DB::table('tournament')->select('tournament.mini_game_type_id', 'tournament.tournament_id', 'tournament.tournament_name', 'tournament.BUYIN', 'tournament.ENTRY_FEE', 'tournament.TOURNAMENT_START_TIME', 'tournament.REGISTER_START_TIME', 'tournament.REGISTER_END_TIME',  'minigames_type.ref_game_code')->join('minigames_type', 'minigames_type.MINIGAMES_TYPE_ID', '=', 'tournament.mini_game_type_id')
            // ->where(['something' => 'something', 'otherThing' => 'otherThing'])
            ->where(['tournament.tournament_id' => $tournament_id])->first();
        return $refCode;
    }

    public function insertAdminActivity($data, $action)
    {
        $activity = [
            'admin_id' => \Auth::user()->id,
            'module_id' => '16',
            'action' => $action,
            'data' => json_encode($data)
        ];
        \PokerBaazi::storeActivity($activity);
    }

    public function updateReason($where, $data){
       return TournamentUserTicket::where($where)->update($data);
    }

    public function matchReason($where){
        return ActionReason::where($where)->active()->first();
    }

    
    public function getReason(){
        return ActionReason::whereRaw('FIND_IN_SET(2,REASON_CATEGORY_ID)')->active()->get();
    }
}
