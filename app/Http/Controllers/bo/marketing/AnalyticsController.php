<?php

namespace App\Http\Controllers\bo\marketing;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Http\Controllers\bo\BaseController;
use App\Models\BranchWebhook;

class AnalyticsController extends BaseController
{
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        
        return view('bo.views.marketing.analytics.index');
    }
   
    public function channelChart(Request $request){
        try {
            DB::enableQueryLog();
            $filterBy = !empty($request->filterBy) ? $request->filterBy : "WEEK";
            $from = !empty($request->from) ? Carbon::parse($request->from)->format('Y-m-d 00:00:00') : Carbon::yesterday()->format('Y-m-d 00:00:00');
            $to = !empty($request->to) ? Carbon::parse($request->to)->format('Y-m-d 23:59:59') : Carbon::yesterday()->format('Y-m-d 23:59:59');
            
            // $query = "SELECT  $filterBy(EVENT_TIMESTAMP), {$filterCal} as xAxis, COUNT(DISTINCT USER_ID), (CASE WHEN attributed = 'true' THEN ATTR_CHANNEL ELSE 'Organic' END ) AS 'CHANNEL' FROM branch_event_tracking WHERE EVENT_TIMESTAMP BETWEEN '{$from}' AND '{$to}' GROUP BY  $filterBy(xAxis),CHANNEL";

            $query =BranchWebhook::query();
            $query->select(DB::raw("$filterBy(EVENT_TIMESTAMP) as `range`"));
            $query->addSelect(DB::raw('COUNT(DISTINCT USER_ID) as `userCount`'));
            $query->addSelect(DB::raw("(CASE WHEN attributed = 'true' THEN ATTR_CHANNEL ELSE 'Organic' END ) AS `CHANNEL`"));
            $query->whereBetween('EVENT_TIMESTAMP', ["$from", "$to"]);
            $query->groupBy('range', 'CHANNEL');
            $chartData = $query->get();
            
            // $data['chartDataByChannel'] = $chartData->groupBy('CHANNEL');
            $data['chartDataByRange'] = $chartData->groupBy('range');
            $data['channels'] =$chartData->keyBy('CHANNEL')->keys()->toArray();
            $data['ranges'] = $data['chartDataByRange']->keys()->toArray();
            
            // $log = DB::getQueryLog();
            // $chartData = \DB::select($query);
            // dd($chartData, $log);
            
            return response()->json(['status'=>200, 'message'=>'success', 'data'=>$data]);
        } catch (\Exception $e) {
            return response()->json(['status'=>500, 'message'=>$e->getMessage()], 500);
        }
    }
    public function campaignChart(Request $request){
        try {
            DB::enableQueryLog();
            $filterBy = !empty($request->filterBy) ? $request->filterBy : "WEEK";
            $from = !empty($request->from) ? Carbon::parse($request->from)->format('Y-m-d 00:00:00') :  Carbon::yesterday()->format('Y-m-d 00:00:00');
            $to = !empty($request->to) ? Carbon::parse($request->to)->format('Y-m-d 23:59:59') : Carbon::yesterday()->format('Y-m-d 23:59:59');
            // $query = "SELECT  $filterBy(EVENT_TIMESTAMP), {$filterCal} as xAxis, COUNT(DISTINCT USER_ID), (CASE WHEN attributed = 'true' THEN ATTR_CHANNEL ELSE 'Organic' END ) AS 'CHANNEL' FROM branch_event_tracking WHERE EVENT_TIMESTAMP BETWEEN '{$from}' AND '{$to}' GROUP BY  $filterBy(xAxis),CHANNEL";

            $query =BranchWebhook::query();
            $query->select('ATTR_CAMPAIGN',DB::raw("$filterBy(EVENT_TIMESTAMP) as `range`"));
            $query->addSelect(DB::raw('COUNT(DISTINCT USER_ID) as `userCount`'));
            $query->whereBetween('EVENT_TIMESTAMP', ["$from", "$to"]);
            $query->where('attributed', 'true');
            $query->groupBy('range', 'ATTR_CAMPAIGN');
            $chartData = $query->get();
            
            // $data['chartDataByCampaign'] = $chartData->groupBy('ATTR_CAMPAIGN');
            $data['chartDataByRange'] = $chartData->groupBy('range');
            $data['compaign'] = $chartData->keyBy('ATTR_CAMPAIGN')->keys()->toArray();
            $data['ranges'] = $data['chartDataByRange']->keys()->toArray();
            // $log = DB::getQueryLog();
            // $chartData = \DB::select($query);
            // dd($chartData, $log);
            
            return response()->json(['status'=>200, 'message'=>'success', 'data'=>$data]);
        } catch (\Exception $e) {
            return response()->json(['status'=>500, 'message'=>$e->getMessage()], 500);
        }
    }
}