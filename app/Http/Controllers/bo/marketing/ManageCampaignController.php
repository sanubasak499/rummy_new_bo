<?php

namespace App\Http\Controllers\bo\marketing;
use App\Http\Controllers\bo\BaseController;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PromoCampaignType;
use App\Models\CampaignToUser;
use App\Models\Partner;
use App\Models\Tournament;
use App\Models\CampaignTournamentMapping;
use App\Models\User;
use App\Models\PromoRule;
use App\Models\BulkPromoCampaign;
use App\Models\CampaignUserSpecific;
use App\Models\PromoCampaign;
use App\Models\PromoStatus;
use App\Imports\UserImport;
use Maatwebsite\Excel\Importer;
use Redirect;
use URL;
use Session;
use Auth;
use stdClass;
use DB;
use Carbon\Carbon;
use Validator;
class ManageCampaignController extends BaseController
{
	public function index(Request $request){
		if($request->has("action") && $request->action == "getUniqueUser"){
			$campaignUserUsed = DB::table((new CampaignToUser)->getTable())
			->select('campaign_to_user.USER_ID','campaign_to_user.UPDATED_DATE','u.USERNAME','u.EMAIL_ID','u.CONTACT')
			->leftjoin('user as u','u.USER_ID',"=",'campaign_to_user.USER_ID')
			->where('campaign_to_user.PROMO_CAMPAIGN_ID',"=",$request->id)
			->groupBy('u.USER_ID')
			->orderBy('campaign_to_user.USER_ID','DESC')
            ->get();
			$unique_user_cam_info = DB::table((new PromoCampaign)->getTable())
			->where('PROMO_CAMPAIGN_ID', $request->id)
			->select('promo_campaign.PROMO_CAMPAIGN_CODE','promo_campaign.PROMO_CAMPAIGN_NAME','promo_campaign.PROMO_CAMPAIGN_DESC','promo_campaign.START_DATE_TIME','promo_campaign.END_DATE_TIME','promo_campaign.PARTNER_ID','promo_campaign.PROMO_CAMPAIGN_TYPE_ID','pct.PROMO_CAMPAIGN_TYPE','p.PARTNER_NAME')
			->leftjoin('promo_campaign_type as pct','pct.PROMO_CAMPAIGN_TYPE_ID',"=",'promo_campaign.PROMO_CAMPAIGN_TYPE_ID')
			->leftjoin('partner as p','p.PARTNER_ID',"=",'promo_campaign.PARTNER_ID')
			->first();
			
			return response()->json(['campaignUserUsed'=>$campaignUserUsed, 'unique_user_cam_info'=>$unique_user_cam_info]);
		}
		
		if($request->has("action") && $request->action == "viewCampaignData"){
			return response()->json($this->viewCampaign($request->id));
		}
		
		$FK_PARTNER_TYPE_ID = Auth::user()->partner->FK_PARTNER_TYPE_ID;
		if($FK_PARTNER_TYPE_ID!=5) {
			$campaignType = PromoCampaignType::where('PROMO_CAMPAIGN_TYPE_ID','!=','')->orderBy('PROMO_CAMPAIGN_TYPE_ID','ASC')->get();
		}else {
			$campaignType = PromoCampaignType::where('PROMO_CAMPAIGN_TYPE_ID',2)->orderBy('PROMO_CAMPAIGN_TYPE_ID','ASC')->get();
		}
		$getPartnerNames = Partner::Select('PARTNER_ID','PARTNER_NAME')->get();
		$getCampaignStatus = PromoStatus::Select('PROMO_STATUS_ID', 'PROMO_STATUS_DESC')->get();
		
		$partnerId = Auth::user()->partner->PARTNER_ID;
		if(!isset($request->is_searched) && empty($request->is_searched)){
			return view('bo.views.marketing.managecampaign.managecampaign',['campaignType'=>$campaignType,'getPartnerNames'=>$getPartnerNames,'getCampaignStatus'=>$getCampaignStatus,'CampaignInfo'=>[],'old'=>[],'page'=>true]);
		}
		
		#get campaign info
		$CampaignInfo = $this->getCampaignInfo($request,$partnerId);
		#get promocampaignid from this campaign
		if($CampaignInfo == " "){
			$campaignUserUsed = 0;
			$CampaignInfo = '';
		}else{
			$resValue=[];
			foreach($CampaignInfo as $index => $campaignDetail) {
				$temp = null;
				$temp = new stdClass();
				$temp->PROMO_CAMPAIGN_ID  = $campaignDetail->PROMO_CAMPAIGN_ID;
				$temp->PROMO_CAMPAIGN_TYPE = $campaignDetail->PROMO_CAMPAIGN_TYPE;
				$temp->PROMO_CAMPAIGN_TYPE_ID = $campaignDetail->PROMO_CAMPAIGN_TYPE_ID;
				$temp->PROMO_CAMPAIGN_CODE = $campaignDetail->PROMO_CAMPAIGN_CODE;
				$temp->PROMO_CAMPAIGN_NAME = $campaignDetail->PROMO_CAMPAIGN_NAME;
				$temp->START_DATE_TIME = $campaignDetail->START_DATE_TIME;
				$temp->END_DATE_TIME = $campaignDetail->END_DATE_TIME;
				$temp->PARTNER_ID = $campaignDetail->PARTNER_ID;
				#get no of clicks users on promo
				$promoUserUsedCount = $this->getCampaignUsedUserCount($campaignDetail->PROMO_CAMPAIGN_ID);
				
				if($promoUserUsedCount!=0){
					
					$temp->noOfClicks = $promoUserUsedCount;
				}else{
					$temp->noOfClicks = "";
				}
				#get campaign user used count all
				if($promoUserUsedCount!=0){
					$campaignUserUsed = $this->getCampaignUsedUsers($campaignDetail->PROMO_CAMPAIGN_ID);
					
					$temp->RegUser = count($campaignUserUsed);
				}else{
					$temp->RegUser = "";
				}
				$temp->PROMO_STATUS_ID = $campaignDetail->PROMO_STATUS_ID;
				$resValue[] = $temp;
			}
			
		}
		
		
		return view('bo.views.marketing.managecampaign.managecampaign',['campaignType'=>$campaignType,'getPartnerNames'=>$getPartnerNames,'getCampaignStatus'=>$getCampaignStatus,'CampaignInfo'=>$resValue,'old'=>($request->all()),'page'=>false]);
		
	}
	public function createCampaign(Request $request){
		
		$getPartnerNames = Partner::Select('PARTNER_ID','PARTNER_NAME')->get();
		$campaignType = PromoCampaignType::orderBy('PROMO_CAMPAIGN_TYPE_ID','ASC')->get();
		$getCampaignStatus = PromoStatus::Select('PROMO_STATUS_ID', 'PROMO_STATUS_DESC')->get();
		$date = date('Y-m-d H:i:s');
		$tournaments = app(Tournament::class)->getAllUpcomingTournaments($date);
		
		return view('bo.views.marketing.managecampaign.createcampaign',['campaignType'=>$campaignType,'getPartnerNames'=>$getPartnerNames,'tournaments'=>$tournaments,]);
	}
	public function addCampaign(Request $request, Importer $importer){
		$validator = Validator::make($request->all(), [
            'campaignType' => 'required',
            'partner_name' => 'required',
            'startdate' => 'required',
            'campaignname' => 'required'
		])->validate();
		
		if($request->campaignfeature == 'on'){
			$campaignfeature = 1;
		}else{
			$campaignfeature = 0;
		}
		if($request->campaignstatus == 'on'){
			$status = 1;
		}else{
			$status = 2;
		}
		
		if($request->campaignfeature == 'on'){
			$Is_featured = 1;
		}else{
			$Is_featured = 0;
		}
		if($request->p_promochips == 'on'){
			$p_promochips = 1;
		}else{
			$p_promochips = 0;
		}
		if($request->p_unclaimedbonus == 'on'){
			$p_unclaimedbonus = 1;
		}else{
			$p_unclaimedbonus = 0;
		}
		if($request->p_tournamenttickets == 'on'){
			$p_tournamenttickets = 1;
		}else{
			$p_tournamenttickets = 0;
		}
		if($request->s_promochips == 'on'){
			$s_promochips = 1;
		}else{
			$s_promochips = 0;
		}
		if($request->s_tournamenttickets == 'on'){
			$s_tournamenttickets = 1;
		}else{
			$s_tournamenttickets = 0;
		}
		if($request->display_status == 'on'){
			$display_status = 1;
		}else{
			$display_status = 0;
		}
		if($request->excel_camp || $request->excel_camp_regist){
			$IS_USER_SPECIFIC = 1;
		}else{
			$IS_USER_SPECIFIC = 0;
		}
		
		if($request->campaign_remark){
			$REMARK = $request->campaign_remark;
		}else{
			$REMARK = NULL;
		}
		if($request->withdrawl_criteria){
			$WITHDRAW_CRITERIA = $request->withdrawl_criteria;
		}else{
			$WITHDRAW_CRITERIA = NULL;
		}
		$startdate = date('Y-m-d H:i:s', strtotime($request->startdate));
		
		if($request->enddate){
			$enddate = date('Y-m-d H:i:s', strtotime($request->enddate));
		}else{
			$enddate = '2099-12-31 23:59:59';
		}
		$PROMO_CAMPAIGN_TYPE_ID = $request->campaignType;
		if($PROMO_CAMPAIGN_TYPE_ID == 1) {
			if($request->p_promochips=="" && $request->p_unclaimedbonus == ""){
				return back()->with('error', "Payment rule settings can not be left blank");
			}
		}
		if($request->expusermin > $request->expusermax){
			return back()->with('error', "User Min Value Cannot be greater than User Max Value.");			
		}
		if($startdate > $enddate)
		{
			return back()->with('error', "Start date can not be greater than end date");	
		}
		if($PROMO_CAMPAIGN_TYPE_ID == 6 || $PROMO_CAMPAIGN_TYPE_ID==8 ) {			
			if($p_promochips=="" && $p_unclaimedbonus==""){
				return back()->with('error', "Payment rule settings can not be left blank.");
			}
		}
		if($PROMO_CAMPAIGN_TYPE_ID==5) {
			if($request->s_promochips == ""){
				return back()->with('error', "Payment rule settings can not be left blank.");
			}
		}
		
		#if tournament ticket is checked for payment and first deposit
		if($PROMO_CAMPAIGN_TYPE_ID==1 || $PROMO_CAMPAIGN_TYPE_ID==6 || $PROMO_CAMPAIGN_TYPE_ID==8){ 
			if($request->m_depositamount==''){ 
				$MINIMUM_DEPOSIT_AMOUNT	= 100;
			}else{ 
				$MINIMUM_DEPOSIT_AMOUNT	= $request->m_depositamount;
			}
			if($request->max_depositamount==''){ 
				$MAXIMUM_DEPOSIT_AMOUNT	= 50000;
			}else{ 
				$MAXIMUM_DEPOSIT_AMOUNT	= $request->max_depositamount;
			}
			if($request->coins_required==''){ 
				$COINS_REQUIRED	= $MAXIMUM_DEPOSIT_AMOUNT;
			}else { 
				$COINS_REQUIRED	= $request->coins_required;
			}
			if($request->p_tournamentticketid != ''){ 
				$tournament = Tournament::where('TOURNAMENT_ID',$request->p_tournamentticketid)->select('TOURNAMENT_NAME','TOURNAMENT_ID')->get();
				$TOURNAMENT_ID = $tournament[0]->TOURNAMENT_ID;
				$TOURNAMENT_NAME = $tournament[0]->TOURNAMENT_NAME;
			}else{
				$TOURNAMENT_ID= NULL;
				$TOURNAMENT_NAME= NULL;
			}
		}
		#unique code and bulk code tournament
		elseif($request->campaignType ==5 || $request->campaignType ==7 ) {
			if($request->s_tournamentticketid != ''){
				$tournament = Tournament::where('TOURNAMENT_ID',$request->s_tournamentticketid)->select('TOURNAMENT_NAME')->get();
				$TOURNAMENT_ID = $request->s_tournamentticketid;
				$TOURNAMENT_NAME = $tournament[0]->TOURNAMENT_NAME;
			}else{
				$TOURNAMENT_ID = 0;
				$TOURNAMENT_NAME = NULL;
			}
		}
		#not for bulk code campaign	test
		if($PROMO_CAMPAIGN_TYPE_ID != 7) { 
			$chkCCodeExistStatus = PromoCampaign::select('PROMO_CAMPAIGN_CODE')->where('PROMO_CAMPAIGN_CODE',$request->campaigncode)->get();
			if(!$chkCCodeExistStatus->isEmpty()) {
				return back()->with('error', "The given campaign code is used already.");
			}
		}
		 
	
		$userName=[];
		$contactResponse=[];
		$PromoCampaign = new PromoCampaign();
			$PromoCampaign->PARTNER_ID          = $request->partner_name;
			$PromoCampaign->PROMO_CAMPAIGN_NAME   = $request->campaignname;
			$PromoCampaign->PROMO_CAMPAIGN_DESC     = $request->campaign_description;
			$PromoCampaign->PROMO_STATUS_ID        = $status;
			$PromoCampaign->START_DATE_TIME       = $startdate;
			$PromoCampaign->END_DATE_TIME  = $enddate;
			$PromoCampaign->COST   = $request->cost;
			$PromoCampaign->TOTAL_USER_EXPECTED    = $request->expecteduser;
			$PromoCampaign->COST_PER_USER    = $request->costperuser;
			$PromoCampaign->EXP_USER_MIN             = $request->expusermin;
			$PromoCampaign->EXP_USER_MAX        	= $request->expusermax; 
			$PromoCampaign->CREATED_ON        = date('Y-m-d 00:00:00');
			$PromoCampaign->STATUS        = $status;
			$PromoCampaign->MONEY_OPTION        = $request->payment_type;
			$PromoCampaign->PROMO_CAMPAIGN_TYPE_ID        = $request->campaignType;
			$PromoCampaign->IS_FEATURED        = $campaignfeature;
			$PromoCampaign->DISPLAY_STATUS        = $display_status;
			
		if($PROMO_CAMPAIGN_TYPE_ID == 7){
			$PromoCampaign->TOTAL_UNIQUE_CODE_COUNT    = $request->noofuniquecode;
			$PromoCampaign->TOURNAMENT_ID        = $TOURNAMENT_ID;
			$PromoCampaign->TOURNAMENT_NAME        = $TOURNAMENT_NAME;
		}else if($PROMO_CAMPAIGN_TYPE_ID==1 || $PROMO_CAMPAIGN_TYPE_ID==6 || 			   $PROMO_CAMPAIGN_TYPE_ID==8){
			$PromoCampaign->PROMO_CAMPAIGN_CODE  = $request->campaigncode;
			$PromoCampaign->MINIMUM_DEPOSIT_AMOUNT        = $MINIMUM_DEPOSIT_AMOUNT;
			$PromoCampaign->MAXIMUM_DEPOSIT_AMOUNT        = $MAXIMUM_DEPOSIT_AMOUNT;
			$PromoCampaign->COINS_REQUIRED        = $COINS_REQUIRED;
			$PromoCampaign->TOURNAMENT_ID        = $TOURNAMENT_ID;
			$PromoCampaign->TOURNAMENT_NAME        = $TOURNAMENT_NAME;
			$PromoCampaign->IS_USER_SPECIFIC        = $IS_USER_SPECIFIC;
			$PromoCampaign->REMARK        = $REMARK;
			$PromoCampaign->WITHDRAW_CRITERIA        = $WITHDRAW_CRITERIA;
		}elseif($PROMO_CAMPAIGN_TYPE_ID == 5){
				$PromoCampaign->PROMO_CAMPAIGN_CODE  = $request->campaigncode;
				$PromoCampaign->TOURNAMENT_ID        = $TOURNAMENT_ID;
				$PromoCampaign->TOURNAMENT_NAME        = $TOURNAMENT_NAME;
				$PromoCampaign->IS_USER_SPECIFIC        = $IS_USER_SPECIFIC;
				$PromoCampaign->REMARK        = $REMARK;
				$PromoCampaign->WITHDRAW_CRITERIA        = $WITHDRAW_CRITERIA;
		}else{
			    $PromoCampaign->PROMO_CAMPAIGN_CODE  = $request->campaigncode;
			    $PromoCampaign->PROMO_CAMPAIGN_URL  = $request->campaignurl;
				$PromoCampaign->IS_USER_SPECIFIC        = $IS_USER_SPECIFIC;
				$PromoCampaign->REMARK        = $REMARK;
				$PromoCampaign->WITHDRAW_CRITERIA        = $WITHDRAW_CRITERIA;
		}	
		
		if($PromoCampaign->save()){
			$add_promo_campaign_id = $PromoCampaign->PROMO_CAMPAIGN_ID;
			#activity tracking
			 \PokerBaazi::storeActivityWithParams($action="Create Campaign", json_encode($PromoCampaign), $module_id="NULL");
			#End activity tracking
			
			
			if($request->excel_camp != ''){
				#code of users excel when creating campaign
				$importer->import($data = new UserImport, $request->excel_camp);
				$campaign_list_excel = $data->data;
				foreach($campaign_list_excel as $campaign_list_users){
					if(isset($campaign_list_users['Username']) && !empty($campaign_list_users['Username'])){
						$userName = $campaign_list_users['Username'];
					}
				}
				$userExcelData = $this->createCampaignUserRegisterExcel($add_promo_campaign_id, $campaign_list_excel);
		
			 }elseif($request->excel_camp_regist){
				 #code of contacts excel when creating campaign
				$importer->import($data = new UserImport, $request->excel_camp_regist);
				$campaign_list_excel = $data->data;
				$tempContact = [];
				if(isset($campaign_list_excel) && !empty($campaign_list_excel)){
					$dataArray=[];
					foreach($campaign_list_excel as $campaign_list_contact){
						$tempContactVal = $userContact = '';
						$userContact = trim($campaign_list_contact['Contact']);
						if($userContact == "" || $userContact == null){
							continue;
						}
						$tempContactVal = $campaign_list_contact['Contact'];
						$userContact = filter_var($userContact, FILTER_SANITIZE_NUMBER_INT);
						$alreadyAvailable = CampaignUserSpecific::where("PROMO_CAMPAIGN_ID", $add_promo_campaign_id)->where('REG_NUMBERS', $userContact)->count();
						
						if($userContact == "" || $userContact == null ){
							$tempContact[]= $tempContactVal;
							$contactResponse[] = ['userContact' => $userContact];
						}elseif(isset($alreadyAvailable) && !empty($alreadyAvailable) && $alreadyAvailable > 0 ){
							$tempContact[]= $tempContactVal;
							$contactResponse[] = ['userContact' => $userContact];
						}else{
							if(isset($tempContactVal) && (strlen($tempContactVal) == "10")){
							$userContact= $userContact;
							$dataObj=[];
							$dataObj['STATUS']=1;
							$dataObj['PROMO_CAMPAIGN_ID']=$add_promo_campaign_id;
							$dataObj['CREATED_DATE']=date('Y-m-d H:i:s');
							$dataObj['REG_NUMBERS']=$userContact;
							$data = CampaignUserSpecific::insert($dataObj);
							$contactResponse[] = ['userContact' => $userContact];
						}
						}//else close
							
					}//foreach close
				}
			
				$val = CampaignUserSpecific::where('REG_NUMBERS',$userContact)->where('STATUS',0)->select('STATUS','CAMPAIGN_USER_SPECIFIC_ID')->get();
					
				foreach($val as $index => $value){
					if($value->STATUS == 0){
						$output = CampaignUserSpecific::where('CAMPAIGN_USER_SPECIFIC_ID',$value->CAMPAIGN_USER_SPECIFIC_ID)->update(['STATUS'=>2]);
					}
				}
			}
		}
		
		if($add_promo_campaign_id != ''){
			if($request->campaignType == 1 || $request->campaignType == 6 || $request->campaignType ==8){
				#for adding multicampaign
					if($request->p_tournamentticketid != ''){
						$multiTourTickets=$request->p_tournamentticketid;
						foreach($multiTourTickets as $multiValue){
							$multiTourTicketsNames = Tournament::where('TOURNAMENT_ID',$multiValue)->select('TOURNAMENT_NAME')->get();
							$TOURNAMENT_NAME = $multiTourTicketsNames->toArray()[0]["TOURNAMENT_NAME" ];
							$addMultiCampaign = new CampaignTournamentMapping();
							$addMultiCampaign->PROMO_CAMPAIGN_ID = $add_promo_campaign_id;
							$addMultiCampaign->TOURNAMENT_ID= $multiValue;
							$addMultiCampaign->TOURNAMENT_NAME = $TOURNAMENT_NAME;
							$addMultiCampaign->CREATED_DATE = date('Y-m-d H:i:s');
							$addMultiCamp = $addMultiCampaign->save();
							#activity tracking
							 \PokerBaazi::storeActivityWithParams($action="Create Campaign", json_encode($addMultiCamp), $module_id="NULL");
							#End activity tracking
						}
					}
				}
				
				if($request->campaignType == 3){
					$rafUserName 	= ""; //no need for Tell a friend campaign
					$rafPercentage 	= $request->promovalue1;   
				}else{
					$rafUserName 	= $request->ref_username;
					$rafPercentage 	= $request->ref_precentage;
				}
				if($request->p_tournamentticketid != ''){
					$P_TOURNAMENTS_TICKET_ID= $request->p_tournamentticketid;
					$P_TOURNAMENT_TICKET_ID= $P_TOURNAMENTS_TICKET_ID[0];
				}else{
					$P_TOURNAMENT_TICKET_ID = NULL;
				}
				$PROMO_PAYMENT_TYPE_ID  = $request->payment_type;
				if($request->promotype != ''){				 				
					$PROMO_TYPE_ID        	= $request->promotype;
				}else {
					$PROMO_TYPE_ID        	= 1;
				}
				$addCampaign = new PromoRule();
					$addCampaign->PROMO_CAMPAIGN_ID = $add_promo_campaign_id;
					$addCampaign->PROMO_PAYMENT_TYPE_ID = $PROMO_PAYMENT_TYPE_ID;
					$addCampaign->PROMO_TYPE_ID = $PROMO_TYPE_ID;
					$addCampaign->PROMO_MIN_VALUE = $request->promominvalue;
					$addCampaign->PROMO_MAX_VALUE= $request->promomaxvalue;
					$addCampaign->PROMO_VALUE1   =$request->promovalue1;
					$addCampaign->PROMO_VALUE2 = $request->promovalue2;
					$addCampaign->P_PROMO_CHIPS = $p_promochips;
					$addCampaign->P_PROMO_VALUE = $request->p_promovalue;
					$addCampaign->P_PROMO_CAP_VALUE = $request->p_promocapvalue;
					$addCampaign->P_UNCLAIMED_BONUS = $p_unclaimedbonus;
					$addCampaign->P_CREDIT_DEBITCARD_VALUE = $request->p_creditdebitcardvalue;
					$addCampaign->P_NETBANKING_VALUE = $request->p_netbankingvalue;
					$addCampaign->P_BONUS_CAP_VALUE = $request->p_bonuscapvalue;
					$addCampaign->P_TOURNAMENT_TICKETS = $p_tournamenttickets;
					$addCampaign->P_TOURNAMENT_TICKET_ID= $P_TOURNAMENT_TICKET_ID;
					$addCampaign->S_PROMO_CHIPS =$s_promochips;
					$addCampaign->S_PROMO_CHIP_VALUE = $request->s_promovalue;
					$addCampaign->S_TOURNAMENT_TICKETS  = $s_tournamenttickets;
					$addCampaign->S_TOURNAMENT_TICKET_ID  = $request->s_tournamentticketid;	
					$addCampaign->REFERRAL_USERNAME = $rafUserName;	
					$addCampaign->RAF_PERCENTAGE = $rafPercentage;
					if($addCampaign->save()){
						$addCampaignPromoRule = $addCampaign->PROMO_RULE_ID;
					}
		
				#for bulk code
				if($request->campaignType == 7){
					$PROMO_CAMPAIGN_CODE	= "Promo_".$request->noOfUniqueCode;
					$TOTAL_UNIQUE_CODE = $request->noOfUniqueCode;
					if($request->s_promovalue !=''){
						$PROMO_CAMPAIGN_VALUE = $request->s_promovalue;
					}else{
						$PROMO_CAMPAIGN_VALUE = 0;
					}
					
					for($i=1;$i<=$TOTAL_UNIQUE_CODE;$i++){
						$BULK_UNIQUE_CODE = 'BL'.$this->generate_bulk_unique_code(8);
					
						$dataArray=[];
						$dataObj['PROMO_CAMPAIGN_NAME']=$request->campaignname;
						$dataObj['PROMO_CAMPAIGN_ID']=$add_promo_campaign_id;
						$dataObj['PROMO_CAMPAIGN_CODE']=$PROMO_CAMPAIGN_CODE;
						$dataObj['BULK_UNIQUE_CODE']=$BULK_UNIQUE_CODE;
						$dataObj['PROMO_CAMPAIGN_VALUE']=$PROMO_CAMPAIGN_VALUE;
						$dataObj['CREATED_DATE']=date('Y-m-d H:i:s');
						$dataObj['STATUS']=1;
						$dataArray[]=$dataObj;
						$data = BulkPromoCampaign::insert($dataArray);
					}
					
				}
				if($addCampaignPromoRule != ''){
					if($request->excel_camp_regist){
						return back()->with(['success'=>'Campaign is added successfully.',
						'excelContactData'=>$contactResponse
						]);
					}elseif($request->excel_camp){
						return back()->with(['success'=>'Campaign is added successfully.',
						'exceldata'=>$userExcelData
						]);
					}else{
						return back()->with(['success'=>'Campaign is added successfully.'
						]);
					}
				}
		}else{
				return back()->with('error', 'Record Not Insert');
		}
	}
	public function createCampaignUserRegisterExcel($add_promo_campaign_id, $campaign_list_excel){
		$dataArray=[];
		
		foreach($campaign_list_excel as $campaign_list_users){
			
		$userName = '';
		if(isset($campaign_list_users['Username']) && !empty($campaign_list_users['Username'])){
			$userName = $campaign_list_users['Username'];
		}
		$user_id = getUserIdFromUsername($campaign_list_users['Username']);

		$alreadyAvailable = app(CampaignUserSpecific::class)->chkUserAlreadyExist($user_id, $add_promo_campaign_id);
		
		$chkInvalidUserEntry = User::where('USER_ID', $user_id)->count();
		
			if(isset($alreadyAvailable) && !empty($alreadyAvailable) && $alreadyAvailable > 0 ){
				$user_id = $user_id;
				$userStatusResponse[] = ['USER_ID' => $user_id, 'userName' => $userName, 'STATUS' => '0'];
			}elseif($chkInvalidUserEntry <= 0){
				$user_id = $user_id;
				$userStatusResponse[] = ['USER_ID' => $user_id, 'userName' => $userName, 'STATUS' => '2'];
			}else{
				$dataArray=[];
				$dataObj['USER_ID']=$user_id;
				$dataObj['STATUS']=1;
				$dataObj['PROMO_CAMPAIGN_ID']=$add_promo_campaign_id;
				$dataObj['CREATED_DATE']=date('Y-m-d H:i:s');
				$dataArray[]=$dataObj;
				
				$data = CampaignUserSpecific::insert($dataArray);
				$userStatusResponse[] = ['USER_ID' => $user_id, 'userName' => $userName, 'STATUS' => '1'];
			}
		}//Loop close
		return $userStatusResponse;
	}
	public function editCampaign(Request $request, $id){
		
		$promo_campaignID = decrypt($id);
		$getCampaignStatus = PromoStatus::Select('PROMO_STATUS_ID', 'PROMO_STATUS_DESC')->get();
		$date = date('Y-m-d H:i:s');
		$tournaments = app(Tournament::class)->getAllUpcomingTournaments($date);
		$getdataEditCampaign = $this->getEditCampaignData($promo_campaignID);
		
		$campaigntypeName = PromoCampaignType::where('PROMO_CAMPAIGN_TYPE_ID',$getdataEditCampaign[0]->PROMO_CAMPAIGN_TYPE_ID)->select('PROMO_CAMPAIGN_TYPE')->get();
		
		$partnerName = Partner::where('PARTNER_ID',$getdataEditCampaign[0]->PARTNER_ID)->select('PARTNER_NAME')->get();
		$getTournamentId = $this->editCampaignTournamentId($promo_campaignID);
		
		return view('bo.views.marketing.managecampaign.editcampaign',['partnerName'=>$partnerName,'tournaments'=>$tournaments,'promo_campaignID'=>$promo_campaignID,'getTournamentId'=>$getTournamentId,'campaigntypeName'=>$campaigntypeName,'getdataEditCampaign'=>$getdataEditCampaign[0]]); 
		
	}
	public function updateCampaign(Request $request){
		//dd($request->all());
		if($request->frmsubmitedit == 'Update'){
			
			$campaignID = $request->campaignTypeid;
			$PROMO_CAMPAIGN_ID = $request->promo_campaignid_edit;
			if($request->campaignstatus == 'on'){
				$status = 1;
			}else{
				$status = 2;
			}
			if($request->display_status == 'on'){
			$display_status = 1;
			}else{
				$display_status = 0;
			}
			if(isset($request->campaignfeature) && $request->campaignfeature == 'on'){
				$campaignfeature = 1;
			}else{
				$campaignfeature = 0;
			}
			if($request->p_promochips == 'on'){
				$p_promochips = 1;
			}else{
				$p_promochips = 0;
			}
			if($request->p_unclaimedbonus == 'on'){
				$p_unclaimedbonus = 1;
			}else{
				$p_unclaimedbonus = 0;
			}
			if($request->p_tournamenttickets == 'on'){
				$p_tournamenttickets = 1;
			}else{
				$p_tournamenttickets = 0;
			}
			if($request->s_promochips == 'on'){
				$s_promochips = 1;
			}else{
				$s_promochips = 0;
			}
			if($request->s_tournamenttickets == 'on'){
				$s_tournamenttickets = 1;
			}else{
				$s_tournamenttickets = 0;
			}
			if($request->campaign_remark){
				$REMARK = $request->campaign_remark;
			}else{
				$REMARK = NULL;
			}
			if($request->withdrawl_criteria){
				$WITHDRAW_CRITERIA = $request->withdrawl_criteria;
			}else{
				$WITHDRAW_CRITERIA = NULL;
			}
		$CODE_VAL = PromoCampaign::where('PROMO_CAMPAIGN_ID',$PROMO_CAMPAIGN_ID)->select('IS_USER_SPECIFIC','TOTAL_UNIQUE_CODE_COUNT')->first();
		if($CODE_VAL->IS_USER_SPECIFIC == ''){
			$IS_USER_SPECIFIC = 0;
		}else{
			$IS_USER_SPECIFIC = $CODE_VAL->IS_USER_SPECIFIC; 
		}
		if($CODE_VAL->TOTAL_UNIQUE_CODE_COUNT == ''){
			$TOTAL_UNIQUE_CODE_COUNT = '';
		}else{
			$TOTAL_UNIQUE_CODE_COUNT = $CODE_VAL->TOTAL_UNIQUE_CODE_COUNT; 
		}
		$startdate = date('Y-m-d H:i:s', strtotime($request->startdate));
		$enddate = date('Y-m-d H:i:s', strtotime($request->enddate));
		
		$PROMO_CAMPAIGN_TYPE_ID = $request->campaignTypeid;
		
		if($PROMO_CAMPAIGN_TYPE_ID == 1) {
			if($request->p_promochips=="" && $request->p_unclaimedbonus == ""){
				return back()->with('error', "Payment rule settings can not be left blank");				
			}
		}
		if($request->expusermin > $request->expusermax){
			return back()->with('error', "User Min Value Cannot be greater than User Max Value.");			
		}
		if($startdate > $enddate)
		{
			return back()->with('error', "Start date Cannot be greater than end date.");	
		}
		if($PROMO_CAMPAIGN_TYPE_ID == 6 || $PROMO_CAMPAIGN_TYPE_ID==8 ) {			
			if($p_promochips=="" && $p_unclaimedbonus==""){
				return back()->with('error', "Payment rule settings can not be left blank.");
			}
		}
		if($PROMO_CAMPAIGN_TYPE_ID==5) {
			if($request->s_promochips == ""){
				return back()->with('error', "Payment rule settings can not be left blank.");
			}
		}
		
		#if tournament ticket is checked for payment and first deposit
		if($PROMO_CAMPAIGN_TYPE_ID==1 || $PROMO_CAMPAIGN_TYPE_ID==6 || $PROMO_CAMPAIGN_TYPE_ID==8){ 
			if($request->m_depositamount==''){ 
				$MINIMUM_DEPOSIT_AMOUNT	= 100;
			}else{ 
				$MINIMUM_DEPOSIT_AMOUNT	= $request->m_depositamount;
			}
			if($request->max_depositamount==''){ 
				$MAXIMUM_DEPOSIT_AMOUNT	= 50000;
			}else{ 
				$MAXIMUM_DEPOSIT_AMOUNT	= $request->max_depositamount;
			}
			if($request->coins_required==''){ 
				$COINS_REQUIRED	= $MAXIMUM_DEPOSIT_AMOUNT;
			}else { 
				$COINS_REQUIRED	= $request->coins_required;
			}
			
			if($request->p_tournamentticketid != ''){ 
			
				$tournament = Tournament::where('TOURNAMENT_ID',$request->p_tournamentticketid)->select('TOURNAMENT_NAME')->get();
				
				$TOURNAMENT_NAME = $tournament[0]->TOURNAMENT_NAME;
				$GET_TOURNAMENT_ID = Tournament::where('TOURNAMENT_NAME',$TOURNAMENT_NAME)->select('TOURNAMENT_ID')->get();
				$TOURNAMENT_ID = $GET_TOURNAMENT_ID[0]->TOURNAMENT_ID;
				
			}else{
				$TOURNAMENT_ID= NULL;
				$TOURNAMENT_NAME= NULL;
			}
			
		}
		
		#unique code and bulk code tournament
		elseif($request->campaignTypeid ==5 || $request->campaignTypeid ==7 ) {
			if($request->s_tournamentticketid != ''){
				$tournament = Tournament::where('TOURNAMENT_ID',$request->s_tournamentticketid)->select('TOURNAMENT_NAME')->get();
				$TOURNAMENT_ID = $request->s_tournamentticketid;
				$TOURNAMENT_NAME = $tournament[0]->TOURNAMENT_NAME;
			}else{
				$TOURNAMENT_ID = 0;
				$TOURNAMENT_NAME = NULL;
			}
			
		}
		$PromoCampaign = PromoCampaign::find($PROMO_CAMPAIGN_ID);
			$PromoCampaign->PROMO_CAMPAIGN_DESC     = $request->campaign_description;
			$PromoCampaign->PROMO_CAMPAIGN_NAME     = $request->campaignname;
			$PromoCampaign->PROMO_STATUS_ID        = $status;
			$PromoCampaign->START_DATE_TIME       = $startdate;
			$PromoCampaign->END_DATE_TIME  = $enddate;
			$PromoCampaign->COST   = $request->cost;
			$PromoCampaign->TOTAL_USER_EXPECTED    = $request->expecteduser;
			$PromoCampaign->COST_PER_USER    = $request->costperuser;
			$PromoCampaign->TOTAL_UNIQUE_CODE_COUNT    = $TOTAL_UNIQUE_CODE_COUNT;
			$PromoCampaign->EXP_USER_MIN             = $request->expusermin;
			$PromoCampaign->EXP_USER_MAX        	= $request->expusermax; 
			$PromoCampaign->CREATED_ON        = date('Y-m-d H:i:s');
			//$PromoCampaign->STATUS        = $status;
			$PromoCampaign->MONEY_OPTION        = $request->payment_type;
			$PromoCampaign->PROMO_CAMPAIGN_TYPE_ID        = $request->campaignTypeid;
			$PromoCampaign->IS_FEATURED        = $campaignfeature;
			$PromoCampaign->DISPLAY_STATUS        = $display_status;
		if($PROMO_CAMPAIGN_TYPE_ID == 7){
			$PromoCampaign->TOURNAMENT_ID        = $TOURNAMENT_ID;
			$PromoCampaign->TOURNAMENT_NAME        = $TOURNAMENT_NAME;
		}else if($PROMO_CAMPAIGN_TYPE_ID==1 || $PROMO_CAMPAIGN_TYPE_ID==6 || 			   $PROMO_CAMPAIGN_TYPE_ID==8){
				$PromoCampaign->MINIMUM_DEPOSIT_AMOUNT        = $MINIMUM_DEPOSIT_AMOUNT;
				$PromoCampaign->MAXIMUM_DEPOSIT_AMOUNT        = $MAXIMUM_DEPOSIT_AMOUNT;
				$PromoCampaign->COINS_REQUIRED        = $COINS_REQUIRED;
				$PromoCampaign->TOURNAMENT_ID        = $TOURNAMENT_ID;
				$PromoCampaign->TOURNAMENT_NAME        = $TOURNAMENT_NAME;
				$PromoCampaign->IS_USER_SPECIFIC        = $IS_USER_SPECIFIC;
				$PromoCampaign->REMARK        = $REMARK;
				$PromoCampaign->WITHDRAW_CRITERIA        = $WITHDRAW_CRITERIA;
		}elseif($PROMO_CAMPAIGN_TYPE_ID == 5){
				$PromoCampaign->TOURNAMENT_ID        = $TOURNAMENT_ID;
				$PromoCampaign->TOURNAMENT_NAME        = $TOURNAMENT_NAME;
				$PromoCampaign->IS_USER_SPECIFIC        = $IS_USER_SPECIFIC;
				$PromoCampaign->REMARK        = $REMARK;
				$PromoCampaign->WITHDRAW_CRITERIA        = $WITHDRAW_CRITERIA;
		}else{
				$PromoCampaign->IS_USER_SPECIFIC        = $IS_USER_SPECIFIC;
				$PromoCampaign->REMARK        = $REMARK;
				$PromoCampaign->WITHDRAW_CRITERIA        = $WITHDRAW_CRITERIA;
		}
		if($PromoCampaign->save()){
			$add_promo_campaign_id = $PromoCampaign->PROMO_CAMPAIGN_ID;
			#activity tracking
			 \PokerBaazi::storeActivityWithParams($action="Update Campaign", json_encode($PromoCampaign), $module_id="NULL");
			#End activity tracking
		}
		if($request->campaignTypeid == 1 || $request->campaignTypeid == 6 || $request->campaignTypeid ==8){
				#for delete multicampaign
				if($request->p_tournamentticketid!=''){
					$resultDLT = CampaignTournamentMapping::where('PROMO_CAMPAIGN_ID',$PROMO_CAMPAIGN_ID)->delete();
					#for adding multicampaign
					if($resultDLT || $request->p_tournamentticketid!=''){
						$multiTourUTickets=$request->p_tournamentticketid;
						foreach($multiTourUTickets as $multiUvalue){
						$multiTourTicketsNames = Tournament::where('TOURNAMENT_ID',$multiUvalue)->select('TOURNAMENT_NAME')->get();
						$TOURNAMENT_NAME = $multiTourTicketsNames->toArray()[0]["TOURNAMENT_NAME" ];
						$addMultiCampaign = new CampaignTournamentMapping();
							$addMultiCampaign->PROMO_CAMPAIGN_ID = $PROMO_CAMPAIGN_ID;
							$addMultiCampaign->TOURNAMENT_ID = $multiUvalue;
							$addMultiCampaign->TOURNAMENT_NAME = $TOURNAMENT_NAME;
							$addMultiCampaign->CREATED_DATE = date('Y-m-d H:i:s');
							$addMultiCamp = $addMultiCampaign->save();
						}
					}
				}
		}
			$updateCampaignRule = 1;
			if($add_promo_campaign_id != '' || $updateCampaignRule==1){
				if($request->campaignTypeid == 3){
					$rafUserName 	= ""; //no need for Tell a friend campaign
					$rafPercentage 	= $request->promovalue1;   
				}else{
					$rafUserName 	= $request->ref_username;
					$rafPercentage 	= $request->ref_precentage;
				}
				
				if($request->p_tournamentticketid != ''){
					$P_TOURNAMENT_TICKET_ID= $request->p_tournamentticketid;
				}else{
					$P_TOURNAMENT_TICKET_ID = NULL;
				}
				$PROMO_PAYMENT_TYPE_ID  = $request->paymenttype;
				if($request->promotype != '' || $request->promotype !=0){				 				
					$PROMO_TYPE_ID        	= $request->promotype;
				}else{
					$PROMO_TYPE_ID        	= 1;
				}

				
				$promo_id = PromoRule::where('PROMO_CAMPAIGN_ID',$PROMO_CAMPAIGN_ID)->select('PROMO_RULE_ID')->first();
				if($promo_id != ''){
				   $addCampaign = PromoRule::find($promo_id->PROMO_RULE_ID);
					$addCampaign->PROMO_CAMPAIGN_ID = $PROMO_CAMPAIGN_ID;
					$addCampaign->PROMO_PAYMENT_TYPE_ID = $PROMO_PAYMENT_TYPE_ID;
					$addCampaign->PROMO_TYPE_ID = $PROMO_TYPE_ID;
					$addCampaign->PROMO_MIN_VALUE = $request->promominvalue;
					$addCampaign->PROMO_MAX_VALUE= $request->promomaxvalue;
					$addCampaign->PROMO_VALUE1   =$request->promovalue1;
					$addCampaign->PROMO_VALUE2 = $request->promovalue2;
					$addCampaign->P_PROMO_CHIPS = $p_promochips;
					$addCampaign->P_PROMO_VALUE = $request->p_promovalue;
					$addCampaign->P_PROMO_CAP_VALUE = $request->p_promocapvalue;
					$addCampaign->P_UNCLAIMED_BONUS = $p_unclaimedbonus;
					$addCampaign->P_CREDIT_DEBITCARD_VALUE = $request->p_creditdebitcardvalue;
					$addCampaign->P_NETBANKING_VALUE = $request->p_netbankingvalue;
					$addCampaign->P_BONUS_CAP_VALUE = $request->p_bonuscapvalue;
					$addCampaign->P_TOURNAMENT_TICKETS = $p_tournamenttickets;
					$addCampaign->P_TOURNAMENT_TICKET_ID= $P_TOURNAMENT_TICKET_ID;
					$addCampaign->S_PROMO_CHIPS = $s_promochips;
					$addCampaign->S_PROMO_CHIP_VALUE = $request->s_promovalue;
					$addCampaign->S_TOURNAMENT_TICKETS  = $s_tournamenttickets;
					$addCampaign->S_TOURNAMENT_TICKET_ID  = $request->s_tournamentticketid;	
					$addCampaign->REFERRAL_USERNAME = $rafUserName;	
					$addCampaign->RAF_PERCENTAGE = $rafPercentage;
					if($addCampaign->save()){
						$addCampaignPromoRule = $addCampaign->PROMO_RULE_ID;
					}
				}
				return back()->with('success', 'Campaign modified successfully');
				
			}else{
				return back()->with('error', 'Campaign not modified');
			}
		}
	}
	public function generate_bulk_unique_code($length = 8) {
		$alphabets = range('A','Z');
		$numbers = range('0','9');
		$final_array = array_merge($alphabets,$numbers);

		$password = '';

		while($length--) {
		  $key = array_rand($final_array);
		  $password .= $final_array[$key];
		}
		return $password;
   }
	private function getCampaignFilterData($request,$partnerId){
		$campaigntype = $request->campaigntype;
		$campaigncode = $request->campaigncode;
		$campaignname = $request->campaignname;
		$campaignstatus = $request->campaignstatus;
		$startdate = $request->startdate;
		$enddate = $request->enddate;
		if($partnerId != '10001'){
			$getpartnerChildId = Partner::Select('PARTNER_ID')->where('FK_PARTNER_ID',$partnerId)->get();
			$search_campaign = PromoCampaign::Select('promo_campaign.*','t2.PROMO_CAMPAIGN_TYPE','t3.PROMO_STATUS_DESC')
			->leftjoin('promo_campaign_type as t2','t2.PROMO_CAMPAIGN_TYPE_ID', '=', 'promo_campaign.PROMO_CAMPAIGN_TYPE_ID')
			->leftjoin('promo_status as t3','t3.PROMO_STATUS_ID', '=', 'promo_campaign.STATUS')
			->where('promo_campaign.PROMO_STATUS_ID',$getpartnerChildId)->get()->toArray();
		}else{
			 $search_campaign = PromoCampaign::Select('promo_campaign.*','t2.PROMO_CAMPAIGN_TYPE','t3.PROMO_STATUS_DESC')
			->leftjoin('promo_campaign_type as t2','t2.PROMO_CAMPAIGN_TYPE_ID', '=', 'promo_campaign.PROMO_CAMPAIGN_TYPE_ID')
			->leftjoin('promo_status as t3','t3.PROMO_STATUS_ID', '=', 'promo_campaign.STATUS')
			->whereNotNull('promo_campaign.PROMO_STATUS_ID')
			->when(!empty($campaigntype), function($query) use ($campaigntype){
                        return $query->where('promo_campaign.PROMO_CAMPAIGN_TYPE_ID', $campaigntype);
                    })
			->when(!empty($campaigncode), function($query) use ($campaigncode){
					return $query->where('PROMO_CAMPAIGN_CODE', $campaigncode);
                    })
			->when(!empty($campaignname), function($query) use ($campaignname){
					return $query->where('PROMO_CAMPAIGN_NAME', $campaignname);
                    })
			 ->when(!empty($campaignstatus), function($query) use ($campaignstatus){
					return $query->where('promo_campaign.PROMO_STATUS_ID', $campaignstatus);
                    })
			->when(!empty($startdate), function($query) use ($startdate){
				$startdate = date('Y-m-d',strtotime( $startdate));
					return $query->whereDate('promo_campaign.START_DATE_TIME', ">=", $startdate);
             })
			->when(!empty($enddate), function($query) use ($enddate){
					$enddate = date('Y-m-d',strtotime( $enddate));
					return $query->whereDate('promo_campaign.END_DATE_TIME', ">=", $enddate);
			})
			->get();
			return $search_campaign;
		}
	}
	private function getCampaignInfo($request,$partnerId){
		
		#update query for make deactivate status of campaign which date is expired 
		$yes_date = date('Y-m-d',strtotime("-1 days"));
		$change_status = PromoCampaign::whereDate('END_DATE_TIME','<=', $yes_date)->update(['PROMO_STATUS_ID'=>2]);
		
		$campaigntype = $request->CampaignType;
		$campaigncode = $request->campaigncode;
		$campaignname = $request->campaignname;
		$campaignstatus = $request->campaignstatus;
		$startdate = $request->startdate;
		$partner_name = $request->partner_name;
		$enddate = $request->enddate;
		$perPage = config('poker_config.paginate.per_page');
		$getpartnerChildId = Partner::Select('PARTNER_ID')->where('FK_PARTNER_ID',$partnerId)->get();
		if($partnerId!=10001) {
			$CampaignInfo = PromoCampaign::Select('promo_campaign.*','t2.PROMO_CAMPAIGN_TYPE','t3.PROMO_STATUS_DESC')
			->leftjoin('promo_campaign_type as t2','t2.PROMO_CAMPAIGN_TYPE_ID', '=', 'promo_campaign.PROMO_CAMPAIGN_TYPE_ID')
			->leftjoin('promo_status as t3','t3.PROMO_STATUS_ID', '=', 'promo_campaign.STATUS')
			->where('promo_campaign.PARTNER_ID',$getpartnerChildId)
			->orderBy('PROMO_CAMPAIGN_ID','ASC')->limit($perPage)->get();
		}else{
			$CampaignInfo = PromoCampaign::Select('promo_campaign.*','t2.PROMO_CAMPAIGN_TYPE','t3.PROMO_STATUS_DESC')
			->leftjoin('promo_campaign_type as t2','t2.PROMO_CAMPAIGN_TYPE_ID', '=', 'promo_campaign.PROMO_CAMPAIGN_TYPE_ID')
			->leftjoin('promo_status as t3','t3.PROMO_STATUS_ID', '=', 'promo_campaign.STATUS')
			->whereNotNull('promo_campaign.PROMO_STATUS_ID')
			->where('promo_campaign.STATUS','!=',2)
			->when(!empty($campaigntype), function($query) use ($campaigntype){
                        return $query->where('promo_campaign.PROMO_CAMPAIGN_TYPE_ID', $campaigntype);
                    })
			->when(!empty($campaigncode), function($query) use ($campaigncode){
					return $query->where('PROMO_CAMPAIGN_CODE','LIKE', "%{$campaigncode}%");
                    })
			->when(!empty($campaignname), function($query) use ($campaignname){
					return $query->where('PROMO_CAMPAIGN_NAME','LIKE', "%{$campaignname}%");
                    })
			 ->when(!empty($campaignstatus), function($query) use ($campaignstatus){
					return $query->where('promo_campaign.PROMO_STATUS_ID', $campaignstatus);
                    })
			->when(!empty($startdate), function($query) use ($startdate){
				$startdate = date('Y-m-d',strtotime( $startdate));
					return $query->whereDate('promo_campaign.START_DATE_TIME', ">=", $startdate);
             })
			->when(!empty($enddate), function($query) use ($enddate){
					$enddate = date('Y-m-d',strtotime( $enddate));
					return $query->whereDate('promo_campaign.END_DATE_TIME', "<=", $enddate);
			})
			->when(!empty($partner_name), function($query) use ($partner_name){
				return $query->where('PARTNER_ID', $partner_name);
			})
			->orderBy('PROMO_CAMPAIGN_ID','DESC')->limit($perPage)->get();
			return $CampaignInfo;
		
		}
	}
	private function getCampaignUsedUserCount($campaignID){
		return CampaignToUser::Select('PROMO_CAMPAIGN_ID')->where('PROMO_CAMPAIGN_ID',$campaignID)->get()->count();
	}
	public function getCampaignUsedUsers($campaignID){
		return $campaignUserUsed = DB::table((new CampaignToUser)->getTable())
			->select('campaign_to_user.USER_ID','u.USERNAME')
			->leftjoin('user as u','u.USER_ID',"=",'campaign_to_user.USER_ID')
			->where('campaign_to_user.PROMO_CAMPAIGN_ID',"=",$campaignID)
			->groupBy('u.USER_ID')
			->orderBy('campaign_to_user.USER_ID','DESC')
            ->get();
	}
	public function viewCampaign($campaignID){
		$promo_campaign_data_get = PromoCampaign::Select('promo_campaign.*','t2.PROMO_CAMPAIGN_TYPE','t3.PARTNER_NAME','t4.*')
		->leftjoin('promo_campaign_type as t2','t2.PROMO_CAMPAIGN_TYPE_ID', "=" ,'promo_campaign.PROMO_CAMPAIGN_TYPE_ID')
		->leftjoin('partner as t3','t3.PARTNER_ID', "=" ,'promo_campaign.PARTNER_ID')
		->leftjoin('promo_rule as t4','t4.PROMO_CAMPAIGN_ID', "=" ,'promo_campaign.PROMO_CAMPAIGN_ID')
		->where('promo_campaign.PROMO_CAMPAIGN_ID', "=" ,$campaignID)->get();
		$campaignViewData = array();
		foreach($promo_campaign_data_get as $key =>$promo_campaign_data_get)
		//$tempdata = null;
		$tempdata = new stdClass();
		$tempdata->PROMO_CAMPAIGN_TYPE = $promo_campaign_data_get->PROMO_CAMPAIGN_TYPE;
		$tempdata->PARTNER_NAME = $promo_campaign_data_get->PARTNER_NAME;
		$tempdata->PROMO_CAMPAIGN_NAME = $promo_campaign_data_get->PROMO_CAMPAIGN_NAME;
		$tempdata->PROMO_CAMPAIGN_CODE = $promo_campaign_data_get->PROMO_CAMPAIGN_CODE;
		$tempdata->PROMO_CAMPAIGN_URL = $promo_campaign_data_get->PROMO_CAMPAIGN_URL;
		if($promo_campaign_data_get->PROMO_STATUS_ID == 1){
			$tempdata->PROMO_STATUS_ID = "Active";
		}else{
			$tempdata->PROMO_STATUS_ID = "In Active";
		}
		$tempdata->START_DATE_TIME = $promo_campaign_data_get->START_DATE_TIME;
		$tempdata->END_DATE_TIME = $promo_campaign_data_get->END_DATE_TIME;
		
		if($promo_campaign_data_get->PROMO_CAMPAIGN_TYPE_ID == 1 ||$promo_campaign_data_get->PROMO_CAMPAIGN_TYPE_ID == 6 ||$promo_campaign_data_get->PROMO_CAMPAIGN_TYPE_ID == 8){
			$tempdata->PROMO_CAMPAIGN_TYPE_ID = $promo_campaign_data_get->PROMO_CAMPAIGN_TYPE_ID;
			if($promo_campaign_data_get->P_PROMO_CHIPS ==1){
				$tempdata->P_PROMO_CHIPS = "Yes";
			}else{
				$tempdata->P_PROMO_CHIPS = "No";
			}
			$tempdata->P_PROMO_VALUE = $promo_campaign_data_get->P_PROMO_VALUE;
			$tempdata->P_PROMO_CAP_VALUE = $promo_campaign_data_get->P_PROMO_CAP_VALUE;
			$tempdata->MINIMUM_DEPOSIT_AMOUNT = $promo_campaign_data_get->MINIMUM_DEPOSIT_AMOUNT;
			if($promo_campaign_data_get->P_UNCLAIMED_BONUS == 1){
				$tempdata->P_UNCLAIMED_BONUS = "Yes";
			}else{
				$tempdata->P_UNCLAIMED_BONUS = "NO";
			}
			$tempdata->P_CREDIT_DEBITCARD_VALUE = $promo_campaign_data_get->P_CREDIT_DEBITCARD_VALUE;
			$tempdata->P_NETBANKING_VALUE = $promo_campaign_data_get->P_NETBANKING_VALUE;
			$tempdata->P_BONUS_CAP_VALUE = $promo_campaign_data_get->P_BONUS_CAP_VALUE;
			if($promo_campaign_data_get->P_TOURNAMENT_TICKETS == 1){
				$tempdata->P_TOURNAMENT_TICKETS = "Yes";
			}else{
				$tempdata->P_TOURNAMENT_TICKETS = "No";
			}
			
		}elseif($promo_campaign_data_get->PROMO_CAMPAIGN_TYPE_ID == 5){
			$tempdata->PROMO_CAMPAIGN_TYPE_ID = $promo_campaign_data_get->PROMO_CAMPAIGN_TYPE_ID;
			if($promo_campaign_data_get->S_PROMO_CHIPS == 1){
				$tempdata->S_PROMO_CHIPS = "Yes";
			}else{
				$tempdata->S_PROMO_CHIPS = "No";
			}
			$tempdata->S_PROMO_CHIP_VALUE = $promo_campaign_data_get->S_PROMO_CHIP_VALUE;
			if($promo_campaign_data_get->S_TOURNAMENT_TICKETS == 1){
				$tempdata->S_TOURNAMENT_TICKETS = "Yes";
			}else{
				$tempdata->S_TOURNAMENT_TICKETS = "No";
			}
		}else{
			$tempdata->PROMO_CAMPAIGN_TYPE_ID = $promo_campaign_data_get->PROMO_CAMPAIGN_TYPE_ID;
			if($promo_campaign_data_get->PROMO_PAYMENT_TYPE_ID == 1){
				$tempdata->PROMO_PAYMENT_TYPE_ID = "Promo";
			}else{
				$tempdata->PROMO_PAYMENT_TYPE_ID = "Bonus";
			}
			if($promo_campaign_data_get->PROMO_TYPE_ID == 1){
				$tempdata->PROMO_TYPE_ID = "CHIPS";
			}else{
				$tempdata->PROMO_TYPE_ID = "POINTS";
			}
			
		}
		
		$tempdata->PROMO_VALUE1 = $promo_campaign_data_get->PROMO_VALUE1;
		$typeId = $promo_campaign_data_get->PROMO_CAMPAIGN_TYPE_ID;
		$rafCampTypeIds = array(3,2,5,6);
		if(in_array($typeId,$rafCampTypeIds)){
			$tempdata->REFERRAL_USERNAME = $promo_campaign_data_get->REFERRAL_USERNAME;
			$tempdata->RAF_PERCENTAGE = $promo_campaign_data_get->RAF_PERCENTAGE;
		}
		$tempdata->EXP_USER_MIN = $promo_campaign_data_get->EXP_USER_MIN;
		$tempdata->EXP_USER_MAX = $promo_campaign_data_get->EXP_USER_MAX;
		return $tempdata;
		  
	}
	public function getCampaignURL(Request $request){
		$campaigncode = $request->campaigncode;
		$CampaignType = $request->CampaignType;
		$getUrl = URL::to('/');
		if(empty($campaigncode)){
			if($CampaignType == 1) {
					$txt = "PAY_";
				} elseif($CampaignType == 2) {
					$txt = "REG_";
				} else {
					$txt = "TAF_";
				}
				$intRandom = rand(100000,999999);
				$gcampaignCODE = $txt.$intRandom;
				
				if($getUrl == 'http://localhost/bo/public'){
					$url = 'https://localhost/';
				}elseif($getUrl == 'http://dev.pokerchronicles.com/bo/public'){
					$url ='https://devweb.alphabetabox.com/';
				}elseif($getUrl == 'http://pokerchronicles.com/bo/public'){
					$url = 'https://qa.alphabetabox.com/';
				}else{
					$url = 'https://www.pokerbaazi.com/';
				}
				$gcampaignURL = $url."?campaigncode=$gcampaignCODE";
				
				return response()->json(['gcampaignCODE'=>$gcampaignCODE,'gcampaignURL'=>$gcampaignURL]);
		}else{
			$gcampaignCODE = $campaigncode;
			if($getUrl == 'http://localhost/bo/public'){
				$url = 'https://localhost/';
			}elseif($getUrl == 'http://dev.pokerchronicles.com/bo/public'){
				$url ='https://devweb.alphabetabox.com/';
			}elseif($getUrl == 'http://pokerchronicles.com/bo/public'){
				$url = 'https://qa.alphabetabox.com/';
			}else{
				$url = 'https://www.pokerbaazi.com/';
			}
			$gcampaignURL = $url."?campaigncode=$gcampaignCODE";
			return response()->json(['gcampaignCODE'=>$gcampaignCODE,'gcampaignURL'=>$gcampaignURL]);
		}
	}
	public function chkUserValid(Request $request){
		$responseString="";
		if(isset($request->ref_username) && !empty($request->ref_username)){
			$getUserInfo = User::where('USERNAME', $request->ref_username)->select('USERNAME')->first();
			
			if(empty($getUserInfo)){
				$responseString="false";	
			}				
			else{
				$responseString="true";			
			}
		}
		else {
			$responseString="false";
		}
		return $responseString;
	}
	
	public function getEditCampaignData($campaignID){
		return PromoCampaign::select('promo_campaign.PROMO_CAMPAIGN_ID','promo_campaign.PROMO_CAMPAIGN_TYPE_ID','promo_campaign.IS_FEATURED','promo_campaign.DISPLAY_STATUS','promo_campaign.PARTNER_ID','promo_campaign.PROMO_CAMPAIGN_NAME','promo_campaign.PROMO_CAMPAIGN_CODE','promo_campaign.PROMO_CAMPAIGN_DESC','promo_campaign.PROMO_STATUS_ID','promo_campaign.COINS_REQUIRED','promo_campaign.START_DATE_TIME','promo_campaign.END_DATE_TIME','promo_campaign.COST','promo_campaign.TOTAL_USER_EXPECTED','promo_campaign.COST_PER_USER','promo_campaign.EXP_USER_MIN','promo_campaign.EXP_USER_MAX','promo_campaign.CREATED_ON','promo_campaign.STATUS','promo_campaign.PROMO_CAMPAIGN_URL','promo_campaign.MONEY_OPTION','promo_campaign.MINIMUM_DEPOSIT_AMOUNT','promo_campaign.MAXIMUM_DEPOSIT_AMOUNT','promo_campaign.TOURNAMENT_ID','promo_campaign.TOURNAMENT_NAME','promo_campaign.REMARK','promo_campaign.WITHDRAW_CRITERIA','r.PROMO_RULE_ID','r.PROMO_CAMPAIGN_ID','r.PROMO_PAYMENT_TYPE_ID','r.PROMO_TYPE_ID','r.PROMO_MIN_VALUE','r.PROMO_MAX_VALUE','r.PROMO_VALUE1','PROMO_VALUE2','r.P_PROMO_CHIPS','r.P_PROMO_VALUE','r.P_PROMO_CAP_VALUE','r.P_UNCLAIMED_BONUS','r.P_CREDIT_DEBITCARD_VALUE','r.P_NETBANKING_VALUE','r.P_BONUS_CAP_VALUE','r.P_TOURNAMENT_TICKETS','r.P_TOURNAMENT_TICKET_ID','r.S_PROMO_CHIPS','r.S_PROMO_CHIP_VALUE','r.S_TOURNAMENT_TICKETS','r.S_TOURNAMENT_TICKET_ID','r.REFERRAL_USERNAME','r.RAF_PERCENTAGE')
		->leftjoin('promo_rule as r', 'r.PROMO_CAMPAIGN_ID', '=', 'promo_campaign.PROMO_CAMPAIGN_ID')
		->where('promo_campaign.PROMO_CAMPAIGN_ID',$campaignID)
		->get();
	}
	public function editCampaignTournamentId($campaignID){
		return CampaignTournamentMapping::select('TOURNAMENT_ID','TOURNAMENT_NAME')->where('PROMO_CAMPAIGN_ID',$campaignID)->get();
	}
	public function viewUserExcel($id){
		
		$PROMO_CAMPAIGN_ID = decrypt($id);
		$campaign_info = PromoCampaign::where('PROMO_CAMPAIGN_ID', $PROMO_CAMPAIGN_ID)->select('PROMO_CAMPAIGN_ID','PROMO_CAMPAIGN_CODE','PROMO_CAMPAIGN_NAME','PROMO_CAMPAIGN_DESC','START_DATE_TIME','END_DATE_TIME','PARTNER_ID','PROMO_CAMPAIGN_TYPE_ID')->first();
		
		#get campaign type form campaignid
		$campaign_type_id = PromoCampaignType::where('PROMO_CAMPAIGN_TYPE_ID',$campaign_info->PROMO_CAMPAIGN_TYPE_ID)->select('PROMO_CAMPAIGN_TYPE')->first();
		
		#get partner name from partner id
		$partner_type_id = Partner::where('PARTNER_ID',$campaign_info->PARTNER_ID)->select('PARTNER_NAME')->first();
		
		$query = CampaignUserSpecific::query();
        $query->from("campaign_user_specific as cus");
        $query->join('user as u', 'u.USER_ID', '=', 'cus.USER_ID');
        $query->when(!empty($PROMO_CAMPAIGN_ID), function ($query) use ($PROMO_CAMPAIGN_ID){
            return $query->where('cus.PROMO_CAMPAIGN_ID', $PROMO_CAMPAIGN_ID);
        });
		$query->when(!empty($PROMO_CAMPAIGN_ID), function ($query) use ($PROMO_CAMPAIGN_ID){
            return $query->where('cus.STATUS',1);
        });
        $query->orderBy('cus.CREATED_DATE', 'DESC');
        $getUserExcelData = $query->select(
            'u.USERNAME',
            'u.EMAIL_ID',
            'u.CONTACT',
            'u.CONTACT',
            'cus.CAMPAIGN_USER_SPECIFIC_ID',
            'cus.USER_ID',
            'cus.STATUS',
            'cus.CREATED_DATE'
        );
		$getUserExcel = $getUserExcelData->get();
		return view('bo.views.marketing.managecampaign.viewuserexcel',['getUserExcel'=>$getUserExcel,'campaign_info'=>$campaign_info,'campaign_type_id'=>$campaign_type_id,'partner_type_id'=>$partner_type_id]);
		
	}
	public function editDataExcel(Request $request){
		
		if(isset($request->username) && !empty($request->username)){	
			$user_id = User::where('USERNAME', $request->username)->count();
			if($user_id){
				return "true";
			}else{
				return "false";
			}
		}else{
			$cus_user_id = CampaignUserSpecific::where('CAMPAIGN_USER_SPECIFIC_ID', $request->id)->select('USER_ID')->first();
			if(isset($cus_user_id) && !(empty($cus_user_id))){
				$user_id = User::where('USER_ID', $cus_user_id->USER_ID)->select('USERNAME')->first();
				return response()->json(['name'=>$user_id->USERNAME]);
			}else{
				return "true";
			}
		}
		
	}
	public function updateDataExcel(Request $request){
		
		$username = $request->name;
		$cus_id = $request->cus_id;
		
		$promo_cam_id = CampaignUserSpecific::where('CAMPAIGN_USER_SPECIFIC_ID', $cus_id)->select('PROMO_CAMPAIGN_ID')->first();
		$user_id = getUserIdFromUsername($username);
		
		$alreadyAvailable = app(CampaignUserSpecific::class)->chkUserAlreadyExist($user_id, $promo_cam_id->PROMO_CAMPAIGN_ID);
		
		if(isset($alreadyAvailable) && !empty($alreadyAvailable) && $alreadyAvailable > 0 ){
			return back()->with('error', "User is already available.");
		}else{
			$update_user_id = CampaignUserSpecific::where('CAMPAIGN_USER_SPECIFIC_ID', $cus_id)->update(['USER_ID' => $user_id]);
			if($update_user_id != ''){
				return back()->with('success', "User Updated successfully.");
			}else{
				return back()->with('error', "User Not Updated.");
			}
		}
	}
	public function deleteDataExcel(Request $request){
		
		$cus_id = $request->id;
		$cus_id_deactivate = CampaignUserSpecific::where('CAMPAIGN_USER_SPECIFIC_ID', $cus_id)->update(['STATUS'=>2]);
		if(isset($cus_id_deactivate) && !(empty($cus_id_deactivate))){
			return response()->json(['status'=>true,'success', "Record Deleted."]);
		}else{
			return response()->json(['status'=>false,'error', "Record Not Deleted."]);
		}
	}
	#function for import excel of users when adding only excel not creating campaign
	public function addViewUserExcel(Request $request, Importer $importer){
		
		if($request->excel_viewuser){
			$extension =  strtolower($request->excel_viewuser->getClientOriginalExtension());
			if(in_array($extension, ['csv', 'xls', 'xlsx'])){
				$importer->import($data = new UserImport, $request->excel_viewuser);
				$campaign_list_excel = $data->data;
				if($campaign_list_excel == '' || $campaign_list_excel == null){
					return response()->json(['message'=>"Excel file is empty",'status'=>102]);
				}
				$headingArr = array_keys($campaign_list_excel[1]);
				if($headingArr[0] == "Username"){
					if($request->campaign_id){
						$userStatusResponse = $this->campaignUserRegisterExcelAdd($campaign_list_excel, $request);
							return back()->with(['success'=>'Excel is uploaded successfully.']);
						
					}else{
						$userStatusResponse = $this->campaignUserRegisterExcelChk($campaign_list_excel ,$request);
					return response()->json(['userStatusResponse'=>$userStatusResponse]);
					}
				}else{
					return response()->json(['message'=>"Please upload a valid index file",'status'=>101]);
				}
			}else{
				return back()->with('error', "The excel file must be a file of type: csv, xls, xlsx.");
			}
		}else{
            return back()->with('error', 'Please upload an excel.');
		}
	}
	public function campaignUserRegisterExcelChk($campaign_list_excel, $request){
		
		$tempUser=[];
		foreach($campaign_list_excel as $campaign_list_users){
			$userName = '';
			$userNameVal = trim($campaign_list_users['Username']);
			if($userNameVal == "" || $userNameVal == null ){
			  continue;
		    }
			if(isset($userNameVal) && !empty($userNameVal)){
				$userName = $userNameVal;
			}
			$user_id = getUserIdFromUsername($userNameVal);
			$chkInvalidUserEntry = User::where('USER_ID', $user_id)->count();
				if($chkInvalidUserEntry <= 0){
					$tempUser[] = $user_id;
					$userStatusResponse[] = ['USER_ID' => $user_id, 'userName' => $userName, 'STATUS' => 'Invalid User','styleClass'=>'text-danger'];
				}else{
					$tempStatus = '';
					$styleClass='';
					if(in_array($user_id,$tempUser)){
						$tempStatus = 'User already exists';
						$styleClass = 'text-danger';
					}else{
						$tempStatus = 'Register User';
						$styleClass = 'text-success';
					}
					$userStatusResponse[] = ['USER_ID' => $user_id, 'userName' => $userName, 'STATUS' => $tempStatus,'styleClass'=>$styleClass];
				}
				$tempUser[]= $user_id;
			}//Loop close
			return $userStatusResponse;
	}
	
	public function campaignUserRegisterExcelAdd($campaign_list_excel, $request){
		
		foreach($campaign_list_excel as $campaign_list_users){
			
		$userName = '';
		if(isset($campaign_list_users['Username']) && !empty($campaign_list_users['Username'])){
			$userName = $campaign_list_users['Username'];
		}
		
		$userName = $userName;
		$user_id = getUserIdFromUsername($campaign_list_users['Username']);

		$alreadyAvailable = app(CampaignUserSpecific::class)->chkUserAlreadyExist($user_id, $request->campaign_id);
		
		$chkInvalidUserEntry = User::where('USER_ID', $user_id)->count();
		
			if(isset($alreadyAvailable) && !empty($alreadyAvailable) && $alreadyAvailable > 0 ){
				$user_id = $user_id;
				$userStatusResponse[] = ['USER_ID' => $user_id, 'userName' => $userName, 'STATUS' => '0'];
			}elseif($chkInvalidUserEntry <= 0){
				$user_id = $user_id;
				$userStatusResponse[] = ['USER_ID' => $user_id, 'userName' => $userName, 'STATUS' => '2'];
			}else{
				$dataArray=[];
				$dataObj['USER_ID']=$user_id;
				$dataObj['STATUS']=1;
				$dataObj['PROMO_CAMPAIGN_ID']=$request->campaign_id;
				$dataObj['CREATED_DATE']=date('Y-m-d H:i:s');
				$dataArray[]=$dataObj;
				
				$data = CampaignUserSpecific::insert($dataArray);
				$userStatusResponse[] = ['USER_ID' => $user_id, 'userName' => $userName, 'STATUS' => '1'];
			}
		}
		return $userStatusResponse;
		//}
	}
	public function viewbulkcode($id){
		
		$PROMO_CAMPAIGN_ID = decrypt($id);
		
		$promo_info = PromoCampaign::where('PROMO_CAMPAIGN_ID', $PROMO_CAMPAIGN_ID)->select('PROMO_CAMPAIGN_NAME','PARTNER_ID','PROMO_CAMPAIGN_DESC','START_DATE_TIME','END_DATE_TIME','EXP_USER_MIN','EXP_USER_MAX','CREATED_ON','PROMO_CAMPAIGN_TYPE_ID')->first();
		
		#get campaign type form campaignid
		$campaign_type_id = PromoCampaignType::where('PROMO_CAMPAIGN_TYPE_ID',$promo_info->PROMO_CAMPAIGN_TYPE_ID)->select('PROMO_CAMPAIGN_TYPE')->first();
		
		#get partner name from partner id
		$partner_type_id = Partner::where('PARTNER_ID',$promo_info->PARTNER_ID)->select('PARTNER_NAME')->first();
		
		$bulk_promo_data = BulkPromoCampaign::where('PROMO_CAMPAIGN_ID', $PROMO_CAMPAIGN_ID)->select('PROMO_CAMPAIGN_NAME','BULK_UNIQUE_CODE','STATUS','CREATED_DATE','USER_ID')->get();
		
		return view('bo.views.marketing.managecampaign.viewbulkcode', ['bulk_promo_data'=>$bulk_promo_data,'campaign_info'=>$promo_info,'campaign_type_id'=>$campaign_type_id,'partner_type_id'=>$partner_type_id]);
	}
	public function changeCampaignStatus(Request $request) {
		
		$promo_campaign_id = $request->id;
		$status = $request->status;
		
		$change_status = PromoCampaign::where('PROMO_CAMPAIGN_ID', $promo_campaign_id)->update(['PROMO_STATUS_ID'=>$status]);
		
		if(isset($change_status) && !(empty($change_status))){
			if($status == '1' ){
				return response()->json(['status'=>true, 'message'=>"Status activate."]);
			}else if($status == '2' ){
				return response()->json(['status'=>true,'message'=> "Status Deactivate."]);
			}
		}else{
			return response()->json(['status'=>false,'message'=> "Status change operation failed."]);
		}
	}
	public function viewContactExcel($id){
		
		$PROMO_CAMPAIGN_ID = decrypt($id);
		
		$campaign_info = PromoCampaign::where('PROMO_CAMPAIGN_ID', $PROMO_CAMPAIGN_ID)->select('PROMO_CAMPAIGN_ID','PROMO_CAMPAIGN_CODE','PROMO_CAMPAIGN_NAME','PROMO_CAMPAIGN_DESC','START_DATE_TIME','END_DATE_TIME','PARTNER_ID','PROMO_CAMPAIGN_TYPE_ID')->first();
		
		#get campaign type form campaignid
		$campaign_type_id = PromoCampaignType::where('PROMO_CAMPAIGN_TYPE_ID',$campaign_info->PROMO_CAMPAIGN_TYPE_ID)->select('PROMO_CAMPAIGN_TYPE')->first();
		
		#get partner name from partner id
		$partner_type_id = Partner::where('PARTNER_ID',$campaign_info->PARTNER_ID)->select('PARTNER_NAME')->first();
		$getUserExcelData = CampaignUserSpecific::where('PROMO_CAMPAIGN_ID', $PROMO_CAMPAIGN_ID)->where('STATUS', 1)->orderBy('CREATED_DATE', 'DESC')->get();
		
		return view('bo.views.marketing.managecampaign.viewcontactexcel',['getUserExcel'=>$getUserExcelData,'campaign_info'=>$campaign_info,'campaign_type_id'=>$campaign_type_id,'partner_type_id'=>$partner_type_id]);
	}
	public function editDataExcelContact(Request $request){
		
		if(isset($request->id) && !empty($request->id)){	
			$reg_num = CampaignUserSpecific::where('CAMPAIGN_USER_SPECIFIC_ID', $request->id)->select('REG_NUMBERS')->first();
			return response()->json(['mobile_number'=>$reg_num->REG_NUMBERS]); 
		}
	}
	public function updateDataExcelContact(Request $request){
		
		$mobile_number = $request->number;
		$mobile_number = filter_var($mobile_number, FILTER_SANITIZE_NUMBER_INT);
		if(strlen($mobile_number) == "10" && $mobile_number!='' && $mobile_number != null){
			$cus_id = $request->cus_id;
			$promo_cam_id = DB::table('campaign_user_specific')->where("CAMPAIGN_USER_SPECIFIC_ID", $cus_id)->select('PROMO_CAMPAIGN_ID')->first();
			$alreadyAvailable = DB::table('campaign_user_specific')->where("PROMO_CAMPAIGN_ID", $promo_cam_id->PROMO_CAMPAIGN_ID)->where("REG_NUMBERS", $mobile_number)->where('status','!=',2)->count();
			
			if(isset($alreadyAvailable) && !empty($alreadyAvailable) && $alreadyAvailable > 0 ){
				return back()->with('error', "Contact is already available.");
			}else{
				$update_number = CampaignUserSpecific::where('CAMPAIGN_USER_SPECIFIC_ID', $cus_id)->update(['REG_NUMBERS' => $mobile_number]);
				if($update_number != ''){
					return back()->with('success', "Contact Updated successfully.");
				}else{
					return back()->with('error', "Contact Not Updated.");
				}
			}
		}else{
			return back()->with('error', "Invalid Contact");
		}
	}
	public function deleteDataExcelContact(Request $request){
		$cus_id = $request->id;
		
		$cus_id_deactivate = CampaignUserSpecific::where('CAMPAIGN_USER_SPECIFIC_ID', $cus_id)->update(['STATUS'=>2]);
		
		if(isset($cus_id_deactivate) && !(empty($cus_id_deactivate))){
			return response()->json(['status'=>true,'message'=>'Record Deleted.']);
		}else{
			return response()->json(['status'=>false, 'message'=>'Record Not Deleted']);
		}
	}
	public function addViewUserExcelContact(Request $request, Importer $importer){
		if($request->excel_viewcontact){
			$extension =  strtolower($request->excel_viewcontact->getClientOriginalExtension());
			if(in_array($extension, ['csv', 'xls', 'xlsx'])){
				$importer->import($data = new UserImport, $request->excel_viewcontact);
				$campaign_list_excel = $data->data;
				if($campaign_list_excel == '' || $campaign_list_excel == null){
					return response()->json(['message'=>"Excel file is empty",'status'=>102]);
				}
				$headingArr = array_keys($campaign_list_excel[1]);
				if($headingArr[0] == "Contact"){
					foreach($campaign_list_excel as $campaign_list_contact){
						$userContact = trim($campaign_list_contact['Contact']);
					}
					
					if($userContact != "" || $userContact != null){
						if($request->campaign_id){
							$contactStatusResponse = $this->registrationExcelADD($campaign_list_excel, $request);
							if(isset($contactStatusResponse) || !empty($contactStatusResponse)){
								return back()->with(['success'=>'Excel is uploaded successfully.']);
							}else{
								return back()->with(['error'=>'Excel is not uploaded successfully.']);
							}
						}else{
							$contactStatusResponse = $this->registrationExcelChk($campaign_list_excel, $request);
							return response()->json(['contactStatusResponse'=>$contactStatusResponse]);
						}
					}else{
						return response()->json(['message'=>"Excel list is empty",'status'=>102]);
					}
				}else{
					return response()->json(['message'=>"Please upload a valid index file",'status'=>101]);
				}
			}else{
				return back()->with('error', "The excel file must be a file of type: csv, xls, xlsx.");
			}
		}else{
            return back()->with('error', 'Please upload an excel.');
		}
	}
	public function registrationExcelChk($campaign_list_excel, $request){
		$tempContact = [];
		foreach($campaign_list_excel as $campaign_list_contact){
			$tempContactVal = $userContact = '';
			$userContact = trim($campaign_list_contact['Contact']);
			if($userContact == "" || $userContact == null){
				continue;
			}
			$tempContactVal = $campaign_list_contact['Contact'];
			$userContact = filter_var($userContact, FILTER_SANITIZE_NUMBER_INT);
			if($userContact == "" || $userContact == null ){
				$tempContact[]= $tempContactVal;
				$contactResponse[] = ['userContact' => $tempContactVal, 'STATUS' => 'Invalid   Contact Number','styleClass' => 'text-danger'];
			}else{
				$tempStatus = '';
				$styleClass='';
				if(isset($tempContactVal) && (strlen($tempContactVal) == "10")){
					if(in_array($userContact,$tempContact)){
						$tempStatus = 'Contact already exists';
						$styleClass = 'text-danger';
					}else{
						$tempStatus = 'Register Contact';
						$styleClass = 'text-success';
					}
					$contactResponse[] = ['userContact' => $tempContactVal,  'STATUS' => $tempStatus,'styleClass'=>$styleClass];
				}else{
					$contactResponse[] = ['userContact' => $tempContactVal, 'STATUS' => 'Invalid Contact Number','styleClass'=>'text-danger'];
				}
				$tempContact[]= $tempContactVal;
			}//else close
				
		}//foreach close
		return $contactResponse;
	}
	public function registrationExcelADD($campaign_list_excel, $request){
		$tempContact = [];
		if(isset($campaign_list_excel) && !empty($campaign_list_excel)){
			$dataArray=[];
			foreach($campaign_list_excel as $campaign_list_contact){
				$tempContactVal = $userContact = '';
				$userContact = trim($campaign_list_contact['Contact']);
				if($userContact == "" || $userContact == null){
					continue;
				}
				$tempContactVal = $campaign_list_contact['Contact'];
				$userContact = filter_var($userContact, FILTER_SANITIZE_NUMBER_INT);
				$alreadyAvailable = CampaignUserSpecific::where('REG_NUMBERS', $userContact)->where("PROMO_CAMPAIGN_ID", $request->campaign_id)->count();
				if($userContact == "" || $userContact == null ){
					$tempContact[]= $tempContactVal;
					$contactResponse[] = ['userContact' => $tempContactVal];
				}elseif(isset($alreadyAvailable) && $alreadyAvailable > 0 ){
					$tempContact[]= $tempContactVal;
					$contactResponse[] = ['userContact' => $tempContactVal,'Status'=>'100'];
				}else{
					$tempStatus = '';
					$styleClass='';
					if(isset($tempContactVal) && (strlen($tempContactVal) == "10")){
						$userContact= $userContact;
						$dataObj=[];
						$dataObj['STATUS']=1;
						$dataObj['PROMO_CAMPAIGN_ID']=$request->campaign_id;
						$dataObj['CREATED_DATE']=date('Y-m-d H:i:s');
						$dataObj['REG_NUMBERS']=$userContact;
						$data = CampaignUserSpecific::insert($dataObj);
						$contactResponse[] = ['userContact' => $userContact];
					}
				}//else close
					
			}//foreach close
			
		}
		return $contactResponse;
	}
	public function deleteDataPromoCampaign(Request $request){
		$promo_id_deactivate = PromoCampaign::where('PROMO_CAMPAIGN_ID', $request->id)->update(['STATUS'=>2]);
		if(isset($promo_id_deactivate) && !(empty($promo_id_deactivate))){
			return response()->json(['status'=>true,'message'=>'Record Deleted.']);
		}else{
			return response()->json(['status'=>false, 'message'=>'Record Not Deleted']);
		}
	}
	public function chkUsersExcelValid(Request $request, Importer $importer){
		
		$extension =  strtolower($request->excel_camp->getClientOriginalExtension());
		$dataArray=[];
		
		if(in_array($extension, ['csv', 'xls', 'xlsx'])){
			
			$importer->import($data = new UserImport, $request->excel_camp);
			$campaign_list_excel = $data->data;
			if($campaign_list_excel == '' || $campaign_list_excel == null){
				return response()->json(['message'=>"Excel file is empty",'status'=>102]);
			}
			$tempUser=[];
			$headingArr = array_keys($campaign_list_excel[1]);
			
			if($headingArr[0] == "Username"){
			foreach($campaign_list_excel as $campaign_list_users){
				$userName = '';
				$userNameVal = trim($campaign_list_users['Username']);
				if($userNameVal == "" || $userNameVal == null ){
				  continue;
				}
				if(isset($userNameVal) && !empty($userNameVal)){
					$userName = $userNameVal;
				}
				$user_id = getUserIdFromUsername($userNameVal);
				$chkInvalidUserEntry = User::where('USER_ID', $user_id)->count();
					if($chkInvalidUserEntry <= 0){
						$tempUser[] = $user_id;
						$userStatusResponse[] = ['USER_ID' => $user_id, 'userName' => $userName, 'STATUS' => 'Invalid User','styleClass'=>'text-danger'];
					}else{
						$tempStatus = '';
						$styleClass='';
						if(in_array($user_id,$tempUser)){
							$tempStatus = 'User already exists';
							$styleClass = 'text-danger';
						}else{
							$tempStatus = 'Register User';
							$styleClass = 'text-success';
						}
						$userStatusResponse[] = ['USER_ID' => $user_id, 'userName' => $userName, 'STATUS' => $tempStatus,'styleClass'=>$styleClass];
					}
					$tempUser[]= $user_id;
			}//Loop close
			return response()->json(['userStatusResponse'=>$userStatusResponse]);
			}else{
				return response()->json(['message'=>"Please upload a valid index file",'status'=>101]);
				
			}
		
		}else{
			return back()->with('error', "The excel file must be a file of type: csv, xls, xlsx.");
		}
	}
	
	public function chkExcelValidContact(Request $request, Importer $importer){
		$extension =  strtolower($request->excel_camp_regist->getClientOriginalExtension());
		$dataArray=[];
		if(in_array($extension, ['csv', 'xls', 'xlsx'])){
			$importer->import($data = new UserImport, $request->excel_camp_regist);
			$campaign_list_excel = $data->data;
			if($campaign_list_excel == '' || $campaign_list_excel == null){
				return response()->json(['message'=>"Excel file is empty",'status'=>102]);
			}
			$headingArr = array_keys($campaign_list_excel[1]);
			
			if($headingArr[0] == "Contact"){
				$tempContact = [];
				foreach($campaign_list_excel as $campaign_list_contact){
					$tempContactVal = $userContact = '';
					$userContact = trim($campaign_list_contact['Contact']);
					if($userContact == "" || $userContact == null){
						continue;
					}
					$tempContactVal = $campaign_list_contact['Contact'];
					$userContact = filter_var($userContact, FILTER_SANITIZE_NUMBER_INT);
					if($userContact == "" || $userContact == null ){
						$contactResponse[] = ['userContact' => $tempContactVal, 'STATUS' => 'Invalid Contact Number','styleClass' => 'text-danger'];
					}else{
						$tempStatus = '';
						$styleClass='';
						if(isset($tempContactVal) && (strlen($tempContactVal) == "10")){
							if(in_array($userContact,$tempContact)){
								$tempStatus = 'Contact already exists';
								$styleClass = 'text-danger';
							}else{
								$tempStatus = 'Register Contact';
								$styleClass = 'text-success';
							}
							$contactResponse[] = ['userContact' => $tempContactVal,  'STATUS' => $tempStatus,'styleClass'=>$styleClass];
						}
						$tempContact[]= $tempContactVal;
					}//else close
					
				}//Loop close
				return response()->json(['userStatusResponse'=>$contactResponse]);
			}else{
				return response()->json(['message'=>"Please upload a valid index file",'status'=>101]);
			}
			
		}else{
				return back()->with('error', "The excel file must be a file of type: csv, xls, xlsx.");
		}
	}
}
		