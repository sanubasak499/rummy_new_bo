<?php
namespace App\Http\Controllers\bo\marketing;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AdminUser;
use stdClass;
use DB;
use \PDF;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Carbon\Carbon;
class FTDReportController extends Controller 
{
	public function index(Request $request){
           return view('bo.views.marketing.FTDReport.FTDReport');
	}
	
	
	public function FTDExportXlsx(Request $request){
            Validator::make($request->all(), [
                'date_from'=> 'required|date',
                'date_to'=> 'required|date|after_or_equal:date_from',
            ])->validate();
            
            $date_from =Carbon::parse($request->date_from)->setTime(0, 0, 0)->toDateTimeString();//->format('Y-m-d');
            $date_to =Carbon::parse($request->date_to)->setTime(23, 59, 59)->toDateTimeString();//->format('Y-m-d');

            $data = DB::select("call usp_FTD_Report('$date_from','$date_to')");
            $dataArr = json_decode(json_encode($data),true);
            $header=[];
            if(is_array($dataArr) && count($dataArr) > 0){
                $header =  array_keys($dataArr[0]);
               
                $fileName = "FTDreport_" . str_replace(" ","-",$date_from) ."_" . str_replace(" ","-",$date_to) . ".xlsx";
                return Excel::download(new CollectionExport($dataArr,$header), $fileName);
            }else{
                return back()->with('custom_message', ["title"=>"Info","text"=>"No record found."]);
            }
            
        }
}