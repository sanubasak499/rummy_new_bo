<?php

namespace App\Http\Controllers\bo\marketing;

use Illuminate\Http\Request;
use App\Http\Controllers\bo\BaseController;
use DB;
use App\Models\BranchWebhook;
use App\Models\AppBranchWebhook;

class BranchWebhookController extends BaseController
{
    //
    public function insertWebhookData(Request $request)
    {

        date_default_timezone_set('Asia/Kolkata');

        $webHookResponse = $request->all();
        if (isset($webHookResponse['name'])) {
            $name = $webHookResponse['name'];
        } else {
            $name = "";
        }

        if (isset($webHookResponse['event_timestamp'])) {
            $event_timestamp = $webHookResponse['event_timestamp'];
            $event_timestamp = $this->convertUnixTimeStampToDateTime($event_timestamp);
        } else {
            $event_timestamp = "1970-01-01 00:00:00";
        }

        if (isset($webHookResponse['user_data']['developer_identity'])) {
            $user_id = $webHookResponse['user_data']['developer_identity'];
        } else {
            $user_id = "";
        }

        if (isset($webHookResponse['user_data']['geo_region_en'])) {
            $geo_region_en = $webHookResponse['user_data']['geo_region_en'];
        } else {
            $geo_region_en = "";
        }

        if (isset($webHookResponse['user_data']['geo_city_en'])) {
            $geo_city_en = $webHookResponse['user_data']['geo_city_en'];
        } else {
            $geo_city_en = "";
        }

        if (isset($webHookResponse['user_data']['os'])) {
            $os = $webHookResponse['user_data']['os'];
        } else {
            $os = "";
        }

        if (isset($webHookResponse['user_data']['platform'])) {
            $platform = $webHookResponse['user_data']['platform'];
        } else {
            $platform = "";
        }

        if (isset($webHookResponse['user_data']['ip'])) {
            $ip = $webHookResponse['user_data']['ip'];
        } else {
            $ip = "";
        }

        if (isset($webHookResponse['attributed'])) {
            $attributed = $webHookResponse['attributed'];
        } else {
            $attributed = "";
        }

        if (isset($webHookResponse['last_attributed_touch_data']['~feature'])) {
            $feature = $webHookResponse['last_attributed_touch_data']['~feature'];
        } else {
            $feature = "";
        }

        if (isset($webHookResponse['last_attributed_touch_data']['~campaign'])) {
            $campaign = $webHookResponse['last_attributed_touch_data']['~campaign'];
        } else {
            $campaign = "";
        }
        if (isset($webHookResponse['last_attributed_touch_data']['~channel'])) {
            $channel = $webHookResponse['last_attributed_touch_data']['~channel'];
        } else {
            $channel = "";
        }
        if (isset($webHookResponse['last_attributed_touch_data']['~advertising_partner_name'])) {
            $advertising_partner_name = $webHookResponse['last_attributed_touch_data']['~advertising_partner_name'];
        } else {
            $advertising_partner_name = "";
        }
        if (isset($webHookResponse['last_attributed_touch_data']['+url'])) {
            $url = $webHookResponse['last_attributed_touch_data']['+url'];
        } else {
            $url = "";
        }
        if (isset($webHookResponse['last_attributed_touch_data']['+click_timestamp'])) {
            $click_timestamp = $webHookResponse['last_attributed_touch_data']['+click_timestamp'];
            // $click_timestamp = $this->convertUnixTimeStampToDateTime($click_timestamp);
            $click_timestamp = date('Y-m-d H:i:s', $click_timestamp);
        } else {
            $click_timestamp = "0000-00-00 00:00:00";
        }
        
        /**
         * Author : Brijesh
         * Date   : 21-01-2020
         * Code Start
         */
        $ad_name = "";
        if (isset($webHookResponse['last_attributed_touch_data']['~ad_name']) && !empty($webHookResponse['last_attributed_touch_data']['~ad_name'])) {
            $ad_name = $webHookResponse['last_attributed_touch_data']['~ad_name'];
        }
        
        $customer_placement = "";
        if (isset($webHookResponse['last_attributed_touch_data']['~customer_placement'])) {
            $customer_placement = $webHookResponse['last_attributed_touch_data']['~customer_placement'];
        }
        
        $ad_set_name = "";
        if (isset($webHookResponse['last_attributed_touch_data']['~ad_set_name'])) {
            $ad_set_name = $webHookResponse['last_attributed_touch_data']['~ad_set_name'];
        }
        
        $marketing_title = "";
        if (isset($webHookResponse['last_attributed_touch_data']['$marketing_title'])) {
            $marketing_title = $webHookResponse['last_attributed_touch_data']['$marketing_title'];
        }
        /**
         * Brijesh Code End
         */
        
        
        $now = date("Y:m:d H:i:s");

        $webhook = new BranchWebhook();
        $webhook->EVENT_NAME                = $name;
        $webhook->EVENT_TIMESTAMP           = $event_timestamp;
        $webhook->USER_ID                   = $user_id;
        $webhook->STATE                     = $geo_region_en;
        $webhook->CITY                      = $geo_city_en;
        $webhook->SYSTEM_OS                 = $os;
        $webhook->PLATFORM                  = $platform;
        $webhook->ATTRIBUTED                = $attributed;
        $webhook->ATTR_FEATURE              = $feature;
        $webhook->ATTR_CAMPAIGN             = $campaign;
        $webhook->ATTR_CHANNEL              = $channel;
        $webhook->ATTR_ADV_PARTNER_NAME     = ($advertising_partner_name)? $advertising_partner_name : $marketing_title;
        $webhook->ATTR_URL                  = $url;
        
        $webhook->AD_NAME                  = $ad_name;
        $webhook->CUSTOMER_PLACEMENT       = $customer_placement;
        $webhook->AD_SET_NAME              = $ad_set_name;
        $webhook->MARKETING_TITLE          = ($marketing_title)? $marketing_title : $advertising_partner_name;
        
        $webhook->ATTR_CLICK_TIMESTAMP      = $click_timestamp;
        $webhook->FULL_BRANCH_DATA_RECEIVED = json_encode($webHookResponse);
        $webhook->CREATED_DATE              = $now;
        $webhook->IP_ADDRESS                = $ip;
        
        if ($webhook->EVENT_NAME == 'COMPLETE_REGISTRATION') {
            if (empty($branch = BranchWebhook::where('EVENT_NAME', $name)->where('USER_ID', $user_id)->first())) {
                $webhook->save();
                return response()->json(['status' => 200, 'message' => "success"], 200);
            } else {
                return response()->json(['status' => 400, 'message' => "already exist"], 400);
            }
        } else {
            $webhook->save();
            return response()->json(['status' => 200, 'message' => "success"], 200);
        }
        
    }
    public function insertAppWebhookData(Request $request)
    {
        date_default_timezone_set('Asia/Kolkata');
        
        $webHookResponse = $request->all();
        $event_name = "";
        if (isset($webHookResponse['event_name'])) {
            $event_name = $webHookResponse['event_name'];
        }
        $event_timestamp = "1970-01-01 00:00:00";
        if (isset($webHookResponse['event_timestamp'])) {
            $event_timestamp = $this->convertUnixTimeStampToDateTime($webHookResponse['event_timestamp']);
        }
        $channel = "";
        if (isset($webHookResponse["last_attributed_touch_data"]['~channel'])) {
            $channel = $webHookResponse["last_attributed_touch_data"]['~channel'];
        }
        $campaign = "";
        if (isset($webHookResponse["last_attributed_touch_data"]['~campaign'])) {
            $campaign = $webHookResponse["last_attributed_touch_data"]['~campaign'];
        }
        $marketing_title = "";
        if (isset($webHookResponse["last_attributed_touch_data"]['$marketing_title'])) {
            $marketing_title = $webHookResponse["last_attributed_touch_data"]['$marketing_title'];
        }
        $url = "";
        if (isset($webHookResponse["last_attributed_touch_data"]['+url'])) {
            $url = $webHookResponse["last_attributed_touch_data"]['+url'];
        }
        $aaid = "";
        if (isset($webHookResponse["user_data"]['aaid'])) {
            $aaid = $webHookResponse["user_data"]['aaid'];
        }
        $idfa = "";
        if (isset($webHookResponse["user_data"]['idfa'])) {
            $idfa = $webHookResponse["user_data"]['idfa'];
        }
        $brand = "";
        if (isset($webHookResponse["user_data"]['brand'])) {
            $brand = $webHookResponse["user_data"]['brand'];
        }
        $model = "";
        if (isset($webHookResponse["user_data"]['model'])) {
            $model = $webHookResponse["user_data"]['model'];
        }
        $days_from_last_attributed_touch_to_event = "";
        if (isset($webHookResponse['days_from_last_attributed_touch_to_event'])) {
            $days_from_last_attributed_touch_to_event = $webHookResponse['days_from_last_attributed_touch_to_event'];
        }
        $hours_from_last_attributed_touch_to_event = "";
        if (isset($webHookResponse['hours_from_last_attributed_touch_to_event'])) {
            $hours_from_last_attributed_touch_to_event = $webHookResponse['hours_from_last_attributed_touch_to_event'];
        }
        $minutes_from_last_attributed_touch_to_event = "";
        if (isset($webHookResponse['minutes_from_last_attributed_touch_to_event'])) {
            $minutes_from_last_attributed_touch_to_event = $webHookResponse['minutes_from_last_attributed_touch_to_event'];
        }
        $attributed = "";
        if (isset($webHookResponse['attributed'])) {
            $attributed = $webHookResponse['attributed'];
        }
        $timestamp = "";
        if (isset($webHookResponse['timestamp'])) {
            $timestamp = $webHookResponse['timestamp'];
        }
        
        $webhook = new AppBranchWebhook();
        
        $webhook->EVENT_TIMESTAMP = $event_timestamp;
        $webhook->CHANNEL = $channel;
        $webhook->CAMPAIGN = $campaign;
        $webhook->MARKETING_TITLE = $marketing_title;
        $webhook->URL = $url;
        $webhook->AAID = $aaid;
        $webhook->IDFA = $idfa;
        $webhook->BRAND = $brand;
        $webhook->MODEL = $model;
        $webhook->DAYS_FROM_LAST_ATTRIBUTED_TOUCH_TO_EVENT = $days_from_last_attributed_touch_to_event;
        $webhook->HOURS_FROM_LAST_ATTRIBUTED_TOUCH_TO_EVENT = $hours_from_last_attributed_touch_to_event;
        $webhook->MINUTES_FROM_LAST_ATTRIBUTED_TOUCH_TO_EVENT = $minutes_from_last_attributed_touch_to_event;
        $webhook->ATTRIBUTED = $attributed;
        $webhook->EVENT_NAME = $event_name;
        $webhook->TIMESTAMP = $timestamp;
        $webhook->FULL_BRANCH_DATA_RECEIVED = json_encode($webHookResponse);
        $webhook->save();
        if($webhook->save()){
            return response()->json(['status' => 200, 'message' => "success"], 200);
        }else{
            return response()->json(['status' => 400, 'message' => "Operation failed"], 400);
        }
    }
    public function convertUnixTimeStampToDateTime($unixTimestamp)
    {
        date_default_timezone_set('Asia/Kolkata');
        return date('Y-m-d H:i:s', $unixTimestamp / 1000);
    }
}
