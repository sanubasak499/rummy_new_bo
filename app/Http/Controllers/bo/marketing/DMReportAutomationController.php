<?php

namespace App\Http\Controllers\bo\marketing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use DB;
use Carbon\Carbon;
use App;
use Validator;
use Mail;
use Storage;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Facades\Excel;


class DMReportAutomationController extends Controller
{

	public function exportEverydayFTDWise()
	{

		$Result = $this->everydayFTDWise();
		$ResultValue = array();

		foreach ($Result as $ResultVal) {
			$ResultValue[] = (array) $ResultVal;
		}
		$data['date'] = date('Y-m-d', strtotime("-1 day"));
		$data['yesterday_date'] = date('j-F-Y', strtotime("-1 day"));

		$headingofResult  = array_keys($ResultValue[0]);

		$fileName = 'Every_Day_FTD_Wise_Report_' . $data['date'];
		$fileName = urlencode($fileName . ".xlsx");

		if (Storage::disk('local')->exists($fileName)) {
			Storage::delete($fileName);
		}
		Excel::store(new CollectionExport($ResultValue, $headingofResult), $fileName);

		## generate file and then send mail.
		if (Storage::disk('local')->exists($fileName)) {
			Mail::send("bo.mail.dailyMarketingReportMail", $data, function ($message) use ($data, $fileName) {
				$message->from(config('poker_config.mail.from.support'), 'Every day FTD wise repor');
				$message->to(config('poker_config.daily_marketing_aut_mail.emails'))
					->subject('Everyday FTD Wise report for last 60 days' . " " . $data['yesterday_date']);
				$message->attach(storage_path('app/' . $fileName), [
					'as' => $fileName,
					'mime' => 'application/vnd'
				]);
			});
			Storage::delete($fileName);
			return response()->json(["status" => 200, "message" => "success"]);
		} else {
			return response()->json(["status" => 201, "message" => "error"]);
		}
	}

	public function exportFTDday0day7day30()
	{

		$Result = $this->fTDday0day3day5day7day30();
		$ResultValue = array();

		foreach ($Result as $ResultVal) {
			$ResultValue[] = (array) $ResultVal;
		}

		$data['date'] = date('Y-m-d', strtotime("-1 day"));
		$data['yesterday_date'] = date('j-F-Y', strtotime("-1 day"));

		$headingofResult  = array_keys($ResultValue[0]);
		$fileName = 'FTD_Day0_Day7_Day30_Wise_Report_' . $data['date'];
		$fileName = urlencode($fileName . ".xlsx");

		if (Storage::disk('local')->exists($fileName)) {
			Storage::delete($fileName);
		}
		Excel::store(new CollectionExport($ResultValue, $headingofResult), $fileName);

		## generate file and then send mail.
		if (Storage::disk('local')->exists($fileName)) {
			Mail::send("bo.mail.dailyMarketingReportMail", $data, function ($message) use ($data, $fileName) {
				$message->from(config('poker_config.mail.from.support'), 'FTD Day0 Day3 Day5 Day7 Day30 wise report');
				$message->to(config('poker_config.daily_marketing_aut_mail.emails'))
					->subject('fTD Day0 day3 day5 Day7 Day30 Wise report for last 120 days' . " " . $data['yesterday_date']);
				$message->attach(storage_path('app/' . $fileName), [
					'as' => $fileName,
					'mime' => 'application/vnd'
				]);
			});
			Storage::delete($fileName);
			return response()->json(["status" => 200, "message" => "success"]);
		} else {
			return response()->json(["status" => 201, "message" => "error"]);
		}
	}

	public function exportSignUpCode()
	{

		$Result = $this->signUpCode();
		$ResultValue = array();

		foreach ($Result as $ResultVal) {
			$ResultValue[] = (array) $ResultVal;
		}

		$data['date'] = date('Y-m-d', strtotime("-1 day"));
		$data['yesterday_date'] = date('j-F-Y', strtotime("-1 day"));

		$headingofResult  = array_keys($ResultValue[0]);
		$fileName = 'Sign-Up-Code-wias-report' . $data['date'];
		$fileName = urlencode($fileName . ".xlsx");

		if (Storage::disk('local')->exists($fileName)) {
			Storage::delete($fileName);
		}
		Excel::store(new CollectionExport($ResultValue, $headingofResult), $fileName);

		## generate file and then send mail.
		if (Storage::disk('local')->exists($fileName)) {
			Mail::send("bo.mail.dailyMarketingReportMail", $data, function ($message) use ($data, $fileName) {
				$message->from(config('poker_config.mail.from.support'), 'Sign Up Code');
				$message->to(config('poker_config.daily_marketing_aut_mail.emails'))
					->subject('Sign-Up-Code' . " " . $data['yesterday_date']);
				$message->attach(storage_path('app/' . $fileName), [
					'as' => $fileName,
					'mime' => 'application/vnd'
				]);
			});
			Storage::delete($fileName);
			return response()->json(["status" => 200, "message" => "success"]);
		} else {
			return response()->json(["status" => 201, "message" => "error"]);
		}
	}

	public function exportTotalRegistration()
	{

		$Result = $this->totalRegistration();
		$ResultValue = array();

		foreach ($Result as $ResultVal) {
			$ResultValue[] = (array) $ResultVal;
		}

		$data['date'] = date('Y-m-d', strtotime("-1 day"));
		$data['yesterday_date'] = date('j-F-Y', strtotime("-1 day"));

		$headingofResult  = array_keys($ResultValue[0]);
		$fileName = 'Total-Registration' . $data['date'];
		$fileName = urlencode($fileName . ".xlsx");

		if (Storage::disk('local')->exists($fileName)) {
			Storage::delete($fileName);
		}
		Excel::store(new CollectionExport($ResultValue, $headingofResult), $fileName);

		## generate file and then send mail.
		if (Storage::disk('local')->exists($fileName)) {
			Mail::send("bo.mail.dailyMarketingReportMail", $data, function ($message) use ($data, $fileName) {
				$message->from(config('poker_config.mail.from.support'), 'Total Registration');
				$message->to(config('poker_config.daily_marketing_aut_mail.emails'))
					->subject('Total-Registration' . " " . $data['yesterday_date']);
				$message->attach(storage_path('app/' . $fileName), [
					'as' => $fileName,
					'mime' => 'application/vnd'
				]);
			});
			Storage::delete($fileName);
			return response()->json(["status" => 200, "message" => "success"]);
		} else {
			return response()->json(["status" => 201, "message" => "error"]);
		}
	}

	public function everydayFTDWise()
	{

		return $result = DB::connection('slave')->select("SELECT DATE(c.FDD) AS FDD,MIN(d.numofsignup) AS newsignup,COUNT((CASE WHEN a.PAYMENT_TRANSACTION_CREATED_ON=c.FDD THEN a.USER_ID END)) AS total_depositors,
		SUM((CASE WHEN a.PAYMENT_TRANSACTION_CREATED_ON=c.FDD THEN a.PAYMENT_TRANSACTION_AMOUNT END)) AS first_time_depo_amount,
		SUM(PAYMENT_TRANSACTION_AMOUNT) AS totaldepo ,COUNT(*) AS totaldepocount,SUM(DISTINCT b.rev) AS totalREVENUE
		FROM payment_transaction a FORCE INDEX(user_id)
		
		JOIN ( SELECT USER_ID,MIN(PAYMENT_TRANSACTION_CREATED_ON) AS FDD,CONCAT(USER_ID,'-',MIN(UPDATED_DATE)) AS ud FROM payment_transaction FORCE INDEX(user_id)
		WHERE TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111) AND PAYMENT_TRANSACTION_STATUS IN (103 , 125)
		GROUP BY USER_ID HAVING MIN(PAYMENT_TRANSACTION_CREATED_ON) BETWEEN CONCAT(CURDATE()- INTERVAL 60 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59') ) c ON a.USER_ID=c.USER_ID
		
		LEFT JOIN (SELECT USER_ID,SUM(`TOTAL_RAKE`) AS rev FROM `user_turnover_report_daily` WHERE 
		`REPORT_DATE` < CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59') AND
		USER_ID IN (
		
		SELECT tb1.USER_ID FROM (
		SELECT USER_ID,MIN(PAYMENT_TRANSACTION_CREATED_ON) AS FDD,CONCAT(USER_ID,'-',MIN(UPDATED_DATE)) AS ud FROM payment_transaction FORCE INDEX(user_id)
		WHERE TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111) AND PAYMENT_TRANSACTION_STATUS IN (103 , 125)
		GROUP BY USER_ID HAVING MIN(PAYMENT_TRANSACTION_CREATED_ON) BETWEEN CONCAT(CURDATE()- INTERVAL 60 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59'))
		tb1
		
		)
		GROUP BY USER_ID) b ON a.USER_ID=b.USER_ID
		
		JOIN ( SELECT COUNT(*) AS numofsignup,DATE(REGISTRATION_TIMESTAMP) AS dayd FROM `user`
		WHERE REGISTRATION_TIMESTAMP BETWEEN CONCAT(CURDATE()- INTERVAL 60 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY DATE(REGISTRATION_TIMESTAMP)) d ON d.dayd=DATE(c.FDD)
		
		WHERE TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111) AND PAYMENT_TRANSACTION_STATUS IN (103 , 125)
		AND DATE(c.FDD)  BETWEEN CONCAT(CURDATE()- INTERVAL 60 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY DATE(c.FDD);");
	}

	public function fTDday0day3day5day7day30()
	{
		return $result = DB::connection('slave')->select("SELECT DATE(a.rdate) AS DATE,b.ATTR_CHANNEL, b.ATTR_CAMPAIGN,b.ATTR_ADV_PARTNER_NAME,b.AD_SET_NAME,b.AD_NAME,COUNT(DISTINCT a.user_id) AS no_of_singups,
		 COUNT(DISTINCT(CASE WHEN DATE(d.FDD) = DATE(a.rdate) THEN a.user_id END)) AS became_ftd_within_0_day,
		 COUNT(DISTINCT(CASE WHEN d.FDD < ADDDATE(a.rdate,INTERVAL 3 DAY) THEN a.user_id END)) AS became_ftd_within_3_day,
		 COUNT(DISTINCT(CASE WHEN d.FDD < ADDDATE(a.rdate,INTERVAL 5 DAY) THEN a.user_id END)) AS became_ftd_within_5_day,
		 COUNT(DISTINCT(CASE WHEN d.FDD < ADDDATE(a.rdate,INTERVAL 7 DAY) THEN a.user_id END)) AS became_ftd_within_7_day,
		 COUNT(DISTINCT(CASE WHEN d.FDD < ADDDATE(a.rdate,INTERVAL 14 DAY) THEN a.user_id END)) AS became_ftd_within_14_day,
		 COUNT(DISTINCT(CASE WHEN d.FDD < ADDDATE(a.rdate,INTERVAL 30 DAY) THEN a.user_id END)) AS became_ftd_within_30_day,
		 COUNT(DISTINCT(CASE WHEN d.FDD < ADDDATE(a.rdate,INTERVAL 60 DAY) THEN a.user_id END)) AS became_ftd_within_60_day,
		 COUNT(DISTINCT(CASE WHEN d.FDD < ADDDATE(a.rdate,INTERVAL 90 DAY) THEN a.user_id END)) AS became_ftd_within_90_day,
		 COUNT(DISTINCT(CASE WHEN d.FDD < ADDDATE(a.rdate,INTERVAL 180 DAY) THEN a.user_id END)) AS became_ftd_within_180_day,
		 COUNT(DISTINCT(CASE WHEN d.FDD IS NOT NULL THEN d.user_id END)) AS became_ftd_life_time,
		 SUM(d0_deposit_count) AS d0_deposit_count,
		 SUM(d3_deposit_count) AS d3_deposit_count,SUM(d5_deposit_count) AS d5_deposit_count,
		 SUM(d7_deposit_count) AS d7_deposit_count,SUM(d14_deposit_count) AS d14_deposit_count,SUM(d30_deposit_count) AS d30_deposit_count,
		 SUM(d60_deposit_count) AS d60_deposit_count,SUM(d90_deposit_count) AS d90_deposit_count,SUM(d180_deposit_count) AS d180_deposit_count,
		 SUM(d0_amount) AS d0_amount,
		 SUM(d3_amount) AS d3_amount,SUM(d5_amount) AS d5_amount,SUM(d7_amount) AS d7_amount,SUM(d14_amount) AS d14_amount,SUM(d30_amount) AS d30_amount,
		 SUM(d60_amount) AS d60_amount,SUM(d90_amount) AS d90_amount,SUM(d180_amount) AS d180_amount,
		 SUM(lifetimedepositamount) AS lifetimedepositamount,
		 SUM(d0_revenue) AS d0_revenue,
		 SUM(d3_revenue) AS d3_revenue,
		 SUM(d5_revenue) AS d5_revenue,
		 SUM( d7_revenue) AS d7_revenue,
		 SUM(d14_revenue) AS d14_revenue,
		 SUM(d30_revenue) AS d30_revenue,
		 SUM(d60_revenue) AS d60_revenue,
		 SUM(d90_revenue) AS d90_revenue,
		 SUM(d180_revenue) AS d180_revenue,
		 SUM(lifetime_revenue) AS lifetime_revenue
		 FROM (SELECT user_id,(REGISTRATION_TIMESTAMP) AS rdate FROM `user` ) a
		 JOIN branch_event_tracking b ON a.user_id=b.user_id
		 LEFT JOIN (SELECT r.user_id,
		 SUM(CASE WHEN r.report_date < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 1 DAY) THEN r.total_rake END ) AS d0_revenue,
		 SUM(CASE WHEN r.report_date < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 3 DAY) THEN r.total_rake END ) AS d3_revenue,
		 SUM(CASE WHEN r.report_date < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 5 DAY) THEN r.total_rake END ) AS d5_revenue,
		 SUM(CASE WHEN r.report_date < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 7 DAY) THEN r.total_rake END ) AS d7_revenue,
		 SUM(CASE WHEN r.report_date < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 14 DAY) THEN r.total_rake END ) AS d14_revenue,
		 SUM(CASE WHEN r.report_date < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 30 DAY) THEN r.total_rake END ) AS d30_revenue,
		 SUM(CASE WHEN r.report_date < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 60 DAY) THEN r.total_rake END ) AS d60_revenue,
		 SUM(CASE WHEN r.report_date < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 90 DAY) THEN r.total_rake END ) AS d90_revenue,
		 SUM(CASE WHEN r.report_date < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 180 DAY) THEN r.total_rake END ) AS d180_revenue,
		 SUM(r.total_rake) AS lifetime_revenue FROM
		 user_turnover_report_daily r JOIN user u ON u.user_id=r.user_id GROUP BY user_id)c
		 ON a.user_id=c.user_id
		 LEFT JOIN
		 ( SELECT user_id,MIN(PAYMENT_TRANSACTION_CREATED_ON) AS FDD FROM payment_transaction
		 WHERE TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111)
		 AND PAYMENT_TRANSACTION_STATUS IN (103 , 125)
		 GROUP BY user_id HAVING MIN(PAYMENT_TRANSACTION_CREATED_ON) BETWEEN CURDATE()- INTERVAL 120 DAY AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59') ) d ON a.user_id=d.user_id
		 LEFT JOIN
		 ( SELECT p.user_id,SUM(PAYMENT_TRANSACTION_AMOUNT) AS lifetimedepositamount,COUNT(*)
		 AS lifetimedepositcount,
		 COUNT((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 1 DAY) THEN p.user_id END)) AS d0_deposit_count,
		 COUNT((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 3 DAY) THEN p.user_id END)) AS d3_deposit_count,
		 COUNT((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 5 DAY) THEN p.user_id END)) AS d5_deposit_count,
		 COUNT((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 7 DAY) THEN p.user_id END)) AS d7_deposit_count,
		 COUNT((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 14 DAY) THEN p.user_id END)) AS d14_deposit_count,
		 COUNT((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 30 DAY) THEN p.user_id END)) AS d30_deposit_count,
		 COUNT((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 60 DAY) THEN p.user_id END)) AS d60_deposit_count,
		 COUNT((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 90 DAY) THEN p.user_id END)) AS d90_deposit_count,
		 COUNT((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 180 DAY) THEN p.user_id END)) AS d180_deposit_count,
		 SUM((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 1 DAY ) THEN p.PAYMENT_TRANSACTION_AMOUNT END)) AS d0_amount,
		 SUM((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 3 DAY ) THEN p.PAYMENT_TRANSACTION_AMOUNT END)) AS d3_amount,
		 SUM((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 5 DAY ) THEN p.PAYMENT_TRANSACTION_AMOUNT END)) AS d5_amount,
		 SUM((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 7 DAY ) THEN p.PAYMENT_TRANSACTION_AMOUNT END)) AS d7_amount,
		 SUM((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 14 DAY ) THEN p.PAYMENT_TRANSACTION_AMOUNT END)) AS d14_amount,
		 SUM((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 30 DAY ) THEN p.PAYMENT_TRANSACTION_AMOUNT END)) AS d30_amount,
		 SUM((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 60 DAY ) THEN p.PAYMENT_TRANSACTION_AMOUNT END)) AS d60_amount,
		 SUM((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 90 DAY ) THEN p.PAYMENT_TRANSACTION_AMOUNT END)) AS d90_amount,
		 SUM((CASE WHEN p.PAYMENT_TRANSACTION_CREATED_ON < ADDDATE(u.REGISTRATION_TIMESTAMP,INTERVAL 180 DAY ) THEN p.PAYMENT_TRANSACTION_AMOUNT END)) AS d180_amount
		 FROM payment_transaction p
		 JOIN user u ON u.user_id=p.user_id
		 WHERE TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111)
		 AND PAYMENT_TRANSACTION_STATUS IN (103 , 125) GROUP BY user_id) e ON a.user_id=e.user_id
		 WHERE DATE(a.rdate) BETWEEN CURDATE()- INTERVAL 120 DAY AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		 GROUP BY DATE(a.rdate),b.ATTR_CHANNEL, b.ATTR_CAMPAIGN,b.ATTR_ADV_PARTNER_NAME,b.AD_SET_NAME,b.AD_NAME;");
	}

	public function signUpCode()
	{

		return $result = DB::connection('slave')->select("SELECT DATE(c.rdate) AS rd,abcd.PROMO_CAMPAIGN_NAME,abcd.PROMO_CAMPAIGN_CODE,abcd.numofsignup,
		COUNT((CASE WHEN a.PAYMENT_TRANSACTION_CREATED_ON = c.FDD THEN a.user_id END)) AS total_depositors,
		SUM((CASE WHEN a.PAYMENT_TRANSACTION_CREATED_ON = c.FDD THEN a.PAYMENT_TRANSACTION_AMOUNT END)) AS first_time_depo_amount,
		SUM(PAYMENT_TRANSACTION_AMOUNT) AS totaldepo,COUNT(*)AS totaldepocount,
		SUM(DISTINCT b.rev) AS totalREVENUE 
		FROM payment_transaction a FORCE INDEX(user_id)
		
		JOIN (SELECT p.user_id,e.rdate,MIN(PAYMENT_TRANSACTION_CREATED_ON) AS FDD,achannel,acam
		FROM payment_transaction p FORCE INDEX(user_id)
		JOIN (SELECT nu.user_id,DATE(REGISTRATION_TIMESTAMP) AS rdate,PROMO_CAMPAIGN_NAME AS acam,PROMO_CAMPAIGN_CODE AS achannel
		FROM `user` nu LEFT JOIN campaign_to_user ctu ON ctu.user_id = nu.user_id LEFT JOIN promo_campaign b ON ctu.PROMO_CAMPAIGN_ID=b.PROMO_CAMPAIGN_ID
		WHERE REGISTRATION_TIMESTAMP  BETWEEN CONCAT(CURDATE()- INTERVAL 60 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')) AS e ON e.user_id = p.user_id
		WHERE TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111) AND PAYMENT_TRANSACTION_STATUS IN (103 , 125)
		GROUP BY user_id,e.rdate,achannel,acam HAVING MIN(PAYMENT_TRANSACTION_CREATED_ON)  BETWEEN CONCAT(CURDATE()- INTERVAL 60 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')) c ON a.user_id = c.user_id
		
		LEFT JOIN (
		SELECT USER_ID,SUM(`TOTAL_RAKE`) AS rev FROM `user_turnover_report_daily` WHERE 
		`REPORT_DATE` < CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59') AND
		USER_ID IN (SELECT tb1.user_id FROM (
		SELECT p.user_id,e.rdate,MIN(PAYMENT_TRANSACTION_CREATED_ON) AS FDD,achannel,acam
		FROM payment_transaction p FORCE INDEX(user_id)
		JOIN (SELECT nu.user_id,DATE(REGISTRATION_TIMESTAMP) AS rdate,PROMO_CAMPAIGN_NAME AS acam,PROMO_CAMPAIGN_CODE AS achannel
		FROM `user` nu LEFT JOIN campaign_to_user ctu ON ctu.user_id = nu.user_id LEFT JOIN promo_campaign b ON ctu.PROMO_CAMPAIGN_ID=b.PROMO_CAMPAIGN_ID
		WHERE REGISTRATION_TIMESTAMP  BETWEEN CONCAT(CURDATE()- INTERVAL 60 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')) AS e ON e.user_id = p.user_id
		WHERE TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111) AND PAYMENT_TRANSACTION_STATUS IN (103 , 125)
		GROUP BY user_id,e.rdate,achannel,acam HAVING MIN(PAYMENT_TRANSACTION_CREATED_ON)  BETWEEN CONCAT(CURDATE()- INTERVAL 60 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59'))tb1
		
		)
		GROUP BY USER_ID) b ON a.user_id = b.user_id
		
		LEFT JOIN (SELECT COUNT(DISTINCT u.user_id) AS numofsignup,PROMO_CAMPAIGN_NAME,PROMO_CAMPAIGN_CODE,
		DATE(REGISTRATION_TIMESTAMP) AS dayd FROM `user` u LEFT JOIN campaign_to_user ctu ON ctu.user_id = u.user_id
		LEFT JOIN promo_campaign b ON ctu.PROMO_CAMPAIGN_ID=b.PROMO_CAMPAIGN_ID WHERE REGISTRATION_TIMESTAMP  BETWEEN CONCAT(CURDATE()- INTERVAL 60 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY dayd,PROMO_CAMPAIGN_NAME,PROMO_CAMPAIGN_CODE) abcd ON abcd.dayd= c.rdate AND acam = abcd.PROMO_CAMPAIGN_NAME AND achannel = abcd.PROMO_CAMPAIGN_CODE
		
		WHERE TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111) AND PAYMENT_TRANSACTION_STATUS IN (103 , 125)
		AND DATE(c.rdate)  BETWEEN CONCAT(CURDATE()- INTERVAL 60 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY rd,abcd.PROMO_CAMPAIGN_NAME,abcd.PROMO_CAMPAIGN_CODE,abcd.numofsignup");
	}

	public function totalRegistration()
	{

		return $result = DB::connection('slave')->select("SELECT COUNT(DISTINCT u.user_id) AS numofsignup,ATTR_CAMPAIGN,ATTR_CHANNEL,AD_SET_NAME,AD_NAME,
		DATE(REGISTRATION_TIMESTAMP) AS dayd 
		FROM `user` u 
		LEFT JOIN branch_event_tracking b ON u.user_id = b.user_id 
		WHERE REGISTRATION_TIMESTAMP BETWEEN CONCAT(CURDATE()- INTERVAL 60 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')       
		GROUP BY dayd,ATTR_CAMPAIGN,ATTR_CHANNEL,AD_SET_NAME,AD_NAME;");
	}
}
