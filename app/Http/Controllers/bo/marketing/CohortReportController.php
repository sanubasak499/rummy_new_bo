<?php

namespace App\Http\Controllers\bo\marketing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AdminUser;
use stdClass;
use DB;
use \PDF;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Carbon\Carbon;
use Mail;
use Storage;
use App;

class CohortReportController extends Controller {

    public function index(Request $request) {
        $today = Carbon::today()->setTime("00", "00", "00")->format('d-M-Y H:i:s');
        return view('bo.views.marketing.cohortreport.cohortreport', ['today' => $today]);
    }

    public function CohortXportXlsx(Request $request) {

        $before = "";
        $diffDays = 31;
        if (isset($request->date_from) && !empty($request->date_from)) {
            $before = "|before:" . Carbon::parse($request->date_from)->addDays($diffDays)->toDateString();
        }

        Validator::make($request->all(), [
            'date_from' => 'required|date',
            'date_to' => 'required|date|after_or_equal:date_from' . $before,
                ], ['before' => "Date difference must be less than or equal to $diffDays days"])->validate();

        $date_from = Carbon::parse($request->date_from)->toDateString(); //->format('Y-m-d');
        $date_to = Carbon::parse($request->date_to)->toDateString(); //->format('Y-m-d');

        $data = DB::select("call usp_marketing_stat_report('$date_from','$date_to')");
        /*
         * This Loop find an Integer field with zero then convert zero to string zero
         */
        foreach ($data as $key => $value) {
            foreach ($value as $key1 => $newVal) {
                if (is_integer($data[$key]->{$key1}) && $newVal == 0) {
                    $data[$key]->{$key1} = '0';
                }
            }
        }/* Loop end */
        $dataArr = json_decode(json_encode($data), true);

        $header = [];
        if (is_array($dataArr) && count($dataArr) > 0) {
            $header = array_keys($dataArr[0]);
            $fileName = "cohort_report_" . str_replace(" ", "-", $date_from) . "_" . str_replace(" ", "-", $date_to) . ".xlsx";
            return Excel::download(new CollectionExport($dataArr, $header), $fileName);
        } else {
            return back()->with('custom_message', ["title" => "Info", "text" => "No record found."]);
        }
    }

    public function scheduleCRONmailCohortReport(Request $request, $days = null) {
        ##Checking and init default date.
        if ($days != null) {
            $days = (int) $days;
        }
        if (is_integer($days) && ($days > 36 || $days < 1) || $days == null) {
            $days = 36;
        }

        $date_from = $data['date_from'] = Carbon::now()->subDays($days)->toDateString(); //->format('Y-m-d');
        $date_to = $data['date_to'] = Carbon::now()->subDays(1)->toDateString(); //->format('Y-m-d'); 

        $DBdata = DB::select("call usp_marketing_stat_report('$date_from','$date_to')");
        /*
         * This Loop find an Integer field with zero then convert zero to string zero
         */
        foreach ($DBdata as $key => $value) {
            foreach ($value as $key1 => $newVal) {
                if (is_integer($DBdata[$key]->{$key1}) && $newVal == 0) {
                    $DBdata[$key]->{$key1} = '0';
                }
            }
        }/* Loop end */
        $dataArr = json_decode(json_encode($DBdata), true);

        $data['mail_env'] = '';
        $env = App::environment();
        $env = ($env)?$env:'local';
        if ($env == 'local') {
            $data['mail_env'] = 'Testing Mail ';
        }

        ## get file location
        $file = 'cron_file/' . "Cohort_Report_" . $data['date_from'] . "_" . $data['date_to'] . ".xlsx";
        ## check file and delete if exists
        if (Storage::disk('local')->exists($file)) {
            Storage::delete($file);
        }
        $header = [];
        if (is_array($dataArr) && count($dataArr) > 0) {
            $header = array_keys($dataArr[0]);
        }
        $data['idEmpty'] = false;
        if (isset($dataArr) && count($dataArr) <= 0) {
            $data['idEmpty'] = true;
        }
        ## generate new file and then send mail.
        if (Excel::store(new CollectionExport($dataArr, $header), $file, 'local')) {
            Mail::send("bo.mail.cohortReportMail", $data, function ($message) use($data, $dataArr, $file) {
                $message->from(config('poker_config.mail.from.support'), 'Cohort Report');
                $message->to(config('poker_config.cohort_report_mail_to.emails'), $data['mail_env'])
                        ->subject($data['mail_env'] . 'PokerBaazi Cohort report for last 35 days');
                if (!$data['idEmpty']) {
                    $message->attach(storage_path('app/' . $file), [
                        'as' => $file,
                        'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                    ]);
                }
            }); //Mail Close
            if (count(Mail::failures()) <= 0 && Storage::disk('local')->exists($file)) {
                Storage::delete($file);
            }
        }// if close if(Excel::store(new CollectionExport($dataArr,$header),$file,'local')){
    }
}
