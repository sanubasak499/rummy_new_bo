<?php

namespace App\Http\Controllers\bo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function __construct(){
        parent::__construct();
    }
}
