<?php

namespace App\Http\Controllers\bo\baazirewards;

use Illuminate\Http\Request;
use App\Http\Controllers\bo\BaseController;
use App\Models\RewardCashbackLevel;
use Auth;
use App\Facades\PokerBaazi;



/**
 * Author: Nitesh Kumar Jha
 * Purpose:  Insert and update Reward CashbackLevel 
 * Created Date: 24-3-2020
 * modify date: 25-9-2020
 */

class CashbackLevelsController extends BaseController
{
    //
    public function index()
    {  
        $RewardCashbackLevelResult = RewardCashbackLevel::select('LEVEL_ID', 'MIN_REWARD_POINTS','MAX_REWARD_POINTS','CASHBACK_AMOUNT')
            ->orderBy('LEVEL_ID', 'DESC')
            ->get();
       return view('bo.views.cashbacklevels.index',['RewardCashbackLevelResult' => $RewardCashbackLevelResult]);
    }

    public function insertCashbackLevel(Request $request)
    {
     
        request()->page;
        $request->validate([
            "MIN_REWARD_POINTS" => 'required',
            "MAX_REWARD_POINTS" => 'required',
            "CASHBACK_AMOUNT" =>'required',
        ]);

        $insert = [
            'MIN_REWARD_POINTS' => $request->MIN_REWARD_POINTS,
            'MAX_REWARD_POINTS' => $request->MAX_REWARD_POINTS,
            'CASHBACK_AMOUNT' =>  $request->CASHBACK_AMOUNT
        ];
        
        $data = RewardCashbackLevel::insert($insert);
                //Start user Activity Tracking 
                $activity = [
                    'admin' => Auth::user()->id,
                    'module_id' => 70,
                    'action' => "Add Cashback levels",
                    'data' => json_encode($insert)
                ];
                PokerBaazi::storeActivity($activity);
                if ($data == 1) {
                    return redirect()->back()->with('success', "Insert Data Successfully");
                } else {
                    return redirect()->back()->with('error', "Insert Data Failed.");
                }
               
    }


    public function UpdateCashbackLevel(Request $request)
    {
     
        request()->page;
        $request->validate([
            "MIN_REWARD_POINTS" => 'required',
            "MAX_REWARD_POINTS" => 'required',
            "CASHBACK_AMOUNT" => 'required',
            "LEVEL_ID" => 'required',
        ]);
       
        $update = [
            'MIN_REWARD_POINTS' => $request->MIN_REWARD_POINTS,
            'MAX_REWARD_POINTS' => $request->MAX_REWARD_POINTS,
            'CASHBACK_AMOUNT' =>  $request->CASHBACK_AMOUNT
        ];
       
        $data = RewardCashbackLevel::where('LEVEL_ID', $request->LEVEL_ID)
        ->update($update);
                //Start user Activity Tracking 
                $activity = [
                    'admin' => Auth::user()->id,
                    'module_id' => 70,
                    'action' => "Update Cashback levels",
                    'data' => json_encode($update)
                ];
                PokerBaazi::storeActivity($activity);
                if ($data == 1) {
                    return redirect()->back()->with('success', "Update Successfully");
                } else {
                    return redirect()->back()->with('error', "Update Failed.");
                }
               
                
    }


}
