<?php
/**
 * Author : Brijesh Kumar
 * Date   : 27-Dec-2019 15:52 
 */
namespace App\Http\Controllers\bo\livetournament;

use App\Http\Controllers\bo\BaseController;
use App\Models\BranchWebhook;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\bo\user\UserController;
use App\Models\Tournament;

use Validator;
use Illuminate\Validation\Rule;
use DB;

class LiveTournamentEntriesController extends BaseController
{
    // Load live tournament search page
    public function index()
    {
       $tornamentlist = Tournament::liveTournamentList();
        return view('bo.views.livetournament.entries',['tournament_list'=>$tornamentlist]);
    }
    public function ajaxGetUserStatus(Request $request) {

        $validator = \Validator::make($request->all(), [
            'username' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->all()]);
        }

        #Call to helper function to check status of user
        $result = [];
        $result['status'] = getUserIdFromUsername($request->username);
        if(!$result['status']){
           $result['message'] = "Username does't exists";
        }else{
            $result['message'] = "Username exists";
        }
        return response()->json($result);
    }
    /**
     * This function is used to store Live Entries Resources
     * 
     */
    public function store(Request $request){
        
        
       //echo  Tournament::getTournamentByeinPlusEntryFee($request->tournament);exit;
        Validator::make($request->all(), [
            'username' => [
                            'required',
                            function ($attribute, $value, $fail) use($request) { 
                                $userId = getUserIdFromUsername($value);
                                if(!isset($value) && empty($value)){
                                    $fail('Invalid username value.');
                                    //return false;
                                }else if($userId == false ){
                                    $fail("Username does't exists.");
                                }
                            }  
            ],
            'tournament' => [
                                'required',
                                'integer',
                                'min:0',
                                // Custom validation
                                function ($attribute, $value, $fail) use($request) { 
                                    
                                    if(!isset($value) && empty($value)){
                                        $fail('Invalid Tournament value.');
                                        //return false;
                                    }elseif($request->action == '1'){
                                        $tournament_amount = Tournament::getTournamentByeinPlusEntryFee($value)->TOURNAMENT_AMOUNT;
                                        if($tournament_amount != $request->amount){
                                            $fail('Amount value of ' . $attribute.' is invalid.');
                                        }
                                    }
                                }   
                            ],
            'amount' => 'required|numeric|min:0',
            'action' => [
                            'required',
                            Rule::in(['1', '2','3']),
                        ],
        ])->validate();
        $userId = getUserIdFromUsername($request->username);  
        $RefrenceNoPrefix =  $this->getTournamentGameTypeCode($request->tournament)->ref_game_code;
        //dd($RefrenceNoPrefix);
        $InternalRefrenceNo = $RefrenceNoPrefix . $userId . date('YmdHis');
        //call sp_player_tournament_registration(204743,10400,@regstatus);
        switch($request->action){
            case '1' : #Case 1 for "Buy in"
                $query = DB::raw("call sp_player_tournament_registration('$request->tournament','$userId',@out1)");
                $status = DB::select($query);
                $outQuery = DB::raw("select @out1 as result");
                $outStatus = DB::select($outQuery);
                
                if($outStatus[0]->result){
                    return back()->with('success', "Data saved successfully.");
                }else{
                    return back()->with('custom_message', ["title"=>"Operation failed","text"=>"User already exists/Insufficient balance."]);
                }
            break;
           
            case '2' : #Case 2 for "Tournament Win"
                $status = DB::select('call sp_player_tournament_win_update(?,?,?,?,?,?,?,?)',array($request->tournament,$userId,'0',"$InternalRefrenceNo",'1',$request->amount,'0','0'));
            break;
            
            case '3' : #Case 3 for "Bountey Win"
                $status = DB::select('call sp_player_tournament_win_update(?,?,?,?,?,?,?,?)',array($request->tournament,$userId,'0',"$InternalRefrenceNo",'1','0','0',$request->amount));
            break;

        }        

        return back()->with('success', "Data saved successfully.");
    
    }
    /**
     * This function Returns Code prefix of Internal_Refrence_Code e.g 'AAA'
     * 
     * Note : Need to be a common function because it is being use on differenct controller.
     * @param type $tournament_id
     * @return type
     */
    private function getTournamentGameTypeCode($tournament_id) {
        $refCode = DB::table('tournament')->select('minigames_type.ref_game_code')->join('minigames_type', 'minigames_type.MINIGAMES_TYPE_ID', '=', 'tournament.mini_game_type_id')
        // ->where(['something' => 'something', 'otherThing' => 'otherThing'])
        ->where(['tournament.tournament_id' => $tournament_id])->first();
        return $refCode;
    }
    
}
