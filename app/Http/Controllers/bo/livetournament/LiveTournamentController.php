<?php

/**
 * Author: Nitesh Kumar Jha
 * Description: All Function Related to Live Tournament Result , Update ASSIGN TABLE ,Allow Re-Buy,Close Let Registration 
 * Created Date: 3-12-2019 to 4-12-2019
 * Modify Date: 8-1-2020 to 9-1-20120
 *  
 */
namespace App\Http\Controllers\bo\livetournament;

use App\Http\Controllers\bo\BaseController;
use Illuminate\Http\Request;
use App\Models\Tournament;
use App\Models\ReEntryRegistration;
use App\Models\TournamentRegistration;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\bo\livetournament\Exports\LiveTournamentExport;
use Carbon\Carbon;
use DB;
use Auth;
use App\Facades\PokerBaazi;


class LiveTournamentController extends BaseController
{
    public function __construct()
    {
        $this->tournamentModel = app(Tournament::class);
    }

    // Load live tournament search page
    public function index(){
        return view('bo.views.livetournament.search');
    }

    // Load live Tournament search result
    public function searchResult(Request $request){
        $perPage = config('poker_config.paginate.per_page');
        if (!$request->isMethod('post')) {
            return view('bo.views.livetournament.search');
        }
        $page = request()->page;
        $date_to = $request->date_to;
        $date_from = $request->date_from;
        
        
        if ($date_to != '' && $date_from != '') {
            $request->session()->put('date_from', $date_from);
            $request->session()->put('date_to', $date_to);
        } else{
            $request->session()->forget('date_from', $date_from);
            $request->session()->forget('date_from', $date_from);
        }

        $liveResult = Tournament::select('TOURNAMENT_ID', 'TOURNAMENT_NAME', 'TOURNAMENT_START_TIME', 'REGISTRATED_PLAYER_COUNT', 'TOURNAMENT_STATUS')
            ->when(!empty($request->date_from), function ($query) use ($request) {
                $date_from = Carbon::parse($request->date_from);
                return $query->where('TOURNAMENT_START_TIME', ">=", $date_from);
            })
            ->when(!empty($request->date_to), function ($query) use ($request) {
                $date_to = Carbon::parse($request->date_to);
                return $query->where('TOURNAMENT_START_TIME', "<=", $date_to);
            })

            ->where('TOURNAMENT_TYPE_ID', 12)
            ->where('IS_ACTIVE', '1')
            ->whereIn('TOURNAMENT_STATUS', array(0, 1, 2, 3, 4, 6))
            ->orderBy('TOURNAMENT_START_TIME', 'DESC')
            ->paginate($perPage, ['*'], 'page', $page);
        $params = $request->all();
        $params['page'] = $page;
        return view('bo.views.livetournament.search', ['liveResult' => $liveResult, 'params' => $params]);
    }

    // Load live tournament details view and data 
    public function liveTournamentDetails($TOURNAMENT_ID){

        $res = Tournament::from(app(Tournament::class)->getTable()." as t")
					->select('t.TOURNAMENT_ID','t.TOURNAMENT_NAME','t.TOURNAMENT_START_TIME','t.REGISTRATED_PLAYER_COUNT',
					't.TOURNAMENT_STATUS','tr.PLAYER_NAME','tr.USER_ID','u.CONTACT','u.EMAIL_ID','tr.REGISTERED_DATE','tr.LIVE_TABLE_NO',DB::raw("'Buyin' as Buyin"))
					->join('tournament_registration as tr', 'tr.TOURNAMENT_ID' ,'=', 't.TOURNAMENT_ID')
					->join('user as u', 'u.USER_ID' ,'=', 'tr.USER_ID')
					->where('t.TOURNAMENT_ID',$TOURNAMENT_ID )
					->orderBy('tr.REGISTERED_DATE', 'DESC');

		$union = ReEntryRegistration::from(app(ReEntryRegistration::class)->getTable()." as ret")
					->select('ret.TOURNAMENT_ID','t.TOURNAMENT_NAME','t.TOURNAMENT_START_TIME','t.REGISTRATED_PLAYER_COUNT','t.TOURNAMENT_STATUS',
					'ret.USER_NAME','ret.USER_ID','USER_ID','USER_ID','ret.CREATED_DATE','t.TOURNAMENT_END_TIME', 'RE_ENTRY_COUNT')
					->join('tournament as t', 't.TOURNAMENT_ID' ,'=', 'ret.TOURNAMENT_ID')
					->where('ret.TOURNAMENT_ID',$TOURNAMENT_ID )
					->orderBy('ret.CREATED_DATE', 'DESC');
                   $results = $res->union($union)->get();
                    
        return view('bo.views.livetournament.liveTournamentDetails', ['results' => $results]);
    }

    // This function use to closed ket registration
    public function lateregistrationClosed($tournament_id){
        if ($tournament_id != '') {
            $update = ['LATE_REGISTRATION_ALLOW' => 0];
            $data['arrs'] =  Tournament::where('TOURNAMENT_ID', $tournament_id)
                ->update($update);
             // Start User Activity tracking
            $activity = [
                'admin'=>Auth::user()->id,
                'module_id'=>30,
                'action' => "Late Registration Closed",
                'data' => json_encode($update)
                ];
            PokerBaazi::storeActivity($activity);
            // End User Activity tracking 
            if ($data['arrs'] == 1) {
                return redirect()->back()->with('success', "Late Registration Closed Successfully");
            } else {
                return redirect()->back()->with('error', "Late Registration Closed");
            }
        }
    }

    // This function use to update re-buy coin
    public function updateRebuy($tid, $id){
        if (isset($tid) != "" && isset($id) != "") {
            $update = ['STATUS' => 5];
            $data['arrs'] = TournamentRegistration::where('TOURNAMENT_ID', $tid)
                ->where('USER_ID', $id)
                ->update($update);
                
            // Start User Activity tracking
            $activity = [
                'admin'=>Auth::user()->id,
                'module_id'=>30,
                'action' => "Update Re-Buy",
                'data' => json_encode($update)
                ];
            PokerBaazi::storeActivity($activity);
            // End User Activity tracking 

            if ($data['arrs'] == 1) {
                return redirect()->back()->with('success', "Re-Buy Updated successfully.");
            } else {
                return redirect()->back()->with('error', "Re-Buy Updation Failed.");
            }
            return redirect()->back()->with('error', "Re-Buy Updation Failed.");
        }
    }

    // This function use to call processor to update un-registration
    public function updateUnRegistration($tid, $id){
        if (isset($tid) != "" && isset($id) != "") {
            $update = ['TOURNAMENT_ID' =>$tid,
                       'USER_ID' => $id
                      ];
            $queryData = "call sp_tournament_unregistration('$tid','$id')";
            
            $data =  DB::select($queryData);
        
            // $dataresult = $data[0]->ticketStatus;
            // Start User Activity tracking
            $activity = [
                'admin'=>Auth::user()->id,
                'module_id'=>30,
                'action' => "Call Processor to update un-registration",
                'data' => json_encode($update)
                ];
            PokerBaazi::storeActivity($activity);
            // End User Activity tracking 
        }
        if (!empty($data[0]->refNo)) {
            return redirect()->back()->with('success', "User UnRegister successfully.");
        } else {
            return redirect()->back()->with('error', "User UnRegister Failed.");
        }
    }

    // This function use to update assign table 
    public function updateAssignTable(Request $request){
        $data = $request->all();
        //print_r($data);die();
        $tournament_id = $request->input('tournament_id');
        $user_count = $request->input('user_count');
        
         $i=0;
        while ($i < $user_count ){
            $USER_ID = $request->input('USER_ID' . $i);
            $live_table_no = $request->input('live_table_no' . $i);
            $update = ['LIVE_TABLE_NO' => $live_table_no];
            $updateArray['array'] = ['LIVE_TABLE_NO' => $live_table_no];
            $data['arrs'] = TournamentRegistration::where('TOURNAMENT_ID', $tournament_id)
                ->where('USER_ID', $USER_ID)
                ->update($update);
         $i++;
        }
     
        // Start User Activity Tracking 
        $activity = [
        'admin'=>Auth::user()->id,
        'module_id'=>30,
        'action' => "Update Assign Table",
        'data' => json_encode($updateArray['array'])
        ];
        PokerBaazi::storeActivity($activity);
        // End User Activity Tracking 
        if ($data['arrs'] == 1) {
            return redirect()->back()->with('success', "Assign Table Updated successfully.");
        } else {
            return redirect()->back()->with('error', "Assign Table Updation Failed");
        }
    }

    // This function use to get tournament status name
    public static function getTournamentStatusName($status_id){
        $statusNameResult = array("0"=>"Announced", "1"=>"Registering", "2"=>"Seating","3"=>"Started","4"=>"Breaking","5"=>"Finished","6"=>"Cancelled","7"=>"Announced"); 
        if(array_key_exists($status_id, $statusNameResult)){
           return $statusName = $statusNameResult[$status_id];
        }else{
            return $statusName = $statusNameResult['0'];
        }
    }

    // This function use to count re entry registration 
    public static function countReEntryRegistration($tournamnetid, $userid){
        $results = ReEntryRegistration::selectRaw('count(*) as reentry')
            ->where('TOURNAMENT_ID', $tournamnetid)
            ->where('USER_ID', $userid)
            ->get();
        return $results;
    }

    // Live Tournament Export To Excel 
    public function exportExcel(){
        return $result = Excel::download(new LiveTournamentExport, 'LiveTournamet.xlsx');
    }
}
