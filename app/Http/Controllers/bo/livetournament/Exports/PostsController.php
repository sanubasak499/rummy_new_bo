<?php

namespace App\Http\Controllers\bo\livetournament\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Facades\Excel;
//use App\Exports\UsersExport;
use App\Http\Controllers\bo\livetournament\Exports\UsersExport;
use App\Imports\UsersImport;
use App\Http\Controllers\Controller\bo\livetournament\LiveTournamentController;
use DB;

class PostsController extends Controller
{
   
 public function index()
    {
     $customer_data = DB::table('tournament')->get();
     return view('bo.views.livetournament.export_excel')->with('customer_data', $customer_data);
    }

    public function exportExcel(request $request) 
    {
        return $result = Excel::download(new UsersExport, 'users.xlsx');
    }


}
