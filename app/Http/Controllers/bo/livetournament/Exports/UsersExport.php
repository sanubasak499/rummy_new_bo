<?php
  
namespace App\Http\Controllers\bo\livetournament\Exports;
use DB;
use App\Models\Tournament;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Carbon\Carbon;
  
class UsersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      $date_from = session()->get('date_from');
      $date_to = session()->get('date_to');
      if($date_from!='' && $date_to!='') {
        $date_from = Carbon::parse($date_from);
        $date_to = Carbon::parse($date_to);

        return Tournament::select('TOURNAMENT_ID','TOURNAMENT_NAME','TOURNAMENT_START_TIME', 'REGISTRATED_PLAYER_COUNT','TOURNAMENT_STATUS')
        ->whereDate('TOURNAMENT_START_TIME', '>=', $date_from)
        ->whereDate('TOURNAMENT_START_TIME', '<=', $date_to)
        ->where('TOURNAMENT_TYPE_ID',12)
        ->where('IS_ACTIVE','1')
        ->whereIn('TOURNAMENT_STATUS', array(0,1,2,3,4,6))
        ->orderBy('TOURNAMENT_START_TIME', 'DESC')
        ->get();
      } else {
  
        return Tournament::select('TOURNAMENT_ID','TOURNAMENT_NAME','TOURNAMENT_START_TIME', 'REGISTRATED_PLAYER_COUNT','TOURNAMENT_STATUS')
        ->where('TOURNAMENT_TYPE_ID',12)
        ->where('IS_ACTIVE','1')
        ->whereIn('TOURNAMENT_STATUS', array(0,1,2,3,4,6))
        ->orderBy('TOURNAMENT_START_TIME', 'DESC')
        ->get();
      }
        
    }
    public function headings(): array
    {
        return [
            'TOURNAMENT_ID',
            'TOURNAMENT_NAME',
            'TOURNAMENT_START_TIME',
            'REGISTRATED_PLAYER_COUNT',
            'TOURNAMENT_STATUS',
        ];
    }
}