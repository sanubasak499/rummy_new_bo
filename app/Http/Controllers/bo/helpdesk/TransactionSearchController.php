<?php

namespace App\Http\Controllers\bo\helpdesk;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use App\Models\CoinType;
use App\Models\User;
use DB;
use Carbon\Carbon;
use Session;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CollectionExport;

class TransactionSearchController extends Controller
{
    //

    public function index(Request $request)
    {
        $perPage = config('poker_config.paginate.per_page');
        $coinTypes = $this->getAllCoinTypes();
        $transTypes = $this->getAllTransactionTypes();
        if (!$request->isMethod('post')) {
            return view('bo.views.helpdesk.transactionSearch', ['coinTypes' => $coinTypes, 'transTypes' => $transTypes]);
        } else {
            $page = request()->page;

            $username          = $request->username;
            $ref_no            = $request->ref_no;
            $amount            = $request->amount;
            $coinType          = $request->coin_type;
            $transactionType   = $request->transaction_type;
            $transactionStatus = $request->transaction_status;
            $dateFrom          = $request->date_from;
            $dateTo            = $request->date_to;
            $search_by         = $request->search_by;
            $pvt_table         = $request->private_table;
            $diff = strtotime($dateTo) - strtotime($dateFrom);
            $days = abs(round($diff / 86400));
            if (empty($ref_no) && empty($dateTo) && empty($dateFrom)) {
                return Redirect::back()->with('message', 'Please add Ref No or Date Range!');
            }
            if ((empty($ref_no) && empty($username)) && $days > 3) {
                return Redirect::back()->with('message', 'Please add Ref No or Username in filter if Date Range  is more than 3 !');
            }
            if ($days > 30) {
                return Redirect::back()->with('message', 'Date Range must be less than 30 days !');
                
            }

            Session::put('trans_username', $username);
            Session::put('trans_ref_no', $ref_no);
            Session::put('trans_amount', $amount);
            Session::put('trans_coin_type', $coinType);
            Session::put('trans_trans_type', $transactionType);
            Session::put('trans_trans_status', $transactionStatus);
            Session::put('trans_dateFrom', $dateFrom);
            Session::put('trans_dateTo', $dateTo);
            Session::put('trans_search_by', $search_by);
            Session::put('pvt_table', $pvt_table);

            $tTypeIdsTableName = array(31, 71, 13);
            $tTypeIdsTournamentName = array(70, 25, 79, 101, 102, 103, 105);
            $trans_status = array();

            if ($transactionStatus != "blank") {
                if ($transactionStatus == 1) {
                    $trans_status = array('101', '102', '103', '107', '108', '111', '125', '201', '202', '203');
                } elseif ($transactionStatus == 2) {
                    $trans_status = array('104', '109', '205');
                } elseif ($transactionStatus == 3) {
                    $trans_status = array('106', '110', '207');
                } elseif ($transactionStatus == 4) {
                    $trans_status = array('105', '112', '206');
                } elseif ($transactionStatus == 204) {
                    $trans_status = array('204', '216');
                } else {
                    $trans_status[] = $transactionStatus;
                }
            }
            if ($coinType == 3) {
                $tableName = "master_transaction_history_fppbonus";
            } else if ($coinType == 9) {
                $tableName = "master_transaction_history_coins";
            } else if ($coinType == 6) {
                $tableName = "master_transaction_history_baazi";
            } else if ($coinType == 12) {
                $tableName = "master_transaction_history_rewardpoints";
            } else if ($coinType == 16) {
                $tableName = "master_transaction_history_pokercommission";
            } else {
                $tableName = "master_transaction_history";
            }
            // $user_id = null;
            // if (isset($username)) {
            //     $user_id = $this->getUserIDFromUsername($username, $search_by);
            // }

            $transactionData = $this->getAllTrnasactionData($username, $search_by, $ref_no, $amount, $transactionType, $trans_status, $dateFrom, $dateTo, $tableName, $pvt_table);
            // print_r($transactionData->getBindings());
            // dd($transactionData->toSql());
            // $totalAmount = $transactionData->sum('mth.TRANSACTION_AMOUNT');
            $allTransactionSearchData = $transactionData->paginate($perPage, ['*'], 'page', $page);
            // dd($allTransactionSearchData->toArray());
            $params = $request->all();
            $params['page'] = $page;
            return view('bo.views.helpdesk.transactionSearch', ['allTransactionSearchData' => $allTransactionSearchData, 'coinTypes' => $coinTypes, 'transTypes' => $transTypes, 'params' => $params]);
        }
    }


    public function getAllTrnasactionData($username, $search_by, $ref_no, $amount, $transactionType, $trans_status, $dateFrom, $dateTo, $tableName, $pvt_table)
    {
        if (empty($dateTo)) {
            $dateTo = date('Y-m-d H:i:s');
        }

        $query = DB::connection('slave')->table("$tableName as mth");
        $query->join('user as u', 'u.USER_ID', '=', 'mth.USER_ID');
        //$query->join('transaction_type as tt', 'tt.TRANSACTION_TYPE_ID', '=', 'mth.TRANSACTION_TYPE_ID');
        $query->join('transaction_status as ts', 'ts.TRANSACTION_STATUS_ID', '=', 'mth.TRANSACTION_STATUS_ID');
        $query->leftJoin('payment_transaction as pt', 'pt.INTERNAL_REFERENCE_NO', '=', 'mth.INTERNAL_REFERENCE_NO');
        $query->leftJoin('game_transaction_history as gth', 'gth.INTERNAL_REFFERENCE_NO', '=', 'mth.INTERNAL_REFERENCE_NO');
        $query->leftJoin('tournament_tables as tts', 'tts.TOURNAMENT_TABLE_ID', '=', 'gth.TOURNAMENT_TABLE_ID');
        $query->leftJoin('tournament as t', 't.TOURNAMENT_ID', '=', 'tts.TOURNAMENT_ID');
        $query->leftJoin('players_private_tables_details as pptd', 'pptd.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID');
        // $query->where('pptd.USER_ID', function ($query) {
        //     $query->select(DB::raw('USERNAME AS PVT_USERNAME'))->from('user')->whereRaw('user.USER_ID = IFNULL(`pptd`.`USER_ID`,0)');
        // });

        //search username
        $query->when(!empty($username) && $search_by == "username", function ($query) use ($username) {
            return $query->Where('u.USERNAME', 'LIKE', $username . '%');
        });

        $query->when(!empty($username) && $search_by == "email", function ($query) use ($username) {
            return $query->where('u.EMAIL_ID', $username);
        });
        $query->when(!empty($username) && $search_by == "contact_no", function ($query) use ($username) {
            return $query->where('u.CONTACT', $username);
        });
        $query->when(!empty($ref_no), function ($query) use ($ref_no) {
            return $query->where('mth.INTERNAL_REFERENCE_NO', $ref_no);
        });
        $query->when(!empty($amount), function ($query) use ($amount) {
            return $query->where('mth.TRANSACTION_AMOUNT', $amount);
        });

        $query->when(!empty($transactionType), function ($query) use ($transactionType) {
            return $query->whereIn('mth.TRANSACTION_TYPE_ID', $transactionType);
        });
        $query->when(!empty($trans_status), function ($query) use ($trans_status) {
            return $query->whereIn('mth.TRANSACTION_STATUS_ID', $trans_status);
        });
        // $query->when(!empty($dateFrom), function ($query) use ($dateFrom) {
        //     $dateFrom = Carbon::parse($dateFrom)->format('Y-m-d H:i:s');
        //     return $query->whereDate('mth.TRANSACTION_DATE', ">=", $dateFrom);
        // });
        $query->when(!empty($dateFrom) && !empty($dateTo) && empty($ref_no), function ($query) use ($dateFrom, $dateTo) {
            $dateFrom = Carbon::parse($dateFrom)->format('Y-m-d H:i:s');
            $dateTo = Carbon::parse($dateTo)->format('Y-m-d H:i:s');
            return $query->whereBetween('mth.TRANSACTION_DATE', [$dateFrom, $dateTo]);
        });
        // $query->when(!empty($dateTo), function ($query) use ($dateTo) {
        //     $dateTo = Carbon::parse($dateTo)->format('Y-m-d H:i:s');
        //     return $query->whereDate('mth.TRANSACTION_DATE', "<=", $dateTo);
        // });
        $query->when(!empty($pvt_table) && $pvt_table == "pvt_yes", function ($query) use ($pvt_table) {
            return $query->where('t.PRIVATE_TABLE', 2);
        });
        $query->when(!empty($pvt_table) && $pvt_table == "pvt_no", function ($query) use ($pvt_table) {
            return $query->where('t.PRIVATE_TABLE', 0);
        });
        $query->orderBy('mth.MASTER_TRANSACTTION_ID', 'DESC');
        $query->groupBy(['mth.MASTER_TRANSACTTION_ID']);
        $transactionData = $query->select(
            'u.USERNAME',
            'mth.USER_ID',
            'mth.BALANCE_TYPE_ID',
            'mth.TRANSACTION_STATUS_ID',
            'mth.TRANSACTION_TYPE_ID',
            'mth.TRANSACTION_AMOUNT',
            'mth.TRANSACTION_DATE',
            'mth.INTERNAL_REFERENCE_NO',
            'mth.CURRENT_TOT_BALANCE',
            'mth.CLOSING_TOT_BALANCE',
            'mth.PARTNER_ID',
            // 'tt.TRANSACTION_DESCRIPTION',
            'ts.TRANSACTION_STATUS_DESCRIPTION',
            'pt.PROMO_CODE',
            'tts.TOURNAMENT_NAME',
            't.PRIVATE_TABLE',
            DB::raw('(SELECT `user`.`USERNAME` FROM `user` WHERE `user`.`USER_ID`=IFNULL(`pptd`.`USER_ID`,0)) PVT_USERNAME'),
            DB::raw('(SELECT `tt`.`TRANSACTION_DESCRIPTION` FROM `transaction_type` AS `tt`WHERE `tt`.`TRANSACTION_TYPE_ID` = `mth`.`TRANSACTION_TYPE_ID` ) TRANSACTION_DESCRIPTION'),
        );

        return $transactionData;
    }


    public function getAllCoinTypes()
    {

        $getAllCoin = CoinType::select('COIN_TYPE_ID', 'NAME')
            ->where(['status' => '1'])
            ->get();
        return $getAllCoin;
    }

    public function getAllTransactionTypes()
    {
        $getAllTransTypes = DB::table('transaction_type')->select('TRANSACTION_TYPE_NAME', 'TRANSACTION_TYPE_ID')
            ->get();
        return $getAllTransTypes;
    }

    public function getUserIDFromUsername($username, $search_by)
    {
        return $user_id = User::select('USER_ID', 'EMAIL_ID', 'CONTACT')
            ->when($search_by == "username", function ($query) use ($username) {
                return $query->where('USERNAME', $username);
            })
            ->when($search_by == "email", function ($query) use ($username) {
                return $query->where('EMAIL_ID', $username);
            })
            ->when($search_by == "contact_no", function ($query) use ($username) {
                return $query->where('CONTACT', $username);
            })
            // ->where('USERNAME', $user_name)
            ->first()->USER_ID ?? null;
    }

    public function getUserNameFromUserId($user_name)
    {
        return $user_id = User::select('USER_ID')->where('USERNAME', $user_name)->first()->USER_ID;
    }

    public function promoCodeInfo(Request $request)
    {
        $userId = $request->userId;
        $date_from = $request->date_from;
        // \DB::enableQueryLog();
        $promoCodeDetails = DB::table('master_transaction_history_rewardcoins')
            ->selectRaw('sum(TRANSACTION_AMOUNT) as POINTS')
            ->where('USER_ID', $userId)
            ->where('TRANSACTION_TYPE_ID', '112')
            ->where('TRANSACTION_STATUS_ID', '236')
            ->when(!empty($date_from), function ($query) use ($date_from) {
                $date_from = Carbon::parse($date_from)->format('Y-m-d H:i:s');
                return $query->whereDate('TRANSACTION_DATE', ">=", $date_from);
            })
            ->limit(1)
            ->first();
        // $query = \DB::getQueryLog();
        // dd($promoCodeDetails, $query); exit;

        return response()->json(['status' => 200, 'data' => $promoCodeDetails]);
    }


    public function bountyInfo(Request $request)
    {
        $userId = $request->userId;
        $ref_no = $request->in_ref_no;
        // \DB::connection('tour')->enableQueryLog();
        $bountyDetails = DB::connection('tour')->table('bounty_transaction_details')
            ->select('USER_ID', 'BURST_USER_NAME', 'BURST_USER_BOUNTY_AMOUNT', 'OPENING_BALANCE', 'BOUNTY_AMOUNT', 'CLOSING_BALANCE')
            ->where('USER_ID', $userId)
            ->where('INTERNAL_REFERENCE_NO', $ref_no)
            // ->limit(1)
            ->get();
        // $query = \DB::connection('tour')->getQueryLog();
        // dd($bountyDetails, $query); exit;

        return response()->json(['status' => 200, 'data' => $bountyDetails]);
    }

    public function excelExport()
    {
        $username = Session::get('trans_username');
        $ref_no = Session::get('trans_ref_no');
        $amount = Session::get('trans_amount');
        $coinType = Session::get('trans_coin_type');
        $transactionType =  Session::get('trans_trans_type');
        $transactionStatus = Session::get('trans_trans_status');
        $dateFrom = Session::get('trans_dateFrom');
        $dateTo = Session::get('trans_dateTo');
        $search_by =  Session::get('trans_search_by');
        $pvt_table =  Session::get('pvt_table');
        $tTypeIdsTableName = array(31, 71, 13);
        $tTypeIdsTournamentName = array(70, 25, 79, 101, 102, 103, 105);
        $trans_status = array();

        if ($transactionStatus != "blank") {
            if ($transactionStatus == 1) {
                $trans_status = array('101', '102', '103', '107', '108', '111', '125', '201', '202', '203');
            } elseif ($transactionStatus == 2) {
                $trans_status = array('104', '109', '205');
            } elseif ($transactionStatus == 3) {
                $trans_status = array('106', '110', '207');
            } elseif ($transactionStatus == 4) {
                $trans_status = array('105', '112', '206');
            } elseif ($transactionStatus == 204) {
                $trans_status = array('204', '216');
            } else {
                $trans_status[] = $transactionStatus;
            }
        }

        if ($coinType == 3) {
            $tableName = "master_transaction_history_fppbonus";
        } else if ($coinType == 9) {
            $tableName = "master_transaction_history_coins";
        } else if ($coinType == 6) {
            $tableName = "master_transaction_history_baazi";
        } else {
            $tableName = "master_transaction_history";
        }

        // $user_id = null;

        // if (isset($username)) {
        //     $user_id = $this->getUserIDFromUsername($username, $search_by);
        // }

        $transactionData = $this->getAllTrnasactionData($username, $search_by, $ref_no, $amount, $transactionType, $trans_status, $dateFrom, $dateTo, $tableName, $pvt_table);
        $allTransactionSearchData = $transactionData->get();
        // dd($allTransactionSearchData);
        foreach ($allTransactionSearchData as $transactionData) {
            if ($transactionData->TRANSACTION_DESCRIPTION == "Deposit") {
                $details = $transactionData->PROMO_CODE;
            } else if ($transactionData->TRANSACTION_DESCRIPTION == "Refer a Friend Bonus") {
                $details = $transactionData->gerReferFriendName($transactionData->INTERNAL_REFERENCE_NO)->PROMO_CODE;
            } else {
                $details = '--';
            }
            // $tourName = TransactionSearchController::getTournamentNameByRefNo($transactionData->INTERNAL_REFERENCE_NO);
            // if (!empty($transactionData->TOURNAMENT_NAME)) {
            //     $tourName = $transactionData->TOURNAMENT_NAME;
            // } else {
            //     $tourName = "--";
            // }
            $data_array[] =
                array(
                    'USERNAME' => $transactionData->USERNAME,
                    'TOURNAMENT_NAME' => $transactionData->TOURNAMENT_NAME ?? "--",
                    'INTERNAL_REFERENCE_NO' => $transactionData->INTERNAL_REFERENCE_NO,
                    'TRANSACTION_TYPE_ID' => $transactionData->TRANSACTION_DESCRIPTION,
                    'TRANSACTION_STATUS_ID' => $transactionData->TRANSACTION_STATUS_DESCRIPTION,
                    'CURRENT_TOT_BALANCE' => $transactionData->CURRENT_TOT_BALANCE,
                    'TRANSACTION_AMOUNT' => $transactionData->TRANSACTION_AMOUNT,
                    'CLOSING_TOT_BALANCE' => $transactionData->CLOSING_TOT_BALANCE,
                    'TRANSACTION_DATE' => $transactionData->TRANSACTION_DATE,
                    'DETAILS' => $details,
                );
        }
        // echo "<pre>";
        // print_r($data_array); exit;
        $headingofsheetArray = array('username', 'Table name', 'Ref No', 'Type', 'Status', 'Current Balance', 'Amount', 'Closing Balance', 'Date', 'Details');
        $fileName = urlencode("Trans_Search" . date('d-m-Y') . ".xlsx");
        return Excel::download(new CollectionExport($data_array, $headingofsheetArray), $fileName);
    }

    public static function getTournamentNameByRefNo($INTERNAL_REFERENCE_NO)
    {
        return \DB::connection('slave')->table('tournament_tables')
            ->select('tournament_tables.TOURNAMENT_NAME')
            ->join('poker_wallet_transaction', 'poker_wallet_transaction.ROOM_ID', '=', 'tournament_tables.TOURNAMENT_TABLE_ID')
            ->where('poker_wallet_transaction.INTERNAL_REFERENCE_NO', $INTERNAL_REFERENCE_NO)
            ->first();
    }
}
