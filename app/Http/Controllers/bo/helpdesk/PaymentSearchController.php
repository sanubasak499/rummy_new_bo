<?php

namespace App\Http\Controllers\bo\helpdesk;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\helpdesk\MasterTransactionHistory;
use App\Models\PaymentTransaction;
use App\Models\WithdrawalCriteria;
use App\Models\PlayerLedger;
use App\Models\CoinType;
use App\Models\PromoCampaign;
use App\Models\CampaignToUser;
use App\Models\MasterTransactionHistoryRewardcoins;
use App\Models\MasterTransactionHistoryFppBonus;
use App\Models\TournamentUserTicket;
use App\Models\CampaignTournamentMapping;
use App\Models\Tournament;
use App\Models\User;
use App\Models\UserPoint;
use App\Models\PromoRule;
use DB;
use Carbon\Carbon;
use Auth;
use Hash;
use App\ThirdParty\PayTMLibrary;
use App\Traits\Models\common_methods;
use Session;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
use App\Traits\common_mail;
use PDF;

class PaymentSearchController extends Controller
{
  //
  use common_methods, common_mail;

  public function index(Request $request)
  {
    // DB::enableQueryLog();
    $perPage = config('poker_config.paginate.per_page');
    if (!$request->isMethod('post')) {
      return view('bo.views.helpdesk.paymentSearch');
    } else {

      if ($request->isMethod('post')) {
        $page = request()->page;
        $username          = $request->username;
        $ref_no            = $request->ref_no;
        $amount            = $request->amount;
        $paymentMethod     = $request->payment_method;
        $depositCode       = $request->deposit_code;
        $paymentStatus     = $request->payment_status;
        $dateFrom          = $request->date_from;
        $dateTo            = $request->date_to;
        $search_by         = $request->search_by;
        $date_range        = $request->date_range;


        $trans_type_id = array(8, 61, 62, 83, 111);
        $trans_statusArray = array();
        if (!empty($paymentStatus)) {
          if (in_array("success", $paymentStatus)) { //sucess
            $trans_statusArray = array_merge($trans_statusArray, array('101', '102', '103', '107', '108', '111', '125', '201', '202', '203'));
          }
          if (in_array("pending", $paymentStatus)) { //pending
            $trans_statusArray = array_merge($trans_statusArray, array('122'));
          }
          if (in_array("rejected", $paymentStatus)) { //rejected
            $trans_statusArray = array_merge($trans_statusArray, array('105', '112', '206'));
          }
          if (in_array("cancelled", $paymentStatus)) { //cancelled
            $trans_statusArray = array_merge($trans_statusArray, array('106', '110', '207'));
          }
          if (in_array("failed", $paymentStatus)) { //failed
            $trans_statusArray = array_merge($trans_statusArray, array('204'));
          }
        }

        //session set for excel export

        Session::put('username', $username);
        Session::put('ref_no', $ref_no);
        Session::put('amount', $amount);
        Session::put('paymentMethod', $paymentMethod);
        Session::put('depositCode', $depositCode);
        Session::put('paymentStatus', $paymentStatus);
        Session::put('dateFrom', $dateFrom);
        Session::put('dateTo', $dateTo);
        Session::put('search_by', $search_by);
        //session set for excel export end
        $user_id = null;
        if (isset($username)) {
          $user_id = $this->getUserIDFromUsername($username, $search_by);
        }
        //get all transaction data
        $payTransDataRes = $this->getAllTransactionData($username, $ref_no, $amount, $paymentMethod, $depositCode, $paymentStatus, $dateFrom, $dateTo, $search_by, $user_id, $trans_statusArray, $trans_type_id);
        // print_r($payTransDataRes->getBindings());
        // dd($payTransDataRes->toSql());
        $totalAmount = $payTransDataRes->sum('PAYMENT_TRANSACTION_AMOUNT');
        $payTransData = $payTransDataRes->paginate($perPage, ['*'], 'page', $page);

        $params = $request->all();
        $params['page'] = $page;
        //$pendingRes = $this->getAllTransactionData($username, $ref_no, $amount, $paymentMethod, $depositCode, $paymentStatus, $dateFrom, $dateTo, $search_by, $user_id, array('122'), $trans_type_id);
        // $pendingRes->dd();
        // $pendingAmount = $pendingRes->sum('PAYMENT_TRANSACTION_AMOUNT');
        // $failedRes = $this->getAllTransactionData($username, $ref_no, $amount, $paymentMethod, $depositCode, $paymentStatus, $dateFrom, $dateTo, $search_by, $user_id, array('204'), $trans_type_id);
        //$failedAmount = $failedRes->sum('PAYMENT_TRANSACTION_AMOUNT');

        return view('bo.views.helpdesk.paymentSearch', ['payTransData' => $payTransData, 'params' => $params, 'totalAmount' => $totalAmount]);
      }
    }
  }

  public function getAllTransactionData($username, $ref_no, $amount, $paymentMethod, $depositCode, $paymentStatus, $dateFrom, $dateTo, $search_by, $user_id, $trans_statusArray, $trans_type_id)
  {
    $query = PaymentTransaction::query();
    $query->from(app(PaymentTransaction::class)->getTable() . " as pt");
    $query->join('transaction_status as ts', 'ts.TRANSACTION_STATUS_ID', '=', 'pt.PAYMENT_TRANSACTION_STATUS');
    $query->join('user as u', 'u.USER_ID', '=', 'pt.USER_ID');
    $query->join('payment_provider as pp', 'pp.PAYMENT_PROVIDER_ID', '=', 'pt.PAYMENT_PROVIDER_ID');
    $query->when(!empty($ref_no), function ($query) use ($ref_no) {
      return $query->where('pt.INTERNAL_REFERENCE_NO', $ref_no);
    })
      ->when(!empty($amount), function ($query) use ($amount) {
        return $query->where('pt.PAYMENT_TRANSACTION_AMOUNT', $amount);
      });
    $query->when(!empty($paymentMethod), function ($query) use ($paymentMethod) {
      return $query->where('pt.PAYPAL_RETURN_VALUES', $paymentMethod);
    });
    $query->when(!empty($user_id), function ($query) use ($user_id) {
      return $query->where('pt.USER_ID', $user_id);
    });
    $query->when(!empty($depositCode), function ($query) use ($depositCode) {
      return $query->where('pt.PROMO_CODE', $depositCode);
    });
    $query->when(!empty($trans_statusArray), function ($query) use ($trans_statusArray) {
      return $query->whereIn('pt.PAYMENT_TRANSACTION_STATUS', $trans_statusArray);
    });
    $query->when(!empty($trans_type_id), function ($query) use ($trans_type_id) {
      return $query->whereIn('pt.TRANSACTION_TYPE_ID', $trans_type_id);
    });
    $query->when(!empty($dateFrom), function ($query) use ($dateFrom) {
      $dateFrom = Carbon::parse($dateFrom)->format('Y-m-d H:i:s');
      return $query->whereDate('pt.PAYMENT_TRANSACTION_CREATED_ON', ">=", $dateFrom);
    });
    $query->when(!empty($dateTo), function ($query) use ($dateTo) {
      $dateTo = Carbon::parse($dateTo)->format('Y-m-d H:i:s');
      return $query->whereDate('pt.PAYMENT_TRANSACTION_CREATED_ON', "<=", $dateTo);
    });
    $query->orderBy('pt.PAYMENT_TRANSACTION_CREATED_ON', 'DESC');
    $payTransData = $query->select(
      'u.USERNAME',
      'pt.USER_ID',
      'pt.PAYMENT_PROVIDER_ID',
      'pt.TRANSACTION_TYPE_ID',
      'pt.PAYMENT_TRANSACTION_AMOUNT',
      'pt.PAYMENT_TRANSACTION_STATUS',
      'pt.TRANSACTION_TYPE_ID',
      'pt.PAYMENT_TRANSACTION_CREATED_ON',
      'pt.INTERNAL_REFERENCE_NO',
      'pt.PROMO_CODE',
      'pt.PAYPAL_RETURN_VALUES',
      'pt.UPDATED_DATE',
      'pt.PAYMENT_TRANSACTION_ID',
      'ts.TRANSACTION_STATUS_DESCRIPTION',
      'pp.PROVIDER_NAME'
    );

    return $payTransData;
    //   $bind=$payTransData->getBindings();
    //  $res=$payTransData->toSql();
    //  print_r($bind);
    //  dd($res);

  }
  //approve pending and failed payment
  public function changePaymentStatus(Request $request)
  {

    //DB::enableQueryLog();
    if ($request->isMethod('post')) {
      $trans_status = $request->trans_status;
      $ref_no = $request->ref_no;
      $user_id = $request->user_id;
      $pay_trans_id = $request->pay_trans_id;
      $trans_pwd = $request->trans_pwd;
      $getway_type = $request->getway_type;
      $trans_type_id = $request->trans_type_id;
      $getWaySatus = $request->getWaySatus;

      // if ($getWaySatus != "SUCCESS") {
      // echo $trans_type_id;exit;
      if ($trans_status != 1) {
        $response = "Please toggle the button to approve the transaction";
        return response()->json(['status' => 199, 'message' => $response]);
      }

      if ($trans_pwd == "") {
        $response = "Opps ! you have forgot to fill your transaction password .";
        return response()->json(['status' => 201, 'message' => $response]);
      }

      //check transaction password
      $pwdRes = \PokerBaazi::checkTransactionPassword($trans_pwd);
      if ($pwdRes == FALSE) {
        $response = "Opps ! your transaction password is wrong .";
        //activity entry
        $data['Wrong Transaction Password'] = "Wrong Transaction Password";
        $action = "Wrong Transaction Password";
        $this->insertAdminActivity($data, $action);
        return response()->json(['status' => 202, 'message' => $response]);
      }
      //}


      //get updated getway status from individual getway status Api
      //if transaction type not pay by cash or neft
      if ($trans_type_id != 83 && $trans_type_id != 111) { //if transaction type not pay by cash or neft  //if transaction type not pay by cash or neft

        if ($getway_type == "PAYU") {
          // $getWayStatus = "SUCCESS";
          //get payu satus and update in database
          $payUStatus = $this->getPayUGetWayStatus($ref_no);
          $payUStatusArry = $payUStatus->getData();
          $data = array('getWayStatus' => $payUStatusArry, 'refNo' => $ref_no);
          $action = "Payu Getway Satus(Payment Approve )";
          $this->insertAdminActivity($data, $action);

          if ($payUStatusArry->txnStatus == "SUCCESS") {
            $getWayStatus = "SUCCESS";
          } else if ($payUStatusArry->txnStatus == "Settlement in Process" || $payUStatusArry->txnStatus == "PENDING") {
            $getWayStatus = "PENDING";
          } else {
            $getWayStatus = $payUStatusArry->txnStatus;
          }
        } else if ($getway_type == "PayTM") {
          //get paytm satus and update in database
          $payTmStatus = $this->getPayTMGetWayStatus($ref_no);
          $payTmStatusArry = $payTmStatus->getData();
          $data = array('getWayStatus' => $payTmStatusArry, 'refNo' => $ref_no);
          $action = "PayTM Getway Satus(Payment Approve )";
          $this->insertAdminActivity($data, $action);

          if ($payTmStatusArry->txnStatus == "SUCCESS") {
            $getWayStatus = "SUCCESS";
          } else if ($payTmStatusArry->txnStatus == "PENDING") {
            $getWayStatus = "PENDING";
          } else {
            $getWayStatus = $payTmStatusArry->txnStatus;
          }
        } else if ($getway_type == "Cash Free") {
          //get cashfree satus and update in database
          $cashFreeRes = $this->getCashfreeGetWayStatus($ref_no);
          $cashFreeStatusArry = $cashFreeRes->getData();
          $data = array('getWayStatus' => $cashFreeStatusArry, 'refNo' => $ref_no);
          $action = "Cshfree Getway Satus(Payment Approve )";
          $this->insertAdminActivity($data, $action);
          // print_r($cashFreeStatusArry->txnStatus); exit;

          if ($cashFreeStatusArry->txnStatus == "SUCCESS") {
            $getWayStatus = "SUCCESS";
          } else if ($cashFreeStatusArry->txnStatus == "PENDING") {
            $getWayStatus = "PENDING";
          } else {
            $getWayStatus = $cashFreeStatusArry->txnStatus;
          }
        } else {
          $response = "Something went wrong in getway selection. Please try again";
          return response()->json(['status' => 206, 'message' => $response]);
        }
      } else {
        $getWayStatus = "CASH/NEFT"; // only for pay by neft and pay by cash
      }
      $masterTransRes = $this->updatePaymentStatus($user_id, $ref_no, $pay_trans_id, $getWayStatus, $trans_type_id);
      if ($masterTransRes == "APPROVED") {
        //for manual approve
        if ($getWayStatus == "SUCCESS") {
          $response = "The payment has been auto approve by getway status .  <br> Ref No : '$ref_no' ";
          return response()->json(['status' => 200, 'message' => $response]);
        } else if ($trans_type_id == 83 || $trans_type_id == 111) {
          $response = "The CASH/NEFT payment has been manually approved .  <br> Ref No : '$ref_no' ";
          return response()->json(['status' => 200, 'message' => $response]);
        } else {
          $response = "This Payment has been forcefully approved .  <br> Ref No : '$ref_no' <br> GateWay Status: '$getWayStatus' ";
          return response()->json(['status' => 200, 'message' => $response]);
        }
      } else if ($masterTransRes == "ALREADY APPROVED") {
        $response = "Already Approved";
        return response()->json(['status' => 204, 'message' => $response]);
      } else if ($masterTransRes == "USER_POINTS_NOT_UPDATED") {
        $response = "User Balance not updated";
        return response()->json(['status' => 204, 'message' => $response]);
      } else {
        $response = "Approved Failed";
        return response()->json(['status' => 204, 'message' => $response]);
      }
      // } else {
      //   $response = "Approve Failed1 ";
      //   return response()->json(['status' => 205, 'message' => $response]);
      // }
    } else {
      $response = "Approve Failed";
      return response()->json(['status' => 206, 'message' => $response]);
    }
  }




  // update payment transaction table and insert into master transaction table after sucessfully approved
  public function updatePaymentStatus($user_id, $ref_no, $pay_trans_id, $getWayStatus, $trans_type_id)
  {
    $coin_type_id = 1;
    $balance_type_id = 1;
    $trans_status_id = 103;
    if ($trans_type_id != 83 || $trans_type_id != 111) { //if transaction type not pay by cash or neft no need getway status 
      if ($getWayStatus == "SUCCESS") {
        $upadteType = "Getway Update";
      } else {
        $upadteType = "force Update";
      }
    } else {
      $upadteType = "NEFT/CASH - manual Update";
      $getWayStatus = "CASH/NEFT";
    }

    // echo $trans_status_id; exit;
    $trans_type_id = 8;
    $now = date("Y:m:d H:i:s");
    $partner_id = 10001;

    $getTransactionData = $this->getUserPendingPayment($user_id, $ref_no, $pay_trans_id);
    $payTransAmount = $getTransactionData->PAYMENT_TRANSACTION_AMOUNT;
    $payTransstatus = $getTransactionData->PAYMENT_TRANSACTION_STATUS;
    if ($payTransstatus == 122 || $payTransstatus == 204) { // if payment is pending state or failed
      $userCurrentBal = $this->getUsercurrentBalance($user_id, $coin_type_id);
      //get user current balance before approve
      $userCurrentDepositBalance = $userCurrentBal->USER_DEPOSIT_BALANCE;
      $userCurrentTotalBalance = $userCurrentBal->USER_TOT_BALANCE;
      // user total balance after approve
      $totaldepositBalance = $userCurrentDepositBalance + $payTransAmount;
      $userTotalBalanceAfterApprove = $userCurrentTotalBalance + $payTransAmount;
      // update status on payment transaction table
      $updatePaymentTransStatus = PaymentTransaction::where(['INTERNAL_REFERENCE_NO' => $ref_no, 'USER_ID' => $user_id, 'PAYMENT_TRANSACTION_ID' => $pay_trans_id])
        ->update(['PAYMENT_TRANSACTION_STATUS' => $trans_status_id]);
      // update balance on user_points table
      if ($updatePaymentTransStatus) {
        $updateUserPoints = UserPoint::where(['USER_ID' => $user_id, 'COIN_TYPE_ID' => $coin_type_id])
          ->update(['USER_DEPOSIT_BALANCE' => $totaldepositBalance, 'USER_TOT_BALANCE' => $userTotalBalanceAfterApprove]);
        if ($updateUserPoints) {
          $masterTransRes = $this->insertMasterTransTable($user_id, $balance_type_id, $trans_status_id, $trans_type_id, $payTransAmount, $now, $ref_no, $userCurrentTotalBalance, $userTotalBalanceAfterApprove, $partner_id);
          if ($masterTransRes) {
            //update promo balance after approve
            $userDetails = $this->getUserDetailsFromUserId($user_id);
            $promUpdate = "";
            if (!empty($getTransactionData->PROMO_CODE)) {
              $promUpdate = $this->updatePromoBalance($user_id, $coin_type_id, $getTransactionData->PROMO_CODE, $payTransAmount, $userDetails, $ref_no);
            }
            $playerLedgerStatus = $this->updatePlayerLedger($ref_no, "DEPOSIT");
            // $updateWithdraw = $this->updateWithdrawalCriteria($ref_no);
            //activity entry
            $data = array(
              'getWayStatus' => $getWayStatus,
              'prev_staus' => $payTransstatus,
              'cur_satus' => $trans_status_id,
              'USER_ID' => $user_id,
              'INTERNAL_REFERENCE_NO' => $ref_no,
              'masterTransRes' => $masterTransRes,
              'playerLedgerStatus' => $playerLedgerStatus,
              // 'updateWithdraw' => $updateWithdraw,
              'trans_type_id' => $trans_type_id,
              'promoused' => $promUpdate,
              'updateType' => $upadteType,
              'updateUserPoints' => $updateUserPoints
            );
            $action = "Payment Approve-" . $ref_no;
            $this->insertAdminActivity($data, $action);


            //if getway status is sucess
            //mail approve params
            $params['email'] = $userDetails->EMAIL_ID;
            $params['username'] = $userDetails->USERNAME;
            $params['tempId'] = 49;
            $params['subject'] = "Payment Approve";
            $params['refNo'] = $ref_no;
            $params['amount'] = $payTransAmount;
            $params['date'] = date('d-m-Y');
            $params['time'] = date('H:i:s');
            $this->sendMail($params);

            if ($getWayStatus != "SUCCESS" && $getWayStatus != "CASH/NEFT") {

              $directorEmails = config('poker_config.director_mail_id.emails');
              if (!empty($directorEmails)) {
                foreach ($directorEmails as $directorEmails) {
                  $adminUsername = \Auth::user();
                  $params['username'] = $userDetails->USERNAME;
                  $params['email'] = $directorEmails;
                  $params['tempId'] = 50;
                  $params['subject'] = "Payment Force Approved";
                  $params['getway_status'] = $getWayStatus;
                  $params['approve_by'] = $adminUsername->username;
                  $params['amount'] = $payTransAmount;
                  $params['date'] = date('d-m-Y');
                  $params['time'] = date('H:i:s');
                  $params['refNo'] = $ref_no;
                  $this->sendMail($params);
                }
              }
            }


            //send mail to user after sucessfull payment approve

            return "APPROVED";
            // }
          } else {
            return "MASTER_TRANS_NOT_INSERTED";
          }
        } else {
          return "USER_POINTS_NOT_UPDATED";
        }
      }
    } else {
      return "ALREADY APPROVED";
    }
  }

  public function getUserIDFromUsername($username, $search_by)
  {
    return $user_id = User::select('USER_ID', 'EMAIL_ID', 'CONTACT')
      ->when($search_by == "username", function ($query) use ($username) {
        return $query->where('USERNAME', $username);
      })
      ->when($search_by == "email", function ($query) use ($username) {
        return $query->where('EMAIL_ID', $username);
      })
      ->when($search_by == "contact_no", function ($query) use ($username) {
        return $query->where('CONTACT', $username);
      })
      // ->where('USERNAME', $user_name)
      ->first()->USER_ID ?? null;
  }

  public function getUserNameFromUserId($user_name)
  {
    return $user_id = User::select('USER_ID')->where('USERNAME', $user_name)->first()->USER_ID;
  }

  public function getUserPendingPayment($user_id, $ref_no, $pay_trans_id)
  {
    return $paymentTransData = PaymentTransaction::select('PAYMENT_TRANSACTION_AMOUNT', 'PAYMENT_TRANSACTION_STATUS', 'PROMO_CODE')->where(['PAYMENT_TRANSACTION_ID' => $pay_trans_id, 'INTERNAL_REFERENCE_NO' => $ref_no, 'USER_ID' => $user_id])->first();
  }



  //insert in master transaction table after approve
  public function insertMasterTransTable($user_id, $balance_type_id, $trans_status_id, $trans_type_id, $trans_amount, $trans_date, $ref_no, $current_bal, $closing_bal, $partner_id)
  {
    // DB::enableQueryLog();
    $masterTransaction = new MasterTransactionHistory();
    $masterTransaction->USER_ID                = $user_id;
    $masterTransaction->BALANCE_TYPE_ID        = $balance_type_id;
    $masterTransaction->TRANSACTION_STATUS_ID  = $trans_status_id;
    $masterTransaction->TRANSACTION_TYPE_ID    = $trans_type_id;
    $masterTransaction->TRANSACTION_AMOUNT     = $trans_amount;
    $masterTransaction->TRANSACTION_DATE       = $trans_date;
    $masterTransaction->INTERNAL_REFERENCE_NO  = $ref_no;
    $masterTransaction->CURRENT_TOT_BALANCE    = $current_bal;
    $masterTransaction->CLOSING_TOT_BALANCE    = $closing_bal;
    $masterTransaction->PARTNER_ID             = $partner_id;
    return $masterTransaction->save();
    // $queriesss = DB::getQueryLog();
    // dd($queriesss);
  }

  //insert in master transaction table after approve
  public function insertMasterTransFppBonusTable($user_id, $balance_type_id, $trans_status_id, $trans_type_id, $trans_amount, $trans_date, $ref_no, $current_bal, $closing_bal, $partner_id)
  {
    // DB::enableQueryLog();
    $masterTransactionFpp = new MasterTransactionHistoryFppBonus();
    $masterTransactionFpp->USER_ID                = $user_id;
    $masterTransactionFpp->BALANCE_TYPE_ID        = $balance_type_id;
    $masterTransactionFpp->TRANSACTION_STATUS_ID  = $trans_status_id;
    $masterTransactionFpp->TRANSACTION_TYPE_ID    = $trans_type_id;
    $masterTransactionFpp->TRANSACTION_AMOUNT     = $trans_amount;
    $masterTransactionFpp->TRANSACTION_DATE       = $trans_date;
    $masterTransactionFpp->INTERNAL_REFERENCE_NO  = $ref_no;
    $masterTransactionFpp->CURRENT_TOT_BALANCE    = $current_bal;
    $masterTransactionFpp->CLOSING_TOT_BALANCE    = $closing_bal;
    $masterTransactionFpp->PARTNER_ID             = $partner_id;
    return $masterTransactionFpp->save();
    // $queriesss = DB::getQueryLog();
    // dd($queriesss);
  }
  //fetch all getway status to show in popup
  public function getAllPaymentGetWayStatus(Request $request)
  {
    if ($request->isMethod('post')) {

      $ref_no = $request->ref_no;
      $user_id = $request->user_id;
      $getway_type = $request->getway_type;
      $trans_type_id = $request->trans_type_id;
      if ($getway_type == "Cash Free") {
        $cashFreeRes = $this->getCashfreeGetWayStatus($ref_no);
        return $cashFreeRes;
      } else if ($getway_type == "PAYU") {
        $payuStatus = $this->getPayUGetWayStatus($ref_no);
        return $payuStatus;
      } else if ($getway_type == "PayTM") {
        // $ref_no ="12110330200120015855";
        $payTmStatus = $this->getPayTMGetWayStatus($ref_no);
        return $payTmStatus;
      } else if ($trans_type_id == 83 || $trans_type_id == 111) {
        return response()->json(['txnStatus' => '', 'txnAmount' => '', 'orderStatus' => '', 'txnTime' => '', 'paymentMode' => '', 'bankName' => '', 'getWayType' => 'PAY BY CASH']);
      } else {
        $response = "Something Went wrong in getway status api please try again .";
        return response()->json(['status' => 501, 'message' => $response]);
      }
    }
  }

  public function getPayUGetWayStatus($orderId)
  {

    $PAYU_CURL_URL = config('poker_config.payment.PAYU.payu_stat_url');
    $MERCHANT_KEY = config('poker_config.payment.PAYU.PAYU_MERCHANT_KEY');
    $AUTH_KEY = config('poker_config.payment.PAYU.payu_auth_key');

    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $PAYU_CURL_URL . "?merchantKey=" . $MERCHANT_KEY . "&merchantTransactionIds=" . $orderId . "",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "authorization:$AUTH_KEY",
      ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    $resData = json_decode($response);
    if (!empty($resData->result[0]->status)) {
      if ($resData->result[0]->status == "Money with Payumoney"  || $resData->result[0]->status == "Completed") {
        $txnStatus = "SUCCESS";
      } else {
        $txnStatus = $resData->result[0]->status;
      }
    } else {
      $txnStatus = 'NULL';
    }

    if (!empty($resData->result[0]->amount)) {
      $txnAmount = $resData->result[0]->amount;
    } else {
      $txnAmount = "";
    }

    if (!empty($resData->result[0]->status)) {
      $orderStatus = $resData->result[0]->status;
    } else {
      $orderStatus = "";
    }



    $txnTime = "";
    $paymentMode = "";
    $bankName = "";
    return response()->json(['txnStatus' => $txnStatus, 'txnAmount' => $txnAmount, 'orderStatus' => $orderStatus, 'txnTime' => $txnTime, 'paymentMode' => $paymentMode, 'bankName' => $bankName, 'getWayType' => 'PayU']);
  }
  public function getPayTMGetWayStatus($orderId)
  {
    $liObj = new PayTMLibrary();

    /* initialize an array */
    $paytmParams = array();
    /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
    $paytmParams["MID"] = config('poker_config.payment.PAYTM.mid');
    /* Enter your order id which needs to be check status for */
    $paytmParams["ORDERID"] = $orderId;
    /**
     * Generate checksum by parameters we have in body
     * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
     */
    $checksum = $liObj->getChecksumFromArray($paytmParams, "1iifM_31svgZgYeC");

    // $checksum = getChecksumFromArray($paytmParams, "1iifM_31svgZgYeC");
    /* put generated checksum value here */
    $paytmParams["CHECKSUMHASH"] = $checksum;
    /* prepare JSON string for request */
    $post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);
    /* for Staging */
    $url = config('poker_config.payment.PAYTM.paytm_status_url');
    /* for Production */
    // $url = "https://securegw.paytm.in/order/status";

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $response = curl_exec($ch);

    $resData = json_decode($response);
    // echo  "<pre>";
    // print_r($resData); exit;
    // echo  $resData->STATUS; exit;
    if ($resData->STATUS == "TXN_SUCCESS") {
      $txnStatus = "SUCCESS";
    } else {
      $txnStatus = $resData->STATUS;
    }

    if (!empty($resData->TXNAMOUNT)) {
      $txnAmount = $resData->TXNAMOUNT;
    } else {
      $txnAmount = "";
    }

    if (!empty($resData->STATUS)) {
      $orderStatus = $resData->STATUS;
    } else {
      $orderStatus = "";
    }

    if (!empty($resData->TXNDATE)) {
      $txnTime = $resData->TXNDATE;
    } else {
      $txnTime = "";
    }

    if (!empty($resData->PAYMENTMODE)) {
      $paymentMode = $resData->PAYMENTMODE;
    } else {
      $paymentMode = "";
    }

    if (!empty($resData->GATEWAYNAME)) {
      $bankName = $resData->GATEWAYNAME;
    } else {
      $bankName = "";
    }
    return response()->json(['txnStatus' => $txnStatus, 'txnAmount' => $txnAmount, 'orderStatus' => $orderStatus, 'txnTime' => $txnTime, 'paymentMode' => $paymentMode, 'bankName' => $bankName, 'getWayType' => 'Paytm']);
  }

  public function getCashfreeGetWayStatus($orderId)
  {

    // $orderId="12113848060819123836";
    $CASHFREE_ID = config('poker_config.payment.CASHFREE.cashfree_id');
    $CASHFREE_SEC = config('poker_config.payment.CASHFREE.cashfree_sec');
    $CASHFREE_URL = config('poker_config.payment.CASHFREE.cashfree_status_url');
    $curl = curl_init();
    $data = [
      'appId' => $CASHFREE_ID,
      'secretKey' => $CASHFREE_SEC,
      'orderId'   => $orderId
    ];
    //curl params for get cashfree payment status by order id

    curl_setopt($curl, CURLOPT_URL, $CASHFREE_URL);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response =  curl_exec($curl);
    $resData = json_decode($response);
    // echo  "<pre>";
    // print_r($resData); exit;
    if (!empty($resData->txStatus)) {
      if ($resData->txStatus == "SUCCESS") {
        $txnStatus = "SUCCESS";
      } else {
        $txnStatus = $resData->txStatus;
      }
    } else {
      $txnStatus = "";
    }

    if (!empty($resData->txTime)) {
      $txnTime = $resData->txTime;
    } else {
      $txnTime = "";
    }

    if (!empty($resData->paymentDetails->paymentMode)) {
      $paymentMode = $resData->paymentDetails->paymentMode;
    } else {
      $paymentMode = "";
    }

    if (!empty($resData->paymentDetails->bankName)) {
      $bankName = $resData->paymentDetails->bankName;
    } else {
      $bankName = "";
    }

    if (!empty($resData->orderAmount)) {
      $txnAmount = $resData->orderAmount;
    } else {
      $txnAmount = "";
    }

    if (!empty($resData->orderStatus)) {
      $orderStatus = $resData->orderStatus;
    } else {
      $orderStatus = "";
    }


    return response()->json(['txnStatus' => $txnStatus, 'txnAmount' => $txnAmount, 'orderStatus' => $orderStatus, 'txnTime' => $txnTime, 'paymentMode' => $paymentMode, 'bankName' => $bankName, 'getWayType' => 'Cashfree']);
    // echo  "txnStatus :".$txnStatus ."txnAmount:".$txnAmount."orderStatus :".$orderStatus."txnTime:".$txnTime ."paymentMode:".$paymentMode."bankName:".$bankName ; exit;
  }




  public function insertAdminActivity($data, $action)
  {
    $activity = [
      'admin_id' => \Auth::user()->id,
      'module_id' => '41',
      'action' => $action,
      'data' => json_encode($data)
    ];
    \PokerBaazi::storeActivity($activity);
  }

  public function paymentExcelExport()
  {
    $username = Session::get('username');
    $ref_no   = Session::get('ref_no');
    $amount   = Session::get('amount');
    $paymentMethod   = Session::get('paymentMethod');
    $depositCode   = Session::get('depositCode');
    $dateFrom   = Session::get('dateFrom');
    $dateFrom   = Session::get('dateFrom');
    $dateTo   = Session::get('dateTo');
    $search_by   = Session::get('search_by');
    $paymentStatus   = Session::get('paymentStatus');

    $trans_type_id = array(8, 61, 62, 83, 111);
    $trans_statusArray = array();
    if (!empty($paymentStatus)) {
      if (in_array("success", $paymentStatus)) { //sucess
        $trans_statusArray = array_merge($trans_statusArray, array('101', '102', '103', '107', '108', '111', '125', '201', '202', '203'));
      }
      if (in_array("pending", $paymentStatus)) { //pending
        $trans_statusArray = array_merge($trans_statusArray, array('122'));
      }
      if (in_array("rejected", $paymentStatus)) { //rejected
        $trans_statusArray = array_merge($trans_statusArray, array('105', '112', '206'));
      }
      if (in_array("cancelled", $paymentStatus)) { //cancelled
        $trans_statusArray = array_merge($trans_statusArray, array('106', '110', '207'));
      }
      if (in_array("failed", $paymentStatus)) { //failed
        $trans_statusArray = array_merge($trans_statusArray, array('204'));
      }
    }
    $user_id = null;
    if (isset($username)) {
      $user_id = $this->getUserIDFromUsername($username, $search_by);
    }
    $payTransExpoData = $this->getAllTransactionData($username, $ref_no, $amount, $paymentMethod, $depositCode, $paymentStatus, $dateFrom, $dateTo, $search_by, $user_id, $trans_statusArray, $trans_type_id);

    $payTransExpoData = $payTransExpoData->get();
    // $headingofsheet = collect($payTransExpoDataP->first())->keys()->toArray();
    foreach ($payTransExpoData as $payTransExpoData) {
      $data_array[] =
        array(
          'USERNAME' => $payTransExpoData->USERNAME,
          'INTERNAL_REFERENCE_NO' => $payTransExpoData->INTERNAL_REFERENCE_NO,
          'PAYPAL_RETURN_VALUES' => $payTransExpoData->PAYPAL_RETURN_VALUES,
          'PROVIDER_NAME' => $payTransExpoData->getPaymentProviderName->PROVIDER_NAME,
          'PAYMENT_TRANSACTION_AMOUNT' => $payTransExpoData->PAYMENT_TRANSACTION_AMOUNT,
          'PAYMENT_TRANSACTION_CREATED_ON' => $payTransExpoData->PAYMENT_TRANSACTION_CREATED_ON,
          'TRANSACTION_STATUS_DESCRIPTION' => $payTransExpoData->TRANSACTION_STATUS_DESCRIPTION,
          'PROMO_CODE' => $payTransExpoData->PROMO_CODE,
        );
    }
    // echo "<pre>";
    // print_r($data_array); exit;
    $headingofsheetArray = array('username', 'Rference No', 'Pay Method', 'Mode', 'Amount', 'Date', 'Status', 'Promocode');
    $fileName = urlencode("Payment_Search" . date('d-m-Y') . ".xlsx");
    return Excel::download(new CollectionExport($data_array, $headingofsheetArray), $fileName);
  }

  //if promo code applied
  public function updatePromoBalance($user_id, $coin_type_id, $promocode, $purchaseAmount, $userDetails, $ref_no)
  {
    $userCurrentBal = $this->getUsercurrentBalance($user_id, $coin_type_id);
    $getMoneOption = PromoCampaign::select('MONEY_OPTION', 'PROMO_CAMPAIGN_ID', 'PROMO_CAMPAIGN_TYPE_ID', 'COST_PER_USER', 'TOURNAMENT_ID', 'TOURNAMENT_NAME')
      ->where('PROMO_CAMPAIGN_CODE', $promocode)
      ->first();
    $userPromOfferPlace = $this->getPromorules($getMoneOption->PROMO_CAMPAIGN_ID);
    $internalRef = $ref_no;
    $now = date("Y:m:d H:i:s");

    if ($getMoneOption->PROMO_CAMPAIGN_TYPE_ID == 1 || $getMoneOption->PROMO_CAMPAIGN_TYPE_ID == 6) { //PAYMENT CAMPAIGN
      $updateWithdraw = $this->updateWithdrawalCriteria($ref_no);
      if ($userPromOfferPlace->P_PROMO_CHIPS == 1 && $userPromOfferPlace->P_PROMO_VALUE != "" && $userPromOfferPlace->P_PROMO_VALUE != 0) { //This is to give value in promo
        $PPromPercentageVal = floor(($purchaseAmount * $userPromOfferPlace->P_PROMO_VALUE) / 100);
        // echo $PPromPercentageVal.'----'.$userPromOfferPlace->P_PROMO_VALUE; exit;
        $PPromoCap = $userPromOfferPlace->P_PROMO_CAP_VALUE;

        $promoCodeValue = "";
        if ($PPromPercentageVal > $PPromoCap) {
          $promoCodeValue = $PPromoCap;
        } else {
          $promoCodeValue = $PPromPercentageVal;
        }

        $realmoneyEPromoBalance = $userCurrentBal->USER_PROMO_BALANCE;
        $realmoneyNPromoBalance = $realmoneyEPromoBalance + $promoCodeValue;
        $realmoneyECTotalBalanceP = $userCurrentBal->USER_TOT_BALANCE;
        $realmoneyNCTotalBalanceP = $realmoneyECTotalBalanceP + $promoCodeValue;

        $balance_type_id = "2";
        $trans_status_id = "107";
        $trans_type_id = "9";
        $partner_id = 10001;
        // echo  $promoCodeValue; exit;
        $updateUserPoints = UserPoint::where(['USER_ID' => $user_id, 'COIN_TYPE_ID' => $coin_type_id])
          ->update(['VALUE' => $promoCodeValue, 'USER_PROMO_BALANCE' => $realmoneyNPromoBalance, 'USER_TOT_BALANCE' => $realmoneyNCTotalBalanceP]);
        $masterTransRes = $this->insertMasterTransTable($user_id, $balance_type_id, $trans_status_id, $trans_type_id, $promoCodeValue, $now, $internalRef, $realmoneyECTotalBalanceP, $realmoneyNCTotalBalanceP, $partner_id);
        $this->insertIntocampaignToUser($user_id, $getMoneOption->PROMO_CAMPAIGN_ID);
        //assign multiple tickets

        $query = CampaignTournamentMapping::query();
        $query->from(app(CampaignTournamentMapping::class)->getTable() . " as ctm");
        $query->leftJoin('promo_campaign as pc', 'ctm.PROMO_CAMPAIGN_ID', '=', 'pc.PROMO_CAMPAIGN_ID');
        $query->where('pc.PROMO_CAMPAIGN_CODE', $promocode);
        $capmRes = $query->select(
          'ctm.TOURNAMENT_NAME'
        );
        $tournamentNameMappedToCampaign = $capmRes->get();
        foreach ($tournamentNameMappedToCampaign as $tournamentName) {
          $query = Tournament::query();
          $query->from(app(Tournament::class)->getTable());
          $query->where('TOURNAMENT_NAME', $tournamentName->TOURNAMENT_NAME);
          $query->where('is_active', 1);
          $query->where('TOURNAMENT_START_TIME', '>', \DB::raw('NOW()'));
          $query->orderBy('TOURNAMENT_START_TIME', 'ASC');
          $capmRes = $query->select(
            'TOURNAMENT_ID',
            'TOURNAMENT_NAME',
            'ENTRY_FEE',
            'TOURNAMENT_START_TIME',
            'REGISTER_START_TIME',
            'REGISTER_END_TIME',
            'BUYIN'
          );
          $chktournamentInfo = $capmRes->first() ?? null;
          if (!empty($chktournamentInfo->TOURNAMENT_ID)  && !empty($chktournamentInfo->TOURNAMENT_NAME)) {
            $tourId   = $chktournamentInfo->TOURNAMENT_ID;
            $ticketCount = $this->checkUserHaveAlreadyTikcet($user_id, $tourId);
            if (!empty($getMoneOption->TOURNAMENT_ID) && $ticketCount < 1) {
              $ticketGive = $this->executeProcedureTicket($tourId, $user_id);
              // echo "<pre>";
              // print_r($ticketGive); exit;
              if ($ticketGive == 1) {
                $getProcResult = $this->executeProcedure($tourId, $user_id);
                //   echo "<pre>";
                // print_r($getProcResult); exit;
                $getTourDetails = $this->getTournamentDetailsById($tourId);
                //mail params
                $params['email'] = $userDetails->EMAIL_ID;
                $params['username'] = $userDetails->USERNAME;
                $params['tempId'] = 17;
                $params['subject'] = "Tournament Ticket";
                $params['tourName'] = $getTourDetails->TOURNAMENT_NAME;
                $params['tourBuyIn'] = $getTourDetails->BUYIN;
                $params['tourEntryFee'] = $getTourDetails->ENTRY_FEE;
                $params['tourStartTime'] = $getTourDetails->TOURNAMENT_START_TIME;
                $params['tourRegStartTime'] = $getTourDetails->REGISTER_START_TIME;
                $params['tourRegEndTime'] = $getTourDetails->REGISTER_END_TIME;
                //send mail to user after sucessfull payment approve and tour tickets get by promo code
                $this->sendMail($params);
              }
            }
          }
        }
      }

      if ($userPromOfferPlace->P_UNCLAIMED_BONUS == 1 && $userPromOfferPlace->P_CREDIT_DEBITCARD_VALUE != "") { //This is to give value in bonus
        $UBonusPercentageVal = floor(($purchaseAmount * $userPromOfferPlace->P_CREDIT_DEBITCARD_VALUE) / 100);
        $UBonusCap = $userPromOfferPlace->P_BONUS_CAP_VALUE;

        $uBonusValue = "";
        if ($UBonusPercentageVal > $UBonusCap) {
          $uBonusValue = $UBonusCap;
        } else {
          $uBonusValue = $UBonusPercentageVal;
        }

        $userPointInfoPromo1 = $this->getUsercurrentBalance($user_id, '3');
        $promoCodeValue1 = $uBonusValue;
        $realmoneyEPromoBalance1 = $userPointInfoPromo1->USER_PROMO_BALANCE;
        $realmoneyNPromoBalance1 = $realmoneyEPromoBalance1 + $promoCodeValue1;
        $realmoneyECTotalBalanceP1 = $userPointInfoPromo1->USER_TOT_BALANCE;
        $realmoneyNCTotalBalanceP1 = $realmoneyECTotalBalanceP1 + $promoCodeValue1;
        $updateUserPointsPromo = UserPoint::where(['USER_ID' => $user_id, 'COIN_TYPE_ID' => '3'])
          ->update(['VALUE' => $promoCodeValue1, 'USER_PROMO_BALANCE' => $realmoneyNPromoBalance1, 'USER_TOT_BALANCE' => $realmoneyNCTotalBalanceP1]);

        $fppRes = $this->insertMasterTransFppBonusTable($user_id, $balance_type_id, $trans_status_id, $trans_type_id, $promoCodeValue1, $now, $internalRef, $realmoneyECTotalBalanceP1, $realmoneyNCTotalBalanceP1, $partner_id);
        if ($fppRes) {
          $this->insertIntocampaignToUser($user_id, $getMoneOption->PROMO_CAMPAIGN_ID);
        }
      }
      return "PAYMENT CAMPAIGN";
    } else  if ($getMoneOption->PROMO_CAMPAIGN_TYPE_ID == 8) { // ONE TIME PAYMENT CAMPAIGN

      $firstDepositPromoCheck = $this->checkUserFirstDepositBonusCode($user_id);

      if ($firstDepositPromoCheck <= 1) {
        $updateWithdraw = $this->updateWithdrawalCriteria($ref_no);

        if ($userPromOfferPlace->P_PROMO_CHIPS == 1 && $userPromOfferPlace->P_PROMO_VALUE != "" && $userPromOfferPlace->P_PROMO_VALUE != 0) { //This is to give value in promo
          
          $PPromPercentageVal = floor(($purchaseAmount * $userPromOfferPlace->P_PROMO_VALUE) / 100);
          // echo $PPromPercentageVal.'----'.$userPromOfferPlace->P_PROMO_VALUE; exit;
          $PPromoCap = $userPromOfferPlace->P_PROMO_CAP_VALUE;

          $promoCodeValue = "";
          if ($PPromPercentageVal > $PPromoCap) {
            $promoCodeValue = $PPromoCap;
          } else {
            $promoCodeValue = $PPromPercentageVal;
          }

          $realmoneyEPromoBalance = $userCurrentBal->USER_PROMO_BALANCE;
          $realmoneyNPromoBalance = $realmoneyEPromoBalance + $promoCodeValue;
          $realmoneyECTotalBalanceP = $userCurrentBal->USER_TOT_BALANCE;
          $realmoneyNCTotalBalanceP = $realmoneyECTotalBalanceP + $promoCodeValue;

          $balance_type_id = "2";
          $trans_status_id = "107";
          $trans_type_id = "9";
          $partner_id = 10001;
          // echo  $promoCodeValue; exit;
          $updateUserPoints = UserPoint::where(['USER_ID' => $user_id, 'COIN_TYPE_ID' => $coin_type_id])
            ->update(['VALUE' => $promoCodeValue, 'USER_PROMO_BALANCE' => $realmoneyNPromoBalance, 'USER_TOT_BALANCE' => $realmoneyNCTotalBalanceP]);
          $masterTransRes = $this->insertMasterTransTable($user_id, $balance_type_id, $trans_status_id, $trans_type_id, $promoCodeValue, $now, $internalRef, $realmoneyECTotalBalanceP, $realmoneyNCTotalBalanceP, $partner_id);
          $this->insertIntocampaignToUser($user_id, $getMoneOption->PROMO_CAMPAIGN_ID);
          //assign multiple tickets

          $query = CampaignTournamentMapping::query();
          $query->from(app(CampaignTournamentMapping::class)->getTable() . " as ctm");
          $query->leftJoin('promo_campaign as pc', 'ctm.PROMO_CAMPAIGN_ID', '=', 'pc.PROMO_CAMPAIGN_ID');
          $query->where('pc.PROMO_CAMPAIGN_CODE', $promocode);
          $capmRes = $query->select(
            'ctm.TOURNAMENT_NAME'
          );
          $tournamentNameMappedToCampaign = $capmRes->get();
          foreach ($tournamentNameMappedToCampaign as $tournamentName) {
            $query = Tournament::query();
            $query->from(app(Tournament::class)->getTable());
            $query->where('TOURNAMENT_NAME', $tournamentName->TOURNAMENT_NAME);
            $query->where('is_active', 1);
            $query->where('TOURNAMENT_START_TIME', '>', \DB::raw('NOW()'));
            $query->orderBy('TOURNAMENT_START_TIME', 'ASC');
            $capmRes = $query->select(
              'TOURNAMENT_ID',
              'TOURNAMENT_NAME',
              'ENTRY_FEE',
              'TOURNAMENT_START_TIME',
              'REGISTER_START_TIME',
              'REGISTER_END_TIME',
              'BUYIN'
            );
            $chktournamentInfo = $capmRes->first() ?? null;
            if ($chktournamentInfo->TOURNAMENT_ID != '' && $chktournamentInfo->TOURNAMENT_ID != 0 && $chktournamentInfo->TOURNAMENT_NAME != '') {
              $tourId   = $chktournamentInfo->TOURNAMENT_ID;
              $ticketCount = $this->checkUserHaveAlreadyTikcet($user_id, $tourId);
              if (!empty($getMoneOption->TOURNAMENT_ID) && $ticketCount < 1) {
                $ticketGive = $this->executeProcedureTicket($tourId, $user_id);
                // echo "<pre>";
                // print_r($ticketGive); exit;
                if ($ticketGive == 1) {
                  $getProcResult = $this->executeProcedure($tourId, $user_id);
                  //   echo "<pre>";
                  // print_r($getProcResult); exit;
                  $getTourDetails = $this->getTournamentDetailsById($tourId);
                  //mail params
                  $params['email'] = $userDetails->EMAIL_ID;
                  $params['username'] = $userDetails->USERNAME;
                  $params['tempId'] = 17;
                  $params['subject'] = "Tournament Ticket";
                  $params['tourName'] = $getTourDetails->TOURNAMENT_NAME;
                  $params['tourBuyIn'] = $getTourDetails->BUYIN;
                  $params['tourEntryFee'] = $getTourDetails->ENTRY_FEE;
                  $params['tourStartTime'] = $getTourDetails->TOURNAMENT_START_TIME;
                  $params['tourRegStartTime'] = $getTourDetails->REGISTER_START_TIME;
                  $params['tourRegEndTime'] = $getTourDetails->REGISTER_END_TIME;
                  //send mail to user after sucessfull payment approve and tour tickets get by promo code
                  $this->sendMail($params);
                }
              }
            }
          }
        }

        if ($userPromOfferPlace->P_UNCLAIMED_BONUS == 1 && $userPromOfferPlace->P_CREDIT_DEBITCARD_VALUE != "") { //This is to give value in bonus
          $UBonusPercentageVal = floor(($purchaseAmount * $userPromOfferPlace->P_CREDIT_DEBITCARD_VALUE) / 100);
          $UBonusCap = $userPromOfferPlace->P_BONUS_CAP_VALUE;

          $uBonusValue = "";
          if ($UBonusPercentageVal > $UBonusCap) {
            $uBonusValue = $UBonusCap;
          } else {
            $uBonusValue = $UBonusPercentageVal;
          }

          $userPointInfoPromo1 = $this->getUsercurrentBalance($user_id, '3');
          $promoCodeValue1 = $uBonusValue;
          $realmoneyEPromoBalance1 = $userPointInfoPromo1->USER_PROMO_BALANCE;
          $realmoneyNPromoBalance1 = $realmoneyEPromoBalance1 + $promoCodeValue1;
          $realmoneyECTotalBalanceP1 = $userPointInfoPromo1->USER_TOT_BALANCE;
          $realmoneyNCTotalBalanceP1 = $realmoneyECTotalBalanceP1 + $promoCodeValue1;
          $updateUserPointsPromo = UserPoint::where(['USER_ID' => $user_id, 'COIN_TYPE_ID' => '3'])
            ->update(['VALUE' => $promoCodeValue1, 'USER_PROMO_BALANCE' => $realmoneyNPromoBalance1, 'USER_TOT_BALANCE' => $realmoneyNCTotalBalanceP1]);

          $fppRes = $this->insertMasterTransFppBonusTable($user_id, $balance_type_id, $trans_status_id, $trans_type_id, $promoCodeValue1, $now, $internalRef, $realmoneyECTotalBalanceP1, $realmoneyNCTotalBalanceP1, $partner_id);
          if ($fppRes) {
            $this->insertIntocampaignToUser($user_id, $getMoneOption->PROMO_CAMPAIGN_ID);
          }
        }
        return "PAYMENT CAMPAIGN";
      }
    } else if ($getMoneOption->PROMO_CAMPAIGN_TYPE_ID == 5) { //SCARATCH CARD CAMPAIGN
      $updateWithdraw = $this->updateWithdrawalCriteria($ref_no);
      if ($userPromOfferPlace->S_PROMO_CHIPS == 1 && $userPromOfferPlace->S_PROMO_CHIP_VALUE != "") { //This is to give value in bonus
        $userPointInfoPromo = $this->getUsercurrentBalance($user_id, '3');
        $promoCodeValue = $userPromOfferPlace->S_PROMO_CHIP_VALUE;
        $realmoneyEPromoBalance = $userPointInfoPromo->USER_PROMO_BALANCE;
        $realmoneyNPromoBalance = $realmoneyEPromoBalance + $promoCodeValue;
        $realmoneyECTotalBalanceP = $userPointInfoPromo->USER_TOT_BALANCE;
        $realmoneyNCTotalBalanceP = $realmoneyECTotalBalanceP + $promoCodeValue;
        $updateUserPointsPromo = UserPoint::where(['USER_ID' => $user_id, 'COIN_TYPE_ID' => '3'])
          ->update(['VALUE' => $promoCodeValue1, 'USER_PROMO_BALANCE' => $realmoneyNPromoBalance1, 'USER_TOT_BALANCE' => $realmoneyNCTotalBalanceP1]);
        $this->insertIntocampaignToUser($user_id, $getMoneOption->PROMO_CAMPAIGN_ID);
        $masterTransRes = $this->insertMasterTransTable($user_id, $balance_type_id, $trans_status_id, $trans_type_id, $promoCodeValue, $now, $internalRef, $realmoneyECTotalBalanceP, $realmoneyNCTotalBalanceP, $partner_id);
      }
    } else { //REGISTRATION,TELL A FRIEND
      $promoCodeValue1 =  $promoCodeValue1 ?? 0;
      $userPointInfoPromo = $this->getUsercurrentBalance($user_id, '3');
      $promoCodeValue = $getMoneOption->PROMO_VALUE1;
      $realmoneyEPromoBalance = $userPointInfoPromo->USER_PROMO_BALANCE;
      $realmoneyNPromoBalance = $realmoneyEPromoBalance + $promoCodeValue;
      $realmoneyECTotalBalanceP = $userPointInfoPromo->USER_TOT_BALANCE;
      $realmoneyNCTotalBalanceP = $realmoneyECTotalBalanceP + $promoCodeValue;
      $updateUserPointsPromo = UserPoint::where(['USER_ID' => $user_id, 'COIN_TYPE_ID' => '3'])
        ->update(['VALUE' => $promoCodeValue1, 'USER_PROMO_BALANCE' => $realmoneyNPromoBalance1, 'USER_TOT_BALANCE' => $realmoneyNCTotalBalanceP1]);
      $this->insertIntocampaignToUser($user_id, $getMoneOption->PROMO_CAMPAIGN_ID);
      $fppRes = $this->insertMasterTransFppBonusTable($user_id, $balance_type_id, $trans_status_id, $trans_type_id, $promoCodeValue1, $now, $internalRef, $realmoneyECTotalBalanceP1, $realmoneyNCTotalBalanceP1, $partner_id);
    }
  }

  public function getPromorules($promoCampId)
  {
    $promoRuleDetails = PromoRule::select('P_PROMO_CHIPS', 'P_PROMO_VALUE', 'P_PROMO_CAP_VALUE', 'P_UNCLAIMED_BONUS', 'P_CREDIT_DEBITCARD_VALUE', 'P_BONUS_CAP_VALUE')
      ->where('PROMO_CAMPAIGN_ID', $promoCampId)
      ->first();
    return $promoRuleDetails;
  }

  public function insertIntocampaignToUser($userId, $promocamId)
  {
    date_default_timezone_set('Asia/Kolkata');
    $currentDate = date('Y-m-d H:i:s');
    // DB::enableQueryLog();
    $campData = new CampaignToUser();
    $campData->USER_ID            = $userId;
    $campData->PROMO_CAMPAIGN_ID  = $promocamId;
    return $campData->save();
  }
  public function downloadInvoice(Request $request)
  {
    $customPaper = array(0, 0, 700, 900);
    $pdf = PDF::loadView('bo.views.invoice.invoice', ['data' => $request])->setPaper($customPaper, 'potrait');
    return $pdf->stream('Invoice-' . $request->username . '-' . $request->merchant_id . '.pdf');
  }

  // before calling tournament procedure check if user has laready ticket
  public function checkUserHaveAlreadyTikcet($userId, $tourId)
  {
    $query = TournamentUserTicket::query();
    $query->from(app(TournamentUserTicket::class)->getTable());
    $query->where('USER_ID', $userId);
    $query->where('TOURNAMENT_ID', $tourId);
    $userTicket = $query->select(DB::RAW('USER_ID'));
    $userTicketResult = $userTicket->count();
    return $userTicketResult;
  }
}
