<?php

namespace App\Http\Controllers\bo\helpdesk;

use App\Http\Controllers\Controller;
use App\Mail\CommonMail;
use App\Models\DepositLimitSetting;
use App\Models\DepositLimitUser;
use App\Models\ResponsibleGameLog;
use App\Models\SiteJincNewsletter;
use App\Models\UserAccountDetail;
use App\Models\UserKycDetail;
use App\Traits\common_mail;
use Illuminate\Http\Request;
use App\Models\UserKycDetails;
use App\Models\User;
use App\Models\UserAccountDetails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Response;
use stdClass;
use Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class KycController extends Controller
{
    use common_mail;
	public function index(Request $request){
		if($request->all()){
			$perPage = config('poker_config.paginate.per_page');
			$page = request()->page;
			$get_data = $this->kycDataSearch($request);
			$kycData = $get_data->paginate($perPage, ['*'], 'page', $page);
			$params = $request->all();
			$params['page'] = $page;
			//dd($kycData);
			return view('bo.views.helpdesk.kyc.kyc',['kycData' => $kycData, 'params' => $params,'pageView'=>true]);
		}
		return view('bo.views.helpdesk.kyc.kyc',['pageView'=>false,'kycData' => [],'params' => [] ]);
	}

	public function kycDataSearch($request){
		if($request->doc_type == '1' || $request->doc_type == '2' || $request->doc_type == '3'){
			return $this->kycDetailsAccoDocTyp($request);
		}
		elseif($request->doc_type == '4'){
			return $this->bankDetails($request);
		}
		else{
			$result1 = $this->kycDetailsAccoDocTyp($request);
			$result2 = $this->bankDetails($request);
			return $result = $result1->union($result2);
		}
	}
	public function kycDetailsAccoDocTyp($request){

		$perPage = config('poker_config.paginate.per_page');
		return UserKycDetails::from(app(UserKycDetails::class)->getTable()." as ukd")
		->select('ukd.USER_ID','ukd.USER_KYC_ID','ukd.DOCUMENT_STATUS','ukd.DOCUMENT_TYPE','ukd.DOCUMENT_NUMBER','ukd.DOCUMENT_FRONT_URL','ukd.DOCUMENT_BACK_URL','ukd.UPDATED_AT','u.USERNAME','u.EMAIL_ID','u.CONTACT')
		->join('user as u', 'u.USER_ID' ,'=', 'ukd.USER_ID')
		->when(!empty($request->search_by == "username") && ($request->search_by_value != ''), function($query) use ($request){
			return $query->where('u.USERNAME',  'LIKE', "%{$request->search_by_value}%");
		})
		->when(!empty($request->search_by == "email"),function($query) use ($request){
		   return $query->where('u.EMAIL_ID',  'LIKE', "%{$request->search_by_value}%");
		})
		->when(!empty($request->search_by == "contact_no"), function($query) use ($request){
		   return $query->where('u.CONTACT',  'LIKE', "%{$request->search_by_value}%");
		})
		->when(!empty($request->status), function($query) use ($request){
			return $query->where('ukd.DOCUMENT_STATUS', $request->status);
		})
		->when(!empty($request->date_from), function($query) use ($request){
		   $dateFrom = Carbon::parse($request->date_from)->format('Y-m-d H:i:s');
		   return $query->whereDate('ukd.UPDATED_AT', ">=", $dateFrom);
		})
		->when(!empty($request->date_to), function($query) use ($request){
		   $dateTo = Carbon::parse($request->date_to)->format('Y-m-d H:i:s');
		   return $query->whereDate('ukd.UPDATED_AT', "<=", $dateTo);
		})
		->when(!empty($request->doc_type), function($query) use ($request){
			return $query->where('ukd.DOCUMENT_TYPE',$request->doc_type);
		})
		->when(!empty($request->doc_num), function($query) use ($request){
			return $query->where('ukd.DOCUMENT_NUMBER',$request->doc_num);
		})
		->groupBy('USER_ID','DOCUMENT_TYPE')->orderBy('ukd.UPDATED_AT');
	}
	public function bankDetails($request){

		$perPage = config('poker_config.paginate.per_page');
		return UserAccountDetails::from(app(UserAccountDetails::class)->getTable()." as uad")
		->select('uad.USER_ID','uad.USER_ACCOUNT_ID','uad.STATUS','uad.ACCOUNT_TYPE','uad.ACCOUNT_NUMBER','uad.ACCOUNT_HOLDER_NAME','uad.IFSC_CODE','uad.UPDATED_DATE','u.USERNAME','u.EMAIL_ID','u.CONTACT')
		->join('user as u', 'u.USER_ID' ,'=', 'uad.USER_ID')
		->when(!empty($request->status), function($query) use ($request){
			return $query->where('uad.STATUS', $request->status);
		})
		->when(!empty($request->date_from), function($query) use ($request){
		   $dateFrom = Carbon::parse($request->date_from)->format('Y-m-d H:i:s');
		   return $query->whereDate('uad.UPDATED_DATE', ">=", $dateFrom);
		})
		->when(!empty($request->date_to), function($query) use ($request){
		   $dateTo = Carbon::parse($request->date_to)->format('Y-m-d H:i:s');
		   return $query->whereDate('uad.UPDATED_DATE', "<=", $dateTo);
		})
		->when(!empty($request->doc_num), function($query) use ($request){
			return $query->where('uad.ACCOUNT_NUMBER',$request->doc_num);
		})
		->when(!empty($request->search_by == "username") && ($request->search_by_value != ''), function($query) use ($request){
			return $query->where('u.USERNAME',  'LIKE', "%{$request->search_by_value}%");
		})
		->when(!empty($request->search_by == "email"),function($query) use ($request){
		   return $query->where('u.EMAIL_ID',  'LIKE', "%{$request->search_by_value}%");
		})
		->when(!empty($request->search_by == "contact_no"), function($query) use ($request){
		   return $query->where('u.CONTACT',  'LIKE', "%{$request->search_by_value}%");
		})
		->groupBy('USER_ID')->orderBy('uad.UPDATED_DATE');
	}
	public function viewKycImg(Request $request){
		if(isset($request->id) && !empty($request->id)){
			$details = UserKycDetails::where('USER_KYC_ID',$request->id)->select('USER_ID','DOCUMENT_TYPE')->first();

			$data = UserKycDetails::where('USER_ID',$details->USER_ID)->where('DOCUMENT_TYPE',$details->DOCUMENT_TYPE)->select('USER_KYC_ID','DOCUMENT_SUB_TYPE','DOCUMENT_STATUS','DOCUMENT_NUMBER','DOCUMENT_FRONT_URL','DOCUMENT_BACK_URL','DOCUMENT_TYPE','UPDATED_AT')->get();

			$kycData = [];
			foreach($data as $key=> $val){
				$temp = null;
				$temp = new stdClass();
				$temp->USER_KYC_ID = $val->USER_KYC_ID;
				$temp->DOCUMENT_STATUS = $val->DOCUMENT_STATUS;
				$temp->DOCUMENT_SUB_TYPE = $val->DOCUMENT_SUB_TYPE;
				$temp->DOCUMENT_NUMBER = $val->DOCUMENT_NUMBER;
				$temp->DOCUMENT_TYPE = $val->DOCUMENT_TYPE;
				$temp->UPDATED_AT = $val->UPDATED_AT;
				if (Storage::disk('s3KYC')->exists($val->DOCUMENT_FRONT_URL)) {
					$temp->DOCUMENT_FRONT_URL = Storage::disk('s3KYC')->url($val->DOCUMENT_FRONT_URL);

				}else{
					$temp->DOCUMENT_FRONT_URL = '';
				}
				if (Storage::disk('s3KYC')->exists($val->DOCUMENT_BACK_URL)) {
					$temp->DOCUMENT_BACK_URL = Storage::disk('s3KYC')->url($val->DOCUMENT_BACK_URL);
					}else{
					$temp->DOCUMENT_BACK_URL = '';
				}
				$kycData[] = $temp;
			}
			return response()->json(['userInfo'=>$kycData]);
		}
	}
	public function viewUserBankDetails(Request $request){
		if(isset($request->id) && !empty($request->id)){
			$accid= UserAccountDetails::where('USER_ID',$request->id)->select('USER_ACCOUNT_ID')->first();
			$data = UserAccountDetails::where('USER_ACCOUNT_ID',$accid->USER_ACCOUNT_ID)->select('BANK_NAME','USER_ACCOUNT_ID','UPDATED_DATE','IFSC_CODE','ACCOUNT_HOLDER_NAME','ACCOUNT_TYPE','STATUS','ACCOUNT_NUMBER')->get();
			return response()->json(['userInfo'=>$data]);
		}
	}
	public function viewKycStatus(Request $request){
		if(isset($request->id) && !empty($request->id)){
			$data = UserKycDetails::where('USER_KYC_ID',$request->id)->select('DOCUMENT_STATUS')->first();
			return response()->json(['doc_status'=>$data]);
		}
	}
	public function viewBankStatus(Request $request){
		if(isset($request->id) && !empty($request->id)){
			$data = UserAccountDetails::where('USER_ACCOUNT_ID',$request->id)->select('STATUS')->first();
			return response()->json(['status'=>$data]);
		}
	}
	public function updateKycStatus(Request $request){
		if(isset($request->user_kyc_id) && !empty($request->user_kyc_id)){
			$data = UserKycDetails::where('USER_KYC_ID',$request->user_kyc_id)->update(['DOCUMENT_STATUS'=>$request->doc_status]);
			if(isset($data) && !(empty($data))){
				return response()->json(['status'=>true, 'message'=>"Status updated."]);
			}else{
				return response()->json(['status'=>false, 'message'=>"Status not updated."]);
			}
		}
	}
	public function updateBankStatus(Request $request){
		if(isset($request->user_acc_id) && !empty($request->user_acc_id)){
			$data = UserAccountDetails::where('USER_ACCOUNT_ID',$request->user_acc_id)->update(['STATUS'=>$request->bank_status]);

			if(isset($data) && !(empty($data))){
				return response()->json(['status'=>true, 'message'=>"Status updated."]);
			}else{
				return response()->json(['status'=>false, 'message'=>"Status not updated."]);
			}
		}
	}

    public function addKycDetais(Request $request)
    {
        if ($request->all()) {
            $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
                'doc_type' => 'required',
            ])->validate();
            $user_id = \Illuminate\Support\Facades\Auth::user()->id;
            $kyc_details = new UserKycDetails();
            $kyc_details['DOCUMENT_TYPE'] = $request->doc_type;
            $kyc_details['USER_ID'] = $request->user_id;
            if ($request->pan_number) {
                $kyc_details['DOCUMENT_NUMBER'] = $request->pan_number;
            } else {
                $kyc_details['DOCUMENT_NUMBER'] = $request->doc_num;
            }
            $kyc_details['DOCUMENT_FRONT_URL'] = $request->doc_front_img;
            $kyc_details['DOCUMENT_BACK_URL'] = $request->doc_back_img;

            if ($kyc_details->save()) {
                return back()->with(['success' => 'Kyc Details is insert successfully']);
            } else {
                return back()->with(['error', 'Kyc Details not insert']);
            }
        }
        return view('bo.views.helpdesk.kyc.addKyc');
    }

    public function searchUserByUserName(Request $request)
    {
        $searchQuery = $request->get('searchQuery');
        if (!empty($searchQuery)) {
            return User::select('USERNAME', 'USER_ID')->where('USERNAME', 'LIKE', "%{$searchQuery}%")->limit(10)->get();
        } else {
            return back()->with(['error', 'No search query found']);
        }
    }

    /*doc sub type */
    const AADHAAR = 1;
    const DL = 2;
    const VOTER_ID = 3;

    /* doc type */
    const ADDRESS_PROOF = 1;
    const PAN = 2;
    const BANK_DETAILS = 3;
    const OTHER = 4;
    const INVOID_FEILD_STATUS_TRUE = '1';

    const FULL_MATCH = 'Full Matched';
    const PARTIAL_MATCH = 'Partial Match';
    const NO_MATCH = 'No Match';

    /* verification status */
    const FAILED_CODE = 0;
    const SUCCESS_CODE = 1;
    const PARTIAL_MATCH_CODE = 2;
    const REJECTED_BY_ADMIN_CODE = 3;

    /* approved via*/
    const NOT_APPROVED = 0;
    const SYSTEM = 1;
    const MANUAL = 2;

    public function fetchDetailsFromInVoid(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'doc_type' => 'required|string|in:1,2,3,4',
            'userName' => 'required|string',
        ]);

        $validator->sometimes('doc_number', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('doc_sub_type', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('front_image', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('pan_number', 'required', function ($input) {
            return $input->doc_type == self::PAN;
        });

        $validator->sometimes('pan_image', 'required', function ($input) {
            return $input->doc_type == self::PAN;
        });

        $validator->sometimes('account_number', 'required', function ($input) {
            return $input->doc_type == self::BANK_DETAILS;
        });

        $validator->sometimes('ifsc_code', 'required', function ($input) {
            return $input->doc_type == self::BANK_DETAILS;
        });

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 400);
        }

        $formParams = array();
        $docType = $request->get('doc_type');
        $userName = $request->get('userName');

        $user = User::select('FIRSTNAME', 'LASTNAME', 'USER_ID', 'CONTACT')->where('USERNAME', $userName)->limit(1)->first() ?? null;

        if ($user == null) {
            return Response::json(array(
                'success' => false,
                'errors' => 'User not found with given username'
            ), 500);
        }

        $userFullName = $user->FIRSTNAME . " " . $user->LASTNAME;

        if (empty($userFullName)) {
            $userFullName = $request->get('first_name') . $request->get('last_name');
        }

        $documentNumber = $request->get('doc_number');
        $panNumber = $request->get('pan_number');
        $accountNumber = $request->get('account_number');

        if ($docType == self::BANK_DETAILS) {
            /* bank document type */
            $bankDetails = UserAccountDetail::select('USER_ID')
                ->where('STATUS', self::SUCCESS_CODE)
                ->where('ACCOUNT_NUMBER', $accountNumber)
                ->first();
            if (!empty($bankDetails)) {
                return $this->respondWithError('The Given Doc Number Already Exist', 500);
            }
        } else {
            $kycDetail = UserKycDetail::select('USER_ID')
                ->when($documentNumber, function ($query) use ($documentNumber) {
                    $query->where('DOCUMENT_NUMBER', $documentNumber);
                    $query->where('DOCUMENT_STATUS', self::SUCCESS_CODE);
                })
                ->when($panNumber, function ($query) use ($panNumber) {
                    $query->where('DOCUMENT_NUMBER', $panNumber);
                    $query->where('DOCUMENT_STATUS', self::SUCCESS_CODE);
                })
                ->latest('CREATED_AT')
                ->first();

            if (!empty($kycDetail)) {
                return $this->respondWithError('The Given Doc Number Already Exist', 500);
            }
        }

        if ($docType == self::ADDRESS_PROOF) {
            switch ($request->get('doc_sub_type')) {
                case self::AADHAAR:
                    $document = 'aadhaar';
                    break;
                case self::DL:
                    $document = 'dl';
                    break;
                case self::VOTER_ID:
                    $document = 'voter';
                    break;
            }

            $frontFile = $request->file('front_image');
            $backFile = $request->file('back_image');

            /*file based verification inVoid verification*/
            $formParams['doctype'] = $document;
            $formParams['front'] = new \CURLFile($frontFile);
            $formParams['back'] = new \CURLFile($backFile);
            $formParams['handle'] = 1; /* Mandatory in order to perform OCR [fixed] */
            $formParams['gc'] = 1; /* 1 - To perform Official Database Check & Verification, 0 - To only perform ID Card detection and Validation*/
            $formParams['form-docNumber'] = $documentNumber;
            $formParams['form-name'] = $userFullName;
            $formParams['userId'] = $user->USER_ID;
            $apiBaseUrl = config('poker_config.inVoid.fileBasedApiUrl');
            $response = $this->curlRequest($apiBaseUrl, $formParams) ?? null;

            $country = 'India';
            $city = null;
            $state = null;
            $dob = null;
            $pinCode = null;
            $userFirstName = null;
            $userLastName = null;

            if ($response != null) {
                if (!is_array($response->response) || empty($response->response[0]->data)) {
                    /* verification failed */
                    return $this->respondWithSuccess(self::NO_MATCH);
                } else {
                    if ($response->response[0]->status == '2111' || $response->response[0]->status == '2110'
                        || $response->response[0]->status == '2101' || $response->response[0]->status == '2100') {
                        if ($document == 'dl') {
                            /* driving license*/
                            $dateOfBirthResponse = $response->response[0]->data->dateOfBirth ?? null;
                            $licenseState = $response->response[0]->data->licenseState ?? null;
                            $dob = $dateOfBirthResponse->value ?? null;
                            $state = $licenseState->value ?? null;
                        } elseif ($document == 'aadhaar') {
                            /* aadhaar case */
                            $addressResponse = $response->response[0]->data->address ?? null;
                            $dateOfBirthResponse = $response->response[0]->data->dateOfBirth ?? null;
                            $dob = $dateOfBirthResponse->value ?? null;
                            $state = $addressResponse->state ?? null;
                            $city = $addressResponse->district ?? null;
                            $pinCode = $addressResponse->pinCode ?? null;
                            $responseFullName = $response->response[0]->data->name ?? null;
                            if (!empty($responseFullName) && $responseFullName->status == 1) {
                                $name = explode(" ", $responseFullName->value);
                                $userFirstName = $name[0] ?? null;
                                $userLastName = $name[1] ?? null;
                            }
                        } else {
                            /* Voter-id card */
                            $govtResponse = $response->response[0]->data->govtDetails->data->result ?? null;
                            if (!empty($govtResponse)) {
                                $city = $govtResponse->district->value ?? null;
                                $dob = $govtResponse->dob->value ?? null;
                                $responseFullName = $govtResponse->name ?? null;
                                if (!empty($responseFullName) && $responseFullName->status == 1) {
                                    $name = explode(" ", $responseFullName->value);
                                    $userFirstName = $name[0] ?? null;
                                    $userLastName = $name[1] ?? null;
                                }
                                $state = $govtResponse->state->value ?? null;
                            }
                        }
                        if ($dob == null) {
                            /* if DOB is not exist in the response of inVoid */
                            $dob = $request->get('dob');
                        }
                        if ($state == null) {
                            /* if state is not exist in the response of inVoid */
                            $state = $request->get('state');
                        }
                        $data = [
                            'city' => $city,
                            'state' => $state,
                            'country' => $country,
                            'firstName' => $userFirstName,
                            'lastName' => $userLastName,
                            'dob' => $dob,
                            'pinCode' => $pinCode];

                        /* check restricted state */
                        if ($this->isRestrictedState($state)) {
                            /* document is belonging to restricted state*/
                            array_push($data['message'], self::NO_MATCH);
                            return $this->respondWithSuccess($data);
                        }
                        /* check age bar of user */
                        if ($this->checkAgeBar($dob) < config('poker_config.ageLimit')) {
                            array_push($data['message'], self::NO_MATCH);
                            return $this->respondWithSuccess($data);
                        }
                        /* check entered document number and ocr document matched or not */
                        $documentNumberResponse = $response->response[0]->data->documentNumber ?? null;
                        if (!empty($documentNumberResponse)) {
                            if ($documentNumberResponse->status != 1 || $documentNumberResponse->value != $request->get('doc_number')) {
                                $data['message'] = self::NO_MATCH;
                                return $this->respondWithSuccess($data);
                            }
                        }
                        $nameSimilarity = $response->response[0]->nameSimilarity[0] ?? null;
                        if (!empty($nameSimilarity)) {
                            $similarity = $nameSimilarity->similarity + 0;
                            if ($similarity >= config('poker_config.kycMatchPercentage.fullMatchPercentage')) {
                                $data['message'] = self::FULL_MATCH;
                                return $this->respondWithSuccess($data);
                            } elseif ($similarity < config('poker_config.kycMatchPercentage.fullMatchPercentage') && $similarity >= config('poker_config.kycMatchPercentage.partialMatchPercentage')) {
                                $data['message'] = self::PARTIAL_MATCH;
                                return $this->respondWithSuccess($data);
                            } else {
                                $data['message'] = self::NO_MATCH;
                                return $this->respondWithSuccess($data);
                            }
                        } else {
                            /* name similarity empty or null */
                            $data['message'] = self::NO_MATCH;
                            return $this->respondWithSuccess($data);
                        }
                    } else {
                        /*  inVoid status no match codes */
                        $data['message'] = self::NO_MATCH;
                        return $this->respondWithSuccess($data);
                    }
                }
            } else {
                /*failed case by inVoid*/
                return $this->respondWithError('Internal Server Error', 500);
            }
        } elseif ($docType == self::PAN) {
            $data = array();
            $formParams['docType'] = $docType;
            $formParams['docNumber'] = $request->get('pan_number');
            $formParams['form-name'] = $userFullName;
            $apiBaseUrl = config('poker_config.inVoid.numberBasedApiUrl');
            $response = $this->curlRequest($apiBaseUrl, $formParams) ?? null;
            if ($response != null) {
                if (!is_array($response->response) && $response->response->govtCheckStatus == 'Failure') {
                    return $this->respondWithError('Internal Server error');
                } elseif ($response->response->status == "1") {
                    $nameSimilarity = $response->response->nameSimilarity[0]->similarity ?? null;
                    if ($nameSimilarity != null) {
                        $nameSimilarity = $nameSimilarity + 0;
                        if ($nameSimilarity >= config('poker_config.kycMatchPercentage.fullMatchPercentage')) {
                            /* verified and matched details*/
                            $data['message'] = self::FULL_MATCH;
                            return $this->respondWithSuccess($data);
                        } elseif ($nameSimilarity < config('poker_config.kycMatchPercentage.fullMatchPercentage') && $nameSimilarity >= config('poker_config.kycMatchPercentage.partialMatchPercentage')) {
                            /*partial match */
                            $data['message'] = self::PARTIAL_MATCH;
                            return $this->respondWithSuccess($data);
                        } else {
                            /*failed case by inVoid*/
                            $data['message'] = self::NO_MATCH;
                            return $this->respondWithSuccess($data);
                        }
                    } else {
                        /* name similarity object not found */
                        $data['message'] = self::PARTIAL_MATCH;
                        return $this->respondWithSuccess($data);
                    }
                } else {
                    $data['message'] = self::NO_MATCH;
                    return $this->respondWithSuccess(self::NO_MATCH);
                }
            } else {
                /*failed case by inVoid*/
                return $this->respondWithError('Internal Server Error', 500);
            }
        } elseif ($docType == self::BANK_DETAILS) {
            $data = array();
            $accountNumber = $request->get('account_number');
            $ifscCode = $request->get('ifsc_code');
            $formParams['docType'] = 'bank-account';
            $formParams['docNumber'] = $accountNumber;/* account number*/
            $formParams['ifsc'] = $ifscCode;/* ifsc code */
            $formParams['form-name'] = $userName;/*user full name */
            $apiBaseUrl = config('poker_config.inVoid.numberBasedApiUrl');
            $response = $this->curlRequest($apiBaseUrl, $formParams) ?? null;
            if ($response != null) {
                if ($response->response->status == 1) {
                    if ($response->response->govtCheckStatus == 'Success') {
                        if ($response->response->nameSimilarity[0]->similarity > config('poker_config.kycMatchPercentage.fullMatchPercentage')) {
                            $data['message'] = self::FULL_MATCH;
                        } elseif ($response->response->nameSimilarity[0]->similarity <= config('poker_config.kycMatchPercentage.fullMatchPercentage')
                            && $response->response->nameSimilarity[0]->similarity >= config('poker_config.kycMatchPercentage.partialMatchPercentage')) {
                            $data['message'] = self::PARTIAL_MATCH;
                        } else {
                            $data['message'] = self::FULL_MATCH;
                        }
                    } else {
                        /*govt status is not success */
                        $data['message'] = self::PARTIAL_MATCH;
                    }
                    return $this->respondWithSuccess($data);
                } elseif ($response->response->status == 3) {
                    $data['message'] = self::PARTIAL_MATCH;
                    return $this->respondWithSuccess($data);
                } else {
                    $data['message'] = self::NO_MATCH;
                    return $this->respondWithSuccess($data);
                }
            } else {
                return $this->respondWithError('Internal server error', 500);
            }
        } else {
            /* other document */
            return $this->respondWithSuccess('Support doc fetch details', 200, 2001);
        }
    }

    private function curlRequest($apiBaseUrl, $formParams)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $apiBaseUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $formParams,
            CURLOPT_HTTPHEADER => array(
                "authkey: 58057023-975e-4a99-917a-b1ebb070f47a"
            ),
        ));
        $result = curl_exec($curl);
        $errors = curl_error($curl);
        curl_close($curl);
        return (object)[
            'response' => empty($errors) && !empty($result) ? json_decode($result) : $result,
            'errors' => $errors,
        ];
    }

    private function isRestrictedState($stateName)
    {
        $restrictedState = array('Andhra Pradesh', 'Assam', 'Odisha', 'Telangana', 'Gujarat');
        /* check restricted state */
        if (array_search(strtolower($stateName), array_map('strtolower', $restrictedState))) {
            return true;
        } else {
            return false;
        }
    }

    protected function respondWithSuccess($data, $code = 200, $customCode = null)
    {
        return Response::json([
            'success' => true,
            'code' => $customCode,
            'data' => $data
        ], $code);
    }

    protected function respondWithError($message = 'Internal Server error', $code = 500)
    {
        return Response::json([
            'success' => false,
            'message' => $message
        ], $code);
    }

    public function approveUserKyc(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'doc_type' => 'required|string|in:1,2,3,4',
            'userName' => 'required|string',
        ]);

        $validator->sometimes('doc_number', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('doc_sub_type', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('front_image', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('first_name', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('last_name', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('dob', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('city', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('state', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('country', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('pin_code', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('pan_number', 'required', function ($input) {
            return $input->doc_type == self::PAN;
        });

        $validator->sometimes('pan_image', 'required', function ($input) {
            return $input->doc_type == self::PAN;
        });

        $validator->sometimes('account_number', 'required', function ($input) {
            return $input->doc_type == self::BANK_DETAILS;
        });

        $validator->sometimes('ifsc_code', 'required', function ($input) {
            return $input->doc_type == self::BANK_DETAILS;
        });

        $validator->sometimes('support-image', 'required', function ($input) {
            return $input->doc_type == self::OTHER;
        });

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 400);
        }

        $docType = $request->get('doc_type');
        $userName = $request->get('userName');
        $kycId = $request->get('kyc_id');
        $bankAccountId = $request->get('account_id');
        $documentNumber = $request->get('doc_number');
        $panNumber = $request->get('pan_number');
        $accountNumber = $request->get('account_number');

        if ($docType == self::BANK_DETAILS) {
            /* bank document type */
            $bankDetails = UserAccountDetail::select('USER_ID')
                ->where('ACCOUNT_NUMBER', $accountNumber)
                ->where('STATUS', self::SUCCESS_CODE)
                ->first();
            if (!empty($bankDetails)) {
                return $this->respondWithError('The Given Doc Number Already Exist', 500);
            }
        } else {
            $kycDetail = UserKycDetail::select('USER_ID')
                ->when($documentNumber, function ($query) use ($documentNumber) {
                    $query->where('DOCUMENT_NUMBER', $documentNumber);
                    $query->where('DOCUMENT_STATUS', self::SUCCESS_CODE);
                })
                ->when($panNumber, function ($query) use ($panNumber) {
                    $query->where('DOCUMENT_NUMBER', $panNumber);
                    $query->where('DOCUMENT_STATUS', self::SUCCESS_CODE);
                })
                ->latest('CREATED_AT')
                ->first();

            if (!empty($kycDetail)) {
                return $this->respondWithError('The Given Doc Number Already Exist', 500);
            }
        }

        $user = User::select('FIRSTNAME', 'LASTNAME', 'USER_ID', 'CONTACT')
                ->where('USERNAME', $userName)
                ->limit(1)
                ->first() ?? null;

        if ($user == null) {
            return Response::json(array(
                'success' => false,
                'errors' => 'User not found with given username'
            ), 500);
        }

        try {
            if ($docType == self::ADDRESS_PROOF) {
                if ($request->hasFile('front_image')) {
                    $frontFile = $request->file('front_image');
                    $frontFileName = $this->putImageOnS3($frontFile);
                } else {
                    $frontFileName = $request->get('front_image');
                }

                if ($request->hasFile('back_image')) {
                    $backFile = $request->file('back_image');
                    $backFileName = $this->putImageOnS3($backFile);
                    if (empty($backFileName)) {
                        return $this->respondWithError("Internal Server Error", 500);
                    }
                } else {
                    $backFileName = $request->get('back_image');
                }

                if (!empty($frontFileName)) {
                    \Illuminate\Support\Facades\DB::transaction(function () use ($kycId, $backFileName, $frontFileName, $request, $docType, $user) {
                        $this->updateOrCreateUserKycStatus($kycId, $user->USER_ID, $docType, $request->get('doc_number'),
                            self::SUCCESS_CODE, null,
                            null, $frontFileName,
                            $backFileName, self::MANUAL, $request->get('doc_sub_type'));

                        User::where('USER_ID', $user->USER_ID)->update([
                            'FIRSTNAME' => $request->get('first_name'),
                            'LASTNAME' => $request->get('last_name'),
                            'DATE_OF_BIRTH' => $request->get('dob'),
                            'CITY' => $request->get('city'),
                            'STATE' => $request->get('state'),
                            'COUNTRY' => $request->get('country'),
                            'PINCODE' => $request->get('pin_code'),
                            'KYC_REMAINING_STEPS' => 2,/*remaining step 2 left after address proof uploaded*/
                        ]);
                    });
                    $data['message'] = "Address successfully verified";
                    return $this->respondWithSuccess($data, 200, 2001);
                } else {
                    return $this->respondWithError("Internal Server Error", 500);
                }
            } elseif ($docType == self::PAN) {
                if ($request->hasFile('pan_image')) {
                    $panFile = $request->file('pan_image');
                    $panFileName = $this->putImageOnS3($panFile);
                } else {
                    $panFileName = $request->get('pan_image');
                }
                if ($panFileName != null) {
                    DB::transaction(function () use ($kycId, $panFileName, $request, $docType, $user) {
                        $this->updateOrCreateUserKycStatus($kycId, $user->USER_ID, $docType, $request->get('pan_number'),
                            self::SUCCESS_CODE, null,
                            null, $panFileName,
                            null, self::MANUAL, null);

                        /* reCalibrate Deposit limit of current user */
                        $this->reCalibrateDepositLimits($user->USER_ID);

                        /*update user status on user table */
                        User::where('USER_ID', $user->USER_ID)->update([
                            'KYC_REMAINING_STEPS' => 1  /*remaining step 1 left after pan card upload*/
                        ]);
                    });
                    $data['message'] = "Pan details approved successfully";
                    return $this->respondWithSuccess($data, 200, 2001);
                } else {
                    return $this->respondWithError("Internal Server Error", 500);
                }
            } elseif ($docType == self::BANK_DETAILS) {
                $this->updateOrCreateBankAccountDetails($bankAccountId, $user->USER_ID, $user->FIRSTNAME . " " . $user->LASTNAME,
                    $request->get('account_number'),
                    $request->get('ifsc_code'), null, self::SUCCESS_CODE,
                    self::MANUAL);
                /*update user status on user table */
                User::where('USER_ID', $user->USER_ID)->update([
                    'KYC_REMAINING_STEPS' => 0
                ]);
                $data['message'] = "Bank Detail Verified Successfully";
                return $this->respondWithSuccess($data, 200, 2001);
            } else {
                if ($request->hasFile('support-image')) {
                    $supportFile = $request->file('support-image');
                    $fileName = $this->putImageOnS3($supportFile);
                } else {
                    $fileName = $request->get('support-image');
                }

                $this->updateOrCreateUserKycStatus($kycId, $user->USER_ID, $docType, null,
                    self::SUCCESS_CODE, null,
                    null, $fileName,
                    null, self::MANUAL, null);

                $data['message'] = "Support Doc Verified Successfully";
                return $this->respondWithSuccess($data, 200, 2001);
            }
        } catch (\Exception $exception) {
            return $this->respondWithError("Internal Server Error", 500);
        }
    }

    public function rejectUserKyc(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'doc_type' => 'required|string|in:1,2,3,4',
            'userName' => 'required|string',
        ]);

        $validator->sometimes('doc_number', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('doc_sub_type', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('front_image', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('first_name', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('last_name', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('dob', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('city', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('state', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('country', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('pin_code', 'required', function ($input) {
            return $input->doc_type == self::ADDRESS_PROOF;
        });

        $validator->sometimes('pan_number', 'required', function ($input) {
            return $input->doc_type == self::PAN;
        });

        $validator->sometimes('pan_image', 'required', function ($input) {
            return $input->doc_type == self::PAN;
        });

        $validator->sometimes('account_number', 'required', function ($input) {
            return $input->doc_type == self::BANK_DETAILS;
        });

        $validator->sometimes('ifsc_code', 'required', function ($input) {
            return $input->doc_type == self::BANK_DETAILS;
        });

        $validator->sometimes('support-image', 'required|file', function ($input) {
            return $input->doc_type == self::OTHER;
        });

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 400);
        }

        $docType = $request->get('doc_type');
        $userName = $request->get('userName');
        $kycId = $request->get('kyc_id');
        $bankAccountId = $request->get('account_id');

        $user = User::select('FIRSTNAME', 'LASTNAME', 'USER_ID', 'CONTACT')->where('USERNAME', $userName)->limit(1)->first() ?? null;

        if ($user == null) {
            return Response::json(array(
                'success' => false,
                'errors' => "User not found"
            ), 500);
        }

        try {
            if ($docType == self::ADDRESS_PROOF) {
                if ($request->hasFile('front_image')) {
                    $frontFile = $request->file('front_image');
                    $frontFileName = $this->putImageOnS3($frontFile);
                } else {
                    $frontFileName = $request->get('front_image');
                }

                if ($request->hasFile('back_image')) {
                    $backFile = $request->file('back_image');
                    $backFileName = $this->putImageOnS3($backFile);
                    if (empty($backFileName)) {
                        return $this->respondWithError("Internal Server Error", 500);
                    }
                } else {
                    $backFileName = $request->get('back_image');
                }

                if (!empty($frontFileName)) {
                    $this->updateOrCreateUserKycStatus($kycId, $user->USER_ID, $docType, $request->get('doc_number'),
                        self::REJECTED_BY_ADMIN_CODE, null,
                        null, $frontFileName,
                        $backFileName, null, $request->get('doc_sub_type'));
                    $data['message'] = 'Address details rejected successfully.';
                    return $this->respondWithSuccess($data, 200, 2001);
                } else {
                    return $this->respondWithError("Internal Server Error", 500);
                }

            } elseif ($docType == self::PAN) {
                if ($request->hasFile('pan_image')) {
                    $panFile = $request->file('pan_image');
                    $panFileName = $this->putImageOnS3($panFile);
                } else {
                    $panFileName = $request->get('pan_image');
                }
                if ($panFileName != null) {
                    $this->updateOrCreateUserKycStatus($kycId, $user->USER_ID, $docType, $request->get('pan_number'),
                        self::REJECTED_BY_ADMIN_CODE, null,
                        null, $panFileName,
                        null, null, null);
                    $data['message'] = "Pan details rejected successfully";
                    return $this->respondWithSuccess($data, 200, 2001);
                } else {
                    return $this->respondWithError("Internal Server Error", 500);
                }
            } elseif ($docType == self::BANK_DETAILS) {
                $this->updateOrCreateBankAccountDetails($bankAccountId, $user->USER_ID, $user->FIRSTNAME . " " . $user->LASTNAME,
                    $request->get('account_number'),
                    $request->get('ifsc_code'), null, self::REJECTED_BY_ADMIN_CODE,
                    null);
                $data['message'] = "Bank Detail rejected Successfully";
                return $this->respondWithSuccess($data, 200, 2001);
            } else {
                /* other doc  */
                if ($request->hasFile('support-image')) {
                    $supportFile = $request->file('support-image');
                    $fileName = $this->putImageOnS3($supportFile);
                } else {
                    $fileName = $request->get('support-image');
                }

                $this->updateOrCreateUserKycStatus($kycId, $user->USER_ID, $docType, null,
                    self::REJECTED_BY_ADMIN_CODE, null,
                    null, $fileName,
                    null, self::MANUAL, null);
                $data['message'] = "Support Doc Verified Successfully";

                return $this->respondWithSuccess($data, 200, 2001);
            }
        } catch (\Exception $exception) {
            return $this->respondWithError("Internal Server Error", 500);
        }
    }

    private function updateOrCreateUserKycStatus($kycId, $userId, $docType, $docNumber, $documentStatus, $inVoidResponse,
                                                 $approvedBy, $frontImage, $backImage, $approvedVia, $documentSubType)
    {
        UserKycDetail::updateOrCreate([
            'USER_KYC_ID' => $kycId
        ], [
            'USER_ID' => $userId,
            'DOCUMENT_TYPE' => $docType,
            'DOCUMENT_FRONT_URL' => $frontImage,
            'DOCUMENT_BACK_URL' => $backImage,
            'DOCUMENT_NUMBER' => $docNumber,
            'DOCUMENT_STATUS' => $documentStatus,
            'APROVED_VIA' => $approvedVia,
            'API_RESPONSE' => json_encode($inVoidResponse),
            'APPROVED_BY' => $approvedBy,
            'APPROVED_AT' => $documentStatus == 1 ? Carbon::now() : null, /*success status =1 */
            'DOCUMENT_SUB_TYPE' => $documentSubType
        ]);
    }

    private function updateOrCreateBankAccountDetails($accountId, $userId, $accountHolderName, $accountNumber,
                                                      $ifscCode, $inVoidResponse, $status, $approvedBy)
    {
        UserAccountDetail::updateOrCreate([
            'USER_ACCOUNT_ID' => $accountId
        ], [
            'USER_ID' => $userId,
            'ACCOUNT_HOLDER_NAME' => $accountHolderName,
            'ACCOUNT_NUMBER' => $accountNumber,
            'IFSC_CODE' => $ifscCode,
            'API_RESPONSE' => json_encode($inVoidResponse),
            'APPROVED_BY' => $approvedBy,
            'STATUS' => $status,
            'MICR_CODE' => "",
            'BANK_NAME' => "",
            'BRANCH_NAME' => "",
            'APPROVED_AT' => $status == 1 ? Carbon::now() : null /*success status =1 */
        ]);
    }

    private function putImageOnS3($file)
    {
        $randNum = rand(100, 999);
        $randAlpha = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), -1);
        $name = date('mdYHis') . $randNum . $randAlpha . '.' . $file->getClientOriginalExtension();
        if (empty($name)) {
            return null;
        } else {
            $s3 = \Illuminate\Support\Facades\Storage::disk('s3KYC')->getDriver();
            $s3->put($name, file_get_contents($file));
            return $name;
        }
    }

    private function checkAgeBar($date)
    {
        $dob = new \DateTime($date);
        $now = new \DateTime();
        $difference = $now->diff($dob);
        $age = $difference->y;
        return $age;
    }

    public function editKycDetails($userName, $kycId, $docType)
    {
        $kycDetail = null;
        $bankDetails = null;
        $accountTypes = ['Current', 'Savings'];
        $user = User::select('USER_ID', 'USERNAME', 'FIRSTNAME', 'LASTNAME', 'DATE_OF_BIRTH',
                'CITY', 'STATE', 'COUNTRY', 'PINCODE')->where('USERNAME', $userName)->limit(1)->first() ?? null;
        if ($user != null) {
            if (in_array($docType, $accountTypes)) {
                $bankDetails = UserAccountDetail::select('ACCOUNT_NUMBER', 'IFSC_CODE', 'USER_ACCOUNT_ID', 'STATUS')
                    ->where('USER_ACCOUNT_ID', $kycId)
                    ->where('USER_ID', $user->USER_ID)
                    ->limit(1)
                    ->first();

                /* if document status is not pending */
                if ($bankDetails->STATUS != 2) {
                    return back()->with('error', 'The given details status is not pending');
                }
            } else {
                $kycDetail = UserKycDetail::select('USER_KYC_ID', 'DOCUMENT_TYPE', 'DOCUMENT_STATUS', 'DOCUMENT_FRONT_URL', 'DOCUMENT_BACK_URL',
                    'DOCUMENT_NUMBER', 'DOCUMENT_SUB_TYPE')
                    ->where('USER_KYC_ID', $kycId)
                    ->where('USER_ID', $user->USER_ID)
                    ->limit(1)
                    ->first();

                /* if document status is not pending */
                if ($kycDetail->DOCUMENT_STATUS != 2) {
                    return back()->with('error', 'The given details status is not pending');
                }
            }
            return view('bo.views.helpdesk.kyc.addkyc', compact('user', 'kycDetail', 'bankDetails'));
        } else {
            return back()->with('error', 'user not found with given username');
        }
    }

    /*Re calibrition function for User account below*/
    public function reCalibrateDepositLimits($userId)
    {
        $templateID = 40;
        $kycStatusResult = UserKycDetail::where('USER_ID', $userId)
            ->where('DOCUMENT_TYPE', self::ADDRESS_PROOF)
            ->where('DOCUMENT_STATUS', self::SUCCESS_CODE)
            ->count();
        //if kyc and both pending or approved
        if ($kycStatusResult > 0) {
            $currentDepositLimitValues = $this->getCurrentDepositLimitValues($userId);
            if ($currentDepositLimitValues->UPDATED_BY == "SYSTEM_SIGN_UP") {
                $settingResult = $this->getDepositLimitSettings("POST_KYC");
                $res = $this->updateUserDepositLimitValues($userId, $settingResult->DEP_AMOUNT, $settingResult->DEP_AMOUNT_PER_DAY, $settingResult->DEP_AMOUNT_PER_WEEK, $settingResult->DEP_AMOUNT_PER_MONTH, $settingResult->TRANSACTIONS_PER_DAY, $settingResult->TRANSACTIONS_PER_WEEK, $settingResult->TRANSACTIONS_PER_MONTH, "POST_KYC", "1", "SYSTEM_KYC_UPLOADED");
                $userDetailsforMail = User::select('USERNAME', 'EMAIL_ID')->where('USER_ID', $userId)->first();
                return $this->revisedDepositLimitMail($userDetailsforMail->EMAIL_ID, $userDetailsforMail->USERNAME, $templateID);
            } else if ($currentDepositLimitValues->UPDATED_BY == "SYSTEM_KYC_REJECTED") {
                $lastDepositLimitValuesFromLogs = $this->getLastDepositLimitValuesFromLogs($userId);
                $newDepositAmount = ($currentDepositLimitValues->DEP_AMOUNT < ($lastDepositLimitValuesFromLogs[0] ?? 0)) ? ($lastDepositLimitValuesFromLogs[0] ?? 0) : $currentDepositLimitValues->DEP_AMOUNT;
                $newDepositAmountPerDay = ($currentDepositLimitValues->DEP_AMOUNT_PER_DAY < ($lastDepositLimitValuesFromLogs[1] ?? 0)) ? ($lastDepositLimitValuesFromLogs[1] ?? 0) : $currentDepositLimitValues->DEP_AMOUNT_PER_DAY;
                $newDepositAmountPerWeek = ($currentDepositLimitValues->DEP_AMOUNT_PER_WEEK < ($lastDepositLimitValuesFromLogs[3] ?? 0)) ? ($lastDepositLimitValuesFromLogs[3] ?? 0) : $currentDepositLimitValues->DEP_AMOUNT_PER_WEEK;
                $newDepositAmountPerMonth = ($currentDepositLimitValues->DEP_AMOUNT_PER_MONTH < ($lastDepositLimitValuesFromLogs[5] ?? 0)) ? ($lastDepositLimitValuesFromLogs[5] ?? 0) : $currentDepositLimitValues->DEP_AMOUNT_PER_MONTH;
                $newDepositCountPerDay = ($currentDepositLimitValues->TRANSACTIONS_PER_DAY < ($lastDepositLimitValuesFromLogs[2] ?? 0)) ? ($lastDepositLimitValuesFromLogs[2] ?? 0) : $currentDepositLimitValues->TRANSACTIONS_PER_DAY;
                $newDepositCountPerWeek = ($currentDepositLimitValues->TRANSACTIONS_PER_WEEK < ($lastDepositLimitValuesFromLogs[4] ?? 0)) ? ($lastDepositLimitValuesFromLogs[4] ?? 0) : $currentDepositLimitValues->TRANSACTIONS_PER_WEEK;
                $newDepositCountPerMonth = ($currentDepositLimitValues->TRANSACTIONS_PER_MONTH < ($lastDepositLimitValuesFromLogs[6] ?? 0)) ? ($lastDepositLimitValuesFromLogs[6] ?? 0) : $currentDepositLimitValues->TRANSACTIONS_PER_MONTH;
                $this->updateUserDepositLimitValues($userId, $newDepositAmount, $newDepositAmountPerDay, $newDepositAmountPerWeek, $newDepositAmountPerMonth, $newDepositCountPerDay, $newDepositCountPerWeek, $newDepositCountPerMonth, "SYSTEM", "1", "SYSTEM_KYC_UPLOADED");
                $userDetailsforMail = User::select('USERNAME', 'EMAIL_ID')->where('USER_ID', $userId)->first();
                $this->revisedDepositLimitMail($userDetailsforMail->EMAIL_ID, $userDetailsforMail->USERNAME, $templateID);
            }
        }
    }

    // get User's current deposit values
    public function getCurrentDepositLimitValues($userId)
    {
        $depositLimitResult = DepositLimitUser::select('DEP_AMOUNT', 'DEP_AMOUNT_PER_DAY', 'DEP_AMOUNT_PER_WEEK', 'DEP_AMOUNT_PER_MONTH', 'TRANSACTIONS_PER_DAY', 'TRANSACTIONS_PER_WEEK', 'TRANSACTIONS_PER_MONTH', 'UPDATED_BY')->where('USER_ID', $userId)->first();
        return $depositLimitResult;
    }

    // get user's last deposit values from logs
    public function getDepositLimitSettings($depositLevel)
    {
        $settingResult = DepositLimitSetting::select('DEP_AMOUNT', 'DEP_AMOUNT_PER_DAY', 'DEP_AMOUNT_PER_WEEK', 'DEP_AMOUNT_PER_MONTH', 'TRANSACTIONS_PER_DAY', 'TRANSACTIONS_PER_WEEK', 'TRANSACTIONS_PER_MONTH', 'DEP_LEVEL')->where('DEP_LEVEL', $depositLevel)->first();
        return $settingResult;
    }

    // update user deposit limit values
    public function updateUserDepositLimitValues($userId, $newDepositAmount, $newDepositAmountPerDay, $newDepositAmountPerWeek, $newDepositAmountPerMonth, $newDepositCountPerDay, $newDepositCountPerWeek, $newDepositCountPerMonth, $depositLevel, $updatedStatus, $updatedBy)
    {
        $data = array();
        $data['DEP_AMOUNT'] = $newDepositAmount;
        $data['DEP_AMOUNT_PER_DAY'] = $newDepositAmountPerDay;
        $data['DEP_AMOUNT_PER_WEEK'] = $newDepositAmountPerWeek;
        $data['DEP_AMOUNT_PER_MONTH'] = $newDepositAmountPerMonth;
        $data['TRANSACTIONS_PER_DAY'] = $newDepositCountPerDay;
        $data['TRANSACTIONS_PER_WEEK'] = $newDepositCountPerWeek;
        $data['TRANSACTIONS_PER_MONTH'] = $newDepositCountPerMonth;
        $data['DEP_LEVEL'] = $depositLevel;
        $data['UPDATED_STATUS'] = $updatedStatus;
        $data['UPDATED_BY'] = $updatedBy;
        return DepositLimitUser::where('USER_ID', $userId)->update($data);
    }

    public function revisedDepositLimitMail($email, $userName, $templateID)
    {
        $live_site = config('poker_config.cdn_url');
        $live_site .= "/";
        $getEmailTemplateResult = SiteJincNewsletter::select('name', 'description')->find($templateID);
        $headerLogoURL = $live_site . "images/mailer/pokerbaazi-logo-blue.png";
        $headerImageURL = $live_site . "images/mailer/mailer-01.jpg";
        $footerImageURL = $live_site . "images/mailer/footer.jpg";
        $footerFacebookURL = $live_site . "images/mailer/social-media-facebook.png";
        $footerTwitterURL = $live_site . "images/mailer/social-media-twitter.png";
        $footerLinkedinURL = $live_site . "images/mailer/social-media-linkedin.png";
        $footerYoutubeURL = $live_site . "images/mailer/social-media-youtube.png";
        $downloadAndroidURL = $live_site . "images/mailer/android.png";
        $downloadiOSURL = $live_site . "images/mailer/apple.png";
        $mailSubject = $getEmailTemplateResult->name;
        $mailContent = $getEmailTemplateResult->description;
        date_default_timezone_set('Asia/Kolkata');
        $currentDate = date('Y-m-d H:i:s');
        $requestNo = date_format(date_create($currentDate), "jS M 'y g:iA");
        $subject = $getEmailTemplateResult->name . " | Request Dated: " . $requestNo;
        $sender = array($getEmailTemplateResult->senderaddr, $getEmailTemplateResult->sendername);
        $mailContent = $getEmailTemplateResult->description;
        // Images
        $mailContent = str_replace('_HEADER_LOGO_URL_', $headerLogoURL, $mailContent);
        $mailContent = str_replace('_HEADER_IMG_URL_', $headerLogoURL, $mailContent);
        $mailContent = str_replace('_HEADER_IMAGE_URL_', $headerImageURL, $mailContent);
        $mailContent = str_replace('_DOWN_ARROW_IMG_URL_', $headerImageURL, $mailContent);
        $mailContent = str_replace('_FOOTER_IMAGE_URL_', $footerImageURL, $mailContent);
        $mailContent = str_replace('_FOOTER_FACEBOOK_URL_', $footerFacebookURL, $mailContent);
        $mailContent = str_replace('_FOOTER_TWITTER_URL_', $footerTwitterURL, $mailContent);
        $mailContent = str_replace('_FOOTER_LINKEDIN_URL_', $footerLinkedinURL, $mailContent);
        $mailContent = str_replace('_FOOTER_YOUTUBE_URL_', $footerYoutubeURL, $mailContent);
        $mailContent = str_replace('_DOWNLOAD_ANDROID_URL_', $downloadAndroidURL, $mailContent);
        $mailContent = str_replace('_DOWNLOAD_IOS_URL_', $downloadiOSURL, $mailContent);
        // Information
        $mailContent = str_replace("_USERNAME_", $userName, $mailContent);
        $mailContent = str_replace("_USER_NAME_", $userName, $mailContent);
        $data['mailContent'] = $mailContent;
        $data['requestNo'] = $requestNo;
        $data['email'] = $email;
        $data['subject'] = $subject;
        $data['tempId'] = $templateID;
        $data['username'] = $userName;
        $this->sendMail($data);
    }

    // get user's last deposit values from logs
    public function getLastDepositLimitValuesFromLogs($userId)
    {
        $depositLimitLogs_array = ResponsibleGameLog::select('VALUE')
            ->where('USER_ID', $userId)
            ->where('RESPONSIBLE_SETTINGS_TYPE', 'DEPOSIT_LIMIT')
            ->orderBy('RESPONSIBLE_GAME_LOGS_ID', 'DESC')
            ->first();
        $depositLimitLogs_array = explode(',', $depositLimitLogs_array->VALUE ?? '');
        return $depositLimitLogs_array;
    }


}
