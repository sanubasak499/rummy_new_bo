<?php

namespace App\Http\Controllers\bo\responsiblegaming;

use App\Http\Controllers\Controller;
use App\Models\ResponsibleGameSetting;
use App\Models\ResponsibleGameLog;
use App\Models\ResponsibleGamingRequest;
use App\Models\DepositLimitUser;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
use App\Traits\common_mail;
use Auth;
use DB;
use Session;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CollectionExport;

class ResponsibleGamingController extends Controller
{
    use common_mail;
    // Listing Poker Break User Data based on filter search
    function index(request $request){
        $data['pokerbreakdata'] = [];
        if($request->all()){
            $result = ResponsibleGameSetting::query();
            if (!empty($request->username)) {
                $user_id = getUserIdFromUsername($request->username);
                $result = $result->where('USER_ID', $user_id);
            }
            if ($request->status!='') {
               $result = $result->where('BREAK_ACTIVE', $request->status);
            }
            if (!empty($request->updatedBy)) {
                if($request->updatedBy=='Admin'){
                    $result = $result->where('BREAK_UPDATED_BY','!=', 'SELF')
                    ->where('BREAK_UPDATED_BY','!=', '0')
                    ->where('BREAK_UPDATED_BY','!=', 'NULL');
                }else{
                    $result = $result->where('BREAK_UPDATED_BY', $request->updatedBy);
                }
            }
            if(strtoupper($request->searchbydate) ==strtoupper('Start Date')){
                if (!empty($request->date_from)) {
                        $date_from = Carbon::parse($request->date_from);
                        $result = $result->whereDate('BREAK_FROM_DATE', ">=", $date_from);
                }
                if (!empty($request->date_to)) {
                    $date_to = Carbon::parse($request->date_to);
                    $result = $result->whereDate('BREAK_FROM_DATE', "<=", $date_to);
                }
            }
            if(strtoupper($request->searchbydate) ==strtoupper('End Date')){
                if (!empty($request->date_from)) {
                        $date_from = Carbon::parse($request->date_from);
                        $result = $result->whereDate('BREAK_TO_DATE', ">=", $date_from);
                }
                if (!empty($request->date_to)) {
                    $date_to = Carbon::parse($request->date_to);
                    $result = $result->whereDate('BREAK_TO_DATE', "<=", $date_to);
                }
            }
            $data['pokerbreakdata'] = $result->select('RESPONSIBLE_GAME_SETTINGS_ID','USER_ID','BREAK_ACTIVE','BREAK_FROM_DATE','BREAK_TO_DATE','BREAK_UPDATED_BY','BREAK_UPDATED_DATE')->orderBy('BREAK_UPDATED_DATE','desc')->get();
            Session::put('getAllPokerBreakData',$data['pokerbreakdata']);
        }
        $data['params'] = $request->all();
        return view('bo.views.responsiblegaming.pokerbreak',$data);
    }

    //Show Data in Poker Break Model Click after change status
    function show(request $request){
        if($request->pokerbreakid){
            $data = ResponsibleGameSetting::select('USER_ID','BREAK_ACTIVE','BREAK_FROM_DATE','BREAK_TO_DATE')->where('RESPONSIBLE_GAME_SETTINGS_ID',$request->pokerbreakid)->first();
            return response(['status'=>'200', 'message'=>"success", 'data'=>$data]);
        };
    }

    //Update Poker Break User Data
    function update(request $request){ 
        $updateTransaction = function() use($request){
            $rules = [
                'model_status' => 'required',
                'model_duration' => 'required_if:model_status,1',
                'date_from_model' =>'required_if:model_duration,custom',
                'date_to_model' =>'required_if:model_duration,custom'
            ];
            $this->validate($request, $rules);
            try{
                $breakStatus = $request->model_status==1?1:0;
                $CommonData =[
                    'BREAK_ACTIVE' => $breakStatus,
                    'BREAK_UPDATED_BY' => Auth::user()->username,
                    'BREAK_UPDATED_DATE' => date("Y-m-d H:i:s")
                ];
                if($request->model_duration=='custom'){
                    $customDate =[
                        'BREAK_FROM_DATE' => date('Y-m-d H:i:s',strtotime($request->date_from_model)),
                        'BREAK_TO_DATE' => date('Y-m-d H:i:s',strtotime($request->date_to_model))
                    ];
                }else{
                    $selectedDate =[
                        'BREAK_FROM_DATE' => date('Y-m-d H:i:s'),
                        'BREAK_TO_DATE' => date('Y-m-d 23:59:59',strtotime($request->model_duration_date))
                    ];
                }
                $data = ($request->model_duration=='custom'?array_merge($CommonData,$customDate):($request->model_status==0?$CommonData:array_merge($CommonData,$selectedDate)));
                $where = ['USER_ID' => $request->model_userid];
            
                $ACTION_REASION = ($request->model_status=='1')?'PB_START':'PB_END';
                $logsData = array(
                    'USER_ID' => $request->model_userid,
                    'REQUEST_ID' =>1,
                    'RESPONSIBLE_SETTINGS_TYPE' => "POKER_BREAK",
                    'STATUS' => $request->model_status,
                    'ACTION' => $ACTION_REASION,
                    'UPDATED_BY' => Auth::user()->username,
                    'UPDATED_DATE' => date('Y-m-d H:i:s'),
                    'CREATED_DATE' => date('Y-m-d H:i:s')
                );
                $logData = ($request->model_duration=='custom'?array_merge([
                    'FROM_DATE' => date('Y-m-d H:i:s',strtotime($request->date_from_model)),
                    'TO_DATE' => date('Y-m-d H:i:s',strtotime($request->date_to_model))
                ],$logsData):($request->model_status==0?$logsData:array_merge($logsData,[
                    'FROM_DATE' => date('Y-m-d H:i:s'),
                    'TO_DATE' => date('Y-m-d 23:59:59',strtotime($request->model_duration_date))
                ])));
                $message = ($request->model_status=='1')?'Poker Break is active':'Poker Break is in-active';
                
                /* Check already active or in-active
                *$checkdata['OFC_ACTIVE'],$checkdata['OFC_MAX_POINT']
                */
                $checkdata = ResponsibleGameSetting::select('BREAK_ACTIVE','BREAK_FROM_DATE','BREAK_TO_DATE')->where($where)->first();
                if($breakStatus == 0){
                    if($breakStatus == $checkdata['BREAK_ACTIVE']){
                        return response()->json(['status'=>'201', 'message'=>'Status is already Inactive', 'title'=>'Sorry']);
                    }
                }
                if($breakStatus != 0){
                    if(strtotime($data['BREAK_TO_DATE']) == strtotime($checkdata['BREAK_TO_DATE']) && $checkdata['BREAK_ACTIVE']==1){
                        return response()->json(['status'=>'201', 'message'=>'You have already applied poker break for this range date', 'title'=>'Sorry']);
                    }
                }

                // Update poker break data in responsible game setting table
                $result_update_pokerbreak = $this->updateCommon($where,$data);
                //store activity
                $admin_activity_log = \PokerBaazi::storeActivityWithParams($action=$message, json_encode($logData), $module_id="NULL");
                //insert data in responsible game logs table
                $insertLogs = $this->insertgamelogs($logData);

                //send mail start to user
                $params['email'] = getEmailFromUserId($request->model_userid);
                $params['username'] = getUsernameFromUserId($request->model_userid);
                $params['MAIL_STATUS'] = $ACTION_REASION;
                if($params['MAIL_STATUS'] =='PB_START'){
                    $params['BREAK_FROM_DATE'] = date('d-m-Y H:i:s',strtotime($data['BREAK_FROM_DATE']));
                    $params['BREAK_TO_DATE'] = date('d-m-Y H:i:s',strtotime($data['BREAK_TO_DATE']));
                    $params['tempId'] = 48;
                }else{
                    $params['tempId'] = 47;
                }
                $params['subject'] = $request->model_status=='1'?'Your PokerBaazi profile is now locked, "'.getUsernameFromUserId($request->model_userid) .'"!':'Your PokerBaazi profile is now unlocked, "'.getUsernameFromUserId($request->model_userid) .'"!';
                $params['GAME_TYPE'] = 'POKER_BREAK';
                $params['date'] = date('d-m-Y');
                $params['time'] = date('H:i:s');
                $this->sendMail($params);
                //send mail end to user

                return response()->json(['status'=>'200', 'message'=>$message, 'title'=>'Congrats']);
            
            }catch(\Exception $e){
                return response()->json(['status'=>500, 'message'=>$e->getMessage(),'title'=>'Sorry']);
            }
        };
        return DB::transaction($updateTransaction);
    }

    //Common function for update responsible game setting data with where condition
    function updateCommon($where,$data){
        $result = new ResponsibleGameSetting($data);
        return $result->where($where)->update($data);
    }

    //Common function for insert responsible game loigs data
    function insertgamelogs($data){
        $result = new ResponsibleGameLog($data);
        return $result->save($data);
    }

    //show all user logs data
    function showAllLogs(request $request){
        $data['alllogs'] = [];
        if($request->all()){
            $result = ResponsibleGameLog::query();
            if (!empty($request->username)) {
                $user_id = getUserIdFromUsername($request->username);
                $result = $result->where('USER_ID', $user_id);
            }
            if(!empty($request->settingType)){
                $result = $result->whereIn ('RESPONSIBLE_SETTINGS_TYPE', $request->settingType);
            }
            if(!empty($request->action)){
                $result = $result->where('ACTION', $request->action);
            }
            if ($request->status!='') {
               $result = $result->where('STATUS', $request->status);
            }
            
            if(strtoupper($request->searchByDate) ==strtoupper('start_date')){
                if (!empty($request->date_from)) {
                        $date_from = Carbon::parse($request->date_from);
                        $result = $result->whereDate('FROM_DATE', ">=", $date_from);
                }
                if (!empty($request->date_to)) {
                    $date_to = Carbon::parse($request->date_to);
                    $result = $result->whereDate('FROM_DATE', "<=", $date_to);
                }
            }
            if(strtoupper($request->searchByDate) ==strtoupper('update_date')){
                if (!empty($request->date_from)) {
                        $date_from = Carbon::parse($request->date_from);
                        $result = $result->whereDate('UPDATED_DATE', ">=", $date_from);
                }
                if (!empty($request->date_to)) {
                    $date_to = Carbon::parse($request->date_to);
                    $result = $result->whereDate('UPDATED_DATE', "<=", $date_to);
                }
            }
            $data['alllogs'] = $result->select('RESPONSIBLE_GAME_LOGS_ID','USER_ID','RESPONSIBLE_SETTINGS_TYPE','ACTION','FROM_DATE','TO_DATE','STATUS','VALUE','UPDATED_BY','CREATED_DATE', 'UPDATED_DATE')->orderBy('RESPONSIBLE_GAME_LOGS_ID','desc')->get();
        }
        
        $data['params'] = $request->all();
        return view('bo.views.responsiblegaming.alllogs',$data);
    }

    // add new user if username not exist in responsible game setting 
    function addusername(request $request){
        if($request->has('username')){
            $request->validate([
                'username'=>'required'
            ]);
            $user_id = getUserIdFromUsername($request->username);
            if($user_id){
                $matchuser = ResponsibleGameSetting::select('USER_ID')->where('USER_ID',$user_id)->first();
                if($matchuser!=''){
                    return back()->withInput()->with('error', 'This Username is already active in responsible gaming');
                }else{
                    $data = array(
                        'USER_ID' => $user_id,
                        'CASH_MAX_BIG_BLIND' => 2000,
                        'OFC_MAX_POINT' => 2000,
                        'CREATED_DATE' => date('Y-m-d H:i:s')
                    );
                    // this function call for insert username
                    if($this->storeUserName($data)){
                        $data['CREATED_BY'] = Auth::user()->username;
                        \PokerBaazi::storeActivityWithParams($action="Add Username", json_encode($data), $module_id="NULL");
                        return back()->with('success', 'User Add Successfully');
                    }else{
                        return back()->withInput()->with('error', 'Data Updation Failed');
                    }
                }
            }else{
                return back()->withInput()->with('error', 'This user does not exist');
            }
        }
        return view('bo.views.responsiblegaming.adduser');
    }

    //common function for inserting data in responsible game setting
    function storeUserName($data){
        $insertData = new ResponsibleGameSetting($data);
        return $insertData->save($data);
    }

    //listing ofc limit user data based on filter search
    function ofclimit(request $request){
        $data['ofcData'] = [];
        if($request->all()){
            $result = ResponsibleGameSetting::query();
            if (!empty($request->username)) {
                $user_id = getUserIdFromUsername($request->username);
                $result = $result->where('USER_ID', $user_id);
            }
            if(!empty($request->ofclimit)){
                $result = $result->whereIn ('OFC_MAX_POINT', $request->ofclimit);
            }
            if (!empty($request->updatedBy)) {
                if($request->updatedBy=='Admin'){
                    $result = $result->where('OFC_UPDATED_BY','!=', 'SELF')
                    ->where('OFC_UPDATED_BY','!=', '0')
                    ->where('OFC_UPDATED_BY','!=', 'NULL');
                }else{
                    $result = $result->where('OFC_UPDATED_BY', $request->updatedBy);
                }
            }
            if ($request->status!='') {
               $result = $result->where('OFC_ACTIVE', $request->status);
            }
            
            if(strtoupper($request->searchbydate) ==strtoupper('start date')){
                if (!empty($request->date_from)) {
                        $date_from = Carbon::parse($request->date_from);
                        $result = $result->whereDate('OFC_FROM_DATE', ">=", $date_from);
                }
                if (!empty($request->date_to)) {
                    $date_to = Carbon::parse($request->date_to);
                    $result = $result->whereDate('OFC_FROM_DATE', "<=", $date_to);
                }
            }
            if(strtoupper($request->searchbydate) ==strtoupper('update date')){
                if (!empty($request->date_from)) {
                        $date_from = Carbon::parse($request->date_from);
                        $result = $result->whereDate('OFC_UPDATED_DATE', ">=", $date_from);
                }
                if (!empty($request->date_to)) {
                    $date_to = Carbon::parse($request->date_to);
                    $result = $result->whereDate('OFC_UPDATED_DATE', "<=", $date_to);
                }
            }
            $data['ofcData'] = $result->select('RESPONSIBLE_GAME_SETTINGS_ID','USER_ID','OFC_FROM_DATE','OFC_TO_DATE','OFC_ACTIVE','OFC_MAX_POINT','OFC_UPDATED_BY','OFC_UPDATED_DATE')->orderBy('OFC_UPDATED_DATE','desc')->where('OFC_MAX_POINT','!=',0)->get();
        }
        
        $data['params'] = $request->all();
        return view('bo.views.responsiblegaming.ofclimit',$data);
    }

    //show data in ofc model click after edit action
    function showofctable(request $request){
        if($request->ofctableid && $request->USER_ID){
            $data = ResponsibleGameSetting::select('RESPONSIBLE_GAME_SETTINGS_ID','USER_ID','OFC_MAX_POINT','OFC_ACTIVE')->where('RESPONSIBLE_GAME_SETTINGS_ID',$request->ofctableid)->where('USER_ID',$request->USER_ID)->first();
            return response(['status'=>'200', 'message'=>"success", 'data'=>$data]);
        };
    }

    //update ofc limit data
    function updateofclimit(request $request){ 
        $updateTransaction = function() use($request){
            $rules = [
                'model_status' => 'required',
                'ofc_table_limit' => 'required_if:model_status,1'
            ];
            $this->validate($request, $rules);
            try{
                $ofclimitsattus = $request->model_status==1?1:0;
                $action_reasion = ($request->old_ofc_limit > $request->ofc_table_limit)?'Decrease':'Increase';
                $ofctabledata =[
                    'OFC_ACTIVE' => $ofclimitsattus,
                    'OFC_MAX_POINT' => $request->ofc_table_limit,
                    'OFC_UPDATED_BY' => Auth::user()->username,
                    'OFC_FROM_DATE' =>date("Y-m-d H:i:s"),
                    'OFC_TO_DATE' => date("2099-12-21 23:59:59"),
                    'OFC_UPDATED_DATE' => date("Y-m-d H:i:s")
                ];
                $ofcdata = $request->model_status==1?$ofctabledata:['OFC_ACTIVE' => $ofclimitsattus,
                'OFC_UPDATED_BY' => Auth::user()->username,
                'OFC_UPDATED_DATE' => date("Y-m-d H:i:s")];

                $where = ['USER_ID' => $request->model_userid];

                $logsData = array(
                    'USER_ID' => $request->model_userid,
					'REQUEST_ID' =>1,
                    'RESPONSIBLE_SETTINGS_TYPE' => "OFC",
                    'FROM_DATE' => date("Y-m-d H:i:s"),
                    'TO_DATE' => date("2099-12-21 23:59:59"),
                    'STATUS' => $ofclimitsattus,
                    'ACTION' => $action_reasion,
                    'VALUE' => $request->ofc_table_limit,
                    'UPDATED_BY' => Auth::user()->username,
                    'UPDATED_DATE' => date("Y-m-d H:i:s"),
                    'CREATED_DATE' => date("Y-m-d H:i:s")
                );
                $message = ($request->model_status=='1')?'OFC Limit is active':'OFC Limit is in-active';

                /* Check already active or in-active
                *$checkdata['OFC_ACTIVE'],$checkdata['OFC_MAX_POINT']
                */
                $checkdata = ResponsibleGameSetting::select('OFC_ACTIVE','OFC_MAX_POINT')->where($where)->first();
                if($ofclimitsattus == 0){
                    if($ofclimitsattus == $checkdata['OFC_ACTIVE']){
                        return response()->json(['status'=>'201', 'message'=>'Status is already disabled', 'title'=>'Sorry']);
                    }
                }
                if($request->ofc_table_limit == $checkdata['OFC_MAX_POINT'] && $ofclimitsattus != 0){
                    return response()->json(['status'=>'201', 'message'=>'Please change ofc table limit', 'title'=>'Sorry']);
                }

                //common function for update data
                $this->updateCommon($where,$ofcdata);
                //store ofc activity
                $admin_activity_log = \PokerBaazi::storeActivityWithParams($action=$message, json_encode($logsData), $module_id="NULL");
                //insert ofc logs
                $insertLogs = $this->insertgamelogs($logsData);
                // Start Send Mail to user
                $params['email'] = getEmailFromUserId($request->model_userid);
                $params['username'] = getUsernameFromUserId($request->model_userid);
                $params['MAIL_STATUS'] = $request->model_status==1?'Approve':'Reject';
                if($action_reasion == 'Increase' && $params['MAIL_STATUS'] =='Approve'){
                    $params['tempId'] = 44;
                }else if($action_reasion == 'Increase' || $action_reasion == 'Decrease' && $params['MAIL_STATUS'] =='Reject'){
                    $params['tempId'] = 46;
                } else{
                    $params['tempId'] = 43;
                }
                $params['subject'] = 'Your request to revise your OFC table limit has been received, "'.getUsernameFromUserId($request->model_userid) .'"!';
                $params['OFC_MAX_POINT'] = $request->ofc_table_limit;
                $params['GAME_TYPE'] = 'OFC';
                $params['date'] = date('d-m-Y');
                $params['time'] = date('H:i:s');
                $this->sendMail($params);
                //end send mail to user

                return response()->json(['status'=>'200', 'message'=>$message, 'title'=>'Congrats']);
            }catch(\Exception $e){
                return response()->json(['status'=>500, 'message'=>$e->getMessage(),'title'=>'Sorry']);
            }
        };
        return DB::transaction($updateTransaction);
    }

    //listing cash table limit data based on search filter
    function cashtablelimit(request $request){
        $data['cashData'] = [];
        if($request->all()){
            $result = ResponsibleGameSetting::query();
            if (!empty($request->username)) {
                $user_id = getUserIdFromUsername($request->username);
                $result = $result->where('USER_ID', $user_id);
            }
            if(!empty($request->cashlimit)){
                $result = $result->whereIn ('CASH_MAX_BIG_BLIND', $request->cashlimit);
            }
            if (!empty($request->updatedBy)) {
                if($request->updatedBy=='Admin'){
                    $result = $result->where('CASH_UPDATED_BY','!=', 'SELF')
                    ->where('OFC_UPDATED_BY','!=', '0')
                    ->where('OFC_UPDATED_BY','!=', 'NULL');
                }else{
                    $result = $result->where('CASH_UPDATED_BY', $request->updatedBy);
                }
            }
            if ($request->status!='') {
               $result = $result->where('CASH_ACTIVE', $request->status);
            }
            
            if(strtoupper($request->searchbydate) ==strtoupper('start_date')){
                if (!empty($request->date_from)) {
                        $date_from = Carbon::parse($request->date_from);
                        $result = $result->whereDate('CASH_FROM_DATE', ">=", $date_from);
                }
                if (!empty($request->date_to)) {
                    $date_to = Carbon::parse($request->date_to);
                    $result = $result->whereDate('CASH_FROM_DATE', "<=", $date_to);
                }
            }
            if(strtoupper($request->searchbydate) ==strtoupper('update date')){
                if (!empty($request->date_from)) {
                        $date_from = Carbon::parse($request->date_from);
                        $result = $result->whereDate('CASH_UPDATED_DATE', ">=", $date_from);
                }
                if (!empty($request->date_to)) {
                    $date_to = Carbon::parse($request->date_to);
                    $result = $result->whereDate('CASH_UPDATED_DATE', "<=", $date_to);
                }
            }
            $data['cashData'] = $result->select('RESPONSIBLE_GAME_SETTINGS_ID','USER_ID','CASH_FROM_DATE','CASH_TO_DATE','CASH_ACTIVE','CASH_MAX_BIG_BLIND','CASH_UPDATED_BY','CASH_UPDATED_DATE')->orderBy('CASH_UPDATED_DATE','desc')->where('CASH_MAX_BIG_BLIND','!=',0)->get();
        }
        
        $data['params'] = $request->all();
        return view('bo.views.responsiblegaming.cashtablelimit',$data);
    }

    //show data in cash table model click after change status action
    function showcashtable(request $request){
        if($request->cashtableid && $request->USER_ID){
            $data = ResponsibleGameSetting::select('RESPONSIBLE_GAME_SETTINGS_ID','USER_ID','CASH_MAX_BIG_BLIND','CASH_ACTIVE')->where('RESPONSIBLE_GAME_SETTINGS_ID',$request->cashtableid)->where('USER_ID',$request->USER_ID)->first();
            return response(['status'=>'200', 'message'=>"success", 'data'=>$data]);
        };
    }

    //update cash limit
    function updatecashlimit(request $request){ 
        $updateTransaction = function() use($request){
            $rules = [
                'model_status' => 'required',
                'cash_table_limit' => 'required_if:model_status,1'
            ];
            $this->validate($request, $rules);
            try{
                $cashlimitsattus = $request->model_status==1?1:0;
                $action_reasion = ($request->old_cash_limit > $request->cash_table_limit)?'Decrease':'Increase';
                $cashtabledata =[
                    'CASH_ACTIVE' => $cashlimitsattus,
                    'CASH_MAX_BIG_BLIND' => $request->cash_table_limit,
                    'CASH_UPDATED_BY' => Auth::user()->username,
                    'CASH_FROM_DATE' =>date("Y-m-d H:i:s"),
                    'CASH_TO_DATE' => date("2099-12-21 23:59:59"),
                    'CASH_UPDATED_DATE' => date("Y-m-d H:i:s")
                ];
                $cashdata = $request->model_status==1?$cashtabledata:['CASH_ACTIVE' => $cashlimitsattus,
                'CASH_UPDATED_BY' => Auth::user()->username,
                'CASH_UPDATED_DATE' => date("Y-m-d H:i:s")];

                $where = ['USER_ID' => $request->model_userid];

                $logsData = array(
                    'USER_ID' => $request->model_userid,
                    'REQUEST_ID'=>1,
                    'RESPONSIBLE_SETTINGS_TYPE' => "CASH",
                    'FROM_DATE' => date("Y-m-d H:i:s"),
                    'TO_DATE' => date("2099-12-21 23:59:59"),
                    'STATUS' => $cashlimitsattus,
                    'ACTION' => $action_reasion,
                    'VALUE' => $request->cash_table_limit,
                    'UPDATED_BY' => Auth::user()->username,
                    'UPDATED_DATE' => date("Y-m-d H:i:s"),
                    'CREATED_DATE' => date("Y-m-d H:i:s")
                );
                $message = ($request->model_status=='1')?'Cash Limit is active':'Cash Limit is in-active';

                /* Check already active or inactive
                *$checkdata['CASH_ACTIVE'],$checkdata['CASH_MAX_BIG_BLIND']
                */
                $checkdata = ResponsibleGameSetting::select('CASH_ACTIVE','CASH_MAX_BIG_BLIND')->where($where)->first();
                if($cashlimitsattus == 0){
                    if($cashlimitsattus == $checkdata['CASH_ACTIVE']){
                        return response()->json(['status'=>'201', 'message'=>'Status is already disabled', 'title'=>'Sorry']);
                    }
                }
                if($request->cash_table_limit == $checkdata['CASH_MAX_BIG_BLIND'] && $cashlimitsattus != 0){
                    return response()->json(['status'=>'201', 'message'=>'Please change cash table limit', 'title'=>'Sorry']);
                }
                //update data in responsible game setting
                $this->updateCommon($where,$cashdata);
                //store activity
                $admin_activity_log = \PokerBaazi::storeActivityWithParams($action=$message, json_encode($logsData), $module_id="NULL");
                //insert cash limit logs
                $insertLogs = $this->insertgamelogs($logsData);

                //start Send Mail to user
                $params['email'] = getEmailFromUserId($request->model_userid);
                $params['username'] = getUsernameFromUserId($request->model_userid);
                $params['MAIL_STATUS'] = $request->model_status==1?'Approve':'Reject';
                if($action_reasion == 'Increase' && $params['MAIL_STATUS'] =='Approve'){
                    $params['tempId'] = 36;
                }else if($action_reasion == 'Increase' || $action_reasion == 'Decrease' && $params['MAIL_STATUS'] =='Reject'){
                    $params['tempId'] = 38;
                } else{
                    $params['tempId'] = 35;
                }
                $params['subject'] = 'Your Cash Table limit has been revised, "'.getUsernameFromUserId($request->model_userid) .'"!';
                $params['CASH_MAX_BIG_BLIND'] = $request->cash_table_limit;
                $params['GAME_TYPE'] = 'CASH';
                $params['date'] = date('d-m-Y');
                $params['time'] = date('H:i:s');
                $this->sendMail($params);
                //end send mail to user
                return response()->json(['status'=>'200', 'message'=>$message, 'title'=>'Congrats']);
            }catch(\Exception $e){
                return response()->json(['status'=>500, 'message'=>$e->getMessage(),'title'=>'Sorry']);
            }
        };
        return DB::transaction($updateTransaction);
    }

    //listing all user request based on search filter
    function allrequest(request $request){
        $data['allrequest'] = [];
        if($request->all()){
            $result = ResponsibleGamingRequest::query();
            if (!empty($request->username)) {
                $user_id = getUserIdFromUsername($request->username);
                $result = $result->where('USER_ID', $user_id);
            }
            if(!empty($request->requestfor)){
                $result = $result->where('GAME_TYPE', $request->requestfor);
            }
            if ($request->status!='') {
               $result = $result->where('STATUS', $request->status);
            }
            if (!empty($request->date_from)) {
                $date_from = Carbon::parse($request->date_from);
                $result = $result->whereDate('STATUS_UPDATED_ON', ">=", $date_from);
            }
            if (!empty($request->date_to)) {
                $date_to = Carbon::parse($request->date_to);
                $result = $result->whereDate('STATUS_UPDATED_ON', "<=", $date_to);
            }
            $data['allrequest'] = $result->select('REQUEST_ID','USER_ID','GAME_TYPE','CURRENT_LIMIT','REQUESTED_LIMIT','REASON','REQUESTED_DATE','STATUS_UPDATED_ON','STATUS','STATUS_UPDATED_BY')->orderBy('REQUESTED_DATE', 'desc')->get();
            Session::put('getAllRequest',$data['allrequest']);
        }
        
        $data['params'] = $request->all();
        return view('bo.views.responsiblegaming.allrequest',$data);
    }

    //update user request
    function updaterequest(request $request){
        $updateTransaction = function() use($request){
            $rules = [
                'model_status' => 'required',
                'model_reasion' => 'required_if:model_status,10'
            ];
            $this->validate($request, $rules);
            try{
                $whereNotificationTable = array(
                    "USER_ID" => $request->model_userid,
                    "REQUEST_ID" => $request->request_id,
                );
                
                $recentNotification = ResponsibleGamingRequest::where($whereNotificationTable)->first();
                $where_log_match=['USER_ID' => $request->model_userid,'STATUS'=>$request->old_status,'RESPONSIBLE_SETTINGS_TYPE'=>$recentNotification->GAME_TYPE,'VALUE'=>$recentNotification->REQUESTED_LIMIT];

                $getlogsdata = ResponsibleGameLog::select('RESPONSIBLE_GAME_LOGS_ID','USER_ID','RESPONSIBLE_SETTINGS_TYPE','STATUS','VALUE')->where($where_log_match)->first();
                $where = ['USER_ID' => $request->model_userid];
                $logsData = array(
                    'STATUS' => $request->model_status,
                    'UPDATED_BY' => Auth::user()->username,
                    'UPDATED_DATE' => date("Y-m-d H:i:s")
                );
                $whereLogs = array(
                    "USER_ID" => $request->model_userid,
                    'RESPONSIBLE_SETTINGS_TYPE' => $recentNotification->GAME_TYPE,
                    //'RESPONSIBLE_GAME_LOGS_ID' => $getlogsdata->RESPONSIBLE_GAME_LOGS_ID,
                    'REQUEST_ID' => $request->request_id
                );

                $updateGamingRequest = array(
                    'STATUS' => $request->model_status,
                    'STATUS_UPDATED_BY' => Auth::user()->username,
                    'STATUS_UPDATED_ON' => date('Y-m-d H:i:s'),
                    'REASON' => $request->model_reasion
                );
                if($recentNotification){
                    //if status approve
                    if($request->model_status == 11){
                        if($recentNotification->GAME_TYPE=='DEPOSIT_LIMIT'){
                            $subject = 'Your request to revise your deposit limits have been received, ';
                            $tempID = ($recentNotification->REQUESTED_LIMIT > $recentNotification->CURRENT_LIMIT)?'40':'39';
                            $depositLimit = explode(",",$recentNotification->REQUESTED_LIMIT);
                            $depositLimitUserdata = array(
                                'DEP_LEVEL' => Auth::user()->username,
                                'DEP_AMOUNT_PER_DAY' => $depositLimit[0],
                                'DEP_AMOUNT_PER_WEEK' => $depositLimit[1],
                                'DEP_AMOUNT_PER_MONTH' => $depositLimit[2],
                                'UPDATED_STATUS' => 1,
                                'UPDATED_BY' => Auth::user()->username,
                                'UPDATED_ON' => date('Y-m-d H:i:s'),
                            
                            );

                        }else if($recentNotification->GAME_TYPE=='CASH'){
                            $subject = 'Your Cash Table limit has been revised, ';
                            $tempID = $recentNotification->CURRENT_LIMIT < $recentNotification->REQUESTED_LIMIT?'36':'35';
                            $depositLimitUserdata = array(
                                'CASH_MAX_BIG_BLIND' => $recentNotification->REQUESTED_LIMIT,
                                'CASH_ACTIVE' => 1,
                                'CASH_UPDATED_BY' => Auth::user()->username,
                                'CASH_UPDATED_DATE' => date('Y-m-d H:i:s'),
                            );
                        } else if($recentNotification->GAME_TYPE=='OFC'){
                            $subject = 'Your OFC table limit has been revised, ';
                            $tempID = $recentNotification->CURRENT_LIMIT < $recentNotification->REQUESTED_LIMIT?'44':'43';
                            $depositLimitUserdata = array(
                                'OFC_MAX_POINT' => $recentNotification->REQUESTED_LIMIT,
                                'OFC_ACTIVE' => 1,
                                'OFC_UPDATED_BY' => Auth::user()->username,
                                'OFC_UPDATED_DATE' => date('Y-m-d H:i:s'),
                            
                            );
                        }

                        $message = $subject;
                        //update data in Game request
                        $this->updateResponsibleGameRequest($updateGamingRequest,$whereNotificationTable);
                        //update data in logs
                        $this->updateLogs($logsData,$whereLogs);
                        //update data in responsible game setting if status 11 (Approve)
                        $updateResponsibleGameSettings = $recentNotification->GAME_TYPE=='DEPOSIT_LIMIT'?$this->updatedepositLimitUser($where,$depositLimitUserdata):$this->updateCommon($where,$depositLimitUserdata);

                        //start send mail to user
                        $params = $depositLimitUserdata;
                        $params['tempId'] = $tempID;
                        $params['email'] = getEmailFromUserId($request->model_userid);
                        $params['username'] = getUsernameFromUserId($request->model_userid);
                        $params['MAIL_STATUS'] = 'Approve';
                        $params['subject'] = $subject .getUsernameFromUserId($request->model_userid);
                        $params['GAME_TYPE'] = $recentNotification->GAME_TYPE;
                        $params['date'] = date('d-m-Y');
                        $params['time'] = date('H:i:s');
                        $params['dep_amount'] = 'No Change';
                        $params['transactions_per_day'] = 'No Change';
                        $params['transactions_per_week'] = 'No Change';
                        $params['transactions_per_month'] = 'No Change';
                        $this->sendMail($params);
                        //end send mail to user
                        
                    }else{
                        //if status rejected
                        if($recentNotification->GAME_TYPE=='DEPOSIT_LIMIT') {
                            $params['tempId'] = 42;
                            $params['dep_amount'] = "";
                            $params['DEP_AMOUNT_PER_DAY'] = "";
                            $params['DEP_AMOUNT_PER_WEEK'] = "";
                            $params['DEP_AMOUNT_PER_MONTH'] = "";
                            $params['transactions_per_day'] = "";
                            $params['transactions_per_week'] = "";
                            $params['transactions_per_month'] = "";
                            $subject = 'Your deposit limits have been declined, ';
                        } elseif($recentNotification->GAME_TYPE=='CASH') {
                            $params['tempId'] = 38;
                            $params['CASH_MAX_BIG_BLIND'] = "";
                            $subject = 'Your Cash Table limit request has been declined, ';
                        } else if($recentNotification->GAME_TYPE=='OFC') {
                            $params['tempId'] = 46;
                            $params['OFC_MAX_POINT'] ="";
                            $subject = 'Your OFC Table limit request has been declined, ';
                        }
                        $message = $subject;
                        //update data in logs
                        $this->updateLogs($logsData,$whereLogs);
                        //update data in Game request
                        $this->updateResponsibleGameRequest($updateGamingRequest,$whereNotificationTable);

                        //send mail
                        $params['email'] = getEmailFromUserId($request->model_userid);
                        $params['username'] = getUsernameFromUserId($request->model_userid);
                        $params['MAIL_STATUS'] = 'Reject';
                        $params['subject'] = $subject .getUsernameFromUserId($request->model_userid);
                        $params['GAME_TYPE'] = $recentNotification->GAME_TYPE;
                        $params['date'] = date('d-m-Y');
                        $params['time'] = date('H:i:s');
                        $this->sendMail($params);
                    }
                    $admin_activity_log = \PokerBaazi::storeActivityWithParams($action=$message, json_encode($logsData), $module_id="NULL");
                    return response()->json(['status'=>'200', 'message'=>$message, 'title'=>'Congrats']);
                }else{
                    return response()->json(['status'=>'200', 'message'=>"Updation Failed", 'title'=>'Sorry']);
                }
            }catch(\Exception $e){
                return response()->json(['status'=>500, 'message'=>$e->getMessage(),'title'=>'Sorry']);
            }
        };
        return DB::transaction($updateTransaction);
    }

    //common function for update responsible gameing log table based on where condition
    function updateLogs($data,$where){
        $result = new ResponsibleGameLog($data);
        return $result->where($where)->update($data);
    }

    //update responsible gaming requset table
    function updateResponsibleGameRequest($data,$where){
        $result = new ResponsibleGamingRequest($data);
        return $result->where($where)->update($data);
    }

    //update deposit limit user
    function updatedepositLimitUser($where,$data){
        $result = new DepositLimitUser($data);
        return $result->where($where)->update($data);
    }

    //get excel export for responsible gaming request
    public function excelExport(){
        $requestData = Session::get('getAllRequest');
        foreach($requestData as $allRequest){
            $data_array[] = array(
                "USERNAME"          =>  getUsernameFromUserId($allRequest['USER_ID']),
                "REQUESTED_FOR"     =>  $allRequest['GAME_TYPE'],
                "CURRENT_LIMIT"     =>  $allRequest['CURRENT_LIMIT'],
                "REQUESTED_LIMIT"   =>  $allRequest['REQUESTED_LIMIT'],
                "REASON"            =>  $allRequest['REASON'],
                "REQUESTED_DATE"    =>  $allRequest['REQUESTED_DATE'],
                "STATUS"            =>  $allRequest['STATUS']==10?'Reject':$allRequest['STATUS']==11?           'Approve':$allRequest['STATUS']==12?'Pending':'AutoApprove',
                "STATUS_UPDATED_BY" =>  $allRequest['STATUS_UPDATED_BY'],
                "STATUS_UPDATED_ON" =>  $allRequest['STATUS_UPDATED_ON'],
            );
        }
        $headingofsheetArray = array('Username', 'Request For', 'Current Limit', 'Requested Limit', 'Reasion', 'Requested Date', 'Status', 'Updated By', 'Updated On');
        $fileName = urlencode("responsible_gaming_request" . date('d-m-Y') . ".xlsx");
        return Excel::download(new CollectionExport($data_array, $headingofsheetArray), $fileName);
    }

    //get excel export for poker break
    public function excelExportforPokerBreak(){
        $pokerBreakData = Session::get('getAllPokerBreakData');
        foreach($pokerBreakData as $pokerBreak){
            $data_array[] = array(
                "USERNAME"             =>  getUsernameFromUserId($pokerBreak['USER_ID']),
                "BREAK_ACTIVE"         =>  $pokerBreak['BREAK_ACTIVE']==1?'Enabled':'Disabled',
                "BREAK_FROM_DATE"      =>  $pokerBreak['BREAK_FROM_DATE'],
                "BREAK_TO_DATE"        =>  $pokerBreak['BREAK_TO_DATE'],
                "BREAK_UPDATED_BY"     =>  $pokerBreak['BREAK_UPDATED_BY'],
                "BREAK_UPDATED_DATE"   =>  $pokerBreak['BREAK_UPDATED_DATE']
            );
        }
        $headingofsheetArray = array('Username', 'PB Status', 'Break From', 'Break Till', 'Updated By', 'Updated ON');
        $fileName = urlencode("poker_break" . date('d-m-Y') . ".xlsx");
        return Excel::download(new CollectionExport($data_array, $headingofsheetArray), $fileName);
    }

}
