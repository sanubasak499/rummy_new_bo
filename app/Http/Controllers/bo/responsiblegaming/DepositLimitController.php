<?php

namespace App\Http\Controllers\bo\responsiblegaming;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\DepositLimitUser;
use App\Models\ResponsibleGameLog;
use Auth;
use App\Traits\common_mail;

class DepositLimitController extends Controller
{
    use common_mail;
    public function index(Request $request){
        $user_id = decrypt($request->user_id);
        $data['deposit_limit_user_data'] = self::getCommondata($user_id);
        return view('bo.views.responsiblegaming.editdepositlimit',$data);
    }

    public function store(Request $request){
        $this->validate($request, [
            'deposit_amount' => 'required|numeric|lte:dep_amount_per_day',
            'dep_amount_per_day' => 'required|numeric|lte:deposit_limit_per_week',
            'deposit_limit_per_week' =>'required|numeric|gte:dep_amount_per_day',
            'deposit_limit_per_month' =>'required|numeric|gte:deposit_limit_per_week',
            'no_of_transaction_per_day' =>'required|numeric|lte:no_of_transaction_per_week',
            'no_of_transaction_per_week' =>'required|numeric|gte:no_of_transaction_per_day',
            'no_of_transaction_per_month' =>'required|numeric|gte:no_of_transaction_per_week',
        ]);
        if(!empty($request->user_id)){
            $depositLimit["dep_level"] 	= "ADMIN";
            $depositLimit["updated_by"] = Auth::user()->username;
            $depositLimit["dep_amount"]	= $request->deposit_amount;
            $depositLimit["dep_amount_per_day"] = $request->dep_amount_per_day;
            $depositLimit["transactions_per_day"] = $request->no_of_transaction_per_day;
            $depositLimit["dep_amount_per_week"] = $request->deposit_limit_per_week;
            $depositLimit["transactions_per_week"] = $request->no_of_transaction_per_week;
            $depositLimit["dep_amount_per_month"] = $request->deposit_limit_per_month;
            $depositLimit["transactions_per_month"] = $request->no_of_transaction_per_month;
            $depositLimit["updated_on"] = date('Y-m-d H:i:s');
            $userid['USER_ID'] = decrypt($request->user_id);

            // fetch Old Data
            $old_data = self::getCommondata($userid['USER_ID']);
            $old_dep_amount_per_month = round($old_data->dep_amount_per_month);
            $old_dep_amount = round($old_data->dep_amount);
            $old_txn_per_month = $old_data->transactions_per_month;

            // Check request data modified or not
            if($old_txn_per_month != $depositLimit["transactions_per_month"] || $old_dep_amount_per_month != $depositLimit["dep_amount_per_month"] || $old_dep_amount != $depositLimit["dep_amount"]){

                // Check deposit limit increase or decrease
                if($depositLimit["dep_amount_per_month"] > $old_dep_amount_per_month || $depositLimit["transactions_per_month"] > $old_txn_per_month || $depositLimit["dep_amount"] > $old_dep_amount){
                    $ACTION_REASION = "Increase";
                } else {
                    $ACTION_REASION = "Decrease";
                }

                // Check Input data
                if($depositLimit["dep_amount"] > $depositLimit["dep_amount_per_day"]) {
                    return back()->withInput()->with('error', 'Txn limit must be less than daily limit.');
                }else if($depositLimit["dep_amount_per_day"] > $depositLimit["dep_amount_per_week"]) {
                    return back()->withInput()->with('error', 'Daily limit must be less than weekly limit.');
                }else if($depositLimit["dep_amount_per_week"] > $depositLimit["dep_amount_per_month"]) {
                    return back()->withInput()->with('error', 'Weekly limit must be less than monthly limit.');
                }else if($depositLimit["transactions_per_day"] > $depositLimit["transactions_per_week"]) {
                    return back()->withInput()->with('error', 'Daily count must be less than weekly count.');
                }else if($depositLimit["transactions_per_week"] > $depositLimit["transactions_per_month"]) {
                    return back()->withInput()->with('error', 'Weekly count must be less than monthly count.');
                }

                //update deposit limit user table
                $updateDepositLimit = self::updateDepositLimitUser($depositLimit,$userid);
                if($updateDepositLimit){

                    //Logs data
                    $data = array(
                        'USER_ID' => $userid['USER_ID'],
                        'RESPONSIBLE_SETTINGS_TYPE' => "DEPOSIT_LIMIT",
                        'REQUEST_ID'    =>1,
                        'FROM_DATE' => date('Y-m-d H:i:s'),
                        'TO_DATE' => date('2099-12-31 23:59:59'),
                        'STATUS' => '1',
                        'ACTION' => $ACTION_REASION,
                        'VALUE' => $depositLimit["dep_amount"].','.$depositLimit["dep_amount_per_day"].','.$depositLimit["transactions_per_day"].','.$depositLimit["dep_amount_per_week"].','.$depositLimit["transactions_per_week"].','.$depositLimit["dep_amount_per_month"].','.$depositLimit["transactions_per_month"],
                        'UPDATED_BY' => Auth::user()->username,
                        'UPDATED_DATE' => date('Y-m-d H:i:s'),
                        'CREATED_DATE' => date('Y-m-d H:i:s')
                    );

                    //Insert Data in logs table
                    $insertLogs = self::insertLogsdata($data);

                    //send mail
                    $username = getUsernameFromUserId($userid['USER_ID']);
                    $emaildata = array(
                        "username" => $username,
                        "email" => getEmailFromUserId($userid['USER_ID']),
                        "MAIL_STATUS" => $ACTION_REASION
                    );

                    if($ACTION_REASION = "Increase"){
                        $emaildata['tempId'] = 40;
                    }else{
                        $emaildata['tempId'] = 39;
                    }
                    
                    $emaildata['subject'] = 'Your deposit limits have been revised, "'.$username .'"!';
                
                    $emaildata['dep_amount'] = $depositLimit["dep_amount"];
                    $emaildata['DEP_AMOUNT_PER_DAY'] =  $depositLimit["dep_amount_per_day"];
                    $emaildata['DEP_AMOUNT_PER_WEEK'] =  $depositLimit["dep_amount_per_week"];
                    $emaildata['DEP_AMOUNT_PER_MONTH'] =  $depositLimit["dep_amount_per_month"];
                    $emaildata['transactions_per_day'] =  $depositLimit["transactions_per_day"];
                    $emaildata['transactions_per_week'] =  $depositLimit["transactions_per_week"];
                    $emaildata['transactions_per_month'] = $depositLimit["transactions_per_month"];
                
                    $emaildata['GAME_TYPE'] = 'DEPOSIT_LIMIT';
                    $this->sendMail($emaildata);
                    //end send mail to user
                    return back()->withInput()->with('success', 'Deposit limit modified successfully');

                }else{
                    return back()->withInput()->with('error', 'Deposit limit modified failed please try again?');
                }

            }else{
                return back()->withInput()->with('error', 'Change Any Deposit limit Value?.');
            }
        }else{
            return back()->withInput()->with('error', 'Invalid User');
        }
    }

    public function getCommondata($user_id){
        return DepositLimitUser::select('deposit_limit_user.dep_amount_per_day','deposit_limit_user.dep_amount_per_month','deposit_limit_user.USER_ID','deposit_limit_user.deposit_limit_user_id','deposit_limit_user.dep_amount','deposit_limit_user.dep_level','deposit_limit_user.dep_amount_per_week','deposit_limit_user.transactions_per_day','deposit_limit_user.transactions_per_month','deposit_limit_user.transactions_per_week','user.username','deposit_limit_user.updated_on','deposit_limit_user.created_by','deposit_limit_user.updated_by','deposit_limit_user.created_date')
        ->join('user', 'user.USER_ID','=','deposit_limit_user.USER_ID')
        ->where('deposit_limit_user.USER_ID',$user_id)
        ->first();
    }

    //common update deposit limit user
    public function updateDepositLimitUser($depositLimit,$userid){
        return DepositLimitUser::where($userid)->update($depositLimit);
    }

    //common insert log data 
    public function insertLogsdata($data){
        $insertData = new ResponsibleGameLog($data);
        return $insertData->save($data);
    }
}
