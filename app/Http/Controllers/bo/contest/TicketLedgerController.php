<?php

namespace App\Http\Controllers\bo\contest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TournamentUserTicket;
use App\Models\Tournament;
use App\Models\ActionReason;
use Carbon\Carbon;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

/**
 * Author: Nitesh Kumar Jha
 * Purpose: Get all data related to user assigned ticked 
 * Created 10-09-2020 
 */

class TicketLedgerController extends Controller
{

    public function index()
    {

        $ActionReasonResult = $this->ActionReasonResult();

        return view('bo.views.contest.TicketLedger', ['ActionReasonResult' => $ActionReasonResult]);
    }
    // Search Ticket Ledgers report deatils 
    public function TicketLedgerSearch(Request $request)
    {
        $perPage = config('poker_config.paginate.per_page');
        if (!$request->isMethod('post')) {
            return view('bo.views.contest.TicketLedger');
        }

        $ActionReasonResult = $this->ActionReasonResult();

        $page = request()->page;

        $TicketLedgersResult = $this->getAllResult($request);
        $TicketLedgersResult =  $TicketLedgersResult->paginate($perPage, ['*'], 'page', $page);
        $params = $request->all();
        $params['page'] = $page;

        return view('bo.views.contest.TicketLedger', ['TicketLedgersResult' => $TicketLedgersResult, 'params' => $params, 'ActionReasonResult' => $ActionReasonResult]);
    }
    public function getAllResult($request)
    {   
        if ($request->search_by == 'TOURNAMENT_ID') {
            $TournamentIDresult[] = $request->search_by_value;
        } elseif ($request->search_by == 'TOURNAMENT_NAME') {
            $TournamentIDresult = Tournament::select('TOURNAMENT_ID')
                ->when($request->search_by == "TOURNAMENT_NAME", function ($query) use ($request) {
                    return $query->where('TOURNAMENT_NAME', 'LIKE', "%{$request->search_by_value}%");
                })
                ->whereIn('TOURNAMENT_TYPE_ID', [3, 4])
                ->where('IS_ACTIVE', 1)
                ->where('LOBBY_ID', 2)
                ->get();
            $ResultValue = array();
            foreach ($TournamentIDresult as $ResultVal) {
                $ResultValue[] = (array) $ResultVal->TOURNAMENT_ID;
            }
            if (!empty($TournamentIDresult = $ResultValue)) {
                $TournamentIDresult = $ResultValue;
            } else {
                $TournamentIDresult[] = '';
            }
        } else {
            $TournamentIDresult[] = '';
        }

        $TicketLedgersResult = TournamentUserTicket::from(app(TournamentUserTicket::class)->getTable() . " as tut")
            ->select(
                'tut.TOURNAMENT_USER_TICKET_ID',
                'tut.TICKET_SOURCE',
                'tut.TICKET_REASON',
                'tut.CREATED',
                'tut.UPDATED',
                't.TOURNAMENT_NAME',
                'u.USERNAME'
            )
            ->join('tournament as t', 't.TOURNAMENT_ID', '=', 'tut.TOURNAMENT_ID')
            ->join('user as u', 'u.USER_ID', '=', 'tut.USER_ID')

            ->when(!empty($TournamentIDresult), function ($query) use ($TournamentIDresult) {
                return $query->whereIn('tut.TOURNAMENT_ID', $TournamentIDresult);
            })

            ->when(!empty($request->TICKET_SOURCE), function ($query) use ($request) {
                return $query->where('tut.TICKET_SOURCE', $request->TICKET_SOURCE);
            })
            ->when(!empty($request->TICKET_REASON), function ($query) use ($request) {
                return $query->where('tut.TICKET_REASON', $request->TICKET_REASON);
            })

            ->when(!empty($request->START_DATE_TIME), function ($query) use ($request) {
                $dateFrom = Carbon::parse($request->START_DATE_TIME)->format('Y-m-d H:i:s');
                return $query->whereDate('tut.CREATED', ">=", $dateFrom);
            })
            ->when(!empty($request->END_DATE_TIME), function ($query) use ($request) {
                $dateTo = Carbon::parse($request->END_DATE_TIME)->format('Y-m-d H:i:s');
                return $query->whereDate('tut.CREATED', "<=", $dateTo);
            })
            ->orderBy('tut.TOURNAMENT_USER_TICKET_ID', 'DESC');

        return $TicketLedgersResult;
    }

    public function ActionReasonResult()
    {
        $reasonCategoryId = config('poker_config.reasonCategoryId.reason_category_id');
        $ActionReasonResult = ActionReason::select('ACTIONS_REASON')
            ->where('REASON_CATEGORY_ID', $reasonCategoryId)
            ->where('STATUS', 1)
            ->orderBy('CREATED_DATE', 'DESC')
            ->get();
        return $ActionReasonResult;
    }

    //Export To Excel 
    public function exportExcel(Request $request)
    {
        $TicketLedgersResult = $this->getAllResult($request)->get();

        foreach ($TicketLedgersResult as $value) {

            $excelData[] =
                array(
                    'TOURNAMENT_NAME' => $value->TOURNAMENT_NAME,
                    'USERNAME' => $value->USERNAME,
                    'CREATED' => $value->CREATED,
                    'TICKET_SOURCE' => $value->TICKET_SOURCE,
                    'TICKET_REASON' => $value->TICKET_REASON,
                    'UPDATED' => $value->UPDATED,
                );
        }

        $data[0] = array("TOURNAMENT_NAME" => "", "USERNAME" => "", "CREATED" => "", "TICKET_SOURCE" => "", "REASON" => "", "TICKET ASSIGNED ON" => "");
        $name = "Ticket-Ledgers";
        $fileName = $name . ".xlsx";
        $headings = array_keys($data[0]);

        return Excel::download(new CollectionExport($excelData, $headings), $fileName);
    }
}
