<?php

namespace App\Http\Controllers\bo\settings\reason;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use\App\Models\ReasonCategory;
use\App\Models\ActionReason;
use DB;
use Auth;

class ManageReasonController extends Controller
{
    public function index(){

    	$categories = ReasonCategory::active()->get();
        
    	return view('bo.views.settings.reason.action.index',['activeCategories' => $categories, 'dataSets' => ActionReason::notDeleted()->orderBy('ACTIONS_REASONS_ID','DESC')->get()]);
    }

    public function store(Request $request){

    	$request->validate([
    		'reason_category_id'=>'required',
    		'actions_reason'=>'required'
    	]);

    	$data['reason_category_id'] = implode(',',$request->reason_category_id);
    	$data['actions_reason'] = $request->actions_reason;
    	$data['actions_reason_description'] = $request->actions_reason_description;
    	$data['updated_by'] = $data['created_by'] = Auth::user()->username;
    	$data['created_date'] = $data['update_date'] = date('Y-m-d H:i:s'); 
    
    	if(ActionReason::create($data)){
    		return redirect()->back()->with('success', "Reason Created Successfully.");
    	}
    	else{
    		return redirect()->back()->with('danger', "Failed To Create Reason.");
    	}
    }

    public function update(Request $request, $id){

        

    	$actionReason = ActionReason::find($id);
    	$actionReason->updated_by=Auth::user()->username;
    	$actionReason->update_date=date('Y-m-d H:i:s');

    	if($request->has('action') && $request->action='toggleStatus'){     		
    		if($request->value!='true' && $request->value!='false'){
    			$actionReason->status=$request->value;
    			$actionReason->save(); 
    			return redirect()->back()->with('success','Reason Deleted Successfully.');
    		}
    		else{
    			$actionReason->status=($request->value=='true'? 1 : 0);
    			$actionReason->save();
    			return response()->json($actionReason);
    		}
    	}

    	$actionReason->actions_reason = $request->actions_reason;
    	$actionReason->actions_reason_description = $request->actions_reason_description;
    	$actionReason->reason_category_id = implode(',',$request->reason_category_id);

        if($actionReason->save()){
            return redirect()->back()->with('success', "Reason Updated Successfully.");
        }
        else{
            return redirect()->back()->with('danger', "Failed To Update Reason.");
        }

    }

    public function show($id){
    	return ActionReason::findOrFail($id);
    }

}
