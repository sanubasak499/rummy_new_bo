<?php

namespace App\Http\Controllers\bo\settings\reason;

use App\Http\Controllers\bo\BaseController;
use Illuminate\Http\Request;
use App\Models\ReasonCategory;
use DB;
use Auth;

class ManageReasonCategoryController extends BaseController
{
    public function index(){
    	return view('bo.views.settings.reason.category.index',['dataSets' => ReasonCategory::orderBy('REASON_CATEGORY_ID','DESC')->get()]);
    }

    public function store(Request $request){

    	$request->validate([
    		'category_name' => 'required|unique:reason_category,reason_category_name|string',
    		'category_description' => 'nullable|string'
    	]);

    	$data['reason_category_name'] = $request->category_name;
    	$data['reason_category_description'] = $request->category_description;
    	$data['created_by'] = Auth::user()->username;
    	$data['updated_by'] = Auth::user()->username;
    	$data['created_date'] = date('Y-m-d H:i:s');

    	if(ReasonCategory::create($data)){
    		return redirect()->back()->with('success', "Reason Category Created Successfully.");
    	}
        else{
            return redirect()->back()->with('danger', "Failed To Create Reason Category.");
        }

    }

    public function show($id){
    	return ReasonCategory::findOrFail($id);
    }	

    public function update(Request $request, $id){

		if($request->has('action') && $request->action=='toggleStatus'){
    		$category = ReasonCategory::where('REASON_CATEGORY_ID', $id)->update(
    			[
    				'STATUS' => $request->value == "true" ? 1 : 0, 
    				'updated_by'=>Auth::user()->username, 
    				'update_date'=>date('Y-m-d H:i:s')
    			]
    		);
    		return response()->json($category);
    	}

        $category = ReasonCategory::find($id); 

    	$request->validate([
    		'category_name' => 'required|unique:reason_category,reason_category_name,'.$category->REASON_CATEGORY_ID.',REASON_CATEGORY_ID|string',
    		'category_description' => 'nullable|string'
    	]);

    	$category = ReasonCategory::find($id);

    	$category->reason_category_name = $request->category_name;
    	$category->reason_category_description = $request->category_description;
    	$category->updated_by = Auth::user()->username;
    	$category->update_date = date('Y-m-d H:i:s');
    	


    	if($category->save()){
    		return redirect()->back()->with('info', "Reason Category Updated.");
    	}
    	else{
    		return redirect()->back()->with('danger', "Failed To Updatde Reason Category.");
    	}

    }
}
