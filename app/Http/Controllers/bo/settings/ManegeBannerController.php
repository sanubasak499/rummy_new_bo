<?php

namespace App\Http\Controllers\bo\settings;

use App\Traits\s3Bucket;
use App\Http\Controllers\bo\BaseController;
use Illuminate\Http\Request;
use App\Models\Tournament;
use App\Models\AppTypes;
use App\Models\BannerCategories;
use App\Models\BannerTypes;
use App\Models\AppBanners;
use App\Models\SegmentType;
use Session;
use DB;
use Auth;
use App\Facades\PokerBaazi;

/**
 * Author: Nitesh Kumar Jha
 * Purpose: Get all the Manage Banner details 
 * Created Date: 11-12-2019 To 13-12-2019
 */
class ManegeBannerController extends BaseController
{
    /**
     * This method use to upload file in to S3Bucket 
     * @class app/traits/s3bucket
     * @method s3FileUpload
     * @param $file, $filePath, $allowedFormats, $hasFile
     */
    use s3Bucket;

    public function __construct()
    {
        $this->tournamentModel = app(Tournament::class);
        $this->AppBannersModel = app(AppBanners::class);
        $this->AppTypesModel = app(AppTypes::class);
        $this->BannerCategoriesModel = app(BannerCategories::class);
        $this->BannerTypesModel = app(BannerTypes::class);
    }


    // View Add Banner
    public function index()
    {
        $data['SegmentType'] = SegmentType::select('SEGMENT_ID', 'SEGMENT_TITLE')
            ->where('STATUS', 1)
            ->orderBy('SEGMENT_ID', 'DESC')
            ->get();

        // print_r($data['SegmentType']);die();
        $data['AppTypeResult'] = $this->AppTypesModel->AppTypeResult();
        $data['BannerCategoriesResult'] = $this->BannerCategoriesModel->BannerCategoriesResult();
        $data['BannerTypeResult'] = $this->BannerTypesModel->BannerTypeResult();
        $data['TournamentResult'] = $this->tournamentModel->tournamentDetailsIsActiveOne();
        return view('bo.views.settings.banner.index', $data);
    }

    // Insert Banner
    public function insertbanner(Request $request)
    {
        request()->page;
        $request->validate([
            "APP_TYPE_ID" => 'required',
            "BANNER_TYPE_ID"    => 'required',
            "BANNER_CATEGORY_ID" => 'required',
            "BANNER_LINK_BUTTON_TEXT" => 'required',
            "START_TIME" => 'required',
            "END_TIME" => 'required',
        ]);

        $APP_TYPE_ID = $request->APP_TYPE_ID;
        $BANNER_CATEGORY_ID = $request->BANNER_CATEGORY_ID;
        $BANNER_TYPE_ID = $request->BANNER_TYPE_ID;
        $BANNER_LINK_BUTTON_TEXT = $request->BANNER_LINK_BUTTON_TEXT;
        $TOURNAMENT_ID = $request->TOURNAMENT_ID;
        $BANNER_REDIRECTION_URL = $request->BANNER_REDIRECTION_URL;
        $SEGMENT_ID = $request->SEGMENT_ID;
        $START_TIME = $request->START_TIME;
        $END_TIME = $request->END_TIME;

        if($request->STATUS==1){
            $STATUS = $request->STATUS;
        }else{
            $STATUS=0;
        }

        if ($TOURNAMENT_ID != '') {
            $TournamentResult = $this->tournamentModel->tournamentDetailsWithServerDetails($TOURNAMENT_ID);
            $TOURNAMENT_NAME = $TournamentResult[0]->TOURNAMENT_NAME;
            $SERVER_ID = $TournamentResult[0]->SERVER_ID;
            $SERVER_URL = $TournamentResult[0]->SERVER_URL;
            $BANNER_REDIRECTION_URL = 'NULL';
            $RELATIVE_URL = 0;
            $EXTERNAL_URL = 0;
        } else {
            $TOURNAMENT_ID=0;
            $TOURNAMENT_NAME = 'NULL';
            $SERVER_ID = 0;
            $SERVER_URL = 'NULL';
            $BANNER_REDIRECTION_URL = $BANNER_REDIRECTION_URL;
            if($request->RELATIVE_URL==1){
                $RELATIVE_URL = $request->RELATIVE_URL;
            }else{
                $RELATIVE_URL=0;
            }
            if($request->EXTERNAL_URL==1){
                $EXTERNAL_URL = $request->EXTERNAL_URL;
            }else{
                $EXTERNAL_URL=0;
            }
            
            
        }

        //Start image upload 
        $file = $request->file('image');
        $allowedFormats = array("Jpeg", "jpeg", "JPEG", "JPG", "jpg", "png", "PNG");
        $filePath = 'banner/images/';
        $hasFile = $request->hasFile('image');
        $result = $this->s3FileUpload($file, $filePath, $allowedFormats, $hasFile);
        if ($result->status == 200) {
            $fileUploadUrl = $result->url;
        } else {
            return redirect()->back()->with('error', $result->message);
        }

        // End image upload
        $insert = [
            'APP_TYPE_ID' => $APP_TYPE_ID,
            'BANNER_CATEGORY_ID' => $BANNER_CATEGORY_ID,
            'BANNER_TYPE_ID' => $BANNER_TYPE_ID,
            'SEGMENT_ID'  => $SEGMENT_ID,
            'BANNER_IMAGE_URL' => $fileUploadUrl,
            'RELATIVE_URL' => $RELATIVE_URL,
            'EXTERNAL_URL' => $EXTERNAL_URL,
            'BANNER_LINK_BUTTON_TEXT' => $BANNER_LINK_BUTTON_TEXT,
            'BANNER_REDIRECTION_URL' => $BANNER_REDIRECTION_URL,
            'TOURNAMENT_ID' => $TOURNAMENT_ID,
            'TOURNAMENT_NAME' => $TOURNAMENT_NAME,
            'SERVER_ID' => $SERVER_ID,
            'SERVER_URL' => $SERVER_URL,
            'STATUS' => $STATUS,
            'START_TIME' => $START_TIME,
            'END_TIME' => $END_TIME,
            'CREATED_DATE' => DB::raw('NOW()')
        ];

        $data['arrs'] = AppBanners::insert($insert);
        // Start User Activity Tracking 
        $activity = [
            'admin' => Auth::user()->id,
            'module_id' => 35,
            'action' => "Add Banner",
            'data' => json_encode($insert)
        ];
        PokerBaazi::storeActivity($activity);
        //End User Activity Tracking
        if ($data['arrs'] == 1) {
            return redirect()->back()->with('success', "Banner added successfully.");
        } else {
            return back()->with('warning', ["title"=>"Sorry","text"=>"Adding banner failed, try again."]);
        }
       
        return back()->with('warning', ["title"=>"Sorry","text"=>"Adding banner failed, try again."]);
    }

    // Mannage Banner Details 
    public function managebanner(Request $request)
    {   
        $countstatus = AppBanners::select('APP_BANNER_ID')
        ->where('STATUS',1)
        ->get(); 
        $countAppBannerStatus= count($countstatus);

        $TotalBannerres = AppBanners::select('APP_BANNER_ID')
        ->get(); 
        $TotalBanner = count($TotalBannerres);

        $perPage = config('poker_config.paginate.per_page');
        $AppBannerResult = AppBanners::from(app(AppBanners::class)->getTable() . " as ab")
            ->select(
                'ab.APP_BANNER_ID',
                'ab.APP_TYPE_ID',
                'ab.BANNER_CATEGORY_ID',
                'ab.BANNER_TYPE_ID',
                'ab.BANNER_IMAGE_URL',
                'ab.BANNER_LINK_BUTTON_TEXT',
                'ab.BANNER_REDIRECTION_URL',
                'ab.TOURNAMENT_ID',
                'ab.TOURNAMENT_NAME',
                'ab.SERVER_ID',
                'ab.SERVER_URL',
                'ab.STATUS',
                'ab.START_TIME',
                'ab.END_TIME',
                'ab.CREATED_DATE',
                'at.APP_TYPE_DESC',
                'bc.BANNER_CATEGORY_DESC',
                'bt.BANNER_TYPE_DESC',
                't.TOURNAMENT_NAME',
                'ab.RELATIVE_URL',
                'ab.EXTERNAL_URL',
                'ab.SEGMENT_ID'
            )
            ->leftjoin('tournament as t', 't.TOURNAMENT_ID', '=', 'ab.TOURNAMENT_ID')
            ->join('app_types as at', 'at.APP_TYPE_ID', '=', 'ab.APP_TYPE_ID')
            ->join('banner_categories as bc', 'bc.BANNER_CATEGORY_ID', '=', 'ab.BANNER_CATEGORY_ID')
            ->join('banner_types as bt', 'bt.BANNER_TYPE_ID', '=', 'ab.BANNER_TYPE_ID')
            ->orderBy('ab.APP_BANNER_ID', 'DESC')
            ->paginate(100);
        $params = $request->all();
      
        return view('bo.views.settings.banner.managebanner', ['AppBannerResult' => $AppBannerResult, 'countAppBannerStatus'=>$countAppBannerStatus, 'TotalBanner'=>$TotalBanner, 'params' => $params]);
    }

    // Edit Aap banner
    public function editappbanner($APP_BANNER_ID)
    {
        $SegmentType = SegmentType::select('SEGMENT_ID', 'SEGMENT_TITLE')
            ->orderBy('SEGMENT_ID', 'DESC')
            ->get();
        $AppTypeResult =   $this->AppTypesModel->AppTypeResult();
        $BannerCategoriesResult =   $this->BannerCategoriesModel->BannerCategoriesResult();
        $BannerTypeResult =   $this->BannerTypesModel->BannerTypeResult();
        $TournamentResult =   $this->tournamentModel->tournamentDetailsIsActiveOne();
        $AppBannerResult = $this->AppBannersModel->appBannerEdit($APP_BANNER_ID);
        return view('bo.views.settings.banner.EditAppBanner', ['AppTypeResult' => $AppTypeResult, 'BannerCategoriesResult' => $BannerCategoriesResult, 'BannerTypeResult' => $BannerTypeResult, 'TournamentResult' => $TournamentResult, 'AppBannerResult' => $AppBannerResult, 'SegmentType' => $SegmentType]);
    }

    // Update App Banner
    public function updateAppBanner($APP_BANNER_ID, Request $request)
    {
        request()->page;
        $request->validate([
            "APP_TYPE_ID" => 'required',
            "BANNER_TYPE_ID"    => 'required',
            "BANNER_CATEGORY_ID" => 'required',
            "BANNER_LINK_BUTTON_TEXT" => 'required',
            "START_TIME" => 'required',
            "END_TIME" => 'required',
        ]);
        $SEGMENT_ID = $request->SEGMENT_ID;
        if ($APP_BANNER_ID != '') {
            $APP_TYPE_ID = $request->APP_TYPE_ID;
            $BANNER_CATEGORY_ID = $request->BANNER_CATEGORY_ID;
            $BANNER_TYPE_ID = $request->BANNER_TYPE_ID;
            $BANNER_LINK_BUTTON_TEXT = $request->BANNER_LINK_BUTTON_TEXT;
            $TOURNAMENT_ID = $request->TOURNAMENT_ID;
            $BANNER_REDIRECTION_URL = $request->BANNER_REDIRECTION_URL;
            $RELATIVE_URL = $request->RELATIVE_URL;
            $EXTERNAL_URL = $request->EXTERNAL_URL;
            $START_TIME = $request->START_TIME;
            $END_TIME = $request->END_TIME;

            if($request->STATUS==1){
                $STATUS = $request->STATUS;
            }else{
                $STATUS=0;
            }
            // Get Tournament Name , Server Ip, Server Name 
            if ($TOURNAMENT_ID != '') {
                $TournamentResult =  $this->tournamentModel->tournamentDetailsWithServerDetails($TOURNAMENT_ID);
                $TOURNAMENT_NAME = $TournamentResult[0]->TOURNAMENT_NAME;
                $SERVER_ID = $TournamentResult[0]->SERVER_ID;
                $SERVER_URL = $TournamentResult[0]->SERVER_URL;
                $BANNER_REDIRECTION_URL = 'NULL';
                $RELATIVE_URL = 0;
                $EXTERNAL_URL = 0;
            } else {
                $TOURNAMENT_ID=0;
                $TOURNAMENT_NAME = 'NULL';
                $SERVER_ID = 0;
                $SERVER_URL = 'NULL';
                $BANNER_REDIRECTION_URL = $request->BANNER_REDIRECTION_URL;

                if($request->RELATIVE_URL==1){
                    $RELATIVE_URL = $request->RELATIVE_URL;
                }else{
                    $RELATIVE_URL=0;
                }
                if($request->EXTERNAL_URL==1){
                    $EXTERNAL_URL = $request->EXTERNAL_URL;
                }else{
                    $EXTERNAL_URL=0;
                }
                
            }

            // Start Image Upload 
            $file = $request->file('image');
            $allowedFormats = array("Jpeg", "jpeg", "JPEG", "JPG", "jpg", "png", "PNG");
            $filePath = 'banner/images/';
            $hasFile = $request->hasFile('image');
            if ($request->hasFile('image')) {
                $result = $this->s3FileUpload($file, $filePath, $allowedFormats, $hasFile);
                if ($result->status == 200) {
                    $fileUploadUrl = $result->url;
                } else {
                    return redirect()->back()->with('error', $result->message);
                }
            } else {
                $fileUploadUrl = $request->IMAGE_LINK_OLD;
            }

            //End image upload
            $update = [
                'APP_TYPE_ID' => $APP_TYPE_ID,
                'BANNER_CATEGORY_ID' => $BANNER_CATEGORY_ID,
                'BANNER_TYPE_ID' => $BANNER_TYPE_ID,
                'SEGMENT_ID' => $SEGMENT_ID,
                'BANNER_IMAGE_URL' => $fileUploadUrl,
                'RELATIVE_URL' => $RELATIVE_URL,
                'EXTERNAL_URL' => $EXTERNAL_URL,
                'BANNER_LINK_BUTTON_TEXT' => $BANNER_LINK_BUTTON_TEXT,
                'BANNER_REDIRECTION_URL' => $BANNER_REDIRECTION_URL,
                'TOURNAMENT_ID' => $TOURNAMENT_ID,
                'TOURNAMENT_NAME' => $TOURNAMENT_NAME,
                'SERVER_ID' => $SERVER_ID,
                'SERVER_URL' => $SERVER_URL,
                'STATUS' => $STATUS,
                'START_TIME' => $START_TIME,
                'END_TIME' => $END_TIME,
                'CREATED_DATE' => DB::raw('NOW()')
            ];
            $data['arrs'] = AppBanners::where('APP_BANNER_ID', $APP_BANNER_ID)
                ->update($update);
            // Start User Activity Tracking 
            $activity = [
                'admin' => Auth::user()->id,
                'module_id' => 35,
                'action' => "Update Banner",
                'data' => json_encode($update)
            ];
            PokerBaazi::storeActivity($activity);
            //End User Activity Tracking
            if ($data['arrs'] == 1) {
                
               return redirect()->back()->with('success', "Banner updated successfully");
            } else {
                return back()->with('warning', ["title"=>"Sorry","text"=>"Banner update failed, try again."]);
            }
            return back()->with('warning', ["title"=>"Sorry","text"=>"Banner update failed, try again."]);
        }
    }

    // Banner Status Change 
    public function bannerChangeStatus(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'STATUS' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->all()]);
        }
        $statusChange = AppBanners::find($request->id);
        $status = $statusChange->STATUS = ($request->STATUS) ? 1 : 0;
        $statusChange->save();

        $activity = [
            'admin' => Auth::user()->id,
            'module_id' => 35,
            'action' => "Banner Status Change",
            'data' => json_encode($status)
        ];
        PokerBaazi::storeActivity($activity);

        Session::flash('message', 'Banner is ' . (($request->STATUS) ? "enabled" : "disabled") . ' successfully');
        return response()->json(['status' => true, 'message' => 'Banner is ' . (($request->STATUS) ? "<b><i>active</i></b>" : "<b><i>in-active</i></b>")]);
    }

    public static function getsegmentType($SEGMENT_ID)
    {
        $results = SegmentType::select('SEGMENT_ID', 'SEGMENT_TITLE')
            ->where('SEGMENT_ID', $SEGMENT_ID)
            ->orderBy('SEGMENT_ID', 'DESC')
            ->first();
        return $results;
    }
}
