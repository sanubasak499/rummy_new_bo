<?php

namespace App\Http\Controllers\bo\settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\bo\BaseController;
use App\Models\Menu;
use App\Models\Module;
use Auth;
use DB;

class ModuleController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['module_menu'] = Menu::select('id', 'display_name')
                                ->whereHas('module')
                                ->with('childrenWithModule')
                                ->with('module')
                                ->orderBy('menu_order')
                                ->whereNull('parent_id')
                                ->get();
        $data['module_none_menu'] = Module::whereNull('menu_id')->get();
        
        return view('bo.views.settings.module.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id=null)
    {
        if(!empty($id)){
            if(empty($module = Module::find($id))){
                return redirect()->back()->with('error', "Something went wrong");
            }
        }else{
            $module = new Module;
        }
        // if($first = Module::select('id')->where('module_key', $request->module_key)->first()){
        //     if($first->id != $module->id){
        //         return redirect()->back()->with('error', "Module Key already exist");
        //     }
        // }

        $module->module_key = $request->module_key;
        $module->display_name = $request->display_name;
        $module->description = $request->description;
        $module->menu_id = $request->menu_id;
        
        if($module->save()){
            return redirect()->back()->with('success', "Module Add/Update Succesfully");
        }else{
            return redirect()->back()->with('error', "Something went wrong");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Module  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }
}
