<?php

namespace App\Http\Controllers\bo\settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\bo\BaseController;
use App\Models\Menu;
use App\Models\Module;
use Auth;
use DB;
use Illuminate\Support\Str;

class MenuController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['menus'] = Menu::orderBy('menu_order')->whereNull('parent_id')->get();
        return view('bo.views.settings.menu.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id=null)
    {
        if(!empty($id)){
            if(empty($menu = Menu::find($id))){
                return redirect()->back()->with('error', "Something went wrong");
            }
        }else{
            $menu = new Menu;
        }
        $menu->display_name = $request->display_name;
        $menu->description = $request->description;
        $menu->parent_id = $request->parent_id;
        $menu->target = $request->target;
        $menu->icon_class = $request->icon_class;
        $menu->url = $request->url;
        $menu->route = $request->route;
        $menu->parameters = $request->parameters;
        $menu->absolute_url = $request->absolute_url;
        $menu->status = $request->status;
        
        if($menu->save()){
            if($module = $menu->module){
                $module->display_name = $menu->display_name;
            }else{
                $module = new Module;
                $module->menu_id = $menu->id;
                $module->module_key = Str::snake($menu->display_name);
                $module->display_name = $menu->display_name;
                $module->description = $menu->description;
            }
            $module->save();
            return redirect()->back()->with('success', "Menu Add/Update Succesfully");
        }else{
            return redirect()->back()->with('error', "Something went wrong");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    public function order(Request $request){
        try {
            if(!empty($order = $request->order)){
                $order = collect(json_decode(base64_decode($order)));
                // dd($order);
                $orderId = [];
                $i=1;
                $query = "UPDATE bo_admin_menus SET ";
                $query_menu_order = " menu_order = CASE ";
                $query_parent_id = ", parent_id = CASE ";
        
                $result = $this->RecursiveFunctionMenuOrder($order, $i);
                
                $query_menu_order .= $result['query_menu_order'];
                $query_parent_id .= $result['query_parent_id'];
                $orderId = array_merge($orderId, $result['orderId']);

                $query_menu_order .= " ELSE menu_order ";
                $query_menu_order .= " END ";

                $query_parent_id .= " ELSE parent_id ";
                $query_parent_id .= " END ";

                $query_parent_id = trim($result['query_parent_id']) != "" ? $query_parent_id : ""; 
                $query = "$query $query_menu_order $query_parent_id WHERE id IN (".implode(',', $orderId).")";
                
                $result = DB::select($query);
                
                if($request->ajax()){
                    return response()->json(['status'=>200, 'message'=>'Order Changed Successfully']);
                }else{
                    return redirect()->back()->with('success', "Order changed Successfully");
                }
            }else{
                if($request->ajax()){
                    return response()->json(['status'=>400, 'message'=>'params missing']);
                }else{
                    return redirect()->back()->with('error', "Something went Wrong");
                }
            }
        }catch(\Exception $e){
            if($request->ajax()){
                return response()->json(['status'=>'500', 'message'=>$e->getMessage()]);
            }else{
                return redirect()->back()->with('error', "Something Went wrong");
            }
        }
    }

    public function RecursiveFunctionMenuOrder($order, $i, $depth=null) {
        $query_menu_order = "";
        $query_parent_id = "";
        $orderId = [];
        foreach($order as $key => $parent){
            $query_menu_order .= " WHEN id=$parent->id THEN $i ";
            $i++;
            $orderId[] = $parent->id;
            if($depth){
                if($depth->id != $parent->parentId){
                    $query_parent_id .= " WHEN id=$parent->id THEN $depth->id";
                }
            }else{
                if(!empty($parent->parentId)){
                    $query_parent_id .= " WHEN id=$parent->id THEN null";
                }
            }
            if(property_exists($parent, 'children')){
                $result =  $this->RecursiveFunctionMenuOrder($parent->children, $i, $parent);
                $query_menu_order .= $result['query_menu_order'];
                $query_parent_id .= $result['query_parent_id'];
                $orderId = array_merge($orderId, $result['orderId']);
            }
        }
        $data['query_menu_order']= $query_menu_order;
        $data['query_parent_id']= $query_parent_id;
        $data['orderId']= $orderId;
        return $data;
    }
}
