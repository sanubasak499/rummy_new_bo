<?php

namespace App\Http\Controllers\bo\affiliate;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PartnersTransactionDetails;
use App\Models\Partner;
use Carbon\Carbon;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Author: Nitesh Kumar Jha
 * Purpose: Get all the Affiliate Tax Summary
 * Created Date: 13-2-2020
 */

class affiliateBaaziTdsSearchController extends Controller
{

    public function index(){
        $PartnerResult = Partner::select('PARTNER_ID','PARTNER_NAME','PARTNER_USERNAME')
        ->where('PARTNER_STATUS',1)
        ->get();

          return view('bo.views.affiliate.affiliateBaaziTdsSearch',['PartnerResult'=>$PartnerResult ]);
        }
    // Search New Baazi tax report deatils 
    public function affiliateTdsSearch(Request $request){
        $perPage = config('poker_config.paginate.per_page');
        if (!$request->isMethod('post')) {
            return view('bo.views.affiliate.affiliateBaaziTdsSearch');
        }
        $page = request()->page;

       if($request->PARTNER_ID!=''){
        $PARTNER_ID =$request->PARTNER_ID;
        }else{
         $PARTNER_ID = array(); 
        }    

        if($PARTNER_ID !=''){
            $request->session()->put('transactionType', $PARTNER_ID);
        }else{
            $request->session()->forget('transactionType', $PARTNER_ID);
        } 
     
        if($request->transaction_type!=''){
            $transactionType   = $request->transaction_type;
        }else{
            $transactionType = array(); 
        } 
        if($transactionType !=''){
            $request->session()->put('transactionType', $transactionType);
        }else{
            $request->session()->forget('transactionType', $transactionType);
        } 

        $PartnerResult = Partner::select('PARTNER_ID','PARTNER_NAME','PARTNER_USERNAME')
        ->where('PARTNER_STATUS',1)
        ->get(); 
     
        $transactionData =$this->getAllResult($request);
        $TDSAmount = $transactionData->sum('AMOUNT'); 
        $totalAmount = $transactionData->sum('PAYMENT_TRANSACTION_AMOUNT'); 
        $transactionData=$transactionData ->paginate($perPage,['*'],'page',$page);
        $params = $request->all();
        $params['page'] = $page;
        return view('bo.views.affiliate.affiliateBaaziTdsSearch',['allTransactionSearchData'=>$transactionData,'totalAmount'=>$totalAmount,'TDSAmount'=>$TDSAmount,'params'=>$params,'PartnerResult'=>$PartnerResult]);
    }

    public function getAllResult($request){

        $transactionData = PartnersTransactionDetails::from(app(PartnersTransactionDetails::class)->getTable()." as ptd")
        ->select('ptd.AMOUNT','ptd.CREATED_TIMESTAMP','ptd.TRANSACTION_STATUS_ID','ppt.PAYMENT_TRANSACTION_STATUS','ptd.USER_ID','ptd.INTERNAL_REFERENCE_NO','p.PARTNER_NAME','u.USERNAME','u.FIRSTNAME','u.LASTNAME','u.EMAIL_ID','u.CONTACT','ukyc.PAN_NUMBER','ppt.PAYMENT_TRANSACTION_AMOUNT')
        ->join('partner as p', 'p.PARTNER_ID' ,'=', 'ptd.PARTNER_ID')
        ->join('partner_payment_transaction as ppt', 'ppt.INTERNAL_REFERENCE_NO' ,'=', 'ptd.INTERNAL_REFERENCE_NO')
        ->join('user as u', 'u.USER_ID' ,'=', 'ptd.USER_ID')
        ->join('USER_KYC as ukyc', 'ukyc.USER_ID' ,'=', 'ptd.USER_ID')

       ->when($request->search_by == "USERNAME", function($query) use ($request){
           return $query->where('u.USERNAME', 'LIKE', "%{$request->search_by_value}%");
       })
       ->when($request->search_by == "EMAIL_ID",function($query) use ($request){
           return $query->where('u.EMAIL_ID', 'LIKE', "%{$request->search_by_value}%");
       })
       ->when($request->search_by == "CONTACT", function($query) use ($request){
           return $query->where('u.CONTACT', 'LIKE', "%{$request->search_by_value}%");
       })
       
       ->when(!empty($request->PAN_NUMBER), function($query) use ($request){
           return $query->where('ukyc.PAN_NUMBER',$request->PAN_NUMBER);
       })

        ->when(!empty($request->INTERNAL_REFERENCE_NO), function($query) use ($request){
            return $query->where('ptd.INTERNAL_REFERENCE_NO',$request->INTERNAL_REFERENCE_NO);
        })
     
       ->when(!empty($request->PARTNER_ID), function($query) use ($request){
        return $query->whereIn('p.PARTNER_ID',$request->PARTNER_ID);
       })
       

       ->when(!empty($request->transaction_type), function($query) use ($request){
        return $query->whereIn('ppt.PAYMENT_TRANSACTION_STATUS',$request->transaction_type);
       })


       ->when(!empty($request->START_DATE_TIME), function($query) use ($request){
           $dateFrom = Carbon::parse($request->START_DATE_TIME)->format('Y-m-d H:i:s');
           return $query->whereDate('ptd.CREATED_TIMESTAMP', ">=", $dateFrom);
       })
       ->when(!empty($request->END_DATE_TIME), function($query) use ($request){
           $dateTo = Carbon::parse($request->END_DATE_TIME)->format('Y-m-d H:i:s');
           return $query->whereDate('ptd.CREATED_TIMESTAMP', "<=", $dateTo);
       })
       ->where('ptd.TRANSACTION_TYPE_ID',109)
       ->orderBy('ptd.CREATED_TIMESTAMP','DESC');
       return $transactionData;
    }
  
    // Export To Excel 
    public function exportExcel(Request $request){
        $request->all();
        $transactionType = session()->get('transactionType');
        $PARTNER_ID = session()->get('PARTNER_ID');
        $transactionData = PartnersTransactionDetails::from(app(PartnersTransactionDetails::class)->getTable()." as ptd")
        ->select('ptd.AMOUNT','ptd.CREATED_TIMESTAMP','ptd.TRANSACTION_STATUS_ID','ppt.PAYMENT_TRANSACTION_STATUS','ptd.USER_ID','ptd.INTERNAL_REFERENCE_NO','p.PARTNER_NAME','u.FIRSTNAME','u.LASTNAME','u.USERNAME','u.EMAIL_ID','u.CONTACT','ukyc.PAN_NUMBER','ppt.PAYMENT_TRANSACTION_AMOUNT')
        ->join('partner as p', 'p.PARTNER_ID' ,'=', 'ptd.PARTNER_ID')
        ->join('partner_payment_transaction as ppt', 'ppt.INTERNAL_REFERENCE_NO' ,'=', 'ptd.INTERNAL_REFERENCE_NO')
        ->join('user as u', 'u.USER_ID' ,'=', 'ptd.USER_ID')
        ->join('USER_KYC as ukyc', 'ukyc.USER_ID' ,'=', 'ptd.USER_ID')

       ->when($request->search_by == "USERNAME", function($query) use ($request){
           return $query->where('u.USERNAME', 'LIKE', "%{$request->search_by_value}%");
       })
       ->when($request->search_by == "EMAIL_ID",function($query) use ($request){
           return $query->where('u.EMAIL_ID', 'LIKE', "%{$request->search_by_value}%");
       })
       ->when($request->search_by == "CONTACT", function($query) use ($request){
           return $query->where('u.CONTACT', 'LIKE', "%{$request->search_by_value}%");
       })
       
       ->when(!empty($request->PAN_NUMBER), function($query) use ($request){
           return $query->where('ukyc.PAN_NUMBER',$request->PAN_NUMBER);
       })

       ->when(!empty($request->INTERNAL_REFERENCE_NO), function($query) use ($request){
        return $query->where('ptd.INTERNAL_REFERENCE_NO',$request->INTERNAL_REFERENCE_NO);
       })
       
       ->when(!empty($PARTNER_ID), function($query) use ($PARTNER_ID){
        return $query->whereIn('p.PARTNER_ID',$PARTNER_ID);
       })

       ->when(!empty($transactionType), function($query) use ($transactionType){
        return $query->whereIn('ppt.PAYMENT_TRANSACTION_STATUS',$transactionType);
       })
       ->when(!empty($request->dateFrom), function($query) use ($request){
           $dateFrom = Carbon::parse($request->dateFrom)->format('Y-m-d H:i:s');
           return $query->whereDate('ptd.CREATED_TIMESTAMP', ">=", $dateFrom);
       })
       ->when(!empty($request->dateTo), function($query) use ($request){
           $dateTo = Carbon::parse($request->dateTo)->format('Y-m-d H:i:s');
           return $query->whereDate('ptd.CREATED_TIMESTAMP', "<=", $dateTo);
       })
       ->where('ptd.TRANSACTION_TYPE_ID',109)
       ->orderBy('ptd.CREATED_TIMESTAMP','DESC')
       ->get();
        $i=0;
        foreach($transactionData as $value) {
            $newArray[$i]['USERNAME']	 		           =  $value->USERNAME;
            $newArray[$i]['PARTNER_NAME']	 		           =  $value->PARTNER_NAME;
            $newArray[$i]['INTERNAL_REFERENCE_NO']           =  $value->INTERNAL_REFERENCE_NO;
            $newArray[$i]['NAME'] 	   = $value->FIRSTNAME .' '. $value->LASTNAME;
            $newArray[$i]['EMAIL_ID']			  	   =  $value->EMAIL_ID;
            $newArray[$i]['CONTACT']                   =  $value->CONTACT;
            $newArray[$i]['PAN_NUMBER'] 				   =  $value->PAN_NUMBER;
            $newArray[$i]['PAYMENT_TRANSACTION_AMOUNT']				   =  $value->PAYMENT_TRANSACTION_AMOUNT;
            $newArray[$i]['AMOUNT']				   =  $value->AMOUNT;
            $newArray[$i]['CREATED_TIMESTAMP']				   =  $value->CREATED_TIMESTAMP;
                if($value->PAYMENT_TRANSACTION_STATUS=='231') {
                $newArray[$i]['PAYMENT_TRANSACTION_STATUS'] = 'Pending';
            } else if($value->PAYMENT_TRANSACTION_STATUS=='233') {
                $newArray[$i]['PAYMENT_TRANSACTION_STATUS'] = 'Approved';
            } else if($value->PAYMENT_TRANSACTION_STATUS=='232') {
                $newArray[$i]['PAYMENT_TRANSACTION_STATUS'] = 'Rejected';	
            }
            $i++;	
        }
        
    $data[0]=array("USERNAME"=>"","PARTNER_NAME"=>"","INTERNAL_REFERENCE_NO"=>"","NAME"=>"","EMAIL_ID"=>"","CONTACT"=>"","PAN_NUMBER"=>"","WITHDRAW_AMOUNT"=>"","TDS_AMOUNT"=>"","Date_And_Time"=>"","STATUS"=>"");
    $name = "Affiliate-Tax-Summary";
    $fileName = $name.".xlsx";
    $excelData = $newArray;
    $headings = array_keys($data[0]);	
    return Excel::download(new CollectionExport($excelData,$headings), $fileName);
    }  
}		