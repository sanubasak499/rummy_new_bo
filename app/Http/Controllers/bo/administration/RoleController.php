<?php

namespace App\Http\Controllers\bo\administration;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use App\Http\Controllers\bo\BaseController;
use App\Models\Role;

class RoleController extends BaseController
{
    public function __construct(){
        parent::__construct();
        
    }

    public function index(Request $request){
        $data['roles'] = Role::get();
        return view('bo.views.administration.role.index', $data);
    }

    public function store(Request $request){
        $role = new Role($request->all());
        $role->name = \ucfirst($request->name);
        if($role->save()){
            return redirect()->back()->with('success', "Update Successfully"); 
        }else{
            return redirect('/')->with('error', "Something went Wrong");
        }
    }

    public function edit(Request $request, $id){
        try{
            $id = decrypt($id);
            $role = Role::find($id);
            
            if($role){    
                return response()->json(['status'=>200, 'message'=>'success', 'data'=>$role], 200);
            }else{
                return response()->json(['status'=>201, 'message'=>'Role not found'], 201);
            }
        }catch(DecryptException $e){
            return response()->json(['status'=>500, 'message'=>$e->getMessage()], 500);
        }catch(\Exception $e){
            return response()->json(['status'=>500, 'message'=>$e->getMessage()], 500);
        }
    }
    public function update(Request $request, $id){
        try{
            $id = decrypt($id);
            $role = Role::find($id);
            $role->name = \ucfirst($request->name);
            $role->description=$request->description;
            if($role->save()){    
                return redirect()->back()->with('success', "Update Successfully");  
            }else{
                return redirect()->back()->with('error', "Something went Wrong");
            }
        }catch(DecryptException $e){
            return redirect('/')->with('error', "Something went Wrong");
        }catch(\Exception $e){
            return redirect('/')->with('error', $e->getMessage());
        }
    }
}
