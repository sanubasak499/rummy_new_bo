<?php

namespace App\Http\Controllers\bo\administration;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use App\Http\Controllers\bo\BaseController;
use App\Models\Admin;
use App\Models\Menu;
use App\Models\Role;
use App\Models\Module;
use App\Models\RolePermission;
use App\Models\CustomAdminPermission;
use Carbon\Carbon;
use Auth;
use DB;

class UserController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->roles = Role::get();
    }

    public function index(Request $request)
    {
        // $partner = Auth::user()->load('partner:PARTNER_ID,FK_PARTNER_TYPE_ID');        

        $perPage = config('poker_config.paginate.per_page');
        $data['roles'] = Role::get();

        if (!$request->isMethod('post')) {
            return view('bo.views.administration.user.index', $data);
        }
        $page = request()->page;

        $queryObj = Admin::query();

        $queryObj->when($request->search_by == "username", function ($query) use ($request) {
            return $query->where('username', 'LIKE', "%{$request->search_by_value}%");
        })
            ->when($request->search_by == "email", function ($query) use ($request) {
                return $query->where('email', 'LIKE', "%{$request->search_by_value}%");
            })
            ->when($request->search_by == "contact_no", function ($query) use ($request) {
                return $query->where('mobile', 'LIKE', "%{$request->search_by_value}%");
            })
            ->when(!empty($request->date_from), function ($query) use ($request) {
                $date_from = Carbon::parse($request->date_from);
                return $query->whereDate('created_at', ">=", $date_from);
            })
            ->when(!empty($request->date_to), function ($query) use ($request) {
                $date_to = Carbon::parse($request->date_to);
                return $query->whereDate('created_at', "<=", $date_to);
            })
            ->when(!empty($request->status), function ($query) use ($request) {
                return $query->where('account_status', $request->status);
            })
            ->when(!empty($request->role), function ($query) use ($request) {
                return $query->where('role_id', $request->role);
            });

        $queryStatsObj = clone $queryObj;
        
        $users = $queryObj
            ->select('id', 'role_id', 'firstname', 'lastname', 'username', 'email', 'fk_partner_id', 'mobile', 'account_status', 'created_at')
            ->orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page);

        $stats = DB::select(
            $queryStatsObj->select(
                DB::raw("ifnull(account_status,'total') as `status`"),
                DB::raw("count(account_status) as `total`")
            )
                ->groupBy(DB::raw('account_status WITH ROLLUP'))->toSql(),
            $queryStatsObj->getBindings()
        );
        $stats = collect($stats)->keyBy('status');
        
        $params = $request->all();
        $params['page'] = $page;

        $data['params'] = $params;
        $data['users'] = $users;
        $data['stats'] = $stats;
        return view('bo.views.administration.user.index', $data);
    }

    public function create()
    {
        $data['roles'] = $this->roles;
        return view('bo.views.administration.user.edit-add', $data);
    }

    public function store(Request $request)
    {
        $params = $request->all();
        $params['password'] = bcrypt($params['password']);
        $params['transcation_password'] = bcrypt($params['transcation_password']);
        $params['fk_partner_id'] = 10001;
        $params['created_by'] = Auth::user()->id;

        if ($admin = Admin::create($params)) {
            return redirect()->back()->with('success', "Admin User Addeded Successfully");
        }
    }

    public function show($id)
    {
        try {
            $id = decrypt($id);
            dd($id);
        } catch (DecryptException $e) {
            return redirect('/')->with('error', "Something went Wrong");
        }
    }

    public function edit($id)
    {
        try {
            $id = decrypt($id);
            $data['roles'] = $this->roles;
            $data['user'] = Admin::find($id);
            return view('bo.views.administration.user.edit-add', $data);
        } catch (DecryptException $e) {
            return redirect('/')->with('error', "Something went Wrong");
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $id = decrypt($id);
            $data['roles'] = $this->roles;

            $user = Admin::find($id);
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->role_id = $request->role_id;
            if (!empty($request->password)) {
                $user->password = bcrypt($request->password);
            }
            if (!empty($request->transcation_password)) {
                $user->transcation_password = bcrypt($request->transcation_password);
            }
            if ($user->save()) {
                $data['user'] = $user;
                return redirect()->route('administration.user.edit', encrypt($user->id))->with('success', "Update Successfully");
            } else {
                return redirect()->back()->with('error', "Something went Wrong");
            }
        } catch (DecryptException $e) {
            return redirect('/')->with('error', "Something went Wrong");
        } catch (\Exception $e) {
            return redirect('/')->with('error', $e->getMessage());
        }
    }

    public function rolePermissions(Request $request, $id)
    {
        try {
            $id = decrypt($id);
            $data['user'] = $user = Admin::find($id);
            $role = $user->role;

            $data['role_id'] = $role_id = $role->id;

            $data['menus'] = Menu::with('children')
                ->with('module')
                ->whereNull('parent_id')->get();

            $data['custom_modules'] = Module::whereNull('menu_id')->get();
            $data['permissions'] = RolePermission::where('role_id', $role_id)->get();
            $data['custom_permissions'] = CustomAdminPermission::where('admin_id', $user->id)->get();

            return view('bo.views.administration.user.user-roles', $data);
        } catch (DecryptException $e) {
            return redirect('/')->with('error', "Something went Wrong");
        } catch (\Exception $e) {
            return redirect('/')->with('error', $e->getMessage());
        }
    }

    public function rolePermissionsUpdate(Request $request, $id)
    {
        try {
            $id = decrypt($id);
            $data['user'] = $user = Admin::find($id);
            $role = $user->role;
            $role_id = $role->id;

            $modules = $request->modules;
            $modulesCollectionRequest = collect($modules)->keys();

            $rolePermissions = RolePermission::where('role_id', $role_id)->get();
            $custom_permissions = $user->custom_permissions;
            $rolePermissionsModuleId =  $rolePermissions->keyBy('module_id')->keys();
            $custom_permissions_module_id =  $custom_permissions->keyBy('module_id')->keys();

            $removeRolePermissionModuleId = $rolePermissionsModuleId->diff($modulesCollectionRequest)->values();
            $insertRolePermissionModuleId = $modulesCollectionRequest->diff($rolePermissionsModuleId)->values();
            $commonPermissionsModuleId = $rolePermissionsModuleId->intersect($modulesCollectionRequest)->values();

            $commonRolePermissionInsert = [];
            foreach ($commonPermissionsModuleId as $key => $module_id) {
                $rolePermission = $rolePermissions->where('module_id', $module_id)->first();
                $edit = $modules[$module_id]['edit'] ?? 0;
                $view = $modules[$module_id]['view'] ?? 0;
                if ($rolePermission->edit != $edit || $rolePermission->view != $view) {
                    $insertCommon = [];
                    $insertCommon['module_id'] = $module_id;
                    $insertCommon['admin_id'] = $user->id;
                    $insertCommon['edit'] = $edit;
                    $insertCommon['view'] = $view;
                    $commonRolePermissionInsert[] = $insertCommon;
                }
            }
            $removeRolePermissionInsert = [];
            foreach ($removeRolePermissionModuleId as $key => $module_id) {
                $insertCommon = [];
                $insertCommon['module_id'] = $module_id;
                $insertCommon['admin_id'] = $user->id;
                $insertCommon['edit'] = 0;
                $insertCommon['view'] = 0;
                $removeRolePermissionInsert[] = $insertCommon;
            }

            $insertRolePermissionInsert = [];
            foreach ($insertRolePermissionModuleId as $key => $module_id) {
                $insertCommon = [];
                $insertCommon['module_id'] = $module_id;
                $insertCommon['admin_id'] = $user->id;
                $insertCommon['edit'] = $modules[$module_id]['edit'] ?? 0;
                $insertCommon['view'] = $modules[$module_id]['view'] ?? 0;
                $insertRolePermissionInsert[] = $insertCommon;
            }

            $finalPermissionForCustom = array_merge($commonRolePermissionInsert, $removeRolePermissionInsert, $insertRolePermissionInsert);

            \DB::transaction(function () use ($user, $finalPermissionForCustom) {
                CustomAdminPermission::where('admin_id', $user->id)->delete();
                CustomAdminPermission::insert($finalPermissionForCustom);
            }, 5);

            return redirect()->back()->with('success', "Permissions Updated Successfully");
        } catch (DecryptException $e) {
            return redirect('/')->with('error', "Something went Wrong");
        }
    }
}
