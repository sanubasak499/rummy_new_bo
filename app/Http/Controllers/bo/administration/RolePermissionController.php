<?php

namespace App\Http\Controllers\bo\administration;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use App\Http\Controllers\bo\BaseController;
use App\Models\Menu;
use App\Models\Module;
use App\Models\Role;
use App\Models\RolePermission;
// use App\PokerBaazi\Traits\RolePermission as RolePermissionTraits;

class RolePermissionController extends BaseController
{
    // use RolePermissionTraits;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request, $role_id)
    {
        try {
            $data['role_id'] = $role_id = decrypt($role_id);
            $data['menus'] = Menu::with('children')
                ->with('module')
                ->whereNull('parent_id')->get();

            $data['role'] = Role::select('name')->find($role_id);
            $data['custom_modules'] = Module::whereNull('menu_id')->get();
            $data['permissions'] = RolePermission::where('role_id', $role_id)->get();

            return view('bo.views.administration.permission.index', $data);
        } catch (DecryptException $e) {
            return redirect('/')->with('error', "Something went Wrong");
        }
    }

    public function update(Request $request, $role_id)
    {
        try {
            $data['role_id'] = $role_id = decrypt($role_id);
            $existingPermissions = RolePermission::select('id', 'module_id')->where('role_id', $role_id)->get();
            $existingModulesIds = $existingPermissions->pluck('module_id');

            $modules = $request->modules;
            $modulesCollectionRequest = collect($modules)->keys();
            $toRemovePermissionsIds = $existingModulesIds->diff($modulesCollectionRequest)->values();
            $updatePermissionsIds = $existingModulesIds->intersect($modulesCollectionRequest);
            $insertPermissionsIds = $modulesCollectionRequest->diff($updatePermissionsIds)->values();

            $query = '';
            if (count($updatePermissionsIds) > 0) {
                // building update Permissions Query 
                $query = "UPDATE bo_admin_roles_permissions SET ";
                $module_view_permission = " view = CASE ";
                $module_edit_permission = ", edit = CASE ";

                foreach ($updatePermissionsIds as $key => $update) {
                    $view = $modules[$update]['view'] ?? 0;
                    $edit = $modules[$update]['edit'] ?? 0;
                    $module_view_permission .= " WHEN module_id=$update THEN $view ";
                    $module_edit_permission .= " WHEN module_id=$update THEN $edit ";
                }

                $module_view_permission .= " ELSE view ";
                $module_view_permission .= " END ";

                $module_edit_permission .= " ELSE edit ";
                $module_edit_permission .= " END ";

                $query = "$query $module_view_permission $module_edit_permission WHERE role_id=$role_id AND module_id IN (" . implode(',', $updatePermissionsIds->toArray()) . ")";
                // End Update Permissions query 
            }

            // building Insert New permmission Query 
            $insertData = [];
            $currentDate = date('Y-m-d H:i:s');
            foreach ($insertPermissionsIds as $key => $module_id) {
                $rolePermission = new RolePermission();
                $newRole['role_id'] = $role_id;
                $newRole['module_id'] = $module_id;
                $newRole['view'] = $modules[$module_id]['view'] ?? 0;
                $newRole['edit'] = $modules[$module_id]['edit'] ?? 0;
                $newRole['created_at'] = $currentDate;
                $newRole['updated_at'] = $currentDate;
                $insertData[] = $newRole;
            }

            \DB::transaction(function () use ($role_id, $query, $insertData, $toRemovePermissionsIds, $updatePermissionsIds) {
                if (count($updatePermissionsIds) > 0) {
                    \DB::select($query);
                }
                RolePermission::insert($insertData);
                // delete permissions
                RolePermission::where('role_id', $role_id)->whereIn('module_id', $toRemovePermissionsIds)->delete();
            }, 5);

            return redirect()->back()->with('success', "Permissions Updated Successfully");
        } catch (DecryptException $e) {
            return redirect('/')->with('error', "Something went Wrong");
        } catch (\Exception $e) {
            return redirect('/')->with('error', "Something went Wrong");
        }
    }
}
