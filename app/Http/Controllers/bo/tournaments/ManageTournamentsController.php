<?php

namespace App\Http\Controllers\bo\tournaments;

use App\Exports\CollectionExport;
use Illuminate\Http\Request;
use App\Http\Requests\bo\tournament\CreateTournamentValidation;
use App\Http\Requests\bo\tournament\EditTournamentValidation;
use App\Models\Tournament;
use Carbon\Carbon;
use App\Models\TournamentWinnerLevel;
use App\Models\Template;
use App\Models\TournamentLevel;
use DB;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Manage Tournaments 
 * 
 * In this class file wee will work for the manage Tournaments,
 * @example Create, Edit, Delate and View Tournaments.
 * 
 * Class ManageTournamentsController
 * 
 * @category    Manage Tournament
 * @package     Tournament
 * @copyright   PokerBaazi
 * @author      Nitin Sharma<nitin.sharma@moonshinetechnology.com>
 * Created At:  03/02/2020
 */

class ManageTournamentsController extends TournamentBaseController
{

    protected $queryObj;

    const FREE_ROLL = 99;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $data['gameTypes'] = $this->getTournamentGameTypes();
        $data['currencyTypes'] = $this->getAllCurrencyTypes();

        $perPage = config('poker_config.paginate.per_page');

        if (!$request->isMethod('post')) {
            return view('bo.views.tournaments.manage-tourneys.index', $data);
        }

        $conditionObj = $this->filterTournamentConditionalQuery($request->all(), ['select' => true, 'tournamentRegistrationsCount' => true]);
        // $statsQueryObj = clone $conditionObj;

        $data['tournaments'] = $conditionObj
            ->orderBy('TOURNAMENT_START_TIME', 'desc')
            ->paginate($perPage);
        // ->dd();

        // $data['count'] = $statsQueryObj->select(DB::raw("count(PUSH_NOTIFICATION_ID) as `total`"))->pluck('total')->first() ?? 0;

        $data['params'] = $request->all();
        $data['params'] = (object) $data['params'];

        return view('bo.views.tournaments.manage-tourneys.index', $data);
    }


    public function create($template_id = null)
    {
        if (!empty($template_id)) {
            $template_id = decrypt($template_id);
            $tournament = Template::findOrFail($template_id);
            $data['tournament'] = json_decode($tournament->data);
        }
        $data = $data ?? [];
        $data = $data + $this->createEditTournamentData($data);

        return view('bo.views.tournaments.manage-tourneys.create', $data);
    }

    /**
     * use App\Http\Requests\bo\tournament\CreateTournamentValidation
     * CreateTournamentValidation class is used for validation,
     * after validating come into the function 
     */
    public function store(CreateTournamentValidation $request)
    {
        $data = $request->all();

        /**
         * extract game constant array to contants
         */
        $gameConstants = config('constant.game');
        \extract($gameConstants);

        /**
         * all this given values taking from
         * constant file game section
         */
        $data['IS_REGISTER_GAME'] = $IS_REGISTER_GAME;
        $data['TOURNAMENT_BREAK_TIME'] = $TOURNAMENT_BREAK_TIME;
        $data['BREAK_INTERVAL_TIME'] = $BREAK_INTERVAL_TIME;
        $data['PAID_OPTION'] = $PAID_OPTION;
        $data['ALLOW_OFFLINE_PLAYERS'] = $ALLOW_OFFLINE_PLAYERS;
        $data['TOURNAMENT_STATUS'] = $TOURNAMENT_STATUS;
        $data['LOBBY_ID'] = $TOURNAMENT_LOBBY_ID;
        $data['MINIGAMES_ID'] = $MINIGAMES_ID;
        $data['PARTNER_ID'] = $ADMIN_USER_ID;
        $data['SITOUT_TIME'] = $SITEOUT_TIME;
        $data['TIME_BANK_INTERVAL_TIME'] = $TIME_BANK_INTERVAL_TIME;
        // end constants

        /**
         * static values defined
         */
        $data['LOBBY_MENU_ID']  =   0;
        $data['ADDON_COUNT']    =   1;
        // end static values

        /**
         * extract all the request parameter to variables
         */
        extract($data);

        /**
         * To Save Template
         * Tournament Name treat as Template Name
         */
        if (!empty($saveButton) && $saveButton == "saveTemplate") {
            if (!Template::where('template_name', $TOURNAMENT_NAME)->where('template_type_id', 1)->first()) {
                $tournament = new Template();
                $tournament->template_type_id = 1;
                $tournament->template_name = $TOURNAMENT_NAME;

                $data['IS_ACTIVE'] = $data['IS_ACTIVE'] ?? "0";
                $data['LATE_REGISTRATION_ALLOW'] = $data['LATE_REGISTRATION_ALLOW'] ?? "0";
                $data['DEPOSIT_BALANCE_ALLOW'] = $data['DEPOSIT_BALANCE_ALLOW'] ?? "0";
                $data['PROMO_BALANCE_ALLOW'] = $data['PROMO_BALANCE_ALLOW'] ?? "0";
                $data['WIN_BALANCE_ALLOW'] = $data['WIN_BALANCE_ALLOW'] ?? "0";
                $data['DOUBLE_REBUYIN'] = $data['DOUBLE_REBUYIN'] ?? "0";

                $tournament->data = json_encode($data);
                if ($tournament->save()) {
                    return redirect()->route('tournaments.manage-tourneys.create')->with('success', "Template Saved Successfully");
                } else {
                    return back()->with('error', "Something went wrong");
                }
            } else {
                return back()->with('error', "Same template name already exist");
            }
        }

        /**
         * Tournament Information Section
         */
        $data['FK_PARENT_TOURNAMENT_ID'] = '';
        $data['TIMER_TOURNAMENT'] = '';
        $data['TIMER_TOURNAMENT_END_TIME'] = '';

        if ($TOURNAMENT_SUB_TYPE_ID == 1) {
            $data['FK_PARENT_TOURNAMENT_ID'] = '';
            $data['TIMER_TOURNAMENT'] = 0;
        } else if ($TOURNAMENT_SUB_TYPE_ID == 2) {
            $data['FK_PARENT_TOURNAMENT_ID'] = $TOURNAMENT_PARENT_SATELLITE_ID;
            $data['TIMER_TOURNAMENT'] = 0;
        } else if ($TOURNAMENT_SUB_TYPE_ID == 3) {
            $data['FK_PARENT_TOURNAMENT_ID'] = $TOURNAMENT_PARENT_MULTI_DAY_ID;
            $data['TIMER_TOURNAMENT'] = 1;
        } else if ($TOURNAMENT_SUB_TYPE_ID == 4) {
            $data['FK_PARENT_TOURNAMENT_ID'] = '';
            $data['TIMER_TOURNAMENT'] = 1;
            $data['TIMER_TOURNAMENT_END_TIME'] = $TIMER_TOURNAMENT_END_TIME;
        }
        /**
         * End Tournament Information Section
         */

        /**
         * Timing Information Section
         */
        $CREATED_DATE = date("Y-m-d H:i:s");
        if ($TOURNAMENT_TYPE_ID == 1) {
            $data['LOBBY_ID'] = 3;
            $data['CATEGORY'] = 1;

            $data['REGISTER_START_TIME'] = $CREATED_DATE;
            $data['TOURNAMENT_START_TIME'] = date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s", time()) . " + 365 day"));
        } else {
            $data['TOURNAMENT_START_TIME'] = Carbon::parse($data['TOURNAMENT_START_TIME'])->format('Y-m-d H:i:s');
            $data['REGISTER_START_TIME'] = Carbon::parse($data['REGISTER_START_TIME'])->format('Y-m-d H:i:s');
        }


        $data['REGISTER_END_TIME'] = $data['TOURNAMENT_START_TIME'];
        $data['LATE_REGISTRATION_ALLOW'] = $data['LATE_REGISTRATION_ALLOW'] ?? 0;
        $data['LATE_REGISTRATION_END_TIME'] = $data['LATE_REGISTRATION_ALLOW'] == 1 ? $LATE_REGISTRATION_END_TIME : 0;
        $data['LOBBY_DISPLAY_INTERVAL'] = !empty($LOBBY_DISPLAY_INTERVAL) ? $LOBBY_DISPLAY_INTERVAL : 18;
        /**
         * End Timing Information Section
         */

        /**
         * Player Section
         */
        $data['PLAYER_PER_TABLE'] = $PLAYER_PER_TABLE;
        $data['T_MIN_PLAYERS'] = $T_MIN_PLAYERS;
        $data['T_MAX_PLAYERS'] = $T_MAX_PLAYERS;
        $data['MIN_PLAYERS'] = 2;
        $data['MAX_PLAYERS'] = $PLAYER_PER_TABLE;
        /**
         * End Player Section
         */

        /**
         * Entry criteria section
         */
        $COIN_TYPE_ID = $data['COIN_TYPE_ID'];
        if ($COIN_TYPE_ID == 8) {
            $data['BUYIN'] = 1;
            $data['ENTRY_FEE'] = 0;
            $data['TOURNAMENT_COMMISION'] = 0;
        } else {
            $data['BUYIN'] = $BUYIN;
            $data['ENTRY_FEE'] = $ENTRY_FEE;
            $data['TOURNAMENT_COMMISION'] = $ENTRY_FEE != 0 ? ($BUYIN / $ENTRY_FEE) : 0;
        }

        // Bounty Details
        $data['BOUNTY_AMOUNT'] = $BOUNTY_AMOUNT;
        $data['BOUNTY_ENTRY_FEE'] = $BOUNTY_ENTRY_FEE;
        $data['PROGRESSIVE_BOUNTY_PERCENTAGE'] = $PROGRESSIVE_BOUNTY_PERCENTAGE;

        if ($TOURNAMENT_TYPE_ID == 4 || $TOURNAMENT_TYPE_ID == 12 || $TOURNAMENT_TYPE_ID == 9) {
            $data['BOUNTY_AMOUNT'] = "";
            $data['BOUNTY_ENTRY_FEE'] = "";
            $data['PROGRESSIVE_BOUNTY_PERCENTAGE'] = "";
        }
        if ($TOURNAMENT_TYPE_ID == 10) {
            $data['PROGRESSIVE_BOUNTY_PERCENTAGE'] = "";
        }
        // End Bounty Details

        // for free roll tournament
        if ($CATEGORY == self::FREE_ROLL) {
            $data['LOBBY_ID'] = 9;
            $data['CATEGORY'] = 1;
        }

        $data['DEPOSIT_BALANCE_ALLOW'] = $data['DEPOSIT_BALANCE_ALLOW'] ?? 0;
        $data['PROMO_BALANCE_ALLOW'] = $data['PROMO_BALANCE_ALLOW'] ?? 0;
        $data['WIN_BALANCE_ALLOW'] = $data['WIN_BALANCE_ALLOW'] ?? 0;
        /**
         * End Entry criteria section
         */

        /**
         * Blind Structure
         */
        $data['BLIND_STRUCTURE_ID'] = $BLIND_STRUCTURE_ID;
        $data['TOURNAMENT_LEVEL'] = $TOURNAMENT_LEVEL;

        $data['SMALL_BLIND'] = $this->getSmallBlindByLevel($TOURNAMENT_LEVEL, $BLIND_STRUCTURE_ID);
        $data['BIG_BLIND'] = $this->getBigBlindByLevel($TOURNAMENT_LEVEL, $BLIND_STRUCTURE_ID);
        $data['LEVEL_PERIOD'] = $LEVEL_PERIOD;
        $data['TOURNAMENT_CHIPS'] = $TOURNAMENT_CHIPS;
        /**
         * End Blind Structure
         */

        /**
         * Time Settings
         */
        $data['PLAYER_HAND_TIME'] = $PLAYER_HAND_TIME;
        $data['DISCONNECT_TIME'] = $DISCONNECT_TIME;
        $data['EXTRA_TIME'] = $EXTRA_TIME;
        $data['PLAYER_MAX_EXTRATIME'] = $PLAYER_MAX_EXTRATIME;
        $data['ADDITIONAL_EXTRATIME_LEVEL_INTERVAL'] = $ADDITIONAL_EXTRATIME_LEVEL_INTERVAL;
        $data['ADDITIONAL_EXTRATIME'] = $ADDITIONAL_EXTRATIME;
        /**
         * End Time Settings
         */

        /**
         * Rebuy and Addon settings
         */
        $data['REBUY_ADDON_RE_ENTRY'] = $REBUY_ADDON_RE_ENTRY;
        /**
         * Server side checking for rebuy & addon values...................................
         * If coin type is VIP = 7 or Ticket = 8 then rebuy/addon features will be disabled
         * If tounament is freeroll means tournament amount = 0 then rebuy/addon feaures are disabled
         * If the rebuy-addon setting is not enabled then rebuy/addon features are disabled
         */
        if ($data['COIN_TYPE_ID'] == 8) {
            $data['IS_REBUY'] = 0;
            $data['IS_ADDON'] = 0;
            $data['REBUY_CHIPS'] = 0;
            $data['REBUY_ELIGIBLE_CHIPS'] = 0;
            $data['REBUY_END_TIME'] = 0;
            $data['REBUY_COUNT'] = 0;
            $data['REBUY_IN'] = 0;
            $data['REBUY_ENTRY_FEE'] = 0;
            $data['ADDON_CHIPS'] = 0;
            $data['ADDON_AMOUNT'] = 0;
            $data['ADDON_ENTRY_FEE'] = 0;
            $data['ADDON_BREAK_TIME'] = 0;
        } else {
            if ($data['REBUY_ADDON_RE_ENTRY'] == 1) {
                $data['IS_REBUY'] = 1;
                $data['IS_ADDON'] = 1;
                $data['REBUY_CHIPS'] = $REBUY_CHIPS;
                $data['REBUY_ELIGIBLE_CHIPS'] = $REBUY_ELIGIBLE_CHIPS;
                $data['REBUY_END_TIME'] = $REBUY_END_TIME;
                $data['REBUY_COUNT'] = $REBUY_COUNT;
                $data['REBUY_IN'] = $REBUY_IN;
                $data['REBUY_ENTRY_FEE'] = $REBUY_ENTRY_FEE;
                $data['DOUBLE_REBUYIN'] = $data['DOUBLE_REBUYIN'] ?? 0;
                $data['ADDON_CHIPS'] = $ADDON_CHIPS;
                $data['ADDON_AMOUNT'] = $ADDON_AMOUNT;
                $data['ADDON_ENTRY_FEE'] = $ADDON_ENTRY_FEE;
                $data['ADDON_BREAK_TIME'] = $ADDON_BREAK_TIME;
            } else if ($data['REBUY_ADDON_RE_ENTRY'] == 2) {
                $data['IS_REBUY'] = 1;
                $data['IS_ADDON'] = 0;
                $data['REBUY_CHIPS'] = $REBUY_CHIPS;
                $data['REBUY_ELIGIBLE_CHIPS'] = 0;
                $data['REBUY_END_TIME'] = $REBUY_END_TIME;
                $data['REBUY_COUNT'] = $REBUY_COUNT;
                $data['REBUY_IN'] = $REBUY_IN;
                $data['REBUY_ENTRY_FEE'] = $REBUY_ENTRY_FEE;
                $data['DOUBLE_REBUYIN'] = 0;
                $data['ADDON_CHIPS'] = 0;
                $data['ADDON_AMOUNT'] = 0;
                $data['ADDON_ENTRY_FEE'] = 0;
                $data['ADDON_BREAK_TIME'] = 0;
            } else {
                $data['IS_REBUY'] = 0;
                $data['IS_ADDON'] = 0;
                $data['REBUY_CHIPS'] = 0;
                $data['REBUY_ELIGIBLE_CHIPS'] = 0;
                $data['REBUY_END_TIME'] = 0;
                $data['REBUY_COUNT'] = 0;
                $data['REBUY_IN'] = 0;
                $data['REBUY_ENTRY_FEE'] = 0;
                $data['ADDON_CHIPS'] = 0;
                $data['ADDON_AMOUNT'] = 0;
                $data['ADDON_ENTRY_FEE'] = 0;
                $data['ADDON_BREAK_TIME'] = 0;
            }
        }
        /**
         * End Rebuy and Addon settings
         */

        /**
         * Prize Settings
         */
        $data['PRIZE_STRUCTURE_ID'] = 0;
        $data['PRIZE_STRUCTURE_TYPE_ID'] = 0;
        $data['PRIZE_TYPE'] = 1;
        $data['PRIZE_COIN_TYPE_ID'] = 1;
        $data['PRIZE_BALANCE_TYPE'] = 0;
        $data['FIXED_PRIZE'] = 0;
        $data['GUARENTIED_PRIZE'] = 0;
        $data['TICKET_VALUE'] = 0;
        $data['STAKE_LEVELS'] = $gameConstants['STAKE_LEVELS'];
        $data['MULTIDAY_PLAYER_PERCENTAGE'] = 0;
        $data['NO_OF_WINNERS'] = 3;
        $data['SATELLITES_GUARANTEED_PLACES_PAID'] = 1;
        if ($TOURNAMENT_TYPE_ID == 4 || $TOURNAMENT_TYPE_ID == 12 || $TOURNAMENT_TYPE_ID == 10 || $TOURNAMENT_TYPE_ID == 11 || $TOURNAMENT_TYPE_ID == 1) {
            if ($TOURNAMENT_SUB_TYPE_ID == 1) {
                // Normal Normal
                $data['PRIZE_STRUCTURE_ID'] = $PRIZE_STRUCTURE_ID;
                $data['PRIZE_STRUCTURE_TYPE_ID'] = $PRIZE_STRUCTURE_TYPE_ID;
                $data['PRIZE_BALANCE_TYPE'] = $PRIZE_BALANCE_TYPE;
                $data['FIXED_PRIZE'] = $FIXED_PRIZE;
                $data['GUARENTIED_PRIZE'] = $GUARENTIED_PRIZE;
                if ($data['PRIZE_STRUCTURE_ID'] == 1) {
                    $data['NO_OF_WINNERS'] = $NO_OF_WINNERS_CUSTOM;
                } else if ($data['PRIZE_STRUCTURE_ID'] == 4) {
                    $data['NO_OF_WINNERS'] = $NO_OF_WINNERS_CUSTOM_MIX;
                }
            } else if ($TOURNAMENT_SUB_TYPE_ID == 2) {
                // Normal Satellite
                $data['PRIZE_STRUCTURE_ID'] = 2;
                $data['PRIZE_STRUCTURE_TYPE_ID'] = 1;
                $data['PRIZE_TYPE'] = 8;
                $data['PRIZE_COIN_TYPE_ID'] = 8;
                $data['PRIZE_BALANCE_TYPE'] = $PRIZE_BALANCE_TYPE;
                $data['FIXED_PRIZE'] = $FIXED_PRIZE;
                if (!empty($SATELLITES_GUARANTEED_PLACES_PAID) && $SATELLITES_GUARANTEED_PLACES_PAID != 0) {
                    $data['SATELLITES_GUARANTEED_PLACES_PAID'] = $SATELLITES_GUARANTEED_PLACES_PAID;
                }
                $TICKET_VALUE_GET = $this->getParentTournamentTicketValue($data['FK_PARENT_TOURNAMENT_ID']);
                $data['GUARENTIED_PRIZE'] = $data['SATELLITES_GUARANTEED_PLACES_PAID'] * $TICKET_VALUE_GET;
                $data['TICKET_VALUE'] = $TICKET_VALUE_GET;
            } else if ($TOURNAMENT_SUB_TYPE_ID == 3) {
                // Normal Multi Child
                $data['PRIZE_STRUCTURE_ID'] = 3;
                $data['PRIZE_STRUCTURE_TYPE_ID'] = 1;
                $data['PRIZE_TYPE'] = 8;
                $data['PRIZE_COIN_TYPE_ID'] = 8;
                $data['PRIZE_BALANCE_TYPE'] = $PRIZE_BALANCE_TYPE;
                $data['FIXED_PRIZE'] = $FIXED_PRIZE;
                $data['GUARENTIED_PRIZE'] = $GUARENTIED_PRIZE;
                $data['MULTIDAY_PLAYER_PERCENTAGE'] = $MULTIDAY_PLAYER_PERCENTAGE;
            } else if ($TOURNAMENT_SUB_TYPE_ID == 4) {
                // Normal Timer
                $data['PRIZE_STRUCTURE_ID'] = 3;
                $data['PRIZE_STRUCTURE_TYPE_ID'] = 1;
                $data['PRIZE_TYPE'] = 1;
                $data['PRIZE_COIN_TYPE_ID'] = 1;
                $data['PRIZE_BALANCE_TYPE'] = $PRIZE_BALANCE_TYPE;
                $data['FIXED_PRIZE'] = $FIXED_PRIZE;
                $data['GUARENTIED_PRIZE'] = $GUARENTIED_PRIZE;
            }
        } else if ($TOURNAMENT_TYPE_ID == 9) {
            // Multi Day Main
            $data['PRIZE_STRUCTURE_ID'] = 3;
            $data['PRIZE_STRUCTURE_TYPE_ID'] = 1;
            $data['PRIZE_TYPE'] = 1;
            $data['PRIZE_COIN_TYPE_ID'] = 1;
            $data['PRIZE_BALANCE_TYPE'] = $PRIZE_BALANCE_TYPE;
            $data['FIXED_PRIZE'] = $FIXED_PRIZE;
            $data['GUARENTIED_PRIZE'] = $GUARENTIED_PRIZE;
            $data['STAKE_LEVELS'] = $TOURNAMENT_LEVEL;
            $data['BUYIN'] = 1;
            $data['ENTRY_FEE'] = 0;
            $data['TOURNAMENT_CHIPS'] = 0;
        }
        /**
         * End Prize Settings
         */

        //private table and password
        if ($PRIVATE_TABLE == 1) {
            $data['PASSWORD'] = md5($PASSWORD);
        }

        DB::transaction(function () use ($data) {
            $tournament = new Tournament($data);

            $tournament->save();
            $TOURNAMENT_ID = $tournament->TOURNAMENT_ID;

            /**
             * extended from App\Http\Controllers\bo\tournament\TournamentBaseController
             */
            $this->insertTournamentBlindStructures($TOURNAMENT_ID, $data['TOURNAMENT_LEVEL'], $data['BLIND_STRUCTURE_ID'], $data['LEVEL_PERIOD'], $data['MINI_GAME_TYPE_ID'], $data['TOURNAMENT_TYPE_ID']);

            if ($data['PRIZE_STRUCTURE_ID'] == 1) {

                if ($data['NO_OF_WINNERS'] > 0) {
                    $tournamentWinnerLevelArray = [];

                    foreach ($data['WINNER_PERCENTAGE'] as $key => $value) {
                        $tournamentWinnerLevel = [];

                        if ($data['TOURNAMENT_SUB_TYPE_ID'] == 2) {
                            $prize_value = 1;
                        } else {
                            $prize_value = $this->calPercentage($value, $data['GUARENTIED_PRIZE'], 0);
                        }

                        $winner_percentage = $value;

                        $tournamentWinnerLevel['TOURNAMENT_ID']     = $TOURNAMENT_ID;
                        $tournamentWinnerLevel['RANK']              = $key + 1;
                        $tournamentWinnerLevel['PRIZE_VALUE']       = $prize_value;
                        $tournamentWinnerLevel['PRIZE_TYPE']        = $data['PRIZE_TYPE'];
                        $tournamentWinnerLevel['WINNER_PERCENTAGE'] = $winner_percentage;

                        $tournamentWinnerLevelArray[] = $tournamentWinnerLevel;
                    }
                    TournamentWinnerLevel::insert($tournamentWinnerLevelArray);
                }
            } else if ($data['PRIZE_STRUCTURE_ID'] == 4) {
                if ($data['NO_OF_WINNERS'] > 0) {
                    $tournamentWinnerLevelArray = [];

                    for ($RANK = 0; $RANK < $data['NO_OF_WINNERS']; $RANK++) {
                        $tournamentWinnerLevel = [];

                        if ($data['TOURNAMENT_SUB_TYPE_ID'] == 2) {
                            $prize_value = 1;
                            $prize_tournament_name = "";
                        } else {
                            $data['PRIZE_TYPE'] = $data["CUSTOM_MIX_PRIZE_TYPE"][$RANK];
                            if ($data['PRIZE_TYPE'] == 8) {
                                $prize_value = 1;
                                $prize_tournament_name = $data['PRIZE_TOURNAMENT_NAME'][$RANK];
                                $winner_percentage = 0;
                            } else {
                                $prize_value = $data["PRIZE_VALUE"][$RANK];
                                $prize_tournament_name = "";
                                $winner_percentage = ($data["PRIZE_VALUE"][$RANK] / $data['GUARENTIED_PRIZE']) * 100;
                            }
                        }

                        $tournamentWinnerLevel['TOURNAMENT_ID']         = $TOURNAMENT_ID;
                        $tournamentWinnerLevel['RANK']                  = $RANK + 1;
                        $tournamentWinnerLevel['PRIZE_VALUE']           = $prize_value;
                        $tournamentWinnerLevel['PRIZE_TYPE']            = $data['PRIZE_TYPE'];
                        $tournamentWinnerLevel['WINNER_PERCENTAGE']     = $winner_percentage;
                        $tournamentWinnerLevel['PRIZE_TOURNAMENT_NAME'] = $prize_tournament_name;

                        $tournamentWinnerLevelArray[] = $tournamentWinnerLevel;
                    }
                    TournamentWinnerLevel::insert($tournamentWinnerLevelArray);
                }
            }
        }, 5);

        return redirect()->route('tournaments.manage-tourneys.create')->with('success', "Tournament Added successfully");
    }

    public function createEditTournamentData($data = null)
    {
        $data = $data ?? [];
        $data["tournamentLimits"] = $this->getTournamentLimits();
        $data["servers"] = $this->getTournamentServers();
        $data["satalliteTournaments"]    = $this->getTournamentsForSatellite();
        $data["multiDayChildTournaments"]    = $this->getTournamentsForMultiDayChild();
        $data["blindStructures"]    = $this->getBlindStructure();
        $data["multiplePrizeStructures"] = $this->getMultiplePrizeStructures();
        $data["categories"] = $this->getTournamentSubTabCategory();
        $data["templates"] = Template::select('template_id', 'template_name')
            ->where('template_type_id', 1)
            ->orderBy('template_id', 'desc')
            ->active()
            ->get();
        return $data;
    }

    public function edit(Request $request, $tournamentId)
    {
        $tournamentId = \decrypt($tournamentId);
        $tournament = $this->reverseEngineeringTournament($tournamentId);
        // dd($tournament);
        $data['tournament'] = $tournament;

        $data = $data ?? [];
        $data = $data + $this->createEditTournamentData($data);

        return view('bo.views.tournaments.manage-tourneys.edit', $data);
    }



    public function update(EditTournamentValidation $request, $tournamentId)
    {
        $TOURNAMENT_ID = decrypt($tournamentId);
        $tournament = Tournament::find($TOURNAMENT_ID);

        $data = $request->all();

        /**
         * extract game constant array to contants
         */
        $gameConstants = config('constant.game');
        \extract($gameConstants);

        /**
         * all this given values taking from
         * constant file game section
         */
        $data['IS_REGISTER_GAME'] = $tournament->IS_REGISTER_GAME;
        $data['TOURNAMENT_BREAK_TIME'] = $tournament->TOURNAMENT_BREAK_TIME;
        $data['BREAK_INTERVAL_TIME'] = $tournament->BREAK_INTERVAL_TIME;
        $data['PAID_OPTION'] = $tournament->PAID_OPTION;
        $data['ALLOW_OFFLINE_PLAYERS'] = $tournament->ALLOW_OFFLINE_PLAYERS;
        $data['TOURNAMENT_STATUS'] = $tournament->TOURNAMENT_STATUS;
        $data['LOBBY_ID'] = $tournament->TOURNAMENT_LOBBY_ID;
        $data['MINIGAMES_ID'] = $tournament->MINIGAMES_ID;
        $data['PARTNER_ID'] = $tournament->ADMIN_USER_ID;
        $data['SITOUT_TIME'] = $tournament->SITEOUT_TIME;
        $data['TIME_BANK_INTERVAL_TIME'] = $tournament->TIME_BANK_INTERVAL_TIME;
        // end constants

        /**
         * static values defined
         */
        $data['LOBBY_MENU_ID']  =   $tournament->LOBBY_MENU_ID;
        $data['ADDON_COUNT']    =   $tournament->ADDON_COUNT;
        // end static values

        /**
         * extract all the request parameter to variables
         */
        extract($data);

        $TOURNAMENT_TYPE_ID = $tournament->TOURNAMENT_TYPE_ID;

        /**
         * Tournament Information Section
         */
        $data['FK_PARENT_TOURNAMENT_ID'] = '';
        $data['TIMER_TOURNAMENT'] = '';
        $data['TIMER_TOURNAMENT_END_TIME'] = '';

        if ($TOURNAMENT_SUB_TYPE_ID == 1) {
            $data['FK_PARENT_TOURNAMENT_ID'] = '';
            $data['TIMER_TOURNAMENT'] = 0;
        } else if ($TOURNAMENT_SUB_TYPE_ID == 2) {
            $data['FK_PARENT_TOURNAMENT_ID'] = $tournament->TOURNAMENT_PARENT_SATELLITE_ID;
            $data['TIMER_TOURNAMENT'] = 0;
        } else if ($TOURNAMENT_SUB_TYPE_ID == 3) {
            $data['FK_PARENT_TOURNAMENT_ID'] = $tournament->TOURNAMENT_PARENT_MULTI_DAY_ID;
            $data['TIMER_TOURNAMENT'] = 1;
        } else if ($TOURNAMENT_SUB_TYPE_ID == 4) {
            $data['FK_PARENT_TOURNAMENT_ID'] = '';
            $data['TIMER_TOURNAMENT'] = 1;
            $data['TIMER_TOURNAMENT_END_TIME'] = $tournament->TIMER_TOURNAMENT_END_TIME;
        }
        /**
         * End Tournament Information Section
         */

        /**
         * Timing Information Section
         */
        $CREATED_DATE = $tournament->TOURNAMENT_START_TIME;
        if ($TOURNAMENT_TYPE_ID == 1) {
            $data['LOBBY_ID'] = 3;
            $data['CATEGORY'] = 1;

            $data['TOURNAMENT_START_TIME'] = $CREATED_DATE;
            $data['REGISTER_START_TIME'] = $tournament->REGISTER_START_TIME;
        } else {
            $data['TOURNAMENT_START_TIME'] = $tournament->TOURNAMENT_START_TIME;
            $data['REGISTER_START_TIME'] = $tournament->REGISTER_START_TIME;
        }


        $data['REGISTER_END_TIME'] = $tournament->REGISTER_END_TIME;
        if ($tournament->TOURNAMENT_STATUS == 0) {
            $data['LATE_REGISTRATION_ALLOW'] = $data['LATE_REGISTRATION_ALLOW'] ?? 0;
            $data['LATE_REGISTRATION_END_TIME'] = $data['LATE_REGISTRATION_ALLOW'] == 1 ? $LATE_REGISTRATION_END_TIME : 0;
        } else {
            $data['LATE_REGISTRATION_ALLOW'] = $tournament->LATE_REGISTRATION_ALLOW;
            $data['LATE_REGISTRATION_END_TIME'] = $tournament->LATE_REGISTRATION_END_TIME;
        }
        $data['LOBBY_DISPLAY_INTERVAL'] = !empty($LOBBY_DISPLAY_INTERVAL) ? $LOBBY_DISPLAY_INTERVAL : 18;
        /**
         * End Timing Information Section
         */

        /**
         * Player Section
         */
        if ($tournament->TOURNAMENT_STATUS == 0 || $tournament->TOURNAMENT_STATUS == 1) {
            $data['PLAYER_PER_TABLE'] = $PLAYER_PER_TABLE;
            $data['T_MIN_PLAYERS'] = $T_MIN_PLAYERS;
            $data['T_MAX_PLAYERS'] = $T_MAX_PLAYERS;
            $data['MIN_PLAYERS'] = 2;
            $data['MAX_PLAYERS'] = $PLAYER_PER_TABLE;
        } else {
            $data['PLAYER_PER_TABLE'] = $tournament->PLAYER_PER_TABLE;
            $data['T_MIN_PLAYERS'] = $tournament->T_MIN_PLAYERS;
            $data['T_MAX_PLAYERS'] = $tournament->T_MAX_PLAYERS;
            $data['MIN_PLAYERS'] = $tournament->MIN_PLAYERS;
            $data['MAX_PLAYERS'] = $tournament->MAX_PLAYERS;
        }
        /**
         * End Player Section
         */

        /**
         * Entry criteria section
         */

        $data['COIN_TYPE_ID'] = $tournament->COIN_TYPE_ID;
        $data['BUYIN'] = $tournament->BUYIN;
        $data['ENTRY_FEE'] = $tournament->ENTRY_FEE;
        $data['TOURNAMENT_COMMISION'] = $tournament->TOURNAMENT_COMMISION;

        // Bounty Details
        $data['BOUNTY_AMOUNT'] = $tournament->BOUNTY_AMOUNT;
        $data['BOUNTY_ENTRY_FEE'] = $tournament->BOUNTY_ENTRY_FEE;
        $data['PROGRESSIVE_BOUNTY_PERCENTAGE'] = $tournament->PROGRESSIVE_BOUNTY_PERCENTAGE;
        // End Bounty Details

        // for free roll tournament
        if (($tournament->TOURNAMENT_STATUS == 0 || $tournament->TOURNAMENT_STATUS == 1) && $CATEGORY == self::FREE_ROLL) {
            $data['LOBBY_ID'] = 9;
            $data['CATEGORY'] = 1;
        } else {
            $data['LOBBY_ID'] = $tournament->LOBBY_ID;
            $data['CATEGORY'] = $tournament->CATEGORY;
        }

        $data['DEPOSIT_BALANCE_ALLOW'] = $tournament->DEPOSIT_BALANCE_ALLOW;
        $data['PROMO_BALANCE_ALLOW'] = $tournament->PROMO_BALANCE_ALLOW;
        $data['WIN_BALANCE_ALLOW'] = $tournament->WIN_BALANCE_ALLOW;
        /**
         * End Entry criteria section
         */

        /**
         * Blind Structure
         */
        if ($tournament->TOURNAMENT_STATUS == 0) {
            $data['BLIND_STRUCTURE_ID'] = $BLIND_STRUCTURE_ID;
            $data['TOURNAMENT_LEVEL'] = $TOURNAMENT_LEVEL;
            $data['SMALL_BLIND'] = $this->getSmallBlindByLevel($data['TOURNAMENT_LEVEL'], $data['BLIND_STRUCTURE_ID']);
            $data['BIG_BLIND'] = $this->getBigBlindByLevel($data['TOURNAMENT_LEVEL'], $data['BLIND_STRUCTURE_ID']);
            $data['LEVEL_PERIOD'] = $LEVEL_PERIOD;
            $data['TOURNAMENT_CHIPS'] = $tournament->TOURNAMENT_CHIPS;
        } else {
            $data['BLIND_STRUCTURE_ID'] = $tournament->BLIND_STRUCTURE_ID;
            $data['TOURNAMENT_LEVEL'] = $tournament->TOURNAMENT_LEVEL;
            $data['SMALL_BLIND'] =  $tournament->SMALL_BLIND;
            $data['BIG_BLIND'] =  $tournament->BIG_BLIND;
            $data['LEVEL_PERIOD'] = $tournament->LEVEL_PERIOD;
            $data['TOURNAMENT_CHIPS'] = $tournament->TOURNAMENT_CHIPS;
        }
        /**
         * End Blind Structure
         */

        /**
         * Time Settings
         */
        $data['PLAYER_HAND_TIME'] = $tournament->TOURNAMENT_STATUS == 0 ? $PLAYER_HAND_TIME : $tournament->PLAYER_HAND_TIME;
        $data['DISCONNECT_TIME'] = $tournament->TOURNAMENT_STATUS == 0 ? $DISCONNECT_TIME : $tournament->DISCONNECT_TIME;
        $data['EXTRA_TIME'] = $tournament->TOURNAMENT_STATUS == 0 ? $EXTRA_TIME : $tournament->EXTRA_TIME;
        $data['PLAYER_MAX_EXTRATIME'] = $tournament->TOURNAMENT_STATUS == 0 ? $PLAYER_MAX_EXTRATIME : $tournament->PLAYER_MAX_EXTRATIME;
        $data['ADDITIONAL_EXTRATIME_LEVEL_INTERVAL'] = $tournament->TOURNAMENT_STATUS == 0 ? $ADDITIONAL_EXTRATIME_LEVEL_INTERVAL : $tournament->ADDITIONAL_EXTRATIME_LEVEL_INTERVAL;
        $data['ADDITIONAL_EXTRATIME'] = $tournament->TOURNAMENT_STATUS == 0 ? $ADDITIONAL_EXTRATIME : $tournament->ADDITIONAL_EXTRATIME;
        /**
         * End Time Settings
         */

        /**
         * Rebuy and Addon settings
         */
        if ($tournament->TOURNAMENT_STATUS == 0) {

            $data['REBUY_ADDON_RE_ENTRY'] = $REBUY_ADDON_RE_ENTRY;
            /**
             * Server side checking for rebuy & addon values...................................
             * If coin type is VIP = 7 or Ticket = 8 then rebuy/addon features will be disabled
             * If tounament is freeroll means tournament amount = 0 then rebuy/addon feaures are disabled
             * If the rebuy-addon setting is not enabled then rebuy/addon features are disabled
             */
            if ($data['COIN_TYPE_ID'] == 8) {
                $data['IS_REBUY'] = 0;
                $data['IS_ADDON'] = 0;
                $data['REBUY_CHIPS'] = 0;
                $data['REBUY_ELIGIBLE_CHIPS'] = 0;
                $data['REBUY_END_TIME'] = 0;
                $data['REBUY_COUNT'] = 0;
                $data['REBUY_IN'] = 0;
                $data['REBUY_ENTRY_FEE'] = 0;
                $data['ADDON_CHIPS'] = 0;
                $data['ADDON_AMOUNT'] = 0;
                $data['ADDON_ENTRY_FEE'] = 0;
                $data['ADDON_BREAK_TIME'] = 0;
            } else {
                if ($data['REBUY_ADDON_RE_ENTRY'] == 1) {
                    $data['IS_REBUY'] = 1;
                    $data['IS_ADDON'] = 1;
                    $data['REBUY_CHIPS'] = $REBUY_CHIPS;
                    $data['REBUY_ELIGIBLE_CHIPS'] = $REBUY_ELIGIBLE_CHIPS;
                    $data['REBUY_END_TIME'] = $REBUY_END_TIME;
                    $data['REBUY_COUNT'] = $REBUY_COUNT;
                    $data['REBUY_IN'] = $REBUY_IN;
                    $data['REBUY_ENTRY_FEE'] = $REBUY_ENTRY_FEE;
                    $data['DOUBLE_REBUYIN'] = $data['DOUBLE_REBUYIN'] ?? 0;
                    $data['ADDON_CHIPS'] = $ADDON_CHIPS;
                    $data['ADDON_AMOUNT'] = $ADDON_AMOUNT;
                    $data['ADDON_ENTRY_FEE'] = $ADDON_ENTRY_FEE;
                    $data['ADDON_BREAK_TIME'] = $ADDON_BREAK_TIME;
                } else if ($data['REBUY_ADDON_RE_ENTRY'] == 2) {
                    $data['IS_REBUY'] = 1;
                    $data['IS_ADDON'] = 0;
                    $data['REBUY_CHIPS'] = $REBUY_CHIPS;
                    $data['REBUY_ELIGIBLE_CHIPS'] = 0;
                    $data['REBUY_END_TIME'] = $REBUY_END_TIME;
                    $data['REBUY_COUNT'] = $REBUY_COUNT;
                    $data['REBUY_IN'] = $REBUY_IN;
                    $data['REBUY_ENTRY_FEE'] = $REBUY_ENTRY_FEE;
                    $data['DOUBLE_REBUYIN'] = 0;
                    $data['ADDON_CHIPS'] = 0;
                    $data['ADDON_AMOUNT'] = 0;
                    $data['ADDON_ENTRY_FEE'] = 0;
                    $data['ADDON_BREAK_TIME'] = 0;
                } else {
                    $data['IS_REBUY'] = 0;
                    $data['IS_ADDON'] = 0;
                    $data['REBUY_CHIPS'] = 0;
                    $data['REBUY_ELIGIBLE_CHIPS'] = 0;
                    $data['REBUY_END_TIME'] = 0;
                    $data['REBUY_COUNT'] = 0;
                    $data['REBUY_IN'] = 0;
                    $data['REBUY_ENTRY_FEE'] = 0;
                    $data['ADDON_CHIPS'] = 0;
                    $data['ADDON_AMOUNT'] = 0;
                    $data['ADDON_ENTRY_FEE'] = 0;
                    $data['ADDON_BREAK_TIME'] = 0;
                }
            }
        }
        /**
         * End Rebuy and Addon settings
         */

        /**
         * Prize Settings
         */
        if ($tournament->TOURNAMENT_STATUS == 0) {
            $data['PRIZE_STRUCTURE_ID'] = 0;
            $data['PRIZE_STRUCTURE_TYPE_ID'] = 0;
            $data['PRIZE_TYPE'] = 1;
            $data['PRIZE_COIN_TYPE_ID'] = 1;
            $data['PRIZE_BALANCE_TYPE'] = 0;
            $data['FIXED_PRIZE'] = 0;
            $data['GUARENTIED_PRIZE'] = 0;
            $data['TICKET_VALUE'] = 0;
            $data['STAKE_LEVELS'] = $gameConstants['STAKE_LEVELS'];
            $data['MULTIDAY_PLAYER_PERCENTAGE'] = 0;
            $data['NO_OF_WINNERS'] = 3;
            $data['SATELLITES_GUARANTEED_PLACES_PAID'] = 1;
            if ($TOURNAMENT_TYPE_ID == 4 || $TOURNAMENT_TYPE_ID == 12 || $TOURNAMENT_TYPE_ID == 10 || $TOURNAMENT_TYPE_ID == 11 || $TOURNAMENT_TYPE_ID == 1) {
                if ($TOURNAMENT_SUB_TYPE_ID == 1) {
                    // Normal Normal
                    $data['PRIZE_STRUCTURE_ID'] = $PRIZE_STRUCTURE_ID;
                    $data['PRIZE_STRUCTURE_TYPE_ID'] = $PRIZE_STRUCTURE_TYPE_ID;
                    $data['PRIZE_BALANCE_TYPE'] = $PRIZE_BALANCE_TYPE;
                    $data['FIXED_PRIZE'] = $FIXED_PRIZE;
                    $data['GUARENTIED_PRIZE'] = $GUARENTIED_PRIZE;
                    if ($data['PRIZE_STRUCTURE_ID'] == 1) {
                        $data['NO_OF_WINNERS'] = $NO_OF_WINNERS_CUSTOM;
                    } else if ($data['PRIZE_STRUCTURE_ID'] == 4) {
                        $data['NO_OF_WINNERS'] = $NO_OF_WINNERS_CUSTOM_MIX;
                    }
                } else if ($TOURNAMENT_SUB_TYPE_ID == 2) {
                    // Normal Satellite
                    $data['PRIZE_STRUCTURE_ID'] = 2;
                    $data['PRIZE_STRUCTURE_TYPE_ID'] = 1;
                    $data['PRIZE_TYPE'] = 8;
                    $data['PRIZE_COIN_TYPE_ID'] = 8;
                    $data['PRIZE_BALANCE_TYPE'] = $PRIZE_BALANCE_TYPE;
                    $data['FIXED_PRIZE'] = $FIXED_PRIZE;
                    if (!empty($SATELLITES_GUARANTEED_PLACES_PAID) && $SATELLITES_GUARANTEED_PLACES_PAID != 0) {
                        $data['SATELLITES_GUARANTEED_PLACES_PAID'] = $SATELLITES_GUARANTEED_PLACES_PAID;
                    }
                    $TICKET_VALUE_GET = $this->getParentTournamentTicketValue($data['FK_PARENT_TOURNAMENT_ID']);
                    $data['GUARENTIED_PRIZE'] = $data['SATELLITES_GUARANTEED_PLACES_PAID'] * $TICKET_VALUE_GET;
                    $data['TICKET_VALUE'] = $TICKET_VALUE_GET;
                } else if ($TOURNAMENT_SUB_TYPE_ID == 3) {
                    // Normal Multi Child
                    $data['PRIZE_STRUCTURE_ID'] = 3;
                    $data['PRIZE_STRUCTURE_TYPE_ID'] = 1;
                    $data['PRIZE_TYPE'] = 8;
                    $data['PRIZE_COIN_TYPE_ID'] = 8;
                    $data['PRIZE_BALANCE_TYPE'] = $PRIZE_BALANCE_TYPE;
                    $data['FIXED_PRIZE'] = $FIXED_PRIZE;
                    $data['GUARENTIED_PRIZE'] = $GUARENTIED_PRIZE;
                    $data['MULTIDAY_PLAYER_PERCENTAGE'] = $MULTIDAY_PLAYER_PERCENTAGE;
                } else if ($TOURNAMENT_SUB_TYPE_ID == 4) {
                    // Normal Timer
                    $data['PRIZE_STRUCTURE_ID'] = 3;
                    $data['PRIZE_STRUCTURE_TYPE_ID'] = 1;
                    $data['PRIZE_TYPE'] = 1;
                    $data['PRIZE_COIN_TYPE_ID'] = 1;
                    $data['PRIZE_BALANCE_TYPE'] = $PRIZE_BALANCE_TYPE;
                    $data['FIXED_PRIZE'] = $FIXED_PRIZE;
                    $data['GUARENTIED_PRIZE'] = $GUARENTIED_PRIZE;
                }
            } else if ($TOURNAMENT_TYPE_ID == 9) {
                // Multi Day Main
                $data['PRIZE_STRUCTURE_ID'] = 3;
                $data['PRIZE_STRUCTURE_TYPE_ID'] = 1;
                $data['PRIZE_TYPE'] = 1;
                $data['PRIZE_COIN_TYPE_ID'] = 1;
                $data['PRIZE_BALANCE_TYPE'] = $PRIZE_BALANCE_TYPE;
                $data['FIXED_PRIZE'] = $FIXED_PRIZE;
                $data['GUARENTIED_PRIZE'] = $GUARENTIED_PRIZE;
                $data['STAKE_LEVELS'] = $TOURNAMENT_LEVEL;
                $data['BUYIN'] = 1;
                $data['ENTRY_FEE'] = 0;
                $data['TOURNAMENT_CHIPS'] = 0;
            }
        }
        /**
         * End Prize Settings
         */

        //private table and password
        $data['PRIVATE_TABLE'] = $tournament->PRIVATE_TABLE;
        if ($tournament->TOURNAMENT_STATUS == 0 && $PRIVATE_TABLE == 1) {
            $data['PASSWORD'] = $PASSWORD == "XXXXXXXX" ? $tournament->PASSWORD : md5($PASSWORD);
        }

        DB::transaction(function () use ($data, $tournament) {
            // $tournament = new Tournament($data);
            $tournament->fill($data);
            $tournament->save();
            $TOURNAMENT_ID = $tournament->TOURNAMENT_ID;

            if ($tournament->TOURNAMENT_STATUS == 0) {
                TournamentLevel::where('TOURNAMENT_ID', $TOURNAMENT_ID)->delete();
                /**
                 * extended from App\Http\Controllers\bo\tournament\TournamentBaseController
                 */
                $this->insertTournamentBlindStructures($TOURNAMENT_ID, $data['TOURNAMENT_LEVEL'], $tournament->BLIND_STRUCTURE_ID, $data['LEVEL_PERIOD'], $tournament->MINI_GAME_TYPE_ID, $tournament->TOURNAMENT_TYPE_ID);

                if ($tournament->PRIZE_STRUCTURE_ID == 1) {

                    if ($data['NO_OF_WINNERS'] > 0) {
                        $tournamentWinnerLevelArray = [];

                        foreach ($data['WINNER_PERCENTAGE'] as $key => $value) {
                            $tournamentWinnerLevel = [];

                            if ($data['TOURNAMENT_SUB_TYPE_ID'] == 2) {
                                $prize_value = 1;
                            } else {
                                $prize_value = $this->calPercentage($value, $data['GUARENTIED_PRIZE'], 0);
                            }

                            $winner_percentage = $value;

                            $tournamentWinnerLevel['TOURNAMENT_ID']     = $TOURNAMENT_ID;
                            $tournamentWinnerLevel['RANK']              = $key + 1;
                            $tournamentWinnerLevel['PRIZE_VALUE']       = $prize_value;
                            $tournamentWinnerLevel['PRIZE_TYPE']        = $data['PRIZE_TYPE'];
                            $tournamentWinnerLevel['WINNER_PERCENTAGE'] = $winner_percentage;

                            $tournamentWinnerLevelArray[] = $tournamentWinnerLevel;
                        }

                        TournamentWinnerLevel::where('TOURNAMENT_ID', $TOURNAMENT_ID)->delete();
                        TournamentWinnerLevel::insert($tournamentWinnerLevelArray);
                    }
                } else if ($tournament->PRIZE_STRUCTURE_ID == 4) {
                    if ($data['NO_OF_WINNERS'] > 0) {
                        $tournamentWinnerLevelArray = [];

                        for ($RANK = 0; $RANK < $data['NO_OF_WINNERS']; $RANK++) {
                            $tournamentWinnerLevel = [];

                            if ($data['TOURNAMENT_SUB_TYPE_ID'] == 2) {
                                $prize_value = 1;
                                $prize_tournament_name = "";
                            } else {
                                $data['PRIZE_TYPE'] = $data["CUSTOM_MIX_PRIZE_TYPE"][$RANK];
                                if ($data['PRIZE_TYPE'] == 8) {
                                    $prize_value = 1;
                                    $prize_tournament_name = $data['PRIZE_TOURNAMENT_NAME'][$RANK];
                                    $winner_percentage = 0;
                                } else {
                                    $prize_value = $data["PRIZE_VALUE"][$RANK];
                                    $prize_tournament_name = "";
                                    $winner_percentage = ($data["PRIZE_VALUE"][$RANK] / $data['GUARENTIED_PRIZE']) * 100;
                                }
                            }

                            $tournamentWinnerLevel['TOURNAMENT_ID']         = $TOURNAMENT_ID;
                            $tournamentWinnerLevel['RANK']                  = $RANK + 1;
                            $tournamentWinnerLevel['PRIZE_VALUE']           = $prize_value;
                            $tournamentWinnerLevel['PRIZE_TYPE']            = $data['PRIZE_TYPE'];
                            $tournamentWinnerLevel['WINNER_PERCENTAGE']     = $winner_percentage;
                            $tournamentWinnerLevel['PRIZE_TOURNAMENT_NAME'] = $prize_tournament_name;

                            $tournamentWinnerLevelArray[] = $tournamentWinnerLevel;
                        }

                        TournamentWinnerLevel::where('TOURNAMENT_ID', $TOURNAMENT_ID)->delete();
                        TournamentWinnerLevel::insert($tournamentWinnerLevelArray);
                    }
                }
            }
        }, 5);

        return redirect()->route('tournaments.manage-tourneys.edit', ['tournamentId' => $tournamentId])->with('success', "Tournament Updated successfully");
    }

    public function clone(Request $request, $tournamentId)
    {
        $tournamentId = \decrypt($tournamentId);
        $tournament = $this->reverseEngineeringTournament($tournamentId);
        // dd($tournament);
        $data['tournament'] = $tournament;

        $data = $data ?? [];
        $data = $data + $this->createEditTournamentData($data);

        return view('bo.views.tournaments.manage-tourneys.create', $data);
    }

    public function deleteTemplate($template_id)
    {
        $template_id = \decrypt($template_id);

        if (Template::where('template_id', $template_id)->update(['status' => 0])) {
            return redirect()->route('tournaments.manage-tourneys.create')->with('success', "Template deleted successfully");
        } else {
            return redirect()->route('tournaments.manage-tourneys.create')->with('error', "Something went wrong");
        }
    }

    public function rank(Request $request, $tournamentId)
    {
        $tournamentId = \decrypt($tournamentId);

        $perPage = config('poker_config.paginate.per_page');

        $tournament = $this->filterTournamentConditionalQuery(
            [],
            [
                'select' => [
                    "t.TOURNAMENT_ID",
                    "t.TOURNAMENT_NAME",
                    "t.TOURNAMENT_START_TIME",
                    "t.TOURNAMENT_END_TIME",
                    "t.NO_OF_WINNERS",
                    "tt.DESCRIPTION as TOURNAMENT_TYPE_DESCRIPTION",
                    "m.MINIGAMES_TYPE_NAME",
                ],
                'isDefaultTournamentTypeId' => false
            ]
        );

        $tournament = $tournament->where('t.TOURNAMENT_ID', $tournamentId)->first();

        $data['tournament'] = $tournament;
        $data['ranks'] = $this->getTournamentAllUsersRanks($tournamentId, null, true)->paginate($perPage);

        return view('bo.views.tournaments.manage-tourneys.rank', $data);
    }

    public function rankExport(Request $request, $tournamentId)
    {
        $tournamentId = \decrypt($tournamentId);

        $ranks = $this->getTournamentAllUsersRanks($tournamentId);

        if (count($ranks) > 0) {
            $resultArray = [];
            foreach ($ranks as $key => $item) {
                $result = [];
                $result['RANK'] = $item->RANK;
                $result['USER_ID'] = $item->USER_ID;
                $result['USERNAME'] = $item->USERNAME;
                $result['EMAIL_ID'] = $item->EMAIL_ID;
                $result['CONTACT'] = $item->CONTACT;
                $result['PRIZE_VALUE'] = $item->PRIZE_VALUE ?? "0";
                $result['WINNER_PERCENTAGE'] = $item->WINNER_PERCENTAGE ?? "0";
                $resultArray[] = $result;
            }
            $heading = ["Place", "User ID", "Username", "Email", "Contact", "Prize Amount", "Percentage"];

            $fileName = $fileName ?? "tournamentUsersRanks" . date("Y-m-d-H-i-s") . ".xlsx";

            return Excel::download(new CollectionExport($resultArray, $heading), $fileName);
        } else {
            return back()->with('info', "Empty tournament rank list.");
        }
    }

    public function userRegistered(Request $request, $tournamentId)
    {
        $tournamentId = \decrypt($tournamentId);

        $perPage = config('poker_config.paginate.per_page');

        $tournament = $this->filterTournamentConditionalQuery(
            [],
            [
                'select' => [
                    "t.TOURNAMENT_ID",
                    "t.TOURNAMENT_NAME",
                    "t.TOURNAMENT_DESC",
                    "t.BUYIN",
                    "t.ENTRY_FEE",
                    "t.TOTAL_REBUY_COUNT",
                    "t.TOTAL_ADDON_COUNT",
                    "m.MINIGAMES_TYPE_NAME",
                    "t.TOURNAMENT_START_TIME",
                    "t.TOURNAMENT_END_TIME",
                    "t.CATEGORY",
                    "tt.DESCRIPTION as TOURNAMENT_TYPE_DESCRIPTION",
                    "tstc.CATEGORY_DESC",
                ],
                'isDefaultTournamentTypeId' => false
            ]
        );
        $tournament->leftJoin('tournament_sub_tab_categories AS tstc', 'tstc.PK_TOURNAMENT_SUBTAB_CATEGORY_ID', '=', 't.CATEGORY');
        $tournament = $tournament->where('t.TOURNAMENT_ID', $tournamentId)->first();

        $displayTab = "";
        $displayTab .= $tournament->getTournamentDisplayTabName();
        $displayTab .= !empty($tournament->CATEGORY_DESC) ? " ($tournament->CATEGORY_DESC)" : '';

        $tournament->DISPLAY_TAB = $displayTab;

        $data['tournament'] = $tournament;
        
        $data['tournament_reg_users'] = $this->getTournamentRegisteredUsers($tournamentId, null, true)->paginate($perPage);
        
        return view('bo.views.tournaments.manage-tourneys.user_registered', $data);
    }

    public function userRegisteredExport(Request $request, $tournamentId)
    {
        $tournamentId = \decrypt($tournamentId);
        $tournament_reg_users =  $this->getTournamentRegisteredUsers($tournamentId);
        
        if (count($tournament_reg_users) > 0) {
            $resultArray = [];
            $i = 0;
            foreach ($tournament_reg_users as $key => $item) {
                $result = [];
                $result['S_NO'] = ++$i;
                $result['USER_ID'] = $item->USER_ID;
                $result['USERNAME'] = $item->PLAYER_NAME;
                $result['EMAIL_ID'] = $item->EMAIL_ID;
                $result['CONTACT'] = "$item->CONTACT";
                $result['SOURCE'] = $item->TICKET_SOURCE;
                $result['REASON'] = $item->TICKET_REASON;
                $result['Rebuy'] = "$item->total_rebuy_user";
                $result['Addon'] = "$item->total_addon_user";
                $result['REGISTERED_DATE'] = $item->REGISTERED_DATE;
                $resultArray[] = $result;
            }
            
            $heading = ["S.No", "User ID", "Username", "Email", "Contact", "Source", "Reason", "Rebuy", "Addon", "Registered On"];

            $fileName = $fileName ?? "tournamentUsersRegistered" . date("Y-m-d-H-i-s") . ".xlsx";

            return Excel::download(new CollectionExport($resultArray, $heading), $fileName);
        } else {
            return back()->with('info', "Empty tournament register users list.");
        }
    }

    public function StuckResumeTournament(Request $request, $tournamentId)
    {
        try {
            $tournamentId = \decrypt($tournamentId);

            $tournament = Tournament::find($tournamentId);

            $LAST_STUCK_STATUS = $tournament->STUCK_STATUS;
            $LAST_STUCK_RESUME = $tournament->STUCK_RESUME;
            $tournament->STUCK_STATUS = 1;
            $tournament->STUCK_RESUME = 1;

            $tournament->save();

            sleep(15);

            DB::beginTransaction();
            $tournament->TOURNAMENT_STATUS = 6;
            $tournament->save();

            $calcLateRegistration = $this->getLateRegistrationTimeInfo($tournament);

            $tournamentReplicate = $tournament->replicate();
            $tournamentReplicate->REGISTER_START_TIME = DB::raw('NOW()');
            $tournamentReplicate->REGISTER_END_TIME = DB::raw('NOW() + INTERVAL 01 MINUTE');
            $tournamentReplicate->TOURNAMENT_START_TIME = DB::raw('NOW() + INTERVAL 01 MINUTE');
            $tournamentReplicate->IS_ACTIVE = 1;
            $tournamentReplicate->TOURNAMENT_STATUS = 1;
            $tournamentReplicate->LATE_REGISTRATION_END_TIME = $calcLateRegistration;
            $tournamentReplicate->STUCK_STATUS = 0;
            $tournamentReplicate->STUCK_RESUME = 1;
            $tournamentReplicate->STUCK_TOURNAMENT_ID = $tournamentId;

            if ($tournamentReplicate->save()) {
                TournamentLevel::where('TOURNAMENT_ID', $tournamentId)->update(['TOURNAMENT_ID' => $tournamentReplicate->TOURNAMENT_ID]);
                TournamentWinnerLevel::where('TOURNAMENT_ID', $tournamentId)->update(['TOURNAMENT_ID' => $tournamentReplicate->TOURNAMENT_ID]);
            } else {
                DB::rollBack();
                return back()->with('error', "Something went wrong");
            }

            DB::commit();
            return back()->with('success', "The Tournament is Stuck & Resume Successfully");
        } catch (\Exception $e) {
            DB::rollBack();
            try {
                $tournament->STUCK_STATUS = $LAST_STUCK_STATUS;
                $tournament->STUCK_RESUME = $LAST_STUCK_RESUME;

                $tournament->save();
            } catch (\Exception $ex) {
            }
            return back()->with('error', "Something went wrong");
        }
    }

    public function cancelTournament(Request $request, $tournamentId)
    {
        $tournamentId = \decrypt($tournamentId);
        if (Tournament::where('TOURNAMENT_ID', $tournamentId)->update(['IS_ACTIVE' => 0])) {
            return back()->with('success', "The Tournament is Cancelled Successfully");
        } else {
            return back()->with('error', "Something went wrong");
        }
    }
}
