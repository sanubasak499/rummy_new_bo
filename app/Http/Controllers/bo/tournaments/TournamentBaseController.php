<?php

namespace App\Http\Controllers\bo\tournaments;

use App\Http\Controllers\bo\BaseController;
use App\Models\Tournament;
use App\Traits\Models\CommonGameQueries;
use Illuminate\Http\Request;
use App\Models\TournamentLevel;
use App\Models\TournamentWalletTransaction;
use DateTime;
use Illuminate\Support\Facades\DB;

/**
 * Tournamet Base Controller
 * 
 * This Controller used as the Base Controller
 * for the Tournament Related Module,
 * To use Common function, for the related modules
 * like Create Tournament, Add Recurrence Tournaments, etc.
 *  
 * Class TournamentBaseController
 * 
 * @category    Manage Tournament
 * @package     Tournament
 * @copyright   PokerBaazi
 * @author      Nitin Sharma<nitin.sharma@moonshinetechnology.com>
 * Created At:  03/02/2020
 */

class TournamentBaseController extends BaseController
{
    /**
     * CommonGameQueries traits is uses for common games related queries
     */
    use CommonGameQueries;

    const TOURNAMENT_REBUY_IN = 101;
    const TOURNAMENT_ADD_ON = 103;

    public function __construct()
    {
        parent::__construct();
    }

    public function tournamentBlindStructure(Request $request)
    {
        try {
            if (!empty($request->BLIND_STRUCTURE_ID)) {
                $params['BLIND_STRUCTURE_ID'] = $request->BLIND_STRUCTURE_ID;
                $getTournamentBlindInfo = $this->getTournamentBlindInfo($params);
                return response()->json(['status' => '200', 'message' => 'success', 'data' => $getTournamentBlindInfo], 200);
            } else {
                return response()->json(['status' => '400', 'message' => 'params missing'], 400);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => '500', 'message' => $e->getMessage()], 500);
        }
    }

    public function tournamentBlindLevelRebuy(Request $request)
    {
        try {
            if (!empty($request->BLIND_STRUCTURE_ID)) {
                $params['BLIND_STRUCTURE_ID'] = $request->BLIND_STRUCTURE_ID;
                if (empty($request->TOURNAMENT_LEVEL)) {
                    $params['TOURNAMENT_LEVEL'] = ['TOURNAMENT_LEVEL', '>', 0];
                } else {
                    $params['TOURNAMENT_LEVEL'] = ['TOURNAMENT_LEVEL', '>', $request->TOURNAMENT_LEVEL];
                }

                $getTournamentBlindInfo = $this->getTournamentBlindInfo($params);
                return response()->json(['status' => '200', 'message' => 'success', 'data' => $getTournamentBlindInfo], 200);
            } else {
                return response()->json(['status' => '400', 'message' => 'params missing'], 400);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => '500', 'message' => $e->getMessage()], 500);
        }
    }
    public function countTournamentBlindInfo(Request $request)
    {
        try {
            // $request->validate([
            //     'BLIND_STRUCTURE_ID'=>'required',
            //     'TOURNAMENT_LEVEL'=> 'required'
            // ]);

            $params['BLIND_STRUCTURE_ID'] = $request->BLIND_STRUCTURE_ID;
            $params['TOURNAMENT_LEVEL'] = $request->TOURNAMENT_LEVEL;

            $getTournamentBlindLevelCount = $this->getTournamentBlindLevelCount($params);
            $remaining_level = $getTournamentBlindLevelCount - $params['TOURNAMENT_LEVEL'];

            return response()->json(['status' => '200', 'message' => 'success', 'data' => $remaining_level], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => '500', 'message' => $e->getMessage()], 500);
        }
    }

    public function getTournamentsForSatellite()
    {
        $request = app(Request::class);

        $params = [
            'NOT_TOURNAMENT_TYPE_ID' => [9],
            'TOURNAMENT_START_TIME' => "greaterThenNow"
        ];
        $data = $this->getTournaments($params);
        if ($request->ajax()) {
            return response()->json(['status' => '200', 'message' => 'success', 'data' => $data], 200);
        }
        return $data;
    }

    public function getTournamentsForMultiDayChild()
    {
        $request = app(Request::class);
        $params = [
            'TOURNAMENT_TYPE_ID' => [9, 12],
            'TOURNAMENT_START_TIME' => "greaterThenNow"
        ];
        $data = $this->getTournaments($params);
        if ($request->ajax()) {
            return response()->json(['status' => '200', 'message' => 'success', 'data' => $data], 200);
        }
        return $data;
    }

    public function insertTournamentBlindStructures($TOURNAMENT_ID, $TOURNAMENT_LEVEL, $BLIND_STRUCTURE_ID, $LEVEL_PERIOD, $MINI_GAME_TYPE_ID, $TOURNAMENT_TYPE_ID)
    {
        $blindStructuresAfterLevel = $this->getBlindStructuresAfterLevel($TOURNAMENT_LEVEL, $BLIND_STRUCTURE_ID);

        $stake = $TOURNAMENT_LEVEL - 1;
        $LEVEL_BREAK_TIME = 0;
        $i = 1;
        $tournamentLevelArray = [];
        $result = false;

        foreach ($blindStructuresAfterLevel as $levels) {
            $STAKE_LEVELS = $TOURNAMENT_TYPE_ID == 9 ? $stake + $i : $i;
            $tournamentLevel = [];
            $tournamentLevel['TOURNAMENT_ID']     = $TOURNAMENT_ID;
            $tournamentLevel['MINIGAMES_TYPE_ID'] = $MINI_GAME_TYPE_ID;
            $tournamentLevel['STAKE_LEVELS']      = $STAKE_LEVELS;
            $tournamentLevel['SMALL_BLIND']       = $levels->SMALL_BLIND;
            $tournamentLevel['BIG_BLIND']         = $levels->BIG_BLIND;
            $tournamentLevel['ANTE']              = $levels->ANTE;
            $tournamentLevel['LEVEL_BREAK_TIME']  = $LEVEL_BREAK_TIME;
            $tournamentLevel['TIME_BANK']         = $levels->TIME_BANK;
            $tournamentLevel['LEVEL_PERIOD']      = $LEVEL_PERIOD;

            $tournamentLevelArray[] = $tournamentLevel;
            $i++;
        }
        if (count($tournamentLevelArray) > 0) {
            $result = TournamentLevel::insert($tournamentLevelArray);
        }
        return $result;
    }

    public function tournamentInfo(Request $request, $tournamentId)
    {
        $tournament = Tournament::query();
        $tournament->from(app(Tournament::class)->getTable() . " AS t");
        $tournament->select([
            "t.TOURNAMENT_ID", "t.TOURNAMENT_NAME", "t.TOURNAMENT_DESC", "m.MINIGAMES_TYPE_NAME", "ts.SERVER_NAME",
            "tt.DESCRIPTION as TOURNAMENT_TYPE_DESCRIPTION",  "tl.TOURNAMENT_LIMIT_NAME as TOURNAMENT_LIMIT_NAME", "tl.DESCRIPTION as TOURNAMENT_LIMIT",
            "tf.TOURNAMENT_NAME as PARENT_TOURNAMENT", "tf.TOURNAMENT_ID as PARENT_TOURNAMENT_ID",
            "t.WIN_THE_BUTTON", "t.FINAL_TABLE_STOP", "t.FEATURED", "t.PRIVATE_TABLE",
            "t.TOURNAMENT_TYPE_ID", "t.TIMER_TOURNAMENT", "t.FK_PARENT_TOURNAMENT_ID", "t.CATEGORY", "tstc.CATEGORY_DESC",

            "t.REGISTER_START_TIME", "t.REGISTER_END_TIME", "t.TOURNAMENT_START_TIME", "t.TOURNAMENT_END_TIME",
            "t.LATE_REGISTRATION_ALLOW", "t.LATE_REGISTRATION_END_TIME",

            "t.PLAYER_PER_TABLE", "t.T_MIN_PLAYERS", "t.T_MAX_PLAYERS",

            "ct.NAME as COIN_TYPE_NAME", "t.COIN_TYPE_ID", "t.PROMO_BALANCE_ALLOW", "t.DEPOSIT_BALANCE_ALLOW", "t.WIN_BALANCE_ALLOW",
            "t.BUYIN", "t.ENTRY_FEE", "t.BOUNTY_AMOUNT", "t.BOUNTY_ENTRY_FEE", "t.PROGRESSIVE_BOUNTY_PERCENTAGE",

            "tbs.DESCRIPTION as TOURNAMENT_BLIND_STRUCTURE_NAME", "t.SMALL_BLIND", "t.BIG_BLIND", "t.TOURNAMENT_CHIPS", "t.TIMER_TOURNAMENT_END_TIME",

            "t.PLAYER_HAND_TIME", "t.DISCONNECT_TIME", "t.EXTRA_TIME", "t.LOBBY_DISPLAY_INTERVAL", "t.PLAYER_MAX_EXTRATIME", "t.ADDITIONAL_EXTRATIME_LEVEL_INTERVAL", "t.ADDITIONAL_EXTRATIME",

            "t.IS_REBUY", "t.REBUY_CHIPS", "t.REBUY_ELIGIBLE_CHIPS", "t.REBUY_END_TIME", "t.REBUY_COUNT", "t.REBUY_IN", "t.REBUY_ENTRY_FEE", "t.DOUBLE_REBUYIN",

            "t.IS_ADDON", "t.ADDON_CHIPS", "t.ADDON_BREAK_TIME", "t.ADDON_AMOUNT", "t.ADDON_ENTRY_FEE",

            "t.NO_OF_WINNERS",

            "t.PRIZE_STRUCTURE_ID", "t.PRIZE_STRUCTURE_TYPE_ID", "dpst.DESCRIPTION as PRIZE_STRUCTURE_TYPE_NAME", "t.PRIZE_BALANCE_TYPE", "t.FIXED_PRIZE",
            "t.TICKET_VALUE", "t.GUARENTIED_PRIZE", "t.MULTIDAY_PLAYER_PERCENTAGE",

            "t.REGISTRATED_PLAYER_COUNT", "t.TOURNAMENT_STATUS",
        ]);

        // get PARENT_TOURNAMENT_ID
        $tournament->leftJoin('tournament AS tf', 'tf.TOURNAMENT_ID', '=', 't.FK_PARENT_TOURNAMENT_ID');
        // get COIN_TYPE_NAME
        $tournament->leftJoin('coin_type AS ct', 'ct.COIN_TYPE_ID', '=', 't.COIN_TYPE_ID');
        //get PRIZE_STRUCTURE_TYPE_NAME
        $tournament->leftJoin('default_prize_structure_types AS dpst', 'dpst.PRIZE_STRUCTURE_TYPE_ID', '=', 't.PRIZE_STRUCTURE_TYPE_ID');
        //get SERVER_NAME
        $tournament->leftJoin('tournament_servers AS ts', 'ts.SERVER_ID', '=', 't.SERVER_ID');
        // get CATEGORY_DESC
        $tournament->leftJoin('tournament_sub_tab_categories AS tstc', 'tstc.PK_TOURNAMENT_SUBTAB_CATEGORY_ID', '=', 't.CATEGORY');
        // get MINIGAMES_TYPE_NAME
        $tournament->join('minigames_type AS m', 'm.MINIGAMES_TYPE_ID', '=', 't.MINI_GAME_TYPE_ID');
        // get TOURNAMENT_TYPE_DESCRIPTION
        $tournament->join('tournament_type AS tt', 'tt.TOURNAMENT_TYPE_ID', '=', 't.TOURNAMENT_TYPE_ID');
        // get TOURNAMENT_LIMIT_NAME
        $tournament->leftJoin('tournament_limit AS tl', 'tl.TOURNAMENT_LIMIT_ID', '=', 't.TOURNAMENT_LIMIT_ID');
        // get tournament_blind_structure
        $tournament->leftJoin('tournament_blind_structure AS tbs', 'tbs.BLIND_STRUCTURE_ID', '=', 't.BLIND_STRUCTURE_ID');

        $tournament = $tournament->where('t.TOURNAMENT_ID', $tournamentId)->first();

        $tournament = $this->reverseEngineeringTournament($tournament);

        $displayTab = "";
        $displayTab .= $tournament->getTournamentDisplayTabName();
        $displayTab .= !empty($tournament->CATEGORY_DESC) ? " ($tournament->CATEGORY_DESC)" : '';

        $tournament->DISPLAY_TAB = $displayTab;

        $statusList = ["4" => "Normal", "9" => "Multi-Day Main", "10" => "Normal Bounty", "11" => "Progressive Bounty", "12" => "Offline", "1" => "Sit & Go"];

        $TOURNAMENT_TYPE_ID = $tournament->TOURNAMENT_TYPE_ID;
        $tournament->TOURNAMENT_TYPE_DESCRIPTION = array_key_exists($TOURNAMENT_TYPE_ID, $statusList) ? "$tournament->TOURNAMENT_TYPE_DESCRIPTION ($statusList[$TOURNAMENT_TYPE_ID])" : $tournament->TOURNAMENT_TYPE_DESCRIPTION;

        return view('bo.views.tournaments.manage-tourneys.show', ['tournament' => $tournament]);
    }

    public function tournamentBlindStructureInfo(Request $request, $tournamentId)
    {
        $tournament = $this->filterTournamentConditionalQuery(
            [],
            [
                'select' => [
                    "t.TOURNAMENT_NAME",
                    "t.BUYIN",
                    "t.ENTRY_FEE",
                    "t.TOURNAMENT_START_TIME",
                    "t.TOURNAMENT_END_TIME",
                    "tt.DESCRIPTION as TOURNAMENT_TYPE_DESCRIPTION",
                    "m.MINIGAMES_TYPE_NAME",
                ],
                'isDefaultTournamentTypeId' => false
            ]
        );

        $tournament = $tournament->where('t.TOURNAMENT_ID', $tournamentId)->first();

        $data['tournament'] = $tournament;

        $data['bindStructure'] = DB::select("call sp_fetch_tournament_blind_sturcture(?)", [$tournamentId]);

        return response()->json(['status' => 200, 'message' => 'success', 'data' => $data]);
    }
    public function tournamentPrizeInfo(Request $request, $tournamentId)
    {
        $params = [
            'TOURNAMENT_ID' => $tournamentId
        ];
        $select = [
            "TOURNAMENT_ID", "REGISTRATED_PLAYER_COUNT", "TOURNAMENT_STATUS", "GUARENTIED_PRIZE", "BUYIN", "REBUY_IN", "IS_ADDON", "COIN_TYPE_ID",
            "TOTAL_REBUY_COUNT", "TOTAL_ADDON_COUNT", "PRIZE_TYPE", "TICKET_VALUE", "PRIZE_STRUCTURE_ID", "PRIZE_STRUCTURE_TYPE_ID",
            "ADDON_AMOUNT", "FIXED_PRIZE", "IS_REBUY", "DOUBLE_REBUYIN", "TOURNAMENT_TYPE_ID"
        ];
        $tournament = $this->getTournamentInfo($params, $select);

        if ($tournament->TOURNAMENT_STATUS === 6) {
            $TOURNAMENT_TYPE_ID = 4;
            $tournamentPrize = DB::select("SELECT pr.PLAYERS_MIN, pr.PLAYERS_MAX, rr.RANK_MIN, rr.RANK_MAX, pp.PRIZE_PERCENTAGE FROM prize_payout pp INNER JOIN payout_players_range pr on pr.PAYOUT_PLAYERS_RANGE_ID = pp.PAYOUT_PLAYERS_RANGE_ID and pr.TOURNAMENT_TYPE_ID = pp.TOURNAMENT_TYPE_ID INNER JOIN payout_rank_range rr on rr.PAYOUT_RANK_RANGE_ID = pp.PAYOUT_RANK_RANGE_ID and rr.TOURNAMENT_TYPE_ID = pp.TOURNAMENT_TYPE_ID WHERE pp.TOURNAMENT_TYPE_ID = $TOURNAMENT_TYPE_ID");
            $tournamentPrize = collect($tournamentPrize);
            $data['tournamentPrize'] = $tournamentPrize->groupBy('RANK_MIN', 'RANK_MAX');
        } else {
            /**
             * output of below stored procedure is in array 
             * [RANK, PRIZE_VALUE, PRIZE_TYPE, WINNER_PERCENTAGE, PRIZE_TOURNAMENT_ID, PRIZE_TOURNAMENT_NAME]
             */
            $data['tournamentPrize'] = DB::select("call sp_winner_levels('?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?','?')", [
                $tournamentId, $tournament->REGISTRATED_PLAYER_COUNT, $tournament->TOURNAMENT_STATUS, $tournament->GUARENTIED_PRIZE,
                $tournament->BUYIN, $tournament->REBUY_IN,
                $tournament->ADDON_AMOUNT, // new 
                $tournament->COIN_TYPE_ID, $tournament->TOTAL_REBUY_COUNT, $tournament->TOTAL_ADDON_COUNT, $tournament->PRIZE_TYPE,
                $tournament->TICKET_VALUE, $tournament->PRIZE_STRUCTURE_ID, 0, 0, $tournament->PRIZE_STRUCTURE_TYPE_ID,
                $tournament->FIXED_PRIZE, // new
                $tournament->IS_REBUY, // new
                $tournament->DOUBLE_REBUYIN, // new
                $tournament->IS_ADDON, // re position
                $tournament->TOURNAMENT_TYPE_ID, // new
            ]);
        }

        return response()->json(['status' => 200, 'message' => 'success', 'data' => $data]);
    }

    /**
     * @param mixed $tournament the tournament param instanceof Tournament Model
     *      or numberic id of tournament by which can select from database
     */
    public function foundTournamentSubTypeId($tournament)
    {
        $tournament = $this->getTournamentModelObject($tournament);

        $TOURNAMENT_SUB_TYPE_ID = 1;
        if ($tournament->FK_PARENT_TOURNAMENT_ID == "") {
            $TOURNAMENT_SUB_TYPE_ID = $tournament->TIMER_TOURNAMENT == 1 && !empty($tournament->TIMER_TOURNAMENT_END_TIME) ? 4  : 1;
        } else {
            $TOURNAMENT_SUB_TYPE_ID = $tournament->TIMER_TOURNAMENT == 1 &&  !empty($tournament->FK_PARENT_TOURNAMENT_ID) ? 3  : 2;
        }

        return $TOURNAMENT_SUB_TYPE_ID;
    }

    public function getTournamentWinnerMapping($tournament)
    {
        $tournamentWinnerLevel = $this->getTournamentWinnerLevel($tournament->TOURNAMENT_ID);

        if ($tournament->TOURNAMENT_SUB_TYPE_ID == 2) {
            if ($tournament->TICKET_VALUE == 0) {
                $SATELLITES_GUARANTEED_PLACES_PAID = $tournament->GUARENTIED_PRIZE;
            } else {
                $SATELLITES_GUARANTEED_PLACES_PAID = $tournament->GUARENTIED_PRIZE / $tournament->TICKET_VALUE;
            }
            $tournament->SATELLITES_GUARANTEED_PLACES_PAID = (int) $SATELLITES_GUARANTEED_PLACES_PAID;
        }

        if ($tournament->PRIZE_STRUCTURE_ID == 1) {
            $tournament->NO_OF_WINNERS_CUSTOM = $tournament->NO_OF_WINNERS;
            $WINNER_PERCENTAGE = [];

            foreach ($tournamentWinnerLevel as $key => $value) {
                $WINNER_PERCENTAGE[$value->RANK - 1] = $value->WINNER_PERCENTAGE;
            }
            $tournament->WINNER_PERCENTAGE = $WINNER_PERCENTAGE;
        } elseif ($tournament->PRIZE_STRUCTURE_ID == 4) {
            $tournament->NO_OF_WINNERS_CUSTOM_MIX = $tournament->NO_OF_WINNERS;
            $CUSTOM_MIX_PRIZE_TYPE = $PRIZE_VALUE = $PRIZE_TOURNAMENT_NAME = [];

            foreach ($tournamentWinnerLevel as $key => $value) {
                $CUSTOM_MIX_PRIZE_TYPE[$value->RANK - 1] = $value->PRIZE_TYPE;
                $PRIZE_VALUE[$value->RANK - 1] = $value->PRIZE_VALUE;
                $PRIZE_TOURNAMENT_NAME[$value->RANK - 1] = $value->PRIZE_TOURNAMENT_NAME;
            }
            $tournament->CUSTOM_MIX_PRIZE_TYPE = $CUSTOM_MIX_PRIZE_TYPE;
            $tournament->PRIZE_VALUE = $PRIZE_VALUE;
            $tournament->PRIZE_TOURNAMENT_NAME = $PRIZE_TOURNAMENT_NAME;
        }

        return $tournament;
    }

    public function reverseEngineeringTournament($tournament)
    {
        $tournament = $this->getTournamentModelObject($tournament);

        $TOURNAMENT_SUB_TYPE_ID = $this->foundTournamentSubTypeId($tournament);
        $tournament->TOURNAMENT_SUB_TYPE_ID = $TOURNAMENT_SUB_TYPE_ID;

        $tournament = $this->getTournamentWinnerMapping($tournament);

        $tournamentLevelPeriodTournamentLevel = $this->getTournamentBlindStructureInfo($tournament->TOURNAMENT_ID);
        $tournament->LEVEL_PERIOD = $tournamentLevelPeriodTournamentLevel->LEVEL_PERIOD;
        $tournament->TOURNAMENT_LEVEL = $tournamentLevelPeriodTournamentLevel->TOURNAMENT_LEVEL;

        if ($tournament->LOBBY_ID == 9 && $tournament->CATEGORY == 1) {
            $tournament->CATEGORY = 9;
        }

        if ($tournament->IS_REBUY == 1 && $tournament->IS_ADDON == 1) {
            $tournament->REBUY_ADDON_RE_ENTRY = 1;
        } elseif ($tournament->IS_REBUY == 1 && $tournament->IS_ADDON == 0) {
            $tournament->REBUY_ADDON_RE_ENTRY = 2;
        } else {
            $tournament->REBUY_ADDON_RE_ENTRY = 0;
        }

        if ($tournament->TOURNAMENT_SUB_TYPE_ID == 2) {
            $tournament->TOURNAMENT_PARENT_SATELLITE_ID = $tournament->FK_PARENT_TOURNAMENT_ID;
        } elseif ($tournament->TOURNAMENT_SUB_TYPE_ID == 3) {
            $tournament->TOURNAMENT_PARENT_MULTI_DAY_ID = $tournament->FK_PARENT_TOURNAMENT_ID;
        }

        $tournament->TOURNAMENT_CHIPS               = (int) $tournament->TOURNAMENT_CHIPS;
        $tournament->ENTRY_FEE                      = (int) $tournament->ENTRY_FEE;
        $tournament->REBUY_CHIPS                    = (int) $tournament->REBUY_CHIPS;
        $tournament->ADDON_CHIPS                    = (int) $tournament->ADDON_CHIPS;
        $tournament->ADDON_AMOUNT                   = (int) $tournament->ADDON_AMOUNT;
        $tournament->MULTIDAY_PLAYER_PERCENTAGE     = (int) $tournament->MULTIDAY_PLAYER_PERCENTAGE;
        $tournament->PROGRESSIVE_BOUNTY_PERCENTAGE  = (int) $tournament->PROGRESSIVE_BOUNTY_PERCENTAGE;
        $tournament->GUARENTIED_PRIZE               = (int) $tournament->GUARENTIED_PRIZE;

        return $tournament;
    }

    public function getLateRegistrationTimeInfo($tournament)
    {
        $tournament = $this->getTournamentModelObject($tournament);

        $stucktime = $tournament->TOURNAMENT_START_TIME;
        $lateRegTime = $tournament->LATE_REGISTRATION_END_TIME;
        $getstucktime = date('Y-m-d H:i:s', strtotime($stucktime));
        $currentTime = date('Y-m-d H:i:s');
        $stuckTournamentTime = new DateTime($getstucktime);
        $currentstuckTime = new DateTime($currentTime);
        $interval = $currentstuckTime->diff($stuckTournamentTime);
        $getDiffInMin = $interval->format('%H:%i:%s');
        $splitData = explode(':', $getDiffInMin);
        $hours = $splitData[0];
        if ($hours != '') {
            $minutesInfo = $this->convertHourstoMin($hours);
        }
        $remainingMin = $splitData[1];
        if ($hours != '') {
            $totalminForLateReg = $minutesInfo + $remainingMin;
        } else {
            $totalminForLateReg = $remainingMin;
        }
        $totalTimeForLateRegistration = $lateRegTime - $totalminForLateReg;
        if ($totalTimeForLateRegistration <= 0) {
            $tournamentLateRegTime = 0;
        } else {
            $tournamentLateRegTime = $totalTimeForLateRegistration;
        }
        return $tournamentLateRegTime;
    }

    public function convertHourstoMin($hours)
    {
        $minutes = 0;
        if (strpos($hours, ':') !== false) {
            // Split hours and minutes. 
            list($hours, $minutes) = explode(':', $hours);
        }
        return  $hours * 60 + $minutes;
    }
}
