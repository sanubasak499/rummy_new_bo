<?php

namespace App\Http\Controllers\bo\reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterTransactionHistoryRewardcoins;
use Carbon\Carbon;
use DB;


/**
 * Author: Nitesh Kumar Jha
 * Purpose: In  this module get coin Summary and reward point summery data.
 * Created Date: 09-10-2020
 */

class CoinsSummaryController extends Controller
{
    /** view coins summary page */

    public function index()
    {

        return view('bo.views.reports.coinsSummary');
    }

    public function coinsSummary(Request $request)
    {
        $perPage = config('poker_config.paginate.per_page');
        if (!$request->isMethod('post')) {
            return view('bo.views.reports.coinsSummary');
        }
        $page = request()->page;

        $transactionData = MasterTransactionHistoryRewardcoins::from(app(MasterTransactionHistoryRewardcoins::class)->getTable() . " as mtr")
            ->select(
                'u.USERNAME',
                'up.USER_TOT_BALANCE as USER_TOT_BALANCE',
                'up.COIN_TYPE_ID',
                DB::raw('sum(mtr.TRANSACTION_AMOUNT)TRANSACTION_AMOUNT'),
                DB::raw('(SELECT 
        SUM(TRANSACTION_AMOUNT) AS TransactionAmount
    FROM
        master_transaction_history_rewardpoints
    WHERE
        user_id = u.USER_ID
            AND TRANSACTION_TYPE_ID = 113) TransactionAmount'),
                DB::raw('(SELECT 
            SUM(RP_CONVERTED) AS RPCONVERTED
        FROM
            reward_points_conversion_history
        WHERE
            user_id = u.USER_ID) RPCONVERTED'),
                DB::raw('(SELECT 
            USER_TOT_BALANCE AS userTotBalance
        FROM
            user_points
        WHERE
            user_id = u.USER_ID
                AND COIN_TYPE_ID = 12) userTotBalance')
            )
            ->join('user as u', 'u.USER_ID', '=', 'mtr.USER_ID')
            ->join('user_points as up', 'up.USER_ID', '=', 'mtr.USER_ID')

            ->when($request->search_by == "USERNAME", function ($query) use ($request) {
                return $query->where('u.USERNAME', 'LIKE', "%{$request->search_by_value}%");
            })
            ->when($request->search_by == "EMAIL_ID", function ($query) use ($request) {
                return $query->where('u.EMAIL_ID', 'LIKE', "%{$request->search_by_value}%");
            })
            ->when($request->search_by == "CONTACT", function ($query) use ($request) {
                return $query->where('u.CONTACT', 'LIKE', "%{$request->search_by_value}%");
            })
            ->when(!empty($request->START_DATE_TIME), function ($query) use ($request) {
                $dateFrom = Carbon::parse($request->START_DATE_TIME)->format('Y-m-d H:i:s');
                return $query->whereDate('mtr.TRANSACTION_DATE', ">=", $dateFrom);
            })
            ->when(!empty($request->END_DATE_TIME), function ($query) use ($request) {
                $dateTo = Carbon::parse($request->END_DATE_TIME)->format('Y-m-d H:i:s');
                return $query->whereDate('mtr.TRANSACTION_DATE', "<=", $dateTo);
            })

            ->where('mtr.TRANSACTION_TYPE_ID', 112)
            ->where('mtr.TRANSACTION_STATUS_ID', 236)
            ->where('up.COIN_TYPE_ID', 11)
            ->groupBy('mtr.USER_ID')
            ->orderBy('mtr.TRANSACTION_AMOUNT', 'DESC')
            ->paginate($perPage, ['*'], 'page', $page);
        $params = $request->all();
        $params['page'] = $page;

        $USER_TOT_BALANCE = $transactionData->sum('TRANSACTION_AMOUNT');

        return view('bo.views.reports.coinsSummary', ['transactionData' => $transactionData, 'params' => $params, 'USER_TOT_BALANCE' => $USER_TOT_BALANCE]);
    }
}
