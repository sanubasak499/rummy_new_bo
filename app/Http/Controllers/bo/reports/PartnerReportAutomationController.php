<?php
namespace App\Http\Controllers\bo\reports;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use stdClass;
use DB;
use Carbon\Carbon;
use \PDF;
use App;
use Validator;
use Mail;
use Storage;

class PartnerReportAutomationController extends Controller 
{
	public function __construct() {
		parent::__construct();
		$this->poker_partner_transaction_type_id_for_debit = config('poker_config.transaction_type_id.debit_id');
		$this->poker_partner_transaction_type_id_for_credit = config('poker_config.transaction_type_id.credit_id');
		$this->poker_partner = config('poker_config.poker_partner.partner_id');
		//$this->poker_partner = 10001;
	}

	function count_digit($number) {
		return strlen($number);
	}

	function divider($number_of_digits) {
		$tens="1";

	  if($number_of_digits>8)
		return 10000000;

	  while(($number_of_digits-1)>0)
	  {
		$tens.="0";
		$number_of_digits--;
	  }
	  return $tens;
	}
	public function numberExtChange($val){
		//function call
		$num = floor($val);
		$ext="";//thousand,lac, crore
		
		$number_of_digits = $this->count_digit($num); //this is call :)
		if($number_of_digits>3)
		{
			if($number_of_digits%2!=0){
				$divider=$this->divider($number_of_digits-1);
			}
			else{
				$divider=$this->divider($number_of_digits);
			}
		}
		else{
			$divider=1;
		}
		
		$fraction=$num/$divider;
		$fraction=number_format($fraction,2);
		if($number_of_digits==4 ||$number_of_digits==5)
			$ext="K";
		if($number_of_digits==6 ||$number_of_digits==7)
			$ext="Lacs";
		if($number_of_digits==8 ||$number_of_digits>=9)
			$ext="Crores";
		
		return $fraction." ".$ext;
	}
	public function index(Request $request){
		
		$date = date('Y-m-d', strtotime("-1 day"));
		$date_from =Carbon::parse($date)->setTime(0, 0, 0)->toDateTimeString();
		$date_to =Carbon::parse($date)->setTime(23, 59, 59)->toDateTimeString();

		//$date_from ='2020-05-01 00:00:00';
		//$date_to='2020-11-03 23:59:59';
		
		#Poker Partner Daily Active Users,DAUs (logins) :-
		$userDAULogin = $this->userDAUsLogin($date_from,$date_to);
		$userDAULogins = $userDAULogin[0]->Daus;
		
		#poker partner signup users
		$signUp = $this->signUpLogins($date_from,$date_to);
		$signUps = $signUp[0]->Registrations;
		
		#Poker Partner Transferred from PB wallet user count and amount
		$transferredFromPbWallet = $this->transferredFromPBWallet($date_from,$date_to);
		$fromPbWalletAmount = $transferredFromPbWallet[0]->Transferred_from_PB_Wallet_amount?$this->numberExtChange($transferredFromPbWallet[0]->Transferred_from_PB_Wallet_amount):'0';
		$fromPbWalletUserCount = $transferredFromPbWallet[0]->Transferred_from_PB_Wallet_user_count;

		#Poker Partner Transferred to PB wallet user count and amount
		$transferredToPbWallet = $this->transferredToPBWallet($date_from,$date_to);
		$toPbWalletAmount = $transferredToPbWallet[0]->Transferred_to_PB_Wallet_amount?$this->numberExtChange($transferredToPbWallet[0]->Transferred_to_PB_Wallet_amount):'0';
		$toPbWalletUserCount = $transferredToPbWallet[0]->Transferred_to_PB_Wallet_user_count;

		#Poker Partner First Time Transferred To PB Wallet User Count
		$firstTimetoPBWallet = $this->firstTimeTransferredToPBWalletCount($date_from,$date_to);
		$firstTimetoPBWalletUserCount = $firstTimetoPBWallet[0]->firast_time_Transferred_to_PB_Wallet_user_count;

		#Poker Partner First Time Transferred Back To BB Wallet User Count
		$firstTimeBacktoPBWallet = $this->firstTimeTransferredBackToBBWalletCount($date_from,$date_to);
		$firstTimeBacktoBBWalletUserCount = $firstTimeBacktoPBWallet[0]->firast_time_Transferred_back_to_BB_Wallet_user_count;

		#Poker Partner Unique Wagering users (Playing) Count
		$wageringUserPlayingCount = $this->wageringUserPlayingCount($date_from,$date_to);
		$uniqueWageringUserPlayingCount = $wageringUserPlayingCount[0]->Unique_Wagering_users_Playing;

		#Poker Partner Unique Wagering users (Playing) Count Non Depositer
		$wageringUserPlayingCountNonDeposit = $this->wageringUserPlayingCountNonDeposit($date_from,$date_to);
		$uniqueWageringUserPlayingCountNonDepositer = $wageringUserPlayingCountNonDeposit[0]->Unique_Wagering_users_non_depositors;

		#Poker Partner Unique Free Roll Playing Users Count
		$uniqueFreerollUserCount = $this->uniqueFreerollUserCount($date_from,$date_to);
		$uniqueFreerollUserPlayingCount = $uniqueFreerollUserCount[0]->Unique_freeroll_playing_users;

		#Poker Partner Game Type Five Card, Omaha, Taxes Holdem
		$gameTypeFiveCardOmahaTaxes = $this->gameTypeFiveCardOmahaTaxes($date_from,$date_to);
		$minigamesTypeName = ['Five cards omaha','Omaha' ,"Texas Hold'em"];

		$fivecard_temp=[];
		foreach($minigamesTypeName as $key => $minigamesTypeNameValue ){
			$fivecard_temp[$minigamesTypeNameValue] = null;
			foreach($gameTypeFiveCardOmahaTaxes as $key=> $gameTypeFiveCardOmahaTaxesValue)
			{
				
				if($minigamesTypeNameValue === $gameTypeFiveCardOmahaTaxesValue->MINIGAMES_TYPE_NAME){

					$unique_user = number_format($gameTypeFiveCardOmahaTaxesValue->Unique_users, 2);

					$ARPU = $unique_user==0?0:($gameTypeFiveCardOmahaTaxesValue->GGR/$unique_user);
					
					if($gameTypeFiveCardOmahaTaxesValue->GGR !=''){
						$gameTypeFiveCardOmahaTaxesValue->GGR_SUM = $gameTypeFiveCardOmahaTaxesValue->GGR;
						$gameTypeFiveCardOmahaTaxesValue->GGR = $this->numberExtChange($gameTypeFiveCardOmahaTaxesValue->GGR);
						
					}
					if($gameTypeFiveCardOmahaTaxesValue->Bonus !=''){
						$gameTypeFiveCardOmahaTaxesValue->BONUS_SUM = $gameTypeFiveCardOmahaTaxesValue->Bonus;
						$gameTypeFiveCardOmahaTaxesValue->Bonus = $this->numberExtChange($gameTypeFiveCardOmahaTaxesValue->Bonus);
						
					}
					if($gameTypeFiveCardOmahaTaxesValue->NGR !=''){
						$gameTypeFiveCardOmahaTaxesValue->NGR_SUM = $gameTypeFiveCardOmahaTaxesValue->NGR;
						$gameTypeFiveCardOmahaTaxesValue->NGR = $this->numberExtChange($gameTypeFiveCardOmahaTaxesValue->NGR);
						
					}
					if($gameTypeFiveCardOmahaTaxesValue->GGR !='' && $gameTypeFiveCardOmahaTaxesValue->Unique_users !=''){
						$gameTypeFiveCardOmahaTaxesValue->ARPU = $this->numberExtChange($ARPU);
					}
					$fivecard_temp[$minigamesTypeNameValue] = $gameTypeFiveCardOmahaTaxesValue;
				}
			}
		}

		#Poker Partner Game Type OFC
		$gameTypeOfc = $this->gameTypeOfc($date_from,$date_to);
		$ofcminigamesTypeName = ['OFC'];
		
		$ofc_temp=[];
		foreach($ofcminigamesTypeName as $key => $ofcminigamesTypeNameValue ){
			$ofc_temp[$ofcminigamesTypeNameValue] = null;
			foreach($gameTypeOfc as $key=> $gameTypeOfcValue)
			{
				
				if($ofcminigamesTypeNameValue === $gameTypeOfcValue->MINIGAMES_TYPE_NAME){

					$unique_user = number_format($gameTypeOfcValue->Unique_users, 2);

					$ARPU = $unique_user == 0?0:($gameTypeOfcValue->GGR/$unique_user);
					
					if($gameTypeOfcValue->GGR !=''){
						$gameTypeOfcValue->GGR = $this->numberExtChange($gameTypeOfcValue->GGR);
						$gameTypeOfcValue->GGR_SUM = $gameTypeOfcValue->GGR;
					}
					if($gameTypeOfcValue->Bonus !=''){
						$gameTypeOfcValue->Bonus = $this->numberExtChange($gameTypeOfcValue->Bonus);
						$gameTypeOfcValue->BONUS_SUM = $gameTypeOfcValue->Bonus;
					}
					if($gameTypeOfcValue->NGR !=''){
						$gameTypeOfcValue->NGR = $this->numberExtChange($gameTypeOfcValue->NGR);
						$gameTypeOfcValue->NGR_SUM = $gameTypeOfcValue->NGR;
					}
					if($gameTypeOfcValue->GGR !='' && $gameTypeFiveCardOmahaTaxesValue->Unique_users !=''){
						$gameTypeOfcValue->ARPU = $this->numberExtChange($ARPU);
					}
					$ofc_temp[$ofcminigamesTypeNameValue] = $gameTypeOfcValue;
				}
			}
		}

		#Poker Partner Tournament  Bonus	
		$tournamentBonus = $this->tournamentBonus($date_from,$date_to);
		$tournamentBonusGame = '';
		if($tournamentBonus !=''){
				$data['bounus'] = $tournamentBonus[0]->bonus_T;
				$tournamentBonusGame = $data;
		}

		#Poker Partner Game Tournament  GGR(GGR-T) and UniqueUser	
		$tournamentGgr = $this->tournamentGGR($date_from,$date_to);
		
		$tournamentGame = [];
		if($tournamentGgr !=''){
			foreach($tournamentGgr as $index => $tournamentGgr_value) {
				
				$unique_user = number_format($tournamentGgr_value->UniqueUser, 2);
				$ARPU = $unique_user == 0?0:($tournamentGgr_value->GGR_T/$unique_user);
				$temp = null;
				$temp = new stdClass();
				$temp->UniqueUser = $tournamentGgr_value->UniqueUser;
				$temp->ARPU = $this->numberExtChange($ARPU);
				$temp->Bonus = $this->numberExtChange($tournamentBonusGame['bounus']);
				$temp->BONUS_SUM = $tournamentBonusGame['bounus'];
				$temp->GGR = $this->numberExtChange($tournamentGgr_value->GGR_T);
				$temp->GGR_SUM = $tournamentGgr_value->GGR_T;
				$temp->NGR = $this->numberExtChange($tournamentGgr_value->GGR_T - $tournamentBonusGame['bounus']);
				$temp->NGR_SUM = ($tournamentGgr_value->GGR_T - $tournamentBonusGame['bounus']);
				$tournamentGame[] = $temp;
			}
			
		}
		
		#Poker Partner Revenue KPI's
		#Gross_Revenue = sum of all rows of GGR in Game kpis
		#Bonus_Revenue = sum of all rows of Bonus in Game kpis
		#Net_Revenue = sum of all rows of NGR in Game kpis
		$FIVECARD_GGR_SUM[] = 0;
		$FIVECARD_BONUS_SUM[] = 0;
		$FIVECARD_NGR_SUM[] = 0;
		if($fivecard_temp){
			foreach($fivecard_temp as $fivecard_temp_value){
				$FIVECARD_GGR_SUM[] = isset($fivecard_temp_value->GGR_SUM)?$fivecard_temp_value->GGR_SUM:0;
				$FIVECARD_BONUS_SUM[] = isset($fivecard_temp_value->BONUS_SUM)?$fivecard_temp_value->BONUS_SUM:0;
				$FIVECARD_NGR_SUM[] = isset($fivecard_temp_value->NGR_SUM)?$fivecard_temp_value->NGR_SUM:0;
				
			}
		}

		$OFC_GGR_SUM[] = 0;
		$OFC_BONUS_SUM[] = 0;
		$OFC_NGR_SUM[] = 0;
		if($ofc_temp){
			foreach($ofc_temp as $ofc_temp_value){
				$OFC_GGR_SUM[] = isset($ofc_temp_value->GGR_SUM)?$ofc_temp_value->GGR_SUM:0;
				$OFC_BONUS_SUM[] = isset($ofc_temp_value->BONUS_SUM)?$ofc_temp_value->BONUS_SUM:0;
				$OFC_NGR_SUM[] = isset($ofc_temp_value->NGR_SUM)?$ofc_temp_value->NGR_SUM:0;
			}
		}

		$TOURNAMENT_GGR_SUM[] = 0;
		$TOURNAMENT_BONUS_SUM[] = 0;
		$TOURNAMENT_NGR_SUM[] = 0;
		if($tournamentGame){
			foreach($tournamentGame as $tournamentGame_value){
				$TOURNAMENT_GGR_SUM[] = isset($tournamentGame_value->GGR_SUM)?$tournamentGame_value->GGR_SUM:0;
				$TOURNAMENT_BONUS_SUM[] = isset($tournamentGame_value->BONUS_SUM)?$tournamentGame_value->BONUS_SUM:0;
				$TOURNAMENT_NGR_SUM[] = isset($tournamentGame_value->NGR_SUM)?$tournamentGame_value->NGR_SUM:0;
			}
		}
		
		#Gross_Revenue = sum of all rows of GGR in Game kpis
		$Gross_Revenue = $this->numberExtChange((array_sum($FIVECARD_GGR_SUM) + array_sum($OFC_GGR_SUM) + array_sum($TOURNAMENT_GGR_SUM)));

		#Bonus_Revenue = sum of all rows of Bonus in Game kpis
		$Bonus_Revenue = $this->numberExtChange((array_sum($FIVECARD_BONUS_SUM) + array_sum($OFC_BONUS_SUM) + array_sum($TOURNAMENT_BONUS_SUM)));

		#Net_Revenue = sum of all rows of NGR in Game kpis
		$Net_Revenue = $this->numberExtChange((array_sum($FIVECARD_NGR_SUM) + array_sum($OFC_NGR_SUM) + array_sum($TOURNAMENT_NGR_SUM)));
		

		$yesterday_date = $data['yesterday_date'] = date('j-F-Y', strtotime("-1 day"));
		$date = $data['date'] = date('Y-m-d', strtotime("-1 day"));
		
		$pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('bo.views.exportpdf.pokerpartnerautomationpdf', [
			'grossRevenue'=>$Gross_Revenue,
			'bonusRevenue'=>$Bonus_Revenue,
			'netRevenue'=>$Net_Revenue,
			'userDAULogins'=>$userDAULogins,
			'signUp'=>$signUps,
			'fromPbWalletUserCount'=>$fromPbWalletUserCount,
			'fromPbWalletAmount'=>$fromPbWalletAmount,
			'toPbWalletAmount' => $toPbWalletAmount,
			'toPbWalletUserCount' => $toPbWalletUserCount,
			'firstTimetoPBWalletUserCount' => $firstTimetoPBWalletUserCount,
			'firstTimeBacktoBBWalletUserCount' => $firstTimeBacktoBBWalletUserCount,
			'uniqueWageringUserPlayingCount' => $uniqueWageringUserPlayingCount,
			'uniqueWageringUserPlayingCountNonDepositer' => $uniqueWageringUserPlayingCountNonDepositer,
			'uniqueFreerollUserPlayingCount' => $uniqueFreerollUserPlayingCount,
			'gameTypeFiveCardtaxesOmaha' => $fivecard_temp,
			'gameTypeOfc' => $ofc_temp,
			'tournamentGame' => $tournamentGame,
			'yesterday_date'=>$yesterday_date
		])->setPaper('a4','landscape');
		
		//return $pdf->download($file.".pdf");
		$dataArr = $pdf->output();
		## get file location
		
		$file = "Poker_Partner_Automation_Report_" . $data['date'] . ".pdf";
		## check file and delete if exists
		if (Storage::disk('local')->exists($file)) {
			Storage::delete($file);
		}
		Storage::put($file, $dataArr);
		
		## generate new file and then send mail.
		if (Storage::disk('local')->exists($file)) {
			Mail::send("bo.mail.pokerPartnerReportMail", $data, function ($message) use($data, $dataArr, $file) {
				$message->from(config('poker_config.mail.from.analytics'), 'Poker Partner Automation Report');
				$message->to(config('poker_config.mail.from.analytics'))
				->bcc(config('poker_config.poker_partner_automation_mail.emails'))
						->subject('PB<>BB Daily Turnover Report for ' . " " . $data['yesterday_date']);
				
					$message->attach(storage_path('app/' . $file), [
						'as' => $file,
						'mime' => 'application/pdf'
					]);
				
			}); 
			if (count(Mail::failures()) <= 0 && Storage::disk('local')->exists($file)) {
				Storage::delete($file);
			}
		}
		
		return response()->json(["status" => 200, "message" => "success"]);
	}
	
	
	//Poker Partner Daily Active Users or Login Users
	public function userDAUsLogin($date_from,$date_to){
		$result = DB::connection('slave')->select("select count(distinct dau) as Daus from ((select distinct user_id as dau from user_turnover_report_daily where PARTNER_ID=$this->poker_partner and (REPORT_DATE) between '$date_from' AND '$date_to'  ) union (select distinct user_id as dau from tournament_registration where updated_date between '$date_from' AND '$date_to' and user_id in (select distinct user_id from user where partner_id=$this->poker_partner)) union (select distinct user_id as dau from ofc_game_transaction_history where PARTNER_ID=$this->poker_partner and (Started) between '$date_from' AND '$date_to' ))a"
		);
		return $result;
	}

	
	//Poker Partner Total registred users
	public function signUpLogins($date_from,$date_to){
		return $result = DB::connection('slave')->select("select count(distinct user_id) as Registrations from user where partner_id=$this->poker_partner and registration_timestamp BETWEEN '$date_from' AND '$date_to'");
	}

	#Transferred From Poker Partner PB Wallet (Amount and User Count (Debit = 132))
	public function transferredFromPBWallet($date_from,$date_to){
		$result = DB::connection('slave')->select("select tt.TRANSACTION_TYPE_NAME as Credit_or_Debit, sum(mth.TRANSACTION_AMOUNT) as Transferred_from_PB_Wallet_amount, count(distinct user_id) as Transferred_from_PB_Wallet_user_count from master_transaction_history mth left join transaction_type tt on tt.TRANSACTION_TYPE_ID=mth.TRANSACTION_TYPE_ID where mth.transaction_type_id in ($this->poker_partner_transaction_type_id_for_debit) and mth.partner_id=$this->poker_partner and (TRANSACTION_DATE) between '$date_from' AND '$date_to'");
		return $result;
	}

	#Transferred To Poker Partner PB Wallet (Amount and User Count (Credit = 133))
	public function transferredToPBWallet($date_from,$date_to){
		$result = DB::connection('slave')->select("select tt.TRANSACTION_TYPE_NAME as Credit_or_Debit,sum(mth.TRANSACTION_AMOUNT) as Transferred_to_PB_Wallet_amount,count(distinct user_id) as Transferred_to_PB_Wallet_user_count from master_transaction_history mth left join transaction_type tt on tt.TRANSACTION_TYPE_ID=mth.TRANSACTION_TYPE_ID where mth.transaction_type_id in ($this->poker_partner_transaction_type_id_for_credit) and mth.partner_id=$this->poker_partner and (TRANSACTION_DATE) between '$date_from' AND '$date_to'");
		return $result;
	}

	#Poker Partner First Time Transferred To PB Wallet Count(Credit = 133)
	public function firstTimeTransferredToPBWalletCount($date_from,$date_to){
		$result = DB::connection('slave')->select("select count(distinct user_id) as firast_time_Transferred_to_PB_Wallet_user_count  from (select tt.TRANSACTION_TYPE_NAME as Credit_or_Debit , user_id from master_transaction_history mth left join transaction_type tt on tt.TRANSACTION_TYPE_ID=mth.TRANSACTION_TYPE_ID where mth.transaction_type_id in ($this->poker_partner_transaction_type_id_for_credit) and mth.partner_id=$this->poker_partner group by user_id having min(TRANSACTION_DATE) between '$date_from' AND '$date_to')a");
		return $result;
	}

	#Poker Partner First Time Transferred Back To BB Wallet Count(Debit = 132)
	public function firstTimeTransferredBackToBBWalletCount($date_from,$date_to){
		$result = DB::connection('slave')->select("select count(distinct user_id) as firast_time_Transferred_back_to_BB_Wallet_user_count  from (select tt.TRANSACTION_TYPE_NAME as Credit_or_Debit , user_id from master_transaction_history mth left join transaction_type tt on tt.TRANSACTION_TYPE_ID=mth.TRANSACTION_TYPE_ID where mth.transaction_type_id in ($this->poker_partner_transaction_type_id_for_debit) and mth.partner_id=$this->poker_partner group by user_id having min(TRANSACTION_DATE) between '$date_from' AND '$date_to')a");
		return $result;
	}

	#Poker Partner Unique Wagering users (Playing) Count
	public function wageringUserPlayingCount($date_from,$date_to){
		$result = DB::connection('slave')->select("select  count(distinct user_id) as Unique_Wagering_users_Playing from (select user_id from (select distinct user_id from game_transaction_history where  PARTNER_ID=$this->poker_partner and started BETWEEN  '$date_from' AND '$date_to')a union select user_id from (select user_id from ofc_game_transaction_history where PARTNER_ID=$this->poker_partner and started BETWEEN  '$date_from' AND '$date_to')b)c");
		return $result;
	}

	#Poker Partner Unique Wagering users (Playing) Count Non Depositer (Credit = 133 Transaction_type_id)
	public function wageringUserPlayingCountNonDeposit($date_from,$date_to){
		$result = DB::connection('slave')->select("select  count(distinct user_id) as Unique_Wagering_users_non_depositors from (select user_id from (select distinct user_id from game_transaction_history where  PARTNER_ID=$this->poker_partner and started BETWEEN  '$date_from' AND '$date_to')a union select user_id from (select user_id from ofc_game_transaction_history where PARTNER_ID=$this->poker_partner and started BETWEEN  '$date_from' AND '$date_to')b)c where user_id not in (select distinct user_id from master_transaction_history where transaction_type_id=$this->poker_partner_transaction_type_id_for_credit)");
		return $result;
	}

	#Poker Partner Unique Free Roll Playing Users Count
	public function uniqueFreerollUserCount($date_from,$date_to){
		$result = DB::connection('slave')->select("select count(distinct user_id) as Unique_freeroll_playing_users  from tournament_registration where REGISTERED_DATE between '$date_from' AND '$date_to' and TOURNAMENT_ID in(select tournament_id from tournament where buyin=0 and tournament_name not like '%satty%') and user_id in (select distinct user_id from user where partner_id=$this->poker_partner)");
		return $result;
	}
	
	#Poker Partner Game Type Five Card, Omaha, Taxes Holdem
	public function gameTypeFiveCardOmahaTaxes($date_from,$date_to){
		$result = DB::connection('slave')->select("select MINIGAMES_TYPE_NAME,sum(REVENUE) as GGR,sum(BONUS_REVENUE) as Bonus,round(sum((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) as NGR,count(distinct user_id) as Unique_users,COUNT(DISTINCT PLAY_GROUP_ID) as hands from game_transaction_history where PARTNER_ID=$this->poker_partner and started BETWEEN  '$date_from' AND '$date_to' group by MINIGAMES_TYPE_NAME");
		return $result;
	}

	#Poker Partner Game Type OFC
	public function gameTypeOfc($date_from,$date_to){
		$result = DB::connection('slave')->select("select MINIGAMES_TYPE_NAME,sum(REVENUE) as GGR,sum(BONUS_REVENUE) as Bonus,round(sum((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) as NGR,count(distinct user_id) as Unique_users,COUNT(DISTINCT PLAY_GROUP_ID) as hands from ofc_game_transaction_history where PARTNER_ID=$this->poker_partner and started BETWEEN  '$date_from' AND '$date_to' group by MINIGAMES_TYPE_NAME");
		return $result;
		
	}

	#Poker Partner Tournament  GGR(GGR-T) and UniqueUser	
	public function tournamentGGR($date_from,$date_to){
		$result = DB::connection('slave')->select("SELECT COUNT(DISTINCT USER_ID) AS UniqueUser,SUM(TRANSACTION_AMOUNT) As 'GGR_T' FROM master_transaction_history WHERE TRANSACTION_DATE BETWEEN '$date_from' AND '$date_to' and transaction_type_id in (25,102,105,125) and INTERNAL_REFERENCE_NO not in (select INTERNAL_REFERENCE_NO from  master_transaction_history where TRANSACTION_TYPE_ID=13 and TRANSACTION_STATUS_ID=108) and user_id in (select  user_id from user where partner_id=$this->poker_partner ) and TRANSACTION_AMOUNT> 0 ");
		return $result;
		
	}

	#Poker Partner Tournament  Bonus	
	public function tournamentBonus($date_from,$date_to){
		//\DB::connection('slave')->enableQueryLog();
		$result = DB::connection('slave')->select("SELECT SUM(TRANSACTION_AMOUNT) As 'bonus_T' FROM master_transaction_history WHERE TRANSACTION_DATE BETWEEN '$date_from' AND '$date_to' and transaction_type_id in (25,102,105,125) and INTERNAL_REFERENCE_NO not in (select INTERNAL_REFERENCE_NO from master_transaction_history where TRANSACTION_TYPE_ID=13 and TRANSACTION_STATUS_ID=108) and user_id in (select  user_id from user where partner_id=$this->poker_partner) and TRANSACTION_AMOUNT> 0 and balance_type_id=2");
		//$queries = \DB::connection('slave')->getQueryLog();
		//dd($queries);
		return $result;
		
	}
	
	
	
}