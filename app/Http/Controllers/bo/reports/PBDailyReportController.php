<?php
namespace App\Http\Controllers\bo\reports;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use stdClass;
use DB;
use Carbon\Carbon;
use \PDF;
use App;
use Validator;
use Mail;
use Storage;

class PBDailyReportController extends Controller 
{
	
	function count_digit($number) {
		return strlen(abs($number));
	}

	function divider($number_of_digits) {
		$tens="1";

	  if($number_of_digits>8)
		return 10000000;

	  while(($number_of_digits-1)>0)
	  {
		$tens.="0";
		$number_of_digits--;
	  }
	  return $tens;
	}
	public function numberExtChange($val){
		//function call
		$num = floor($val);
		$ext="";//thousand,lac, crore
		
		$number_of_digits = $this->count_digit($num); //this is call :)
		if($number_of_digits>3)
		{
			if($number_of_digits%2!=0){
				$divider=$this->divider($number_of_digits-1);
			}
			else{
				$divider=$this->divider($number_of_digits);
			}
		}
		else{
			$divider=1;
		}
		
		$fraction=$num/$divider;
		$fraction=number_format($fraction,2);
		if($number_of_digits==4 ||$number_of_digits==5)
			$ext="K";
		if($number_of_digits==6 ||$number_of_digits==7)
			$ext="Lacs";
		if($number_of_digits==8 ||$number_of_digits>=9)
			$ext="Crores";
		
		return $fraction." ".$ext;
	}
	public function index(Request $request){
		
		$date = date('Y-m-d', strtotime("-1 day"));
		$date_from =Carbon::parse($date)->setTime(0, 0, 0)->toDateTimeString();
		$date_to =Carbon::parse($date)->setTime(23, 59, 59)->toDateTimeString();
		#1financial query 
		#Gross Revenue , Bonus Revenue , Net Revenue , NGR/GGR   []-
		$grossBonusNetNGRGGRRevenues = $this->grossBonusNetNGRGGRRevenue();
		
			if($grossBonusNetNGRGGRRevenues != []){
			
			$object = json_decode(json_encode($grossBonusNetNGRGGRRevenues),true);
			$grossBonusNetNGRGGRRevenue  = json_decode(json_encode($object[0]), FALSE);
			if($grossBonusNetNGRGGRRevenue->GROSS_REVENE != ''){
				$grossRevenue = $this->numberExtChange($grossBonusNetNGRGGRRevenue->GROSS_REVENE);
			}else{
				$grossRevenue = '';
			}
			if($grossBonusNetNGRGGRRevenue->BONUS_REVENUE != ''){
				$bonusRevenue = $this->numberExtChange($grossBonusNetNGRGGRRevenue->BONUS_REVENUE);
			}else{
				$bonusRevenue = '';
			}
			if($grossBonusNetNGRGGRRevenue->NET_REVENUE != ''){
				$netRevenue = $this->numberExtChange($grossBonusNetNGRGGRRevenue->NET_REVENUE);
			}else{
				$netRevenue = '';
			}
			if($grossBonusNetNGRGGRRevenue->NGRGGR != ''){
				$ngrGGR = $this->numberExtChange($grossBonusNetNGRGGRRevenue->NGRGGR);
			}else{
				$ngrGGR = '';
			}
		}else{
			$grossRevenue = '';
			$bonusRevenue = '';
			$netRevenue = '';
			$ngrGGR = '';
		}		
			
		#2.Deposit withdrawl net cash
		$dep_with_netCash = $this->depositsWithdrawalsNetCash();
		
		if($dep_with_netCash != []){
			if($dep_with_netCash[0]->DEPOSIT_AMOUNT !=''){
				$deposit = $this->numberExtChange($dep_with_netCash[0]->DEPOSIT_AMOUNT);
			}else{
				$deposit = '';
			}
			if($dep_with_netCash[0]->NET_CASH !=''){
				$netCash = $this->numberExtChange($dep_with_netCash[0]->NET_CASH);
			}else{
				$netCash = '';
			}
			if($dep_with_netCash[0]->WITHDRAWING_AMOUNT !=''){
				$withdraw = $this->numberExtChange($dep_with_netCash[0]->WITHDRAWING_AMOUNT);
			}else{
				$withdraw = '';
			}
		}else{
			$deposit = '';
			$netCash = '';
			$withdraw = '';
		}	
		
		#end deposite
		
		#Users,DAUs (logins) :-
		$userDAULogins = $this->userDAUsLogin($date_from,$date_to);
		
		#signup logins
		$signUps = $this->signUpLogins($date_from,$date_to);
		
		#.Total promo cost
		$Total_Promo_Cost = $this->totalPromoCost();
		$object = json_decode(json_encode($Total_Promo_Cost),true);
		$totpromoCost  = json_decode(json_encode($object[0]), FALSE);
		if($totpromoCost->TOTAL_PROMO_COST !=''){
			$TPC = $this->numberExtChange($totpromoCost->TOTAL_PROMO_COST);
		}else{
			$TPC = '';
		}
		#4.Withdrawable player balance (WPB)
		$WPB_NUM = $this->withdrawalPlayerBalance();
		
		if($WPB_NUM != ''){
			$WPB = $this->numberExtChange($WPB_NUM[0]->bal_1);
			
			if($WPB_NUM[0]->bal_1 != ''){
				$oneDayAccuBal = $WPB_NUM[0]->bal_1;
			}else{
				$oneDayAccuBal = 0.00;
			}
		}
		
		
		#bal2 
		$bal2 = $this->balTwo();
		if($bal2 != ''){
			if($bal2[0]->bal_2 != ''){
				$twoDayAccBal = $bal2[0]->bal_2;
			}else{
				$twoDayAccBal = 0.00;
			}
		}
		
		#bal3 
		$bal3 = $this->balThree();
		if($bal3 != ''){
			$onemonthAccuBal = $bal3[0]->bal_3;
			if($bal3[0]->bal_3 != ''){
				$onemonthAccuBal = $bal3[0]->bal_3;
			}else{
				$onemonthAccuBal = 0.00;
			}
		}
		#Monthly Liability Change = bal1-bal3 
	
		if(($oneDayAccuBal == '')  && ($onemonthAccuBal == '')){
			$monthLiaChng = '';
		}else{
			$monthLiaChngBal = ($oneDayAccuBal - $onemonthAccuBal);
			if($monthLiaChngBal != ''){
				$monthLiaChng = $this->numberExtChange($monthLiaChngBal);
			}else{
				$monthLiaChng = '';
			}	
		}
		
		##Net Cash (Including Liabilities)=Deposit-Withdrawals-bal1+bal2
		if($dep_with_netCash != []){
			if($dep_with_netCash[0]->DEPOSIT_AMOUNT != ''){
				$dep_bal = $dep_with_netCash[0]->DEPOSIT_AMOUNT;
			}else{
				$dep_bal = 0.00;
			}
			if($dep_with_netCash[0]->WITHDRAWING_AMOUNT != ''){
				$witd_bal = $dep_with_netCash[0]->WITHDRAWING_AMOUNT;
			}else{
				$witd_bal = 0.00;
			}
			
		}else{
			$dep_bal = 0.00;
			$witd_bal = 0.00;
		}
		
		if(($dep_bal == '')  && ($witd_bal == '')  && ($twoDayAccBal == '') && ($oneDayAccuBal == '')){
			$netCashIncLia = '';
		}else{
			$netCashIncLiaBal = $dep_bal-$witd_bal-$oneDayAccuBal+$twoDayAccBal;
			if($netCashIncLiaBal != ''){
				$netCashIncLia = $this->numberExtChange($netCashIncLiaBal);
			}else{
				$netCashIncLia = '';
			}	
		}
		
		#Withdrawable pendings 
		$pendingWithdrawalAmt = $this->pendingWithdrawalAmount();
		
		if($pendingWithdrawalAmt != []){
			$pendingWithdrawalAmount = $this->numberExtChange($pendingWithdrawalAmt[0]->PENDING_WITHDRAWAL_AMOUNT);
		}else{
			$pendingWithdrawalAmount = '';
		}
		#% change In WPB  //(bal1-bal2)/bal2
		
		if($twoDayAccBal != ''){
			$percentchangeInWPBBal = (($oneDayAccuBal - $twoDayAccBal)/$twoDayAccBal)*100;
			$percentchangeInWPB = round($percentchangeInWPBBal,1);;
		}else{
			$percentchangeInWPB = '';
		}
		
		# change In WPB  //bal1-bal2
		$changeInWPBBal = $oneDayAccuBal - $twoDayAccBal;
		$changeInWPB = $this->numberExtChange($changeInWPBBal);
		#FTDs reports
		$ftdReport = $this->ftdsReports($date_from,$date_to);
		$object = json_decode(json_encode($ftdReport),true);
		$ftdReports  = json_decode(json_encode($object[0]), FALSE);
		
		#FTWs reports
		$ftwReport = $this->ftwsReports();
		$object = json_decode(json_encode($ftwReport),true);
		$ftwsReport  = json_decode(json_encode($object[0]), FALSE);
		
		
		#Unique  Withdrawing Users reports			[]-
		$uniqueWithdrawingUsers = $this->uniqueWithdrawingUser($date_from,$date_to);
		
		if($uniqueWithdrawingUsers != []){
			
			 $object = json_decode(json_encode($uniqueWithdrawingUsers),true);
			 $uniqueWithdrawingUser  = json_decode(json_encode($object[0]), FALSE);
		}else{
			$uniqueWithdrawingUser = '';
		}
		
		#Unique Depositers  			[]-
		$uniqueDepositers = $this->uniqueDepositer($date_from,$date_to);
		if($uniqueDepositers != ''){
			
			$object = json_decode(json_encode($uniqueDepositers),true);
			$uniqueDepositer  = json_decode(json_encode($object[0]), FALSE);
		}else{
			
			$uniqueDepositer = '';
		}
		#Unique Wagering Users Players reports
		$UniqueWageringUser = $this->UniqueWageringUsers($date_from,$date_to);
		
		#Lifetime Depositors Wagering( LTDW) :  reports
		$lifetimeDepositorsWagerings = $this->lifetimeDepositorsWagering();
		if($lifetimeDepositorsWagerings != []){
			
			$object = json_decode(json_encode($lifetimeDepositorsWagerings),true);
			$lifetimeDepositorsWagering  = json_decode(json_encode($object[0]), FALSE);
		}else{
			
			$lifetimeDepositorsWagering = '';
		}
		
		
		#Wagering users (non-depositors)  :  reports
		$wageringusers_non_depositor  = $this->wageringusersNonDepositors();
		if($wageringusers_non_depositor != []){
			
			$object = json_decode(json_encode($wageringusers_non_depositor),true);
			$wageringusers_non_depositors  = json_decode(json_encode($object[0]), FALSE);
		}else{
			
			$wageringusers_non_depositors = '';
		}
		
		
		#Unique freeroll playing users :   reports
		$uniqueFreerollPlayingUsers  = $this->uniqueFreerollPlayingUsers($date_from,$date_to);
		
		#Promo split(Signup promo,Deposit promo,Locked promo released),
		$partnerToSite = $this->partnerToSite(); 
		$bonusCategories = ['Deposit_promo' ,'Locked_promo_released' ,'Signup_promo' ,'Partner_to_site'
		];
		$partnerToSite_temp=[];
		foreach($bonusCategories as $key => $bonusCategoriesValue ){
			$partnerToSite_temp[$bonusCategoriesValue] = null;
			foreach($partnerToSite as $key=> $partnerToSiteValue)
			{
				if($bonusCategoriesValue === $partnerToSiteValue->Bonus_categories){
					if($partnerToSiteValue->Amount != ''){
						$partnerToSiteValue->Amount = $this->numberExtChange($partnerToSiteValue->Amount);
					}
					$partnerToSite_temp[$bonusCategoriesValue] = $partnerToSiteValue;
				}
			}
		}
		
		#RCC RBB
		$RCC = $this->rCC(); 
		$rCC = [];
		if($RCC != []){	
			
			foreach($RCC as $index => $rCC_value) {
				$temp = null;
				$temp = new stdClass();
				$temp->RCC_AMOUNT = $this->numberExtChange($rCC_value->RCC_AMOUNT);
				$temp->RCC_UNIQUE_USERS = $rCC_value->RCC_UNIQUE_USERS;
				$temp->RCC_NO_OF_TXN = $rCC_value->RCC_NO_OF_TXN;
				$temp->RCC_AVG_PROMO = $rCC_value->RCC_AVG_PROMO;
				
				$rCC[] = $temp;
			}
		}
		
		$RCB = $this->rCB();
		$rCB = [];
		if($RCB != []){	
			
			foreach($RCB as $index => $rCB_value) {
				$temp = null;
				$temp = new stdClass();
				$temp->RCB_AMOUNT = $this->numberExtChange($rCB_value->RCB_AMOUNT);
				$temp->RCB_UINQUE_USERS = $rCB_value->RCB_UINQUE_USERS;
				$temp->RCB_NO_OF_TXN = $rCB_value->RCB_NO_OF_TXN;
				$temp->RCB_AVG_PROMO = $rCB_value->RCB_AVG_PROMO;
				
				$rCB[] = $temp;
			}
		}
		
		#tournament Tickets Expense
		$tournamentTicketsExpenses = $this->tournamentTicketsExpense($date_from,$date_to);
		$tournamentTicketsExpense = [];
		if($tournamentTicketsExpenses != []){	
			
			foreach($tournamentTicketsExpenses as $index => $tourTicExp_value) {
				$temp = null;
				$temp = new stdClass();
				$temp->TOURNAMENT_TICKETS_EXPENSE_AMOUNT = $this->numberExtChange($tourTicExp_value->TOURNAMENT_TICKETS_EXPENSE_AMOUNT);
				$temp->TOURNAMENT_TICKETS_EXPENSE_NO_OF_TXN = $tourTicExp_value->TOURNAMENT_TICKETS_EXPENSE_NO_OF_TXN;
				$tournamentTicketsExpense[] = $temp;
			}
		}
		#tournament overlays
		$tournamentOverlay = $this->tournamentOverlays();
		$tournamentOverlays = [];
		if($tournamentOverlay != []){	
			
			foreach($tournamentOverlay as $index => $tourOver_value) {
				$temp = null;
				$temp = new stdClass();
				$temp->TOURNAMENT_OVERLAYS_AMOUNT = $this->numberExtChange($tourOver_value->TOURNAMENT_OVERLAYS_AMOUNT);
				$temp->OVERLAY_TOURNAMENT_COUNT = $tourOver_value->OVERLAY_TOURNAMENT_COUNT;
				$temp->TOTAL_TOURNAMENT = $tourOver_value->TOTAL_TOURNAMENT;
				$tournamentOverlays[] = $temp;
			}
		}
		#Texas Hold'em(Stakes (Big Blind))  
		$texas_stacks = $this->texasHoldemBigBlindStacks($date_from,$date_to);
		$stacks = [];
		if($texas_stacks != []){	
			foreach($texas_stacks as $index => $texas_stack_value) {
				$temp = null;
				$temp = new stdClass();
				$temp->GROSS_RAKE = $this->numberExtChange($texas_stack_value->GROSS_RAKE);
				$temp->NET_RAKE = $this->numberExtChange($texas_stack_value->NET_RAKE);
				$temp->BONUS_REVENUE = $this->numberExtChange($texas_stack_value->BONUS_REVENUE);
				$temp->BIG_BLIND = $texas_stack_value->BIG_BLIND;
				$temp->PLAYING_USER_TEXAS = $texas_stack_value->PLAYING_USER_TEXAS;
				$temp->NUMBER_OF_HANDS = $texas_stack_value->NUMBER_OF_HANDS;
				$temp->ARPU = $texas_stack_value->ARPU;
				$stacks[] = $temp;
			}
		}
		# Small Medium High
		$texasHoldemBigBlindStacksSmallHighMid = $this->texasHoldemBigBlindStacksSmallHighMid($date_from,$date_to);
		
		$big_blind_case = ['SMALL' ,'MID' ,'HIGH'];
		$texas_temp=[];
		foreach($big_blind_case as $key => $big_blind_caseValue ){
			$texas_temp[$big_blind_caseValue] = null;
			foreach($texasHoldemBigBlindStacksSmallHighMid as $key=> $texasHoldemBigBlindStacksSmallHighMidValue)
			{
				if($big_blind_caseValue === $texasHoldemBigBlindStacksSmallHighMidValue->BIG_BLIND_CASE){
					if($texasHoldemBigBlindStacksSmallHighMidValue->GROSS_RAKE != ''){
						$texasHoldemBigBlindStacksSmallHighMidValue->GROSS_RAKE = $this->numberExtChange($texasHoldemBigBlindStacksSmallHighMidValue->GROSS_RAKE);
					}
					if($texasHoldemBigBlindStacksSmallHighMidValue->NET_RAKE != ''){
						$texasHoldemBigBlindStacksSmallHighMidValue->NET_RAKE = $this->numberExtChange($texasHoldemBigBlindStacksSmallHighMidValue->NET_RAKE);
					}
					if($texasHoldemBigBlindStacksSmallHighMidValue->BONUS_REVENUE != ''){
						$texasHoldemBigBlindStacksSmallHighMidValue->BONUS_REVENUE = $this->numberExtChange($texasHoldemBigBlindStacksSmallHighMidValue->BONUS_REVENUE);
					}
					$texas_temp[$big_blind_caseValue] = $texasHoldemBigBlindStacksSmallHighMidValue;
				}
			}
		}
		
		# Pot Limit Omaha 4 card			
		$potLimitOmaha4cards = $this->potLimitOmaha4card();
		$potLimitOmaha4card = [];
		if($potLimitOmaha4cards != []){	
			foreach($potLimitOmaha4cards as $index => $potlimit4_card_value) {
				$temp = null;
				$temp = new stdClass();
				$temp->GROSS_RAKE = $this->numberExtChange($potlimit4_card_value->GROSS_RAKE);
				$temp->NET_RAKE = $this->numberExtChange($potlimit4_card_value->NET_RAKE);
				$temp->BONUS_REVENUE = $this->numberExtChange($potlimit4_card_value->BONUS_REVENUE);
				$temp->BIG_BLIND = $potlimit4_card_value->BIG_BLIND;
				$temp->PLAYING_USER = $potlimit4_card_value->PLAYING_USER;
				$temp->NUMBER_OF_HANDS = $potlimit4_card_value->NUMBER_OF_HANDS;
				$temp->ARPU = $potlimit4_card_value->ARPU;
				$potLimitOmaha4card[] = $temp;
			}
		}
		# High , mid & low					
		$potLimitOmaha4CardhighMidLowCard = $this->potLimitOmaha4CardhighMidLowCard();
		$bigBlindCase = ['SMALL' ,'MID' ,'HIGH'];
		$pot_temp=[];
		foreach($bigBlindCase as $key => $bigBlindCaseValue ){
			$pot_temp[$bigBlindCaseValue] = null;
			foreach($potLimitOmaha4CardhighMidLowCard as $key=> $potLimitOmaha4CardhighMidLowCardValue)
			{
				if($bigBlindCaseValue === $potLimitOmaha4CardhighMidLowCardValue->BIG_BLIND_CASE){
					if($potLimitOmaha4CardhighMidLowCardValue->GROSS_RAKE != ''){
						$potLimitOmaha4CardhighMidLowCardValue->GROSS_RAKE = $this->numberExtChange($potLimitOmaha4CardhighMidLowCardValue->GROSS_RAKE);
					}
					if($potLimitOmaha4CardhighMidLowCardValue->NET_RAKE != ''){
						$potLimitOmaha4CardhighMidLowCardValue->NET_RAKE = $this->numberExtChange($potLimitOmaha4CardhighMidLowCardValue->NET_RAKE);
					}
					if($potLimitOmaha4CardhighMidLowCardValue->BONUS_REVENUE != ''){
						$potLimitOmaha4CardhighMidLowCardValue->BONUS_REVENUE = $this->numberExtChange($potLimitOmaha4CardhighMidLowCardValue->BONUS_REVENUE);
					}
					$pot_temp[$bigBlindCaseValue] = $potLimitOmaha4CardhighMidLowCardValue;
				}
			}
		}
		#card Pot Limit Omaha(By stakes):       
		$cardPotLimit5Omaha = $this->cardPotLimit5OmahaByStacks($date_from,$date_to);
		$cardPotLimit5OmahaByStacks = [];
		if($cardPotLimit5Omaha != []){	
			foreach($cardPotLimit5Omaha as $index => $potlimit5_card_value) {
				$temp = null;
				$temp = new stdClass();
				$temp->GROSS_RAKE = $this->numberExtChange($potlimit5_card_value->GROSS_RAKE);
				$temp->NET_RAKE = $this->numberExtChange($potlimit5_card_value->NET_RAKE);
				$temp->BONUS_REVENUE = $this->numberExtChange($potlimit5_card_value->BONUS_REVENUE);
				$temp->STAKES = $potlimit5_card_value->STAKES;
				$temp->FIVE_CARD_PLAYING_USER = $potlimit5_card_value->FIVE_CARD_PLAYING_USER;
				$temp->NUMBER_OF_HANDS = $potlimit5_card_value->NUMBER_OF_HANDS;
				$temp->ARPU = $potlimit5_card_value->ARPU;
				$cardPotLimit5OmahaByStacks[] = $temp;
			}
		}
		#card Pot Limit Omaha(High ,mid & low)   
		$cardPotLimit5OmahaByHighMidLow = $this->cardPotLimit5OmahaByHighMidLow($date_from, $date_to);
		$bigBlindCasePot5 = ['SMALL' ,'MID' ,'HIGH'];
		$pot5Card_temp=[];
		foreach($bigBlindCasePot5 as $key => $bigBlindCaseValue ){
			$pot5Card_temp[$bigBlindCaseValue] = null;
			foreach($cardPotLimit5OmahaByHighMidLow as $key=> $cardPotLimit5OmahaByHighMidLowValue)
			{
				if($bigBlindCaseValue === $cardPotLimit5OmahaByHighMidLowValue->BIG_BLIND_CASE){
					if($cardPotLimit5OmahaByHighMidLowValue->GROSS_RAKE != ''){
						$cardPotLimit5OmahaByHighMidLowValue->GROSS_RAKE = $this->numberExtChange($cardPotLimit5OmahaByHighMidLowValue->GROSS_RAKE);
					}
					if($cardPotLimit5OmahaByHighMidLowValue->NET_RAKE != ''){
						$cardPotLimit5OmahaByHighMidLowValue->NET_RAKE = $this->numberExtChange($cardPotLimit5OmahaByHighMidLowValue->NET_RAKE);
					}
					if($cardPotLimit5OmahaByHighMidLowValue->BONUS_REVENUE != ''){
						$cardPotLimit5OmahaByHighMidLowValue->BONUS_REVENUE = $this->numberExtChange($cardPotLimit5OmahaByHighMidLowValue->BONUS_REVENUE);
					}
					$pot5Card_temp[$bigBlindCaseValue] = $cardPotLimit5OmahaByHighMidLowValue;
				}
			}
		}
		
		#ofc(stacks)     
		$ofc_Stack = $this->ofcByStacks($date_from, $date_to);
		$ofcByStack = [];
		if($ofc_Stack != []){	
			foreach($ofc_Stack as $index => $ofc_Stack_value) {
				$temp = null;
				$temp = new stdClass();
				$temp->GROSS_RAKE = $this->numberExtChange($ofc_Stack_value->GROSS_RAKE);
				$temp->NET_RAKE = $this->numberExtChange($ofc_Stack_value->NET_RAKE);
				$temp->BONUS_REVENUE = $this->numberExtChange($ofc_Stack_value->BONUS_REVENUE);
				$temp->POINT_VALUE = $ofc_Stack_value->POINT_VALUE;
				$temp->PLAYING_USER = $ofc_Stack_value->PLAYING_USER;
				$temp->NUMBER_OF_HANDS = $ofc_Stack_value->NUMBER_OF_HANDS;
				$temp->ARPU = $ofc_Stack_value->ARPU;
				$ofcByStack[] = $temp;
			}
		}
		#ofc (high mid low)     
		$ofcHighMidLow = $this->ofcHighMidLow();
		$bigBlindCaseOfc = ['SMALL' ,'MID' ,'HIGH'];
		$Ofc_temp=[];
		foreach($bigBlindCaseOfc as $key => $bigBlindCaseValue ){
			$Ofc_temp[$bigBlindCaseValue] = null;
			foreach($ofcHighMidLow as $key=> $ofcHighMidLowValue)
			{
				if($bigBlindCaseValue === $ofcHighMidLowValue->BIG_BLIND_CASE){
					if($ofcHighMidLowValue->GROSS_RAKE != ''){
						$ofcHighMidLowValue->GROSS_RAKE = $this->numberExtChange($ofcHighMidLowValue->GROSS_RAKE);
					}
					if($ofcHighMidLowValue->NET_RAKE != ''){
						$ofcHighMidLowValue->NET_RAKE = $this->numberExtChange($ofcHighMidLowValue->NET_RAKE);
					}
					if($ofcHighMidLowValue->BONUS_REVENUE != ''){
						$ofcHighMidLowValue->BONUS_REVENUE = $this->numberExtChange($ofcHighMidLowValue->BONUS_REVENUE);
					}
					$Ofc_temp[$bigBlindCaseValue] = $ofcHighMidLowValue;
				}
			}
		}
		
		#Game type(Texas ,Plo,5card plo,ofc)
		$gameTypeTexasPlo5cardPloOfc = $this->gameTypeTexasPlo5cardPloOfc($date_from, $date_to);
		$minigamesTypeName = ['Five cards omaha' ,'OFC' ,'Omaha' ,"Texas Hold'em"];
		
		$m_temp=[];
		foreach($minigamesTypeName as $key => $minigamesTypeNameValue ){
			
			$m_temp[$minigamesTypeNameValue] = null;
			foreach($gameTypeTexasPlo5cardPloOfc as $key=> $gameTypeTexasPlo5cardPloOfcValue)
			{
				if($minigamesTypeNameValue === $gameTypeTexasPlo5cardPloOfcValue->MINIGAMES_TYPE_NAME){
					
					if($gameTypeTexasPlo5cardPloOfcValue->GROSS_RAKE !=''){
						$gameTypeTexasPlo5cardPloOfcValue->GROSS_RAKE = $this->numberExtChange($gameTypeTexasPlo5cardPloOfcValue->GROSS_RAKE);
					}
					if($gameTypeTexasPlo5cardPloOfcValue->NET_RAKE !=''){
						$gameTypeTexasPlo5cardPloOfcValue->NET_RAKE = $this->numberExtChange($gameTypeTexasPlo5cardPloOfcValue->NET_RAKE);
					}
					if($gameTypeTexasPlo5cardPloOfcValue->BONUS_REVENUE !=''){
						$gameTypeTexasPlo5cardPloOfcValue->BONUS_REVENUE = $this->numberExtChange($gameTypeTexasPlo5cardPloOfcValue->BONUS_REVENUE);
					}
					$m_temp[$minigamesTypeNameValue] = $gameTypeTexasPlo5cardPloOfcValue;
				}
			}
		}
	
		#Game type (Tournament):
		$gameTypeTour = $this->gameTypeTournament();
		
		$gameTypeTournament = [];
		if($gameTypeTour !=''){
			foreach($gameTypeTour as $index => $gameType_Tournament) {
				$temp = null;
				$temp = new stdClass();
				$temp->DATE1 = $gameType_Tournament->DATE1;
				$temp->PLAYING_USER = $gameType_Tournament->PLAYING_USER;
				$temp->ARPU = $gameType_Tournament->ARPU;
				$temp->GGR = $this->numberExtChange($gameType_Tournament->GGR);
				$temp->NGR = $this->numberExtChange($gameType_Tournament->NGR);
				$temp->REVENUE_GENERATED_THROUGH_BONUS = $this->numberExtChange($gameType_Tournament->REVENUE_GENERATED_THROUGH_BONUS);
				$gameTypeTournament[] = $temp;
			}
			
		}
		
		#Game type(High,mid& low)
		$gameTypeHighMidLow = $this->gameTypeHighMidLow();
		$big_blinds = ['SMALL' ,'MID' ,'HIGH'];
		$gameTypeHighMidLow_temp=[];
		foreach($big_blinds as $key => $big_blindsValue ){
			$gameTypeHighMidLow_temp[$big_blindsValue] = null;
			foreach($gameTypeHighMidLow as $key=> $gameTypeHighMidLowValue)
			{
				if($big_blindsValue === $gameTypeHighMidLowValue->BIG_BLIND_CASE){
					if($gameTypeHighMidLowValue->GROSS_RAKE !=''){
						$gameTypeHighMidLowValue->GROSS_RAKE = $this->numberExtChange($gameTypeHighMidLowValue->GROSS_RAKE);
					}
					if($gameTypeHighMidLowValue->NET_RAKE !=''){
						$gameTypeHighMidLowValue->NET_RAKE = $this->numberExtChange($gameTypeHighMidLowValue->NET_RAKE);
					}
					if($gameTypeHighMidLowValue->BONUS_REVENUE !=''){
						$gameTypeHighMidLowValue->BONUS_REVENUE = $this->numberExtChange($gameTypeHighMidLowValue->BONUS_REVENUE);
					}
					$gameTypeHighMidLow_temp[$big_blindsValue] = $gameTypeHighMidLowValue;
				}
			}
		}
		#Net Cash (Including Liabilities)Current month depositCurrent month Current Month Net cash	-Monthly Liability Change
		 $depo_monts = $this->currentMonthDepositWithdrawalNetCashLiability();
	
		if($depo_monts != []){
			
			$object = json_decode(json_encode($depo_monts),true);
			$currentMonthDepositWithdrawalNetCashLiability  = json_decode(json_encode($object[0]), FALSE);
			if($currentMonthDepositWithdrawalNetCashLiability->deposit_amount !=''){
				$depo_amount = $this->numberExtChange($currentMonthDepositWithdrawalNetCashLiability->deposit_amount);
			}else{
				$depo_amount = '';
			}
			if($currentMonthDepositWithdrawalNetCashLiability->withdrawing_amount !=''){
				$withdra_amount = $this->numberExtChange($currentMonthDepositWithdrawalNetCashLiability->withdrawing_amount);
			}else{
				$withdra_amount = '';
			}
			if($currentMonthDepositWithdrawalNetCashLiability->Net_cash !=''){
				$net_amount = $this->numberExtChange($currentMonthDepositWithdrawalNetCashLiability->Net_cash);
			}else{
				$net_amount = '';
			}
		}else{		
			$depo_amount = '';
			$withdra_amount = '';
			$net_amount = '';
		}
		$monthlyTdsAffComm = $this->monthlyTdsAffCOMM();
		$monthlyTdsPlayerComm = $this->monthlyTdsPlayerCOMM();
	
		$monthlyComm = ($monthlyTdsAffComm[0]->Affiliate_commissions + $monthlyTdsPlayerComm[0]->Player_commissions);
		if($monthlyComm != ''){
			$monthlyCOMM = $this->numberExtChange($monthlyComm);
		}else{
			$monthlyCOMM = '';
		}
		$playerCommTds = $this->playerCommTDS();
		$playerwinComm = $this->playerwinTDS();
		$oldplayerwinComm = $this->oldqueryPlayerWinTDS();
		$affCommTds = $this->affiCommTDS();
		
		$monthlyTds = ($playerCommTds[0]->Player_commission_TDS + $playerwinComm[0]->Player_winnings_TDS+ $oldplayerwinComm[0]->Player_winnings_TDS_OLD + $affCommTds[0]->Affiliate_Commission_TDS);
		
		if($monthlyTds != ''){
			$monthlyTDS = $this->numberExtChange($monthlyTds);
		}else{
			$monthlyTDS = '';
		}
		$yesterday_date = $data['yesterday_date'] = date('j-F-Y', strtotime("-1 day"));
		$date = $data['date'] = date('Y-m-d', strtotime("-1 day"));
		
		$pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('bo.views.exportpdf.pbdailystatspdf', [
			'grossRevenue'=>$grossRevenue,
			'bonusRevenue'=>$bonusRevenue,
			'netRevenue'=>$netRevenue,
			'monthlyCOMM'=>$monthlyCOMM,
			'monthlyTDS'=>$monthlyTDS,
			'ngrGGR'=>$ngrGGR,
			'deposit'=>$deposit,
			'withdraw'=>$withdraw,
			'netCash'=>$netCash,
			'userDAULogins'=>$userDAULogins,
			'signUp'=>$signUps,
			'TPC'=>$TPC,
			'WPB'=>$WPB,
			'monthLiaChng'=>$monthLiaChng,
			'netCashIncLia'=>$netCashIncLia,
			'changeInWPB'=>$changeInWPB,
			'percentchangeInWPB'=>$percentchangeInWPB,
			'pendingWithdrawalAmount'=>$pendingWithdrawalAmount,
			'ftdReports'=>$ftdReports,
			'ftwsReport'=>$ftwsReport,
			'uniqueWithdrawingUser'=>$uniqueWithdrawingUser,
			'uniqueDepositer'=>$uniqueDepositer,
			'UniqueWageringUser'=>$UniqueWageringUser,
			'lifetimeDepositorsWagering'=>$lifetimeDepositorsWagering,
			'wageringusers_non_depositors'=>$wageringusers_non_depositors,
			'uniqueFreerollPlayingUsers'=>$uniqueFreerollPlayingUsers,
			'partnerToSite'=>$partnerToSite_temp,
			'rCC'=>$rCC,
			'rCB'=>$rCB,
			'tournamentTicketsExpense'=>$tournamentTicketsExpense,
			'tournamentOverlays'=>$tournamentOverlays,
			'stacks'=>$stacks,
			'texasHoldemBigBlindStacksSmallHighMid'=>$texas_temp,
			'potLimitOmaha4card'=>$potLimitOmaha4card,
			'potLimitOmaha4CardhighMidLowCard'=>$pot_temp,
			'cardPotLimit5OmahaByStacks'=>$cardPotLimit5OmahaByStacks,
			'cardPotLimit5OmahaByHighMidLow'=>$pot5Card_temp,
			'ofcByStack'=>$ofcByStack,
			'ofcHighMidLow'=>$Ofc_temp,
			'gameTypeTexasPlo5cardPloOfc'=>$m_temp,
			'gameTypeTournament'=>$gameTypeTournament,
			'gameTypeHighMidLow'=>$gameTypeHighMidLow_temp,
			'depo_amount'=>$depo_amount,                          //current month amt
			'withdra_amount'=>$withdra_amount,						//current month amt
			'net_amount'=>$net_amount,								//current month amt
			'yesterday_date'=>$yesterday_date
		])->setPaper('a4','landscape');
		//$file = "PBDaily_Stats_Report_" . $data['date'] . ".pdf";
		//return $pdf->download($file.".pdf");
		$dataArr = $pdf->output();
		
		## get file location
		
		$file = "PBDaily_Stats_Report_" . $data['date'] . ".pdf";
		## check file and delete if exists
		if (Storage::disk('local')->exists($file)) {
			Storage::delete($file);
		}
		Storage::put($file, $dataArr);
		
		## generate new file and then send mail.
		if (Storage::disk('local')->exists($file)) {
			Mail::send("bo.mail.pbDailyStatsReportMail", $data, function ($message) use($data, $dataArr, $file) {
				$message->from(config('poker_config.mail.from.analytics'), 'PB Daily Stats Report');
				$message->to(config('poker_config.mail.from.analytics'))
				->bcc(config('poker_config.pbdailystats_report_mail_to.emails'))
						->subject('PokerBaazi Daily Stats Report for ' . " " . $data['yesterday_date']);
				
					$message->attach(storage_path('app/' . $file), [
						'as' => $file,
						'mime' => 'application/pdf'
					]);
				
			}); 
			if (count(Mail::failures()) <= 0 && Storage::disk('local')->exists($file)) {
				Storage::delete($file);
			}
		}
		
		return response()->json(["status" => 200, "message" => "success"]);
	}
	
	public function grossBonusNetNGRGGRRevenue(){
		return $result = DB::connection('slave')->select("SELECT GROSS_REVENE1+GROSS_REVENE_OFC+GGR_T AS GROSS_REVENE,BONUS_REVENUE1+BONUS_REVENUE_OFC+B_REV_T  AS BONUS_REVENUE,ROUND(NET_REVENUE1+NET_RAKE_OFC+NET_REV_T,2) AS NET_REVENUE,
		ROUND(((NET_REVENUE1+NET_RAKE_OFC+NET_REV_T)/(GROSS_REVENE1+GROSS_REVENE_OFC+GGR_T) * 100),0) AS 'NGRGGR'
		 FROM
		(SELECT DATE(REPORT_DATE) AS DATE1,SUM(TOTAL_RAKE) AS GROSS_REVENE1,SUM(TOTAL_BONUS_REVENUE) AS BONUS_REVENUE1,SUM((ACTUAL_REVENUE*TOTAL_REAL_REVENUE)/TOTAL_RAKE) AS NET_REVENUE1
		FROM user_turnover_report_daily WHERE TOTAL_RAKE > 0 AND REPORT_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59') GROUP BY DATE1)a
		JOIN
		(
		SELECT DATE(STARTED) AS DATE1,SUM(REVENUE) AS GROSS_REVENE_OFC,SUM(BONUS_REVENUE) AS BONUS_REVENUE_OFC,
		SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE) AS NET_RAKE_OFC
		FROM ofc_game_transaction_history WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59') GROUP BY DATE1)b ON a.DATE1=b.DATE1
		JOIN
		(
		SELECT DATE(TRANSACTION_DATE) AS DATE1,SUM(TRANSACTION_AMOUNT) AS GGR_T,SUM(CASE WHEN BALANCE_TYPE_ID IN (1,3) THEN TRANSACTION_AMOUNT END) AS NET_REV_T,
		SUM(CASE WHEN BALANCE_TYPE_ID=2 THEN TRANSACTION_AMOUNT END) AS B_REV_T
		FROM master_transaction_history WHERE TRANSACTION_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		AND TRANSACTION_TYPE_ID IN (25,102,105) GROUP BY DATE1)c ON a.DATE1=c.DATE1;");
		
	}
	public function depositsWithdrawalsNetCash(){
		return $result = DB::Connection('slave')->select("SELECT a.DEPOSIT_AMOUNT,b.WITHDRAWING_AMOUNT,(a.DEPOSIT_AMOUNT-b.WITHDRAWING_AMOUNT) NET_CASH FROM (
		(SELECT SUM(PAYMENT_TRANSACTION_AMOUNT) DEPOSIT_AMOUNT,DATE(PAYMENT_TRANSACTION_CREATED_ON) D1
		FROM payment_transaction WHERE TRANSACTION_TYPE_ID IN (8,61,62,83,111) AND PAYMENT_TRANSACTION_STATUS IN (103,125)
		AND PAYMENT_TRANSACTION_CREATED_ON BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59') GROUP BY DATE(PAYMENT_TRANSACTION_CREATED_ON))a
		LEFT JOIN
		(SELECT SUM(PAYMENT_TRANSACTION_AMOUNT) WITHDRAWING_AMOUNT,DATE(PAYMENT_TRANSACTION_CREATED_ON) D2
		FROM payment_transaction WHERE TRANSACTION_TYPE_ID IN (10,74,80) AND PAYMENT_TRANSACTION_STATUS IN (208,209)
		AND PAYMENT_TRANSACTION_CREATED_ON BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY DATE(PAYMENT_TRANSACTION_CREATED_ON)) b ON a.D1= b.D2);");
		
	}
	public function withdrawalPlayerBalance(){
		return $result = DB::connection('slave')->select('select sum(USER_DEPOSIT_BALANCE+USER_WIN_BALANCE) as bal_1 from accumulated_daily_userpoints_balance where date(started)=adddate(curdate(),interval -1 day) and coin_type_id=1;');
	}
	public function balTwo(){
		return $result = DB::connection('slave')->select('select sum(USER_DEPOSIT_BALANCE+USER_WIN_BALANCE) as bal_2 from accumulated_daily_userpoints_balance where date(started)=adddate(curdate(),interval -2 day) and coin_type_id=1;');
	}
	public function balThree(){
		return $result = DB::connection('slave')->select('select sum(USER_DEPOSIT_BALANCE+USER_WIN_BALANCE) as bal_3 from accumulated_daily_userpoints_balance where date(started)= last_day(adddate(curdate(),interval -1 month)) and coin_type_id=1;');
	}
	public function pendingWithdrawalAmount(){
		return $result = DB::connection('slave')->select("SELECT SUM(PAYMENT_TRANSACTION_AMOUNT) PENDING_WITHDRAWAL_AMOUNT,DATE(PAYMENT_TRANSACTION_CREATED_ON) D2
		FROM payment_transaction WHERE TRANSACTION_TYPE_ID IN (10) AND PAYMENT_TRANSACTION_STATUS IN (109)
		AND PAYMENT_TRANSACTION_CREATED_ON BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY DATE(PAYMENT_TRANSACTION_CREATED_ON);");
	}
	public function totalPromoCost(){
		return $result = DB::connection('slave')->select("SELECT SUM(PROMO1+PROMO2) AS TOTAL_PROMO_COST FROM (
		(SELECT DATE(ADJUSTMENT_CREATED_ON) AS DATE1,
		 COALESCE(SUM(CASE WHEN TRANSACTION_TYPE_ID = 21 AND ADJUSTMENT_ACTION = 'Add'
			THEN ADJUSTMENT_AMOUNT END ),0)  -
		 COALESCE(SUM(CASE WHEN TRANSACTION_TYPE_ID = 21 AND ADJUSTMENT_ACTION = 'subtract'
			THEN ADJUSTMENT_AMOUNT END ),0) +
		 COALESCE(SUM(CASE WHEN TRANSACTION_TYPE_ID = 22 AND ADJUSTMENT_ACTION = 'Add'
			THEN ADJUSTMENT_AMOUNT END ),0) -
		 COALESCE(SUM(CASE WHEN TRANSACTION_TYPE_ID = 22 AND ADJUSTMENT_ACTION = 'subtract'
			THEN ADJUSTMENT_AMOUNT END ),0) +
		 COALESCE(SUM(CASE WHEN TRANSACTION_TYPE_ID = 23 AND ADJUSTMENT_ACTION = 'Add'
			THEN ADJUSTMENT_AMOUNT END ),0) -
		  COALESCE(SUM(CASE WHEN TRANSACTION_TYPE_ID = 23 AND (ADJUSTMENT_ACTION IN('subtract','Remove')) THEN ADJUSTMENT_AMOUNT END ),0) AS PROMO2
		FROM adjustment_transaction
		WHERE ADJUSTMENT_CREATED_ON BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		 GROUP BY date(ADJUSTMENT_CREATED_ON))a
		 JOIN
		(SELECT SUM(mth.TRANSACTION_AMOUNT) AS PROMO1,DATE(TRANSACTION_DATE) DATE2
		FROM master_transaction_history mth WHERE mth.TRANSACTION_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		 AND mth.TRANSACTION_TYPE_ID IN (9,27,65,86,117)GROUP BY DATE2)b ON a.DATE1 = b.DATE2)
		");
	}
	public function percentchangeInWPB(){
		return $result = DB::connection('slave')->select("SELECT ROUND(((WITHDRAWABLE_PLAYER_BALANCE-WITHDRAWABLE_PLAYER_BALANCE_1)/WITHDRAWABLE_PLAYER_BALANCE_1 *100),0)AS '% Change in WPB'
		FROM (
		SELECT DATE(UPDATED_DATE) DATE1,SUM(USER_DEPOSIT_BALANCE+USER_WIN_BALANCE) WITHDRAWABLE_PLAYER_BALANCE
		FROM user_points
		WHERE UPDATED_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59') GROUP BY DATE1 )a
		JOIN
		(
		SELECT DATE(ADDDATE(UPDATED_DATE,INTERVAL 1 DAY)) DATE1,SUM(USER_DEPOSIT_BALANCE+USER_WIN_BALANCE) WITHDRAWABLE_PLAYER_BALANCE_1
		FROM user_points
		WHERE UPDATED_DATE BETWEEN curdate()- INTERVAL 2 DAY AND CONCAT(curdate()- INTERVAL 2 DAY,' 23:59:59') GROUP BY DATE1)b ON a.DATE1=b.DATE1;
		");
	}
	
	public function userDAUsLogin($date_from,$date_to){
		$q1 = DB::connection('slave')->table('user_turnover_report_daily')
					->whereBetween('REPORT_DATE', [$date_from, $date_to])
					->selectRaw('DISTINCT user_id AS Daus');
		$q2 = DB::connection('slave')->table('tournament_registration')
					->whereBetween('UPDATED_DATE', [$date_from, $date_to])
					->selectRaw('DISTINCT user_id AS Daus');
		$q3 = DB::connection('slave')->table('ofc_game_transaction_history')
					->whereBetween('STARTED', [$date_from, $date_to])
					->selectRaw('DISTINCT user_id AS Daus');
		 return $result = $q1->union($q2)->union($q3)->count();
		
	}
	public function signUpLogins($date_from,$date_to){
		return $result = DB::connection('slave')->table('user')
		->select([
			'user_id',
        ])
		->whereBetween('REGISTRATION_TIMESTAMP', [$date_from, $date_to])
		->count();
	}
	public function ftdsReports($date_from,$date_to){
		return $result = DB::connection('slave')->table('first_deposit_daily')
		->selectRaw('COUNT(DISTINCT USER_ID) AS ftds')
		->whereBetween('PAYMENT_TRANSACTION_CREATED_ON', [$date_from, $date_to])
		->get();
		
	}
	public function ftwsReports(){
		return $result = DB::connection('slave')->select("SELECT COUNT(*) ftws FROM (
		SELECT COUNT(DISTINCT USER_ID)
		FROM payment_transaction WHERE TRANSACTION_TYPE_ID=10 AND PAYMENT_TRANSACTION_STATUS IN (208,209) GROUP BY USER_ID
		HAVING MIN(PAYMENT_TRANSACTION_CREATED_ON) BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59'))a;
		");
	}
	
	public function uniqueWithdrawingUser($date_from,$date_to){
		return $result = DB::connection('slave')->table('payment_transaction')
		->WhereIn('TRANSACTION_TYPE_ID',['10','74','80'])
		->whereIn('PAYMENT_TRANSACTION_STATUS',['208','209'])
		->whereBetween('PAYMENT_TRANSACTION_CREATED_ON', [$date_from, $date_to])
		->selectRaw('COUNT(DISTINCT USER_ID) Unique_Withdrawing_users')->get();
	}
	public function uniqueDepositer($date_from,$date_to){ 
		return $result = DB::connection('slave')->table('payment_transaction')
		->WhereIn('TRANSACTION_TYPE_ID',['8','61','62','83','111'])
		->whereIn('PAYMENT_TRANSACTION_STATUS',['103','125'])
		->whereBetween('PAYMENT_TRANSACTION_CREATED_ON', [$date_from, $date_to])
		->selectRaw('COUNT(DISTINCT USER_ID) unique_dep')->get();
		
	}
	public function UniqueWageringUsers($date_from,$date_to){
		
		$q1 = DB::connection('slave')->table('user_turnover_report_daily')
					->whereBetween('REPORT_DATE', [$date_from, $date_to])
					->selectRaw('DISTINCT USER_ID');
		$q2 = DB::connection('slave')->table('ofc_game_transaction_history')
					->whereBetween('STARTED', [$date_from, $date_to])
					->selectRaw('DISTINCT USER_ID');
		return $result = $q1->union($q2)->count();
	}
	public function lifetimeDepositorsWagering(){
		return $result = DB::Connection('slave')->select("SELECT COUNT(*) Lifetime_Depositors_Wagering FROM (
		SELECT DISTINCT USER_ID FROM user_turnover_report_daily WHERE REPORT_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		UNION
		SELECT DISTINCT USER_ID FROM ofc_game_transaction_history WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59'))a
		WHERE USER_ID IN (SELECT DISTINCT USER_ID FROM payment_transaction WHERE TRANSACTION_TYPE_ID IN (8,61,62,83,111) AND PAYMENT_TRANSACTION_STATUS IN (103,125))");
	}
	
	public function wageringusersNonDepositors(){
		return $result = DB::Connection('slave')->select("SELECT COUNT(*) Wagering_users FROM (
		SELECT DISTINCT USER_ID FROM user_turnover_report_daily WHERE REPORT_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		UNION
		SELECT DISTINCT USER_ID FROM ofc_game_transaction_history WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59'))a
		WHERE USER_ID NOT IN (SELECT DISTINCT USER_ID FROM payment_transaction WHERE TRANSACTION_TYPE_ID IN (8,61,62,83,111) AND PAYMENT_TRANSACTION_STATUS IN (103,125));");
	}
	public function uniqueFreerollPlayingUsers($date_from,$date_to){
		return $query = DB::connection('slave')->select("SELECT COUNT(DISTINCT USER_ID) Unique_freeroll_playing_users FROM tournament_registration tr  JOIN tournament t ON tr.TOURNAMENT_ID=t.TOURNAMENT_ID  WHERE BUYIN=0 AND TOURNAMENT_START_TIME BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')");
		
	}
	public function partnerToSite(){
		return $query1 = DB::connection('slave')->select("SELECT Bonus_categories,Amount,Unique_users,No_of_txns,Avg_amt FROM (
		(SELECT * ,(CASE WHEN gh.TRANSACTION_TYPE_ID = 65 THEN 'Signup_promo'
		WHEN gh.TRANSACTION_TYPE_ID = 9 THEN 'Deposit_promo'
		WHEN gh.TRANSACTION_TYPE_ID = 27 THEN 'Locked_promo_released'
		WHEN gh.TRANSACTION_TYPE_ID = 86 THEN 'Partner_to_site'
		ELSE NULL END)Bonus_categories FROM(
		SELECT SUM(mth.TRANSACTION_AMOUNT)Amount,COUNT(DISTINCT USER_ID)Unique_users,COUNT(USER_ID) AS No_of_txns,mth.TRANSACTION_TYPE_ID,
		(SUM(mth.TRANSACTION_AMOUNT)/COUNT(DISTINCT USER_ID))Avg_amt
		FROM master_transaction_history mth WHERE mth.TRANSACTION_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		AND mth.TRANSACTION_TYPE_ID IN (9,27,65,86)and transaction_amount > 0 GROUP BY mth.TRANSACTION_TYPE_ID) gh))b;");
	}
	public function rCB(){
		return $query = DB::connection('slave')->select("SELECT SUM(PROMO_PRICE) RCB_AMOUNT,COUNT(DISTINCT USER_ID) RCB_UINQUE_USERS,COUNT(USER_ID) RCB_NO_OF_TXN,(SUM(PROMO_PRICE)/COUNT(DISTINCT USER_ID)) RCB_AVG_PROMO
		FROM reward_points_conversion_history WHERE CREATED_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		AND PROMO_PRICE > 0 GROUP BY DATE(CREATED_DATE)");
		
	}
	public function rCC(){
		return $query = DB::connection('slave')->select('SELECT SUM(REAL_PRICE) RCC_AMOUNT,COUNT(DISTINCT USER_ID) RCC_UNIQUE_USERS,COUNT(USER_ID) RCC_NO_OF_TXN,(SUM(REAL_PRICE)/COUNT(DISTINCT USER_ID)) RCC_AVG_PROMO
		FROM reward_points_conversion_history WHERE CREATED_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY," 23:59:59")
		AND REAL_PRICE > 0 GROUP BY DATE(CREATED_DATE);');
	}
	public function tournamentTicketsExpense($date_from, $date_to){
		return $query = DB::connection('slave')->table('tournament_turnover_history')
		->selectRaw('SUM(BUY_IN*PROMO_TICKETS_COUNT) TOURNAMENT_TICKETS_EXPENSE_AMOUNT,SUM(PROMO_TICKETS_COUNT) TOURNAMENT_TICKETS_EXPENSE_NO_OF_TXN')
		->whereBetween('TOURNAMENT_START_TIME', [$date_from, $date_to])
		->where('BUY_IN','>',1)
		->get();
	}
	public function tournamentOverlays(){
		return $query = DB::connection('slave')->select("SELECT SUM(TOURNAMENT_OVERLAYS_AMOUNT) TOURNAMENT_OVERLAYS_AMOUNT,COUNT(CASE WHEN TOURNAMENT_OVERLAYS_AMOUNT>1 THEN TOURNAMENT_ID END) OVERLAY_TOURNAMENT_COUNT,
		COUNT(*) TOTAL_TOURNAMENT FROM(
		SELECT t.TOURNAMENT_ID,t.TOURNAMENT_NAME,DATE1,
		IF((SUM(PRIZE_GIVEN)-SUM(BUY_IN*(CASH_TICKETS_COUNT+SATELLITE_TICKETS_COUNT+PROMO_TICKETS_COUNT))-SUM(REBUY_IN*REBUYS_COUNT)-SUM(ADDON*ADDONS_COUNT))>0,
		SUM(PRIZE_GIVEN)-SUM(BUY_IN*(CASH_TICKETS_COUNT+SATELLITE_TICKETS_COUNT+PROMO_TICKETS_COUNT))-SUM(REBUY_IN*REBUYS_COUNT)-SUM(ADDON*ADDONS_COUNT),0) TOURNAMENT_OVERLAYS_AMOUNT,
		PROMO_TICKETS_COUNT,PROMO_TICKETS_COUNT*BUY_IN PROMO_TICKETS_COST
		FROM tournament_turnover_history t
		JOIN
		(SELECT TOURNAMENT_ID,BUYIN,DATE(TOURNAMENT_START_TIME) DATE1 FROM tournament
		WHERE TOURNAMENT_START_TIME BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		AND BUYIN>1 )a ON t.TOURNAMENT_ID=a.TOURNAMENT_ID GROUP BY t.TOURNAMENT_ID, t.TOURNAMENT_NAME,DATE1,PROMO_TICKETS_COUNT,BUY_IN)a;");
		
	}
	public function texasHoldemBigBlindStacks($date_from,$date_to){
	
		return $query = DB::connection('slave')->table('game_transaction_history as gth')
		->selectRaw('COUNT(DISTINCT gth.USER_ID) AS PLAYING_USER_TEXAS,t.BIG_BLIND,COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
		SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.USER_ID)),2) ARPU')
		->join('tournament_tables as tt', 'gth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->where('MINIGAMES_TYPE_NAME', 'like', '%' . 'Texas Hol' . '%')
		->groupBy('t.BIG_BLIND')->orderBy('t.BIG_BLIND', 'ASC')->get();
	}
	public function texasHoldemBigBlindStacksSmallHighMid($date_from,$date_to){
		
		return $query = DB::connection('slave')->table('game_transaction_history as gth')
		->selectRaw('COUNT(DISTINCT gth.USER_ID) AS PLAYING_USER_TEXAS,
			(CASE WHEN t.BIG_BLIND  <= 10 THEN "SMALL" WHEN t.BIG_BLIND > 10 AND t.BIG_BLIND <=200 THEN "MID" WHEN t.BIG_BLIND >200 THEN "HIGH" ELSE NULL END) AS BIG_BLIND_CASE,
			COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
			SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
			ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.USER_ID)),2) ARPU')
		->join('tournament_tables as tt', 'gth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->where('MINIGAMES_TYPE_NAME', 'like', '%' . 'Texas Hol' . '%')
		->groupBy('BIG_BLIND_CASE')->orderBy('BIG_BLIND_CASE', 'DESC')->get();
	}
	public function potLimitOmaha4card(){
		return $query = DB::connection('slave')->select("SELECT COUNT(DISTINCT gth.USER_ID) AS PLAYING_USER,t.BIG_BLIND,COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
		SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.USER_ID)),2) ARPU
		FROM game_transaction_history gth INNER JOIN tournament_tables tt ON gth.TOURNAMENT_TABLE_ID=tt.TOURNAMENT_TABLE_ID
		INNER JOIN tournament t ON tt.TOURNAMENT_ID=t.TOURNAMENT_ID
		WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		AND MINIGAMES_TYPE_NAME LIKE 'Omaha%' GROUP BY t.BIG_BLIND ORDER BY t.BIG_BLIND;");
	}
	public function potLimitOmaha4CardhighMidLowCard(){
		
		return $query = DB::connection('slave')->select("SELECT COUNT(DISTINCT gth.USER_ID) AS PLAYING_USER, MINIGAMES_TYPE_NAME,
		(CASE WHEN t.BIG_BLIND  <= 10 THEN 'SMALL'WHEN t.BIG_BLIND >10  AND t.BIG_BLIND <= 200 THEN 'MID' WHEN t.BIG_BLIND >200 THEN 'HIGH' ELSE NULL END) AS BIG_BLIND_CASE,
		COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
		SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.USER_ID)),2) ARPU
		FROM game_transaction_history gth
		INNER JOIN tournament_tables tt ON gth.TOURNAMENT_TABLE_ID=tt.TOURNAMENT_TABLE_ID
		INNER JOIN tournament t ON tt.TOURNAMENT_ID=t.TOURNAMENT_ID
		WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		AND MINIGAMES_TYPE_NAME LIKE 'Omaha%' GROUP BY BIG_BLIND_CASE,MINIGAMES_TYPE_NAME ORDER BY BIG_BLIND_CASE DESC;");
	}
	public function cardPotLimit5OmahaByStacks($date_from,$date_to){
		
		return $query = DB::connection('slave')->table('game_transaction_history as gth')
		->selectRaw('t.BIG_BLIND AS STAKES,SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,
		SUM(BONUS_REVENUE) AS BONUS_REVENUE,COUNT(DISTINCT gth.USER_ID) AS FIVE_CARD_PLAYING_USER,
		COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.user_id)),2) ARPU')
		->join('tournament_tables as tt', 'gth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->where('MINIGAMES_TYPE_NAME', 'like', '%' . 'Five cards omaha' . '%')
		->groupBy('t.BIG_BLIND')->groupBy('MINIGAMES_TYPE_NAME')->orderBy('MINIGAMES_TYPE_NAME', 'ASC')
		->orderBy('t.BIG_BLIND', 'ASC')->get();
	
	}
	public function cardPotLimit5OmahaByHighMidLow($date_from, $date_to){
		return $query = DB::connection('slave')->table('game_transaction_history as gth')
		->selectRaw('COUNT(DISTINCT gth.USER_ID) AS PLAYING_USER_OMAHA,
		(CASE WHEN t.BIG_BLIND  <= 10 THEN "SMALL" WHEN t.BIG_BLIND  >10 AND t.BIG_BLIND <= 200 THEN "MID" WHEN t.BIG_BLIND >200 THEN "HIGH" ELSE NULL END) AS BIG_BLIND_CASE,
		COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
		SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.USER_ID)),2) ARPU')
		->join('tournament_tables as tt', 'gth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->where('MINIGAMES_TYPE_NAME', 'like', '%' . 'Five cards omaha' . '%')
		->groupBy('BIG_BLIND_CASE')->orderBy('BIG_BLIND_CASE', 'DESC')->get();
	}
	public function ofcByStacks($date_from, $date_to){
		return $query = DB::connection('slave')->table('ofc_game_transaction_history as ogth')
		->selectRaw('COUNT(DISTINCT ogth.USER_ID) AS PLAYING_USER,MINIGAMES_TYPE_NAME,t.POINT_VALUE,COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
		SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT ogth.USER_ID)),2) ARPU')
		->join('tournament_tables as tt', 'ogth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->groupBy('t.POINT_VALUE','MINIGAMES_TYPE_NAME')
		->orderBy('t.POINT_VALUE','ASC')
		->get();
	}
	public function ofcHighMidLow(){
		return $query = DB::connection('slave')->select("SELECT COUNT(DISTINCT ogth.USER_ID) AS PLAYING_USER, MINIGAMES_TYPE_NAME,
		(CASE WHEN t.POINT_VALUE <= 10 THEN 'SMALL' WHEN t.POINT_VALUE >10 AND t.POINT_VALUE <= 200   THEN 'MID' WHEN t.POINT_VALUE >200 THEN 'HIGH' ELSE NULL END) AS BIG_BLIND_CASE,
		COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT ogth.USER_ID)),2) ARPU
		FROM ofc_game_transaction_history ogth
		INNER JOIN tournament_tables tt ON ogth.TOURNAMENT_TABLE_ID=tt.TOURNAMENT_TABLE_ID
		INNER JOIN tournament t ON tt.TOURNAMENT_ID=t.TOURNAMENT_ID
		WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY BIG_BLIND_CASE,MINIGAMES_TYPE_NAME ORDER BY MINIGAMES_TYPE_NAME,BIG_BLIND_CASE DESC;");
	}
	public function gameTypeTexasPlo5cardPloOfc($date_from, $date_to){ 
		$q1 = DB::connection('slave')->table('game_transaction_history as gth')
		->selectRaw('COUNT(DISTINCT gth.USER_ID) AS PLAYING_USER,MINIGAMES_TYPE_NAME,COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
		SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.USER_ID)),2) ARPU')
		->join('tournament_tables as tt', 'gth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->groupBy('MINIGAMES_TYPE_NAME')->orderBy('MINIGAMES_TYPE_NAME','asc');
		
		$q2 = DB::connection('slave')->table('ofc_game_transaction_history as ogth')
		->selectRaw('COUNT(DISTINCT ogth.USER_ID) AS PLAYING_USER,MINIGAMES_TYPE_NAME,COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
		SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT ogth.USER_ID)),2) ARPU')
		->join('tournament_tables as tt', 'ogth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->groupBy('MINIGAMES_TYPE_NAME')->orderBy('MINIGAMES_TYPE_NAME','asc');
		
		return $result = $q1->union($q2)->get();
	}
	public function gameTypeTournament(){ 
		return $query = DB::connection('slave')->select("SELECT DATE(TRANSACTION_DATE) AS DATE1,SUM(CASE WHEN BALANCE_TYPE_ID IN (1,2,3) THEN TRANSACTION_AMOUNT END) AS GGR,
		SUM(CASE WHEN BALANCE_TYPE_ID IN (1,3) THEN TRANSACTION_AMOUNT END) AS NGR,
		SUM(CASE WHEN BALANCE_TYPE_ID=2 THEN TRANSACTION_AMOUNT END) AS REVENUE_GENERATED_THROUGH_BONUS,
		COUNT(DISTINCT USER_ID) PLAYING_USER, ROUND(((SUM(CASE WHEN BALANCE_TYPE_ID IN (1,2,3) THEN TRANSACTION_AMOUNT END))/COUNT(DISTINCT USER_ID)),2) ARPU
		FROM master_transaction_history WHERE TRANSACTION_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')   AND TRANSACTION_TYPE_ID IN (25,102,105) AND TRANSACTION_AMOUNT>0 GROUP BY DATE1");
		
	}
	public function gameTypeHighMidLow(){
		
		return $result = DB::connection('slave')->select("SELECT COUNT(DISTINCT PLAYING_USER) AS PLAYING_USER,
		(CASE WHEN BIG_BLIND <= 10 THEN 'SMALL' WHEN (BIG_BLIND > 10 AND BIG_BLIND <=  200) THEN 'MID' WHEN BIG_BLIND > 200 THEN 'HIGH' ELSE NULL END ) AS BIG_BLIND_CASE,
		COUNT(DISTINCT NUMBER_OF_HANDS) AS NUMBER_OF_HANDS,SUM(GROSS_RAKE) AS GROSS_RAKE,SUM(NET_RAKE) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,ROUND((SUM(GROSS_RAKE)/COUNT(DISTINCT PLAYING_USER)),2) ARPU
		FROM
		( SELECT PLAYING_USER,BIG_BLIND,NUMBER_OF_HANDS,GROSS_RAKE,MINIGAMES_TYPE_NAME,NET_RAKE,BONUS_REVENUE
		FROM ( SELECT USER_ID AS PLAYING_USER,t.BIG_BLIND,PLAY_GROUP_ID AS NUMBER_OF_HANDS,MINIGAMES_TYPE_NAME,
		REVENUE AS GROSS_RAKE,ROUND(((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,
		(BONUS_REVENUE) AS BONUS_REVENUE FROM game_transaction_history gth
		INNER JOIN tournament_tables tt ON gth.TOURNAMENT_TABLE_ID=tt.TOURNAMENT_TABLE_ID
		INNER JOIN tournament t ON tt.TOURNAMENT_ID=t.TOURNAMENT_ID
		WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59') ) a
		UNION ALL
		SELECT PLAYING_USER,BIG_BLIND,NUMBER_OF_HANDS,GROSS_RAKE,NET_RAKE,BONUS_REVENUE,MINIGAMES_TYPE_NAME
		FROM ( SELECT USER_ID AS PLAYING_USER,POINT_VALUE AS BIG_BLIND,MINIGAMES_TYPE_NAME,
		PLAY_GROUP_ID AS NUMBER_OF_HANDS, REVENUE AS GROSS_RAKE,
		ROUND(((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,
		(BONUS_REVENUE) AS BONUS_REVENUE FROM ofc_game_transaction_history ogth
		INNER JOIN tournament_tables tt ON ogth.TOURNAMENT_TABLE_ID=tt.TOURNAMENT_TABLE_ID
		INNER JOIN tournament t ON tt.TOURNAMENT_ID=t.TOURNAMENT_ID
		WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')) b) c
		GROUP BY BIG_BLIND_CASE ORDER BY BIG_BLIND_CASE DESC;");
	}
	public function currentMonthDepositWithdrawalNetCashLiability(){
		return $result = DB::connection('slave')->select("select a.deposit_amount ,b.withdrawing_amount , (a.deposit_amount-b.withdrawing_amount) Net_cash  from ((
		(select sum(payment_transaction_amount) Deposit_Amount,month(PAYMENT_TRANSACTION_CREATED_ON)  D1
		from payment_transaction where TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111)
		 and PAYMENT_TRANSACTION_STATUS IN (103 , 125) and
		 month(PAYMENT_TRANSACTION_CREATED_ON) = month(curdate())
		 and year(PAYMENT_TRANSACTION_CREATED_ON) = year(curdate())
		 group by D1 ) ) a
		left join
		(select sum(payment_transaction_amount) withdrawing_amount ,
		 month(PAYMENT_TRANSACTION_CREATED_ON) D2
		from payment_transaction where TRANSACTION_TYPE_ID IN (10)
		 and PAYMENT_TRANSACTION_STATUS IN (208 , 209) and
		 month(PAYMENT_TRANSACTION_CREATED_ON) = month(curdate())
		 and year(PAYMENT_TRANSACTION_CREATED_ON) = year(curdate())
		 group by D2) b on a.D1= b.D2);
		");
	}
	public function monthlyTdsAffCOMM(){
		return $result = DB::connection('slave')->select("select sum(AMOUNT) as Affiliate_commissions from(
		select max(TRANSACTION_STATUS_ID) as tsid,AMOUNT,INTERNAL_REFERENCE_NO from  partners_transaction_details
		where TRANSACTION_TYPE_ID=108
		and month(CREATED_TIMESTAMP)=month(curdate())  and year(CREATED_TIMESTAMP)=year(curdate())  group by AMOUNT,INTERNAL_REFERENCE_NO)a
		where tsid in (233,231);");
	}
	public function monthlyTdsPlayerCOMM(){
		return $result = DB::connection('slave')->select("select sum(PAYMENT_TRANSACTION_AMOUNT)*.9625 as Player_commissions from payment_transaction_pokercommission
		where TRANSACTION_TYPE_ID=10 and PAYMENT_TRANSACTION_STATUS in (109,208)
		and month(PAYMENT_TRANSACTION_CREATED_ON)= month(curdate()) and year(PAYMENT_TRANSACTION_CREATED_ON)= year(curdate())  ;
		");
	}
	public function playerCommTDS(){
		return $result = DB::connection('slave')->select(" select sum(TRANSACTION_AMOUNT) as  Player_commission_TDS from  master_transaction_history_pokercommission
		where TRANSACTION_TYPE_ID=80 and TRANSACTION_STATUS_ID=208
		and month(TRANSACTION_DATE)=month(curdate()) and year(TRANSACTION_DATE)=year(curdate()) 
		");
	}
	public function playerwinTDS(){
		return $result = DB::connection('slave')->select("select sum(WITHDRAW_TDS) as Player_winnings_TDS from  withdraw_transaction_history where TRANSACTION_STATUS_ID=208 and BALANCE_TYPE_ID =3 and month(TRANSACTION_DATE)=month(curdate()) and year(TRANSACTION_DATE)=year(curdate())");
	}
	public function oldqueryPlayerWinTDS(){
		return $result = DB::connection('slave')->select("select sum(TRANSACTION_AMOUNT) as Player_winnings_TDS_OLD from  master_transaction_history
		where TRANSACTION_TYPE_ID=80 and TRANSACTION_STATUS_ID=208 and BALANCE_TYPE_ID =3 
		and month(TRANSACTION_DATE)=month(curdate()) and year(TRANSACTION_DATE)=year(curdate());
		");
	}
	public function affiCommTDS(){
		return $result = DB::connection('slave')->select(" select sum(AMOUNT) as Affiliate_Commission_TDS from  partners_transaction_details 
		where TRANSACTION_TYPE_ID=109 and TRANSACTION_STATUS_ID=233
		and month(CREATED_TIMESTAMP)=month(curdate()) and year(CREATED_TIMESTAMP)=year(curdate())");
	}
}