<?php

namespace App\Http\Controllers\bo\reports;

use App\Http\Controllers\Controller;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
/**
 * Author: Nitesh Kumar Jha
 * Purpose: Get rake report result
 * Created Date: 22-1-2020 To 22-1-2020
 */


class RakeReportController extends Controller
{
   // view file
    public function index(){
        return view('bo.views.reports.RakeReport');
    }
    public function exportExcel(Request $request){
            Validator::make($request->all(), [
                'date_from'=> 'required|date',
//                'date_to'=> 'required|date|after_or_equal:date_from',
            ])->validate();
            
            $date_from 	= Carbon::parse($request->date_from)->setTime(0, 0, 0)->toDateTimeString();//->format('Y-m-d');
            $date_to 	= Carbon::parse($request->date_from)->setTime(23, 59, 59)->toDateTimeString();//->format('Y-m-d');

            $data = DB::select("call usp_RakeReport('$date_from','$date_to')");
            $dataArr = json_decode(json_encode($data),true);
            $header=[];
            if(is_array($dataArr) && count($dataArr) > 0){
                $header =  array_keys($dataArr[0]);
               
                $fileName = "Rake_Report" . str_replace(" ","-",$date_from) ."_" . str_replace(" ","-",$date_to) . ".xlsx";
                return Excel::download(new CollectionExport($dataArr,$header), $fileName);
            }else{
                return back()->with('custom_message', ["title"=>"Info","text"=>"No record found."]);
            }
            
        }

}		