<?php
namespace App\Http\Controllers\bo\reports;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use stdClass;
use DB;
use Carbon\Carbon;
use \PDF;
use App;
use Validator;
use Mail;
use Storage;

class TestPBDailyReportController extends Controller 
{
	public function index(Request $request){
		
		$date = date('Y-m-d', strtotime("-1 day"));
		$date_from =Carbon::parse($date)->setTime(0, 0, 0)->toDateTimeString();
		$date_to =Carbon::parse($date)->setTime(23, 59, 59)->toDateTimeString();
		#1financial query 
		#Gross Revenue , Bonus Revenue , Net Revenue , NGR/GGR   []-
		$grossBonusNetNGRGGRRevenues = $this->grossBonusNetNGRGGRRevenue();
		
		if($grossBonusNetNGRGGRRevenues != []){
			
			$object = json_decode(json_encode($grossBonusNetNGRGGRRevenues),true);
			$grossBonusNetNGRGGRRevenue  = json_decode(json_encode($object[0]), FALSE);
		}else{
			
			$grossBonusNetNGRGGRRevenue = '';
		}		
		#2.Deposite withdrawl net cash
		$DWNC = $this->depositsWithdrawalsNetCash();
		
		#end deposite
		
		#Users,DAUs (logins) :-
		$userDAULogins = $this->userDAUsLogin($date_from,$date_to);
		
		
		#signup logins
		$signUps = $this->signUpLogins($date_from,$date_to);
		
		
		#.Total promo cost
		$Total_Promo_Cost = $this->totalPromoCost();
		$object = json_decode(json_encode($Total_Promo_Cost),true);
		$TPC  = json_decode(json_encode($object[0]), FALSE);
		
		#4.Withdrawable player balance (WPB)
		$WPB = $this->withdrawalPlayerBalance();
		
		#Withdrawable pendings 
		$pendingWithdrawalAmount = $this->pendingWithdrawalAmount();
		
		#% change In WPB  //pending
		$percentchangeInWPB = $this->percentchangeInWPB();
		
		#FTDs reports
		$ftdReport = $this->ftdsReports($date_from,$date_to);
		$object = json_decode(json_encode($ftdReport),true);
		$ftdReports  = json_decode(json_encode($object[0]), FALSE);
		
		#FTWs reports
		$ftwReport = $this->ftwsReports();
		$object = json_decode(json_encode($ftwReport),true);
		$ftwsReport  = json_decode(json_encode($object[0]), FALSE);
		
		
		#Unique  Withdrawing Users reports			[]-
		$uniqueWithdrawingUsers = $this->uniqueWithdrawingUser($date_from,$date_to);
		
		if($uniqueWithdrawingUsers != []){
			
			 $object = json_decode(json_encode($uniqueWithdrawingUsers),true);
			 $uniqueWithdrawingUser  = json_decode(json_encode($object[0]), FALSE);
		}else{
			$uniqueWithdrawingUser = '';
		}
		
		#Unique Depositers  			[]-
		$uniqueDepositers = $this->uniqueDepositer($date_from,$date_to);
		if($uniqueDepositers != ''){
			
			$object = json_decode(json_encode($uniqueDepositers),true);
			$uniqueDepositer  = json_decode(json_encode($object[0]), FALSE);
		}else{
			
			$uniqueDepositer = '';
		}
		#Unique Wagering Users Players reports
		$UniqueWageringUser = $this->UniqueWageringUsers($date_from,$date_to);
		
		#Lifetime Depositors Wagering( LTDW) :  reports
		$lifetimeDepositorsWagerings = $this->lifetimeDepositorsWagering();
		if($lifetimeDepositorsWagerings != []){
			
			$object = json_decode(json_encode($lifetimeDepositorsWagerings),true);
			$lifetimeDepositorsWagering  = json_decode(json_encode($object[0]), FALSE);
		}else{
			
			$lifetimeDepositorsWagering = '';
		}
		
		
		#Wagering users (non-depositors)  :  reports
		$wageringusers_non_depositor  = $this->wageringusersNonDepositors();
		if($wageringusers_non_depositor != []){
			
			$object = json_decode(json_encode($wageringusers_non_depositor),true);
			$wageringusers_non_depositors  = json_decode(json_encode($object[0]), FALSE);
		}else{
			
			$wageringusers_non_depositors = '';
		}
		
		
		#Unique freeroll playing users :   reports
		$uniqueFreerollPlayingUsers  = $this->uniqueFreerollPlayingUsers($date_from,$date_to);
		
		#Promo split(Signup promo,Deposit promo,Locked promo released),
		$partnerToSite = $this->partnerToSite(); 
		$bonusCategories = ['Deposit_promo' ,'Locked_promo_released' ,'Signup_promo' ,'Partner_to_site'
		];
		$temp=[];
		foreach($bonusCategories as $key => $bonusCategoriesValue ){
			$temp[$bonusCategoriesValue] = null;
			foreach($partnerToSite as $key=> $partnerToSiteValue)
			{
				if($bonusCategoriesValue === $partnerToSiteValue->Bonus_categories){
					$temp[$bonusCategoriesValue] = $partnerToSiteValue;
				}
			}
		}
		#RCC RBB
		$RCC = $this->rCC(); 
		if($RCC != []){	
			$object = json_decode(json_encode($RCC),true);
			$rCC  = json_decode(json_encode($object[0]), FALSE);
		}else{
			$rCC = '';
		}
		
		$RCB = $this->rCB();
		if($RCB != []){	
		$object = json_decode(json_encode($RCB),true);
		$rCB  = json_decode(json_encode($object[0]), FALSE);
		}else{
			$rCB = '';
		}
		#tournament Tickets Expense
		$tournamentTicketsExpenses = $this->tournamentTicketsExpense($date_from,$date_to);
		$object = json_decode(json_encode($tournamentTicketsExpenses),true);
		$tournamentTicketsExpense  = json_decode(json_encode($object[0]), FALSE);
		
		#tournament overlays
		$tournamentOverlay = $this->tournamentOverlays();
		$object = json_decode(json_encode($tournamentOverlay),true);
		$tournamentOverlays  = json_decode(json_encode($object[0]), FALSE);
		
		#Texas Hold'em(Stakes (Big Blind))  
		$stacks = $this->texasHoldemBigBlindStacks($date_from,$date_to);
		
		# Small Medium High
		$texasHoldemBigBlindStacksSmallHighMid = $this->texasHoldemBigBlindStacksSmallHighMid($date_from,$date_to);
		
		$big_blind_case = ['SMALL' ,'MID' ,'HIGH'];
		$texas_temp=[];
		foreach($big_blind_case as $key => $big_blind_caseValue ){
			$texas_temp[$big_blind_caseValue] = null;
			foreach($texasHoldemBigBlindStacksSmallHighMid as $key=> $texasHoldemBigBlindStacksSmallHighMidValue)
			{
				if($big_blind_caseValue === $texasHoldemBigBlindStacksSmallHighMidValue->BIG_BLIND_CASE){
					$texas_temp[$big_blind_caseValue] = $texasHoldemBigBlindStacksSmallHighMidValue;
				}
			}
		}
		
		# Pot Limit Omaha 4 card			
		$potLimitOmaha4card = $this->potLimitOmaha4card($date_from,$date_to);
		
		# High , mid & low					
		$potLimitOmaha4CardhighMidLowCard = $this->potLimitOmaha4CardhighMidLowCard();
		$bigBlindCase = ['SMALL' ,'MID' ,'HIGH'];
		$pot_temp=[];
		foreach($bigBlindCase as $key => $bigBlindCaseValue ){
			$pot_temp[$bigBlindCaseValue] = null;
			foreach($potLimitOmaha4CardhighMidLowCard as $key=> $potLimitOmaha4CardhighMidLowCardValue)
			{
				if($bigBlindCaseValue === $potLimitOmaha4CardhighMidLowCardValue->BIG_BLIND_CASE){
					$pot_temp[$bigBlindCaseValue] = $potLimitOmaha4CardhighMidLowCardValue;
				}
			}
		}
		#card Pot Limit Omaha(By stakes):       
		$cardPotLimit5OmahaByStacks = $this->cardPotLimit5OmahaByStacks($date_from,$date_to);
		
		#card Pot Limit Omaha(High ,mid & low)   
		$cardPotLimit5OmahaByHighMidLow = $this->cardPotLimit5OmahaByHighMidLow($date_from, $date_to);
		$bigBlindCasePot5 = ['SMALL' ,'MID' ,'HIGH'];
		$pot5Card_temp=[];
		foreach($bigBlindCasePot5 as $key => $bigBlindCaseValue ){
			$pot5Card_temp[$bigBlindCaseValue] = null;
			foreach($cardPotLimit5OmahaByHighMidLow as $key=> $cardPotLimit5OmahaByHighMidLowValue)
			{
				if($bigBlindCaseValue === $cardPotLimit5OmahaByHighMidLowValue->BIG_BLIND_CASE){
					$pot5Card_temp[$bigBlindCaseValue] = $cardPotLimit5OmahaByHighMidLowValue;
				}
			}
		}
		
		#ofc(stacks)     
		$ofcByStack = $this->ofcByStacks($date_from, $date_to);
		
		#ofc (high mid low)     
		$ofcHighMidLow = $this->ofcHighMidLow();
		$bigBlindCaseOfc = ['SMALL' ,'MID' ,'HIGH'];
		$Ofc_temp=[];
		foreach($bigBlindCaseOfc as $key => $bigBlindCaseValue ){
			$Ofc_temp[$bigBlindCaseValue] = null;
			foreach($ofcHighMidLow as $key=> $ofcHighMidLowValue)
			{
				if($bigBlindCaseValue === $ofcHighMidLowValue->BIG_BLIND_CASE){
					$Ofc_temp[$bigBlindCaseValue] = $ofcHighMidLowValue;
				}
			}
		}
		
		#Game type(Texas ,Plo,5card plo,ofc)
		$gameTypeTexasPlo5cardPloOfc = $this->gameTypeTexasPlo5cardPloOfc($date_from, $date_to);
		$minigamesTypeName = ['Five cards omaha' ,'OFC' ,'Omaha' ,"Texas Hold'em"];
		
		$m_temp=[];
		foreach($minigamesTypeName as $key => $minigamesTypeNameValue ){
			
			$m_temp[$minigamesTypeNameValue] = null;
			foreach($gameTypeTexasPlo5cardPloOfc as $key=> $gameTypeTexasPlo5cardPloOfcValue)
			{
				if($minigamesTypeNameValue === $gameTypeTexasPlo5cardPloOfcValue->MINIGAMES_TYPE_NAME){
					$m_temp[$minigamesTypeNameValue] = $gameTypeTexasPlo5cardPloOfcValue;
				}
			}
		}
		#Game type (Tournament):
		$gameTypeTournament = $this->gameTypeTournament();
		
		
		#Game type(High,mid& low)
		$gameTypeHighMidLow = $this->gameTypeHighMidLow();
		$big_blinds = ['SMALL' ,'MID' ,'HIGH'];
		$gameTypeHighMidLow_temp=[];
		foreach($big_blinds as $key => $big_blindsValue ){
			$gameTypeHighMidLow_temp[$big_blindsValue] = null;
			foreach($gameTypeHighMidLow as $key=> $gameTypeHighMidLowValue)
			{
				if($big_blindsValue === $gameTypeHighMidLowValue->BIG_BLIND_CASE){
					$gameTypeHighMidLow_temp[$big_blindsValue] = $gameTypeHighMidLowValue;
				}
			}
		}
		
		#Net Cash (Including Liabilities)Current month depositCurrent month Current Month Net cash	-Monthly Liability Change
		$depo_monts = $this->currentMonthDepositWithdrawalNetCashLiability();
	
		if($depo_monts != []){	
			$object = json_decode(json_encode($depo_monts),true);
			$currentMonthDepositWithdrawalNetCashLiability  = json_decode(json_encode($object[0]), FALSE);
		}else{
			$currentMonthDepositWithdrawalNetCashLiability = '';
		}
		$yesterday_date = $data['yesterday_date'] = date('j-F-Y', strtotime("-1 day"));
		$date = $data['date'] = date('Y-m-d', strtotime("-1 day"));
		
		$pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('bo.views.exportpdf.testpbdailystats', [
			'grossBonusNetNGRGGRRevenue'=>$grossBonusNetNGRGGRRevenue,
			'DWNC'=>$DWNC,
			'userDAULogins'=>$userDAULogins,
			'signUp'=>$signUps,
			'TPC'=>$TPC,
			'WPB'=>$WPB,
			'pendingWithdrawalAmount'=>$pendingWithdrawalAmount,
			'percentchangeInWPB'=>$percentchangeInWPB,
			'ftdReports'=>$ftdReports,
			'ftwsReport'=>$ftwsReport,
			'uniqueWithdrawingUser'=>$uniqueWithdrawingUser,
			'uniqueDepositer'=>$uniqueDepositer,
			'UniqueWageringUser'=>$UniqueWageringUser,
			'lifetimeDepositorsWagering'=>$lifetimeDepositorsWagering,
			'wageringusers_non_depositors'=>$wageringusers_non_depositors,
			'uniqueFreerollPlayingUsers'=>$uniqueFreerollPlayingUsers,
			'partnerToSite'=>$temp,
			'rCC'=>$rCC,
			'rCB'=>$rCB,
			'tournamentTicketsExpense'=>$tournamentTicketsExpense,
			'tournamentOverlays'=>$tournamentOverlays,
			'stacks'=>$stacks,
			'texasHoldemBigBlindStacksSmallHighMid'=>$texas_temp,
			'potLimitOmaha4card'=>$potLimitOmaha4card,
			'potLimitOmaha4CardhighMidLowCard'=>$pot_temp,
			'cardPotLimit5OmahaByStacks'=>$cardPotLimit5OmahaByStacks,
			'cardPotLimit5OmahaByHighMidLow'=>$pot5Card_temp,
			'ofcByStack'=>$ofcByStack,
			'ofcHighMidLow'=>$Ofc_temp,
			'gameTypeTexasPlo5cardPloOfc'=>$m_temp,
			'gameTypeTournament'=>$gameTypeTournament,
			'gameTypeHighMidLow'=>$gameTypeHighMidLow_temp,
			'currentMonthDepositWithdrawalNetCashLiability'=>$currentMonthDepositWithdrawalNetCashLiability,
			'yesterday_date'=>$yesterday_date
		])->setPaper('a4','landscape');
		// $file = "PBDaily_Stats_Report_" . $data['date'] . ".pdf";
		 //return $pdf->download($file.".pdf");
		$dataArr = $pdf->output();
		
		## get file location
		
		$file = "PBDaily_Stats_Report_" . $data['date'] . ".pdf";
		## check file and delete if exists
		if (Storage::disk('local')->exists($file)) {
			Storage::delete($file);
		}
		Storage::put($file, $dataArr);
		
		## generate new file and then send mail.
		if (Storage::disk('local')->exists($file)) {
			Mail::send("bo.mail.pbDailyStatsReportMail", $data, function ($message) use($data, $dataArr, $file) {
				$message->from(config('poker_config.mail.from.analytics'), 'PB Daily Stats Report');
				$message->to(config('poker_config.mail.from.analytics'))
				->bcc(config('poker_config.testpbdailystats_report_mail_to.emails'))
						->subject('New Test PokerBaazi Daily Stats Report for ' . " " . $data['yesterday_date']);
				
					$message->attach(storage_path('app/' . $file), [
						'as' => $file,
						'mime' => 'application/pdf'
					]);
				
			}); 
			if (count(Mail::failures()) <= 0 && Storage::disk('local')->exists($file)) {
				Storage::delete($file);
			}
		}
		
		return response()->json(["status" => 200, "message" => "success"]);
	}
	
	public function grossBonusNetNGRGGRRevenue(){
		return $result = DB::connection('slave')->select("SELECT GROSS_REVENE1+GROSS_REVENE_OFC+GGR_T AS GROSS_REVENE,BONUS_REVENUE1+BONUS_REVENUE_OFC+B_REV_T  AS BONUS_REVENUE,ROUND(NET_REVENUE1+NET_RAKE_OFC+NET_REV_T,2) AS NET_REVENUE,
		ROUND(((NET_REVENUE1+NET_RAKE_OFC+NET_REV_T)/(GROSS_REVENE1+GROSS_REVENE_OFC+GGR_T) * 100),0) AS 'NGRGGR'
		 FROM
		(SELECT DATE(REPORT_DATE) AS DATE1,SUM(TOTAL_RAKE) AS GROSS_REVENE1,SUM(TOTAL_BONUS_REVENUE) AS BONUS_REVENUE1,SUM((ACTUAL_REVENUE*TOTAL_REAL_REVENUE)/TOTAL_RAKE) AS NET_REVENUE1
		FROM user_turnover_report_daily WHERE TOTAL_RAKE > 0 AND REPORT_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59') GROUP BY DATE1)a
		JOIN
		(
		SELECT DATE(STARTED) AS DATE1,SUM(REVENUE) AS GROSS_REVENE_OFC,SUM(BONUS_REVENUE) AS BONUS_REVENUE_OFC,
		SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE) AS NET_RAKE_OFC
		FROM ofc_game_transaction_history WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59') GROUP BY DATE1)b ON a.DATE1=b.DATE1
		JOIN
		(
		SELECT DATE(TRANSACTION_DATE) AS DATE1,SUM(TRANSACTION_AMOUNT) AS GGR_T,SUM(CASE WHEN BALANCE_TYPE_ID IN (1,3) THEN TRANSACTION_AMOUNT END) AS NET_REV_T,
		SUM(CASE WHEN BALANCE_TYPE_ID=2 THEN TRANSACTION_AMOUNT END) AS B_REV_T
		FROM master_transaction_history WHERE TRANSACTION_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		AND TRANSACTION_TYPE_ID IN (25,102,105) GROUP BY DATE1)c ON a.DATE1=c.DATE1;");
		
	}
	public function depositsWithdrawalsNetCash(){
		return $result = DB::Connection('slave')->select("SELECT a.DEPOSIT_AMOUNT,b.WITHDRAWING_AMOUNT,(a.DEPOSIT_AMOUNT-b.WITHDRAWING_AMOUNT) NET_CASH FROM (
		(SELECT SUM(PAYMENT_TRANSACTION_AMOUNT) DEPOSIT_AMOUNT,DATE(PAYMENT_TRANSACTION_CREATED_ON) D1
		FROM payment_transaction WHERE TRANSACTION_TYPE_ID IN (8,61,62,83,111) AND PAYMENT_TRANSACTION_STATUS IN (103,125)
		AND PAYMENT_TRANSACTION_CREATED_ON BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59') GROUP BY DATE(PAYMENT_TRANSACTION_CREATED_ON))a
		LEFT JOIN
		(SELECT SUM(PAYMENT_TRANSACTION_AMOUNT) WITHDRAWING_AMOUNT,DATE(PAYMENT_TRANSACTION_CREATED_ON) D2
		FROM payment_transaction WHERE TRANSACTION_TYPE_ID IN (10,74,80) AND PAYMENT_TRANSACTION_STATUS IN (208,209)
		AND PAYMENT_TRANSACTION_CREATED_ON BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY DATE(PAYMENT_TRANSACTION_CREATED_ON)) b ON a.D1= b.D2);");
		
	}
	public function withdrawalPlayerBalance(){
		return $result = DB::connection('slave')->select('SELECT SUM(USER_DEPOSIT_BALANCE + USER_WIN_BALANCE) WITHDRAWABLE_PLAYER_BALANCE_TILL_DATE FROM user_points where COIN_TYPE_ID =1;');
	}
	public function pendingWithdrawalAmount(){
		return $result = DB::connection('slave')->select("SELECT SUM(PAYMENT_TRANSACTION_AMOUNT) PENDING_WITHDRAWAL_AMOUNT,DATE(PAYMENT_TRANSACTION_CREATED_ON) D2
		FROM payment_transaction WHERE TRANSACTION_TYPE_ID IN (10) AND PAYMENT_TRANSACTION_STATUS IN (109)
		AND PAYMENT_TRANSACTION_CREATED_ON BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY DATE(PAYMENT_TRANSACTION_CREATED_ON);");
	}
	public function totalPromoCost(){
		return $result = DB::connection('slave')->select("SELECT SUM(PROMO1+PROMO2) AS TOTAL_PROMO_COST FROM (
		(SELECT DATE(ADJUSTMENT_CREATED_ON) AS DATE1,
		 COALESCE(SUM(CASE WHEN TRANSACTION_TYPE_ID = 21 AND ADJUSTMENT_ACTION = 'Add'
			THEN ADJUSTMENT_AMOUNT END ),0)  -
		 COALESCE(SUM(CASE WHEN TRANSACTION_TYPE_ID = 21 AND ADJUSTMENT_ACTION = 'subtract'
			THEN ADJUSTMENT_AMOUNT END ),0) +
		 COALESCE(SUM(CASE WHEN TRANSACTION_TYPE_ID = 22 AND ADJUSTMENT_ACTION = 'Add'
			THEN ADJUSTMENT_AMOUNT END ),0) -
		 COALESCE(SUM(CASE WHEN TRANSACTION_TYPE_ID = 22 AND ADJUSTMENT_ACTION = 'subtract'
			THEN ADJUSTMENT_AMOUNT END ),0) +
		 COALESCE(SUM(CASE WHEN TRANSACTION_TYPE_ID = 23 AND ADJUSTMENT_ACTION = 'Add'
			THEN ADJUSTMENT_AMOUNT END ),0) -
		  COALESCE(SUM(CASE WHEN TRANSACTION_TYPE_ID = 23 AND (ADJUSTMENT_ACTION IN('subtract','Remove')) THEN ADJUSTMENT_AMOUNT END ),0) AS PROMO2
		FROM adjustment_transaction
		WHERE ADJUSTMENT_CREATED_ON BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		 GROUP BY date(ADJUSTMENT_CREATED_ON))a
		 JOIN
		(SELECT SUM(mth.TRANSACTION_AMOUNT) AS PROMO1,DATE(TRANSACTION_DATE) DATE2
		FROM master_transaction_history mth WHERE mth.TRANSACTION_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		 AND mth.TRANSACTION_TYPE_ID IN (9,27,65,86,117)GROUP BY DATE2)b ON a.DATE1 = b.DATE2)
		");
	}
	public function percentchangeInWPB(){
		return $result = DB::connection('slave')->select("SELECT ROUND(((WITHDRAWABLE_PLAYER_BALANCE-WITHDRAWABLE_PLAYER_BALANCE_1)/WITHDRAWABLE_PLAYER_BALANCE_1 *100),0)AS '% Change in WPB'
		FROM (
		SELECT DATE(UPDATED_DATE) DATE1,SUM(USER_DEPOSIT_BALANCE+USER_WIN_BALANCE) WITHDRAWABLE_PLAYER_BALANCE
		FROM user_points
		WHERE UPDATED_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59') GROUP BY DATE1 )a
		JOIN
		(
		SELECT DATE(ADDDATE(UPDATED_DATE,INTERVAL 1 DAY)) DATE1,SUM(USER_DEPOSIT_BALANCE+USER_WIN_BALANCE) WITHDRAWABLE_PLAYER_BALANCE_1
		FROM user_points
		WHERE UPDATED_DATE BETWEEN curdate()- INTERVAL 2 DAY AND CONCAT(curdate()- INTERVAL 2 DAY,' 23:59:59') GROUP BY DATE1)b ON a.DATE1=b.DATE1;
		");
	}
	
	public function userDAUsLogin($date_from,$date_to){
		$q1 = DB::connection('slave')->table('user_turnover_report_daily')
					->whereBetween('REPORT_DATE', [$date_from, $date_to])
					->selectRaw('DISTINCT user_id AS Daus');
		$q2 = DB::connection('slave')->table('tournament_registration')
					->whereBetween('UPDATED_DATE', [$date_from, $date_to])
					->selectRaw('DISTINCT user_id AS Daus');
		$q3 = DB::connection('slave')->table('ofc_game_transaction_history')
					->whereBetween('STARTED', [$date_from, $date_to])
					->selectRaw('DISTINCT user_id AS Daus');
		 return $result = $q1->union($q2)->union($q3)->count();
		
	}
	public function signUpLogins($date_from,$date_to){
		return $result = DB::connection('slave')->table('user')
		->select([
			'user_id',
        ])
		->whereBetween('REGISTRATION_TIMESTAMP', [$date_from, $date_to])
		->count();
	}
	public function ftdsReports($date_from,$date_to){
		return $result = DB::connection('slave')->table('first_deposit_daily')
		->selectRaw('COUNT(DISTINCT USER_ID) AS ftds')
		->whereBetween('PAYMENT_TRANSACTION_CREATED_ON', [$date_from, $date_to])
		->get();
		
	}
	public function ftwsReports(){
		return $result = DB::connection('slave')->select("SELECT COUNT(*) ftws FROM (
		SELECT COUNT(DISTINCT USER_ID)
		FROM payment_transaction WHERE TRANSACTION_TYPE_ID=10 AND PAYMENT_TRANSACTION_STATUS IN (208,209) GROUP BY USER_ID
		HAVING MIN(PAYMENT_TRANSACTION_CREATED_ON) BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59'))a;
		");
	}
	public function uniqueWithdrawingUser($date_from,$date_to){
		return $result = DB::connection('slave')->table('payment_transaction')
		->WhereIn('TRANSACTION_TYPE_ID',['10','74','80'])
		->whereIn('PAYMENT_TRANSACTION_STATUS',['208','209'])
		->whereBetween('PAYMENT_TRANSACTION_CREATED_ON', [$date_from, $date_to])
		->selectRaw('COUNT(DISTINCT USER_ID) Unique_Withdrawing_users')->get();
	}
	public function uniqueDepositer($date_from,$date_to){ 
		return $result = DB::connection('slave')->table('payment_transaction')
		->WhereIn('TRANSACTION_TYPE_ID',['8','61','62','83','111'])
		->whereIn('PAYMENT_TRANSACTION_STATUS',['103','125'])
		->whereBetween('PAYMENT_TRANSACTION_CREATED_ON', [$date_from, $date_to])
		->selectRaw('COUNT(DISTINCT USER_ID) unique_dep')->get();
		
	}
	public function UniqueWageringUsers($date_from,$date_to){
		
		$q1 = DB::connection('slave')->table('user_turnover_report_daily')
					->whereBetween('REPORT_DATE', [$date_from, $date_to])
					->selectRaw('DISTINCT USER_ID');
		$q2 = DB::connection('slave')->table('ofc_game_transaction_history')
					->whereBetween('STARTED', [$date_from, $date_to])
					->selectRaw('DISTINCT USER_ID');
		return $result = $q1->union($q2)->count();
	}
	public function lifetimeDepositorsWagering(){
		return $result = DB::Connection('slave')->select("SELECT COUNT(*) Lifetime_Depositors_Wagering FROM (
		SELECT DISTINCT USER_ID FROM user_turnover_report_daily WHERE REPORT_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		UNION
		SELECT DISTINCT USER_ID FROM ofc_game_transaction_history WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59'))a
		WHERE USER_ID IN (SELECT DISTINCT USER_ID FROM payment_transaction WHERE TRANSACTION_TYPE_ID IN (8,61,62,83,111) AND PAYMENT_TRANSACTION_STATUS IN (103,125))");
	}
	
	public function wageringusersNonDepositors(){
		return $result = DB::Connection('slave')->select("SELECT COUNT(*) Wagering_users FROM (
		SELECT DISTINCT USER_ID FROM user_turnover_report_daily WHERE REPORT_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		UNION
		SELECT DISTINCT USER_ID FROM ofc_game_transaction_history WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59'))a
		WHERE USER_ID NOT IN (SELECT DISTINCT USER_ID FROM payment_transaction WHERE TRANSACTION_TYPE_ID IN (8,61,62,83,111) AND PAYMENT_TRANSACTION_STATUS IN (103,125));");
	}
	public function uniqueFreerollPlayingUsers($date_from,$date_to){
		return $query = DB::connection('slave')->select("SELECT COUNT(DISTINCT USER_ID) Unique_freeroll_playing_users FROM tournament_registration tr  JOIN tournament t ON tr.TOURNAMENT_ID=t.TOURNAMENT_ID  WHERE BUYIN=0 AND TOURNAMENT_START_TIME BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')");
		
	}
	public function partnerToSite(){
		return $query1 = DB::connection('slave')->select("SELECT Bonus_categories,Amount,Unique_users,No_of_txns,Avg_amt FROM (
		(SELECT * ,(CASE WHEN gh.TRANSACTION_TYPE_ID = 65 THEN 'Signup_promo'
		WHEN gh.TRANSACTION_TYPE_ID = 9 THEN 'Deposit_promo'
		WHEN gh.TRANSACTION_TYPE_ID = 27 THEN 'Locked_promo_released'
		WHEN gh.TRANSACTION_TYPE_ID = 86 THEN 'Partner_to_site'
		ELSE NULL END)Bonus_categories FROM(
		SELECT SUM(mth.TRANSACTION_AMOUNT)Amount,COUNT(DISTINCT USER_ID)Unique_users,COUNT(USER_ID) AS No_of_txns,mth.TRANSACTION_TYPE_ID,
		(SUM(mth.TRANSACTION_AMOUNT)/COUNT(DISTINCT USER_ID))Avg_amt
		FROM master_transaction_history mth WHERE mth.TRANSACTION_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		AND mth.TRANSACTION_TYPE_ID IN (9,27,65,86) GROUP BY mth.TRANSACTION_TYPE_ID) gh))b;");
	}
	public function rCB(){
		return $query = DB::connection('slave')->select("SELECT SUM(PROMO_PRICE) RCB_AMOUNT,COUNT(DISTINCT USER_ID) RCB_UINQUE_USERS,COUNT(USER_ID) RCB_NO_OF_TXN,(SUM(PROMO_PRICE)/COUNT(DISTINCT USER_ID)) RCB_AVG_PROMO
		FROM reward_points_conversion_history WHERE CREATED_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		AND PROMO_PRICE > 0 GROUP BY DATE(CREATED_DATE)");
		
	}
	public function rCC(){
		return $query = DB::connection('slave')->select('SELECT SUM(REAL_PRICE) RCC_AMOUNT,COUNT(DISTINCT USER_ID) RCC_UNIQUE_USERS,COUNT(USER_ID) RCC_NO_OF_TXN,(SUM(REAL_PRICE)/COUNT(DISTINCT USER_ID)) RCC_AVG_PROMO
		FROM reward_points_conversion_history WHERE CREATED_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY," 23:59:59")
		AND REAL_PRICE > 0 GROUP BY DATE(CREATED_DATE);');
	}
	public function tournamentTicketsExpense($date_from, $date_to){
		return $query = DB::connection('slave')->table('tournament_turnover_history')
		->selectRaw('SUM(BUY_IN*PROMO_TICKETS_COUNT) TOURNAMENT_TICKETS_EXPENSE_AMOUNT,SUM(PROMO_TICKETS_COUNT) TOURNAMENT_TICKETS_EXPENSE_NO_OF_TXN')
		->whereBetween('TOURNAMENT_START_TIME', [$date_from, $date_to])
		->where('BUY_IN','>',1)
		->get();
	}
	public function tournamentOverlays(){
		return $query = DB::connection('slave')->select("SELECT SUM(TOURNAMENT_OVERLAYS_AMOUNT) TOURNAMENT_OVERLAYS_AMOUNT,COUNT(CASE WHEN TOURNAMENT_OVERLAYS_AMOUNT>1 THEN TOURNAMENT_ID END) OVERLAY_TOURNAMENT_COUNT,
		COUNT(*) TOTAL_TOURNAMENT FROM(
		SELECT t.TOURNAMENT_ID,t.TOURNAMENT_NAME,DATE1,
		IF((SUM(PRIZE_GIVEN)-SUM(BUY_IN*(CASH_TICKETS_COUNT+SATELLITE_TICKETS_COUNT+PROMO_TICKETS_COUNT))-SUM(REBUY_IN*REBUYS_COUNT)-SUM(ADDON*ADDONS_COUNT))>0,
		SUM(PRIZE_GIVEN)-SUM(BUY_IN*(CASH_TICKETS_COUNT+SATELLITE_TICKETS_COUNT+PROMO_TICKETS_COUNT))-SUM(REBUY_IN*REBUYS_COUNT)-SUM(ADDON*ADDONS_COUNT),0) TOURNAMENT_OVERLAYS_AMOUNT,
		PROMO_TICKETS_COUNT,PROMO_TICKETS_COUNT*BUY_IN PROMO_TICKETS_COST
		FROM tournament_turnover_history t
		JOIN
		(SELECT TOURNAMENT_ID,BUYIN,DATE(TOURNAMENT_START_TIME) DATE1 FROM tournament
		WHERE TOURNAMENT_START_TIME BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		AND BUYIN>1 )a ON t.TOURNAMENT_ID=a.TOURNAMENT_ID GROUP BY t.TOURNAMENT_ID, t.TOURNAMENT_NAME,DATE1,PROMO_TICKETS_COUNT,BUY_IN)a;");
		
	}
	public function texasHoldemBigBlindStacks($date_from,$date_to){
	
		return $query = DB::connection('slave')->table('game_transaction_history as gth')
		->selectRaw('COUNT(DISTINCT gth.USER_ID) AS PLAYING_USER_TEXAS,BIG_BLIND,COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
		SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.USER_ID)),2) ARPU')
		->join('tournament_tables as tt', 'gth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->where('MINIGAMES_TYPE_NAME', 'like', '%' . 'Texas Hol' . '%')
		->groupBy('BIG_BLIND')->orderBy('BIG_BLIND', 'ASC')->get();
		
	}
	public function texasHoldemBigBlindStacksSmallHighMid($date_from,$date_to){
		
		return $query = DB::connection('slave')->table('game_transaction_history as gth')
		->selectRaw('COUNT(DISTINCT gth.USER_ID) AS PLAYING_USER_TEXAS,
			(CASE WHEN BIG_BLIND  <= 10 THEN "SMALL" WHEN BIG_BLIND > 10 AND BIG_BLIND <=200 THEN "MID" WHEN BIG_BLIND >200 THEN "HIGH" ELSE NULL END) AS BIG_BLIND_CASE,
			COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
			SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
			ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.USER_ID)),2) ARPU')
		->join('tournament_tables as tt', 'gth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->where('MINIGAMES_TYPE_NAME', 'like', '%' . 'Texas Hol' . '%')
		->groupBy('BIG_BLIND_CASE')->orderBy('BIG_BLIND_CASE', 'DESC')->get();
	}
	public function potLimitOmaha4card(){
		return $query = DB::connection('slave')->select("SELECT COUNT(DISTINCT gth.USER_ID) AS PLAYING_USER,BIG_BLIND,COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS, SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE, ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.USER_ID)),2) ARPU FROM game_transaction_history gth INNER JOIN tournament_tables tt ON gth.TOURNAMENT_TABLE_ID=tt.TOURNAMENT_TABLE_ID INNER JOIN tournament t ON tt.TOURNAMENT_ID=t.TOURNAMENT_ID  WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59') AND MINIGAMES_TYPE_NAME LIKE 'Omaha%' GROUP BY BIG_BLIND ORDER BY BIG_BLIND;");
		
	}
	public function potLimitOmaha4CardhighMidLowCard(){
		
		return $query = DB::connection('slave')->select("SELECT COUNT(DISTINCT gth.USER_ID) AS PLAYING_USER, MINIGAMES_TYPE_NAME,
		(CASE WHEN BIG_BLIND  <= 10 THEN 'SMALL'WHEN BIG_BLIND >10  AND BIG_BLIND <= 200 THEN 'MID' WHEN BIG_BLIND >200 THEN 'HIGH' ELSE NULL END) AS BIG_BLIND_CASE,
		COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
		SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.USER_ID)),2) ARPU
		FROM game_transaction_history gth
		INNER JOIN tournament_tables tt ON gth.TOURNAMENT_TABLE_ID=tt.TOURNAMENT_TABLE_ID
		INNER JOIN tournament t ON tt.TOURNAMENT_ID=t.TOURNAMENT_ID
		WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		AND MINIGAMES_TYPE_NAME LIKE 'Omaha%' GROUP BY BIG_BLIND_CASE,MINIGAMES_TYPE_NAME ORDER BY BIG_BLIND_CASE DESC;");
		 
	}
	public function cardPotLimit5OmahaByStacks($date_from,$date_to){
		return $query = DB::connection('slave')->table('game_transaction_history as gth')
		->selectRaw('BIG_BLIND AS STAKES,SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,
		SUM(BONUS_REVENUE) AS BONUS_REVENUE,COUNT(DISTINCT gth.USER_ID) AS FIVE_CARD_PLAYING_USER,
		COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.user_id)),2) ARPU')
		->join('tournament_tables as tt', 'gth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->where('MINIGAMES_TYPE_NAME', 'like', '%' . 'Five cards omaha' . '%')
		->groupBy('BIG_BLIND')->groupBy('MINIGAMES_TYPE_NAME')->orderBy('MINIGAMES_TYPE_NAME', 'ASC')
		->orderBy('BIG_BLIND', 'ASC')->get();
	
	}
	public function cardPotLimit5OmahaByHighMidLow($date_from, $date_to){
		return $query = DB::connection('slave')->table('game_transaction_history as gth')
		->selectRaw('COUNT(DISTINCT gth.USER_ID) AS PLAYING_USER_OMAHA,
		(CASE WHEN BIG_BLIND  <= 10 THEN "SMALL" WHEN BIG_BLIND  >10 AND BIG_BLIND <= 200 THEN "MID" WHEN BIG_BLIND >200 THEN "HIGH" ELSE NULL END) AS BIG_BLIND_CASE,
		COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
		SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.USER_ID)),2) ARPU')
		->join('tournament_tables as tt', 'gth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->where('MINIGAMES_TYPE_NAME', 'like', '%' . 'Five cards omaha' . '%')
		->groupBy('BIG_BLIND_CASE')->orderBy('BIG_BLIND_CASE', 'DESC')->get();
	}
	public function ofcByStacks($date_from, $date_to){
		return $query = DB::connection('slave')->table('ofc_game_transaction_history as ogth')
		->selectRaw('COUNT(DISTINCT ogth.USER_ID) AS PLAYING_USER,MINIGAMES_TYPE_NAME,t.POINT_VALUE,COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
		SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT ogth.USER_ID)),2) ARPU')
		->join('tournament_tables as tt', 'ogth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->groupBy('t.POINT_VALUE','MINIGAMES_TYPE_NAME')
		->orderBy('t.POINT_VALUE','ASC')
		->get();
	}
	public function ofcHighMidLow(){
		return $query = DB::connection('slave')->select("SELECT COUNT(DISTINCT ogth.USER_ID) AS PLAYING_USER, MINIGAMES_TYPE_NAME,
		(CASE WHEN t.POINT_VALUE <= 10 THEN 'SMALL' WHEN t.POINT_VALUE >10 AND t.POINT_VALUE <= 200   THEN 'MID' WHEN t.POINT_VALUE >200 THEN 'HIGH' ELSE NULL END) AS BIG_BLIND_CASE,
		COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT ogth.USER_ID)),2) ARPU
		FROM ofc_game_transaction_history ogth
		INNER JOIN tournament_tables tt ON ogth.TOURNAMENT_TABLE_ID=tt.TOURNAMENT_TABLE_ID
		INNER JOIN tournament t ON tt.TOURNAMENT_ID=t.TOURNAMENT_ID
		WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY BIG_BLIND_CASE,MINIGAMES_TYPE_NAME ORDER BY MINIGAMES_TYPE_NAME,BIG_BLIND_CASE DESC;");
	}
	public function gameTypeTexasPlo5cardPloOfc($date_from, $date_to){ 
		$q1 = DB::connection('slave')->table('game_transaction_history as gth')
		->selectRaw('COUNT(DISTINCT gth.USER_ID) AS PLAYING_USER,MINIGAMES_TYPE_NAME,COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
		SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT gth.USER_ID)),2) ARPU')
		->join('tournament_tables as tt', 'gth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->groupBy('MINIGAMES_TYPE_NAME')->orderBy('MINIGAMES_TYPE_NAME','asc');
		
		$q2 = DB::connection('slave')->table('ofc_game_transaction_history as ogth')
		->selectRaw('COUNT(DISTINCT ogth.USER_ID) AS PLAYING_USER,MINIGAMES_TYPE_NAME,COUNT(DISTINCT PLAY_GROUP_ID) AS NUMBER_OF_HANDS,
		SUM(REVENUE) AS GROSS_RAKE,ROUND(SUM((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,
		ROUND((SUM(REVENUE)/COUNT(DISTINCT ogth.USER_ID)),2) ARPU')
		->join('tournament_tables as tt', 'ogth.TOURNAMENT_TABLE_ID', '=', 'tt.TOURNAMENT_TABLE_ID')
		->join('tournament as t', 'tt.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
		->whereBetween('STARTED', [$date_from, $date_to])
		->groupBy('MINIGAMES_TYPE_NAME')->orderBy('MINIGAMES_TYPE_NAME','asc');
		
		return $result = $q1->union($q2)->get();
	}
	public function gameTypeTournament(){ 
		return $query = DB::connection('slave')->select("SELECT DATE(TRANSACTION_DATE) AS DATE1,SUM(CASE WHEN BALANCE_TYPE_ID IN (1,2,3) THEN TRANSACTION_AMOUNT END) AS GGR,
		SUM(CASE WHEN BALANCE_TYPE_ID IN (1,3) THEN TRANSACTION_AMOUNT END) AS NGR,
		SUM(CASE WHEN BALANCE_TYPE_ID=2 THEN TRANSACTION_AMOUNT END) AS REVENUE_GENERATED_THROUGH_BONUS,
		COUNT(DISTINCT USER_ID) PLAYING_USER, ROUND(((SUM(CASE WHEN BALANCE_TYPE_ID IN (1,2,3) THEN TRANSACTION_AMOUNT END))/COUNT(DISTINCT USER_ID)),2) ARPU
		FROM master_transaction_history WHERE TRANSACTION_DATE BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')   AND TRANSACTION_TYPE_ID IN (25,102,105) AND TRANSACTION_AMOUNT>0 GROUP BY DATE1");
		
	}
	public function gameTypeHighMidLow(){
		
		return $result = DB::connection('slave')->select("SELECT COUNT(DISTINCT PLAYING_USER) AS PLAYING_USER,
		(CASE WHEN BIG_BLIND <= 10 THEN 'SMALL' WHEN (BIG_BLIND > 10 AND BIG_BLIND <=  200) THEN 'MID' WHEN BIG_BLIND > 200 THEN 'HIGH' ELSE NULL END ) AS BIG_BLIND_CASE,
		COUNT(DISTINCT NUMBER_OF_HANDS) AS NUMBER_OF_HANDS,SUM(GROSS_RAKE) AS GROSS_RAKE,SUM(NET_RAKE) AS NET_RAKE,SUM(BONUS_REVENUE) AS BONUS_REVENUE,ROUND((SUM(GROSS_RAKE)/COUNT(DISTINCT PLAYING_USER)),2) ARPU
		FROM
		( SELECT PLAYING_USER,BIG_BLIND,NUMBER_OF_HANDS,GROSS_RAKE,MINIGAMES_TYPE_NAME,NET_RAKE,BONUS_REVENUE
		FROM ( SELECT USER_ID AS PLAYING_USER,BIG_BLIND,PLAY_GROUP_ID AS NUMBER_OF_HANDS,MINIGAMES_TYPE_NAME,
		REVENUE AS GROSS_RAKE,ROUND(((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,
		(BONUS_REVENUE) AS BONUS_REVENUE FROM game_transaction_history gth
		INNER JOIN tournament_tables tt ON gth.TOURNAMENT_TABLE_ID=tt.TOURNAMENT_TABLE_ID
		INNER JOIN tournament t ON tt.TOURNAMENT_ID=t.TOURNAMENT_ID
		WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59') ) a
		UNION ALL
		SELECT PLAYING_USER,BIG_BLIND,NUMBER_OF_HANDS,GROSS_RAKE,NET_RAKE,BONUS_REVENUE,MINIGAMES_TYPE_NAME
		FROM ( SELECT USER_ID AS PLAYING_USER,POINT_VALUE AS BIG_BLIND,MINIGAMES_TYPE_NAME,
		PLAY_GROUP_ID AS NUMBER_OF_HANDS, REVENUE AS GROSS_RAKE,
		ROUND(((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE),2) AS NET_RAKE,
		(BONUS_REVENUE) AS BONUS_REVENUE FROM ofc_game_transaction_history ogth
		INNER JOIN tournament_tables tt ON ogth.TOURNAMENT_TABLE_ID=tt.TOURNAMENT_TABLE_ID
		INNER JOIN tournament t ON tt.TOURNAMENT_ID=t.TOURNAMENT_ID
		WHERE STARTED BETWEEN curdate()- INTERVAL 1 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,' 23:59:59')) b) c
		GROUP BY BIG_BLIND_CASE ORDER BY BIG_BLIND_CASE DESC;");
	}
	public function currentMonthDepositWithdrawalNetCashLiability(){
		return $result = DB::connection('slave')->select("select a.deposit_amount ,b.withdrawing_amount , (a.deposit_amount-b.withdrawing_amount) Net_cash  from ((
		(select sum(payment_transaction_amount) Deposit_Amount,month(PAYMENT_TRANSACTION_CREATED_ON)  D1
		from payment_transaction where TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111)
		 and PAYMENT_TRANSACTION_STATUS IN (103 , 125) and
		 month(PAYMENT_TRANSACTION_CREATED_ON) = month(curdate())
		 and year(PAYMENT_TRANSACTION_CREATED_ON) = year(curdate())
		 group by D1 ) ) a
		left join
		(select sum(payment_transaction_amount) withdrawing_amount ,
		 month(PAYMENT_TRANSACTION_CREATED_ON) D2
		from payment_transaction where TRANSACTION_TYPE_ID IN (10)
		 and PAYMENT_TRANSACTION_STATUS IN (208 , 209) and
		 month(PAYMENT_TRANSACTION_CREATED_ON) = month(curdate())
		 and year(PAYMENT_TRANSACTION_CREATED_ON) = year(curdate())
		 group by D2) b on a.D1= b.D2);
		");
	}
}