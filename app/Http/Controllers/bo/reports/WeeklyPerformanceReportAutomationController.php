<?php
namespace App\Http\Controllers\bo\reports;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use stdClass;
use DB;
use Carbon\Carbon;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App;
use Mail;
use Storage;

class WeeklyPerformanceReportAutomationController extends Controller 
{
	public function index(){
		$today_date = $data['today_date'] = date('j-F-Y');
		$date = $data['date'] = date('Y-m-d', strtotime("-1 day"));
		$week_Gross_Bonus_NetRevenue = $this->weekGrossBonusNetRevenue();
		$week_signUp = $this->weekSignUp();
		$week_ftd = $this->weekFTD();
		$week_deposit = $this->weekDeposits();
		$week_withdraw = $this->weekWithdraws();
		$week_player_comm_tds = $this->weekOfYearPlayerCommTDS();
		
		$week_player_win_tds = $this->weekOfYearPlayerWinTDS();
		$week_player_win_tds_old = $this->weekOfYearPlayerWinTDSOLD();
		$week_aff_comm_tds = $this->weekAffCommTDS();
		$week_app_aff_comm = $this->weekApprAffComm();
		$week_app_player_comm = $this->weekApprPlaComm();
		$week_pen_aff_comm = $this->weekPenAffComm();
		$week_pen_player_comm = $this->weekPenPlaComm();
		$changeInLiability = $this->changeInLiability();
		$change_in_rewardpoint = $this->ChangeInRewardPoints();
		
		$all_array_data = array_merge($week_Gross_Bonus_NetRevenue,$week_signUp,$week_ftd,$week_deposit,$week_withdraw,$week_player_comm_tds,$week_player_win_tds,$week_player_win_tds_old,$week_aff_comm_tds,$week_app_aff_comm,$week_app_player_comm,$week_pen_aff_comm,$week_pen_player_comm,$changeInLiability,$change_in_rewardpoint);
		
		$output = []; 
		$keys=[];
		
		$remove_key_arr = ['WEEKOFYEAR(TRANSACTION_DATE)','WEEKOFYEAR(CREATED_TIMESTAMP)','WEEKOFYEAR(PAYMENT_TRANSACTION_CREATED_ON)'];
	
		foreach($all_array_data as $key => $value){
			$rowObj = $all_array_data[$key];
			$week=null;
			if(isset($rowObj->WEEK) && !empty($rowObj->WEEK)){$week = $rowObj->WEEK;}
			else if(isset($rowObj->{'WEEKOFYEAR(TRANSACTION_DATE)'}) && !empty($rowObj->{'WEEKOFYEAR(TRANSACTION_DATE)'})){
				$week = $rowObj->{'WEEKOFYEAR(TRANSACTION_DATE)'};			
			}else if(isset($rowObj->{'WEEKOFYEAR(CREATED_TIMESTAMP)'}) && !empty($rowObj->{'WEEKOFYEAR(CREATED_TIMESTAMP)'})){
				$week = $rowObj->{'WEEKOFYEAR(CREATED_TIMESTAMP)'};
			}
			else if(isset($rowObj->{'WEEKOFYEAR(PAYMENT_TRANSACTION_CREATED_ON)'}) && !empty($rowObj->{'WEEKOFYEAR(PAYMENT_TRANSACTION_CREATED_ON)'})){
				$week = $rowObj->{'WEEKOFYEAR(PAYMENT_TRANSACTION_CREATED_ON)'};
			}
			
			foreach($value as $k=>$val){
				if(in_array(strtoupper($k),$remove_key_arr,true)){
					continue;
				}
				$keys[$k] = $k;
				$output[$week][$k] = $val;
			}
		}
		
		$remove_key_arr_other =['PLAYER_COMMISSION_TDS','PLAYER_WINNINGS_TDS','PLAYER_WINNINGS_TDS_OLD','AFFILIATE_COMMISSION_TDS','PENDING_AFFILIATE_COMMISSIONS','PENDING_PLAYER_COMMISSIONS','APPROVED_AFFILIATE_COMMISSIONS','APPROVED_PLAYER_COMMISSIONS'];
		
		$finaloutput=[];
		foreach($output as $rowsK => $rowsVal){
			
			$netAmount = 0; $tdsAmount = 0; $pending_ApCommAmount = 0; $approved_ApCommission = 0;
			foreach($keys as $colsK => $colsVal){
				
				#TDS= Player_commission_TDS+ Player_winnings_TDS+ Affiliate_Commission_TDS
					if(isset($output[$rowsK][$colsK]) && !empty($output[$rowsK][$colsK]) && ($colsK === 'PLAYER_COMMISSION_TDS' || $colsK === 'PLAYER_WINNINGS_TDS_OLD' || $colsK === 'PLAYER_WINNINGS_TDS' || $colsK === 'AFFILIATE_COMMISSION_TDS')){
						$tdsAmount +=$output[$rowsK][$colsK];
					}
					#pending_ApCommission = pending_Affiliate_commissions+ pending_Player_commissions 
					if(isset($output[$rowsK][$colsK]) && !empty($output[$rowsK][$colsK]) && ($colsK === 'PENDING_AFFILIATE_COMMISSIONS' || $colsK === 'PENDING_PLAYER_COMMISSIONS')){
						$pending_ApCommAmount +=$output[$rowsK][$colsK];
					}
					# approved_ApCommission = approved_Affiliate_commissions+ approved_Player_commissions 
					if(isset($output[$rowsK][$colsK]) && !empty($output[$rowsK][$colsK]) && ($colsK === 'APPROVED_AFFILIATE_COMMISSIONS' || $colsK === 'APPROVED_PLAYER_COMMISSIONS')){
						$approved_ApCommission +=$output[$rowsK][$colsK];
					}
				if(in_array(strtoupper($colsK),$remove_key_arr_other,true)){
					continue;
				}
				if(!isset($output[$rowsK][$colsK])){
					$finaloutput[$rowsK][$colsK]="0";
				}else{
					if(is_numeric($output[$rowsK][$colsK])){
						$output[$rowsK][$colsK] = $output[$rowsK][$colsK];
					}
					$finaloutput[$rowsK][$colsK]=$output[$rowsK][$colsK];
				}
			}
			
			$finaloutput[$rowsK]['NET_CASH'] = '0.00';
			$finaloutput[$rowsK]['TDS']=round($tdsAmount) . ""; 
			$finaloutput[$rowsK]['PENDING_COMMISSIONS']=round($pending_ApCommAmount) . "";
			$finaloutput[$rowsK]['APPROVED_COMMISSIONS']=round($approved_ApCommission) . "";
			$finaloutput[$rowsK]['Change_In_Liability'] = '0.00';
			$finaloutput[$rowsK]['Change_In_Reward_Points'] = '0.00';
		}
		ksort($finaloutput);
	
		foreach($finaloutput as $rowsK => $val){
			
			$finaloutput[$rowsK]['GROSS_REVENUE'] = round($val['GROSS_REVENUE']) . ""; 
			$finaloutput[$rowsK]['BONUS_REVENUE'] = round($val['BONUS_REVENUE']) . "";
			$finaloutput[$rowsK]['NET_REVENUE'] = round($val['NET_REVENUE']) . "";
			$finaloutput[$rowsK]['DEPOSITS'] = round($val['DEPOSITS']) . "";
			$finaloutput[$rowsK]['WITHDRAWALS'] = round($val['WITHDRAWALS']) . "";
			#Net Cash=Deposits-Withdrawals
			$finaloutput[$rowsK]['NET_CASH'] = round($val['DEPOSITS'] - $val['WITHDRAWALS']) . "";
			if(isset($val['Change_in_Liability']) && !empty($val['Change_in_Liability'])){ 
				$finaloutput[$rowsK]['Change_In_Liability'] = round($val['Change_in_Liability']) . "";
			}
			if(isset($val['Change_in_Reward_points']) && !empty($val['Change_in_Reward_points'])){ 
				$finaloutput[$rowsK]['Change_In_Reward_Points'] = round($val['Change_in_Reward_points']) . "";
			}
		}
		//dd($finaloutput);
		$final_keys = array_keys($finaloutput[$week]);
		
		$fileName = 'Weekly_Performance_Report_' . $data['date'];
		$fileName = urlencode($fileName.".xlsx");
		//return Excel::download(new CollectionExport($finaloutput,$final_keys), $fileName);
		if (Storage::disk('local')->exists($fileName)) {
			Storage::delete($fileName);
		}
		Excel::store(new CollectionExport($finaloutput,$final_keys), $fileName);
		
		## generate file and then send mail.
		if (Storage::disk('local')->exists($fileName)) {
			Mail::send("bo.mail.weeklyReportMail", $data, function ($message) use($data, $fileName) {
				$message->from(config('poker_config.mail.from.analytics'), 'Weekly Performance Report');
				$message->to(config('poker_config.weekly_report_mail_to.emails'))
					->subject('PokerBaazi - Weekly Performance Report ' . " " . $data['today_date']);
				
					$message->attach(storage_path('app/' . $fileName), [
						'as' => $fileName,
						'mime' => 'application/vnd'
					]);
				
			}); 
			if (count(Mail::failures()) <= 0 && Storage::disk('local')->exists($fileName)) {
				Storage::delete($fileName);
			}
		}
		return response()->json(["status" => 200, "message" => "success"]);
	}
	
	public function weekGrossBonusNetRevenue(){
		return DB::Connection('slave')->select("select weekofyear(aa.date1) as WEEK,concat(DATE_FORMAT(MIN(aa.date1),'%d-%b'), ' - ',DATE_FORMAT(MAX(aa.date1),'%d-%b')) AS DATE_RANGE,
		round(sum(Gross_Revene1+Gross_Revene_ofc+GGR_T),0) as GROSS_REVENUE,round(sum(Bonus_revenue1+Bonus_revenue_ofc+b_rev_t),0)  as BONUS_REVENUE,
		round(sum(round(Net_Revenue1+net_rake_ofc+net_rev_t,2)),0) as NET_REVENUE
		 from
		(select date(report_date) as date1,sum(TOTAL_RAKE) as Gross_Revene1,sum(TOTAL_BONUS_REVENUE) as Bonus_revenue1
		,sum((ACTUAL_REVENUE*TOTAL_REAL_REVENUE)/TOTAL_RAKE) as Net_Revenue1
		from
		user_turnover_report_daily where total_rake > 0 and
		report_date between CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') and CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59') group by date1)aa join
		(
		select date(started) as date1,sum(REVENUE) as Gross_Revene_ofc,sum(BONUS_REVENUE) as Bonus_revenue_ofc,
		sum((ACTUAL_REVENUE*REAL_REVENUE)/REVENUE)  as net_rake_ofc
		from
		ofc_game_transaction_history where
		started between CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') and CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59') group by date1)bb on aa.date1=bb.date1
		join
		(
		SELECT
		date(TRANSACTION_DATE) as date1,
		SUM(TRANSACTION_AMOUNT) As GGR_T,sum(case when BALANCE_TYPE_ID in (1,3) then TRANSACTION_AMOUNT end) as net_rev_t,
		sum(case when BALANCE_TYPE_ID in (2) then TRANSACTION_AMOUNT end) as b_rev_t
		FROM master_transaction_history
		WHERE TRANSACTION_DATE between CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		and transaction_type_id in (25,102,105) group by date1 ) cc on aa.date1=cc.date1
		where (aa.date1) between CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') and CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		group by weekofyear(aa.date1);");
	}
	public function weekSignUp(){
		return DB::Connection('slave')->select("SELECT WEEKOFYEAR(REGISTRATION_TIMESTAMP) AS WEEK,COUNT(DISTINCT USER_ID) AS SIGNUPS FROM user WHERE
		REGISTRATION_TIMESTAMP BETWEEN CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59') GROUP BY WEEKOFYEAR(REGISTRATION_TIMESTAMP);");
	}
	public function weekFTD(){
		return DB::Connection('slave')->select("SELECT WEEK,SUM(FTD) AS FTDS FROM (
		SELECT COUNT(DISTINCT USER_ID) AS FTD,WEEKOFYEAR(MIN(PAYMENT_TRANSACTION_CREATED_ON)) AS WEEK  FROM payment_transaction WHERE TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111)
		AND PAYMENT_TRANSACTION_STATUS IN (103 , 125)
		GROUP BY USER_ID
		HAVING MIN(PAYMENT_TRANSACTION_CREATED_ON) BETWEEN CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		) A GROUP BY WEEK;");
	}
	public function weekDeposits(){
		return DB::Connection('slave')->select("SELECT WEEKOFYEAR(PAYMENT_TRANSACTION_CREATED_ON) AS WEEK,SUM(PAYMENT_TRANSACTION_AMOUNT) DEPOSITS
		FROM payment_transaction WHERE TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111)
		AND PAYMENT_TRANSACTION_STATUS IN (103 , 125) AND
		PAYMENT_TRANSACTION_CREATED_ON BETWEEN  CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY WEEKOFYEAR(PAYMENT_TRANSACTION_CREATED_ON)");
	}
	public function weekWithdraws(){
		return DB::Connection('slave')->select(" SELECT WEEKOFYEAR(PAYMENT_TRANSACTION_CREATED_ON) AS WEEK,SUM(PAYMENT_TRANSACTION_AMOUNT) WITHDRAWALS
		FROM payment_transaction WHERE TRANSACTION_TYPE_ID IN (10)
		AND PAYMENT_TRANSACTION_STATUS IN (208,209) AND PAYMENT_TRANSACTION_CREATED_ON BETWEEN CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY WEEKOFYEAR(PAYMENT_TRANSACTION_CREATED_ON);");
	}
	public function weekOfYearPlayerCommTDS(){
		return DB::Connection('slave')->select("SELECT WEEKOFYEAR(TRANSACTION_DATE) ,SUM(TRANSACTION_AMOUNT) AS  PLAYER_COMMISSION_TDS FROM  master_transaction_history_pokercommission
		WHERE TRANSACTION_TYPE_ID=80 AND TRANSACTION_STATUS_ID=208
		AND TRANSACTION_DATE BETWEEN CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY WEEKOFYEAR(TRANSACTION_DATE)");
	}
	public function weekOfYearPlayerWinTDS(){
		return DB::Connection('slave')->select("SELECT WEEKOFYEAR(TRANSACTION_DATE),SUM(WITHDRAW_TDS) AS PLAYER_WINNINGS_TDS FROM withdraw_transaction_history
		WHERE TRANSACTION_STATUS_ID=208 AND BALANCE_TYPE_ID =3
		AND TRANSACTION_DATE BETWEEN CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY WEEKOFYEAR(TRANSACTION_DATE);");
	}
	public function weekOfYearPlayerWinTDSOLD(){	//only have 8sep -2020 data in place of it new table created 
		return DB::Connection('slave')->select("SELECT WEEKOFYEAR(TRANSACTION_DATE) ,SUM(TRANSACTION_AMOUNT) AS PLAYER_WINNINGS_TDS_OLD FROM  master_transaction_history WHERE TRANSACTION_TYPE_ID=80 AND TRANSACTION_STATUS_ID=208 AND BALANCE_TYPE_ID =3 AND  TRANSACTION_DATE BETWEEN CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')  GROUP BY WEEKOFYEAR(TRANSACTION_DATE);");
	}
	public function weekAffCommTDS(){
		return DB::Connection('slave')->select("SELECT WEEKOFYEAR(CREATED_TIMESTAMP),SUM(AMOUNT) AS AFFILIATE_COMMISSION_TDS FROM partners_transaction_details
		WHERE TRANSACTION_TYPE_ID=109 AND TRANSACTION_STATUS_ID=233
		AND CREATED_TIMESTAMP BETWEEN CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY WEEKOFYEAR(CREATED_TIMESTAMP);");
	}
	public function weekApprAffComm(){
		return DB::Connection('slave')->select("SELECT WEEKOFYEAR(CREATED_TIMESTAMP)AS WEEK,SUM(AMOUNT) AS APPROVED_AFFILIATE_COMMISSIONS FROM(
		SELECT MAX(TRANSACTION_STATUS_ID) AS TSID,AMOUNT,INTERNAL_REFERENCE_NO,CREATED_TIMESTAMP FROM partners_transaction_details
		WHERE TRANSACTION_TYPE_ID=108
		AND CREATED_TIMESTAMP BETWEEN CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY INTERNAL_REFERENCE_NO,AMOUNT,CREATED_TIMESTAMP)a
		WHERE TSID IN (233) GROUP BY WEEKOFYEAR(CREATED_TIMESTAMP);");
	}
	public function weekApprPlaComm(){
		return DB::Connection('slave')->select("SELECT WEEKOFYEAR(PAYMENT_TRANSACTION_CREATED_ON),SUM(PAYMENT_TRANSACTION_AMOUNT)*.9625 AS APPROVED_PLAYER_COMMISSIONS FROM payment_transaction_pokercommission
		WHERE TRANSACTION_TYPE_ID=10 AND PAYMENT_TRANSACTION_STATUS IN (208)
		AND PAYMENT_TRANSACTION_CREATED_ON BETWEEN  CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		GROUP BY  WEEKOFYEAR(PAYMENT_TRANSACTION_CREATED_ON);");
	}
	public function weekPenAffComm(){
		return DB::Connection('slave')->select("SELECT WEEKOFYEAR(CREATED_TIMESTAMP)AS WEEK,SUM(AMOUNT) AS PENDING_AFFILIATE_COMMISSIONS FROM( SELECT MAX(TRANSACTION_STATUS_ID) AS TSID,AMOUNT,INTERNAL_REFERENCE_NO,CREATED_TIMESTAMP FROM partners_transaction_details WHERE TRANSACTION_TYPE_ID=108 AND CREATED_TIMESTAMP BETWEEN CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')   GROUP BY INTERNAL_REFERENCE_NO)a WHERE TSID IN (231) GROUP BY WEEKOFYEAR(CREATED_TIMESTAMP);");
	}
	public function weekPenPlaComm(){
		return DB::Connection('slave')->select("SELECT WEEKOFYEAR(PAYMENT_TRANSACTION_CREATED_ON),SUM(PAYMENT_TRANSACTION_AMOUNT)*.9625 AS PENDING_PLAYER_COMMISSIONS FROM payment_transaction_pokercommission
		WHERE TRANSACTION_TYPE_ID=10 AND PAYMENT_TRANSACTION_STATUS IN (109)
		AND PAYMENT_TRANSACTION_CREATED_ON BETWEEN  CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		 GROUP BY  WEEKOFYEAR(PAYMENT_TRANSACTION_CREATED_ON);");
	}
	public function changeInLiability(){
		return DB::Connection('slave')->select("select a.WEEK,bal_1-bal_2 as Change_in_Liability from
		(  select sum(USER_DEPOSIT_BALANCE+USER_WIN_BALANCE) as bal_1,WEEKOFYEAR(started) as WEEK from accumulated_daily_userpoints_balance
		  where date(started)   BETWEEN CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		  and coin_type_id=1
		  and dayname(started)='SUNDAY'
		  group by WEEKOFYEAR(started)
		 )a join
		 
			(select sum(USER_DEPOSIT_BALANCE+USER_WIN_BALANCE) as bal_2,WEEKOFYEAR(started)+1 as WEEK from accumulated_daily_userpoints_balance
		  where date(started)   BETWEEN CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 7 DAY,' 23:59:59')
		  and coin_type_id=1
		  and dayname(started)='SUNDAY'
		  group by WEEKOFYEAR(started)
		  ) b on a.WEEK=b.WEEK;");
	}
	public function ChangeInRewardPoints(){
		return DB::Connection('slave')->select("select a.WEEK,bal_1-bal_2 as Change_in_Reward_points from
		(  select sum(USER_TOT_BALANCE) as bal_1,WEEKOFYEAR(started) as WEEK from accumulated_daily_userpoints_balance
		  where date(started)   BETWEEN CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 1 DAY,' 23:59:59')
		  and coin_type_id=12
		  and dayname(started)='SUNDAY'
		  group by WEEKOFYEAR(started)
		)a join
		 
		(select sum(USER_TOT_BALANCE) as bal_2,WEEKOFYEAR(started)+1 as WEEK from accumulated_daily_userpoints_balance
		where date(started)   BETWEEN CONCAT(CURDATE()- INTERVAL 70 DAY,' 00:00:00') AND CONCAT(CURDATE()- INTERVAL 7 DAY,' 23:59:59')
		and coin_type_id=12
		and dayname(started)='SUNDAY'
		group by WEEKOFYEAR(started)
		) b on a.WEEK=b.WEEK;");
	}
}	