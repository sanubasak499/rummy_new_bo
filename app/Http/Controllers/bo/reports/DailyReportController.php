<?php
namespace App\Http\Controllers\bo\reports;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AdminUser;
use stdClass;
use DB;
use Carbon\Carbon;
use \PDF;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
class DailyReportController extends Controller 
{
	public function index(Request $request){
		
		if($request->yesterday_report == "2"){
			#code for pdf report

			Validator::make($request->all(), [
					'date'=> 'required|date',
			])->validate();

			$date_from =Carbon::parse($request->date)->setTime(0, 0, 0)->toDateTimeString();
			$date_to =Carbon::parse($request->date)->setTime(23, 59, 59)->toDateTimeString();
			$data = DB::select("call usp_daily_stat_report_yesterday('$date_from','$date_to')");
			$fileName = 'Daily-Stats-Report_' . Carbon::parse($request->date)->toDateString();
			//$yesterday_date= date('j<\s\up>S</\s\up> M y',strtotime("-1 days"));
			$yesterday_date = $date_to;
			
			$countdata = DB::select("call usp_daily_stat_report_yesterday_count('$date_from','$date_to')");
			if(( isset($countdata) && !empty($countdata) &&  count($countdata) > 0 ) || (isset($data) && !empty($data) &&  count($data) > 0 ) ){
			
		   //	$pdf = PDF::loadView('bo.views.exportpdf.pdf', ['data'=>$data,'countdata'=>$countdata,'yesterday_date'=>$yesterday_date])->setPaper('a4','landscape');
			// $pdf->save(storage_path().'_filename.pdf');
			$pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('bo.views.exportpdf.pdf', ['data'=>$data,'countdata'=>$countdata,'yesterday_date'=>$yesterday_date])->setPaper('a4','landscape');
			
			return $pdf->download($fileName.".pdf");

			
			}else{
				$dateyes = Carbon::parse($request->date)->toDateString();
				return back()->with('custom_message', ["title"=>"Sorry","text"=>"No Data available for ". $dateyes]);
			}
		
		}
		if($request->yesterday_report == "3"){
			
			Validator::make($request->all(), [
					'date'=> 'required|date',
			])->validate();
			
			#code for export data
			$date_from =Carbon::parse($request->date)->setTime(0, 0, 0)->toDateTimeString();
			$date_to =Carbon::parse($request->date)->setTime(23, 59, 59)->toDateTimeString();
			$data = DB::select("call usp_daily_stat_report_yesterday('$date_from','$date_to')");
			$arrayOfdata =(json_decode(json_encode($data),true));
			$headingofsheet  =[];
			if( isset($arrayOfdata) && !empty($arrayOfdata) &&  count($arrayOfdata) > 0){
			 $headingofsheet  = array_keys($arrayOfdata[0]);
			 
			 	$yesterday_date = $request->date;
				$fileName = 'Daily-Stats-Report_' . Carbon::parse($request->date)->toDateString();
				$fileName = urlencode($fileName.".xlsx");
				return Excel::download(new CollectionExport($arrayOfdata,$headingofsheet), $fileName);
		
			}else{
				$dateyes = Carbon::parse($request->date)->toDateString();
				return back()->with('custom_message', ["title"=>"Sorry","text"=>"No Data available for ". $dateyes]);
			}
		}
		if($request->yesterday_report == "1" ){
			
			Validator::make($request->all(), [
					'date'=> 'required|date',
			])->validate();
			$selected_date = $request->date;
			$date_from =Carbon::parse($request->date)->setTime(0, 0, 0)->toDateTimeString();
			$date_to =Carbon::parse($request->date)->setTime(23, 59, 59)->toDateTimeString();
			#getting data yesterday
			$data = DB::select("call usp_daily_stat_report_yesterday('$date_from','$date_to')");
			$countdata = DB::select("call usp_daily_stat_report_yesterday_count('$date_from','$date_to')");
			
			//return view('bo.views.reports.dailystats',['searchData'=>$data,'countdata'=>$countdata]);
			
			if(isset($data) && !empty($data) && is_array($data)){
				return view('bo.views.reports.dailystats',['searchData'=>$data,'countdata'=>$countdata, 'selected_date'=>$selected_date]);
			}else{
				$dateyes = Carbon::parse($request->date)->toDateString();
				return back()->with('custom_message', ["title"=>"Sorry","text"=>"No Daily Stats report available for ". $dateyes]);
			}
		}
		return view('bo.views.reports.dailystats');
	}
	
	
	// public function ReceipientMailList(Request $request){
		// $user_id = config('poker_config.director_mail_id.mail');
		// $get_mail_id = AdminUser::where('ADMIN_USER_ID',10006)->select('EMAIL')->get();
		//print_r($get_mail_id);exit;
		// return response()->json(['get_mail_id' => $get_mail_id,]);
		
	// }
}