<?php
namespace App\Http\Controllers\bo\reports;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use stdClass;
use DB;
use Carbon\Carbon;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App;
use Mail;
use Storage;

class LandingPageReportAutomationController extends Controller 
{
	public function index(){
		$arrayOfLPRegdata = $this->pbLpReg();
		$date = $data['date'] = date('Y-m-d', strtotime("-1 day"));
		$yesterday_date = $data['yesterday_date'] = date('j-F-Y', strtotime("-1 day"));
		
		foreach($arrayOfLPRegdata as $key=> $val)
		{
			if($val->No_of_Depositor== 0 && $val->No_of_Depositor== ''){
				$arrayOfLPRegdata[$key]->No_of_Depositor = "0";
			}
		}
		$fin_data =(json_decode(json_encode($arrayOfLPRegdata),true));
		$headingofReport1  = array_keys($fin_data[0]);
			
		$fileName = 'pb_lp_reg_' . $data['date'];
		$fileName = urlencode($fileName.".xlsx");
		//return Excel::download(new CollectionExport($fin_data,$headingofReport1), $fileName);
		if (Storage::disk('local')->exists($fileName)) {
		Storage::delete($fileName);
		}
		Excel::store(new CollectionExport($fin_data,$headingofReport1), $fileName);
		
		## generate file and then send mail.
		if (Storage::disk('local')->exists($fileName)) {
			Mail::send("bo.mail.landingPageReportMail", $data, function ($message) use($data, $fileName) {
				$message->from(config('poker_config.mail.from.ankit_singla'), 'PB LP REGISTRATION');
				$message->to(config('poker_config.landing_page_automation_report_mail_to.emails'))
					->subject('PB_LP_REGISTRATION ' . " " . $data['yesterday_date']);
				
					$message->attach(storage_path('app/' . $fileName), [
						'as' => $fileName,
						'mime' => 'application/vnd'
					]);
				
			}); 
			if (count(Mail::failures()) <= 0 && Storage::disk('local')->exists($fileName)) {
				Storage::delete($fileName);
			}
		}
		return response()->json(["status" => 200, "message" => "success"]);
	}
	public function pbLFTD(){
		$report2 = $this->pbLpFTD();
		$arrayOfLPFTDdata =(json_decode(json_encode($report2),true));
		$date = $data['date'] = date('Y-m-d', strtotime("-1 day"));
		$yesterday_date = $data['yesterday_date'] = date('j-F-Y', strtotime("-1 day"));
		$headingofReport1  = array_keys($arrayOfLPFTDdata[0]);
			 
		$fileName = 'pb_lp_ftd' . $data['date'];
		$fileName = urlencode($fileName.".xlsx");
		if (Storage::disk('local')->exists($fileName)) {
		Storage::delete($fileName);
		}
		Excel::store(new CollectionExport($arrayOfLPFTDdata,$headingofReport1), $fileName);
		## generate file and then send mail.
		if (Storage::disk('local')->exists($fileName)) {
			Mail::send("bo.mail.landingPageReportMail", $data, function ($message) use($data, $fileName) {
				$message->from(config('poker_config.mail.from.ankit_singla'), 'PB LP FTD');
				$message->to(config('poker_config.landing_page_automation_report_mail_to.emails'))
					->subject('PB_LP_FTD ' . " " . $data['yesterday_date']);
				
					$message->attach(storage_path('app/' . $fileName), [
						'as' => $fileName,
						'mime' => 'application/vnd'
					]);
				
			}); 
			if (count(Mail::failures()) <= 0 && Storage::disk('local')->exists($fileName)) {
				Storage::delete($fileName);
			}
		}
		return response()->json(["status" => 200, "message" => "success"]);
	}
	public function pbLPREVENUE(){
		$arrayOfLpRevenue = $this->pb_Lp_Revenue();
		$date = $data['date'] = date('Y-m-d', strtotime("-1 day"));
		$yesterday_date = $data['yesterday_date'] = date('j-F-Y', strtotime("-1 day"));
		
		foreach($arrayOfLpRevenue as $key=> $val)
		{
			if($val->became_ftd_within_0_day== 0){
				$arrayOfLpRevenue[$key]->became_ftd_within_0_day = "0";
			}
			if($val->became_ftd_within_3_day== 0){
				$arrayOfLpRevenue[$key]->became_ftd_within_3_day = "0";
			}
			if($val->became_ftd_within_5_day== 0){
				$arrayOfLpRevenue[$key]->became_ftd_within_5_day = "0";
			}
			if($val->d0_deposit_count== 0){
				$arrayOfLpRevenue[$key]->d0_deposit_count = "0";
			}
			if($val->d3_deposit_count== 0){
				$arrayOfLpRevenue[$key]->d3_deposit_count = "0";
			}
			if($val->d5_deposit_count== 0){
				$arrayOfLpRevenue[$key]->d5_deposit_count = "0";
			}
			if($val->became_ftd_within_7_day== 0 ){
				$arrayOfLpRevenue[$key]->became_ftd_within_7_day = "0";
			}
			if($val->became_ftd_within_14_day== 0 ){
				$arrayOfLpRevenue[$key]->became_ftd_within_14_day = "0";
			}
			if($val->became_ftd_within_30_day== 0){
				$arrayOfLpRevenue[$key]->became_ftd_within_30_day = "0";
			}
			if($val->became_ftd_life_time== 0 ){
				$arrayOfLpRevenue[$key]->became_ftd_life_time = "0";
			}
			if($val->d7_deposit_count== 0 && $val->d7_deposit_count== ''){
				$arrayOfLpRevenue[$key]->d7_deposit_count = "0";
			}
			if($val->d14_deposit_count== 0 && $val->d14_deposit_count== ''){
				$arrayOfLpRevenue[$key]->d14_deposit_count = "0";
			}
			if($val->d30_deposit_count== 0 && $val->d30_deposit_count== ''){
				$arrayOfLpRevenue[$key]->d30_deposit_count = "0";
			}
			if($val->d0_amount== 0 && $val->d0_amount== ''){
				$arrayOfLpRevenue[$key]->d0_amount = "0";
			}
			if($val->d3_amount== 0 && $val->d3_amount== ''){
				$arrayOfLpRevenue[$key]->d3_amount = "0";
			}
			if($val->d5_amount== 0 && $val->d5_amount== ''){
				$arrayOfLpRevenue[$key]->d5_amount = "0";
			}
			if($val->d7_amount== 0 && $val->d7_amount== ''){
				$arrayOfLpRevenue[$key]->d7_amount = "0";
			}
			if($val->d14_amount== 0 && $val->d14_amount== ''){
				$arrayOfLpRevenue[$key]->d14_amount = "0";
			}
			if($val->d30_amount== 0 && $val->d30_amount== ''){
				$arrayOfLpRevenue[$key]->d30_amount = "0";
			}
			if($val->lifetimedepositamount== 0 && $val->lifetimedepositamount== ''){
				$arrayOfLpRevenue[$key]->lifetimedepositamount = "0";
			}
			if($val->lifetimedepositcount== 0 && $val->lifetimedepositcount== ''){
				$arrayOfLpRevenue[$key]->lifetimedepositcount = "0";
			}
			if($val->d0_revenue== 0 && $val->d0_revenue== ''){
				$arrayOfLpRevenue[$key]->d0_revenue = "0";
			}
			if($val->d3_revenue== 0 && $val->d3_revenue== ''){
				$arrayOfLpRevenue[$key]->d3_revenue = "0";
			}
			if($val->d5_revenue== 0 && $val->d5_revenue== ''){
				$arrayOfLpRevenue[$key]->d5_revenue = "0";
			}
			if($val->d7_revenue== 0 && $val->d7_revenue== ''){
				$arrayOfLpRevenue[$key]->d7_revenue = "0";
			}
			if($val->d14_revenue== 0 && $val->d14_revenue== ''){
				$arrayOfLpRevenue[$key]->d14_revenue = "0";
			}
			if($val->d30_revenue== 0 && $val->d30_revenue== ''){
				$arrayOfLpRevenue[$key]->d30_revenue = "0";
			}
			if($val->lifetime_revenue== 0 && $val->lifetime_revenue== ''){
				$arrayOfLpRevenue[$key]->lifetime_revenue = "0";
			}
		}
		$fin_data =(json_decode(json_encode($arrayOfLpRevenue),true));
		$headingofReport1  = array_keys($fin_data[0]);
		$fileName = 'pb_lp_revenue' . $data['date'];
		$fileName = urlencode($fileName.".xlsx");
		
		if (Storage::disk('local')->exists($fileName)) {
		Storage::delete($fileName);
		}
		Excel::store(new CollectionExport($fin_data,$headingofReport1), $fileName);
		## generate file and then send mail.
		if (Storage::disk('local')->exists($fileName)) {
			Mail::send("bo.mail.landingPageReportMail", $data, function ($message) use($data, $fileName) {
				$message->from(config('poker_config.mail.from.ankit_singla'), 'PB LP REVENUE');
				$message->to(config('poker_config.landing_page_automation_report_mail_to.emails'))
					->subject('PB_LP_REVENUE ' . " " . $data['yesterday_date']);
				
					$message->attach(storage_path('app/' . $fileName), [
						'as' => $fileName,
						'mime' => 'application/vnd'
					]);
				
			}); 
			if (count(Mail::failures()) <= 0 && Storage::disk('local')->exists($fileName)) {
				Storage::delete($fileName);
			}
		}
		return response()->json(["status" => 200, "message" => "success"]);
	}	
	public function pbLpReg(){
		return $report1 = DB::Connection('slave')->select("select count(bb.user_id) as 		No_of_Registration ,LP,Registartion_date, sum(MOBILE_VERIFY) as no_of_mobile_verify ,sum(email_verify) as no_of_email_verify, ifnull(count(bv.user_id),0) as No_of_Depositor   
		from (
			(select distinct (user_id), a.registration_name as LP ,date(u.created_date) Registartion_date
			from user_register_tracking
			u left join user_registration_settings
			a on u.REGISTRATION_TYPE_ID = a.REGISTRATION_TYPE_ID  
			where  u.created_date BETWEEN curdate()- INTERVAL 60 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,
			 ' 23:59:59')
			) cb
			left join (select distinct (user_id), MOBILE_VERIFY,email_verify  from user ) bb on cb.user_id = bb.user_id left join ( 
			select distinct(user_id) from payment_transaction
			where TRANSACTION_TYPE_ID IN
			(8 , 61, 62, 83, 111) and PAYMENT_TRANSACTION_STATUS IN (103 , 125)) bv on bv.user_id= bb.user_id 
		) group by Registartion_date ,LP");
	}
	public function pbLpFTD(){
		return $report1 = DB::Connection('slave')->select("select count(cb.user_id) No_of_Dep,lp,ftd_date from (
		(select user_id, a.registration_name as lp
		from user_register_tracking u left join user_registration_settings
		a on u.REGISTRATION_TYPE_ID = a.REGISTRATION_TYPE_ID) cb
		inner join (select user_id,min(date(PAYMENT_TRANSACTION_CREATED_On)) as ftd_date
		from payment_transaction
		where TRANSACTION_TYPE_ID IN
		(8 , 61, 62, 83, 111) and PAYMENT_TRANSACTION_STATUS IN (103 , 125) 
		group by user_id 
		having min(date(PAYMENT_TRANSACTION_CREATED_On)) BETWEEN curdate()- INTERVAL 60 DAY AND CONCAT(curdate()- INTERVAL 1 DAY,
		' 23:59:59'))
		bb on cb.user_id = bb.user_id 
		) group by 3,2;");
	}
	public function pb_Lp_Revenue(){
		return $report1 = DB::Connection('slave')->select("select date(a.rdate) as date,lp,count(distinct a.user_id) as no_of_singups,
		count(distinct(case when date(d.FDD) = date(a.rdate) then a.user_id end)) as became_ftd_within_0_day,
		count(distinct(case when d.fdd < adddate(a.rdate,interval 3 day) then a.user_id end)) as
		 became_ftd_within_3_day,
		count(distinct(case when d.FDD < adddate(a.rdate,interval 5 day) then a.user_id end)) as became_ftd_within_5_day,
		count(distinct(case when d.FDD < adddate(a.rdate,interval 7 day) then a.user_id end)) as became_ftd_within_7_day,
		count(distinct(case when d.FDD < adddate(a.rdate,interval 14 day) then a.user_id end)) as became_ftd_within_14_day,
		count(distinct(case when d.FDD < adddate(a.rdate,interval 30 day) then a.user_id end)) as became_ftd_within_30_day,
		count(distinct(case when d.FDD is not null then d.user_id end)) as became_ftd_life_time,
		sum(d0_deposit_count) as d0_deposit_count,sum(d3_deposit_count) as d3_deposit_count,
		sum(d5_deposit_count) as d5_deposit_count,
		sum(d7_deposit_count) as d7_deposit_count,sum(d14_deposit_count) as d14_deposit_count,sum(d30_deposit_count) as d30_deposit_count,
		sum(d0_amount) as d0_amount, sum(d3_amount) as d3_amount
		,sum(d5_amount) as d5_amount, sum(d7_amount) as d7_amount,sum(d14_amount) as d14_amount,sum(d30_amount) as d30_amount,
		sum(lifetimedepositamount) as lifetimedepositamount,sum(lifetimedepositcount) as lifetimedepositcount,
		sum(d0_revenue) as d0_revenue,
		sum(d3_revenue) as d3_revenue,
		sum(d5_revenue) as d5_revenue,
		sum( d7_revenue) as d7_revenue,
		sum(d14_revenue) as d14_revenue,
		sum(d30_revenue) as d30_revenue,
		sum(lifetime_revenue) as lifetime_revenue
		from (select user_id,(REGISTRATION_TIMESTAMP) as rdate from user ) a
		 join (select user_id, z.registration_name as lp
		  from user_register_tracking
		 u left join user_registration_settings
		z on u.REGISTRATION_TYPE_ID = z.REGISTRATION_TYPE_ID) b on a.user_id=b.user_id
		left join (select r.user_id,
		sum(case when date(r.report_date) = date(u.REGISTRATION_TIMESTAMP) then r.total_rake end )
		as d0_revenue,
		sum(case when r.report_date < adddate(u.REGISTRATION_TIMESTAMP,interval 3 day) then r.total_rake end )
		as d3_revenue,
		sum(case when r.report_date < adddate(u.REGISTRATION_TIMESTAMP,interval 5 day) then r.total_rake end )
		as d5_revenue,
		sum(case when r.report_date < adddate(u.REGISTRATION_TIMESTAMP,interval 7 day) then r.total_rake end )
		as d7_revenue,
		sum(case when r.report_date < adddate(u.REGISTRATION_TIMESTAMP,interval 14 day) then r.total_rake end )
		 as d14_revenue,
		sum(case when r.report_date < adddate(u.REGISTRATION_TIMESTAMP,interval 30 day) then r.total_rake end )
		 as d30_revenue,
		sum(r.total_rake) as lifetime_revenue from
		user_turnover_report_daily r join user u on u.user_id=r.user_id group by user_id)c
		on a.user_id=c.user_id
		left join
		( select user_id,min(PAYMENT_TRANSACTION_CREATED_ON) as FDD from payment_transaction
		where TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111)
		AND PAYMENT_TRANSACTION_STATUS IN (103 , 125)
		group by user_id having min(PAYMENT_TRANSACTION_CREATED_ON) >'2019-12-31 23:59:59' ) d on a.user_id=d.user_id
		left join
		( select p.user_id,sum(PAYMENT_TRANSACTION_AMOUNT) as lifetimedepositamount,count(*)
		as lifetimedepositcount,
		count((case when date(p.PAYMENT_TRANSACTION_CREATED_ON) =
		date(u.REGISTRATION_TIMESTAMP) then p.user_id end)) as d0_deposit_count,
		count((case when p.PAYMENT_TRANSACTION_CREATED_ON <
		adddate(u.REGISTRATION_TIMESTAMP,interval 3 day) then p.user_id end)) as d3_deposit_count,
		count((case when p.PAYMENT_TRANSACTION_CREATED_ON <
		adddate(u.REGISTRATION_TIMESTAMP,interval 5 day) then p.user_id end)) as d5_deposit_count,
		count((case when p.PAYMENT_TRANSACTION_CREATED_ON <
		adddate(u.REGISTRATION_TIMESTAMP,interval 7 day) then p.user_id end)) as d7_deposit_count,
		count((case when p.PAYMENT_TRANSACTION_CREATED_ON <
		adddate(u.REGISTRATION_TIMESTAMP,interval 14 day) then p.user_id end)) as d14_deposit_count,
		count((case when p.PAYMENT_TRANSACTION_CREATED_ON <
		adddate(u.REGISTRATION_TIMESTAMP,interval 30 day) then p.user_id end)) as d30_deposit_count,
		sum((case when date(p.PAYMENT_TRANSACTION_CREATED_ON) =
		date(u.REGISTRATION_TIMESTAMP) then p.PAYMENT_TRANSACTION_AMOUNT end)) as d0_amount,
		sum((case when p.PAYMENT_TRANSACTION_CREATED_ON <
		adddate(u.REGISTRATION_TIMESTAMP,interval 3 day ) then p.PAYMENT_TRANSACTION_AMOUNT end)) as d3_amount,
		sum((case when p.PAYMENT_TRANSACTION_CREATED_ON <
		adddate(u.REGISTRATION_TIMESTAMP,interval 5 day ) then p.PAYMENT_TRANSACTION_AMOUNT end)) as d5_amount,
		sum((case when p.PAYMENT_TRANSACTION_CREATED_ON <
		adddate(u.REGISTRATION_TIMESTAMP,interval 7 day ) then p.PAYMENT_TRANSACTION_AMOUNT end)) as d7_amount,
		sum((case when p.PAYMENT_TRANSACTION_CREATED_ON <
		 adddate(u.REGISTRATION_TIMESTAMP,interval 14 day ) then p.PAYMENT_TRANSACTION_AMOUNT end)) as d14_amount,
		sum((case when p.PAYMENT_TRANSACTION_CREATED_ON <
		adddate(u.REGISTRATION_TIMESTAMP,interval 30 day ) then p.PAYMENT_TRANSACTION_AMOUNT end)) as d30_amount
		from payment_transaction p
		join user u on u.user_id=p.user_id
		where TRANSACTION_TYPE_ID IN (8 , 61, 62, 83, 111)
		AND PAYMENT_TRANSACTION_STATUS IN (103 , 125) group by user_id) e on a.user_id=e.user_id
		where a.rdate > '2019-12-31 23:59:59'
		group by date(a.rdate),lp;");
	}
}	