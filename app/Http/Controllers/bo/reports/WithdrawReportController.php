<?php

namespace App\Http\Controllers\bo\reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\helpdesk\MasterTransactionHistory;
use App\Models\WithdrawTransactionHistory;
use App\Models\PaymentTransaction;
use App\Models\User;
use App\Models\UserPoint;
use App\Models\CashoutApproveTransaction;
use App\Models\UserAccountDetails;
use App\Models\GameTransactionHistory;
use Carbon\Carbon;
use App\Traits\Models\common_methods;
use Session;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
use App\Traits\common_mail;
use \PDF;
use DB;
use Auth;
use App\Models\PaymentTransactionPokercommission;
use App\Models\MasterTransactionHistoryPokercommission;

class WithdrawReportController extends Controller
{

  //
  use common_methods, common_mail;

  public function index(Request $request)
  {
    // DB::enableQueryLog();
    $perPage = config('poker_config.paginate.per_page');

    if ($request->isMethod('post')) {

      $page = request()->page;
      $username          = $request->username;
      $ref_no            = $request->ref_no;
      $withdrawStatus    = $request->withdraw_status;
      $transferId        = $request->tranfer_id;
      $withdrawType      = $request->withdraw_type;
      $approveType       = $request->approve_type;
      $amount            = $request->amount;
      $dateFrom          = $request->date_from;
      $dateTo            = $request->date_to;
      $appDateFrom       = $request->app_date_from;
      $appDateTo         = $request->app_date_to;
      $lastUpdateFrom    = $request->last_update_from;
      $lastUpdateTo      = $request->last_update_to;
      $tds_Fee           = $request->tds_fee;
      $first_withdraw    = $request->first_withdraw;
      $userFlag          = $request->user_flag;
      $check_pass        = $request->check_pass;
      $search_by         = $request->search_by;


      //  dd($request->user_flag);
      //session set for excel export

      Session::put('username', $username);
      Session::put('ref_no', $ref_no);
      Session::put('withdrawStatus', $withdrawStatus);
      Session::put('transferId', $transferId);
      Session::put('withdrawType', $withdrawType);
      Session::put('approveType', $approveType);
      Session::put('amount', $amount);
      Session::put('dateFrom', $dateFrom);
      Session::put('dateTo', $dateTo);
      Session::put('app_date_from', $appDateFrom);
      Session::put('app_date_to', $appDateTo);
      Session::put('last_update_from', $lastUpdateFrom);
      Session::put('last_update_to', $lastUpdateTo);
      Session::put('tds_Fee', $tds_Fee);
      Session::put('first_withdraw', $first_withdraw);
      Session::put('userFlag', $userFlag);
      Session::put('check_pass', $check_pass);
      Session::put('search_by', $search_by);
      //session set for excel export end

      // $user_id = null;
      // if (isset($username)) {
      //   $user_id = $this->getUserIDFromUsername($username, $search_by);
      // }
      //get all withdrawl data
      $WithRepoData = $this->getAllWithdrawlData($username, $search_by, $ref_no, $amount, $dateFrom, $dateTo, $withdrawStatus, $transferId, $withdrawType, $approveType, $tds_Fee, $first_withdraw, $userFlag, $check_pass, $appDateFrom, $appDateTo, $lastUpdateFrom, $lastUpdateTo);
      $totalToBePaid = $WithRepoData->get();
      // echo "<pre>";
      // print_r($WithRepoData->getBindings());
      // print_r($WithRepoData->toSql());exit;
      $withData = $WithRepoData->paginate($perPage, ['*'], 'page', $page);
      $params = $request->all();
      $params['page'] = $page;
      if (!empty($username) && empty($user_id) && $search_by == "email") {
        return view('bo.views.reports.withdraw', ['withdrawData' => '', 'params' => $params]);
      } else {
        return view('bo.views.reports.withdraw', ['withdrawData' => $withData, 'params' => $params, 'totalToBePaid' => $totalToBePaid]);
      }
    } else {
      return view('bo.views.reports.withdraw');
    }
  }

  public function getAllWithdrawlData($username, $search_by, $ref_no, $amount, $dateFrom, $dateTo, $withdrawStatus, $transferId, $withdrawType, $approveType, $tds_Fee, $first_withdraw, $userFlag, $check_pass, $appDateFrom, $appDateTo, $lastUpdateFrom, $lastUpdateTo)
  {
    $now = date('Y-m-d H:i:s');
    $yesterday = date('Y-m-d H:i:s');
    if (empty($dateTo)) {
      $dateTo = date('Y-m-d H:i:s');
    }
    if (empty($appDateTo)) {
      $appDateTo = date('Y-m-d H:i:s');
    }
    if (empty($lastUpdateTo)) {
      $lastUpdateTo = date('Y-m-d H:i:s');
    }
    $query = WithdrawTransactionHistory::query();
    $query->from(app(WithdrawTransactionHistory::class)->getTable() . " as wth");
    $query->leftJoin('transaction_status as ts', 'ts.TRANSACTION_STATUS_ID', '=', 'wth.TRANSACTION_STATUS_ID');
    $query->leftJoin('user as u', 'u.USER_ID', '=', 'wth.USER_ID');
    $query->leftJoin('USER_KYC as uk', 'uk.USER_ID', '=', 'wth.USER_ID');
    // $query->leftJoin('withdraw_transaction_history as wth', 'wth.INTERNAL_REFERENCE_NO', '=', 'pt.INTERNAL_REFERENCE_NO');
    $query->when(!empty($ref_no), function ($query) use ($ref_no) {
      return $query->where('wth.INTERNAL_REFERENCE_NO', $ref_no);
    })
      ->when(!empty($amount), function ($query) use ($amount) {
        return $query->where('wth.WITHDRAW_AMOUNT', $amount);
      });
    $query->when(!empty($transferId), function ($query) use ($transferId) {
      return $query->where('wth.GATEWAY_REFERENCE_NO', $transferId);
    });

    // $query->when(!empty($user_id), function ($query) use ($user_id) {
    //   return $query->where('wth.USER_ID', $user_id);
    // });
    $query->when(!empty($withdrawStatus), function ($query) use ($withdrawStatus) {
      return $query->whereIn('wth.TRANSACTION_STATUS_ID', $withdrawStatus);
    });

    $query->when(!empty($withdrawType), function ($query) use ($withdrawType) {
      return $query->where('wth.WITHDRAW_TYPE', $withdrawType);
    });
    $query->when(!empty($approveType) && $approveType == "normal", function ($query) use ($approveType) {
      return $query->whereNotIn('wth.APPROVE_TYPE', ['payu', 'cashfree_greenflag', 'cashfree']);
    });
    $query->when(!empty($approveType) && $approveType == "payu", function ($query) use ($approveType) {
      return $query->where('wth.APPROVE_TYPE', 'payu');
    });
    $query->when(!empty($approveType) && $approveType == "cashfree", function ($query) use ($approveType) {
      return $query->whereIn('wth.APPROVE_TYPE', ['cashfree_greenflag', 'cashfree']);
    });
    $query->when(!empty($dateFrom) && !empty($dateTo) && empty($ref_no), function ($query) use ($dateFrom, $dateTo) {
      $dateFrom = Carbon::parse($dateFrom)->format('Y-m-d H:i:s');
      $dateTo = Carbon::parse($dateTo)->format('Y-m-d H:i:s');
      return $query->whereBetween('wth.TRANSACTION_DATE', [$dateFrom, $dateTo]);
    });
    $query->when(!empty($appDateFrom) && !empty($appDateTo) && empty($ref_no), function ($query) use ($appDateFrom, $appDateTo) {
      $appDateFrom = Carbon::parse($appDateFrom)->format('Y-m-d H:i:s');
      $appDateTo = Carbon::parse($appDateTo)->format('Y-m-d H:i:s');
      return $query->whereBetween('wth.APPROVE_DATE', [$appDateFrom, $appDateTo]);
    });
    $query->when(!empty($lastUpdateFrom) && !empty($lastUpdateTo) && empty($ref_no), function ($query) use ($lastUpdateFrom, $lastUpdateTo) {
      $lastUpdateFrom = Carbon::parse($lastUpdateFrom)->format('Y-m-d H:i:s');
      $lastUpdateTo = Carbon::parse($lastUpdateTo)->format('Y-m-d H:i:s');
      return $query->whereBetween('wth.UPDATED_ON', [$lastUpdateFrom, $lastUpdateTo]);
    });
    $query->when(!empty($tds_Fee) && $tds_Fee == "tds", function ($query) use ($tds_Fee) {
      return $query->where('wth.WITHDRAW_TDS', '!=', "")->where('wth.WITHDRAW_TDS', '!=', NULL);
    });
    $query->when(!empty($tds_Fee) && $tds_Fee == "fee", function ($query) use ($tds_Fee) {
      return $query->where('wth.WITHDRAW_FEE', '!=', "")->where('wth.WITHDRAW_FEE', '!=', NULL);
    });
    $query->when(!empty($userFlag), function ($query) use ($userFlag) {

      return $query->where('u.user_flag', $userFlag);
    });
    $query->when(!empty($check_pass) && $check_pass == 2, function ($query) use ($check_pass) {

      return $query->where('wth.CHECKING_STATUS', $check_pass);
    });

    $query->when(!empty($check_pass) && $check_pass == 3, function ($query) use ($check_pass) {

      return $query->where('wth.CHECKING_STATUS', $check_pass);
    });

    $query->when(!empty($check_pass) && $check_pass == 1, function ($query) use ($check_pass) {

      return $query->whereIn('wth.CHECKING_STATUS', array(NULL, '', 1));
    });
    //search username
    $query->when(!empty($username) && $search_by == "username", function ($query) use ($username) {
      return $query->Where('u.USERNAME', 'LIKE', '%' . $username . '%');
    });

    $query->when(!empty($username) && $search_by == "email", function ($query) use ($username) {
      return $query->where('u.EMAIL_ID', $username);
    });
    $query->when(!empty($username) && $search_by == "contact_no", function ($query) use ($username) {
      return $query->where('u.CONTACT', $username);
    });

    // check first withdraw
    $query->when(!empty($first_withdraw) && $first_withdraw == "yes", function ($query) use ($first_withdraw, $now) {
      return $query->whereNotIn('wth.USER_ID',  function ($query) use ($first_withdraw, $now) {
        $query->select(['pt.USER_ID'])
          ->from((new PaymentTransaction)->getTable() . " as pt")
          ->join('user as u', 'u.USER_ID', '=', 'pt.USER_ID')
          ->where('pt.TRANSACTION_TYPE_ID', 10)
          ->havingRaw('COUNT(pt.USER_ID) > ?', [0])
          ->where('pt.PAYMENT_TRANSACTION_STATUS', 208)
          ->whereBetween('pt.PAYMENT_TRANSACTION_CREATED_ON', ['u.REGISTRATION_TIMESTAMP', $now])
          ->groupBy('pt.USER_ID');
      });
    });
    $query->when(!empty($first_withdraw) && $first_withdraw == "no", function ($query) use ($first_withdraw, $now) {
      return $query->whereIn('wth.USER_ID',  function ($query) use ($first_withdraw, $now) {
        $query->select(['pt.USER_ID'])
          ->from((new PaymentTransaction)->getTable() . " as pt")
          ->join('user as u', 'u.USER_ID', '=', 'pt.USER_ID')
          ->where('pt.TRANSACTION_TYPE_ID', 10)
          ->havingRaw('COUNT(pt.USER_ID) > ?', [0])
          ->where('pt.PAYMENT_TRANSACTION_STATUS', 208)
          ->whereBetween('pt.PAYMENT_TRANSACTION_CREATED_ON', ['u.REGISTRATION_TIMESTAMP', $now])
          ->groupBy('pt.USER_ID');
      });
    });

    $query->where('wth.TRANSACTION_TYPE_ID', '10');
    $query->where('wth.TRANSACTION_STATUS_ID', "!=", '203');
    $query->orderBy('wth.TRANSACTION_DATE', 'DESC');
    $query->groupBy('wth.INTERNAL_REFERENCE_NO');

    $payTransData = $query->select(
      'u.USERNAME',
      'u.REGISTRATION_TIMESTAMP',
      'u.user_flag',
      'u.EMAIL_ID',
      'u.CONTACT',
      'uk.AADHAAR_FRONT_URL',
      'uk.AADHAAR_BACK_URL',
      'uk.AADHAAR_NUMBER',
      'uk.AADHAAR_STATUS',
      'uk.OTHER_DOC_URL',
      'uk.OTHER_DOC_URL_BACK',
      'uk.OTHER_DOC_NUMBER',
      'uk.OTHER_DOC_STATUS',
      // 'uk.PAN_URL',
      'uk.PAN_NUMBER',
      'uk.PAN_STATUS',
      'wth.USER_ID',
      'wth.WITHDRAW_AMOUNT',
      'wth.TRANSACTION_STATUS_ID',
      'wth.TRANSACTION_TYPE_ID',
      'wth.TRANSACTION_DATE',
      'wth.INTERNAL_REFERENCE_NO',
      'wth.UPDATED_ON',
      'wth.APPROVE_TYPE',
      'wth.CHECKING_COMMENT',
      'wth.CHECKING_STATUS',
      'ts.TRANSACTION_STATUS_DESCRIPTION',
      'wth.WITHDRAW_TDS',
      'wth.WITHDRAW_FEE',
      'wth.PAID_AMOUNT',
      'wth.FEE_WAIVED'
      // DB::raw("CASE WHEN pt.TRANSACTION_TYPE_ID = '10'  AND count(pt.user_id) > 0 THEN 'No' ELSE 'Yes' END AS FIRST_WITHDRAW"),
      // DB::raw("MAX(CASE WHEN wth.TRANSACTION_TYPE_ID = 10 THEN wth.PAID_AMOUNT ELSE 0 END) AS toBePaid"), // for get the tobe Paid value
    );
    return $payTransData;
  }

  public function getUserIDFromUsername($username, $search_by)
  {
    return $user_id = User::select('USER_ID', 'EMAIL_ID', 'CONTACT')
      ->when($search_by == "username", function ($query) use ($username) {
        return $query->where(function ($q) use ($username) {
          return $q->where('USERNAME', $username)->orWhere('USERNAME', 'LIKE', '%' . $username . '%');
        });
      })
      ->when($search_by == "email", function ($query) use ($username) {
        return $query->where('EMAIL_ID', $username);
      })
      ->when($search_by == "contact_no", function ($query) use ($username) {
        return $query->where('CONTACT', $username);
      })
      // ->where('USERNAME', $user_name)
      ->first()->USER_ID ?? null;
  }

  //normal approve button click
  public function withdrawNormalApprove(Request $request)
  {
    if (empty($request->all())) {
      return response()->json(['status' => 'failed', 'msg' => 'Please select atleast one row']);
    }
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];
      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $waveoffval = $values['wo'];
        // if ($waveoffval == 'true') {
        //   $feeWaived = 1;
        // } else {
        //   $feeWaived = 0;
        // }
        $withdrwaType = 1;
        $approvedAt = "";

        $withdrawTrans = $this->paymentApprove($refNo, $waveoffval, $withdrwaType, $approvedAt);
        $refNoArray[] = $refNo;
        $armasterStatus[] = $withdrawTrans;
      }

      if (!empty($armasterStatus) && !empty($withdrawTrans->USER_ID)) {
        $data['Withdrwa Normal Approve '] = json_encode($refNoArray);
        $action = "Withdrwa Normal Approve Success";
        $this->insertAdminActivity($data, $action);
        //mail withdraw normal approve params
        $userDetails = $this->getUserDetailsFromUserId($withdrawTrans->USER_ID);
        $params['email'] = $userDetails->EMAIL_ID;
        $params['username'] = $userDetails->USERNAME;
        $params['tempId'] = 12;
        $params['subject'] = "Withdrawl Request Approved";
        $params['refNo'] = $refNo;
        $params['amount'] = $withdrawTrans->WITHDRAW_AMOUNT;
        $params['date'] = date('d-m-Y');
        $params['time'] = date('H:i:s');
        $this->sendMail($params);
        return response()->json(['status' => 'success', 'code' => 1000, 'refNoArray' => $refNoArray]);
      } else {
        $data['Withdrwa Normal Approve Failed'] = json_encode($refNoArray);
        $action = "Withdrwa Normal Approve Failed";
        $this->insertAdminActivity($data, $action);
        return response()->json(['status' => 'failed', 'type' => 'payment_approve_failed', 'msg' => 'some thing went wrong with this payment  (' . $refNo . ') please try again later',  'code' => 1001, 'refNoArray' => $refNoArray]);
      }
    }
  }
  //normal reject button click
  public function withdrawNormalReject(Request $request)
  {
    if (empty($request->all())) {
      return response()->json(['status' => 'failed', 'msg' => 'Please select atleast one row']);
    }
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];
      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $checkRes = $this->getPaymentDetailsFromPaymentTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);

        if ($checkRes->PAYMENT_TRANSACTION_STATUS != "254") { //checking Normal Approve (0) or approve via cashfree(1)
          $updatePaymentTrans = $this->normalWithdrawRejected($refNo, $checkRes->USER_ID);
        }
        $refNoArray[] = $refNo;
        $updatePaymentTransArray[] = $updatePaymentTrans;
      }
      if ($updatePaymentTransArray) {
        $data['Withdrwa Normal Reject Sucess'] = json_encode($refNoArray);
        $action = "Withdrwa Normal Reject Sucess";
        $this->insertAdminActivity($data, $action);
        //mail withdraw normal Reject params
        $userDetails = $this->getUserDetailsFromUserId($checkRes->USER_ID);
        $params['email'] = $userDetails->EMAIL_ID;
        $params['username'] = $userDetails->USERNAME;
        $params['tempId'] = 13;
        $params['subject'] = "Withdrawl Request Rejected";
        $params['refNo'] = $refNo;
        $params['amount'] = $checkRes->PAYMENT_TRANSACTION_AMOUNT;
        $params['date'] = date('d-m-Y');
        $params['time'] = date('H:i:s');
        $this->sendMail($params);
        return response()->json(['status' => 'success', 'type' => 'withdraw_normal_reject', 'code' => 2000, 'refNoArray' => $refNoArray]);
      } else {
        $data['Withdrwa Normal Reject Failed'] = json_encode($refNoArray);
        $action = "Withdrwa Normal Reject Failed";
        $this->insertAdminActivity($data, $action);
        return response()->json(['status' => 'failed', 'type' => 'withdraw_normal_reject', 'msg' => 'some thing went wrong with this payment  (' . $refNoArray . ') please try again later',  'code' => 2001, 'refNoArray' => $refNoArray]);
      }
    }
  }
  // cashfree approve button click
  public function withdrawCashfreeApprove(Request $request)
  {

    if (empty($request->all())) {
      return response()->json(['status' => 'failed', 'msg' => 'Please select atleast one row']);
    }
    if ($request->isMethod('post')) {
      if (empty($request->all()['tableData'])) {
        return response()->json(['status' => 'failed', 'type' => 'empty_selection', 'code' => 501]);
      }
      $request = $request->all()['tableData'];
      foreach ($request as $values) {
        $transferBeneficiaryResponse = [];
        $tranferBeneficiaryData = [];
        $addBeneficiaryData = [];
        $additiionalTracking = [];

        $refNo = $values['refNo'];
        $waveoffval = $values['wo'];
        $getPayUserInfo = $this->getPaymentDetailsFromWithdrawTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL); // //success-pending-reversed, failed(216). when there is no record for the given status, then continue
        $initiatedStatusrRes = $getPayUserInfo->first();
        $transaction_status_id = array(109, 216);

        if (!empty($initiatedStatusrRes) && $initiatedStatusrRes->TRANSACTION_STATUS_ID != "254"  && in_array($initiatedStatusrRes->TRANSACTION_STATUS_ID, $transaction_status_id)) { //if payment is initatiated

          $userId  =  $initiatedStatusrRes->USER_ID;
          $userAccountDetails = $this->getUserAccountDetails($userId);
          $getUserDetails = $this->getUserDetailsFromUserId($userId);
          $userAccountNo = $userAccountDetails->ACCOUNT_NUMBER;
          $userIfscCode = $userAccountDetails->IFSC_CODE;
          $userFullName = $userAccountDetails->ACCOUNT_HOLDER_NAME;
          $userEmail = $getUserDetails->EMAIL_ID;
          $userMobile = $getUserDetails->CONTACT;
          $userAddress = $getUserDetails->ADDRESS;

          $withdrwaType = "";
          $approvedAt = "";
          $transactionStatusID = 254; //initiated

          $tdsApply = $this->reCalculateTdsAndUpdateWhileApprove($initiatedStatusrRes->USER_ID, $initiatedStatusrRes->WITHDRAW_AMOUNT, $initiatedStatusrRes->WITHDRAW_TDS, $initiatedStatusrRes->WITHDRAW_FEE, $refNo, $waveoffval, $withdrwaType, $approvedAt, 'cashfree', $gatewayStatus = NULL, $transactionStatusID);

          if ($tdsApply) {
            $witTrans = $this->getPaymentDetailsFromWithdrawTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, NULL);
            $witTransRes = $witTrans->first();
            $paidamount = $witTransRes->PAID_AMOUNT;
            $updatePaymentStatus = PaymentTransaction::where(['INTERNAL_REFERENCE_NO' => $refNo])
              ->update(['PAYMENT_TRANSACTION_STATUS' => 254]);
            $updateMasterStatus = MasterTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
              ->update(['TRANSACTION_STATUS_ID' => 254]);
          }
          $transferId = $userId . date('YmdHis') . $this->genRandomRefNo(7);
          $this->updateCashfreeTransIdInInstantRespone($transferId, $refNo);
          $addBeneficiaryData["beneId"] = "$userId";
          $addBeneficiaryData["name"] = $userFullName;
          $addBeneficiaryData["email"] = $userEmail;
          $addBeneficiaryData["phone"] = $userMobile;
          $addBeneficiaryData["bankAccount"] = $userAccountNo;
          $addBeneficiaryData["ifsc"] = $userIfscCode;
          $addBeneficiaryData["address1"] = $userAddress;

          $tranferBeneficiaryData["beneId"] = "$userId";
          $tranferBeneficiaryData["amount"] = $paidamount;
          $tranferBeneficiaryData["transferId"] = $transferId;
          $tranferBeneficiaryData["remarks"] = "cash free transfer to $userId";

          $removeBeneficiaryData["beneId"] = "$userId";

          $transferAmount = $paidamount;
          $CASHFREE_PAYOUT_CLIENT_ID = env('CASHFREE_PAYOUT_CLIENT_ID');
          $CASHFREE_PAYOUT_CLIENT_SECRET = env('CASHFREE_PAYOUT_CLIENT_SECRET');
          $CASHFREE_PAYOUT_PAYMENT_URL = env('CASHFREE_PAYOUT_PAYMENT_URL');
          $endpointAuth = $CASHFREE_PAYOUT_PAYMENT_URL . "/authorize";

          $headers = array(
            "X-Client-Id: $CASHFREE_PAYOUT_CLIENT_ID",
            "X-Client-Secret: $CASHFREE_PAYOUT_CLIENT_SECRET"
          );

          $cashfreeGetToken = $this->cashfreePayOutBatchTransferApi($endpointAuth, $headers, $params = []); //get cashfree token

          if ($cashfreeGetToken["status"] == "SUCCESS") { //authendication success
            $endpointVerifyToken = $CASHFREE_PAYOUT_PAYMENT_URL . "/verifyToken";
            $accessToken = $cashfreeGetToken["data"]["token"];
            $accessTokenheaders = array("Authorization: Bearer $accessToken");
            $verifyTokenResponse = $this->cashfreePayOutBatchTransferApi($endpointVerifyToken, $accessTokenheaders); //verify token

            if ($verifyTokenResponse["status"] == "SUCCESS") { //token successfully verified
              $endpointgetBalance = $CASHFREE_PAYOUT_PAYMENT_URL . "/getBalance";
              $getBalanceResponse = $this->getCashfreeCurl($endpointgetBalance, $accessTokenheaders); //get balance

              if ($getBalanceResponse["status"] == "SUCCESS" && $getBalanceResponse["data"]["availableBalance"] >= $transferAmount) {

                $endpointgetBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/getBeneficiary/" . $userId;

                $getBeneficiaryResponse = $this->getCashfreeCurl($endpointgetBeneficiary, $accessTokenheaders);
                $additiionalTracking['getBeneficiary'] = $getBeneficiaryResponse;
                if ($getBeneficiaryResponse["status"] == "SUCCESS") { //beneficiary exists

                  if ($getBeneficiaryResponse["data"]["bankAccount"] == $userAccountNo && $getBeneficiaryResponse["data"]["ifsc"] == $userIfscCode && $getBeneficiaryResponse["data"]["name"] == $userFullName) { // matching Beneficiary account details

                    $endpointTransferBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/requestTransfer";

                    $transferBeneficiaryResponse = $this->cashfreePayOutBatchTransferApi($endpointTransferBeneficiary, $accessTokenheaders, $tranferBeneficiaryData); //Request Transfer
                    $additiionalTracking['transferWithExBen'] = $transferBeneficiaryResponse;
                    $transferBeneficiaryRquestJson = json_encode($tranferBeneficiaryData);
                    $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                    $gateWayJsonResponse = json_encode($transferBeneficiaryResponse);
                    $res = $this->insertCashoutApproveTransaction($witTransRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);

                    if ($transferBeneficiaryResponse["status"] == "SUCCESS") { //transferred successfully with existing beneficiary
                      $cashfreeStatusResponse[] = ['status' => 'success', 'code' => '5010', 'refNo' => $refNo, 'msg' => 'Transferred successfully with existing beneficiary'];
                    } else { //transfer failed with existing beneficiary
                      $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '5050', 'refNo' => $refNo, 'msg' => 'Cashfree Processing'];
                      if (!empty($transferBeneficiaryResponse["status"]) && $transferBeneficiaryResponse["status"] != "PENDING" && ($transferBeneficiaryResponse["status"] != "TRANSFER_REVERSED")) {

                        $this->makeCashfreeFailedIfNotSuccess($refNo);
                      }
                      $transferBeneficiaryRquestJson = json_encode($tranferBeneficiaryData);
                      $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                      $trsnFailedWithExistBen = ['status' => 'failed', 'code' => '5051', 'refNo' => $refNo, 'msg' => 'Transferred FAILED with existing beneficiary'];
                      $gateWayJsonResponse = json_encode($trsnFailedWithExistBen);
                      $res = $this->insertCashoutApproveTransaction($witTransRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                    }
                  } else { //beneficiary account number mismatches
                    /* Remove Beneficiary */
                    $endpointRemoveBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/removeBeneficiary";
                    $removeBeneficiaryResponse = $this->cashfreePayOutBatchTransferApi($endpointRemoveBeneficiary, $accessTokenheaders, $removeBeneficiaryData);
                    $additiionalTracking['remBenIfdontMatch'] = $removeBeneficiaryResponse;
                    if ($removeBeneficiaryResponse["status"] == "SUCCESS") {
                      /* Add Beneficiary */
                      $endpointAddBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/addBeneficiary";
                      $addBeneficiaryResponse = $this->cashfreePayOutBatchTransferApi($endpointAddBeneficiary, $accessTokenheaders, $addBeneficiaryData);
                      $additiionalTracking['addBenAfterRem'] = $addBeneficiaryResponse;
                      if ($addBeneficiaryResponse["status"] == "SUCCESS") { //beneficiary added successfully
                        /* Request Transfer */
                        $endpointTransferBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/requestTransfer";
                        $transferBeneficiaryResponse = $this->cashfreePayOutBatchTransferApi($endpointTransferBeneficiary, $accessTokenheaders, $tranferBeneficiaryData);
                        $additiionalTracking['tranferAftremAddNewBen'] = $transferBeneficiaryResponse;
                        $transferBeneficiaryRquestJson = json_encode($tranferBeneficiaryData);
                        $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                        $gateWayJsonResponse = json_encode($transferBeneficiaryResponse);
                        $res = $this->insertCashoutApproveTransaction($witTransRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                        if ($transferBeneficiaryResponse["status"] == "SUCCESS") { //transferred successfully
                          $cashfreeStatusResponse[] = ['status' => 'success', 'code' => '5012', 'refNo' => $refNo, 'msg' => 'Transferred SUCCESS after remove and add new Beneficiary'];
                        } else {
                          $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '5012', 'refNo' => $refNo, 'msg' => 'Transferred FAILED after remove and add new Beneficiary'];
                          if (!empty($transferBeneficiaryResponse["status"]) &&  $transferBeneficiaryResponse["status"] != "PENDING" && $transferBeneficiaryResponse["status"] != "TRANSFER_REVERSED") {
                            $this->makeCashfreeFailedIfNotSuccess($refNo);
                          }

                          $transferBeneficiaryRquestJson = json_encode($tranferBeneficiaryData);
                          $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                          $removeBen = ['status' => 'failed', 'code' => '5012', 'refNo' => $refNo, 'msg' => 'Transferred FAILED after remove and add new Beneficiary'];
                          $gateWayJsonResponse = json_encode($removeBen);
                          $res = $this->insertCashoutApproveTransaction($witTransRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                        }
                        /* Request Transfer */
                      } else {
                        $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '5018', 'refNo' => $refNo, 'msg' => 'failed to Remove old Beneficiary and add new Beneficiary'];
                        //if ($transferBeneficiaryResponse["status"] !== "PENDING" && $transferBeneficiaryResponse["status"] !== "TRANSFER_REVERSED") {
                        $this->makeCashfreeFailedIfNotSuccess($refNo);
                        $transferBeneficiaryRquestJson = json_encode($tranferBeneficiaryData);
                        $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                        $removeOldAddNewBen = ['status' => 'failed', 'code' => '5019', 'refNo' => $refNo, 'msg' => 'failed to Remove old Beneficiary and add new Beneficiary'];
                        $gateWayJsonResponse = json_encode($removeOldAddNewBen);
                        $res = $this->insertCashoutApproveTransaction($witTransRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);


                        //}
                      }
                    } else {
                      $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '5013', 'refNo' => $refNo, 'msg' => 'failed to Remove old Beneficiary'];
                      $transferBeneficiaryRquestJson = json_encode($tranferBeneficiaryData);
                      $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                      $failremoveOlBen = ['status' => 'failed', 'code' => '5013', 'refNo' => $refNo, 'msg' => 'failed to Remove old Beneficiary'];
                      $gateWayJsonResponse = json_encode($failremoveOlBen);
                      $res = $this->insertCashoutApproveTransaction($witTransRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                      if (!empty($transferBeneficiaryResponse["status"]) && $transferBeneficiaryResponse["status"] != "PENDING" && $transferBeneficiaryResponse["status"] != "TRANSFER_REVERSED") {
                        $this->makeCashfreeFailedIfNotSuccess($refNo);
                      }
                    }
                    /* Remove Beneficiary */
                  }
                } else if ($getBeneficiaryResponse["subCode"] == 404) { //beneficiary does not exist //add beneficiary


                  /* Add Beneficiary */
                  $endpointAddBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/addBeneficiary";
                  $addBeneficiaryResponse = $this->cashfreePayOutBatchTransferApi($endpointAddBeneficiary, $accessTokenheaders, $addBeneficiaryData);
                  $additiionalTracking['addNewBen'] = $addBeneficiaryResponse;
                  if ($addBeneficiaryResponse["status"] == "SUCCESS") { //beneficiary added successfully
                    /* Request Transfer */
                    $endpointTransferBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/requestTransfer";
                    $transferBeneficiaryResponse = $this->cashfreePayOutBatchTransferApi($endpointTransferBeneficiary, $accessTokenheaders, $tranferBeneficiaryData);
                    $additiionalTracking['tranferWithNewBen'] = $transferBeneficiaryResponse;
                    $transferBeneficiaryRquestJson = json_encode($addBeneficiaryData);
                    $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                    $gateWayJsonResponse = json_encode($transferBeneficiaryResponse);
                    $res = $this->insertCashoutApproveTransaction($witTransRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                    if ($transferBeneficiaryResponse["status"] == "SUCCESS") { //transferred successfully after adding Beneficiary
                      $cashfreeStatusResponse[] = ['status' => 'success', 'code' => '5013', 'refNo' => $refNo, 'msg' => 'Transferred Success after adding beneficiary'];
                    } else { //transferred failed after adding Beneficiary
                      $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '5014', 'refNo' => $refNo, 'msg' => 'Transferred failed after adding beneficiary'];
                      $transferBeneficiaryRquestJson = json_encode($addBeneficiaryData);
                      $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                      $failedAddBen = ['status' => 'failed', 'code' => '5014', 'refNo' => $refNo, 'msg' => 'Transferred failed after adding beneficiary'];
                      $gateWayJsonResponse = json_encode($failedAddBen);
                      $res = $this->insertCashoutApproveTransaction($witTransRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                      if (!empty($transferBeneficiaryResponse["status"]) &&  $transferBeneficiaryResponse["status"] != "PENDING" && $transferBeneficiaryResponse["status"] != "TRANSFER_REVERSED") {

                        $this->makeCashfreeFailedIfNotSuccess($refNo);
                      }
                    }
                    /* Request Transfer */
                  } else { // failed to add  Beneficiary
                    $this->makeCashfreeFailedIfNotSuccess($refNo);
                    $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '5071', 'refNo' => $refNo, 'msg' => 'Failed To add beneficiary'];
                    $transferBeneficiaryRquestJson = json_encode($addBeneficiaryData);
                    $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                    $failToAddBen = ['status' => 'failed', 'code' => '5072', 'refNo' => $refNo, 'msg' => 'Failed To add beneficiary'];
                    $gateWayJsonResponse = json_encode($failToAddBen);
                    $res = $this->insertCashoutApproveTransaction($witTransRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                  }
                  /* Add Beneficiary */
                }
              } else {
                $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '7001', 'refNo' => $refNo, 'msg' => 'Insufficient Balance !'];
                $this->makeCashfreeFailedIfNotSuccess($refNo);
              }
            } else {
              $this->makeCashfreeFailedIfNotSuccess($refNo);
              $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '7001', 'refNo' => $refNo, 'msg' => 'Failed to veify token!'];
            }
          } else {
            $this->makeCashfreeFailedIfNotSuccess($refNo);
            $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '7001', 'refNo' => $refNo, 'msg' => 'Failed to generate token!'];
          }
        }
        //adtional tracking 

        $gateWayJsonResponse = json_encode($additiionalTracking);
        $res = $this->insertCashoutApproveTransaction($witTransRes->USER_ID, $refNo, $paidamount, $transferId, "ADTIONAL_TRACKING_ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, "ADTIONAL_TRACKING_ERROR", "cashfree", NULL);
      }
      return response()->json($cashfreeStatusResponse ?? "Something Went wrong !");
    }
  }

  public function cashfreePayOutBatchTransferApi($endpoint, $headers, $params = [])
  {
    $postFields = json_encode($params);
    array_push($headers, 'Content-Type: application/json', 'Content-Length: ' . strlen($postFields));
    $endpoint = $endpoint;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $returnData = curl_exec($ch);
    curl_close($ch);
    if ($returnData != "") {
      return json_decode($returnData, true);
    }
    return NULL;
  }


  //get Beneficiary and check cashfree cashout balance
  public function getCashfreeCurl($endpoint, $headers)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $returnData = curl_exec($ch);
    $err = curl_error($ch);
    curl_close($ch);
    if ($returnData != "") {
      return json_decode($returnData, true);
    }
    return NULL;
  }

  // cashfree reject button click
  public function withdrawCashfreeReject(Request $request)
  {
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];
      foreach ($request as $values) {
        $refNo = $values['refNo'];

        $checkRes = $this->getPaymentDetailsFromWithdrawTransaction($refNo, NULL, NULL, NULL); //For TDS Calculation
        $checkRes = $checkRes->first();
        if ($checkRes->TRANSACTION_STATUS_ID == 216 && ($checkRes->APPROVE_TYPE == 'cashfree' || $checkRes->APPROVE_TYPE == 'cashfree_greenflag')) { //checking Normal Approve (0) or approve via cashfree(1)
          $updatePaymentTrans = $this->normalWithdrawRejected($refNo, $checkRes->USER_ID);
        }
        $refNoArray[] = $refNo;
        $updatePaymentTransArray[] = $updatePaymentTrans;
      }
      if ($updatePaymentTransArray) {
        $data['Withdrwa Cashfree Reject Sucess'] = json_encode($refNoArray);
        $action = "Withdrwa Cashfree Reject Sucess";
        $this->insertAdminActivity($data, $action);
        //mail withdraw normal Reject params
        $userDetails = $this->getUserDetailsFromUserId($checkRes->USER_ID);
        $params['email'] = $userDetails->EMAIL_ID;
        $params['username'] = $userDetails->USERNAME;
        $params['tempId'] = 13;
        $params['subject'] = "Withdrawl Request Rejected";
        $params['refNo'] = $refNo;
        $params['amount'] = $checkRes->PAYMENT_TRANSACTION_AMOUNT;
        $params['date'] = date('d-m-Y');
        $params['time'] = date('H:i:s');
        $this->sendMail($params);
        return response()->json(['status' => 'success', 'type' => 'withdraw_Cashfree_reject', 'code' => 2040, 'refNoArray' => $refNoArray]);
      } else {
        $data['Withdrwa Cashfree Reject Failed'] = json_encode($refNoArray);
        $action = "Withdrwa Cashfree Reject Failed";
        $this->insertAdminActivity($data, $action);
        return response()->json(['status' => 'failed', 'type' => 'withdraw_Cashfree_reject', 'msg' => 'some thing went wrong with this payment  (' . $refNoArray . ') please try again later',  'code' => 2001, 'refNoArray' => $refNoArray]);
      }
    }
  }

  //payu approve button click
  public function withdrawPayuApprove(Request $request)
  {
    if (empty($request->all())) {
      return response()->json(['status' => 'failed', 'msg' => 'Please select atleast one row']);
    }
    if ($request->isMethod('post')) {
      if (empty($request->all()['tableData'])) {
        return response()->json(['status' => 'failed', 'type' => 'empty_selection', 'code' => 501]);
      }
      $request = $request->all()['tableData'];
      $sumOfPaidAmount = 0;
      foreach ($request as $values) {
        $refNoArrayForBalCheck[] = $values['refNo'];
      }
      $sumOfPaidAmount = $this->getPaidSumWithdrawTransaction($refNoArrayForBalCheck);
      //check available balance
      $avillavleBalance = $this->getPayuMerchantAvailableBalance();
      if ($sumOfPaidAmount > $avillavleBalance) {
        return response()->json([
          'status' => 'failed', 'type' => 'insufficient_balance',
          'msg' => 'Insufficient Wallet Balance. Your sum of selected amount should be less than  (' . $avillavleBalance . ')', 'code' => 3025
        ]);
      }
      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $waveoffval = $values['wo'];
        $witTrans = $this->getPaymentDetailsFromWithdrawTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);
        $witTransRes = $witTrans->first();
        $transaction_status_id = array(109, 216);
        // echo  $InitiatedStatusrRes->TRANSACTION_STATUS_ID; exit;
        if (!empty($witTransRes) && $witTransRes->TRANSACTION_STATUS_ID != "254"  && in_array($witTransRes->TRANSACTION_STATUS_ID, $transaction_status_id)) { //if payment is initatiated 
          $withdrwaType = "";
          $approvedAt = "";
          $transactionStatusID = 254; //initiated
          $tdsApply = $this->reCalculateTdsAndUpdateWhileApprove($witTransRes->USER_ID, $witTransRes->WITHDRAW_AMOUNT, $witTransRes->WITHDRAW_TDS, $witTransRes->WITHDRAW_FEE, $refNo, $waveoffval, $withdrwaType, $approvedAt, $gateWay = 'payu', $gatewayStatus = NULL, $transactionStatusID);

          $userId  =  $witTransRes->USER_ID;
          $userAccountDetails = $this->getUserAccountDetails($userId);
          $getUserDetails = $this->getUserDetailsFromUserId($userId);
          $userAccountNo = $userAccountDetails->ACCOUNT_NUMBER;
          $userIfscCode = $userAccountDetails->IFSC_CODE;
          $userFullName = $userAccountDetails->ACCOUNT_HOLDER_NAME;
          $userEmail = $getUserDetails->EMAIL_ID;
          $userMobile = $getUserDetails->CONTACT;

          $witTransResData = $this->getPaymentDetailsFromWithdrawTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);
          $witTransResDataAfterCorrect = $witTransResData->first();

          if (!empty($tdsApply) && !empty($witTransResDataAfterCorrect)) {
            //  $paidamount = $witTransRes->PAID_AMOUNT;
            $paidamount = ($waveoffval == "true" ? $witTransResDataAfterCorrect->PAID_AMOUNT + $witTransResDataAfterCorrect->WITHDRAW_FEE : $witTransResDataAfterCorrect->PAID_AMOUNT);

            $updatePaymentStatus = PaymentTransaction::where(['INTERNAL_REFERENCE_NO' => $refNo])
              ->update(['PAYMENT_TRANSACTION_STATUS' => 254]);
            $updateMasterStatus = MasterTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
              ->update(['TRANSACTION_STATUS_ID' => 254]);
          }

          $payuDataArray[] = [
            "beneficiaryAccountNumber" => $userAccountNo,
            "beneficiaryIfscCode" => $userIfscCode,
            "beneficiaryName" => $userFullName,
            "beneficiaryEmail" => $userEmail,
            "beneficiaryMobile" => $userMobile,
            "purpose" => "transfer request",
            "amount" => $paidamount,
            "batchId" => $witTransResDataAfterCorrect->USER_ID,
            "merchantRefId" => $refNo,
            "paymentType" => "IMPS"
          ];
          // $payuREquest = array('Payu Cashout Request' => $payuDataArray);
          $refNoArray[] = $refNo;
          $sumOfPaidAmount += $paidamount;
          $mailArraydata[] = [
            "email" => $userEmail,
            "username" => $getUserDetails->USERNAME,
            "refNo" => $refNo,
            "amount" => $paidamount
          ];
        } else {
          return response()->json(['status' => 'success', 'type' => 'payment_under_process', 'msg' => 'you cant`t approve this payment.Because this payment (' . $refNo . ') is under process ',  'code' => 3010, 'refNoArray' => $refNo]);
        }
      }
      $casOutPaymentUserData = json_encode($payuDataArray);
      $payuRes = $this->payuCashoutApi($casOutPaymentUserData);
      $gateWayJsonResponse = json_encode($payuRes);
      foreach ($payuDataArray as $payuDataArrayValue) {
        $gateWayjsonRequest = json_encode($payuDataArrayValue);
        $res = $this->insertCashoutApproveTransaction($payuDataArrayValue['batchId'], $payuDataArrayValue['merchantRefId'], $payuDataArrayValue['amount'], NULL, NULL, $payuDataArrayValue['beneficiaryName'], $payuDataArrayValue['beneficiaryAccountNumber'], $payuDataArrayValue['beneficiaryIfscCode'], $gateWayjsonRequest, $gateWayJsonResponse, $gateWayMessage = NULL, "payu", NULL);
      }
      $payuRes = json_decode($payuRes);
      //mail withdraw payu approve params
      foreach ($mailArraydata as $mailData) {
        $params['email'] = $mailData['email'];
        $params['username'] = $mailData['username'];
        $params['tempId'] = 12;
        $params['subject'] = "Withdrawl Request Approved";
        $params['refNo'] = $mailData['refNo'];
        $params['amount'] = $mailData['amount'];
        $params['date'] = date('d-m-Y');
        $params['time'] = date('H:i:s');
        $this->sendMail($params);
      }
      if ($payuRes->status == 0) {
        return response()->json(['status' => 'success', 'type' => 'withdraw_payu_approve', 'code' => 3000, 'refNoArray' => $refNoArray]);
      } else {
        return response()->json(['status' => 'failed', 'msg' => 'Something went wrong unable to approve , please try gain !', 'type' => 'withdraw_payu_approve', 'code' => 3001]);
      }
    }
  }

  //payu Reject button click
  public function withdrawPayuReject(Request $request)
  {
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];
      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $checkRes = $this->getPaymentDetailsFromWithdrawTransaction($refNo, NULL, NULL, NULL); //For TDS Calculation
        $checkRes = $checkRes->first();
        if ($checkRes->TRANSACTION_STATUS_ID == 216 && $checkRes->APPROVE_TYPE == 'payu') { //checking Normal Approve (0) or approve via cashfree(1)

          $updatePaymentTrans = $this->normalWithdrawRejected($refNo, $checkRes->USER_ID);
        }
        $refNoArray[] = $refNo;
        $updatePaymentTransArray[] = $updatePaymentTrans;
      }
      if ($updatePaymentTransArray) {
        $data['Withdrwa Payu Reject Sucess'] = json_encode($refNoArray);
        $action = "Withdrwa Payu Reject Sucess";
        $this->insertAdminActivity($data, $action);
        //mail withdraw normal Reject params
        $userDetails = $this->getUserDetailsFromUserId($checkRes->USER_ID);
        $params['email'] = $userDetails->EMAIL_ID;
        $params['username'] = $userDetails->USERNAME;
        $params['tempId'] = 13;
        $params['subject'] = "Withdrawl Request Rejected";
        $params['refNo'] = $refNo;
        $params['amount'] = $checkRes->PAYMENT_TRANSACTION_AMOUNT;
        $params['date'] = date('d-m-Y');
        $params['time'] = date('H:i:s');
        $this->sendMail($params);
        return response()->json(['status' => 'success', 'type' => 'withdraw_Payu_reject', 'code' => 2020, 'refNoArray' => $refNoArray]);
      } else {
        $data['Withdrwa Payu Reject Failed'] = json_encode($refNoArray);
        $action = "Withdrwa Payu Reject Failed";
        $this->insertAdminActivity($data, $action);
        return response()->json(['status' => 'failed', 'type' => 'withdraw_Payu_reject', 'msg' => 'some thing went wrong with this payment  (' . $refNoArray . ') please try again later',  'code' => 2001, 'refNoArray' => $refNoArray]);
      }
    }
  }


  //passed button click
  public function withdrawPassed(Request $request)
  {
    if (empty($request->all())) {
      return response()->json(['status' => 'failed', 'msg' => 'Please select atleast one row']);
    }
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];
      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $checkingComment = $values['cs'] ?? "";
        $checkingStatus = 2;

        $updatePassStatus = WithdrawTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
          ->update(['CHECKING_STATUS' => $checkingStatus, 'CHECKING_COMMENT' => $checkingComment]);
        $refNoArray[] = $refNo;
      }
      if ($updatePassStatus) {
        return response()->json(['status' => 'success', 'type' => 'update_pass_status_comment', 'code' => 7000, 'refNoArray' => $refNoArray]);
      } else {
        return response()->json(['status' => 'failed', 'type' => 'update_pass_status_comment', 'code' => 7001]);
      }
    }
  }


  //not passed button click
  public function withdrawNotPassed(Request $request)
  {
    if (empty($request->all())) {
      return response()->json(['status' => 'failed', 'msg' => 'Please select atleast one row']);
    }
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];

      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $checkingComment = $values['cs'] ?? "";
        $checkingStatus = 3;
        $updatePassStatus = WithdrawTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
          ->update(['CHECKING_STATUS' => $checkingStatus, 'CHECKING_COMMENT' => $checkingComment]);
        $refNoArray[] = $refNo;
      }
      if ($updatePassStatus) {
        return response()->json(['status' => 'success', 'type' => 'update_pass_status_comment', 'code' => 8000, 'refNoArray' => $refNoArray]);
      } else {
        return response()->json(['status' => 'failed', 'type' => 'update_pass_status_comment', 'code' => 8001]);
      }
    }
  }


  //not passed button click
  public function withdrawUnchecked(Request $request)
  {
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];

      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $checkingComment = $values['cs'] ?? "";
        $checkingStatus = 1;

        $updatePassStatus = WithdrawTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
          ->update(['CHECKING_STATUS' => $checkingStatus, 'CHECKING_COMMENT' => $checkingComment]);
        $refNoArray[] = $refNo;
      }
      if ($updatePassStatus) {
        return response()->json(['status' => 'success', 'type' => 'update_pass_status_comment', 'code' => 8000, 'refNoArray' => $refNoArray]);
      } else {
        return response()->json(['status' => 'failed', 'type' => 'update_pass_status_comment', 'code' => 8001]);
      }
    }
  }

  public function paymentApprove($refNo, $feeWaived, $withdrwaType, $approvedAt)
  {
    $withdrawtRes = $this->getPaymentDetailsFromWithdrawTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);
    $withdrawtRes = $withdrawtRes->first();
    if ($withdrawtRes->TRANSACTION_STATUS_ID == 109 && $withdrawtRes->TRANSACTION_STATUS_ID != 254) { //checking Normal Approve (0) or approve via cashfree(1)

      $transactionStatusID = 208;
      $tdsApply = $this->reCalculateTdsAndUpdateWhileApprove($withdrawtRes->USER_ID, $withdrawtRes->WITHDRAW_AMOUNT, $withdrawtRes->WITHDRAW_TDS, $withdrawtRes->WITHDRAW_FEE, $refNo, $feeWaived, $withdrwaType, $approvedAt, $gateWay = 'normal', $gatewayStatus = NULL, $transactionStatusID);
      if ($tdsApply) {
        $currentStatusId = 109;
        $paymentAndMsterAsSuccessStatus = $this->updateMasterAndPaymentStatusForSuccess($refNo, $currentStatusId); // for normal approve change status from pending to success and calculate tds and credit tds bonus
        return $withdrawtRes;
      }
    } else {
      return response()->json(['status' => 'failed', 'type' => 'normal_approve_failed', 'code' => 9001]);
    }
  }

  public function normalWithdrawRejected($internalReferenceNo, $userID)
  {
    $masTransTypeId = ['10'];
    $mastTransStatusId = ['109', '208', '216'];
    $rstTransactionsData = $this->getPaymentDetailsFromMasterTransaction($internalReferenceNo, $masTransTypeId, $mastTransStatusId, $userID);

    if (!empty($rstTransactionsData)) {  //only allowed pending transactions
      $payTransStatusId = ['112'];
      $rstpTransactionsData = $this->getPaymentDetailsFromPaymentTransaction($internalReferenceNo, $payTransTypeId = NULL, $payTransStatusId, $userID);

      if (empty($rstpTransactionsData)) {
        $transactionTypeID = 75;
        $transactionStatusID = 112;
        $createdBY = $userID;
        $createdOn = date('Y-m-d H:i:s');
        $internalRefNo = $internalReferenceNo;
        $rstTransactionsData = $rstTransactionsData->get();
        // echo  "<pre>";
        // print_r($rstTransactionsData);exit;
        foreach ($rstTransactionsData as $tIndex => $transactionData) {
          //fetch user current wallet information
          $getAccountInfo = $this->getUsercurrentBalance($userID, 1);
          //update user points
          if ($transactionData->BALANCE_TYPE_ID == 2) { //update the userpoints in deposit balance
            $newDBalance = $getAccountInfo->USER_DEPOSIT_BALANCE + $transactionData->TRANSACTION_AMOUNT;
            $newTBalance = $getAccountInfo->USER_TOT_BALANCE + $transactionData->TRANSACTION_AMOUNT;

            $updateUserPoints = UserPoint::where(['USER_ID' => $userID, 'COIN_TYPE_ID' => 1])
              ->update(['VALUE' => $newDBalance, 'USER_DEPOSIT_BALANCE' => $newDBalance, 'USER_TOT_BALANCE' => $newTBalance]);
            $curretnTotBalance = $getAccountInfo->USER_TOT_BALANCE;
            $newTotBalance = $curretnTotBalance + $transactionData->TRANSACTION_AMOUNT;
          } else if ($transactionData->BALANCE_TYPE_ID == 1) { //update the userpoints in deposit balance
            $newDBalance = $getAccountInfo->USER_DEPOSIT_BALANCE + $transactionData->TRANSACTION_AMOUNT;
            $newTBalance = $getAccountInfo->USER_TOT_BALANCE + $transactionData->TRANSACTION_AMOUNT;

            $updateUserPoints = UserPoint::where(['USER_ID' => $userID, 'COIN_TYPE_ID' => 1])
              ->update(['VALUE' => $newDBalance, 'USER_DEPOSIT_BALANCE' => $newDBalance, 'USER_TOT_BALANCE' => $newTBalance]);
            $curretnTotBalance = $getAccountInfo->USER_TOT_BALANCE;
            $newTotBalance = $curretnTotBalance + $transactionData->TRANSACTION_AMOUNT;
          } else { //update the userpoints in win balance
            if ($transactionData->TRANSACTION_STATUS_ID == 208 && $transactionData->TRANSACTION_TYPE_ID == 80) { //TDS Amount           
              $tdsAmount = ($transactionData->TRANSACTION_AMOUNT * 50 / 100);
              $newWBalance = $getAccountInfo->USER_WIN_BALANCE + $tdsAmount;
              $newTBalance = $getAccountInfo->USER_TOT_BALANCE + $tdsAmount;
              $updateUserPoints = UserPoint::where(['USER_ID' => $userID, 'COIN_TYPE_ID' => 1])
                ->update(['VALUE' => $newWBalance, 'USER_WIN_BALANCE' => $newWBalance, 'USER_TOT_BALANCE' => $newTBalance]);
              $transactionTypeID = 107;
              $transactionStatusID = 112;
              $transactionData->TRANSACTION_AMOUNT = $tdsAmount;
              $curretnTotBalance = $getAccountInfo->USER_TOT_BALANCE;
              $newTotBalance = $curretnTotBalance + $tdsAmount;
            } else {
              $transactionTypeID = 75;
              $newWBalance = $getAccountInfo->USER_WIN_BALANCE + $transactionData->TRANSACTION_AMOUNT;
              $newTBalance = $getAccountInfo->USER_TOT_BALANCE + $transactionData->TRANSACTION_AMOUNT;
              $updateUserPoints = UserPoint::where(['USER_ID' => $userID, 'COIN_TYPE_ID' => 1])
                ->update(['VALUE' => $newWBalance, 'USER_WIN_BALANCE' => $newWBalance, 'USER_TOT_BALANCE' => $newTBalance]);
              $curretnTotBalance = $getAccountInfo->USER_TOT_BALANCE;
              $newTotBalance = $curretnTotBalance + $transactionData->TRANSACTION_AMOUNT;
            }
          }

          $partner_id = 10001;
          $masterTransRes = $this->insertMasterTransTable(
            $userID,
            $transactionData->BALANCE_TYPE_ID,
            $transactionStatusID,
            $transactionTypeID,
            $transactionData->TRANSACTION_AMOUNT,
            $createdOn,
            $internalRefNo,
            $curretnTotBalance,
            $newTotBalance,
            $partner_id
          );
          // $this->insertWithdrawTransTable($userID, $transactionData->BALANCE_TYPE_ID, $transactionStatusID, $transactionTypeID, $transactionData->TRANSACTION_AMOUNT, 0, 0, 0, 0, 0, $internalRefNo, 'normal', '', '', 'normal', NULL);
          $updatePaymentTrans = WithdrawTransactionHistory::where(['USER_ID' => $userID, 'INTERNAL_REFERENCE_NO' => $internalReferenceNo])
            ->update(['TRANSACTION_STATUS_ID' => $transactionStatusID]);
          //update status into payment transaction
          $updatePaymentTrans = PaymentTransaction::where(['USER_ID' => $userID, 'INTERNAL_REFERENCE_NO' => $internalReferenceNo])
            ->update(['PAYMENT_TRANSACTION_STATUS' => $transactionStatusID]);
          if ($updatePaymentTrans) {
            // Update Player Ledger for revert
            $this->updatePlayerLedger($internalReferenceNo, "REVERT_REJECT");
            return $updatePaymentTrans;
          }
        }
      }
    }
  }

  public function getPaymentDetailsFromPaymentTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL)
  {
    $res = PaymentTransaction::select(
      'PAYMENT_TRANSACTION_STATUS',
      'TRANSACTION_TYPE_ID',
      'PAYMENT_TRANSACTION_AMOUNT',
      'USER_ID'
    );
    $res->when(!empty($refNo), function ($res) use ($refNo) {
      return $res->where(['INTERNAL_REFERENCE_NO' => $refNo]);
    });
    $res->when(!empty($transTypeId), function ($res) use ($transTypeId) {
      return $res->whereIn('TRANSACTION_TYPE_ID', $transTypeId);
    });
    $res->when(!empty($transStatusId), function ($res) use ($transStatusId) {
      return $res->whereIn('PAYMENT_TRANSACTION_STATUS', $transStatusId);
    });
    $res->when(!empty($userId), function ($res) use ($userId) {
      return $res->where(['USER_ID' => $userId]);
    });
    return $res->first();
  }

  public function getPaymentDetailsFromMasterTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL)
  {
    $res = MasterTransactionHistory::select(
      'USER_ID',
      'INTERNAL_REFERENCE_NO',
      'TRANSACTION_STATUS_ID',
      'TRANSACTION_TYPE_ID',
      'TRANSACTION_AMOUNT',
      'BALANCE_TYPE_ID',
      'TRANSACTION_AMOUNT'
    );
    $res->when(!empty($refNo), function ($res) use ($refNo) {
      return $res->where(['INTERNAL_REFERENCE_NO' => $refNo]);
    });
    $res->when(!empty($transTypeId), function ($res) use ($transTypeId) {
      return $res->whereIn('TRANSACTION_TYPE_ID', $transTypeId);
    });
    $res->when(!empty($transStatusId), function ($res) use ($transStatusId) {
      return $res->whereIn('TRANSACTION_STATUS_ID', $transStatusId);
    });
    $res->when(!empty($userId), function ($res) use ($userId) {
      return $res->where(['USER_ID' => $userId]);
    });
    return $res;
    // print_r($res->getBindings());
    // dd($res->toSql());
  }

  public function getPaymentDetailsFromWithdrawTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL)
  {
    $res = WithdrawTransactionHistory::select(
      'USER_ID',
      'INTERNAL_REFERENCE_NO',
      'BALANCE_TYPE_ID',
      'TRANSACTION_STATUS_ID',
      'TRANSACTION_TYPE_ID',
      'WITHDRAW_AMOUNT',
      'WITHDRAW_TDS',
      'WITHDRAW_FEE',
      'PAYABLE_AMOUNT',
      'FEE_WAIVED',
      'PAID_AMOUNT',
      'APPROVED_BY',
      'APPROVE_TYPE',
      'GATEWAY_STATUS',
      'GATEWAY_REFERENCE_NO'
    );
    $res->when(!empty($refNo), function ($res) use ($refNo) {
      return $res->where(['INTERNAL_REFERENCE_NO' => $refNo]);
    });
    $res->when(!empty($transTypeId), function ($res) use ($transTypeId) {
      return $res->whereIn('TRANSACTION_TYPE_ID', $transTypeId);
    });
    $res->when(!empty($transStatusId), function ($res) use ($transStatusId) {
      return $res->whereIn('TRANSACTION_STATUS_ID', $transStatusId);
    });
    $res->when(!empty($userId), function ($res) use ($userId) {
      return $res->where(['USER_ID' => $userId]);
    });
    return $res;
    // print_r($res->getBindings());
    // dd($res->toSql());
  }

  public function updateMasterAndPaymentStatusForSuccess($refNo, $currentStatusId)
  {
    //check the transaction is in pending status or not
    $statusInfo = $this->getPaymentDetailsFromPaymentTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);
    $statusId = $statusInfo->PAYMENT_TRANSACTION_STATUS;
    $transTypeId = array('10');
    $tdsInfo = $this->getPaymentDetailsFromWithdrawTransaction($refNo, $transTypeId, $transStatusId = NULL, $userId = NULL); //For TDS Calculation
    $tdsInfo = $tdsInfo->first();
    if (!empty($tdsInfo->USER_ID && $tdsInfo->WITHDRAW_TDS > 0)) {
      $this->calculateTdsBonusAndCredit($tdsInfo);
    }
    if ($statusId == $currentStatusId) {
      $updateStatusOnmasterTrans = MasterTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
        ->update(['TRANSACTION_STATUS_ID' => 208]);
      if ($updateStatusOnmasterTrans) {
        $paymentTransStatus = PaymentTransaction::where(['INTERNAL_REFERENCE_NO' => $refNo])
          ->update(['PAYMENT_TRANSACTION_STATUS' => 208]);
        return $paymentTransStatus;
      } else {
        return FALSE;
      }
    } else {
      return FALSE;
    }
  }
  public function calculateTdsBonusAndCredit($tdsData)
  {
    if ($tdsData->WITHDRAW_TDS > 0) {
      $percentage = 100;
      $tdsAmount = ($percentage / 100) * $tdsData->WITHDRAW_TDS;
      $userID = $tdsData->USER_ID;
      $tdsAmount = $this->getBonusOnTds($userID, $tdsAmount); // New code to cap total TDS bonus at 30 k
      $getAccountInfo = $this->getUsercurrentBalance($userID, 1);
      if ($tdsAmount > 0) { // New Code to check if TDS bonus is more than 0
        $newWinBalance = $getAccountInfo->USER_WIN_BALANCE + $tdsAmount;
        $newWBalance = $getAccountInfo->USER_PROMO_BALANCE + $tdsAmount;
        $newTBalance = $getAccountInfo->USER_TOT_BALANCE + $tdsAmount;
        $updateUserPoints = UserPoint::where(['USER_ID' => $tdsData->USER_ID, 'COIN_TYPE_ID' => 1])
          ->update(['VALUE' => $newWBalance, 'USER_PROMO_BALANCE' => $newWBalance, 'USER_TOT_BALANCE' => $newTBalance]);

        $transactionTypeID = 106;
        $transactionStatusID = 208;
        $createdBY = $userID;
        $createdOn = date('Y-m-d H:i:s');
        $curretnTotBalance = $getAccountInfo->USER_TOT_BALANCE;
        $newTotBalance = $curretnTotBalance + $tdsAmount;
        $partner_id = 10001;
        $masterTransRes = $this->insertMasterTransTable(
          $userID,
          2,
          $transactionStatusID,
          $transactionTypeID,
          $tdsAmount,
          $createdOn,
          $tdsData->INTERNAL_REFERENCE_NO,
          $curretnTotBalance,
          $newTotBalance,
          $partner_id
        );
      }
    }
  }

  //insert in master transaction table after approve
  public function insertMasterTransTable(
    $user_id,
    $balance_type_id,
    $trans_status_id,
    $trans_type_id,
    $tdsAmount,
    $trans_date,
    $ref_no,
    $current_bal,
    $closing_bal,
    $partner_id
  ) {
    // DB::enableQueryLog();
    $masterTransaction = new MasterTransactionHistory();
    $masterTransaction->USER_ID                = $user_id;
    $masterTransaction->BALANCE_TYPE_ID        = $balance_type_id;
    $masterTransaction->TRANSACTION_STATUS_ID  = $trans_status_id;
    $masterTransaction->TRANSACTION_TYPE_ID    = $trans_type_id;
    $masterTransaction->TRANSACTION_AMOUNT     = $tdsAmount;
    $masterTransaction->TRANSACTION_DATE       = $trans_date;
    $masterTransaction->INTERNAL_REFERENCE_NO  = $ref_no;
    $masterTransaction->CURRENT_TOT_BALANCE    = $current_bal;
    $masterTransaction->CLOSING_TOT_BALANCE    = $closing_bal;
    $masterTransaction->PARTNER_ID             = $partner_id;
    return $masterTransaction->save();
    // $queriesss = DB::getQueryLog();
    // dd($queriesss);
  }
  public  function getApproveRejectTransactionFromMaster($userID, $transTypeId, $transStatusId, $sdate, $edate)
  {
    # code...
    return $res = MasterTransactionHistory::select(
      'TRANSACTION_AMOUNT'
    )
      ->where(['USER_ID' => $userID, 'TRANSACTION_TYPE_ID' => $transTypeId, 'TRANSACTION_STATUS_ID' => $transStatusId])
      ->whereBetween('TRANSACTION_DATE', [$sdate, $edate])
      ->sum('TRANSACTION_AMOUNT');
  }


  public function getBonusOnTds($userID, $tdsAmount)
  {

    $currnet_month = date('m');
    if ($currnet_month > 3) {
      $sdate = date("Y") . "-04-01 00:00:00";
      $edate = date('Y', strtotime('+1 year')) . "-03-31 23:59:59";
    } else {
      $sdate = date('Y', strtotime('-1 year')) . "-03-31 23:59:59";
      $edate = date("Y") . "-04-01 00:00:00";
    }
    $finalTdsAmount = 0;
    $totalTdsApprovedTillDate = $this->getApproveRejectTransactionFromMaster($userID, 106, 208, $sdate, $edate);
    // $totalTdsRejectedTillDate = $this->getApproveRejectTransactionFromMaster($userID, 107, 208, $sdate, $edate);
    $totalTdsTillDate = $totalTdsApprovedTillDate;
    if ($totalTdsTillDate < 0) {
      $totalTdsTillDate = 0;
    }
    if ($totalTdsTillDate >= env('TDS_APPROVE_BONUS_LIMIT')) {
      $finalTdsAmount = 0;
    } else if ($tdsAmount <= (env('TDS_APPROVE_BONUS_LIMIT') - $totalTdsTillDate)) {

      $finalTdsAmount = $tdsAmount;
    } else {
      $finalTdsAmount = (env('TDS_APPROVE_BONUS_LIMIT') - $totalTdsTillDate);
    }
    return $finalTdsAmount;
  }

  public function getUserNameFromUserId($user_name)
  {
    return $user_id = User::select('USER_ID')->where('USERNAME', $user_name)->first()->USER_ID;
  }

  public function insertAdminActivity($data, $action)
  {

    $activity = [
      'admin_id' => \Auth::user()->id ?? null,
      'module_id' => '60',
      'action' => $action,
      'data' => json_encode($data)
    ];
    \PokerBaazi::storeActivity($activity);
  }

  public function withdrawExcelExport()
  {

    $username = Session::get('username');
    $ref_no =  Session::get('ref_no');
    $withdrawStatus =  Session::get('withdrawStatus');
    $transferId = Session::get('transferId');
    $withdrawType = Session::get('withdrawType');
    $transType = Session::get('approveType');
    $amount   = Session::get('amount');
    $dateFrom = Session::get('dateFrom');
    $dateTo   = Session::get('dateTo');
    $appDateFrom = Session::get('app_date_from');
    $appDateTo = Session::get('app_date_to');
    $lastUpdateFrom = Session::get('last_update_from');
    $lastUpdateTo = Session::get('last_update_to');
    $tds_Fee = Session::get('tds_Fee');
    $first_withdraw = Session::get('first_withdraw');
    $userFlag = Session::get('userFlag');
    $check_pass = Session::get('check_pass');
    $search_by = Session::get('search_by');

    // $user_id = null;
    // if (isset($username)) {
    //   $user_id = $this->getUserIDFromUsername($username, $search_by);
    // }
    $withDrawTransExpoData = $this->getAllWithdrawlData($username, $search_by, $ref_no, $amount, $dateFrom, $dateTo, $withdrawStatus, $transferId, $withdrawType, $transType, $tds_Fee, $first_withdraw, $userFlag, $check_pass, $appDateFrom, $appDateTo, $lastUpdateFrom, $lastUpdateTo);
    $withDrawTransExpoData = $withDrawTransExpoData->get();
    // $headingofsheet = collect($payTransExpoDataP->first())->keys()->toArray();
    foreach ($withDrawTransExpoData as $withDrawTransExpoData) {
      $userAccountDetails = $this->getUserAccountDetails($withDrawTransExpoData->USER_ID);
      $data_array[] =
        array(
          'USERNAME' => $withDrawTransExpoData->USERNAME ?? "",
          'EMAIL' => $withDrawTransExpoData->EMAIL_ID ?? "",
          'CONTACT' => $withDrawTransExpoData->CONTACT ?? "",
          'VIA' =>  $withDrawTransExpoData->WITHDRAW_TYPE = 2 ? 'Online' : 'Cheque',
          'REQ_AMOUNT' => $withDrawTransExpoData->WITHDRAW_AMOUNT ?? "",
          'TDS' => $withDrawTransExpoData->WITHDRAW_TDS ?? "",
          'FEE' => $withDrawTransExpoData->WITHDRAW_FEE ?? "",
          'TYPE' => $withDrawTransExpoData->APPROVE_TYPE ?? "",
          'STATUS' => $withDrawTransExpoData->TRANSACTION_STATUS_DESCRIPTION ?? "",
          'TO_BE_PAID' => $withDrawTransExpoData->PAID_AMOUNT ?? "",
          'REF_NO' => $withDrawTransExpoData->INTERNAL_REFERENCE_NO ?? "",
          'DATE' => $withDrawTransExpoData->TRANSACTION_DATE ?? "",
          'ACCOUNT_HOLDER_NAME' => $userAccountDetails->ACCOUNT_HOLDER_NAME ?? "",
          'ACCOUNT_NO' => $userAccountDetails->ACCOUNT_NUMBER ?? "",
          'IFSC_CODE' => $userAccountDetails->IFSC_CODE ?? "",
          'BANK_NAME' => $userAccountDetails->BANK_NAME ?? "",
          'BRANCH_NAME' => $userAccountDetails->BRANCH_NAME ?? "",
        );
    }
    // echo "<pre>";
    // print_r($data_array); exit;
    $headingofsheetArray = array('USERNAME', 'EMAIL', 'CONTACT', 'VIA', 'REQ_AMOUNT', 'TDS', 'FEE', 'TYPE', 'STATUS', 'TO_BE_PAID', 'REF_NO', 'DATE', 'ACCOUNT_HOLDER_NAME', 'ACCOUNT_NO', 'IFSC_CODE', 'BANK_NAME', 'BRANCH_NAME');
    $fileName = urlencode("Withdraw_report" . date('d-m-Y') . ".xlsx");
    if (!empty($data_array)) {
      return Excel::download(new CollectionExport($data_array, $headingofsheetArray), $fileName);
    } else {
      return view('bo.views.reports.withdraw');
    }
  }
  public function userFlagUpdate(Request $request)
  {
    if ($request->isMethod('post')) {
      $userId = $request->user_id;
      $userFlag = $request->flag_status;
      $res = $this->updateUserFlag($userId, $userFlag);
      if ($res) {
        echo $userFlag;
        exit;
      } else {
        echo "failed";
        exit;
      }
    }
  }

  public function payuCashoutApi($casOutPaymentUserData)
  {

    $token = $this->getPayuTokenForPaymentApi();
    if (!empty($token)) {
      $autorization = 'Bearer ' . $token;
      $PAYU_CASHOUT_URL = env('PAYU_CASHOUT_URL');
      $PAYU_CASHOUT_MERCHANT_ID = env('PAYU_CASHOUT_MERCHANT_ID');
      $PAYU_CASHOUT_ENDPOINT = "/payment";
      $PAYU_URL = $PAYU_CASHOUT_URL . $PAYU_CASHOUT_ENDPOINT;
      $ch = curl_init($PAYU_URL);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $casOutPaymentUserData);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "authorization:$autorization",
        "payoutMerchantId:$PAYU_CASHOUT_MERCHANT_ID",
        "Content-Type: application/json",
      ));
      $response = curl_exec($ch);
      $err = curl_error($ch);
      curl_close($ch);
      // $resData = json_decode($response);
      return $response;
    }
  }

  public function getPayuTokenForPaymentApi()
  {
    // $PAYU_CURL_URL = config('poker_config.payment.PAYU.payu_stat_url');
    $PAYU_CASHOUT_GET_TOKEN_URL = env('PAYU_CASHOUT_GET_TOKEN_URL');
    $PAYU_CASHOUT_CLIENT_ID = env('PAYU_CASHOUT_CLIENT_ID');
    $PAYU_CASHOUT_USERNAME = env('PAYU_CASHOUT_USERNAME');
    $PAYU_CASHOUT_PASSWORD = env('PAYU_CASHOUT_PASSWORD');
    $postFields = [
      'grant_type' => 'password',
      'scope' => 'create_payout_transactions',
      'client_id' => $PAYU_CASHOUT_CLIENT_ID,
      'username' => $PAYU_CASHOUT_USERNAME,
      'password' => $PAYU_CASHOUT_PASSWORD

    ];
    $headers = ['cache-control' => 'no-cache', 'Content-Type' => 'application/x-www-form-urlencoded'];


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $PAYU_CASHOUT_GET_TOKEN_URL);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);
    $resData = json_decode($response);
    return $resData->access_token;
  }
  //check if trnsaction already exist in payu and cashfree table
  public function checkCashoutTransactionBeforeApprove($referenceNo)
  {
    //$checkRes = $this->getPaymentDetailsFromPaymentTransaction($referenceNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);

    //if (!empty($checkRes->PAYMENT_TRANSACTION_STATUS)) {
    $paymentTransactionStatus = $checkRes->PAYMENT_TRANSACTION_STATUS;
    $query = CashoutApproveTransaction::query();
    $query->from(app(CashoutApproveTransaction::class)->getTable() . " as cat");
    $query->where('cat.INTERNAL_REFERENCE_NO', $referenceNo);
    // $query->wherein('pcat.PAYMENT_STATUS', $paymentTransactionStatus);
    $query->orderBy('cat.CASHOUT_TRANSACTTION_ID', 'DESC');
    $PayuTransaction = $query->select(
      'cat.USER_ID',
      'cat.GATEWAY_STATUS',
      'cat.INTERNAL_REFERENCE_NO',
      'cat.TRANSACTION_AMOUNT',
      'cat.ACCOUNT_HOLDER_NAME',
      'cat.ACCOUNT_NUMBER',
      'cat.IFSC_CODE',
      'cat.GATEWAY_REFERENCE_NO'
    );
    return $PayuTransaction->first();
    //}
  }


  //insert in payu_cashout_approve_transaction table after approve
  public function insertCashoutApproveTransaction($user_id, $refNo, $transAmount, $gateWayRefNo, $gateWayStatus, $accountName, $accountNumber, $IfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $gateWayMessage, $gateWayType, $responseDate)
  {
    // DB::enableQueryLog();
    $PayuApproveTransaction = new CashoutApproveTransaction();
    $PayuApproveTransaction->USER_ID                = $user_id;
    $PayuApproveTransaction->INTERNAL_REFERENCE_NO  = $refNo;
    $PayuApproveTransaction->TRANSACTION_AMOUNT     = $transAmount;
    $PayuApproveTransaction->GATEWAY_REFERENCE_NO   = $gateWayRefNo;
    $PayuApproveTransaction->GATEWAY_STATUS         = $gateWayStatus;
    $PayuApproveTransaction->ACCOUNT_HOLDER_NAME    = $accountName;
    $PayuApproveTransaction->ACCOUNT_NUMBER         = $accountNumber;
    $PayuApproveTransaction->IFSC_CODE              = $IfscCode;
    $PayuApproveTransaction->GATEWAY_REQUEST        = $gateWayjsonRequest;
    $PayuApproveTransaction->GATEWAY_RESPONSE       = $gateWayJsonResponse;
    $PayuApproveTransaction->GATEWAY_MESSAGE        = $gateWayMessage;
    $PayuApproveTransaction->TYPE                   = $gateWayType;
    $PayuApproveTransaction->REQUEST_DATE           = date('Y-m-d H:i:s');
    $PayuApproveTransaction->RESPONSE_DATE          = $responseDate;
    return $PayuApproveTransaction->save();
  }


  public function getUserAccountDetails($userId)
  {
    return $userAccountDetails = UserAccountDetails::select('ACCOUNT_HOLDER_NAME', 'ACCOUNT_NUMBER', 'IFSC_CODE', 'BANK_NAME', 'BRANCH_NAME')
      ->where('USER_ID', $userId)
      ->first();
  }

  public function genRandomRefNo($outputLen)
  {
    $outputString = "";
    $inputString = "0123456789";
    for ($i = 0; $i < $outputLen; $i++) {
      $rnum = rand(0, 9);
      $outputString = $outputString . substr($inputString, $rnum, 1);
    }
    return $outputString;
  }


  //receive payu cashout webhook response
  public function transferSuccessPayuCashoutWebhook(Request $request){
    
    if (empty($request->all())) {
      return response()->json(['status' => 'failed', 'msg' => 'EMPTY_REQUEST']);
    }

    $allRequest = $request->all();
    $event = $allRequest['event'];
    $refNo = $allRequest['merchantReferenceId'];
    $gateWayRefId = $allRequest['transferId'] ?? "";
    $msg = $allRequest['msg'];

    $gateWayJsonResponse = json_encode($allRequest);

    $payuApproveTransaction = PaymentTransactionPokercommission::where('PAYU_TRANSFER_ID', $gateWayRefId)->limit(1)->first();

    if(collect($payuApproveTransaction)->isEmpty()){// Go for withdraw
      $withdrawTransaction = $this->getPaymentDetailsFromWithdrawTransaction($refNo);
      $payuApproveTransaction = $withdrawTransaction->limit(1)->first();
      
      $refNo = $payuApproveTransaction->INTERNAL_REFERENCE_NO;
      
      $this->insertCashoutApproveTransaction($payuApproveTransaction->USER_ID, $refNo, $payuApproveTransaction->PAID_AMOUNT, $gateWayRefId, $event, '', '', '', $gateWayjsonRequest = NULL, $gateWayJsonResponse, $msg, "payu", date('Y-m-d H:i:s'));

      if (empty($payuApproveTransaction->INTERNAL_REFERENCE_NO) && $payuApproveTransaction->TYPE == "payu") {
        echo  "No records found in Cashfree Table";
        exit;
      }

      return $this->updateWithdrawCashStatusByWebhook($payuApproveTransaction, $gateWayRefId, $event, $refNo, "payu");
    }
    elseif($payuApproveTransaction->APPROVE_BY_PAYU_STATUS == '1'){// Go for commission
      
      $refNo = $payuApproveTransaction->INTERNAL_REFERENCE_NO;
      $this->insertCashoutApproveTransaction($payuApproveTransaction->USER_ID, $refNo, $payuApproveTransaction->PAYMENT_TRANSACTION_AMOUNT, $gateWayRefId, $event, '', '', '', $gateWayjsonRequest = NULL, $gateWayJsonResponse, $msg, "payu_commission", date('Y-m-d H:i:s'));

      return $this->updateWithdrawCommissionStatusByWebhook($payuApproveTransaction, $gateWayRefId, $event, $refNo, "payu");
    }
    else{
      echo  "No records found with this reference number";
      exit;
    }
    
  }
  //receive cashfree cashout webhook response
  public function transferSuccessCashfreeCashoutWebhook(Request $request)
  {
    $allRequest = $request->all();
    
      // print_r($allRequest);exit;
    $event        = $allRequest['event'];
    $transferId   = $allRequest['transferId'];
    $referenceId  = $allRequest['referenceId'] ?? "";
    $acknowledged = $allRequest['acknowledged'] ?? "";
    $utr          = $allRequest['utr'] ?? "";
    $signature    = $allRequest['signature'] ?? "";

    $gateWayJsonResponse = json_encode($allRequest);


    $withdrawRes = PaymentTransactionPokercommission::where('TRANSFER_ID', $transferId)->limit(1)->first();

    if(collect($withdrawRes)->isEmpty()){// Go for withdraw
      $withdrawTransaction = $this->getPaymentDetailsFromWithdrawTransactionByCashfreeTransId($transferId);
      $withdrawRes = $withdrawTransaction->limit(1)->first();
      
      $refNo = $withdrawRes->INTERNAL_REFERENCE_NO;
      
      $this->insertCashoutApproveTransaction($withdrawRes->USER_ID, $refNo, $withdrawRes->PAID_AMOUNT, $transferId, $event, '', '', '', NULL, $gateWayJsonResponse, $event, "cashfree", date('Y-m-d H:i:s'));

      if(empty($withdrawRes->INTERNAL_REFERENCE_NO) && ($withdrawRes->APPROVE_TYPE == "cashfree" || $withdrawRes->APPROVE_TYPE == "cashfree_greenflag")) {
        echo  "No records found in Cashfree Table";
        exit;
      }

      return $this->updateWithdrawCashStatusByWebhook($withdrawRes, $transferId, $event, $refNo, "cashfree");
    }
    elseif($withdrawRes->APPROVE_BY_CASHFREE_STATUS == '1'){// Go for commission
      
      $refNo = $withdrawRes->INTERNAL_REFERENCE_NO;
      $this->insertCashoutApproveTransaction($withdrawRes->USER_ID, $refNo, $withdrawRes->PAYMENT_TRANSACTION_AMOUNT, $transferId, $event, '', '', '', NULL, $gateWayJsonResponse, $event, "cashfree_commission", date('Y-m-d H:i:s'));

      return $this->updateWithdrawCommissionStatusByWebhook($withdrawRes, $transferId, $event, $refNo, "cashfree");
    }
    else{
      echo  "No records found with this reference number";
      exit;
    }
  }
  //payu status api
  public function getLatestPayuStatus(Request $request)
  {
    $reqAll = $request->all();
    $refNo = $reqAll['ref_no'];
    $token = $this->getPayuTokenForPaymentApi();
    if (!empty($token)) {
      $autorization = 'Bearer ' . $token;
      $PAYU_CASHOUT_URL = env('PAYU_CASHOUT_URL');
      $PAYU_CASHOUT_STATUS_END_POINT = "/payment/listTransactions";
      $PAYU_CASHOUT_STATUS_URL = $PAYU_CASHOUT_URL . $PAYU_CASHOUT_STATUS_END_POINT;
      $PAYU_CASHOUT_MERCHANT_ID = env('PAYU_CASHOUT_MERCHANT_ID');

      $post_data = "merchantRefId=$refNo";

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $PAYU_CASHOUT_STATUS_URL);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', "payoutMerchantId:$PAYU_CASHOUT_MERCHANT_ID", "authorization:$autorization"));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $payuResponse = curl_exec($ch);
      $returnData = json_decode($payuResponse);
      if ($payuResponse != "" && $returnData->status == 0) {
        return response()->json([
          'txnId' => $returnData->data->transactionDetails[0]->txnId ?? "",
          'batchId' => $returnData->data->transactionDetails[0]->batchId ?? "",
          'merchantRefId' => $returnData->data->transactionDetails[0]->merchantRefId ?? "",
          'amount' => $returnData->data->transactionDetails[0]->amount ?? "",
          'txnStatus' => $returnData->data->transactionDetails[0]->txnStatus ?? "",
          'txnDate' => $returnData->data->transactionDetails[0]->txnDate ?? "",
          'payuTransactionRefNo' => $returnData->data->transactionDetails[0]->payuTransactionRefNo ?? "",
          'beneficiaryName' => $returnData->data->transactionDetails[0]->beneficiaryName ?? "",
          'msg' => $returnData->data->transactionDetails[0]->msg ?? "",
          'transferType' => $returnData->data->transactionDetails[0]->transferType ?? "",
          'nameWithBank' => $returnData->data->transactionDetails[0]->nameWithBank ?? "",
        ]);
      } else {
        return NULL;
      }
    }
  }

  //cashfree status api
  public function getLatestCashfreeStatus(Request $request)
  {

    $reqAll = $request->all();
    $refNo = $reqAll['ref_no'];
    $WithTransRes = $this->getPaymentDetailsFromWithdrawTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);
    $cashfreeRes = $WithTransRes->first();
    $CASHFREE_PAYOUT_BALANCE_CLIENT_ID = env('CASHFREE_PAYOUT_CLIENT_ID');
    $CASHFREE_PAYOUT_BALANCE_SECRET = env('CASHFREE_PAYOUT_CLIENT_SECRET');
    $CASHFREE_PAYOUT_STATUS_URL = env('CASHFREE_PAYOUT_PAYMENT_URL');
    $endpointAuth = $CASHFREE_PAYOUT_STATUS_URL . "/authorize";
    $headers = array(
      "X-Client-Id: $CASHFREE_PAYOUT_BALANCE_CLIENT_ID",
      "X-Client-Secret: $CASHFREE_PAYOUT_BALANCE_SECRET"
    );
    $cashfreeGetToken = $this->cashfreePayOutBatchTransferApi($endpointAuth, $headers, $params = []); //get cashfree token
    if ($cashfreeGetToken["status"] == "SUCCESS") { //authendication success
      $accessToken = $cashfreeGetToken["data"]["token"];
      $endpointAuth = $CASHFREE_PAYOUT_STATUS_URL . "/getTransferStatus?transferId=$cashfreeRes->GATEWAY_REFERENCE_NO";
      $accessTokenheaders = array("Authorization: Bearer $accessToken");

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $endpointAuth);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $accessTokenheaders);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $returnData = curl_exec($ch);
      $err = curl_error($ch);
      curl_close($ch);
      $cashFreeResponse = json_decode($returnData);
      // print_r($returnData);exit;
      if (!empty($cashFreeResponse) && $cashFreeResponse->status = "SUCCESS") {

        return response()->json([
          // 'status' => $cashFreeResponse->status,
          'subCode' => $cashFreeResponse->subCode ?? "Sub Code not received",
          'message' => $cashFreeResponse->message ?? "Message not received",
          'bankAccount' => $cashFreeResponse->data->transfer->bankAccount ?? "Bank Account not received",
          'ifsc' => $cashFreeResponse->data->transfer->ifsc ?? "Ifsc not received",
          'beneId' => $cashFreeResponse->data->transfer->beneId ?? "Ben id not received",
          'transferId' => $cashFreeResponse->data->transfer->transferId ?? "Transfer id not received",
          'amount' => $cashFreeResponse->data->transfer->amount ?? "Amount not received",
          'status' => $cashFreeResponse->data->transfer->status ?? "Status not received",
          'utr' => $cashFreeResponse->data->transfer->utr ?? "Transfer utr not received",
          'addedOn' => $cashFreeResponse->data->transfer->addedOn ?? "Add on not received",
          'processedOn' => $cashFreeResponse->data->transfer->processedOn ?? "Transfer process on received",
          'acknowledged' => $acknowledged ?? "Webhook response not received",
          'transferMode' => $cashFreeResponse->data->transfer->transferMode ?? "Transfer Mode not received",
        ]);
      } else {
        $cashoutRes = $this->getDataFromCashoutTransaction($refNo);

        $gatewayResponse = $cashoutRes->GATEWAY_RESPONSE;
        $responsData = json_decode($gatewayResponse);
        // echo  "<pre>";
        // print_r($responsData->msg);exit;
        $msg = !empty($responsData->reason) ? $responsData->reason : (!empty($responsData->event) ? $responsData->event : (!empty($responsData->message) ? $responsData->message : $responsData->msg));
        $msg = (!empty($msg) ? $msg : $gatewayResponse);
        // print_r($msg) ;exit;
        return response()->json([
          // 'status' => $cashFreeResponse->status,
          'subCode' => "Sub Code not received",
          'message' => $msg ?? "Not Received",
          'bankAccount' => $cashoutRes->ACCOUNT_NUMBER ?? "Bank Account not received",
          'ifsc' => $cashoutRes->IFSC_CODE ?? "Ifsc not received",
          'beneId' => $cashoutRes->USER_ID ?? "Ben id not received",
          'transferId' => $cashfreeRes->GATEWAY_REFERENCE_NO ?? "not received",
          'amount' => $cashoutRes->TRANSACTION_AMOUNT ?? "Amount not received",
          'status' => $cashoutRes->GATEWAY_STATUS ?? "Not received",
          'utr' =>  "",
          'addedOn' => "",
          'processedOn' => $cashoutRes->REQUEST_DATE ?? "Transfer process on received",
          'acknowledged' => "",
          'transferMode' => "",
        ]);
      }
    }
  }

  //payu balance check api
  public function getPayuMerchantAvailableBalance()
  {
    $token = $this->getPayuTokenForPaymentApi();
    if (!empty($token)) {
      $autorization = 'Bearer ' . $token;
      $PAYU_CASHOUT_URL = env('PAYU_CASHOUT_URL');
      $PAYU_CASHOUT_BALANCE_END_POINT = "/merchant/getAccountDetail";
      $PAYU_CASHOUT_STATUS_URL = $PAYU_CASHOUT_URL . $PAYU_CASHOUT_BALANCE_END_POINT;
      $PAYU_CASHOUT_MERCHANT_ID = env('PAYU_CASHOUT_MERCHANT_ID');
      $headers = array(
        "authorization:$autorization",
        "cache-control: no-cache'",
        "payoutMerchantId:$PAYU_CASHOUT_MERCHANT_ID"
      );
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $PAYU_CASHOUT_STATUS_URL);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $returnData = curl_exec($ch);
      $accountDetails = json_decode($returnData);
      return $accountDetails->data->balance;
    }
  }

  //get cashfree CASHFREE_REFERENCE_ID from cashfree table
  public function getCashfreeRefId($referenceNo)
  {
    $res = CashoutApproveTransaction::select(
      'CASHFREE_REFERENCE_ID',
      'INTERNAL_REFERENCE_NO',
      'USER_ID'
    );
    $res->where(['INTERNAL_REFERENCE_NO' => $referenceNo]);
    return $res->first();
  }
  // Function to update user flag green,yellow
  public function updateUserFlag($userId, $userFlag)
  {
    return $updateUserFlag = User::where(['USER_ID' => $userId,])
      ->update(['user_flag' => $userFlag]);
  }

  //get user win balance
  public function getUserWin(Request $request)
  {
    $reqAll = $request->all();
    $user_id_array = $reqAll['user_id'];
    foreach ($user_id_array as $user_id) {
      $userTotalDeposit = $this->getUserTotalDeposit($user_id);
      $userTotalWithdraw = $this->getUserTotalWithdraw($user_id);
      $totalWin = $userTotalDeposit - $userTotalWithdraw;
      $result[] = ['user_id' => $user_id, 'winAmount' => $totalWin];
    }
    return response()->json($result);
  }

  //get user private table status
  public function privateTableStatus(Request $request)
  {
    $reqAll = $request->all();
    $user_id_array = $reqAll['user_id'];
    $privateTableCount = $this->checkUserPrivateTableStatus($user_id_array);
    return response()->json($privateTableCount);
  }

  //get user first withdraw status
  public function getUserFirstWithdrawStatus(Request $request)
  {

    $reqAll = $request->all();
    $user_id_array = $reqAll['user_id'];
    foreach ($user_id_array as $user_id) {
      $statusCount = $this->checkUserFirstWithdrawStatus($user_id);
      if ($statusCount > 0) {
        $status = "No";
      } else {
        $status = "Yes";
      }
      $result[] = ['user_id' => $user_id, 'withStatus' => $status];
    }
    return response()->json($result);
  }

  public function checkUserFirstWithdrawStatus($user_id)
  {
    $now = date('Y-m-d H:i:s');
    $queryPayment = PaymentTransaction::query();
    $queryPayment->from(app(PaymentTransaction::class)->getTable() . " as pt");
    $queryPayment->join('user as u', 'u.USER_ID', '=', 'pt.USER_ID');
    $queryPayment->where('pt.USER_ID', $user_id);
    $queryPayment->where('pt.PAYMENT_TRANSACTION_STATUS', 208);
    $queryPayment->where('pt.TRANSACTION_TYPE_ID', 10);
    $queryPayment->whereBetween('pt.PAYMENT_TRANSACTION_CREATED_ON', ['u.REGISTRATION_TIMESTAMP', $now]);
    $queryPayment->orderBy('pt.PAYMENT_TRANSACTION_CREATED_ON', 'DESC')->select('u.USER_ID', 'u.REGISTRATION_TIMESTAMP')
      ->select('u.USER_ID', 'u.REGISTRATION_TIMESTAMP');
    $query = WithdrawTransactionHistory::query();
    $query->from(app(WithdrawTransactionHistory::class)->getTable() . " as wth");
    $query->join('user as u', 'u.USER_ID', '=', 'wth.USER_ID');
    $query->where('wth.USER_ID', $user_id);
    $query->where('wth.TRANSACTION_STATUS_ID', 208);
    $query->where('wth.TRANSACTION_TYPE_ID', 10);
    $query->whereBetween('wth.TRANSACTION_DATE', ['u.REGISTRATION_TIMESTAMP', $now]);
    $query->orderBy('wth.TRANSACTION_DATE', 'DESC');
    $query->union($queryPayment);
    $payTransData = $query->select('u.USER_ID', 'u.REGISTRATION_TIMESTAMP');
    return $statusCount = $payTransData->count();
  }

  public function reCalculateTdsAndUpdateWhileApprove($userID, $withdrawAmount, $withdrawCurrentTds, $withdrawFee, $internalRefNo, $feeWaived, $withdrwaType, $approvedAt, $gateWay, $gatewayStatus, $transactionStatusID)
  {

    $approvedBy = \Auth::user()->username;
    $coinTypeID = 1; // Real Cash
    $tdsPercentage = 0.30;
    $previousLedgerResult = $this->getPreviousPlayerLedgerData($userID); // Check if TDS is applicable - Get previous player ledger data

    if (!empty($userID) && !empty($previousLedgerResult)) {
      $eligibleWithoutTax = $previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX;
      $tax = ($withdrawAmount > $eligibleWithoutTax ? ($tdsPercentage * ($withdrawAmount - $eligibleWithoutTax)) : 0);
      $withdrawTds = $tax;
      $payableAmount = $withdrawAmount - ($withdrawTds + $withdrawFee);
      $paidAmount = ($feeWaived == "true" ? $payableAmount + $withdrawFee : $payableAmount);
      $feeWaived = ($feeWaived == "true" ? 1 : 0);
      $ledger = $this->updatePlayerLedger($internalRefNo, "WITHDRAWAL");
      $wittrans = $this->updateWithdrawTransactionTable($transactionStatusID, $internalRefNo, $userID, $withdrawTds, $withdrawFee, $payableAmount, $feeWaived, $paidAmount, $approvedBy, $gateWay, $gatewayStatus);

      return $wittrans;
    } else {
      // Case: if there was no previous ledger details. This should not be possible, because no player is allowed to withdraw without depositing and if he has deposited then there should be an entry for him in player_ledger table
      return FALSE;
    }
  }


  //insert in withdraw transaction table
  public function insertWithdrawTransTable($userID, $balanceTypeID, $transactionStatusID, $transactionTypeID, $withdrawAmount, $withdrawTds, $withdrawFee, $payableAmount, $feeWaived, $paidAmount, $internalRefNo, $withdrwaType, $approvedAt, $approvedBy, $approveType, $gatewayStatus)
  {

    $currentDate = date('Y-m-d H:i:s');
    // DB::enableQueryLog();
    $masterTransaction = new WithdrawTransactionHistory();
    $masterTransaction->USER_ID                = $userID;
    $masterTransaction->BALANCE_TYPE_ID        = $balanceTypeID;
    $masterTransaction->TRANSACTION_STATUS_ID  = $transactionStatusID;
    $masterTransaction->TRANSACTION_TYPE_ID    = $transactionTypeID;
    $masterTransaction->WITHDRAW_AMOUNT        = $withdrawAmount;
    $masterTransaction->WITHDRAW_TDS           = $withdrawTds;
    $masterTransaction->WITHDRAW_FEE           = $withdrawFee;
    $masterTransaction->PAYABLE_AMOUNT         = $payableAmount;
    $masterTransaction->FEE_WAIVED             = $feeWaived;
    $masterTransaction->PAID_AMOUNT            = $paidAmount;
    $masterTransaction->INTERNAL_REFERENCE_NO  = $internalRefNo;
    $masterTransaction->WITHDRAW_TYPE          = $withdrwaType;
    $masterTransaction->TRANSACTION_DATE       = $currentDate;
    $masterTransaction->APPROVED_AT            = $approvedAt;
    $masterTransaction->APPROVED_BY            = $approvedBy;
    $masterTransaction->APPROVE_TYPE           = $approveType;
    $masterTransaction->GATEWAY_STATUS         = $gatewayStatus;
    return $masterTransaction->save();
  }

  // update withdraw transaction table after approve

  public function updateWithdrawTransactionTable($transStartus, $refNo, $userId, $tds, $fee, $payableAmount, $feeWaived, $paidAmount, $approvedBy, $gateWay, $gatewayStatus)
  {
    return $updatePaymentStatus = WithdrawTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo, 'USER_ID' => $userId])
      ->update([
        'TRANSACTION_STATUS_ID' => $transStartus,
        'WITHDRAW_TDS' => $tds,
        'WITHDRAW_FEE' => $fee,
        'PAYABLE_AMOUNT' => $payableAmount,
        'FEE_WAIVED' => $feeWaived,
        'PAID_AMOUNT' => $paidAmount,
        'APPROVE_DATE' => date('Y-m-d H:i:s'),
        'APPROVED_BY' => $approvedBy,
        'APPROVE_TYPE' => $gateWay,
        'GATEWAY_STATUS' => $gatewayStatus,
      ]);
  }

  public function getPayuStausForManuallyHitWebhoo($refNo)
  {

    $token = $this->getPayuTokenForPaymentApi();
    if (!empty($token)) {
      $autorization = 'Bearer ' . $token;
      $PAYU_CASHOUT_URL = env('PAYU_CASHOUT_URL');
      $PAYU_CASHOUT_STATUS_END_POINT = "/payment/listTransactions";
      $PAYU_CASHOUT_STATUS_URL = $PAYU_CASHOUT_URL . $PAYU_CASHOUT_STATUS_END_POINT;
      $PAYU_CASHOUT_MERCHANT_ID = env('PAYU_CASHOUT_MERCHANT_ID');

      $post_data = "merchantRefId=$refNo";

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $PAYU_CASHOUT_STATUS_URL);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', "payoutMerchantId:$PAYU_CASHOUT_MERCHANT_ID", "authorization:$autorization"));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $payuResponse = curl_exec($ch);
      return $returnData = json_decode($payuResponse);
    }
  }
  public function getAllPayuInitiatedTransaction($approveType)
  {
    $now =  date('Y-m-d H:i:s');
    $time   = strtotime($now);
    $time   = $time - (60 * 60); //one hour
    $beforeOneHour = date("Y-m-d H:i:s", $time);
    $beforeOneWeek = date("Y-m-d H:i:s", strtotime("-1 week"));

    $manualWebhookLimit = env('MANUAL_WEBHOOK_HIT_LIMI');
    return $iniRefNo = WithdrawTransactionHistory::select('INTERNAL_REFERENCE_NO', 'USER_ID', 'APPROVE_TYPE', 'TRANSACTION_STATUS_ID', 'GATEWAY_REFERENCE_NO', 'PAID_AMOUNT')
      ->where('TRANSACTION_STATUS_ID', 254)
      ->whereIn('APPROVE_TYPE', $approveType)
      ->whereBetween('TRANSACTION_DATE', [$beforeOneWeek, $beforeOneHour])
      ->limit($manualWebhookLimit)
      ->orderBy('TRANSACTION_DATE', 'DESC')
      ->get();
  }
  public function payuManuallyWebhookCall()
  {

    $approveType = ["payu"];
    $allInitiatedRefNo = $this->getAllPayuInitiatedTransaction($approveType);
    // echo "<pre>";
    // print_r($allInitiatedRefNo);exit;
    if (!empty($allInitiatedRefNo)) {
      foreach ($allInitiatedRefNo as $refNoData) {
        $refNo = $refNoData->INTERNAL_REFERENCE_NO;
        $payuStatusRes = $this->getPayuStausForManuallyHitWebhoo($refNo);

        if (!empty($payuStatusRes->data->transactionDetails[0]->merchantRefId)) {
          $refNoFromPayu = $payuStatusRes->data->transactionDetails[0]->merchantRefId;
          $payuStatus = $payuStatusRes->data->transactionDetails[0]->txnStatus;
          $txnId = $payuStatusRes->data->transactionDetails[0]->txnId ?? "";
          $amount = $payuStatusRes->data->transactionDetails[0]->amount ?? "";
          $txnDate = $payuStatusRes->data->transactionDetails[0]->txnDate ?? "";
          $payuTransactionRefNo = $payuStatusRes->data->transactionDetails[0]->payuTransactionRefNo ?? "";
          $beneficiaryName = $payuStatusRes->data->transactionDetails[0]->beneficiaryName ?? "";
          $msg = $payuStatusRes->data->transactionDetails[0]->msg ?? "";

          $gateWayJsonResponse = json_encode($payuStatusRes);
          $this->insertCashoutApproveTransaction($refNoData->USER_ID, $refNo, $amount, $payuTransactionRefNo, $msg, '', '', '', $gateWayjsonRequest = NULL, $gateWayJsonResponse, $msg, "payu", date('Y-m-d H:i:s'));

          if ($payuStatus == "SUCCESS" && $refNoData->TRANSACTION_STATUS_ID == 254) { //payu cashout success

            //payu success
            $currentStatusId = $refNoData->TRANSACTION_STATUS_ID; //current status id as payment initiated
            //update PaymentTransaction status after getting success response fro payu
            $paymentAndMsterAsSuccessStatus = $this->updateMasterAndPaymentStatusForSuccess($refNo, $currentStatusId); // for payu approve change status from pending to success and calculate tds and credit tds bonus

            if ($paymentAndMsterAsSuccessStatus) {
              $updateWithdrawStatus = WithdrawTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
                ->update(['TRANSACTION_STATUS_ID' => 208, 'GATEWAY_REFERENCE_NO' => $payuTransactionRefNo, 'APPROVED_AT' => 'manually_webhoo']);
            }

            if (!empty($updateWithdrawStatus)) {
              $refNoArray[] = $refNo;
              $status = 'success';
              $msg = 'TRANSFER_SUCCESS';
              $code = 6000;
              // return response()->json(['status' => 'success', 'msg' => 'TRANSFER_SUCCESS', 'code' => 6000, 'refNoArray' => $refNoArray]);
            }
          }
          if (($payuStatus == "FAILED" || $payuStatus == "REQUEST_PROCESSING_FAILED") && $refNoData->TRANSACTION_STATUS_ID == 254) {   //payu cashout failed
            //payu failed
            //$error = $allRequest['errorCode'];
            //update PaymentTransaction status after getting success response fro payu
            $updatePaymentStatus = PaymentTransaction::where(['INTERNAL_REFERENCE_NO' => $refNo])
              ->update(['PAYMENT_TRANSACTION_STATUS' => 216]);
            //update PaymentTransaction status after getting success response fro payu
            if ($updatePaymentStatus) {
              $updateMsterStatus = MasterTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
                ->update(['TRANSACTION_STATUS_ID' => 216]);
            }
            // as wll update tds value if transaction contais tds
            if ($updateMsterStatus) {
              $updateWithdrawStatus = WithdrawTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
                ->update(['TRANSACTION_STATUS_ID' => 216, 'GATEWAY_REFERENCE_NO' => $payuTransactionRefNo, 'APPROVED_AT' => 'manually_webhoo']);
            }
            $refNoArray[] = $refNo;
            $status = 'success';
            $msg = 'TRANSFER_FAILED_SUCCESS';
            $code = 6001;

            // return response()->json(['status' => 'success', 'msg' => 'TRANSFER_FAILED_SUCCESS', 'code' => 6001, 'refNoArray' => $refNoArray]);
          }
          if ($payuStatus == "TRANSFER_REVERSED" && $refNoData->TRANSACTION_STATUS_ID == 208) {
            $updatePaymentTrans = $this->normalWithdrawRejected($refNo, $refNoData->USER_ID);
            // return response()->json(['status' => 'success', 'msg' => 'TRANSFER_REVERSED_SUCCESS', 'code' => 6002, 'refNoArray' => $refNo]);
          } else {
            // return response()->json(['status' => 'failed', 'msg' => 'FAILED', 'code' => 6003, 'refNoArray' => $refNo]);
          }
        }
      }

      // return response()->json(['msg' => $msg, 'refNoArray' => $refNoArray]);
    }
  }
  public function getCashfreeStausForManuallyHitWebhoo($refNo, $cashfreeRefId)
  {
    $CASHFREE_PAYOUT_BALANCE_CLIENT_ID = env('CASHFREE_PAYOUT_CLIENT_ID');
    $CASHFREE_PAYOUT_BALANCE_SECRET = env('CASHFREE_PAYOUT_CLIENT_SECRET');
    $CASHFREE_PAYOUT_STATUS_URL = env('CASHFREE_PAYOUT_PAYMENT_URL');
    $endpointAuth = $CASHFREE_PAYOUT_STATUS_URL . "/authorize";
    $headers = array(
      "X-Client-Id: $CASHFREE_PAYOUT_BALANCE_CLIENT_ID",
      "X-Client-Secret: $CASHFREE_PAYOUT_BALANCE_SECRET"
    );
    $cashfreeGetToken = $this->cashfreePayOutBatchTransferApi($endpointAuth, $headers, $params = []); //get cashfree token
    if ($cashfreeGetToken["status"] == "SUCCESS") { //authendication success
      $accessToken = $cashfreeGetToken["data"]["token"];

      $endpointAuth = $CASHFREE_PAYOUT_STATUS_URL . "/getTransferStatus?transferId=$cashfreeRefId";
      $accessTokenheaders = array("Authorization: Bearer $accessToken");

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $endpointAuth);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $accessTokenheaders);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $returnData = curl_exec($ch);
      $err = curl_error($ch);
      curl_close($ch);
      return $cashFreeResponse = json_decode($returnData);
    }
  }
  public function cashfreeManuallyWebhookCall()
  {
    //$approveType = "cashfree";
    $approveType = ["cashfree", "cashfree_greenflag"];
    $allInitiatedRefNo = $this->getAllPayuInitiatedTransaction($approveType);
    // dump($allInitiatedRefNo);

    if (!empty($allInitiatedRefNo)) {
      foreach ($allInitiatedRefNo as $refNoData) {
        echo  $refNo = $refNoData->INTERNAL_REFERENCE_NO;
        if ($refNoData->APPROVE_TYPE == "cashfree_greenflag")
          $cashfreeRefId = $refNoData->INTERNAL_REFERENCE_NO;
        else
          $cashfreeRefId = $refNoData->GATEWAY_REFERENCE_NO;
        // $refNo = "13115997450200901122457854664832";
        // $cashfreeRefId = "144472";

        // $cashfreeRefId=GATEWAY_REFERENCE_NO
        $cashfreeStatus = $this->getCashfreeStausForManuallyHitWebhoo($refNo, $cashfreeRefId);


        if (!empty($cashfreeStatus->data->transfer->status)) {
          $gateWayJsonResponse = json_encode($cashfreeStatus);
          $event = $cashfreeStatus->data->transfer->status;
          $msg = $cashfreeStatus->message ?? "Message not received";
          // echo $event."--".$refNoData->TRANSACTION_STATUS_ID ;exit;
          $this->insertCashoutApproveTransaction($refNoData->USER_ID, $refNo, $refNoData->PAID_AMOUNT, $cashfreeRefId, $event, '', '', '', NULL, $gateWayJsonResponse, $event, "cashfree", date('Y-m-d H:i:s'));
          if ($event == "SUCCESS" && $refNoData->TRANSACTION_STATUS_ID == 254) { //cashfree cashout success

            //cashfree success
            $currentStatusId = 254;
            //update PaymentTransaction status after getting success response fro cashfree
            // as wll update tds value if transaction contais tds
            //For TDS Calculation
            $paymentAndMsterAsSuccessStatus = $this->updateMasterAndPaymentStatusForSuccess($refNo, $currentStatusId); // for cashfree approve change status from pending to success and calculate tds and credit tds bonus
            //update withdraw transaction history status after getting success response from cashfree
            $updateWithdrawStatus = WithdrawTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
              ->update(['TRANSACTION_STATUS_ID' => 208, 'GATEWAY_REFERENCE_NO' => $cashfreeRefId, 'APPROVED_AT' => 'manually_webhoo']);
            // return response()->json(['status' => 'success', 'msg' => 'TRANSFER_SUCCESS', 'code' => 6000, 'refNoArray' => $refNo]);
          }
          if ($event == "FAILED" && $refNoData->TRANSACTION_STATUS_ID == 254) {   //cashfree cashout failed
            //update PaymentTransaction status after getting success response fro cashfree
            $updateCashfreeStatusInPayment = PaymentTransaction::where(['INTERNAL_REFERENCE_NO' => $refNo])
              ->update(['PAYMENT_TRANSACTION_STATUS' => 216]);
            //update PaymentTransaction status after getting success response fro cashfree
            $updateCashfreeStatusInMaster = MasterTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
              ->update(['TRANSACTION_STATUS_ID' => 216]);
            //update withdraw transaction history status after getting success response fro cashfree
            $updateWithdrawStatus = WithdrawTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
              ->update(['TRANSACTION_STATUS_ID' => 216, 'GATEWAY_REFERENCE_NO' => $cashfreeRefId, 'APPROVED_AT' => 'manually_webhoo']);
            // return response()->json(['status' => 'success', 'msg' => 'TRANSFER_FAILED_SUCCESS', 'code' => 6001, 'refNoArray' => $refNo]);
          }
          if ($event == "TRANSFER_REVERSED" && $refNoData->TRANSACTION_STATUS_ID == 208) {   //cashfree cashout reversed
            $updatePaymentTrans = $this->normalWithdrawRejected($refNo, $refNoData->USER_ID);
            // return response()->json(['status' => 'success', 'msg' => 'TRANSFER_REVERSED_SUCCESS', 'code' => 6002, 'refNoArray' => $refNo]);
          }
          // return response()->json(['status' => 'failed', 'msg' => 'FAILED', 'code' => 6003, 'refNoArray' => $refNo]);
        }
      }
    }
  }

  public function makeCashfreeFailedIfNotSuccess($refNo)
  {
    $updateWithdrawTran = WithdrawTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
      ->update(['TRANSACTION_STATUS_ID' => 216]);
    $updateMaster = MasterTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
      ->update(['TRANSACTION_STATUS_ID' => 216]);
    $updatePayment = PaymentTransaction::where(['INTERNAL_REFERENCE_NO' => $refNo])
      ->update(['PAYMENT_TRANSACTION_STATUS' => 216]);
  }

  public function updateCashfreeTransIdInInstantRespone($cashfreeTranId, $refNo)
  {
    $updateWithdrawTran = WithdrawTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
      ->update(['GATEWAY_REFERENCE_NO' => $cashfreeTranId]);
  }

  public function getPaymentDetailsFromWithdrawTransactionByCashfreeTransId($cashfreeTranId)
  {
    $res = WithdrawTransactionHistory::select(
      'USER_ID',
      'INTERNAL_REFERENCE_NO',
      'BALANCE_TYPE_ID',
      'TRANSACTION_STATUS_ID',
      'TRANSACTION_TYPE_ID',
      'WITHDRAW_AMOUNT',
      'WITHDRAW_TDS',
      'WITHDRAW_FEE',
      'PAYABLE_AMOUNT',
      'FEE_WAIVED',
      'PAID_AMOUNT',
      'APPROVED_BY',
      'APPROVE_TYPE',
      'GATEWAY_STATUS',
      'GATEWAY_REFERENCE_NO'
    );
    $res->where(['GATEWAY_REFERENCE_NO' => $cashfreeTranId]);
    $res->orderBy('UPDATED_ON', 'DESC');


    return $res;
    // print_r($res->getBindings());
    // print_r($res->toSql());exit;
  }

  //check if trnsaction already exist in payu and cashfree table
  public function getDataFromCashoutTransaction($referenceNo)
  {
    $query = CashoutApproveTransaction::query();
    $query->from(app(CashoutApproveTransaction::class)->getTable() . " as cat");
    $query->where('cat.INTERNAL_REFERENCE_NO', $referenceNo);
    $query->where('cat.GATEWAY_MESSAGE', '!=', 'ADTIONAL_TRACKING_ERROR');
    $query->orderBy('cat.CASHOUT_TRANSACTTION_ID', 'DESC');
    $PayuTransaction = $query->select(
      'cat.USER_ID',
      'cat.GATEWAY_STATUS',
      'cat.INTERNAL_REFERENCE_NO',
      'cat.TRANSACTION_AMOUNT',
      'cat.ACCOUNT_HOLDER_NAME',
      'cat.ACCOUNT_NUMBER',
      'cat.IFSC_CODE',
      'cat.GATEWAY_REFERENCE_NO',
      'cat.GATEWAY_RESPONSE',
      'cat.GATEWAY_RESPONSE',
      'cat.REQUEST_DATE'
    );
    return $PayuTransaction->first();
    //}
  }
  public function checkUserPrivateTableStatus($user_id_array)
  {
    $beforeOneWeek = date("Y-m-d H:i:s", strtotime("-1 week"));
    $now = date('Y-m-d H:i:s');
    $privateTable = GameTransactionHistory::query();
    $privateTable->from(app(GameTransactionHistory::class)->getTable() . " as gth");
    $privateTable->join('tournament_tables as tts', 'tts.TOURNAMENT_TABLE_ID', '=', 'gth.TOURNAMENT_TABLE_ID');
    $privateTable->join('tournament as t', 't.TOURNAMENT_ID', '=', 'tts.TOURNAMENT_ID');
    $privateTable->whereIn('gth.USER_ID', $user_id_array);
    $privateTable->where('t.PRIVATE_TABLE', 2);
    $privateTable->whereBetween('gth.STARTED', [$beforeOneWeek, $now]);
    $privateTable->groupBy('gth.USER_ID');
    $privateTable->orderBy('gth.STARTED', 'DESC')->select('gth.USER_ID', DB::raw('COUNT(gth.USER_ID) AS tableStatus'));
    // print_r($privateTable->getBindings());
    // dd($privateTable->toSql());
    return $statusCount = $privateTable->get();
  }

  public function getPaidSumWithdrawTransaction($refNoArray)
  {
    $res = WithdrawTransactionHistory::select('PAID_AMOUNT');
    $res->whereIn('INTERNAL_REFERENCE_NO', $refNoArray);
    return $res->sum('PAID_AMOUNT');
  }

  private function updateWithdrawCashStatusByWebhook(Object $withdrawRes, String $transferId, String $event, String $refNo, String $gatewayType){
          

      //insert into cashout table
      if ($event == "TRANSFER_SUCCESS" && $withdrawRes->TRANSACTION_STATUS_ID == 254) { //cashfree cashout success
        //cashfree success
        $currentStatusId = 254;
        //update PaymentTransaction status after getting success response fro cashfree
        // as wll update tds value if transaction contais tds
        //For TDS Calculation
        $paymentAndMsterAsSuccessStatus = $this->updateMasterAndPaymentStatusForSuccess($refNo, $currentStatusId); // for cashfree approve change status from pending to success and calculate tds and credit tds bonus
        //update withdraw transaction history status after getting success response from cashfree
        $updateWithdrawStatus = WithdrawTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
          ->update(['TRANSACTION_STATUS_ID' => 208, 'GATEWAY_REFERENCE_NO' => $transferId]);
        return response()->json(['status' => 'success', 'msg' => 'TRANSFER_SUCCESS', 'code' => 6000, 'refNoArray' => $refNo]);
      } else if (($event == "TRANSFER_FAILED" ||  $event == "REQUEST_PROCESSING_FAILED") && $withdrawRes->TRANSACTION_STATUS_ID == 254) {   //cashfree cashout failed
        //update PaymentTransaction status after getting success response fro cashfree
        $updateCashfreeStatusInPayment = PaymentTransaction::where(['INTERNAL_REFERENCE_NO' => $refNo])
          ->update(['PAYMENT_TRANSACTION_STATUS' => 216]);
        //update PaymentTransaction status after getting success response fro cashfree
        $updateCashfreeStatusInMaster = MasterTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
          ->update(['TRANSACTION_STATUS_ID' => 216]);
        //update withdraw transaction history status after getting success response fro cashfree
        $updateWithdrawStatus = WithdrawTransactionHistory::where(['INTERNAL_REFERENCE_NO' => $refNo])
          ->update(['TRANSACTION_STATUS_ID' => 216, 'GATEWAY_REFERENCE_NO' => $transferId]);
        return response()->json(['status' => 'success', 'msg' => 'TRANSFER_FAILED_SUCCESS', 'code' => 6001, 'refNoArray' => $refNo]);
      } else if ($event == "TRANSFER_REVERSED" && $withdrawRes->TRANSACTION_STATUS_ID == 208) {   //cashfree cashout reversed
        $updatePaymentTrans = $this->normalWithdrawRejected($refNo, $withdrawRes->USER_ID);
        return response()->json(['status' => 'success', 'msg' => 'TRANSFER_REVERSED_SUCCESS', 'code' => 6002, 'refNoArray' => $refNo]);
      }
      return response()->json(['status' => 'failed', 'msg' => 'FAILED', 'code' => 6003, 'refNoArray' => $refNo]);
  } 

  private function updateWithdrawCommissionStatusByWebhook(Object $withdrawRes, String $transferId, String $event, String $refNo, String $gatewayType){
    $currentStatusId = $withdrawRes->PAYMENT_TRANSACTION_STATUS;

    if($event == "TRANSFER_SUCCESS" && $withdrawRes->PAYMENT_TRANSACTION_STATUS == 109){
      
      MasterTransactionHistoryPokercommission::where([
        'INTERNAL_REFERENCE_NO' => $refNo,
        'TRANSACTION_STATUS_ID' => $currentStatusId
      ])
      ->update([
        'TRANSACTION_STATUS_ID' => 208
      ]);

      PaymentTransactionPokercommission::where([
        'INTERNAL_REFERENCE_NO' => $refNo,
        'PAYMENT_TRANSACTION_STATUS' => $currentStatusId
      ])
      ->update([
        'PAYMENT_TRANSACTION_STATUS' => 208
      ]);

      
      return response()->json(['status' => 'success', 'msg' => 'TRANSFER_SUCCESS', 'code' => 6000, 'refNoArray' => $refNo]);
    }
    elseif($event == "TRANSFER_FAILED" && $withdrawRes->PAYMENT_TRANSACTION_STATUS == 109){
      MasterTransactionHistoryPokercommission::where([
        'INTERNAL_REFERENCE_NO' => $refNo,
        'TRANSACTION_STATUS_ID' => $currentStatusId
      ])
      ->update([
        'TRANSACTION_STATUS_ID' => 216
      ]);

      PaymentTransactionPokercommission::where([
        'INTERNAL_REFERENCE_NO' => $refNo,
        'PAYMENT_TRANSACTION_STATUS' => $currentStatusId
      ])
      ->update([
        'PAYMENT_TRANSACTION_STATUS' => 216
      ]);

      
      return response()->json(['status' => 'success', 'msg' => 'TRANSFER_FAILED_SUCCESS', 'code' => 6001, 'refNoArray' => $refNo]);
    }
    elseif($event == "TRANSFER_REVERSED" && $withdrawRes->PAYMENT_TRANSACTION_STATUS == 208){
        /*
        * ------------------------------------------------------------------------------------
        * UPDATE PAYMENT TRANSACTION FOR REVERTED STATUS
        * ------------------------------------------------------------------------------------
        */
        PaymentTransactionPokercommission::where([
            'USER_ID' => $withdrawRes->USER_ID,
            'INTERNAL_REFERENCE_NO' => $refNo
        ])->update(['PAYMENT_TRANSACTION_STATUS' => 203]);

        $balanceTypeId = MasterTransactionHistoryPokercommission::select('BALANCE_TYPE_ID')
                            ->where('INTERNAL_REFERENCE_NO', $refNo)
                            ->limit(1)
                            ->pluck('BALANCE_TYPE_ID')
                            ->first();

                          

        $userCurrentCommissionBalance = $this->userCurrentBalance(16, $withdrawRes->USER_ID);
        
        $newTotalBalance = $userCurrentCommissionBalance->total + $withdrawRes->PAYMENT_TRANSACTION_AMOUNT;
                                  

        /*
        * ------------------------------------------------------------------------------------
        * INSERTING INTO MASTER TRANSACTION HISTORY FOR REVERT TRANSACTION
        * ------------------------------------------------------------------------------------
        */
        $newClosingTotalBalance = ($userCurrentCommissionBalance->total + $withdrawRes->PAYMENT_TRANSACTION_AMOUNT);

        $masterTransaction = new MasterTransactionHistoryPokercommission();
        $masterTransaction->USER_ID                = $withdrawRes->USER_ID;
        $masterTransaction->BALANCE_TYPE_ID        = $balanceTypeId;
        $masterTransaction->TRANSACTION_STATUS_ID  = 203;
        $masterTransaction->TRANSACTION_TYPE_ID    = 66;
        $masterTransaction->TRANSACTION_AMOUNT     = $withdrawRes->PAYMENT_TRANSACTION_AMOUNT;
        $masterTransaction->TRANSACTION_DATE       = date('Y-m-d H:i:s');
        $masterTransaction->INTERNAL_REFERENCE_NO  = $refNo;
        $masterTransaction->CURRENT_TOT_BALANCE    = $userCurrentCommissionBalance->total;
        $masterTransaction->CLOSING_TOT_BALANCE    = $newClosingTotalBalance;
        $masterTransaction->PARTNER_ID             = 10001;
        $masterTransaction->save();

        /*
        * ------------------------------------------------------------------------------------
        * UPDATING USER POINTS 
        * ------------------------------------------------------------------------------------
        */
        DB::select(
            'update user_points SET
                        `VALUE` = ?,
                        `USER_WIN_BALANCE` = `USER_WIN_BALANCE` + ?,
                        `USER_TOT_BALANCE` = `USER_WIN_BALANCE` + `USER_PROMO_BALANCE` + `USER_DEPOSIT_BALANCE`,
                        `UPDATED_DATE` = ?
            WHERE
                `USER_ID` = ? AND
                `COIN_TYPE_ID` = ?
            ',
            [
                $withdrawRes->PAYMENT_TRANSACTION_AMOUNT,
                $withdrawRes->PAYMENT_TRANSACTION_AMOUNT,
                date('Y-m-d H:i:s'),
                $withdrawRes->USER_ID,
                16
            ]
        );           
                  
        return response()->json(['status' => 'success', 'msg' => 'TRANSFER_REVERSED_SUCCESS', 'code' => 6002, 'refNoArray' => $refNo]);
    } 

    return response()->json(['status' => 'failed', 'msg' => 'FAILED', 'code' => 6003, 'refNoArray' => $refNo]);  
  }

  private function userCurrentBalance(Int $coinType, $userId){
      return UserPoint::select(
          'USER_DEPOSIT_BALANCE AS deposit',
          'USER_PROMO_BALANCE AS promo',
          'USER_WIN_BALANCE AS win',
          'USER_TOT_BALANCE AS total'
      )
      ->where('USER_ID', $userId)
      ->where('COIN_TYPE_ID', $coinType)
      ->first();
  }

}


