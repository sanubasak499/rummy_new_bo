<?php

namespace App\Http\Controllers\bo\reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterTransactionHistory;
use App\Models\WithdrawTransactionHistory;
use Carbon\Carbon;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Author: Nitesh Kumar Jha
 * Purpose: Get all the User Tax Summary result
 * Created Date: 12-2-2020
 * Updated Date: 14-9-2020
 * updated summary :withdraw_transaction_history and union with old query
 */

class BaazitdsSearchController extends Controller
{

    public function index()
    {

        return view('bo.views.reports.BaazitdsSearch');
    }
    // Search New Baazi tax report deatils 
    public function BaaziTdsSearch(Request $request)
    {
        $perPage = config('poker_config.paginate.per_page');
        if (!$request->isMethod('post')) {
            return view('bo.views.reports.BaazitdsSearch');
        }
        $page = request()->page;
        if ($request->transaction_type != '') {
            $transactionType   = $request->transaction_type;
        } else {
            $transactionType = array();
        }
        if ($transactionType != '') {
            $request->session()->put('transactionType', $transactionType);
        } else {
            $request->session()->forget('transactionType', $transactionType);
        }

        $transactionData = $this->getAllResult($request);

        $totalAmount = $transactionData->sum('PAYMENT_TRANSACTION_AMOUNT');
        $TDSAmount = $transactionData->sum('TRANSACTION_AMOUNT');
        $transactionData = $transactionData->paginate($perPage, ['*'], 'page', $page);
        $params = $request->all();
        $params['page'] = $page;

        return view('bo.views.reports.BaazitdsSearch', ['allTransactionSearchData' => $transactionData, 'params' => $params, 'totalAmount' => $totalAmount, 'TDSAmount' => $TDSAmount]);
    }



    public function getAllResult($request)
    {
       
        $perPage = config('poker_config.paginate.per_page');
        $page = request()->page;
          // old query 
        $transactionData = MasterTransactionHistory::from(app(MasterTransactionHistory::class)->getTable() . " as t")
            ->select('t.TRANSACTION_AMOUNT', 't.TRANSACTION_DATE', 't.TRANSACTION_STATUS_ID', 't.USER_ID', 't.INTERNAL_REFERENCE_NO', 'u.USERNAME', 'u.EMAIL_ID', 'u.CONTACT', 'kyc.PAN_NUMBER', 'pt.PAYMENT_TRANSACTION_STATUS', 'pt.PAYMENT_TRANSACTION_AMOUNT', 'u.FIRSTNAME', 'u.LASTNAME')
            ->join('payment_transaction as pt', 'pt.INTERNAL_REFERENCE_NO', '=', 't.INTERNAL_REFERENCE_NO')
            ->join('user as u', 'u.USER_ID', '=', 't.USER_ID')
            ->join('USER_KYC as kyc', 'kyc.USER_ID', '=', 't.USER_ID')
            ->when($request->search_by == "USERNAME", function ($query) use ($request) {
                return $query->where('u.USERNAME', 'LIKE', "%{$request->search_by_value}%");
            })
            ->when($request->search_by == "EMAIL_ID", function ($query) use ($request) {
                return $query->where('u.EMAIL_ID', 'LIKE', "%{$request->search_by_value}%");
            })
            ->when($request->search_by == "CONTACT", function ($query) use ($request) {
                return $query->where('u.CONTACT', 'LIKE', "%{$request->search_by_value}%");
            })
            ->when(!empty($request->PAN_NUMBER), function ($query) use ($request) {
                return $query->where('kyc.PAN_NUMBER', $request->PAN_NUMBER);
            })
            ->when(!empty($request->INTERNAL_REFERENCE_NO), function ($query) use ($request) {
                return $query->where('t.INTERNAL_REFERENCE_NO', $request->INTERNAL_REFERENCE_NO);
            })

            ->when(!empty($request->transaction_type), function ($query) use ($request) {
                return $query->whereIn('pt.PAYMENT_TRANSACTION_STATUS', $request->transaction_type);
            })

            ->when(!empty($request->START_DATE_TIME), function ($query) use ($request) {
                $dateFrom = Carbon::parse($request->START_DATE_TIME)->format('Y-m-d H:i:s');
                return $query->whereDate('t.TRANSACTION_DATE', ">=", $dateFrom);
            })
            ->when(!empty($request->END_DATE_TIME), function ($query) use ($request) {
                $dateTo = Carbon::parse($request->END_DATE_TIME)->format('Y-m-d H:i:s');
                return $query->whereDate('t.TRANSACTION_DATE', "<=", $dateTo);
            })
            ->where('t.TRANSACTION_TYPE_ID', 80)
            ->where('pt.PAYMENT_TRANSACTION_STATUS','!=', 203)
            ->orderBy('t.TRANSACTION_DATE', 'DESC');

         // new query
        $Withrawresult = WithdrawTransactionHistory::from(app(WithdrawTransactionHistory::class)->getTable() . " as wth")
            ->select('wth.WITHDRAW_TDS as TRANSACTION_AMOUNT', 'wth.TRANSACTION_DATE', 'wth.TRANSACTION_STATUS_ID as PAYMENT_TRANSACTION_STATUS', 'wth.USER_ID', 'wth.INTERNAL_REFERENCE_NO', 'u.USERNAME', 'u.EMAIL_ID', 'u.CONTACT', 'kyc.PAN_NUMBER', 'wth.TRANSACTION_TYPE_ID', 'wth.WITHDRAW_AMOUNT as PAYMENT_TRANSACTION_AMOUNT', 'u.FIRSTNAME', 'u.LASTNAME')
            ->join('user as u', 'u.USER_ID', '=', 'wth.USER_ID')
            ->join('USER_KYC as kyc', 'kyc.USER_ID', '=', 'wth.USER_ID')

            ->when($request->search_by == "USERNAME", function ($query) use ($request) {
                return $query->where('u.USERNAME', 'LIKE', "%{$request->search_by_value}%");
            })
            ->when($request->search_by == "EMAIL_ID", function ($query) use ($request) {
                return $query->where('u.EMAIL_ID', 'LIKE', "%{$request->search_by_value}%");
            })
            ->when($request->search_by == "CONTACT", function ($query) use ($request) {
                return $query->where('u.CONTACT', 'LIKE', "%{$request->search_by_value}%");
            })
            ->when(!empty($request->PAN_NUMBER), function ($query) use ($request) {
                return $query->where('kyc.PAN_NUMBER', $request->PAN_NUMBER);
            })
            ->when(!empty($request->INTERNAL_REFERENCE_NO), function ($query) use ($request) {
                return $query->where('wth.INTERNAL_REFERENCE_NO', $request->INTERNAL_REFERENCE_NO);
            })

            ->when(!empty($request->transaction_type), function ($query) use ($request) {
                return $query->whereIn('wth.TRANSACTION_STATUS_ID', $request->transaction_type);
            })

            ->when(!empty($request->START_DATE_TIME), function ($query) use ($request) {
                $dateFrom = Carbon::parse($request->START_DATE_TIME)->format('Y-m-d H:i:s');
                return $query->whereDate('wth.TRANSACTION_DATE', ">=", $dateFrom);
            })
            ->when(!empty($request->END_DATE_TIME), function ($query) use ($request) {
                $dateTo = Carbon::parse($request->END_DATE_TIME)->format('Y-m-d H:i:s');
                return $query->whereDate('wth.TRANSACTION_DATE', "<=", $dateTo);
            })
            ->where('wth.TRANSACTION_TYPE_ID', 10)
            ->where('wth.TRANSACTION_STATUS_ID','!=', 203)
            ->union($transactionData)
            ->orderBy('TRANSACTION_DATE', 'DESC');
        return $Withrawresult;
    }

    // Export To Excel 
    public function exportExcel(Request $request)
    {
        $request->all();
        $dateFrom = $request->dateFrom;
        $dateTo = $request->dateTo;


        $search_by = $request->search_by;
        $search_by_value = $request->search_by_value;
        $PAN_NUMBER = $request->PAN_NUMBER;
        $INTERNAL_REFERENCE_NO = $request->INTERNAL_REFERENCE_NO;
 
        if (session()->get('transactionType') != '') {
            $transactionType   = session()->get('transactionType');
        } else {
            $transactionType = array();
        }
        if ($transactionType != '') {
            $request->session()->put('transactionType', $transactionType);
        } else {
            $request->session()->forget('transactionType', $transactionType);
        }
        
        $tdsData = MasterTransactionHistory::from(app(MasterTransactionHistory::class)->getTable() . " as t")
            ->select('t.TRANSACTION_AMOUNT', 't.TRANSACTION_DATE', 't.TRANSACTION_STATUS_ID', 't.USER_ID', 't.INTERNAL_REFERENCE_NO', 'u.USERNAME', 'u.FIRSTNAME', 'u.LASTNAME', 'u.EMAIL_ID', 'u.CONTACT', 'kyc.PAN_NUMBER', 'pt.PAYMENT_TRANSACTION_STATUS', 'pt.PAYMENT_TRANSACTION_AMOUNT')
            ->join('payment_transaction as pt', 'pt.INTERNAL_REFERENCE_NO', '=', 't.INTERNAL_REFERENCE_NO')
            ->join('user as u', 'u.USER_ID', '=', 't.USER_ID')
            ->join('USER_KYC as kyc', 'kyc.USER_ID', '=', 't.USER_ID')
            ->when($search_by == "USERNAME", function ($query) use ($search_by_value) {
                return $query->where('u.USERNAME', 'LIKE', "%{$search_by_value}%");
            })
            ->when($search_by == "EMAIL_ID", function ($query) use ($search_by_value) {
                return $query->where('u.EMAIL_ID', 'LIKE', "%{$search_by_value}%");
            })
            ->when($search_by == "CONTACT", function ($query) use ($search_by_value) {
                return $query->where('u.CONTACT', 'LIKE', "%{$search_by_value}%");
            })

            ->when(!empty($PAN_NUMBER), function ($query) use ($PAN_NUMBER) {
                return $query->where('kyc.PAN_NUMBER', $PAN_NUMBER);
            })

            ->when(!empty($INTERNAL_REFERENCE_NO), function ($query) use ($INTERNAL_REFERENCE_NO) {
                return $query->where('t.INTERNAL_REFERENCE_NO', $INTERNAL_REFERENCE_NO);
            })

            ->when(!empty($transactionType), function ($query) use ($transactionType) {
                return $query->whereIn('pt.PAYMENT_TRANSACTION_STATUS', $transactionType);
            })

            ->when(!empty($dateFrom), function ($query) use ($dateFrom) {
                $dateFrom = Carbon::parse($dateFrom)->format('Y-m-d H:i:s');
                return $query->whereDate('t.TRANSACTION_DATE', ">=", $dateFrom);
            })
            ->when(!empty($dateTo), function ($query) use ($dateTo) {
                $dateTo = Carbon::parse($dateTo)->format('Y-m-d H:i:s');
                return $query->whereDate('t.TRANSACTION_DATE', "<=", $dateTo);
            })
            ->where('t.TRANSACTION_TYPE_ID', 80)
            ->where('pt.PAYMENT_TRANSACTION_STATUS','!=', 203)
            ->orderBy('t.TRANSACTION_DATE', 'DESC');


        $WithdrawResult = WithdrawTransactionHistory::from(app(WithdrawTransactionHistory::class)->getTable() . " as wth")
            ->select('wth.WITHDRAW_TDS as TRANSACTION_AMOUNT', 'wth.TRANSACTION_DATE', 'wth.TRANSACTION_STATUS_ID as PAYMENT_TRANSACTION_STATUS', 'wth.USER_ID', 'wth.INTERNAL_REFERENCE_NO', 'u.USERNAME', 'u.FIRSTNAME', 'u.LASTNAME', 'u.EMAIL_ID', 'u.CONTACT', 'kyc.PAN_NUMBER', 'wth.TRANSACTION_TYPE_ID', 'wth.WITHDRAW_AMOUNT as PAYMENT_TRANSACTION_AMOUNT')
            ->join('user as u', 'u.USER_ID', '=', 'wth.USER_ID')
            ->join('USER_KYC as kyc', 'kyc.USER_ID', '=', 'wth.USER_ID')

            ->when($search_by == "USERNAME", function ($query) use ($search_by_value) {
                return $query->where('u.USERNAME', 'LIKE', "%{$search_by_value}%");
            })
            ->when($search_by == "EMAIL_ID", function ($query) use ($search_by_value) {
                return $query->where('u.EMAIL_ID', 'LIKE', "%{$search_by_value}%");
            })
            ->when($search_by == "CONTACT", function ($query) use ($search_by_value) {
                return $query->where('u.CONTACT', 'LIKE', "%{$search_by_value}%");
            })

            ->when(!empty($PAN_NUMBER), function ($query) use ($PAN_NUMBER) {
                return $query->where('kyc.PAN_NUMBER', $PAN_NUMBER);
            })

            ->when(!empty($INTERNAL_REFERENCE_NO), function ($query) use ($INTERNAL_REFERENCE_NO) {
                return $query->where('wth.INTERNAL_REFERENCE_NO', $INTERNAL_REFERENCE_NO);
            })

            ->when(!empty($transactionType), function ($query) use ($transactionType) {
                return $query->whereIn('wth.TRANSACTION_STATUS_ID', $transactionType);
            })

            ->when(!empty($dateFrom), function ($query) use ($dateFrom) {
                $dateFrom = Carbon::parse($dateFrom)->format('Y-m-d H:i:s');
                return $query->whereDate('wth.TRANSACTION_DATE', ">=", $dateFrom);
            })
            ->when(!empty($dateTo), function ($query) use ($dateTo) {
                $dateTo = Carbon::parse($dateTo)->format('Y-m-d H:i:s');
                return $query->whereDate('wth.TRANSACTION_DATE', "<=", $dateTo);
            })
            ->where('wth.TRANSACTION_TYPE_ID', 10)
            ->where('wth.TRANSACTION_STATUS_ID','!=', 203)
            ->union($tdsData)
            ->orderBy('TRANSACTION_DATE', 'DESC')
            ->get();

        foreach ($WithdrawResult as $WithdrawResult) {

            if ($WithdrawResult->PAYMENT_TRANSACTION_STATUS == 109) {
                $dataRes = 'Pending';
            } else if ($WithdrawResult->PAYMENT_TRANSACTION_STATUS == 208) {
                $dataRes = 'Approved';
            } else if ($WithdrawResult->PAYMENT_TRANSACTION_STATUS == 209) {
                $dataRes = 'Paid';
            } else if ($WithdrawResult->PAYMENT_TRANSACTION_STATUS == 216) {
                $dataRes = 'Failed';
            } else if ($WithdrawResult->PAYMENT_TRANSACTION_STATUS == 212) {
                $dataRes = 'Rejected';
            } else if ($WithdrawResult->PAYMENT_TRANSACTION_STATUS == 203) {
                $dataRes = 'Revert Withdraw';
            } else if ($WithdrawResult->PAYMENT_TRANSACTION_STATUS == 254) {
                $dataRes = 'Initiated ';
            }else {
                $dataRes = 'null';
            }
            $newArray[] =
                array(
                    'USER_ID' => $WithdrawResult->USER_ID,
                    'USERNAME' => $WithdrawResult->USERNAME,
                    'INTERNAL_REFERENCE_NO' => $WithdrawResult->INTERNAL_REFERENCE_NO,
                    'FIRSTNAME' => $WithdrawResult->FIRSTNAME,
                    'EMAIL_ID' => addslashes($WithdrawResult->EMAIL_ID),
                    'CONTACT' => $WithdrawResult->CONTACT,
                    'PAN_NUMBER' => $WithdrawResult->PAN_NUMBER,
                    'PAYMENT_TRANSACTION_AMOUNT' => $WithdrawResult->PAYMENT_TRANSACTION_AMOUNT,
                    'TRANSACTION_AMOUNT' => $WithdrawResult->TRANSACTION_AMOUNT,
                    'TRANSACTION_DATE' => $WithdrawResult->TRANSACTION_DATE,
                    'PAYMENT_TRANSACTION_STATUS' => $dataRes,
                );
        }

        $data[0] = array("USER_ID" => "", "USERNAME" => "", "INTERNAL_REFERENCE_NO" => "", "NAME" => "", "EMAIL_ID" => "", "CONTACT" => "", "PAN_NUMBER" => "", "WITHDRAW_AMOUNT" => "", "TDS_AMOUNT" => "", "TRANSACTION_DATE" => "", "TDS_STATUS" => "");
        $name = "User-Tax-Summary";
        $fileName = $name . ".xlsx";
        $excelData = $newArray;
        $headings = array_keys($data[0]);
        return Excel::download(new CollectionExport($excelData, $headings), $fileName);
    }
}
