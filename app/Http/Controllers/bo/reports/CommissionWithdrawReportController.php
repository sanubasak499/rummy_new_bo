<?php

namespace App\Http\Controllers\bo\reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterTransactionHistoryPokercommission;
use App\Models\PaymentTransactionPokercommission;
use App\Models\User;
use App\Models\UserPoint;
use App\Models\CashoutApproveTransaction;
use App\Models\UserAccountDetails;
use Carbon\Carbon;
use App\Traits\Models\common_methods;
use Session;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
use App\Traits\common_mail;
use \PDF;
use DB;
use Auth;

class CommissionWithdrawReportController extends Controller
{

  //
  use common_methods, common_mail;

  public function index(Request $request)
  {

    // DB::enableQueryLog();
    $perPage = config('poker_config.paginate.per_page');

    if ($request->isMethod('post')) {

      $page = request()->page;
      $username          = $request->username;
      $ref_no            = $request->ref_no;
      $withdrawStatus    = $request->withdraw_status;
      $transferId        = $request->tranfer_id;
      $approveType       = $request->approve_type;
      $amount            = $request->amount;
      $dateFrom          = $request->date_from;
      $dateTo            = $request->date_to;
      $first_withdraw    = $request->first_withdraw;
      $userFlag          = $request->user_flag;
      $check_pass        = $request->check_pass;
      $search_by         = $request->search_by;


      //  dd($request->user_flag);
      //session set for excel export

      Session::put('com_username', $username);
      Session::put('com_ref_no', $ref_no);
      Session::put('com_withdrawStatus', $withdrawStatus);
      Session::put('com_transferId', $transferId);
      Session::put('com_approveType', $approveType);
      Session::put('com_amount', $amount);
      Session::put('com_dateFrom', $dateFrom);
      Session::put('com_dateTo', $dateTo);
      Session::put('com_first_withdraw', $first_withdraw);
      Session::put('com_userFlag', $userFlag);
      Session::put('com_check_pass', $check_pass);
      Session::put('com_search_by', $search_by);
      //session set for excel export end
      //get all withdrawl data
      $WithRepoData = $this->getAllWithdrawlData($username, $search_by, $ref_no, $amount, $dateFrom, $dateTo, $withdrawStatus, $transferId, $approveType, $first_withdraw, $userFlag, $check_pass);
      $totalToBePaid = $WithRepoData->get();
      // echo "<pre>";
      // print_r($WithRepoData->getBindings());
      // print_r($WithRepoData->toSql());exit;
      $withData = $WithRepoData->paginate($perPage, ['*'], 'page', $page);
      // echo  "<pre>";
      // print_r($withData);exit;
      $params = $request->all();
      $params['page'] = $page;
      if (!empty($username) && empty($user_id) && $search_by == "email") {
        return view('bo.views.reports.commissionWithdraw', ['withdrawData' => '', 'params' => $params]);
      } else {
        return view('bo.views.reports.commissionWithdraw', ['withdrawData' => $withData, 'params' => $params, 'totalToBePaid' => $totalToBePaid]);
      }
    } else {
      return view('bo.views.reports.commissionWithdraw');
    }
  }

  public function getAllWithdrawlData($username, $search_by, $ref_no, $amount, $dateFrom, $dateTo, $withdrawStatus, $transferId, $approveType, $first_withdraw, $userFlag, $check_pass)
  {
    $now = date('Y-m-d H:i:s');
    $yesterday = date('Y-m-d H:i:s');
    if (empty($dateTo)) {
      $dateTo = date('Y-m-d H:i:s');
    }
    $query = PaymentTransactionPokercommission::query();
    $query->from(app(PaymentTransactionPokercommission::class)->getTable() . " as ptpc");
    $query->join('master_transaction_history_pokercommission as mthp', 'mthp.INTERNAL_REFERENCE_NO', '=', 'ptpc.INTERNAL_REFERENCE_NO');
    $query->leftJoin('cashout_approve_transaction as cat', 'mthp.INTERNAL_REFERENCE_NO', '=', 'ptpc.INTERNAL_REFERENCE_NO');
    $query->join('transaction_status as ts', 'ts.TRANSACTION_STATUS_ID', '=', 'ptpc.PAYMENT_TRANSACTION_STATUS');
    $query->join('user as u', 'u.USER_ID', '=', 'ptpc.USER_ID');
    $query->leftJoin('USER_KYC as uk', 'uk.USER_ID', '=', 'ptpc.USER_ID');
    $query->leftJoin('reward_user_type as rut', 'rut.USER_ID', '=', 'ptpc.USER_ID');
    // $query->leftJoin('withdraw_transaction_history as wth', 'wth.INTERNAL_REFERENCE_NO', '=', 'pt.INTERNAL_REFERENCE_NO');
    $query->when(!empty($ref_no), function ($query) use ($ref_no) {
      return $query->where('ptpc.INTERNAL_REFERENCE_NO', $ref_no);
    })
      ->when(!empty($amount), function ($query) use ($amount) {
        return $query->where('ptpc.PAYMENT_TRANSACTION_AMOUNT', $amount);
      });
    $query->when(!empty($transferId), function ($query) use ($transferId) {
      return $query->where('ptpc.TRANSFER_ID', $transferId);
    });

    // $query->when(!empty($user_id), function ($query) use ($user_id) {
    //   return $query->where('wth.USER_ID', $user_id);
    // });
    $query->when(!empty($withdrawStatus), function ($query) use ($withdrawStatus) {
      return $query->whereIn('ptpc.PAYMENT_TRANSACTION_STATUS', $withdrawStatus);
    });
    $query->when(!empty($approveType) && $approveType == "normal", function ($query) use ($approveType) {
      return $query->where('ptpc.APPROVE_BY_CASHFREE_STATUS', '!=', '1')->where('ptpc.APPROVE_BY_PAYU_STATUS', '!=', 1);
    });
    $query->when(!empty($approveType) && $approveType == "payu", function ($query) use ($approveType) {
      return $query->where('ptpc.APPROVE_BY_PAYU_STATUS', 1);
    });
    $query->when(!empty($approveType) && $approveType == "cashfree", function ($query) use ($approveType) {
      return $query->where('ptpc.APPROVE_BY_CASHFREE_STATUS', 1);
    });
    $query->when(!empty($dateFrom) && !empty($dateTo) && empty($ref_no), function ($query) use ($dateFrom, $dateTo) {
      $dateFrom = Carbon::parse($dateFrom)->format('Y-m-d H:i:s');
      $dateTo = Carbon::parse($dateTo)->format('Y-m-d H:i:s');
      return $query->whereBetween('ptpc.PAYMENT_TRANSACTION_CREATED_ON', [$dateFrom, $dateTo]);
    });


    $query->when(!empty($userFlag), function ($query) use ($userFlag) {
      return $query->where('u.user_flag', $userFlag);
    });
    $query->when(!empty($check_pass) && $check_pass == 2, function ($query) use ($check_pass) {
      return $query->where('ptpc.CHECKING_STATUS', $check_pass);
    });

    $query->when(!empty($check_pass) && $check_pass == 3, function ($query) use ($check_pass) {
      return $query->where('ptpc.CHECKING_STATUS', $check_pass);
    });

    $query->when(!empty($check_pass) && $check_pass == 1, function ($query) use ($check_pass) {

      return $query->whereIn('ptpc.CHECKING_STATUS', array(NULL, '', 1));
    });
    //search username
    $query->when(!empty($username) && $search_by == "username", function ($query) use ($username) {
      return $query->Where('u.USERNAME', 'LIKE', $username . '%');
    });

    $query->when(!empty($username) && $search_by == "email", function ($query) use ($username) {
      return $query->where('u.EMAIL_ID', $username);
    });
    $query->when(!empty($username) && $search_by == "contact_no", function ($query) use ($username) {
      return $query->where('u.CONTACT', $username);
    });
    // check first withdraw
    $query->when(!empty($first_withdraw) && $first_withdraw == "yes", function ($query) use ($first_withdraw, $now) {
      return $query->whereNotIn('ptpc.USER_ID',  function ($query) use ($first_withdraw, $now) {
        $query->select(['pt.USER_ID'])
          ->from((new PaymentTransactionPokercommission)->getTable() . " as pt")
          ->join('user as u', 'u.USER_ID', '=', 'pt.USER_ID')
          ->where('pt.TRANSACTION_TYPE_ID', 10)
          ->havingRaw('COUNT(pt.USER_ID) > ?', [0])
          ->where('pt.PAYMENT_TRANSACTION_STATUS', 208)
          ->whereBetween('pt.PAYMENT_TRANSACTION_CREATED_ON', ['u.REGISTRATION_TIMESTAMP', $now])
          ->groupBy('pt.USER_ID');
      });
    });
    $query->when(!empty($first_withdraw) && $first_withdraw == "no", function ($query) use ($first_withdraw, $now) {
      return $query->whereIn('ptpc.USER_ID',  function ($query) use ($first_withdraw, $now) {
        $query->select(['pt.USER_ID'])
          ->from((new PaymentTransactionPokercommission)->getTable() . " as pt")
          ->join('user as u', 'u.USER_ID', '=', 'pt.USER_ID')
          ->where('pt.TRANSACTION_TYPE_ID', 10)
          ->havingRaw('COUNT(pt.USER_ID) > ?', [0])
          ->where('pt.PAYMENT_TRANSACTION_STATUS', 208)
          ->whereBetween('pt.PAYMENT_TRANSACTION_CREATED_ON', ['u.REGISTRATION_TIMESTAMP', $now])
          ->groupBy('pt.USER_ID');
      });
    });
    $query->where('ptpc.TRANSACTION_TYPE_ID', '10');
    $query->where('ptpc.PAYMENT_TRANSACTION_STATUS', "!=", '203');
    $query->orderBy('ptpc.PAYMENT_TRANSACTION_CREATED_ON', 'DESC');
    $query->groupBy('ptpc.INTERNAL_REFERENCE_NO');

    $payTransData = $query->select(
      'u.USERNAME',
      'u.REGISTRATION_TIMESTAMP',
      'u.user_flag',
      'u.EMAIL_ID',
      'u.CONTACT',
      'uk.AADHAAR_FRONT_URL',
      'uk.AADHAAR_BACK_URL',
      'uk.AADHAAR_NUMBER',
      'uk.AADHAAR_STATUS',
      'uk.OTHER_DOC_URL',
      'uk.OTHER_DOC_URL_BACK',
      'uk.OTHER_DOC_NUMBER',
      'uk.OTHER_DOC_STATUS',
      // 'uk.PAN_URL',
      'uk.PAN_NUMBER',
      'uk.PAN_STATUS',
      'ptpc.USER_ID',
      'ptpc.PAYMENT_TRANSACTION_AMOUNT',
      'ptpc.PAYMENT_TRANSACTION_STATUS',
      'ptpc.PAYMENT_TRANSACTION_CREATED_ON',
      'ptpc.INTERNAL_REFERENCE_NO',
      'ptpc.CHECKING_COMMENT',
      'ptpc.CHECKING_STATUS',
      'ts.TRANSACTION_STATUS_DESCRIPTION',
      'ptpc.APPROVE_BY_CASHFREE_STATUS',
      'ptpc.APPROVE_BY_PAYU_STATUS',
      'rut.REWARD_STATUS',
      DB::raw("MAX(CASE WHEN mthp.TRANSACTION_TYPE_ID = 80 THEN mthp.TRANSACTION_AMOUNT ELSE 0 END) AS COM_TDS"), // for get the tobe Paid value
    );
    return $payTransData;
  }

  

  //normal approve button click
  public function commWithdrawNormalApprove(Request $request)
  {
    if (empty($request->all())) {
      return response()->json(['status' => 'failed', 'msg' => 'Please select atleast one row']);
    }
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];
      foreach ($request as $values) {
        $refNo = $values['refNo'];
        // $withdrwaType = 1;
        // $approvedAt = "";
        $withdrawTrans = $this->paymentApprove($refNo);
        $refNoArray[] = $refNo;
        $armasterStatus[] = $withdrawTrans;
      }
      if (!empty($armasterStatus) && !empty($withdrawTrans->USER_ID)) {
        $data['Withdrwa Normal Approve '] = json_encode($refNoArray);
        $action = "Comm Withdrwa Normal Approve Success";
        $this->insertAdminActivity($data, $action);
        //mail withdraw normal approve params
        $userDetails = $this->getUserDetailsFromUserId($withdrawTrans->USER_ID);
        $params['email'] = $userDetails->EMAIL_ID;
        $params['username'] = $userDetails->USERNAME;
        $params['tempId'] = 12;
        $params['subject'] = "Withdrawl Request Approved";
        $params['refNo'] = $refNo;
        $params['amount'] = $withdrawTrans->WITHDRAW_AMOUNT;
        $params['date'] = date('d-m-Y');
        $params['time'] = date('H:i:s');
        $this->sendMail($params);
        return response()->json(['status' => 'success', 'code' => 1000, 'refNoArray' => $refNoArray]);
      } else {
        $data['Withdrwa Normal Approve Failed'] = json_encode($refNoArray);
        $action = "Comm Withdrwa Normal Approve Failed";
        $this->insertAdminActivity($data, $action);
        return response()->json(['status' => 'failed', 'type' => 'payment_approve_failed', 'msg' => 'some thing went wrong with this payment  (' . $refNo . ') please try again later',  'code' => 1001, 'refNoArray' => $refNoArray]);
      }
    }
  }
  //commission normal reject button click
  public function commWithdrawNormalReject(Request $request)
  {
    if (empty($request->all())) {
      return response()->json(['status' => 'failed', 'msg' => 'Please select atleast one row']);
    }
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];
      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $checkRes = $this->getPaymentDetailsFromPaymentTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);
        //if ($checkRes->PAYMENT_TRANSACTION_STATUS != "109") { //checking Normal Approve (0) or approve via cashfree(1)
        $updatePaymentTrans = $this->commNormalWithdrawRejected($refNo, $checkRes->USER_ID);
        //}
        $refNoArray[] = $refNo;
        $updatePaymentTransArray[] = $updatePaymentTrans;
      }
      if ($updatePaymentTransArray) {
        $data['Withdrwa Normal Reject Sucess'] = json_encode($refNoArray);
        $action = "Withdrwa Normal Reject Sucess";
        $this->insertAdminActivity($data, $action);
        //mail withdraw normal Reject params
        $userDetails = $this->getUserDetailsFromUserId($checkRes->USER_ID);
        $params['email'] = $userDetails->EMAIL_ID;
        $params['username'] = $userDetails->USERNAME;
        $params['tempId'] = 13;
        $params['subject'] = "Comm Withdrawl Request Rejected";
        $params['refNo'] = $refNo;
        $params['amount'] = $checkRes->PAYMENT_TRANSACTION_AMOUNT;
        $params['date'] = date('d-m-Y');
        $params['time'] = date('H:i:s');
        $this->sendMail($params);
        return response()->json(['status' => 'success', 'type' => 'withdraw_normal_reject', 'code' => 2000, 'refNoArray' => $refNoArray]);
      } else {
        $data['Withdrwa Normal Reject Failed'] = json_encode($refNoArray);
        $action = "Withdrwa Normal Reject Failed";
        $this->insertAdminActivity($data, $action);
        return response()->json(['status' => 'failed', 'type' => 'withdraw_normal_reject', 'msg' => 'some thing went wrong with this payment  (' . $refNoArray . ') please try again later',  'code' => 2001, 'refNoArray' => $refNoArray]);
      }
    }
  }
  // cashfree approve button click
  public function withdrawCashfreeApprove(Request $request)
  {
    // echo "hello";exit;
    if (empty($request->all())) {
      return response()->json(['status' => 'failed', 'msg' => 'Please select atleast one row']);
    }
    if ($request->isMethod('post')) {
      if (empty($request->all()['tableData'])) {
        return response()->json(['status' => 'failed', 'type' => 'empty_selection', 'code' => 501]);
      }
      $request = $request->all()['tableData'];
      foreach ($request as $values) {
        $transferBeneficiaryResponse = [];
        $tranferBeneficiaryData = [];
        $addBeneficiaryData = [];
        $additiionalTracking = [];
        $refNo = $values['refNo'];
        $transTypeId = ['10'];
        $getPayUserInfo = $this->getPaymentDetailsFromMasterTransaction($refNo, $transTypeId, $transStatusId = NULL, $userId = NULL); // //success-pending-reversed, failed(216). when there is no record for the given status, then continue
        $initiatedStatusrRes = $getPayUserInfo->first();
        $getPaymentInfo = $this->getPaymentDetailsFromPaymentTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);
        $checkPaymentStatusRes = $getPaymentInfo->first();
        // print_r($initiatedStatusrRes);exit;
        $transaction_status_id = array(109, 216);
        if (!empty($initiatedStatusrRes) && !empty($checkPaymentStatusRes) && $checkPaymentStatusRes->APPROVE_BY_CASHFREE_STATUS != 1  && $checkPaymentStatusRes->APPROVE_BY_PAYU_STATUS != 1 && in_array($initiatedStatusrRes->TRANSACTION_STATUS_ID, $transaction_status_id)) { //if payment is initatiated

          $userId  =  $initiatedStatusrRes->USER_ID;
          $userAccountDetails = $this->getUserAccountDetails($userId);
          $getUserDetails = $this->getUserDetailsFromUserId($userId);
          $userAccountNo = $userAccountDetails->ACCOUNT_NUMBER;
          $userIfscCode = $userAccountDetails->IFSC_CODE;
          $userFullName = $userAccountDetails->ACCOUNT_HOLDER_NAME;
          $userEmail = $getUserDetails->EMAIL_ID;
          $userMobile = $getUserDetails->CONTACT;
          $userAddress = $getUserDetails->ADDRESS;
          // $witTrans = $this->getPaymentDetailsFromMasterTransaction($refNo, $transTypeId, $transStatusId = NULL, NULL);
          // $witTransRes = $witTrans->first();
          $paidamount = $initiatedStatusrRes->TRANSACTION_AMOUNT;

          $transferId = $userId . date('YmdHis') . $this->genRandomRefNo(7);
          $updatePaymentStatus = PaymentTransactionPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
            ->update(['APPROVE_BY_CASHFREE_STATUS' => 1, 'TRANSFER_ID' => $transferId]);
          // $this->updateCashfreeTransIdInInstantRespone($transferId, $refNo);
          $addBeneficiaryData["beneId"] = "$userId";
          $addBeneficiaryData["name"] = $userFullName;
          $addBeneficiaryData["email"] = $userEmail;
          $addBeneficiaryData["phone"] = $userMobile;
          $addBeneficiaryData["bankAccount"] = $userAccountNo;
          $addBeneficiaryData["ifsc"] = $userIfscCode;
          $addBeneficiaryData["address1"] = $userAddress;

          $tranferBeneficiaryData["beneId"] = "$userId";
          $tranferBeneficiaryData["amount"] = $paidamount;
          $tranferBeneficiaryData["transferId"] = $transferId;
          $tranferBeneficiaryData["remarks"] = "cash free transfer to $userId";

          $removeBeneficiaryData["beneId"] = "$userId";

          $transferAmount = $paidamount;
          $CASHFREE_PAYOUT_CLIENT_ID = env('CASHFREE_PAYOUT_CLIENT_ID');
          $CASHFREE_PAYOUT_CLIENT_SECRET = env('CASHFREE_PAYOUT_CLIENT_SECRET');
          $CASHFREE_PAYOUT_PAYMENT_URL = env('CASHFREE_PAYOUT_PAYMENT_URL');
          $endpointAuth = $CASHFREE_PAYOUT_PAYMENT_URL . "/authorize";

          $headers = array(
            "X-Client-Id: $CASHFREE_PAYOUT_CLIENT_ID",
            "X-Client-Secret: $CASHFREE_PAYOUT_CLIENT_SECRET"
          );

          $cashfreeGetToken = $this->cashfreePayOutBatchTransferApi($endpointAuth, $headers, $params = []); //get cashfree token

          if ($cashfreeGetToken["status"] == "SUCCESS") { //authendication success
            $endpointVerifyToken = $CASHFREE_PAYOUT_PAYMENT_URL . "/verifyToken";
            $accessToken = $cashfreeGetToken["data"]["token"];
            $accessTokenheaders = array("Authorization: Bearer $accessToken");
            $verifyTokenResponse = $this->cashfreePayOutBatchTransferApi($endpointVerifyToken, $accessTokenheaders); //verify token

            if ($verifyTokenResponse["status"] == "SUCCESS") { //token successfully verified
              $endpointgetBalance = $CASHFREE_PAYOUT_PAYMENT_URL . "/getBalance";
              $getBalanceResponse = $this->getCashfreeCurl($endpointgetBalance, $accessTokenheaders); //get balance

              if ($getBalanceResponse["status"] == "SUCCESS" && $getBalanceResponse["data"]["availableBalance"] >= $transferAmount) {

                $endpointgetBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/getBeneficiary/" . $userId;

                $getBeneficiaryResponse = $this->getCashfreeCurl($endpointgetBeneficiary, $accessTokenheaders);
                $additiionalTracking['getBeneficiary'] = $getBeneficiaryResponse;
                if ($getBeneficiaryResponse["status"] == "SUCCESS") { //beneficiary exists

                  if ($getBeneficiaryResponse["data"]["bankAccount"] == $userAccountNo && $getBeneficiaryResponse["data"]["ifsc"] == $userIfscCode && $getBeneficiaryResponse["data"]["name"] == $userFullName) { // matching Beneficiary account details

                    $endpointTransferBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/requestTransfer";

                    $transferBeneficiaryResponse = $this->cashfreePayOutBatchTransferApi($endpointTransferBeneficiary, $accessTokenheaders, $tranferBeneficiaryData); //Request Transfer
                    $additiionalTracking['transferWithExBen'] = $transferBeneficiaryResponse;
                    $transferBeneficiaryRquestJson = json_encode($tranferBeneficiaryData);
                    $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                    $gateWayJsonResponse = json_encode($transferBeneficiaryResponse);
                    $res = $this->insertCashoutApproveTransaction($initiatedStatusrRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);

                    if ($transferBeneficiaryResponse["status"] == "SUCCESS") { //transferred successfully with existing beneficiary
                      $cashfreeStatusResponse[] = ['status' => 'success', 'code' => '5010', 'refNo' => $refNo, 'msg' => 'Transferred successfully with existing beneficiary'];
                    } else { //transfer failed with existing beneficiary
                      $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '5050', 'refNo' => $refNo, 'msg' => 'Cashfree Processing'];
                      if (!empty($transferBeneficiaryResponse["status"]) && $transferBeneficiaryResponse["status"] != "PENDING" && ($transferBeneficiaryResponse["status"] != "TRANSFER_REVERSED")) {

                        $this->makeCashfreeFailedIfNotSuccess($refNo);
                      }
                      $transferBeneficiaryRquestJson = json_encode($tranferBeneficiaryData);
                      $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                      $trsnFailedWithExistBen = ['status' => 'failed', 'code' => '5051', 'refNo' => $refNo, 'msg' => 'Transferred FAILED with existing beneficiary'];
                      $gateWayJsonResponse = json_encode($trsnFailedWithExistBen);
                      $res = $this->insertCashoutApproveTransaction($initiatedStatusrRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                    }
                  } else { //beneficiary account number mismatches
                    /* Remove Beneficiary */
                    $endpointRemoveBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/removeBeneficiary";
                    $removeBeneficiaryResponse = $this->cashfreePayOutBatchTransferApi($endpointRemoveBeneficiary, $accessTokenheaders, $removeBeneficiaryData);
                    $additiionalTracking['remBenIfdontMatch'] = $removeBeneficiaryResponse;
                    if ($removeBeneficiaryResponse["status"] == "SUCCESS") {
                      /* Add Beneficiary */
                      $endpointAddBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/addBeneficiary";
                      $addBeneficiaryResponse = $this->cashfreePayOutBatchTransferApi($endpointAddBeneficiary, $accessTokenheaders, $addBeneficiaryData);
                      $additiionalTracking['addBenAfterRem'] = $addBeneficiaryResponse;
                      if ($addBeneficiaryResponse["status"] == "SUCCESS") { //beneficiary added successfully
                        /* Request Transfer */
                        $endpointTransferBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/requestTransfer";
                        $transferBeneficiaryResponse = $this->cashfreePayOutBatchTransferApi($endpointTransferBeneficiary, $accessTokenheaders, $tranferBeneficiaryData);
                        $additiionalTracking['tranferAftremAddNewBen'] = $transferBeneficiaryResponse;
                        $transferBeneficiaryRquestJson = json_encode($tranferBeneficiaryData);
                        $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                        $gateWayJsonResponse = json_encode($transferBeneficiaryResponse);
                        $res = $this->insertCashoutApproveTransaction($initiatedStatusrRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                        if ($transferBeneficiaryResponse["status"] == "SUCCESS") { //transferred successfully
                          $cashfreeStatusResponse[] = ['status' => 'success', 'code' => '5012', 'refNo' => $refNo, 'msg' => 'Transferred SUCCESS after remove and add new Beneficiary'];
                        } else {
                          $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '5012', 'refNo' => $refNo, 'msg' => 'Transferred FAILED after remove and add new Beneficiary'];
                          if (!empty($transferBeneficiaryResponse["status"]) &&  $transferBeneficiaryResponse["status"] != "PENDING" && $transferBeneficiaryResponse["status"] != "TRANSFER_REVERSED") {
                            $this->makeCashfreeFailedIfNotSuccess($refNo);
                          }

                          $transferBeneficiaryRquestJson = json_encode($tranferBeneficiaryData);
                          $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                          $removeBen = ['status' => 'failed', 'code' => '5012', 'refNo' => $refNo, 'msg' => 'Transferred FAILED after remove and add new Beneficiary'];
                          $gateWayJsonResponse = json_encode($removeBen);
                          $res = $this->insertCashoutApproveTransaction($initiatedStatusrRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                        }
                        /* Request Transfer */
                      } else {
                        $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '5018', 'refNo' => $refNo, 'msg' => 'failed to Remove old Beneficiary and add new Beneficiary'];
                        //if ($transferBeneficiaryResponse["status"] !== "PENDING" && $transferBeneficiaryResponse["status"] !== "TRANSFER_REVERSED") {
                        $this->makeCashfreeFailedIfNotSuccess($refNo);
                        $transferBeneficiaryRquestJson = json_encode($tranferBeneficiaryData);
                        $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                        $removeOldAddNewBen = ['status' => 'failed', 'code' => '5019', 'refNo' => $refNo, 'msg' => 'failed to Remove old Beneficiary and add new Beneficiary'];
                        $gateWayJsonResponse = json_encode($removeOldAddNewBen);
                        $res = $this->insertCashoutApproveTransaction($initiatedStatusrRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);


                        //}
                      }
                    } else {
                      $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '5013', 'refNo' => $refNo, 'msg' => 'failed to Remove old Beneficiary'];
                      $transferBeneficiaryRquestJson = json_encode($tranferBeneficiaryData);
                      $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                      $failremoveOlBen = ['status' => 'failed', 'code' => '5013', 'refNo' => $refNo, 'msg' => 'failed to Remove old Beneficiary'];
                      $gateWayJsonResponse = json_encode($failremoveOlBen);
                      $res = $this->insertCashoutApproveTransaction($initiatedStatusrRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                      if (!empty($transferBeneficiaryResponse["status"]) && $transferBeneficiaryResponse["status"] != "PENDING" && $transferBeneficiaryResponse["status"] != "TRANSFER_REVERSED") {
                        $this->makeCashfreeFailedIfNotSuccess($refNo);
                      }
                    }
                    /* Remove Beneficiary */
                  }
                } else if ($getBeneficiaryResponse["subCode"] == 404) { //beneficiary does not exist //add beneficiary


                  /* Add Beneficiary */
                  $endpointAddBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/addBeneficiary";
                  $addBeneficiaryResponse = $this->cashfreePayOutBatchTransferApi($endpointAddBeneficiary, $accessTokenheaders, $addBeneficiaryData);
                  $additiionalTracking['addNewBen'] = $addBeneficiaryResponse;
                  if ($addBeneficiaryResponse["status"] == "SUCCESS") { //beneficiary added successfully
                    /* Request Transfer */
                    $endpointTransferBeneficiary = $CASHFREE_PAYOUT_PAYMENT_URL . "/requestTransfer";
                    $transferBeneficiaryResponse = $this->cashfreePayOutBatchTransferApi($endpointTransferBeneficiary, $accessTokenheaders, $tranferBeneficiaryData);
                    $additiionalTracking['tranferWithNewBen'] = $transferBeneficiaryResponse;
                    $transferBeneficiaryRquestJson = json_encode($addBeneficiaryData);
                    $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                    $gateWayJsonResponse = json_encode($transferBeneficiaryResponse);
                    $res = $this->insertCashoutApproveTransaction($initiatedStatusrRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                    if ($transferBeneficiaryResponse["status"] == "SUCCESS") { //transferred successfully after adding Beneficiary
                      $cashfreeStatusResponse[] = ['status' => 'success', 'code' => '5013', 'refNo' => $refNo, 'msg' => 'Transferred Success after adding beneficiary'];
                    } else { //transferred failed after adding Beneficiary
                      $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '5014', 'refNo' => $refNo, 'msg' => 'Transferred failed after adding beneficiary'];
                      $transferBeneficiaryRquestJson = json_encode($addBeneficiaryData);
                      $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                      $failedAddBen = ['status' => 'failed', 'code' => '5014', 'refNo' => $refNo, 'msg' => 'Transferred failed after adding beneficiary'];
                      $gateWayJsonResponse = json_encode($failedAddBen);
                      $res = $this->insertCashoutApproveTransaction($initiatedStatusrRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                      if (!empty($transferBeneficiaryResponse["status"]) &&  $transferBeneficiaryResponse["status"] != "PENDING" && $transferBeneficiaryResponse["status"] != "TRANSFER_REVERSED") {

                        $this->makeCashfreeFailedIfNotSuccess($refNo);
                      }
                    }
                    /* Request Transfer */
                  } else { // failed to add  Beneficiary
                    $this->makeCashfreeFailedIfNotSuccess($refNo);
                    $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '5071', 'refNo' => $refNo, 'msg' => 'Failed To add beneficiary'];
                    $transferBeneficiaryRquestJson = json_encode($addBeneficiaryData);
                    $gateWayjsonRequest = $transferBeneficiaryRquestJson;
                    $failToAddBen = ['status' => 'failed', 'code' => '5072', 'refNo' => $refNo, 'msg' => 'Failed To add beneficiary'];
                    $gateWayJsonResponse = json_encode($failToAddBen);
                    $res = $this->insertCashoutApproveTransaction($initiatedStatusrRes->USER_ID, $refNo, $paidamount, $transferId, $transferBeneficiaryResponse["status"] ?? "ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $transferBeneficiaryResponse["message"] ?? "", "cashfree", NULL);
                  }
                  /* Add Beneficiary */
                }
              } else {
                $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '7001', 'refNo' => $refNo, 'msg' => 'Insufficient Balance !'];
                $this->makeCashfreeFailedIfNotSuccess($refNo);
              }
            } else {
              $this->makeCashfreeFailedIfNotSuccess($refNo);
              $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '7001', 'refNo' => $refNo, 'msg' => 'Failed to veify token!'];
            }
          } else {
            $this->makeCashfreeFailedIfNotSuccess($refNo);
            $cashfreeStatusResponse[] = ['status' => 'failed', 'code' => '7001', 'refNo' => $refNo, 'msg' => 'Failed to generate token!'];
          }
        }
        //adtional tracking 

        $gateWayJsonResponse = json_encode($additiionalTracking);
        $res = $this->insertCashoutApproveTransaction($initiatedStatusrRes->USER_ID, $refNo, $paidamount, $transferId, "COMM_ADTIONAL_TRACKING_ERROR", $userFullName, $userAccountNo, $userIfscCode, $gateWayjsonRequest, $gateWayJsonResponse, "ADTIONAL_TRACKING_ERROR", "cashfree", NULL);
      }
      return response()->json($cashfreeStatusResponse ?? "Something Went wrong !");
    }
  }

  public function cashfreePayOutBatchTransferApi($endpoint, $headers, $params = [])
  {
    $postFields = json_encode($params);
    array_push($headers, 'Content-Type: application/json', 'Content-Length: ' . strlen($postFields));
    $endpoint = $endpoint;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $returnData = curl_exec($ch);
    curl_close($ch);
    if ($returnData != "") {
      return json_decode($returnData, true);
    }
    return NULL;
  }


  //get Beneficiary and check cashfree cashout balance
  public function getCashfreeCurl($endpoint, $headers)
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $endpoint);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $returnData = curl_exec($ch);
    $err = curl_error($ch);
    curl_close($ch);
    if ($returnData != "") {
      return json_decode($returnData, true);
    }
    return NULL;
  }

  // cashfree reject button click
  public function withdrawCashfreeReject(Request $request)
  {
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];
      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $checkRes = $this->getPaymentDetailsFromPaymentTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);

        // echo "<pre>";
        // print_r($checkRes);exit;
        if ($checkRes->PAYMENT_TRANSACTION_STATUS == 216 && ($checkRes->APPROVE_BY_CASHFREE_STATUS == 1 &&  $checkRes->APPROVE_BY_PAYU_STATUS != 1)) { //checking Normal Approve (0) or approve via cashfree(1)
          $updatePaymentTrans = $this->commNormalWithdrawRejected($refNo, $checkRes->USER_ID);
        }
        $refNoArray[] = $refNo;
        $updatePaymentTransArray[] = $updatePaymentTrans;
      }
      if ($updatePaymentTransArray) {
        $data['Withdrwa Cashfree Reject Sucess'] = json_encode($refNoArray);
        $action = "Withdrwa Cashfree Reject Sucess";
        $this->insertAdminActivity($data, $action);
        //mail withdraw normal Reject params
        $userDetails = $this->getUserDetailsFromUserId($checkRes->USER_ID);
        $params['email'] = $userDetails->EMAIL_ID;
        $params['username'] = $userDetails->USERNAME;
        $params['tempId'] = 13;
        $params['subject'] = "Withdrawl Request Rejected";
        $params['refNo'] = $refNo;
        $params['amount'] = $checkRes->PAYMENT_TRANSACTION_AMOUNT;
        $params['date'] = date('d-m-Y');
        $params['time'] = date('H:i:s');
        $this->sendMail($params);
        return response()->json(['status' => 'success', 'type' => 'withdraw_Cashfree_reject', 'code' => 2040, 'refNoArray' => $refNoArray]);
      } else {
        $data['Withdrwa Cashfree Reject Failed'] = json_encode($refNoArray);
        $action = "Withdrwa Cashfree Reject Failed";
        $this->insertAdminActivity($data, $action);
        return response()->json(['status' => 'failed', 'type' => 'withdraw_Cashfree_reject', 'msg' => 'some thing went wrong with this payment  (' . $refNoArray . ') please try again later',  'code' => 2001, 'refNoArray' => $refNoArray]);
      }
    }
  }

  //payu approve button click
  public function withdrawPayuApprove(Request $request)
  {
    if (empty($request->all())) {
      return response()->json(['status' => 'failed', 'msg' => 'Please select atleast one row']);
    }
    if ($request->isMethod('post')) {
      if (empty($request->all()['tableData'])) {
        return response()->json(['status' => 'failed', 'type' => 'empty_selection', 'code' => 501]);
      }
      $request = $request->all()['tableData'];
      $sumOfPaidAmount = 0;
      foreach ($request as $values) {
        $refNoArrayForBalCheck[] = $values['refNo'];
      }
      $sumOfPaidAmount = $this->getPaidSumMasterTransactionPokerCommission($refNoArrayForBalCheck);
      //check available balance
      $avillavleBalance = $this->getPayuMerchantAvailableBalance();
      if ($sumOfPaidAmount > $avillavleBalance) {
        return response()->json([
          'status' => 'failed', 'type' => 'insufficient_balance',
          'msg' => 'Insufficient Wallet Balance. Your sum of selected amount should be less than  (' . $avillavleBalance . ')', 'code' => 3025
        ]);
      }
      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $transTypeId = ['10'];
        $witTrans = $this->getPaymentDetailsFromMasterTransaction($refNo, $transTypeId, $transStatusId = NULL, $userId = NULL);
        $witTransRes = $witTrans->first();
        $getPaymentInfo = $this->getPaymentDetailsFromPaymentTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);
        $checkPaymentStatusRes = $getPaymentInfo->first();
        $transaction_status_id = array(109, 216);
        $updatePaymentStatus = PaymentTransactionPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
          ->update(['APPROVE_BY_PAYU_STATUS' => 1]);

        if (!empty($witTransRes) && !empty($checkPaymentStatusRes) && $checkPaymentStatusRes->APPROVE_BY_CASHFREE_STATUS != 1  && $checkPaymentStatusRes->APPROVE_BY_PAYU_STATUS != 1 && in_array($checkPaymentStatusRes->PAYMENT_TRANSACTION_STATUS, $transaction_status_id)) { //if payment is initatiated
          $userId  =  $witTransRes->USER_ID;
          $userAccountDetails = $this->getUserAccountDetails($userId);
          $getUserDetails = $this->getUserDetailsFromUserId($userId);
          $userAccountNo = $userAccountDetails->ACCOUNT_NUMBER;
          $userIfscCode = $userAccountDetails->IFSC_CODE;
          $userFullName = $userAccountDetails->ACCOUNT_HOLDER_NAME;
          $userEmail = $getUserDetails->EMAIL_ID;
          $userMobile = $getUserDetails->CONTACT;

          if (!empty($checkPaymentStatusRes) && !empty($witTransRes)) {
            //  $paidamount = $witTransRes->PAID_AMOUNT;
            $paidamount = $witTransRes->TRANSACTION_AMOUNT;
          }

          $payuDataArray[] = [
            "beneficiaryAccountNumber" => $userAccountNo,
            "beneficiaryIfscCode" => $userIfscCode,
            "beneficiaryName" => $userFullName,
            "beneficiaryEmail" => $userEmail,
            "beneficiaryMobile" => $userMobile,
            "purpose" => "transfer request",
            "amount" => $paidamount,
            "batchId" => $witTransRes->USER_ID,
            "merchantRefId" => $refNo,
            "paymentType" => "IMPS"
          ];
          // $payuREquest = array('Payu Cashout Request' => $payuDataArray);
          $refNoArray[] = $refNo;
          $sumOfPaidAmount += $paidamount;
          $mailArraydata[] = [
            "email" => $userEmail,
            "username" => $getUserDetails->USERNAME,
            "refNo" => $refNo,
            "amount" => $paidamount
          ];
        } else {
          return response()->json(['status' => 'success', 'type' => 'payment_under_process', 'msg' => 'you cant`t approve this payment.Because this payment (' . $refNo . ') is under process ',  'code' => 3010, 'refNoArray' => $refNo]);
        }
      }
      $casOutPaymentUserData = json_encode($payuDataArray);
      $payuRes = $this->payuCashoutApi($casOutPaymentUserData);
      $gateWayJsonResponse = json_encode($payuRes);
      foreach ($payuDataArray as $payuDataArrayValue) {
        $gateWayjsonRequest = json_encode($payuDataArrayValue);
        $res = $this->insertCashoutApproveTransaction($payuDataArrayValue['batchId'], $payuDataArrayValue['merchantRefId'], $payuDataArrayValue['amount'], NULL, NULL, $payuDataArrayValue['beneficiaryName'], $payuDataArrayValue['beneficiaryAccountNumber'], $payuDataArrayValue['beneficiaryIfscCode'], $gateWayjsonRequest, $gateWayJsonResponse, $gateWayMessage = NULL, "payu", NULL);
      }
      $payuRes = json_decode($payuRes);
      //mail withdraw payu approve params
      foreach ($mailArraydata as $mailData) {
        $params['email'] = $mailData['email'];
        $params['username'] = $mailData['username'];
        $params['tempId'] = 12;
        $params['subject'] = "Withdrawl Request Approved";
        $params['refNo'] = $mailData['refNo'];
        $params['amount'] = $mailData['amount'];
        $params['date'] = date('d-m-Y');
        $params['time'] = date('H:i:s');
        $this->sendMail($params);
      }
      if ($payuRes->status == 0) {
        return response()->json(['status' => 'success', 'type' => 'withdraw_payu_approve', 'code' => 3000, 'refNoArray' => $refNoArray]);
      } else {
        return response()->json(['status' => 'failed', 'msg' => 'Something went wrong unable to approve , please try gain !', 'type' => 'withdraw_payu_approve', 'code' => 3001]);
      }
    }
  }

  //payu Reject button click
  public function withdrawPayuReject(Request $request)
  {
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];
      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $checkRes = $this->getPaymentDetailsFromPaymentTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);
        if ($checkRes->PAYMENT_TRANSACTION_STATUS == 216 && ($checkRes->APPROVE_BY_PAYU_STATUS == 1 &&  $checkRes->APPROVE_BY_CASHFREE_STATUS != 1)) { //checking Normal Approve (0) or approve via payu(1)
          $updatePaymentTrans = $this->commNormalWithdrawRejected($refNo, $checkRes->USER_ID);
        }
        $refNoArray[] = $refNo;
        $updatePaymentTransArray[] = $updatePaymentTrans;
      }
      if ($updatePaymentTransArray) {
        $data['Commission Withdrwa Payu Reject Sucess'] = json_encode($refNoArray);
        $action = "Commission Withdrwa Payu Reject Sucess";
        $this->insertAdminActivity($data, $action);
        //mail withdraw normal Reject params
        $userDetails = $this->getUserDetailsFromUserId($checkRes->USER_ID);
        $params['email'] = $userDetails->EMAIL_ID;
        $params['username'] = $userDetails->USERNAME;
        $params['tempId'] = 13;
        $params['subject'] = "Withdrawl Request Rejected";
        $params['refNo'] = $refNo;
        $params['amount'] = $checkRes->PAYMENT_TRANSACTION_AMOUNT;
        $params['date'] = date('d-m-Y');
        $params['time'] = date('H:i:s');
        $this->sendMail($params);
        return response()->json(['status' => 'success', 'type' => 'withdraw_Payu_reject', 'code' => 2020, 'refNoArray' => $refNoArray]);
      } else {
        $data['Withdrwa Payu Reject Failed'] = json_encode($refNoArray);
        $action = "Withdrwa Payu Reject Failed";
        $this->insertAdminActivity($data, $action);
        return response()->json(['status' => 'failed', 'type' => 'withdraw_Payu_reject', 'msg' => 'some thing went wrong with this payment  (' . $refNoArray . ') please try again later',  'code' => 2001, 'refNoArray' => $refNoArray]);
      }
    }
  }


  //passed button click
  public function withdrawPassed(Request $request)
  {
    if (empty($request->all())) {
      return response()->json(['status' => 'failed', 'msg' => 'Please select atleast one row']);
    }
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];
      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $checkingComment = $values['cs'] ?? "";
        $checkingStatus = 2;

        $updatePassStatus = PaymentTransactionPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
          ->update(['CHECKING_STATUS' => $checkingStatus, 'CHECKING_COMMENT' => $checkingComment]);
        $refNoArray[] = $refNo;
      }
      if ($updatePassStatus) {
        return response()->json(['status' => 'success', 'type' => 'update_pass_status_comment', 'code' => 7000, 'refNoArray' => $refNoArray]);
      } else {
        return response()->json(['status' => 'failed', 'type' => 'update_pass_status_comment', 'code' => 7001]);
      }
    }
  }


  //not passed button click
  public function withdrawNotPassed(Request $request)
  {
    if (empty($request->all())) {
      return response()->json(['status' => 'failed', 'msg' => 'Please select atleast one row']);
    }
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];

      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $checkingComment = $values['cs'] ?? "";
        $checkingStatus = 3;
        $updatePassStatus = PaymentTransactionPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
          ->update(['CHECKING_STATUS' => $checkingStatus, 'CHECKING_COMMENT' => $checkingComment]);
        $refNoArray[] = $refNo;
      }
      if ($updatePassStatus) {
        return response()->json(['status' => 'success', 'type' => 'update_pass_status_comment', 'code' => 8000, 'refNoArray' => $refNoArray]);
      } else {
        return response()->json(['status' => 'failed', 'type' => 'update_pass_status_comment', 'code' => 8001]);
      }
    }
  }


  //not passed button click
  public function withdrawUnchecked(Request $request)
  {
    if ($request->isMethod('post')) {
      $request = $request->all()['tableData'];

      foreach ($request as $values) {
        $refNo = $values['refNo'];
        $checkingComment = $values['cs'] ?? "";
        $checkingStatus = 1;

        $updatePassStatus = PaymentTransactionPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
          ->update(['CHECKING_STATUS' => $checkingStatus, 'CHECKING_COMMENT' => $checkingComment]);
        $refNoArray[] = $refNo;
      }
      if ($updatePassStatus) {
        return response()->json(['status' => 'success', 'type' => 'update_pass_status_comment', 'code' => 8000, 'refNoArray' => $refNoArray]);
      } else {
        return response()->json(['status' => 'failed', 'type' => 'update_pass_status_comment', 'code' => 8001]);
      }
    }
  }

  public function paymentApprove($refNo)
  {
    $paymentRes = $this->getPaymentDetailsFromPayTransactionPokerComm($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);
    $payRes = $paymentRes->first();
    if ($payRes->PAYMENT_TRANSACTION_STATUS == 109 && $payRes->APPROVE_BY_CASHFREE_STATUS != 1 && $payRes->APPROVE_BY_PAYU_STATUS != 1) { //checking Normal Approve (0) or approve via cashfree(1)
      $currentStatusId = 109;
      $paymentAndMsterAsSuccessStatus = $this->updateMasterAndPaymentStatusForSuccess($refNo, $currentStatusId); // for normal approve change status from pending to success and calculate tds and credit tds bonus
      return $payRes;
    } else {
      return response()->json(['status' => 'failed', 'type' => 'normal_approve_failed', 'code' => 9001]);
    }
  }

  public function commNormalWithdrawRejected($internalReferenceNo, $userID)
  {

    $transactionTypeID = 75;
    $transactionStatusID = 112;
    $createdOn = date('Y-m-d H:i:s');
    $internalRefNo = $internalReferenceNo;
    $payTransStatusId = ['109', '208', '216'];
    $payTranPokComm = $this->getPaymentDetailsFromPaymentTransaction($internalReferenceNo, $payTransTypeId = NULL, $payTransStatusId, $userID);
    if (!empty($payTranPokComm)) {
      //fetch user current wallet information
      $getAccountInfo = $this->getUsercurrentBalance($userID, 16);
      //update user points
      $transactionTypeID = 75;
      $newWBalance = $getAccountInfo->USER_WIN_BALANCE + $payTranPokComm->PAYMENT_TRANSACTION_AMOUNT;
      $newTBalance = $getAccountInfo->USER_TOT_BALANCE + $payTranPokComm->PAYMENT_TRANSACTION_AMOUNT;
      $updateUserPoints = UserPoint::where(['USER_ID' => $userID, 'COIN_TYPE_ID' => 16])
        ->update(['VALUE' => $newWBalance, 'USER_WIN_BALANCE' => $newWBalance, 'USER_TOT_BALANCE' => $newTBalance]);
      $curretnTotBalance = $getAccountInfo->USER_TOT_BALANCE;
      $newTotBalance = $curretnTotBalance + $payTranPokComm->PAYMENT_TRANSACTION_AMOUNT;
      $partner_id = 10001;
      //update status into payment transaction
      $updatePaymentTrans = PaymentTransactionPokercommission::where(['USER_ID' => $userID, 'INTERNAL_REFERENCE_NO' => $internalReferenceNo])
        ->update(['PAYMENT_TRANSACTION_STATUS' => $transactionStatusID]);
      $masterTransRes = $this->insertMasterTransTable(
        $userID,
        3,
        $transactionStatusID,
        $transactionTypeID,
        $payTranPokComm->PAYMENT_TRANSACTION_AMOUNT,
        $createdOn,
        $internalRefNo,
        $curretnTotBalance,
        $newTotBalance,
        $partner_id
      );
      if ($masterTransRes) {
        // Update Player Ledger for revert
        return $masterTransRes;
      }
    }
  }

  public function getPaymentDetailsFromPaymentTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL)
  {
    $res = PaymentTransactionPokercommission::select(
      'PAYMENT_TRANSACTION_STATUS',
      'TRANSACTION_TYPE_ID',
      'PAYMENT_TRANSACTION_AMOUNT',
      'USER_ID',
      'APPROVE_BY_CASHFREE_STATUS',
      'APPROVE_BY_PAYU_STATUS',
      'TRANSFER_ID',
      'PAYU_TRANSFER_ID'
    );
    $res->when(!empty($refNo), function ($res) use ($refNo) {
      return $res->where(['INTERNAL_REFERENCE_NO' => $refNo]);
    });
    $res->when(!empty($transTypeId), function ($res) use ($transTypeId) {
      return $res->whereIn('TRANSACTION_TYPE_ID', $transTypeId);
    });
    $res->when(!empty($transStatusId), function ($res) use ($transStatusId) {
      return $res->whereIn('PAYMENT_TRANSACTION_STATUS', $transStatusId);
    });
    $res->when(!empty($userId), function ($res) use ($userId) {
      return $res->where(['USER_ID' => $userId]);
    });
    return $res->first();
  }

  public function getPaymentDetailsFromMasterTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL)
  {
    $res = MasterTransactionHistoryPokercommission::select(
      'USER_ID',
      'INTERNAL_REFERENCE_NO',
      'TRANSACTION_STATUS_ID',
      'TRANSACTION_TYPE_ID',
      'TRANSACTION_AMOUNT',
      'BALANCE_TYPE_ID',
      'TRANSACTION_AMOUNT'
    );
    $res->when(!empty($refNo), function ($res) use ($refNo) {
      return $res->where(['INTERNAL_REFERENCE_NO' => $refNo]);
    });
    $res->when(!empty($transTypeId), function ($res) use ($transTypeId) {
      return $res->whereIn('TRANSACTION_TYPE_ID', $transTypeId);
    });
    $res->when(!empty($transStatusId), function ($res) use ($transStatusId) {
      return $res->whereIn('TRANSACTION_STATUS_ID', $transStatusId);
    });
    $res->when(!empty($userId), function ($res) use ($userId) {
      return $res->where(['USER_ID' => $userId]);
    });
    return $res;
    // print_r($res->getBindings());
    // dd($res->toSql());
  }

  public function getPaymentDetailsFromPayTransactionPokerComm($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL)
  {
    $res = PaymentTransactionPokercommission::select(
      'USER_ID',
      'INTERNAL_REFERENCE_NO',
      'PAYMENT_TRANSACTION_STATUS',
      'TRANSACTION_TYPE_ID',
      'PAYMENT_TRANSACTION_AMOUNT',
      'APPROVE_BY_CASHFREE_STATUS',
      'APPROVE_BY_PAYU_STATUS',
      'TRANSFER_ID',
      'PAYU_TRANSFER_ID',
      'APPROVE_TYPE'
    );
    $res->when(!empty($refNo), function ($res) use ($refNo) {
      return $res->where(['INTERNAL_REFERENCE_NO' => $refNo]);
    });
    $res->when(!empty($transTypeId), function ($res) use ($transTypeId) {
      return $res->whereIn('TRANSACTION_TYPE_ID', $transTypeId);
    });
    $res->when(!empty($transStatusId), function ($res) use ($transStatusId) {
      return $res->whereIn('TRANSACTION_STATUS_ID', $transStatusId);
    });
    $res->when(!empty($userId), function ($res) use ($userId) {
      return $res->where(['USER_ID' => $userId]);
    });
    return $res;
    // print_r($res->getBindings());
    // dd($res->toSql());
  }

  public function updateMasterAndPaymentStatusForSuccess($refNo, $currentStatusId)
  {
    $statusInfo = $this->getPaymentDetailsFromPaymentTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);
    $statusId = $statusInfo->PAYMENT_TRANSACTION_STATUS;
    if ($statusId == $currentStatusId) {
      $updateStatusOnmasterTrans = MasterTransactionHistoryPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
        ->update(['TRANSACTION_STATUS_ID' => 208]);
      if ($updateStatusOnmasterTrans) {
        $paymentTransStatus = PaymentTransactionPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
          ->update(['PAYMENT_TRANSACTION_STATUS' => 208]);
        return $paymentTransStatus;
      }
      return FALSE;
    }
    return FALSE;
  }
  
  //insert in master transaction table after approve
  public function insertMasterTransTable(
    $user_id,
    $balance_type_id,
    $trans_status_id,
    $trans_type_id,
    $tdsAmount,
    $trans_date,
    $ref_no,
    $current_bal,
    $closing_bal,
    $partner_id
  ) {
    // DB::enableQueryLog();
    $masterTransaction = new MasterTransactionHistoryPokercommission();
    $masterTransaction->USER_ID                = $user_id;
    $masterTransaction->BALANCE_TYPE_ID        = $balance_type_id;
    $masterTransaction->TRANSACTION_STATUS_ID  = $trans_status_id;
    $masterTransaction->TRANSACTION_TYPE_ID    = $trans_type_id;
    $masterTransaction->TRANSACTION_AMOUNT     = $tdsAmount;
    $masterTransaction->TRANSACTION_DATE       = $trans_date;
    $masterTransaction->INTERNAL_REFERENCE_NO  = $ref_no;
    $masterTransaction->CURRENT_TOT_BALANCE    = $current_bal;
    $masterTransaction->CLOSING_TOT_BALANCE    = $closing_bal;
    $masterTransaction->PARTNER_ID             = $partner_id;
    return $masterTransaction->save();
    // $queriesss = DB::getQueryLog();
    // dd($queriesss);
  }
  public  function getApproveRejectTransactionFromMaster($userID, $transTypeId, $transStatusId, $sdate, $edate)
  {
    # code...
    return $res = MasterTransactionHistoryPokercommission::select(
      'TRANSACTION_AMOUNT'
    )
      ->where(['USER_ID' => $userID, 'TRANSACTION_TYPE_ID' => $transTypeId, 'TRANSACTION_STATUS_ID' => $transStatusId])
      ->whereBetween('TRANSACTION_DATE', [$sdate, $edate])
      ->sum('TRANSACTION_AMOUNT');
  }
  public function getUserNameFromUserId($user_name)
  {
    return $user_id = User::select('USER_ID')->where('USERNAME', $user_name)->first()->USER_ID;
  }

  public function insertAdminActivity($data, $action)
  {

    $activity = [
      'admin_id' => \Auth::user()->id ?? null,
      'module_id' => '60',
      'action' => $action,
      'data' => json_encode($data)
    ];
    \PokerBaazi::storeActivity($activity);
  }

  public function withdrawExcelExport()
  {

    $username = Session::get('com_username');
    $ref_no =  Session::get('com_ref_no');
    $withdrawStatus =  Session::get('com_withdrawStatus');
    $transferId = Session::get('com_transferId');
    $transType = Session::get('com_approveType');
    $amount   = Session::get('com_amount');
    $dateFrom = Session::get('com_dateFrom');
    $dateTo   = Session::get('com_dateTo');
    $tds_Fee = Session::get('com_tds_Fee');
    $first_withdraw = Session::get('com_first_withdraw');
    $userFlag = Session::get('com_userFlag');
    $check_pass = Session::get('com_check_pass');
    $search_by = Session::get('com_search_by');
    // $user_id = null;
    // if (isset($username)) {
    //   $user_id = $this->getUserIDFromUsername($username, $search_by);
    // }
    $withDrawTransExpoData = $this->getAllWithdrawlData($username, $search_by, $ref_no, $amount, $dateFrom, $dateTo, $withdrawStatus, $transferId, $transType, $tds_Fee, $first_withdraw, $userFlag, $check_pass);
    $withDrawTransExpoData = $withDrawTransExpoData->get();
    // $headingofsheet = collect($payTransExpoDataP->first())->keys()->toArray();
    foreach ($withDrawTransExpoData as $withDrawTransExpoData) {
      $userAccountDetails = $this->getUserAccountDetails($withDrawTransExpoData->USER_ID);
      $data_array[] =
        array(
          'USERNAME' => $withDrawTransExpoData->USERNAME ?? "",
          'EMAIL' => $withDrawTransExpoData->EMAIL_ID ?? "",
          'CONTACT' => $withDrawTransExpoData->CONTACT ?? "",
          'VIA' =>  $withDrawTransExpoData->WITHDRAW_TYPE = 2 ? 'Online' : 'Cheque',
          'REQ_AMOUNT' => $withDrawTransExpoData->PAYMENT_TRANSACTION_AMOUNT ?? "",
          'TDS' => $withDrawTransExpoData->COM_TDS ?? "",
          'TYPE' => ($withDrawTransExpoData->APPROVE_BY_CASHFREE_STATUS == '1') ? "Cashfree" : (($withDrawTransExpoData->APPROVE_BY_PAYU_STATUS == '1')  ? "Payu" : "Normal"),
          'STATUS' => $withDrawTransExpoData->TRANSACTION_STATUS_DESCRIPTION ?? "",
          'TO_BE_PAID' => $withDrawTransExpoData->PAYMENT_TRANSACTION_AMOUNT - $withDrawTransExpoData->COM_TDS  ?? "",
          'REF_NO' => $withDrawTransExpoData->INTERNAL_REFERENCE_NO ?? "",
          'DATE' => $withDrawTransExpoData->TRANSACTION_DATE ?? "",
          'ACCOUNT_HOLDER_NAME' => $userAccountDetails->ACCOUNT_HOLDER_NAME ?? "",
          'ACCOUNT_NO' => $userAccountDetails->ACCOUNT_NUMBER ?? "",
          'IFSC_CODE' => $userAccountDetails->IFSC_CODE ?? "",
          'BANK_NAME' => $userAccountDetails->BANK_NAME ?? "",
          'BRANCH_NAME' => $userAccountDetails->BRANCH_NAME ?? "",
        );
    }
    // echo "<pre>";
    // print_r($data_array); exit;
    $headingofsheetArray = array('USERNAME', 'EMAIL', 'CONTACT', 'VIA', 'REQ_AMOUNT', 'TDS', 'TYPE', 'STATUS', 'TO_BE_PAID', 'REF_NO', 'DATE', 'ACCOUNT_HOLDER_NAME', 'ACCOUNT_NO', 'IFSC_CODE', 'BANK_NAME', 'BRANCH_NAME');
    $fileName = urlencode("Commission_Withdraw_report" . date('d-m-Y') . ".xlsx");
    if (!empty($data_array)) {
      return Excel::download(new CollectionExport($data_array, $headingofsheetArray), $fileName);
    } else {
      return view('bo.views.reports.withdraw');
    }
  }
  public function userFlagUpdate(Request $request)
  {
    if ($request->isMethod('post')) {
      $userId = $request->user_id;
      $userFlag = $request->flag_status;
      $res = $this->updateUserFlag($userId, $userFlag);
      if ($res) {
        echo $userFlag;
        exit;
      } else {
        echo "failed";
        exit;
      }
    }
  }

  public function payuCashoutApi($casOutPaymentUserData)
  {

    $token = $this->getPayuTokenForPaymentApi();
    if (!empty($token)) {
      $autorization = 'Bearer ' . $token;
      $PAYU_CASHOUT_URL = env('PAYU_CASHOUT_URL');
      $PAYU_CASHOUT_MERCHANT_ID = env('PAYU_CASHOUT_MERCHANT_ID');
      $PAYU_CASHOUT_ENDPOINT = "/payment";
      $PAYU_URL = $PAYU_CASHOUT_URL . $PAYU_CASHOUT_ENDPOINT;
      $ch = curl_init($PAYU_URL);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $casOutPaymentUserData);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "authorization:$autorization",
        "payoutMerchantId:$PAYU_CASHOUT_MERCHANT_ID",
        "Content-Type: application/json",
      ));
      $response = curl_exec($ch);
      $err = curl_error($ch);
      curl_close($ch);
      // $resData = json_decode($response);
      return $response;
    }
  }

  public function getPayuTokenForPaymentApi()
  {
    // $PAYU_CURL_URL = config('poker_config.payment.PAYU.payu_stat_url');
    $PAYU_CASHOUT_GET_TOKEN_URL = env('PAYU_CASHOUT_GET_TOKEN_URL');
    $PAYU_CASHOUT_CLIENT_ID = env('PAYU_CASHOUT_CLIENT_ID');
    $PAYU_CASHOUT_USERNAME = env('PAYU_CASHOUT_USERNAME');
    $PAYU_CASHOUT_PASSWORD = env('PAYU_CASHOUT_PASSWORD');
    $postFields = [
      'grant_type' => 'password',
      'scope' => 'create_payout_transactions',
      'client_id' => $PAYU_CASHOUT_CLIENT_ID,
      'username' => $PAYU_CASHOUT_USERNAME,
      'password' => $PAYU_CASHOUT_PASSWORD

    ];
    $headers = ['cache-control' => 'no-cache', 'Content-Type' => 'application/x-www-form-urlencoded'];


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $PAYU_CASHOUT_GET_TOKEN_URL);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);
    $resData = json_decode($response);
    return $resData->access_token;
  }
  //insert in payu_cashout_approve_transaction table after approve
  public function insertCashoutApproveTransaction($user_id, $refNo, $transAmount, $gateWayRefNo, $gateWayStatus, $accountName, $accountNumber, $IfscCode, $gateWayjsonRequest, $gateWayJsonResponse, $gateWayMessage, $gateWayType, $responseDate)
  {
    // DB::enableQueryLog();
    $PayuApproveTransaction = new CashoutApproveTransaction();
    $PayuApproveTransaction->USER_ID                = $user_id;
    $PayuApproveTransaction->INTERNAL_REFERENCE_NO  = $refNo;
    $PayuApproveTransaction->TRANSACTION_AMOUNT     = $transAmount;
    $PayuApproveTransaction->GATEWAY_REFERENCE_NO   = $gateWayRefNo;
    $PayuApproveTransaction->GATEWAY_STATUS         = $gateWayStatus;
    $PayuApproveTransaction->ACCOUNT_HOLDER_NAME    = $accountName;
    $PayuApproveTransaction->ACCOUNT_NUMBER         = $accountNumber;
    $PayuApproveTransaction->IFSC_CODE              = $IfscCode;
    $PayuApproveTransaction->GATEWAY_REQUEST        = $gateWayjsonRequest;
    $PayuApproveTransaction->GATEWAY_RESPONSE       = $gateWayJsonResponse;
    $PayuApproveTransaction->GATEWAY_MESSAGE        = $gateWayMessage;
    $PayuApproveTransaction->TYPE                   = $gateWayType;
    $PayuApproveTransaction->REQUEST_DATE           = date('Y-m-d H:i:s');
    $PayuApproveTransaction->RESPONSE_DATE          = $responseDate;
    return $PayuApproveTransaction->save();
  }


  public function getUserAccountDetails($userId)
  {
    return $userAccountDetails = UserAccountDetails::select('ACCOUNT_HOLDER_NAME', 'ACCOUNT_NUMBER', 'IFSC_CODE', 'BANK_NAME', 'BRANCH_NAME')
      ->where('USER_ID', $userId)
      ->first();
  }

  public function genRandomRefNo($outputLen)
  {
    $outputString = "";
    $inputString = "0123456789";
    for ($i = 0; $i < $outputLen; $i++) {
      $rnum = rand(0, 9);
      $outputString = $outputString . substr($inputString, $rnum, 1);
    }
    return $outputString;
  }
  //payu status api
  public function getLatestPayuStatus(Request $request)
  {
    $reqAll = $request->all();
    $refNo = $reqAll['ref_no'];
    $token = $this->getPayuTokenForPaymentApi();
    if (!empty($token)) {
      $autorization = 'Bearer ' . $token;
      $PAYU_CASHOUT_URL = env('PAYU_CASHOUT_URL');
      $PAYU_CASHOUT_STATUS_END_POINT = "/payment/listTransactions";
      $PAYU_CASHOUT_STATUS_URL = $PAYU_CASHOUT_URL . $PAYU_CASHOUT_STATUS_END_POINT;
      $PAYU_CASHOUT_MERCHANT_ID = env('PAYU_CASHOUT_MERCHANT_ID');

      $post_data = "merchantRefId=$refNo";

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $PAYU_CASHOUT_STATUS_URL);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', "payoutMerchantId:$PAYU_CASHOUT_MERCHANT_ID", "authorization:$autorization"));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $payuResponse = curl_exec($ch);
      $returnData = json_decode($payuResponse);
      if ($payuResponse != "" && $returnData->status == 0) {
        return response()->json([
          'txnId' => $returnData->data->transactionDetails[0]->txnId ?? "",
          'batchId' => $returnData->data->transactionDetails[0]->batchId ?? "",
          'merchantRefId' => $returnData->data->transactionDetails[0]->merchantRefId ?? "",
          'amount' => $returnData->data->transactionDetails[0]->amount ?? "",
          'txnStatus' => $returnData->data->transactionDetails[0]->txnStatus ?? "",
          'txnDate' => $returnData->data->transactionDetails[0]->txnDate ?? "",
          'payuTransactionRefNo' => $returnData->data->transactionDetails[0]->payuTransactionRefNo ?? "",
          'beneficiaryName' => $returnData->data->transactionDetails[0]->beneficiaryName ?? "",
          'msg' => $returnData->data->transactionDetails[0]->msg ?? "",
          'transferType' => $returnData->data->transactionDetails[0]->transferType ?? "",
          'nameWithBank' => $returnData->data->transactionDetails[0]->nameWithBank ?? "",
        ]);
      } else {
        return NULL;
      }
    }
  }

  //cashfree status api
  public function getLatestCashfreeStatus(Request $request)
  {
    $reqAll = $request->all();
    $refNo = $reqAll['ref_no'];
    $cashfreeRes =  $this->getPaymentDetailsFromPaymentTransaction($refNo, $transTypeId = NULL, $transStatusId = NULL, $userId = NULL);
    $CASHFREE_PAYOUT_BALANCE_CLIENT_ID = env('CASHFREE_PAYOUT_CLIENT_ID');
    $CASHFREE_PAYOUT_BALANCE_SECRET = env('CASHFREE_PAYOUT_CLIENT_SECRET');
    $CASHFREE_PAYOUT_STATUS_URL = env('CASHFREE_PAYOUT_PAYMENT_URL');
    $endpointAuth = $CASHFREE_PAYOUT_STATUS_URL . "/authorize";
    $headers = array(
      "X-Client-Id: $CASHFREE_PAYOUT_BALANCE_CLIENT_ID",
      "X-Client-Secret: $CASHFREE_PAYOUT_BALANCE_SECRET"
    );
    $cashfreeGetToken = $this->cashfreePayOutBatchTransferApi($endpointAuth, $headers, $params = []); //get cashfree token
    if ($cashfreeGetToken["status"] == "SUCCESS") { //authendication success
      $accessToken = $cashfreeGetToken["data"]["token"];
      $endpointAuth = $CASHFREE_PAYOUT_STATUS_URL . "/getTransferStatus?transferId=$cashfreeRes->TRANSFER_ID";
      $accessTokenheaders = array("Authorization: Bearer $accessToken");

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $endpointAuth);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $accessTokenheaders);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $returnData = curl_exec($ch);

      //$err = curl_error($ch);
      curl_close($ch);
      $cashFreeResponse = json_decode($returnData);
      if ($returnData != "" && $cashFreeResponse->status = "SUCCESS") {
        if (!empty($cashFreeResponse->data->transfer->acknowledged)) {
          $acknowledged = $cashFreeResponse->data->transfer->acknowledged;
        } else {
          $acknowledged = NULL;
        }
        return response()->json([
          // 'status' => $cashFreeResponse->status,
          'subCode' => $cashFreeResponse->subCode ?? "Sub Code not received",
          'message' => $cashFreeResponse->message ?? "Message not received",
          'bankAccount' => $cashFreeResponse->data->transfer->bankAccount ?? "Bank Account not received",
          'ifsc' => $cashFreeResponse->data->transfer->ifsc ?? "Ifsc not received",
          'beneId' => $cashFreeResponse->data->transfer->beneId ?? "Ben id not received",
          'transferId' => $cashFreeResponse->data->transfer->transferId ?? "Transfer id not received",
          'amount' => $cashFreeResponse->data->transfer->amount ?? "Amount not received",
          'status' => $cashFreeResponse->data->transfer->status ?? "Status not received",
          'utr' => $cashFreeResponse->data->transfer->utr ?? "Transfer utr not received",
          'addedOn' => $cashFreeResponse->data->transfer->addedOn ?? "Add on not received",
          'processedOn' => $cashFreeResponse->data->transfer->processedOn ?? "Transfer process on received",
          'acknowledged' => $acknowledged ?? "Webhook response not received",
          'transferMode' => $cashFreeResponse->data->transfer->transferMode ?? "Transfer Mode not received",
        ]);
      }
      return NULL;
    }
  }

  //payu balance check api
  public function getPayuMerchantAvailableBalance()
  {
    $token = $this->getPayuTokenForPaymentApi();
    if (!empty($token)) {
      $autorization = 'Bearer ' . $token;
      $PAYU_CASHOUT_URL = env('PAYU_CASHOUT_URL');
      $PAYU_CASHOUT_BALANCE_END_POINT = "/merchant/getAccountDetail";
      $PAYU_CASHOUT_STATUS_URL = $PAYU_CASHOUT_URL . $PAYU_CASHOUT_BALANCE_END_POINT;
      $PAYU_CASHOUT_MERCHANT_ID = env('PAYU_CASHOUT_MERCHANT_ID');
      $headers = array(
        "authorization:$autorization",
        "cache-control: no-cache'",
        "payoutMerchantId:$PAYU_CASHOUT_MERCHANT_ID"
      );
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $PAYU_CASHOUT_STATUS_URL);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $returnData = curl_exec($ch);
      $accountDetails = json_decode($returnData);
      return $accountDetails->data->balance;
    }
  }

  //get cashfree CASHFREE_REFERENCE_ID from cashfree table
  public function getCashfreeRefId($referenceNo)
  {
    $res = CashoutApproveTransaction::select(
      'CASHFREE_REFERENCE_ID',
      'INTERNAL_REFERENCE_NO',
      'USER_ID'
    );
    $res->where(['INTERNAL_REFERENCE_NO' => $referenceNo]);
    return $res->first();
  }
  // Function to update user flag green,yellow
  public function updateUserFlag($userId, $userFlag)
  {
    return $updateUserFlag = User::where(['USER_ID' => $userId,])
      ->update(['user_flag' => $userFlag]);
  }

  //get user win balance
  public function getUserWin(Request $request)
  {
    $reqAll = $request->all();
    $user_id_array = $reqAll['user_id'];
    foreach ($user_id_array as $user_id) {
      $userTotalDeposit = $this->getUserTotalDeposit($user_id);
      $userTotalWithdraw = $this->getUserTotalWithdraw($user_id);
      $totalWin = $userTotalDeposit - $userTotalWithdraw;
      $result[] = ['user_id' => $user_id, 'winAmount' => $totalWin];
    }
    return response()->json($result);
  }

  //get user first withdraw status
  public function getUserFirstWithdrawStatus(Request $request)
  {

    $reqAll = $request->all();
    $user_id_array = $reqAll['user_id'];
    foreach ($user_id_array as $user_id) {
      $statusCount = $this->checkUserFirstWithdrawStatus($user_id);
      if ($statusCount > 0) {
        $status = "No";
      } else {
        $status = "Yes";
      }
      $result[] = ['user_id' => $user_id, 'withStatus' => $status];
    }
    return response()->json($result);
  }

  public function checkUserFirstWithdrawStatus($user_id)
  {
    $now = date('Y-m-d H:i:s');
    $queryPayment = PaymentTransactionPokercommission::query();
    $queryPayment->from(app(PaymentTransactionPokercommission::class)->getTable() . " as pt");
    $queryPayment->join('user as u', 'u.USER_ID', '=', 'pt.USER_ID');
    $queryPayment->where('pt.USER_ID', $user_id);
    $queryPayment->where('pt.PAYMENT_TRANSACTION_STATUS', 208);
    $queryPayment->where('pt.TRANSACTION_TYPE_ID', 10);
    $queryPayment->whereBetween('pt.PAYMENT_TRANSACTION_CREATED_ON', ['u.REGISTRATION_TIMESTAMP', $now]);
    $queryPayment->orderBy('pt.PAYMENT_TRANSACTION_CREATED_ON', 'DESC')->select('u.USER_ID', 'u.REGISTRATION_TIMESTAMP');
    $queryPayment->select('u.USER_ID', 'u.REGISTRATION_TIMESTAMP');

    return $statusCount = $queryPayment->count();
  }
  // update withdraw transaction table after approve

  public function updateWithdrawTransactionTable($transStartus, $refNo, $userId, $tds, $fee, $payableAmount, $feeWaived, $paidAmount, $approvedBy, $gateWay, $gatewayStatus)
  {
    return $updatePaymentStatus = MasterTransactionHistoryPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo, 'USER_ID' => $userId])
      ->update([
        'TRANSACTION_STATUS_ID' => $transStartus,
        'WITHDRAW_TDS' => $tds,
        'WITHDRAW_FEE' => $fee,
        'PAYABLE_AMOUNT' => $payableAmount,
        'FEE_WAIVED' => $feeWaived,
        'PAID_AMOUNT' => $paidAmount,
        'APPROVED_BY' => $approvedBy,
        'APPROVE_TYPE' => $gateWay,
        'GATEWAY_STATUS' => $gatewayStatus,
      ]);
  }

  public function getPayuStausForManuallyHitWebhoo($refNo)
  {

    $token = $this->getPayuTokenForPaymentApi();
    if (!empty($token)) {
      $autorization = 'Bearer ' . $token;
      $PAYU_CASHOUT_URL = env('PAYU_CASHOUT_URL');
      $PAYU_CASHOUT_STATUS_END_POINT = "/payment/listTransactions";
      $PAYU_CASHOUT_STATUS_URL = $PAYU_CASHOUT_URL . $PAYU_CASHOUT_STATUS_END_POINT;
      $PAYU_CASHOUT_MERCHANT_ID = env('PAYU_CASHOUT_MERCHANT_ID');

      $post_data = "merchantRefId=$refNo";

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $PAYU_CASHOUT_STATUS_URL);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', "payoutMerchantId:$PAYU_CASHOUT_MERCHANT_ID", "authorization:$autorization"));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $payuResponse = curl_exec($ch);
      return $returnData = json_decode($payuResponse);
    }
  }
  public function getAllPayuInitiatedTransaction($approveType)
  {
    $now =  date('Y-m-d H:i:s');
    $time   = strtotime($now);
    $time   = $time - (60 * 60); //one hour
    $beforeOneHour = date("Y-m-d H:i:s", $time);
    $beforeOneWeek = date("Y-m-d H:i:s", strtotime("-1 week"));
    $manualWebhookLimit = env('MANUAL_WEBHOOK_HIT_LIMI');
    $iniRefNo = PaymentTransactionPokercommission::select('INTERNAL_REFERENCE_NO', 'USER_ID', 'PAYMENT_TRANSACTION_STATUS', 'APPROVE_BY_CASHFREE_STATUS', 'TRANSFER_ID', 'APPROVE_BY_PAYU_STATUS', 'PAYU_TRANSFER_ID');
    $iniRefNo->where('PAYMENT_TRANSACTION_STATUS', 109);
    // $iniRefNo ->where('APPROVE_TYPE', $approveType);
    $iniRefNo->when(!empty($approveType) && $approveType == "cashfree", function ($query) use ($approveType) {
      return $query->where('APPROVE_BY_CASHFREE_STATUS', 1);
    });
    $iniRefNo->when(!empty($approveType) && $approveType == "payu", function ($query) use ($approveType) {
      return $query->where('APPROVE_BY_PAYU_STATUS', 1);
    });
    $iniRefNo->whereBetween('PAYMENT_TRANSACTION_CREATED_ON', [$beforeOneWeek, $beforeOneHour]);
    $iniRefNo->limit($manualWebhookLimit);
    $iniRefNo->orderBy('PAYMENT_TRANSACTION_CREATED_ON', 'DESC');
    return $iniRefNo->get();
  }
  public function payuManuallyCommWebhookCall()
  {
    $approveType = "payu";
    $allInitiatedRefNo = $this->getAllPayuInitiatedTransaction($approveType);
    // echo "<pre>";
    // print_r($allInitiatedRefNo);exit;
    if (!empty($allInitiatedRefNo)) {
      foreach ($allInitiatedRefNo as $refNoData) {
        $refNo = $refNoData->INTERNAL_REFERENCE_NO;
        $payuStatusRes = $this->getPayuStausForManuallyHitWebhoo($refNo);
        if (!empty($payuStatusRes->data->transactionDetails[0]->merchantRefId)) {
          $refNoFromPayu = $payuStatusRes->data->transactionDetails[0]->merchantRefId;
          $payuStatus = $payuStatusRes->data->transactionDetails[0]->txnStatus;
          $txnId = $payuStatusRes->data->transactionDetails[0]->txnId ?? "";
          $amount = $payuStatusRes->data->transactionDetails[0]->amount ?? "";
          $txnDate = $payuStatusRes->data->transactionDetails[0]->txnDate ?? "";
          $payuTransactionRefNo = $payuStatusRes->data->transactionDetails[0]->payuTransactionRefNo ?? "";
          $beneficiaryName = $payuStatusRes->data->transactionDetails[0]->beneficiaryName ?? "";
          $msg = $payuStatusRes->data->transactionDetails[0]->msg ?? "";

          $gateWayJsonResponse = json_encode($payuStatusRes);
          $this->insertCashoutApproveTransaction($refNoData->USER_ID, $refNo, $amount, $payuTransactionRefNo, $msg, '', '', '', $gateWayjsonRequest = NULL, $gateWayJsonResponse, $msg, "payu", date('Y-m-d H:i:s'));

          if ($payuStatus == "SUCCESS" && $refNoData->PAYMENT_TRANSACTION_STATUS == 109 && $refNoData->APPROVE_BY_PAYU_STATUS == 1) { //payu cashout success
            //payu success
            $currentStatusId = $refNoData->PAYMENT_TRANSACTION_STATUS; //current status id as payment initiated
            //update PaymentTransaction status after getting success response fro payu
            $paymentAndMsterAsSuccessStatus = $this->updateMasterAndPaymentStatusForSuccess($refNo, $currentStatusId,); // for payu approve change status from pending to success and calculate tds and credit tds bonus

            if ($paymentAndMsterAsSuccessStatus) {
              $updateWithdrawStatus = PaymentTransactionPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
                ->update(['PAYU_TRANSFER_ID' => $payuTransactionRefNo, 'APPROVE_TYPE' => 'manually_webhoo']);
            }

            if (!empty($updateWithdrawStatus)) {
              $refNoArray[] = $refNo;
              $status = 'success';
              $msg = 'TRANSFER_SUCCESS';
              $code = 6000;
              // return response()->json(['status' => 'success', 'msg' => 'TRANSFER_SUCCESS', 'code' => 6000, 'refNoArray' => $refNoArray]);
            }
          }

          if (($payuStatus == "FAILED" || $payuStatus == "REQUEST_PROCESSING_FAILED") && $refNoData->PAYMENT_TRANSACTION_STATUS == 109) {   //payu cashout failed
            //payu failed
            //$error = $allRequest['errorCode'];
            //update PaymentTransaction status after getting success response fro payu
            $updatePaymentStatus = PaymentTransactionPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
              ->update(['PAYMENT_TRANSACTION_STATUS' => 216, 'PAYU_TRANSFER_ID' => $payuTransactionRefNo,]);
            //update PaymentTransaction status after getting success response fro payu
            if ($updatePaymentStatus) {
              $updateMsterStatus = MasterTransactionHistoryPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
                ->update(['TRANSACTION_STATUS_ID' => 216]);
            }

            $refNoArray[] = $refNo;
            $status = 'success';
            $msg = 'TRANSFER_FAILED_SUCCESS';
            $code = 6001;

            // return response()->json(['status' => 'success', 'msg' => 'TRANSFER_FAILED_SUCCESS', 'code' => 6001, 'refNoArray' => $refNoArray]);
          }
          if ($payuStatus == "TRANSFER_REVERSED" && $refNoData->PAYMENT_TRANSACTION_STATUS == 208) {
            $updatePaymentTrans = $this->commNormalWithdrawRejected($refNo, $refNoData->USER_ID);
            // return response()->json(['status' => 'success', 'msg' => 'TRANSFER_REVERSED_SUCCESS', 'code' => 6002, 'refNoArray' => $refNo]);
          } else {
            // return response()->json(['status' => 'failed', 'msg' => 'FAILED', 'code' => 6003, 'refNoArray' => $refNo]);
          }
        }
      }

      // return response()->json(['msg' => $msg, 'refNoArray' => $refNoArray]);
    }
  }
  public function getCashfreeStausForManuallyHitWebhoo($refNo, $cashfreeRefId)
  {
    $CASHFREE_PAYOUT_BALANCE_CLIENT_ID = env('CASHFREE_PAYOUT_CLIENT_ID');
    $CASHFREE_PAYOUT_BALANCE_SECRET = env('CASHFREE_PAYOUT_CLIENT_SECRET');
    $CASHFREE_PAYOUT_STATUS_URL = env('CASHFREE_PAYOUT_PAYMENT_URL');
    $endpointAuth = $CASHFREE_PAYOUT_STATUS_URL . "/authorize";
    $headers = array(
      "X-Client-Id: $CASHFREE_PAYOUT_BALANCE_CLIENT_ID",
      "X-Client-Secret: $CASHFREE_PAYOUT_BALANCE_SECRET"
    );
    $cashfreeGetToken = $this->cashfreePayOutBatchTransferApi($endpointAuth, $headers, $params = []); //get cashfree token
    if ($cashfreeGetToken["status"] == "SUCCESS") { //authendication success
      $accessToken = $cashfreeGetToken["data"]["token"];

      $endpointAuth = $CASHFREE_PAYOUT_STATUS_URL . "/getTransferStatus?transferId=$cashfreeRefId";
      $accessTokenheaders = array("Authorization: Bearer $accessToken");

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $endpointAuth);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $accessTokenheaders);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $returnData = curl_exec($ch);
      $err = curl_error($ch);
      curl_close($ch);
      return $cashFreeResponse = json_decode($returnData);
    }
  }
  public function cashfreeManuallyCommWebhookCall()
  {
    $approveType = "cashfree";
    $allInitiatedRefNo = $this->getAllPayuInitiatedTransaction($approveType);
    // print_r($allInitiatedRefNo);
    // exit;
    if (!empty($allInitiatedRefNo)) {
      foreach ($allInitiatedRefNo as $refNoData) {
        $refNo = $refNoData->INTERNAL_REFERENCE_NO;
        $cashfreeRefId = $refNoData->TRANSFER_ID;
        // $refNo = "13115997450200901122457854664832";
        // $cashfreeRefId = "144472";

        // $cashfreeRefId=GATEWAY_REFERENCE_NO
        $cashfreeStatus = $this->getCashfreeStausForManuallyHitWebhoo($refNo, $cashfreeRefId);


        if (!empty($cashfreeStatus->data->transfer->status)) {
          $gateWayJsonResponse = json_encode($cashfreeStatus);
          $event = $cashfreeStatus->data->transfer->status;
          $msg = $cashfreeStatus->message ?? "Message not received";
          // echo $event."--".$refNoData->TRANSACTION_STATUS_ID ;exit;
          $this->insertCashoutApproveTransaction($refNoData->USER_ID, $refNo, $refNoData->PAID_AMOUNT, $refNo, $event, '', '', '', NULL, $gateWayJsonResponse, $event, "cashfree", date('Y-m-d H:i:s'));
          if ($event == "SUCCESS" && $refNoData->TRANSACTION_STATUS_ID == 109) { //cashfree cashout success

            //cashfree success
            $currentStatusId = 109;
            //update PaymentTransaction status after getting success response fro cashfree
            // as wll update tds value if transaction contais tds
            //For TDS Calculation
            $paymentAndMsterAsSuccessStatus = $this->updateMasterAndPaymentStatusForSuccess($refNo, $currentStatusId); // for cashfree approve change status from pending to success and calculate tds and credit tds bonus
            //update withdraw transaction history status after getting success response from cashfree
            $updateWithdrawStatus = MasterTransactionHistoryPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
              ->update(['TRANSACTION_STATUS_ID' => 208, 'GATEWAY_REFERENCE_NO' => $cashfreeRefId, 'APPROVED_AT' => 'manually_webhoo']);
            // return response()->json(['status' => 'success', 'msg' => 'TRANSFER_SUCCESS', 'code' => 6000, 'refNoArray' => $refNo]);
          }
          if ($event == "FAILED" && $refNoData->TRANSACTION_STATUS_ID == 109) {   //cashfree cashout failed
            //update PaymentTransaction status after getting success response fro cashfree
            $updateCashfreeStatusInPayment = PaymentTransactionPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
              ->update(['PAYMENT_TRANSACTION_STATUS' => 216]);
            //update PaymentTransaction status after getting success response fro cashfree
            $updateCashfreeStatusInMaster = MasterTransactionHistoryPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
              ->update(['TRANSACTION_STATUS_ID' => 216]);
            //update withdraw transaction history status after getting success response fro cashfree

          }
          if ($event == "TRANSFER_REVERSED" && $refNoData->TRANSACTION_STATUS_ID == 208) {   //cashfree cashout reversed
            $updatePaymentTrans = $this->commNormalWithdrawRejected($refNo, $refNoData->USER_ID);
            // return response()->json(['status' => 'success', 'msg' => 'TRANSFER_REVERSED_SUCCESS', 'code' => 6002, 'refNoArray' => $refNo]);
          }
          // return response()->json(['status' => 'failed', 'msg' => 'FAILED', 'code' => 6003, 'refNoArray' => $refNo]);
        }
      }
    }
  }

  public function makeCashfreeFailedIfNotSuccess($refNo)
  {
    $updateMaster = MasterTransactionHistoryPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
      ->update(['TRANSACTION_STATUS_ID' => 216]);
    $updatePayment = PaymentTransactionPokercommission::where(['INTERNAL_REFERENCE_NO' => $refNo])
      ->update(['PAYMENT_TRANSACTION_STATUS' => 216]);
  }
  public function getPaymentDetailsFromWithdrawTransactionByCashfreeTransId($cashfreeTranId)
  {
    $res = MasterTransactionHistoryPokercommission::select(
      'USER_ID',
      'INTERNAL_REFERENCE_NO',
      'BALANCE_TYPE_ID',
      'TRANSACTION_STATUS_ID',
      'TRANSACTION_TYPE_ID',
      'WITHDRAW_AMOUNT',
      'WITHDRAW_TDS',
      'WITHDRAW_FEE',
      'PAYABLE_AMOUNT',
      'FEE_WAIVED',
      'PAID_AMOUNT',
      'APPROVED_BY',
      'APPROVE_TYPE',
      'GATEWAY_STATUS',
      'GATEWAY_REFERENCE_NO',
    );
    $res->where(['GATEWAY_REFERENCE_NO' => $cashfreeTranId]);
    $res->orderBy('UPDATED_ON', 'DESC');


    return $res;
    // print_r($res->getBindings());
    // print_r($res->toSql());exit;
  }
  public function getPaidSumMasterTransactionPokerCommission($refNoArray)
  {
    $res = MasterTransactionHistoryPokercommission::select('TRANSACTION_AMOUNT');
    $res->whereIn('INTERNAL_REFERENCE_NO', $refNoArray);
    $res->where('TRANSACTION_TYPE_ID', 10);
    return $res->sum('TRANSACTION_AMOUNT');
  }
}
