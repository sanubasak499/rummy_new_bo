<?php
namespace App\Http\Controllers\bo\reports;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use stdClass;
use DB;
use Carbon\Carbon;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MultiSheetExport;
use Maatwebsite\Excel\Readers\LaravelExcelReader;
use Validator;
use App;
use Mail;
use Storage;
class TournamentReportController extends Controller 
{
	public function index(Request $request){
		if($request->all()){
			
			Validator::make($request->all(), [
				'date_from'=> 'required|date',
				'date_to'=> 'required|date',
			])->validate();
			
			$enddate = Carbon::createFromDate($request->date_to);
			$startdate = Carbon::createFromDate($request->date_from);
			$one_week_date_diff = $enddate->diffInDays($startdate);
			if($one_week_date_diff <= 6){
				$date_from =Carbon::parse($request->date_from)->toDateTimeString();
				$date_to =Carbon::parse($request->date_to)->toDateTimeString();
				#first query
				$result = $this->getDataTour($date_from,$date_to);
				$tour_data =(json_decode(json_encode($result),true));
				
				$fileName = "tournament_report_{$request->date_from}_To_{$request->date_to}.xlsx";
				
				#second query
				$report2 = $this->usersList($date_from,$date_to);
				$user_List =(json_decode(json_encode($report2),true));
				
				if(( isset($tour_data) && !empty($tour_data) &&  count($tour_data) > 0) || ( isset($user_List) && !empty($user_List) &&  count($user_List) > 0)){
					$arrays = [$user_List, $tour_data];
					return Excel::download(new MultiSheetExport($arrays), $fileName);
				}else{
					return back()->with('error', "No tournament held in given date range");
				}
			}else{
				return back()->with('error', "Please select one week data");
			}
		}
		return view('bo.views.reports.tournament_report');
	}
	public function getDataTour($fromdate,$todate){
		
		return $result = DB::Connection('slave')->select("SELECT TOURNAMENT_NAME,TOURNAMENT_START_TIME,BUY_IN,REBUY_IN,ADDON,REGISTRATIONS_COUNT,REBUYS_COUNT,ADDONS_COUNT,CASH_TICKETS_COUNT,SATELLITE_TICKETS_COUNT,
		PROMO_TICKETS_COUNT,GTD_PRIZE,PRIZE_GIVEN,ENTRY_RECEIVED,
		IF(PRIZE_GIVEN>GTD_PRIZE,'P','N') AS 'PROFIT/LOSS',
		IF(PRIZE_GIVEN>GTD_PRIZE,((REBUYS_COUNT+CASH_TICKETS_COUNT+SATELLITE_TICKETS_COUNT+ADDONS_COUNT)*0.1*BUY_IN)-(PROMO_TICKETS_COUNT*BUY_IN),((REBUYS_COUNT+CASH_TICKETS_COUNT+SATELLITE_TICKETS_COUNT+ADDONS_COUNT)*0.1*BUY_IN)-(PROMO_TICKETS_COUNT*BUY_IN)-(PRIZE_GIVEN-(REGISTRATIONS_COUNT+REBUYS_COUNT)*REBUY_IN)) AS 'NET_PROFIT',
		BUY_IN*0.1*(REBUYS_COUNT+CASH_TICKETS_COUNT+SATELLITE_TICKETS_COUNT+ADDONS_COUNT) AS 'GROSS_RAKE',
		REBUYS_COUNT/REGISTRATIONS_COUNT AS 'REBUY/REGISTRATION',
		IF(PRIZE_GIVEN>GTD_PRIZE,PRIZE_GIVEN-GTD_PRIZE,(GTD_PRIZE-(BUY_IN*(REGISTRATIONS_COUNT+REBUYS_COUNT+ADDONS_COUNT)))*-1) AS 'EXTRA_AMOUNT_GIVEN',
		PROMO_TICKETS_COUNT*BUY_IN AS 'FREE_TICKET_AMOUNT',
		GTD_PRIZE/BUY_IN AS 'GTD_X',
		IF(PRIZE_GIVEN>GTD_PRIZE,PRIZE_GIVEN-GTD_PRIZE,(GTD_PRIZE-(BUY_IN*(REGISTRATIONS_COUNT+REBUYS_COUNT+ADDONS_COUNT)))*-1)/BUY_IN AS 'EXTRA_AMOUNT_X',
		TIME(TOURNAMENT_START_TIME) AS 'TIME',
		(SELECT COUNT(tut.TOURNAMENT_ID) AS CODE_TICKETS_COUNT FROM tournament_user_ticket tut
		WHERE tut.GENERATED_TICK_MODE = 9 AND tut.TOURNAMENT_ID =tournament_turnover_history.`TOURNAMENT_ID`) AS CODE_TICKETS_COUNT
		FROM tournament_turnover_history
		WHERE TOURNAMENT_START_TIME BETWEEN ? AND ? ",[$fromdate,$todate]);
	}
	public function usersList($fromdate,$todate){
		return $query = DB::connection('slave')->table('tournament_turnover_history as tth')
		->selectRaw('tth.TOURNAMENT_ID,TOURNAMENT_NAME,tlb.USER_ID,USERNAME,USER_RANK AS "POSITION",SUM_OF_PRIZE AS "AMOUNT_WON"')
		->join('tournament_leader_board as tlb', 'tth.TOURNAMENT_ID', '=', 'tlb.TOURNAMENT_ID')
		->join('user as u', 'tlb.USER_ID', '=', 'u.USER_ID')
		->whereBetween('tth.TOURNAMENT_START_TIME', [$fromdate, $todate])
		->get();
	}
}