<?php

namespace App\Http\Controllers\bo\invoice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \PDF;

class CreateInvoiceController extends Controller
{
    public function index(Request $request){

    	if($request->isMethod('POST')){
    		$customPaper = array(0,0,700,900);
    		$pdf = PDF::loadView('bo.views.invoice.invoice', ['data' => $request])->setPaper($customPaper,'potrait');

			return $pdf->stream('invoice.pdf');
    	}
    	else{
    		return view('bo.views.invoice.index');
    	}

    }
    
}
