<?php

namespace App\Http\Controllers\bo\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\bo\BaseController;
use App\Models\Admin;
use App\Models\Module;
use App\Imports\UserImport;
use Maatwebsite\Excel\Importer;

class WebApiController extends BaseController
{
    public function __construct(){
        parent::__construct();
    }
    
    public function isAdminUserNameExist(Request $request){
        try{
            if(!empty($request->username)){
                if($user = Admin::where('username',$request->username)->first()){
                    return response()->json(['status'=>200, 'message'=>'Exist'], 200);
                }else{
                    return response()->json(['status'=>201, 'message'=>'Not Exist'], 200);
                }
            }else{
                return response()->json(['status'=>404, 'message'=>'params missing'], 404);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>500, 'message'=>$e->getMessage()], 500);
        }
    }
    public function isAdminMobileExist(Request $request){
        try{
            if(!empty($request->mobile)){
                if($user = Admin::where('mobile',$request->mobile)->first()){
                    return response()->json(['status'=>200, 'message'=>'Exist'], 200);
                }else{
                    return response()->json(['status'=>201, 'message'=>'Not Exist'], 200);
                }
            }else{
                return response()->json(['status'=>404, 'message'=>'params missing'], 404);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>500, 'message'=>$e->getMessage()], 500);
        }
    }

    public function isAdminEmailExist(Request $request){
        try{
            if(!empty($request->email)){
                if($user = Admin::where('email',$request->email)->first()){
                    return response()->json(['status'=>200, 'message'=>'Exist'], 200);
                }else{
                    return response()->json(['status'=>201, 'message'=>'Not Exist'], 200);
                }
            }else{
                return response()->json(['status'=>404, 'message'=>'params missing'], 404);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>500, 'message'=>$e->getMessage()], 500);
        }
    }
    public function isModuleKeyExist(Request $request){
        try{
            if(!empty($request->module_key)){
                if($module = Module::where('module_key',$request->module_key)->first()){
                    if(!empty($request->id)){
                        if($module->id == $request->id){
                            return response()->json(['status'=>201, 'message'=>'Not Exist'], 200);    
                        }
                    }
                    return response()->json(['status'=>200, 'message'=>'Exist'], 200);
                }else{
                    return response()->json(['status'=>201, 'message'=>'Not Exist'], 200);
                }
            }else{
                return response()->json(['status'=>404, 'message'=>'params missing'], 404);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>500, 'message'=>$e->getMessage()], 500);
        }
    }

    public function excelToArray(request $request, Importer $importer){
        try{
            if($request->hasFile('excel_file')){
                $importer->import($data = new UserImport, $request->excel_file);
                return response()->json(['status'=>200, 'message'=>'success','data'=>(object)$data]);
            } else {
                return response()->json(['status'=>301, 'message'=>'File Not exist']);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>500, 'message'=>$e->getMessage()],500);
        }   
    }
}
