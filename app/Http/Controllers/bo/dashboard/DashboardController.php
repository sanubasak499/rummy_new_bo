<?php

namespace App\Http\Controllers\bo\dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\bo\BaseController;
use App\Admin;
use Auth;
use Hash;
use DB;

class DashboardController extends BaseController
{
    public function __contruct(){

    }

    public function index(){
        $data = Auth::user();
        return view('bo.views.dashboard.index');
    }

    
}
