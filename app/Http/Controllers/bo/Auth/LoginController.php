<?php

namespace App\Http\Controllers\bo\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Admin;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(){
        return view('bo.auth.login');
    }

    protected function credentials(Request $request) {
        
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL) ? 'EMAIL' : 'USERNAME';
        // Auth::loginUsingId(10869);
        
        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
        ];
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

    protected function authenticated(Request $request, $user){
        // $user->SESSION_ID = session()->getId();
        // $user->save();
    }
}
