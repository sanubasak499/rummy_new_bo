<?php
/*
    |--------------------------------------------------------------------------
    | Package Name: Player Search And User Profile
    | Class Name: AccountController
	| Author: Kamlesh Kumar
	| Purpose: Search All Player and Edit,Update and View User Profile
	| Callers: 
	| Created Date: Dec 20 2019
	| Modified Date: 
	| Modified By: 
	| Modified Details: 
	|--------------------------------------------------------------------------
*/

namespace App\Http\Controllers\bo\user\account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\bo\user\UserController;
use App\Models\User;
use App\Models\UserKyc;
use App\Models\CoinType;
use App\Models\ReferFriendTable;
use App\Models\Partner;
use App\Models\ResponsibleGameSetting;
use App\Models\MasterTransactionHistoryRewardcoins;
use App\Models\MasterTransactionHistoryRewardpoints;
use App\Models\RewardPointsConversionHistory;
use App\Models\RewardUserType;
use App\Models\DepositLimitUser;
use App\Models\UserPoint;
use App\Models\LevelConfig;
use App\Models\State;
use App\Models\MasterTransactionHistory;
use App\Models\PaymentTransaction;
use App\Models\PaymentTransactionPokercommission;
use App\Models\PromoCampaign;
use Carbon\Carbon;
use App\Models\Countries;
use App\Models\ActionReason;
use App\Models\UserAccountLogs;
use App\Traits\s3Bucket;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Exporter;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use DB;
use App\Facades\PokerBaazi;

class AccountController extends UserController
{
	use s3Bucket;

	protected $queryObj;

	public function index()
	{
		$state = self::getState();
		return view('bo.views.user.account.search', ['state' => $state]);
	}

	public function getState()
	{
		return $state	=	User::select('state')->distinct()->where('state', '!=', '')->orderBy('state', 'ASC')->get();
	}

	public function getCity(Request $request)
	{
		$getCity = User::Select($request->input('selectcond'))->distinct()->where($request->input('wherecond'), $request->input('getstate'))->where($request->input('selectcond'), '!=', '')->orderBy($request->input('selectcond'), 'ASC')->get();
		$usercity = array();
		if ($request->input('selectcond') == 'CITY')
			foreach ($getCity as $key => $value) {
				array_push($usercity, $value->CITY);
			}
		else {
			foreach ($getCity as $key => $value) {
				array_push($usercity, $value->PINCODE);
			}
		}
		return $usercity;
	}

	public function searchResult(Request $request)
	{
		\DB::enableQueryLog();

		$partner = Auth::user()->load('partner:PARTNER_ID,FK_PARTNER_TYPE_ID');

		$perPage = config('poker_config.paginate.per_page');

		if (!$request->isMethod('post')) {
			return view('bo.views.user.account.search');
		}
		$page = request()->page;

		$this->buildConditionalQuery($request);
		$statsQueryObj = clone $this->queryObj;

		$users = $this->filerDataObj($request)->paginate($perPage, ['*'], 'page', $page);

		$count = DB::select(
			$statsQueryObj->select(
				DB::raw("ifnull(ACCOUNT_STATUS,'total') as `status`"),
				DB::raw("count(ACCOUNT_STATUS) as `total`")
			)
				->groupBy(DB::raw('ACCOUNT_STATUS WITH ROLLUP'))->toSql(),
			$statsQueryObj->getBindings()
		);

		$params = $request->all();
		$state = self::getState();
		$count = collect($count)->keyBy('status');
		$params['page'] = $page;
		return view('bo.views.user.account.search', ['users' => $users, 'usercount' => $count, 'params' => $params, 'state' => $state]);
	}

	// Select User Profile Data
	public function userProfile($user_id)
	{
		$user_id = decrypt($user_id);

		$ReasonCategoryId = '35';
		$reasonsResult = ActionReason::whereRaw('FIND_IN_SET(35,REASON_CATEGORY_ID)', [$ReasonCategoryId])
			->get();

		$UserAccountLogsRes = UserAccountLogs::from(app(UserAccountLogs::class)->getTable() . " as ual")
			->select('ual.ACCOUNT_STATUS', 'ual.REASON_ID')
			->join('user as u', function ($query) use ($user_id) {
				return $query->on('u.user_id', '=', 'ual.user_id')
					->on('u.ACCOUNT_STATUS', "=", "ual.ACCOUNT_STATUS");
			})
			->where('ual.USER_ID', $user_id)
			->orderBy('ual.ACCOUNT_LOG_ID', 'DESC')
			->limit(1)
			->first();
			
		$users = User::select('USER_ID', 'PARTNER_ID', 'REFERRED_BY', 'USERNAME', 'USER_IMAGE', 'FIRSTNAME', 'LASTNAME', 'EMAIL_ID', 'CONTACT', 'GENDER', 'DATE_OF_BIRTH', 'ADDRESS', 'STREET', 'CITY', 'STATE', 'COUNTRY', 'PINCODE', 'USER_LAST_LOGIN', 'ACCOUNT_STATUS', 'PASSWORD_EXPIRED', 'TWO_STEP_VERIFICATION', 'player_info', 'REGISTRATION_TIMESTAMP')
			->with("user_kyc:USER_ID,PAN_NUMBER,AADHAAR_NUMBER,AADHAAR_FRONT_URL,AADHAAR_BACK_URL,NON_PAN_URL,AADHAAR_STATUS,PAN_STATUS,AADHAAR_COMMENT,PAN_COMMENT")
			->with(['user_points' => function ($query) {
				return $query->with('coin_type')->whereIn('COIN_TYPE_ID', [1, 2, 3, 11, 12, 16]);
				//return $query->with('coin_type')->whereIn('COIN_TYPE_ID', [1,2,3,11,16]);
			}])
			->with("user_account_details:USER_ID,ACCOUNT_HOLDER_NAME,ACCOUNT_NUMBER,MICR_CODE,IFSC_CODE,BANK_NAME,BRANCH_NAME")
			->with("responsible_game_setting:USER_ID,BREAK_ACTIVE")
			->where('USER_ID', $user_id)->first();

		$compaign_id = User::select('PROMO_CAMPAIGN_ID')->where('USER_ID', $user_id)->first();
		$reg_id = PromoCampaign::select('PROMO_CAMPAIGN_CODE')->where('PROMO_CAMPAIGN_ID', $compaign_id->PROMO_CAMPAIGN_ID)->first();
		$coin_types = CoinType::whereIn('COIN_TYPE_ID', [1, 3, 12, 16])->get();

		$user_total_balance_from_coin_type_16 = $users->user_points[3]->USER_TOT_BALANCE;

		//$query->whereBetween('age', [$ageFrom, $ageTo]);
		$user_level = LevelConfig::select('LEVEL_NAME')
			->where('MIN_LOYALTY_POINTS', '<=', $user_total_balance_from_coin_type_16)
			->where('MAX_LOYALTY_POINTS', '>=', $user_total_balance_from_coin_type_16)->first();

		$refered_by = ReferFriendTable::select('REF_BY_NAME')->where('REF_BY_ID', $users->REFERRED_BY)->first();
		$partner_name = Partner::select('PARTNER_NAME')->where('PARTNER_ID', $users->PARTNER_ID)->first();
		$state = State::get();
		$countries = Countries::get();

		return view('bo.views.user.account.userprofile', ['userprofile' => $users, 'reg_code' => $reg_id, 'countries' => $countries, 'state' => $state, 'level_name' => $user_level, 'coint_type' => $coin_types, 'referby' => $refered_by, 'partnername' => $partner_name, 'reasonsResult' => $reasonsResult, 'UserAccountLogsRes' => $UserAccountLogsRes]);
	}

	//update password expiry
	public function updatepasswordexpiry(Request $request)
	{
		$update_date['PASSWORD_EXPIRED'] = $request->password_expiry == 0 ? 1 : 0;
		$result = $this->commonUpdate($USER_ID = $request->user_id, $update_date);
		if ($result) {
			return response()->json(['status' => 200, 'message' => 'Update Successfully', 'title' => 'Congrats'], 200);
		} else {
			return response()->json(['status' => 201, 'message' => 'Updation failed', 'title' => 'Sorry'], 200);
		}
	}

	//update two step verification
	public function twostepverification(Request $request)
	{
		$update_date['TWO_STEP_VERIFICATION'] = $request->two_step_id == 0 ? 1 : 0;
		$result = $this->commonUpdate($USER_ID = $request->user_id, $update_date);
		if ($result) {
			return response()->json(['status' => 200, 'message' => 'Update Successfully', 'title' => 'Congrats'], 200);
		} else {
			return response()->json(['status' => 201, 'message' => 'Updation failed', 'title' => 'Sorry'], 200);
		}
	}

	//common update data user table
	public function commonUpdate($USER_ID, $update_data)
	{
		return User::where('USER_ID', $USER_ID)
			->update($update_data);
	}

	public function responsibleGaming(Request $request)
	{
		try {
			$USER_ID = $request->input('USER_ID');
			$tab_type = $request->input('tab_type');

			if ($tab_type == 'resp_gaming') {
				$responsibleGame = ResponsibleGameSetting::where('USER_ID', $USER_ID)->first();
				$responsibleGameDepositLimit = DepositLimitUser::where('USER_ID', $USER_ID)->first();
				return \Response::json(['status' => '200', 'message' => 'success', 'data' => $responsibleGame, 'depositlimit' => $responsibleGameDepositLimit]);
			} else if ($tab_type == 'baazi_rewards') {
				$accumulated_coins = MasterTransactionHistoryRewardcoins::where('USER_ID', $USER_ID)
					->where('TRANSACTION_TYPE_ID', 12)
					->sum('TRANSACTION_AMOUNT');

				$coins_balance = UserPoint::select('USER_TOT_BALANCE')
					->where('USER_ID', $USER_ID)
					->where('COIN_TYPE_ID', 11)
					->first();

				$accumulated_reward_points = MasterTransactionHistoryRewardpoints::where('USER_ID', $USER_ID)
					->where('TRANSACTION_TYPE_ID', 113)
					->sum('TRANSACTION_AMOUNT');

				$reward_points_spent = RewardPointsConversionHistory::where('USER_ID', $USER_ID)
					->sum('RP_CONVERTED');

				$reward_points_balance = UserPoint::select('USER_TOT_BALANCE')
					->where('USER_ID', $USER_ID)
					->where('COIN_TYPE_ID', 12)
					->first();

				$current_rewards_program = RewardUserType::select('REWARD_STATUS')
					->where('USER_ID', $USER_ID)
					->first();

				$baazi_rewards = array("accumulated_coins" => $accumulated_coins, "coins_balance" => $coins_balance, "accumulated_reward_points" => $accumulated_reward_points, "reward_points_spent" => $reward_points_spent, "reward_points_balance" => $reward_points_balance, "current_rewards_program" => $current_rewards_program);

				return \Response::json(['status' => '200', 'message' => 'success', 'data' => $baazi_rewards]);
			} else if ($tab_type == 'deposit_withdra_details') {
				$dipositwithdraw['deposit'] = self::getUserTotalDepositAmount($USER_ID);
				$dipositwithdraw['rcWithdraw'] = self::getUserTotalWithdrawAmount($USER_ID);
				$dipositwithdraw['commissionWithdraw'] = self::getTotalCommissionWithdrawAmount($USER_ID);
				$dipositwithdraw['dwresult'] = (($dipositwithdraw['deposit']) - ($dipositwithdraw['rcWithdraw'] + $dipositwithdraw['commissionWithdraw']));
				$dipositwithdraw['availableBalance'] = self::getUserAvailableAmount($USER_ID);
				return \Response::json(['status' => '200', 'message' => 'success', 'data' => $dipositwithdraw]);
			} else if ($tab_type == 'tax_details') {
				$taxdetailsData = array();

				$taxDetails = DB::table('master_transaction_history as t')
					->select('t.INTERNAL_REFERENCE_NO', 't.TRANSACTION_AMOUNT', 't.TRANSACTION_DATE', 't.TRANSACTION_STATUS_ID', 't.TRANSACTION_TYPE_ID', 'pt.PAYMENT_TRANSACTION_AMOUNT', 'pt.PAYMENT_TRANSACTION_STATUS', 't.USER_ID', DB::raw('YEAR(t.TRANSACTION_DATE) as FinalYear'), DB::raw('MONTH(t.TRANSACTION_DATE) as FinalMonth'))
					->join('payment_transaction as pt', 'pt.INTERNAL_REFERENCE_NO', '=', 't.INTERNAL_REFERENCE_NO')
					->where(['t.USER_ID' => $USER_ID])
					->where(['t.TRANSACTION_TYPE_ID' => '80'])
					->whereNotIn('pt.PAYMENT_TRANSACTION_STATUS', [203])
					->orderBy('t.TRANSACTION_DATE', 'desc')
					->get();
				$quartersPerYears = [];
				$taxDetails = $taxDetails->groupBy('FinalYear')->map(function ($group) use (&$quartersPerYears) {
					$total = ['apr_jun' => 0, 'july_sep' => 0, 'oct_dec' => 0, 'jan_mar' => 0];
					$finalYear = null;
					$group->map(function ($query) use (&$total, &$finalYear) {
						$finalYear = $query->FinalYear;
						if ($query->FinalMonth > 3 && $query->FinalMonth < 7) {
							$total['apr_jun'] += (int)$query->TRANSACTION_AMOUNT;
						} elseif ($query->FinalMonth > 6 && $query->FinalMonth < 10) {
							$total['july_sep'] += (int)$query->TRANSACTION_AMOUNT;
						} elseif ($query->FinalMonth > 9 && $query->FinalMonth < 1) {
							$total['oct_dec'] += (int)$query->TRANSACTION_AMOUNT;
						} else {
							$total['jan_mar'] += (int)$query->TRANSACTION_AMOUNT;
						}
					});
					$quartersPerYears[$finalYear]['apr_jun'] = $total['apr_jun'];
					$quartersPerYears[$finalYear]['july_sep'] = $total['july_sep'];
					$quartersPerYears[$finalYear]['oct_dec'] = $total['oct_dec'];
					$quartersPerYears[$finalYear]['jan_mar'] = $total['jan_mar'];
					return $group;
				});

				$mainData = [];
				$flag = true;
				foreach ($quartersPerYears as $key => $value) {
					$mainData[$key . '-' . ($key + 1)]['total'] = array_key_exists($key . '-' . ($key + 1), $mainData) ? $mainData[$key . '-' . ($key + 1)]['total'] : 0;

					$mainData[($key - 1) . '-' . $key]['total'] = array_key_exists(($key - 1) . '-' . $key, $mainData) ? $mainData[($key - 1) . '-' . $key]['total'] : 0;

					$mainData[$key . '-' . ($key + 1)]['quarter'] = [
						'apr_jun' => 0,
						'july_sep' => 0,
						'oct_dec' => 0,
						'jan_mar' => 0,
					];
					$mainData[($key - 1) . '-' . $key]['quarter'] = [
						'apr_jun' => 0,
						'july_sep' => 0,
						'oct_dec' => 0,
						'jan_mar' => 0,
					];

					$mainData[$key . '-' . ($key + 1)]['quarter']['apr_jun'] = $value['apr_jun'];
					$mainData[$key . '-' . ($key + 1)]['quarter']['july_sep'] = $value['july_sep'];
					$mainData[$key . '-' . ($key + 1)]['quarter']['oct_dec'] = $value['oct_dec'];
					$mainData[($key - 1) . '-' . $key]['quarter']['jan_mar'] = $value['jan_mar'];

					$mainData[$key . '-' . ($key + 1)]['total'] += $value['apr_jun'] + $value['july_sep'] + $value['oct_dec'];
					$mainData[($key - 1) . '-' . $key]['total'] += $value['jan_mar'];
				}
				return \Response::json(['status' => 200, 'message' => 'success', 'data' => $mainData], 200);
			} else if ($tab_type == 'reason_details') {


				$reasonResult = UserAccountLogs::from(app(UserAccountLogs::class)->getTable() . " as u")
					->select('u.USERNAME', 'u.ACCOUNT_STATUS', 'u.REASON_ID', 'u.UPDATED_BY', 'u.UPDATED_ON', 'a.ACTIONS_REASON')
					->join('action_reasons as a', 'a.ACTIONS_REASONS_ID', '=', 'u.REASON_ID')
					->where('u.USER_ID', $USER_ID)
					->orderBy('u.ACCOUNT_LOG_ID', 'DESC')
					->get();

				$reasonDetails = json_decode(json_encode($reasonResult), true);
				return \Response::json(['status' => '200', 'message' => 'success', 'data' => $reasonDetails]);
			}
		} catch (\Exception $e) {
			return \Response::json(['status' => 500, 'message' => $e->getMessage()], 500);
		}
	}

	public function getUserTotalDepositAmount($USER_ID)
	{
		return $totalDeposit = PaymentTransaction::where('USER_ID', $USER_ID)
			->whereIn('PAYMENT_TRANSACTION_STATUS', [103, 125])
			->whereIn('TRANSACTION_TYPE_ID', [8, 61, 62, 83, 111])
			->sum('PAYMENT_TRANSACTION_AMOUNT');
	}

	public function getUserTotalWithdrawAmount($USER_ID)
	{
		return $totalDeposit = PaymentTransaction::where('USER_ID', $USER_ID)
			->where('PAYMENT_TRANSACTION_STATUS', 208)
			->where('TRANSACTION_TYPE_ID', 10)
			->sum('PAYMENT_TRANSACTION_AMOUNT');
	}

	public function getTotalCommissionWithdrawAmount($USER_ID)
	{
		return $totalDeposit = PaymentTransactionPokercommission::where('USER_ID', $USER_ID)
			->where('PAYMENT_TRANSACTION_STATUS', 208)
			->where('TRANSACTION_TYPE_ID', 10)
			->sum('PAYMENT_TRANSACTION_AMOUNT');
	}

	public function getUserAvailableAmount($USER_ID)
	{
		return $totalDeposit = UserPoint::select('USER_TOT_BALANCE')
			->where('USER_ID', $USER_ID)
			->where('COIN_TYPE_ID', 1)
			->first();
	}

	public function updateuserprofile(Request $request)
	{

		//Update Player Info
		if ($request->input('upDatePlayerInfo')) {
			$validation = $request->validate([
				"player_info" => 'required'
			]);
			$edit_data['player_info'] =  $request->input('player_info');
			$update_result = self::editupdateData($request->input('USER_ID'), $edit_data);
			if ($update_result) {
				return redirect('user/account/detail/' . $request->input('USER_ID'))
					->with('success', 'Data Update successfully');
			} else {
				return redirect('user/account/detail/' . $request->input('USER_ID'))
					->with('error', 'Data updation failed please try again?');
			}
		}

		//Upload PAN Proof For Kyc
		if ($request->input('upload_pan')) {
			$validation = $request->validate([
				"PAN_NO" => 'required|size:10',
				"PAN_IMG"	=> 'required|mimes:jpeg,JPEG,jpg,png,PNG',
			]);
			if ($request->hasFile('PAN_IMG')) {
				$file = $request->file('PAN_IMG');
				$filePath = 'pan/';
				$hasFile = $request->hasFile('PAN_IMG');
				$allowedFormats = array("Jpeg", "jpeg", "JPEG", "JPG", "jpg", "png", "PNG");
				$fileUploadUrl = $this->s3FileUpload($file, $filePath, $allowedFormats, $hasFile);
				if ($fileUploadUrl) {
					$edit_data = [
						"PAN_NUMBER" => $request->input('PAN_NO'),
						"PAN_STATUS" => 1,
						"NON_PAN_URL" => $fileUploadUrl->url
					];
					$update_result = self::updateUserKycTable($USER_ID = $request->input('USER_ID'), $edit_data);
					if ($update_result) {
						return redirect('user/account/detail/' . $request->input('USER_ID'))
							->with('success', 'Data Update successfully');
					} else {
						return redirect('user/account/detail/' . $request->input('USER_ID'))
							->with('error', 'Data updation failed please try again?');
					}
				} else {
					return redirect('user/account/detail/' . $request->input('USER_ID'))
						->with('error', 'Data updation failed please try again?');
				}
			}
		}

		//Upload Address Proof For KYC
		if ($request->input('upload_address')) {
			$validation = $request->validate([
				"address_no" => 'required',
				"addressFront"	=> 'required|mimes:jpeg,JPEG,jpg,png,PNG',
				"addressBack"	=> 'required|mimes:jpeg,JPEG,jpg,png,PNG'
			]);
			if ($request->hasFile('addressFront') && $request->file('addressBack')) {
				$file1 = $request->file('addressFront');
				$file2 = $request->file('addressBack');
				$filePath1 = 'adhar/front/';
				$filePath2 = 'adhar/Back/';
				$hasFile1 = $request->hasFile('addressFront');
				$hasFile2 = $request->hasFile('addressBack');
				$allowedFormats = array("Jpeg", "jpeg", "JPEG", "JPG", "jpg", "png", "PNG");
				$fileUploadUrl1 = $this->s3FileUpload($file1, $filePath1, $allowedFormats, $hasFile1);
				$fileUploadUrl2 = $this->s3FileUpload($file2, $filePath2, $allowedFormats, $hasFile2);
				if ($fileUploadUrl1 && $fileUploadUrl2) {
					$edit_data = [
						"AADHAAR_NUMBER" => $request->input('address_no'),
						"AADHAAR_STATUS" => 1,
						"AADHAAR_FRONT_URL" => $fileUploadUrl1->url,
						"AADHAAR_BACK_URL" => $fileUploadUrl2->url
					];
					$update_result = self::updateUserKycTable($USER_ID = $request->input('USER_ID'), $edit_data);
					if ($update_result) {
						return redirect('user/account/detail/' . $request->input('USER_ID'))
							->with('success', 'Data Update successfully');
					} else {
						return redirect('user/account/detail/' . $request->input('USER_ID'))
							->with('error', 'Data updation failed please try again?');
					}
				} else {
					return redirect('user/account/detail/' . $request->input('USER_ID'))
						->with('error', 'Data updation failed please try again?');
				}
			}
		}

		//Update Pan Kyc and Aadhar Kyc Status
		if ($request->input('verifyPan')) {
			if ($request->input('kyc_type') == 'pan_kyc') {
				$validation = $request->validate([
					"panverify" => 'required',
					"comment"	=> 'Required'
				]);

				$edit_data = [
					"PAN_STATUS"	=> $request->input('panverify'),
					"PAN_COMMENT"	=> $request->input('comment')
				];
			}
			if ($request->input('kyc_type') == 'address_kyc') {
				$validation = $request->validate([
					"addressverify" => 'required',
					"comment"	=> 'Required'
				]);

				$edit_data = [
					"AADHAAR_STATUS"	=> $request->input('addressverify'),
					"AADHAAR_COMMENT"	=> $request->input('comment')
				];
			}
			$update_result = self::updateUserKycTable($request->input('USER_ID'), $edit_data);
			if ($update_result) {
				return redirect('user/account/detail/' . $request->input('USER_ID'))
					->with('success', 'Data Update successfully');
			} else {
				return redirect('user/account/detail/' . $request->input('USER_ID'))
					->with('error', 'Data updation failed please try again?');
			}
		}

		//Update User Account Status
		if ($request->input('updateAcStatus')) {


			$edit_data['ACCOUNT_STATUS'] = $request->input('changeAcStatus');

			$reasonData = array(
				"USER_ID" => $request->input('USER_ID'),
				"USERNAME" => $request->input('USERNAME'),
				"ACCOUNT_STATUS" => $request->input('changeAcStatus'),
				"REASON_ID"	=>	$request->input('reason'),
				"UPDATED_BY"	=> Auth::user()->username
			);

			$userAccountStatus = User::select('ACCOUNT_STATUS')->where('USER_ID', $request->input('USER_ID'))->first();
			
			if($userAccountStatus->ACCOUNT_STATUS != $request->input('changeAcStatus')){
				$update_result = self::editupdateData(encrypt($request->input('USER_ID')), $edit_data);
				if ($update_result) {
					$insetData = UserAccountLogs::insert($reasonData);
					// Start User Activity Tracking 
					$activity = [
						'admin' => Auth::user()->id,
						'module_id' => 51,
						'action' => "Add user account log",
						'data' => json_encode($reasonData)
					];
					PokerBaazi::storeActivity($activity);
					//End User Activity Tracking
					if ($insetData == 1) {
						return redirect('user/account/detail/' . encrypt($request->input('USER_ID')))
							->with('success', 'Status Updated Successfully');
					} else {
						return redirect('user/account/detail/' . encrypt($request->input('USER_ID')))
							->with('error', 'Data updation failed, Please try again');
					}
					return redirect('user/account/detail/' . encrypt($request->input('USER_ID')))
						->with('success', 'Status Updated Successfully');
				} else {
					return redirect('user/account/detail/' . encrypt($request->input('USER_ID')))
						->with('error', 'Data updation failed, Please try again');
				}

			}else{
				return redirect('user/account/detail/' . encrypt($request->input('USER_ID')))
						->with('error', 'You have already changed the status');
			}
			
		}

		// Update Contact info tab
		if ($request->input('Update')) {
			$validation = $request->validate([
				"FIRSTNAME" => 'required',
				"LASTNAME"	=> 'required',
				"STREET"	=> 'required',
				"EMAIl_ID"	=> 'required|email',
				"DATE_OF_BIRTH"	=> 'required',
				"CONTACT"	=> 'required|numeric|digits:10',
				"PINCODE" => 'required|numeric|digits:6',
			]);

			$edit_data = [
				'FIRSTNAME' =>	$request->input('FIRSTNAME'),
				'LASTNAME'	=>	$request->input('LASTNAME'),
				'EMAIl_ID'	=>	$request->input('EMAIl_ID'),
				'CONTACT'	=>	$request->input('CONTACT'),
				'DATE_OF_BIRTH'	=>	$request->input('DATE_OF_BIRTH'),
				'GENDER'	=>	$request->input('GENDER'),
				'COUNTRY'	=>	$request->input('COUNTRY'),
				'STATE'		=>	$request->input('STATE'),
				'CITY'		=>	$request->input('CITY'),
				'STREET'	=>	$request->input('STREET'),
				'ADDRESS'	=>	$request->input('ADDRESS'),
				'PINCODE'	=>	$request->input('PINCODE')
			];

			$update_result = self::editupdateData($request->input('USER_ID'), $edit_data);
			if ($update_result) {
				return redirect('user/account/detail/' . $request->input('USER_ID'))
					->with('success', 'Data Update successfully');
			} else {
				return redirect('user/account/detail/' . $request->input('USER_ID'))
					->with('error', 'Data updation failed please try again?');
			}
		}
	}

	// this function user for update user table data
	public function editupdateData($USER_ID, $edit_data)
	{
		return User::where('USER_ID', decrypt($USER_ID))
			->update($edit_data);
	}

	// this function update data in USER_KYC table
	public function updateUserKycTable($USER_ID, $edit_data)
	{
		$checkUserIdExist = UserKyc::select('USER_ID')->where('USER_ID', decrypt($USER_ID))->first();
		if ($checkUserIdExist) {
			return UserKyc::where('USER_ID', decrypt($USER_ID))
				->update($edit_data);
		} else {
			$insertData = $edit_data;
			$insertData['USER_ID'] = decrypt($USER_ID);
			$insertData['CREATED_DATE'] = date('Y-m-d H:i:s');
			return UserKyc::insert($insertData);
		}
	}

	public function buildConditionalQuery($request)
	{
		$this->queryObj = User::query();
		$this->queryObj->when($request->search_by == "username", function ($query) use ($request) {
			return $query->where('USERNAME', 'LIKE', "%{$request->search_by_value}%");
		})
			->when($request->search_by == "email", function ($query) use ($request) {
				return $query->where('EMAIL_ID', 'LIKE', "%{$request->search_by_value}%");
			})
			->when($request->search_by == "contact_no", function ($query) use ($request) {
				return $query->where('CONTACT', 'LIKE', "%{$request->search_by_value}%");
			})
			->when(!empty($request->date_from), function ($query) use ($request) {
				$date_from = Carbon::parse($request->date_from);
				return $query->whereDate('REGISTRATION_TIMESTAMP', ">=", $date_from);
			})
			->when(!empty($request->date_to), function ($query) use ($request) {
				$date_to = Carbon::parse($request->date_to);
				return $query->whereDate('REGISTRATION_TIMESTAMP', "<=", $date_to);
			})
			->when(!empty($request->status), function ($query) use ($request) {
				return $query->where('ACCOUNT_STATUS', $request->status);
			})
			->when(!empty($request->state), function ($query) use ($request) {
				return $query->where('STATE', $request->state);
			})
			->when(!empty($request->city), function ($query) use ($request) {
				return $query->where('CITY', $request->city);
			})
			->when(!empty($request->pincode), function ($query) use ($request) {
				$userpin = explode(',', $request->pincode);
				return $query->whereIn('PINCODE', $userpin);
			});
		return $this->queryObj;
	}

	public function filerDataObj($request)
	{
		return $this->queryObj
			->select('USER_ID', 'USERNAME', 'CONTACT', 'EMAIL_ID', 'FIRSTNAME', 'LASTNAME', 'PARTNER_ID', 'ACCOUNT_STATUS')
			->when(!empty($request->state) || !empty($request->city) || !empty($request->pincode), function ($query) use ($request) {
				return $query->addSelect('LOCALE', 'STREET', 'CITY', 'STATE', 'COUNTRY', 'PINCODE');
			})
			->with(['user_points' => function ($query) {
				return $query->whereIn('COIN_TYPE_ID', [1, 16]);
			}])
			->with("partner:PARTNER_ID,PARTNER_NAME")
			->with("responsible_game_setting:USER_ID,BREAK_ACTIVE");
	}

	public function excelexport(request $request)
	{
		$this->buildConditionalQuery($request);
		$arrayOfdata = $this->filerDataObj($request)->get();
		foreach ($arrayOfdata as $key => $item) {
			$item['PARTNER_NAME'] = $item->partner->PARTNER_NAME;
			$item['Real Money'] = $item->user_points->where('COIN_TYPE_ID', 1)->first()->USER_TOT_BALANCE ?? 0;
			$item['Commission'] = $item->user_points->where('COIN_TYPE_ID', 16)->first()->USER_TOT_BALANCE ?? 0;
			$item['ACCOUNT_STATUS'] = $item->ACCOUNT_STATUS == 0 ? 'Inactive' : ($item->ACCOUNT_STATUS == 1 ? 'Active' : 'Blocked');
			$item['POKER_BREAK'] = $item->responsible_game_setting['BREAK_ACTIVE'] == 1 ? 'Active' : 'Inactive';
			unset($item['user_points'], $item['partner'], $item['responsible_game_setting']);
		}
		$heading = collect($arrayOfdata->first()->toArray())->keys()->toArray();
		$headingofsheet = array_diff($heading, ['user_points', 'partner', 'responsible_game_setting']);
		$fileName = "PlayerSearch.xlsx";
		return Excel::download(new CollectionExport($arrayOfdata, $headingofsheet), $fileName);
	}
}
