<?php
/*
    |--------------------------------------------------------------------------
    | Package Name: Players/Player Login
    | Class Name: PlayerLoginController
	| Author: Dharminder Singh
	| Purpose: Track all user logins and common played games
	| Callers: 
	| Created Date: Dec 20 2019
	| Modified Date: JULY 02 2020
	| Modified By: Dharminder Singh
	| Modified Details: Connection changed to slave DB
	|--------------------------------------------------------------------------
*/

namespace App\Http\Controllers\bo\user\account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tracking;
use App\Models\GameTransactionHistory;
use App\Models\User;
use App\Models\PokerPlay;
use App\Models\TrackingStatus;
use Carbon\Carbon;
use DB;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Facades\Excel;

class PlayerLoginController extends Controller
{   
    public function index(Request $request){

		$responeData['tracking_status'] = TrackingStatus::all();

		if($request->isMethod('post'))
		{

			$validatedData = $request->validate([
								'date_from' => 'required',
								'date_to' => 'required',
							]);
			
			//get username by input
			if($request->search_by != "username"){
				$username = $this->getUsernameFromInput($request->username,$request->search_by);

				$request->request->add(['username' => $username]);
			}

			$postData[] = $request->all();
			$responeData['params'] = $request->all();
			$playerWithCommonIpRequest= $postData[0];
			$commonGamesUser = $postData[0];
			//get palyer logins details
			$responeData['playerLoginInfo'] = $this->getAllPlayerLoginInfo($postData[0]);
			
			if(!empty($request->username) || !empty($request->ip)){				
				//get matching users with same ips
				$responeData['playerWithCommonIp'] = $this->getAllUserWithMachingIps($playerWithCommonIpRequest);
				//get players who played common games
				$responeData['commonGamesPlayed'] = $this->commonGamesPlayed($commonGamesUser);

			}			
			return view('bo.views.user.account.player_login_search',$responeData);
		}
		else
		{
			return view('bo.views.user.account.player_login_search',$responeData);
		}        
	}
	
	public function getAllPlayerLoginInfo($request)
	{
        $tracking = DB::connection('slave')->table("tracking as t");

		$tracking->join('tracking_status as ts','t.STATUS','=', 'ts.TRACKING_STATUS_ID');

		if(!empty($request['username'])){
			$tracking->where('USERNAME',$request['username']);
		}
		if(!empty($request['ip'])){
			if(is_array($request['ip'])){
				$tracking->whereIn('SYSTEM_IP',$request['ip']);
			}else{
				$tracking->where('SYSTEM_IP',$request['ip']);
			}	
		}
		if(!empty($request['action_type'])){
			$tracking->where('ACTION_NAME',$request['action_type']);
		}
		if(!empty($request['device_type'])){
			$tracking->where('DEVICE_TYPE',$request['device_type']);
		}
		if(!empty($request['app_type'])){
			$tracking->where('APP_TYPE',$request['app_type']);
		}
		if(!empty($request['device_unique_id'])){
			$tracking->where('DEVICE_UNIQUE_ID',$request['device_unique_id']);
		}
		if(!empty($request['status'])){
			$tracking->where('STATUS',$request['status']);
		}
		if(!empty($request['device_model'])){
			$tracking->where('DEVICE_MODEL',$request['device_model']);
		}
		if(!empty($request['pack_version'])){
			$tracking->where('PACK_VERSION',$request['pack_version']);
		}
		if(!empty($request['build_version'])){
			$tracking->where('BUILD_VERSION',$request['build_version']);
		}
		if(!empty($request['browser_name'])){
			$tracking->where('BROWSER_NAME',$request['browser_name']);
		}
		if(!empty($request['operating_system'])){
			$tracking->where('OPERATING_SYSTEM',$request['operating_system']);
		}
		if(!empty($request['screen_size'])){
			$tracking->where('SCREEN_SIZE',$request['screen_size']);
		}
		if(!empty($request['fingerprint'])){
			$tracking->where('FINGERPRINT_ID',$request['fingerprint']);
		}
		if(!empty($request['date_from'])){
			$tracking->where('DATE_TIME', ">=",Carbon::parse($request['date_from'])->format('Y-m-d H:i:s'));
		}
		if(!empty($request['date_to'])){
			$tracking->where('DATE_TIME', "<=",Carbon::parse($request['date_to'])->format('Y-m-d H:i:s'));
		}

		$tracking->select('t.*','ts.*');
		$tracking->orderBy('DATE_TIME','DESC');
		return $data = $tracking->get();		
	}

	public function getAllUserWithMachingIps($playerWithCommonIpRequest)
	{
		// get players with common ip
		if(!empty($playerWithCommonIpRequest['username']))
		{					
			$ips = $this->getAllUsersIpInformation($playerWithCommonIpRequest['username']);

			if(!empty($ips)){
				foreach($ips as $val){
					$ipList[]=$val->SYSTEM_IP;
				}
			}
		}
		$ipList = array();
		if(!empty($playerWithCommonIpRequest['ip'])){
			$playerWithCommonIpRequest['ip'] = array_merge(array($playerWithCommonIpRequest['ip']),$ipList);
		}else{
			$playerWithCommonIpRequest['ip'] = $ipList;		
		}	
		$playerWithCommonIpRequest['username'] = "";				
		return $data = $this->getAllPlayerLoginInfo($playerWithCommonIpRequest);
	}

	public function commonGamesPlayed($commonGamesUser)
	{		
		if(!empty($commonGamesUser['username'])  && empty($commonGamesUser['ip'])){				
			$commonGamesUser['ip'] = "";
			$ipInfo = $this->getSystemIPInfo($commonGamesUser);
			if(!empty($ipInfo)){
				foreach($ipInfo as $systemIPInfo){
					$allPlayTrackingIDs[] = $systemIPInfo->SYSTEM_IP;
				}					
				if(!empty($allPlayTrackingIDs)){
					$commonGamesUser['ip'] = $allPlayTrackingIDs;
					
					$info1 = $this->getSystemIPInfo($commonGamesUser);
					if(!empty($info1)) {
						foreach($info1 as $pIndex=>$systemIPInfo) {
							$allPlayUser[] = $systemIPInfo->USERNAME;
						}
					}
					if(!empty($allPlayUser)){
						$commonGamesUser['username'] = $allPlayUser;
						$commonGamesUser['ip'] = "";
						$playGroupInfo = $this->getAllUsersPlayedInfo($commonGamesUser);

						return $data = $this->commonGamesArray($playGroupInfo);

					}
				}					
			}

		}elseif(empty($commonGamesUser['username']) && !empty($commonGamesUser['ip'])){
		
			$commonGamesUser['username'] = "";
			$info1 = $this->getSystemIPInfo($commonGamesUser);
			if(!empty($info1)) {
				foreach($info1 as $pIndex=>$systemIPInfo) {
					$allPlayUser[] = $systemIPInfo->USERNAME;
				}
				if(!empty($allPlayUser)){
					$commonGamesUser['username'] = $allPlayUser;
					$commonGamesUser['ip'] = "";
					$playGroupInfo = $this->getAllUsersPlayedInfo($commonGamesUser);					
					return $data  = $this->commonGamesArray($playGroupInfo);
				}
			}
			
		}elseif(!empty($commonGamesUser['username']) && !empty($commonGamesUser['ip'])){
			$info = $this->getSystemIPInfo($commonGamesUser);
			if(!empty($info)) {
				$commonGamesUser['username'] = '';
				$info1 = $this->getSystemIPInfo($commonGamesUser);
				foreach($info1 as $pIndex=>$systemIPInfo) {
					$allPlayUser[] = $systemIPInfo->USERNAME;
				}
				if(!empty($allPlayUser)){
					$commonGamesUser['username']=  $allPlayUser;
					$commonGamesUser['ip'] ='';
					$playGroupInfo = $this->getAllUsersPlayedInfo($commonGamesUser);
					return $data  = $this->commonGamesArray($playGroupInfo);
				}
			}
		}
	}

	public function getAllUsersIpInformation($username)
	{
		 return $data = DB::connection('slave')->table('tracking')->select('SYSTEM_IP')->distinct()
		->when(!empty($username), function($query) use ($username){
			return $query->where('USERNAME',$username);
		})
		->orderBy('SYSTEM_IP','DESC')
		->get();
	}

	public function getSystemIPInfo($data)
	{
		$ipInfo = DB::connection('slave')->table("tracking");		 
		$ipInfo->select('USERNAME','ACTION_NAME','DATE_TIME','SYSTEM_IP');
		if(!empty($data['username'])){
			if(is_array($data['username'])){
				$ipInfo->whereIn('USERNAME',$data['username']);
			}else{
				$ipInfo->where('USERNAME',$data['username']);
			}	
		}
		if(!empty($data['ip'])){
			if(is_array($data['ip'])){
				$ipInfo->whereIn('SYSTEM_IP',$data['ip']);
			}else{
				$ipInfo->where('SYSTEM_IP',$data['ip']);
			}	
		}
		if(!empty($data['date_from'])){
			$ipInfo->where('DATE_TIME', ">=",Carbon::parse($data['date_from'])->format('Y-m-d H:i:s'));
		}
		if(!empty($data['date_to'])){
			$ipInfo->where('DATE_TIME', "<=",Carbon::parse($data['date_to'])->format('Y-m-d H:i:s'));
		}
		return $data = $ipInfo->get();
	}

	public function getAllUsersPlayedInfo($data)
	{
        $usersPlayedGames = DB::connection('slave')->table("game_transaction_history as gt");
	
		$usersPlayedGames->join('user as u','u.user_id','=', 'gt.user_id');
		
		if(!empty($data['username'])){
			$usersPlayedGames->whereIn('u.USERNAME',$data['username']);
		}
		if(!empty($data['date_from'])){
			$usersPlayedGames->where('gt.STARTED', ">=",Carbon::parse($data['date_from'])->format('Y-m-d H:i:s'));
		}
		if(!empty($data['date_from'])){
			$usersPlayedGames->where('gt.STARTED', "<=",Carbon::parse($data['date_to'])->format('Y-m-d H:i:s'));
		}	
		$usersPlayedGames->select('gt.PLAY_GROUP_ID','gt.MINIGAMES_TYPE_NAME','gt.STAKE','gt.POT','gt.REVENUE','gt.REAL_REVENUE','gt.BONUS_REVENUE','gt.ACTUAL_REVENUE','gt.SERVICE_TAX');

		$usersPlayedGames->groupBy('gt.PLAY_GROUP_ID','gt.MINIGAMES_TYPE_NAME','gt.STAKE','gt.POT','gt.REVENUE','gt.REAL_REVENUE','gt.BONUS_REVENUE','gt.ACTUAL_REVENUE','gt.SERVICE_TAX');

		return $result = $usersPlayedGames->get();
		
	}

	public function commonGamesArray($playGroupInfo,$flag=false)
	{
		$i = 0;
		$resultset = array();
		foreach( $playGroupInfo as $row ){
			$data['PLAY_GROUP_ID']=$row->PLAY_GROUP_ID;
				$data['username']='';
				$userInfo = $this->getAllPokerPlayedInfo($data);
				$usernameList = array();
				$k=1;
				foreach($userInfo as $user){
						$break="";
						if (($k % 2) == 0) {
							$break="</br>";
						}
						if($flag){
							$usernameList[]= $user->USERNAME;
						}else{
							$usernameList[]=$user->USERNAME;;
						}
						$k++;	
				}
				$username = (!empty($usernameList)?implode(' ',$usernameList):'');
				$resultset[$i]=array(
									'TableId'=>$row->MINIGAMES_TYPE_NAME,
									'PLAY_GROUP_ID'=>$row->PLAY_GROUP_ID,
									'STAKE'=>$row->STAKE,
									'POT'=>$row->POT,
									'REVENUE'=>$row->REVENUE,
									'REAL_REVENUE'=>$row->REAL_REVENUE,
									'BONUS_REVENUE'=>$row->BONUS_REVENUE,
									'ACTUAL_REVENUE'=>$row->ACTUAL_REVENUE,
									'GST'=>$row->SERVICE_TAX,
									'USERNAME'=>$username
									);
				$i++;
		}	
		return $resultset;
	}

	public function getAllPokerPlayedInfo($data)
	{
		$getAllPokerPlayed = DB::connection('slave')->table('poker_play as pp');
	
		$getAllPokerPlayed->join('user as u','u.user_id','=', 'pp.user_id');	
		
		if(!empty($data['username'])){
			$getAllPokerPlayed->whereIn('u.USERNAME',$data['username']);
		}		
		if(!empty($data['PLAY_GROUP_ID'])){
			$getAllPokerPlayed->where('pp.PLAY_GROUP_ID',$data['PLAY_GROUP_ID']);
		}
		if(!empty($data['date_to'])){
			$getAllPokerPlayed->whereBetween('pp.ENDED',[Carbon::parse($data['date_from'])->format('Y-m-d H:i:s'),Carbon::parse($data['date_to'])->format('Y-m-d H:i:s')]);
		}
		$getAllPokerPlayed->select('u.USERNAME','pp.PLAY_GROUP_ID','pp.MINIGAMES_TYPE_NAME','pp.STAKE','pp.POT_AMOUNT','pp.USER_ID','pp.INTERNAL_REFERENCE_NO','pp.GAME_REFERENCE_NO');
		return $data = $getAllPokerPlayed->get();		
	}

	//excel export for player login search starts

	public function export(Request $request)
	{
		if($request->isMethod('get'))
		{			
			$validatedData = $request->validate([
								'date_from' => 'required',
								'date_to' => 'required',
							]);
			
			$postData[] = $request->all();
			$playerWithCommonIpRequest = $postData[0];
			$commonGamesUser = $postData[0];
			
			//get palyer logins details
			if($postData[0]['type'] == "playerLoginInfo"){
				$excelExport = $this->getAllPlayerLoginInfo($postData[0]);
				$name = "playerLoginInfo";

			}elseif(!empty($request->username) || !empty($request->ip)){
				if($postData[0]['type'] == "playerWithCommonIp"){			
					//get matching users with same ips
					$excelExport = $this->getAllUserWithMachingIps($playerWithCommonIpRequest);
					$name = "playerWithCommonIp";
				}
				if($postData[0]['type'] == "commonGamesPlayed"){
					//get players who played common games
					$excelExport = $this->commonGamesPlayed($commonGamesUser);
					$name = "commonGamesPlayed";
				}

			}		
			if(!is_array($excelExport)){
				$data = json_decode(json_encode($excelExport), true); 
			}else{
				$data = $excelExport;
			}
			if($name != "commonGamesPlayed")
			{
				for($i=0; $i < count($data);$i++){
					unset($data[$i]['TRACKING_ID']);
					unset($data[$i]['REFERRENCE_NO']);
					unset($data[$i]['SYSTEM_MAC']);
					unset($data[$i]['STATUS']);
					unset($data[$i]['LOGIN_STATUS']);
					unset($data[$i]['UPDATED_DATE']);
					unset($data[$i]['TRACKING_STATUS_ID']);
					// unset($data[$i]['BUILD_VERSION']);
					
					switch ($data[$i]['DEVICE_TYPE']) {
						case 1:
							$data[$i]['DEVICE_TYPE'] = "Desktop";
							break;
						case 2:
							$data[$i]['DEVICE_TYPE'] = "Mobile";
							break;
						default:
							$data[$i]['DEVICE_TYPE'] = "Unknown/Websites";
					}

					switch ($data[$i]['APP_TYPE']) {
						case 1:
							$data[$i]['APP_TYPE'] = "PC Instant Play";
							break;
						case 2:
							$data[$i]['APP_TYPE'] = "EXE";
							break;
						case 3:
							$data[$i]['APP_TYPE'] = "DMG";
							break;
						case 4:
							$data[$i]['APP_TYPE'] = "Mobile Instant Play";
							break;
						case 5:
							$data[$i]['APP_TYPE'] = "APK";
							break;
						case 6:
							$data[$i]['APP_TYPE'] = "IPA";
							break;
						case 7:
							$data[$i]['APP_TYPE'] = "Website";
							break;
						default:
							$data[$i]['APP_TYPE'] = "Unknown/Websites";
					}	
					
					
				}

			}

			$fileName = $name.".xlsx";
			$excelData = $data;
			$headings = array_keys($data[0]);
			return Excel::download(new CollectionExport($excelData,$headings), $fileName);
			
		}
		else
		{
			return view('bo.views.user.account.player_login_search');
		}        
	}	
	//excel export for player login search ends

	//get username by userId starts

	public function getUsernameFromInput($userinput, $search_by)
    {
		// echo $userinput.$search_by;exit;
        return $username = User::select('USERNAME')
            ->when($search_by == "email", function ($query) use ($userinput) {
                return $query->where('EMAIL_ID', $userinput);
            })
            ->when($search_by == "contact_no", function ($query) use ($userinput) {
                return $query->where('CONTACT', $userinput);
            })
            // ->where('USERNAME', $user_name)
            ->first()->USERNAME ?? null;
    }
	//get username by userId ends

}