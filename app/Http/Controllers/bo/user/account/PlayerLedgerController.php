<?php
/*
    |--------------------------------------------------------------------------
    | Package Name: Re- Correct Player Ledger
    | Class Name: PlayerLedgerController
	| Author: Dharminder Singh
	| Purpose: Update Player Ledger of User
	| Callers: 
	| Created Date: June 06 2020
	| Modified Date: 
	| Modified By: 
	| Modified Details: 
	|--------------------------------------------------------------------------
*/

namespace App\Http\Controllers\bo\user\account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\bo\user\UserController;
use App\Models\PlayerLedger;
use App\Models\PaymentTransaction;
use Carbon\Carbon;
use Auth;
use DB;
Use Session;

class PlayerLedgerController extends UserController
{
    
   	// Function to get update player ledger table on a successfull deposit or withdrawal
    public function index(Request $request)
    {
        
        $userId = $request->input('user_id');

        //get current financial year
        $nextYear = ( date('m') > 3) ? date('Y') + 1 : date('Y');        
        $currentYear = ( date('m') < 3) ? date('Y') - 1 : date('Y');

        $fYearStart = $currentYear."-04-01 00:00:00";
        $fYearEnd = $nextYear."-03-31 23:59:00";
        
        $pendingTransaction = PaymentTransaction::where('USER_ID',$userId)->whereIn('PAYMENT_TRANSACTION_STATUS',[109,254])->whereBetween('PAYMENT_TRANSACTION_CREATED_ON',[$fYearStart,$fYearEnd])->count();

        if($pendingTransaction == 0){
        
            if(!empty($userId)) {
                //Get previous player ledger data excluding Ledger Start
                $previousLedgerExcludingLedgerStartResult = $this->previousLedgerIncludingLedgerStart($userId);

                // 1. Deleting all player ledger entries
                foreach($previousLedgerExcludingLedgerStartResult as $previousLedgerEntryDelete) {
                    $deleteLedgerResult = $this->deletePlayerLedgerEntry($userId,$previousLedgerEntryDelete->PLAYER_LEDGER_ID);
                }
                    
                $this->insertIntoPlayerLedger_20($userId,'LEDGER_START','0','','0','0','0','0','10000','10000','2020-04-01 00:00:00');

                // Get all previous success deposits and approved withdrawal records
                $approvedSuccessDepositsWithdrawals = $this->getApprovedSuccessDepositsWithdrawals($userId);
                
                foreach($approvedSuccessDepositsWithdrawals as $approvedSuccessDepositsWithdrawalsResult) {
                    $referenceNumber = $approvedSuccessDepositsWithdrawalsResult->INTERNAL_REFERENCE_NO;
                        $action = "DEPOSIT";
                    if($approvedSuccessDepositsWithdrawalsResult->TRANSACTION_TYPE_ID == 10) {
                        $action = "WITHDRAWAL";
                    }
                    
                    $insertPlayerLedger = $this->updatePlayerLedger_20($referenceNumber,$action,$userId,$approvedSuccessDepositsWithdrawalsResult->PAYMENT_TRANSACTION_AMOUNT,$approvedSuccessDepositsWithdrawalsResult->PAYMENT_TRANSACTION_CREATED_ON);
                    
                }
                echo  "SUCCESS";exit;
            }else{
                echo  "FAILED";exit;
            }
        }else{
            echo  "PENDING";exit;
        }

    }


    public function previousLedgerIncludingLedgerStart($userId)
    {
        return $previousLedgerResult = PlayerLedger::select('PLAYER_LEDGER_ID','ACTION','TRANSACTION_AMOUNT','INTERNAL_REFERENCE_NO')
                                        ->where('USER_ID',$userId)
                                        ->orderBy('PAYMENT_TRANSACTION_CREATED_ON','ASC')
                                        ->get();

    }
    
    // Function to delete a particular player ledger id
    public function deletePlayerLedgerEntry($userId,$playerLedgerID)
    {
        return $deletedRows = PlayerLedger::where(['USER_ID' => $userId, 'PLAYER_LEDGER_ID' => $playerLedgerID])->delete();
    }

    public function getApprovedSuccessDepositsWithdrawals($userId)
    {
        $startDate = '2020-04-01 00:00:00';
        $endDate = '2021-03-31 23:59:59';

        return $approvedSuccessDepositsWithdrawalsResult = PaymentTransaction::select('USER_ID','INTERNAL_REFERENCE_NO','PAYMENT_TRANSACTION_AMOUNT','TRANSACTION_TYPE_ID','PAYMENT_TRANSACTION_STATUS','PAYMENT_TRANSACTION_CREATED_ON')
                                                            ->where('USER_ID',$userId)
                                                            ->whereIn('PAYMENT_TRANSACTION_STATUS',[125,103,208])
                                                            ->whereBetween('PAYMENT_TRANSACTION_CREATED_ON',[$startDate,$endDate])
                                                            ->orderBy('PAYMENT_TRANSACTION_CREATED_ON','ASC')
                                                            ->get();

    }

    // Function to get update player ledger table on a successfull deposit or withdrawal
    public function updatePlayerLedger_20($referenceNumber,$action,$userId,$amount,$date){
       
        if ($action == "DEPOSIT") {
           
            // Check if reference number already exists then skip
         if($this->checkReferenceAlreadyExistsInPlayerLedger_20($referenceNumber)) {
             return "";
         }

        // Get userid, amount,promo code, deposit time of payment by passing reference number
        // $paymentDetailsResult = $this->getPaymentDetailsWC($referenceNumber);

         // if(count($paymentDetailsResult)!=0) {
             // If records exists for that successful payment

             // $userId = $paymentDetailsResult->USER_ID;
             // $depositAmount = round($paymentDetailsResult->PAYMENT_TRANSACTION_AMOUNT);
             // $paymentTransactionCreatedOn = $paymentDetailsResult->PAYMENT_TRANSACTION_CREATED_ON;

             $depositAmount = round($amount);
             $paymentTransactionCreatedOn = $date;

             //Get previous player ledger data
             $previousLedgerResult = $this->getPreviousPlayerLedgerData_20($userId);

             // print_r($previousLedgerResult);exit;

             if (!empty($previousLedgerResult)) {
                 // Case: if there are previous records of player ledger

                 $newTotalDeposits = $depositAmount + round($previousLedgerResult->TOTAL_DEPOSITS);
                 $newTotalWithdrawals = round($previousLedgerResult->TOTAL_WITHDRAWALS);
                 $newTotalTaxableWithdrawals = round($previousLedgerResult->TOTAL_TAXABLE_WITHDRAWALS);

                 $newEligibleWithdrawalWithoutTax = $newTotalDeposits - $newTotalWithdrawals + $newTotalTaxableWithdrawals;
                 if (($newTotalDeposits - $newTotalWithdrawals + $newTotalTaxableWithdrawals) < 0) {
                     $newEligibleWithdrawalWithoutTax = 0;
                 }

                 $newTenKExemption = round($previousLedgerResult->EXEMPTION_10K);
                 // if (round($newTotalTaxableWithdrawals) == 0) {
                 // 	$newTenKExemption = 10000;
                 // }

                 $newTotalEligibleWithdrawalWithoutTax = $newEligibleWithdrawalWithoutTax + $newTenKExemption;
             } else {
                 // Case: if there was no previous ledger details
                 $newTotalDeposits = $depositAmount;
                 $newTotalWithdrawals = 0;
                 $newTotalTaxableWithdrawals = 0;
                 $newEligibleWithdrawalWithoutTax = $depositAmount;
                 $newTenKExemption = 10000;
                 $newTotalEligibleWithdrawalWithoutTax = $newEligibleWithdrawalWithoutTax + $newTenKExemption;	
             }


             $this->insertIntoPlayerLedger_20($userId,$action,$depositAmount,$referenceNumber,$newTotalDeposits,$newTotalWithdrawals,$newTotalTaxableWithdrawals,$newEligibleWithdrawalWithoutTax,$newTenKExemption,$newTotalEligibleWithdrawalWithoutTax,$paymentTransactionCreatedOn);

             return "PLAYER_LEDGER_INSERTED";

         // } else {
         // 	// No successful payments found for that reference number
         // 	return "REF_NOT_FOUND";
         // }
        } else if ($action == "WITHDRAWAL") {

            // Check if reference number already exists then skip
         if($this->checkReferenceAlreadyExistsInPlayerLedger_20($referenceNumber)) {
             return "";
         }

            // Get details of withdrawal by passing reference number
         // $withdrawalDetailsResult = $this->getWithdrawalDetails($referenceNumber,"NOT_REVERT_REJECTED");


         // if(count($withdrawalDetailsResult)!=0) {
             // If records exists for that withdrawal

             // $userId = $withdrawalDetailsResult->USER_ID;
             // $withdrawAmount = round($withdrawalDetailsResult->PAYMENT_TRANSACTION_AMOUNT); // 450
             // $paymentTransactionCreatedOn = $withdrawalDetailsResult->PAYMENT_TRANSACTION_CREATED_ON;

            $withdrawAmount = round($amount);
            $paymentTransactionCreatedOn = $date;

             //Get previous player ledger data
            $previousLedgerResult = $this->getPreviousPlayerLedgerData_20($userId);

            if (!empty($previousLedgerResult)) {
                 // Case: if there are previous records of player ledger

                $newTotalDeposits = round($previousLedgerResult->TOTAL_DEPOSITS); // 4770
                $newTotalWithdrawals = $withdrawAmount + round($previousLedgerResult->TOTAL_WITHDRAWALS); // 450+6900=7350

                $newTotalTaxableWithdrawals = round($previousLedgerResult->TOTAL_TAXABLE_WITHDRAWALS); // Initialising 2630

                if(
                    $newTotalTaxableWithdrawals <= 10000 &&
                    round($previousLedgerResult->EXEMPTION_10K) > 0 && // 7370
                    round($previousLedgerResult->EXEMPTION_10K) <= 10000 &&
                    (round($previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX) - $withdrawAmount) <= round($previousLedgerResult->EXEMPTION_10K) &&
                    (round($previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX) - $withdrawAmount) > 0
                ) {
                    // Middle Condition of TenK Exemption
                    $newTenKExemption = round($previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX) - $withdrawAmount;
                    $newTotalTaxableWithdrawals = 10000 - $newTenKExemption;

                } else {
                    if (round($previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX) <= $withdrawAmount) {
                        $newTotalTaxableWithdrawals = $withdrawAmount - round($previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX) + round($previousLedgerResult->TOTAL_TAXABLE_WITHDRAWALS) + round($previousLedgerResult->EXEMPTION_10K);
                    }
                    $newTenKExemption = 0;
                    if ($withdrawAmount <= round($previousLedgerResult->ELIGIBLE_WITHDRAWAL_WITHOUT_TAX)) {
                        $newTenKExemption = round($previousLedgerResult->EXEMPTION_10K);
                    }
                    if (round($newTotalTaxableWithdrawals) == 0) {
                        $newTenKExemption = 10000;
                    }

                }

                $newEligibleWithdrawalWithoutTax = $newTotalDeposits - $newTotalWithdrawals + $newTotalTaxableWithdrawals;
                if (($newTotalDeposits - $newTotalWithdrawals + $newTotalTaxableWithdrawals) < 0) {
                    $newEligibleWithdrawalWithoutTax = 0;
                }

                $newTotalEligibleWithdrawalWithoutTax = $newEligibleWithdrawalWithoutTax + $newTenKExemption;

                $this->insertIntoPlayerLedger_20($userId,$action,$withdrawAmount,$referenceNumber,$newTotalDeposits,$newTotalWithdrawals,$newTotalTaxableWithdrawals,$newEligibleWithdrawalWithoutTax,$newTenKExemption,$newTotalEligibleWithdrawalWithoutTax,$paymentTransactionCreatedOn);

                $taxableAmount = 0;
                if ($withdrawAmount > round($previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX)) {
                    $taxableAmount = $withdrawAmount - round($previousLedgerResult->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX);
                }

                // return $taxableAmount;
                return "PLAYER_LEDGER_INSERTED";

            } else if ($action == "REVERT_REJECT") {

                // Get details of withdrawal by passing reference number
                //define in Traits/Models/common_method 
                $withdrawalDetailsResult = $this->getWithdrawalDetails($referenceNumber, "INCLUDING_REVERT_REJECTED");
          
                if (!empty($withdrawalDetailsResult)) {
                  // If records exists for that withdrawal
                  $userId = $withdrawalDetailsResult->USER_ID;
          
                  $withdrawAmount = $withdrawalDetailsResult->PAYMENT_TRANSACTION_AMOUNT;
          
                  //Get previous player ledger data
                  //define in Traits/Models/common_method 
                  $previousLedgerResult = $this->getPreviousPlayerLedgerDataForRevert($userId, $referenceNumber);
                  if (!empty($previousLedgerResult) && $previousLedgerResult != "REF_NOT_FOUND") {
          
                    // 1. We will need to delete the entries including and after this entry in player ledger table
                    // 2. Will need to insert entries in player ledger table again sequentially excluding the reverted or rejected one
          
                    // echo "Going to Deleting all player ledger entries";
                    // print_r("<br/>");
                    // 1. Deleting all player ledger entries
          
                    foreach ($previousLedgerResult as $previousLedgerEntryDelete) {
                      // Delete player ledger Entry
                      //define in Traits/Models/common_method 
          
                      $this->deletePlayerLedgerEntry($userId, $previousLedgerEntryDelete->PLAYER_LEDGER_ID);
                    }
                    //   	echo "Re-Insert entries in player ledger table again sequentially excluding the reverted or rejected one";
                    // print_r("<br/>");
                    // 2. Re-Insert entries in player ledger table again sequentially excluding the reverted or rejected one
                    foreach ($previousLedgerResult as $previousLedgerEntryReInsert) {
                      // echo "$previousLedgerEntryReInsert->INTERNAL_REFERENCE_NO".'----'.$referenceNumber;exit;
                      if ($previousLedgerEntryReInsert->INTERNAL_REFERENCE_NO != $referenceNumber) {
                        $reInsertPlayerLedger = $this->updatePlayerLedger($previousLedgerEntryReInsert->INTERNAL_REFERENCE_NO, $previousLedgerEntryReInsert->ACTION);
                      }
                    }
                  } else {
                    // Case: if there was no previous ledger details
                    // Ideally this should not be possible, because no player is allowed to withdraw without depositing and if he has deposited then there should be an entry for him in player_ledger table
                    // Yet to decide on how to handle this
                    return "NO_LEDGER_FOUND";
                  }
                } else {
                  // No withdrawals found for that reference number
                  return "REF_NOT_FOUND";
                }
            } else {
            // Wrong Action Passed
            return "INCORRECT_ACTION";
            }

        }

    }

    public function checkReferenceAlreadyExistsInPlayerLedger_20($referenceNumber){
      
        $previousCriteriaResult = PlayerLedger::select('USER_ID')
            ->where('INTERNAL_REFERENCE_NO', $referenceNumber)
            ->first();
        return isset($previousCriteriaResult) ? TRUE : FALSE;
       
    }

    // Function to get last entry of a user from player ledger table
    public function getPreviousPlayerLedgerData_20($userId){
    	
    	$previousCriteriaResult = PlayerLedger::select('ACTION', 'TOTAL_DEPOSITS', 'TOTAL_WITHDRAWALS', 'TOTAL_TAXABLE_WITHDRAWALS', 'ELIGIBLE_WITHDRAWAL_WITHOUT_TAX', 'EXEMPTION_10K', 'TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX')
                                                ->where('USER_ID', $userId)
                                                ->orderBy('PLAYER_LEDGER_ID', 'DESC')
                                                ->first();
        return $previousCriteriaResult;
    }

    // Function to insert data in to withdrwal_criteria table
    public function insertIntoPlayerLedger_20($userId,$action,$transactionAmount,$referenceNumber,$totalDeposits,$totalWithdrawals,$totalTaxableWithdrawals,$eligibleWithdrawalWithoutTax,$tenKExemption,$totalEligibleWithdrawalWithoutTax,$paymentTransactionCreatedOn){
    	
    	date_default_timezone_set('Asia/Kolkata');
        $currentDate = date('Y-m-d H:i:s');
        // DB::enableQueryLog();
        $insertPlayerLedgerData = new PlayerLedger();
        $insertPlayerLedgerData->USER_ID                                  = $userId;
        $insertPlayerLedgerData->ACTION                                   = $action;
        $insertPlayerLedgerData->TRANSACTION_AMOUNT                       = $transactionAmount;
        $insertPlayerLedgerData->INTERNAL_REFERENCE_NO                    = $referenceNumber;
        $insertPlayerLedgerData->TOTAL_DEPOSITS                           = $totalDeposits;
        $insertPlayerLedgerData->TOTAL_WITHDRAWALS                        = $totalWithdrawals;
        $insertPlayerLedgerData->TOTAL_TAXABLE_WITHDRAWALS                = $totalTaxableWithdrawals;
        $insertPlayerLedgerData->ELIGIBLE_WITHDRAWAL_WITHOUT_TAX          = $eligibleWithdrawalWithoutTax;
        $insertPlayerLedgerData->EXEMPTION_10K                            = $tenKExemption;
        $insertPlayerLedgerData->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX    = $totalEligibleWithdrawalWithoutTax;
        $insertPlayerLedgerData->CREATED_DATE                             = $currentDate;
        $insertPlayerLedgerData->PAYMENT_TRANSACTION_CREATED_ON           = $paymentTransactionCreatedOn;

        return $insertPlayerLedgerData->save();
    }

    // Function to get withdrawal records from payment_transaction table
    public function getWithdrawalDetails($referenceNo, $withdrawalStatus)
    {
        //Get userid, reference number, amount of withdrawal by passing reference number
        $transStatusId = array('203', '112');
        $withdrawalDetailsResult = PaymentTransaction::select('USER_ID', 'INTERNAL_REFERENCE_NO', 'PAYMENT_TRANSACTION_AMOUNT', 'PAYMENT_TRANSACTION_CREATED_ON')
        ->where('INTERNAL_REFERENCE_NO', $referenceNo)
        ->when($withdrawalStatus == "NOT_REVERT_REJECTED", function ($query) use ($transStatusId) {
            return $query->whereNotIn('PAYMENT_TRANSACTION_STATUS', $transStatusId);
        })
        ->first();
        return $withdrawalDetailsResult;
    }

    // Function to get all entries of a user from player ledger table after the withdrawal revert reference number
    public function getPreviousPlayerLedgerDataForRevert($userId, $referenceNumber)
    {
        $referenceLedgerResult = PlayerLedger::select('PLAYER_LEDGER_ID')
        ->where('USER_ID', $userId)
        ->where('INTERNAL_REFERENCE_NO', $referenceNumber)
        ->first();
        //print_r ($referenceLedgerResult);exit;

        if (!empty($referenceLedgerResult->PLAYER_LEDGER_ID)) {
        $previousLedgerResult = PlayerLedger::select('PLAYER_LEDGER_ID', 'ACTION', 'TRANSACTION_AMOUNT', 'INTERNAL_REFERENCE_NO')
            ->where('USER_ID', $userId)
            ->where('PLAYER_LEDGER_ID', $referenceLedgerResult->PLAYER_LEDGER_ID)
            ->orderBy('PLAYER_LEDGER_ID', 'ASC')
            ->get();

        //print_r ($previousLedgerResult);exit;
        return $previousLedgerResult;
        } else {
        return "REF_NOT_FOUND";
        }
    }
}