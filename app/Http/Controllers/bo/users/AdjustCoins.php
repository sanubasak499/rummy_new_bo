<?php

namespace App\Http\Controllers\bo\users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BalanceType;
use App\Models\CoinType;
use App\Models\UserPoint;
use App\Models\User;
use App\Models\MasterTransactionHistory;
use App\Models\AdjustmentTransaction;
use App\Models\MasterTransactionHistoryFppBonus;
use App\Models\MasterTransactionHistoryCoins;
use App\Models\MasterTransactionHistoryPokercommission;
use App\Models\ActionReason;
use App\Imports\UserImport;
use Maatwebsite\Excel\Importer;
use Auth;
use DB;


class AdjustCoins extends Controller
{
    /**
     * Display Adjust Coins Page And get Dyanamic select option 
    **/
    function AdjustCoins(request $request){
        if(!empty($request->user_id)){
            if(\PokerBaazi::checkTransactionPassword($request->input('transactionpassword'))){
                    return $this->insertUpdateAdjustCoins($request);
                }else{
                return response()->json(['status'=>301, 'message'=>'Transaction password not matched']);
            }
        }
        $adjustCoins['action_reasons'] = ActionReason::whereIn('REASON_CATEGORY_ID',[1])->active()->get();
        $adjustCoins['balanceType'] = BalanceType::select('BALANCE_TYPE_ID','BALANCE_TYPE')->where('BALANCE_TYPE_ID','<','4')->get();
        $adjustCoins['coinTypes'] = CoinType::select('COIN_TYPE_ID','NAME')->where('status','=','1')->wherein('COIN_TYPE_ID',[1,3,12,16])->get();
        return view('bo.views.users.adjustcoins.index',$adjustCoins);
    }

    // Insert and update Adjust Coins
    function insertUpdateAdjustCoins($requestData){
        $requestArray = is_array($requestData) ? $requestData : [$requestData];

        foreach($requestArray as $request){

            if($request->user_id==''){
                return response()->json(['status'=>301, 'message'=>'Username Must Be Required']);
            }
            if($request->adjust_balance_type==''){
                return response()->json(['status'=>301, 'message'=>'Balance Type Must Be Required']);
            }
            if($request->adjust_coin==''){
                return response()->json(['status'=>301, 'message'=>'Amount Must Be Required']);
            }
            if($request->adjust_coin_type==''){
                return response()->json(['status'=>301, 'message'=>'Coin Type Must Be Required']);
            }
            if($request->coin_adjust_type==''){
                return response()->json(['status'=>301, 'message'=>'Adjust Type Must Be Required']);
            }
            if($request->adjust_coin_reason==''){
               return response()->json(['status'=>301, 'message'=>'Reason Must Be Required']);
            }
            if(!is_numeric($request->adjust_coin)){
                return response()->json(['status'=>301, 'message'=>'Amount Must Be Required Integer Type']);
            }
            
    
            $where = [
                'USER_ID' => $request->user_id,
                'COIN_TYPE_ID'  => $request->adjust_coin_type
            ];
            $internalRefNo='122'.rand(0,50).rand(51,99).$request->user_id.date('dmYHis');
            $userPoints = $this->getUserPoints($where);
            $reasionId = $request->adjust_coin_reason;
            // balance type 1 for deposit
            if($request->adjust_coin_type == 1){
                if($request->adjust_balance_type == 1){
                    $action    =	"Adjustment Deposit";
                    $utransactionStatusId = "103";
                    $utransactionTypeId= $request->technical_loss_register=="Yes"?"88":"22";
                    // coin_adjust_type 1 for add else condition for subtraction
                    if($request->coin_adjust_type==1){
                       $data['USER_DEPOSIT_BALANCE'] = $userPoints->USER_DEPOSIT_BALANCE +  $request->adjust_coin;
                       $data['VALUE'] = $userPoints->USER_TOT_BALANCE + $request->adjust_coin;
                       $data['USER_TOT_BALANCE'] = $userPoints->USER_TOT_BALANCE + $request->adjust_coin;
                       $calcSymbol  = '+';
                    } else { 
                        if($request->adjust_coin > $userPoints->USER_DEPOSIT_BALANCE){
                            return response()->json(['status'=>301, 'message'=>'Enter Amount Must Be Less Than Deposit Balance']);
                        } else{
                            $data['USER_DEPOSIT_BALANCE'] = $userPoints->USER_DEPOSIT_BALANCE - $request->adjust_coin;
                            $data['VALUE'] = $userPoints->USER_TOT_BALANCE - $request->adjust_coin;
                            $data['USER_TOT_BALANCE'] = $userPoints->USER_TOT_BALANCE - $request->adjust_coin;
                            $calcSymbol  = '-';
                        }
                    }
    
                } else if ($request->adjust_balance_type == 3){
                    //Balance type 3 for WIN
                    $action   =	"Adjustment Win";
                    $utransactionStatusId = "102";
                    $utransactionTypeId= $request->technical_loss_register=="Yes"?"88":"23";
                    // coin_adjust_type 1 for add else condition for subtraction
                    if($request->coin_adjust_type==1){
                        $data['USER_WIN_BALANCE'] = $userPoints->USER_WIN_BALANCE +  $request->adjust_coin;
                        $data['VALUE'] = $userPoints->USER_TOT_BALANCE + $request->adjust_coin;
                        $data['USER_TOT_BALANCE'] = $userPoints->USER_TOT_BALANCE + $request->adjust_coin;
                        $calcSymbol  = '+';
                    } else {
                         if($request->adjust_coin > $userPoints->USER_WIN_BALANCE){
                            return response()->json(['status'=>301, 'message'=>'Enter Amount Must Be Less Than Win Balance']);
                        } else{
                             $data['USER_WIN_BALANCE'] = $userPoints->USER_WIN_BALANCE - $request->adjust_coin;
                             $data['VALUE'] = $userPoints->USER_TOT_BALANCE - $request->adjust_coin;
                             $data['USER_TOT_BALANCE'] = $userPoints->USER_TOT_BALANCE - $request->adjust_coin;
                             $calcSymbol  = '-';
                        }
                    }
    
                } else {
                    //Balance type 2 for PROMO
                    $action    =	"Adjustment Promo";
                    $utransactionStatusId = "107";
                    $utransactionTypeId= $request->technical_loss_register=="Yes"?"88":"21";
                    // coin_adjust_type 1 for add else condition for subtraction
                    if($request->coin_adjust_type==1){
                        $data['USER_PROMO_BALANCE'] = $userPoints->USER_PROMO_BALANCE +  $request->adjust_coin;
                        $data['VALUE'] = $userPoints->USER_TOT_BALANCE + $request->adjust_coin;
                        $data['USER_TOT_BALANCE'] = $userPoints->USER_TOT_BALANCE + $request->adjust_coin;
                        $calcSymbol  = '+';
                    } else {
                         if($request->adjust_coin > $userPoints->USER_PROMO_BALANCE){
                            return response()->json(['status'=>301, 'message'=>'Enter Amount Must Be Less Than Promo Balance']);
                        } else{
                             $data['USER_PROMO_BALANCE'] = $userPoints->USER_PROMO_BALANCE - $request->adjust_coin;
                             $data['VALUE'] = $userPoints->USER_TOT_BALANCE - $request->adjust_coin;
                             $data['USER_TOT_BALANCE'] = $userPoints->USER_TOT_BALANCE - $request->adjust_coin;
                             $calcSymbol  = '-';
                        }
                    }
    
                }
    
            } else {
                // for deposit
                if($request->adjust_balance_type == 1){
                    $action    =	"Adjustment Deposit";
                    $utransactionStatusId = "103";
                    $utransactionTypeId= $request->technical_loss_register=="Yes"?"88":"22";
                    if($request->coin_adjust_type==1){
                        $data['USER_DEPOSIT_BALANCE'] = $userPoints->USER_DEPOSIT_BALANCE +  $request->adjust_coin;
                        $data['VALUE'] = $userPoints->USER_TOT_BALANCE + $request->adjust_coin;
                        $data['USER_TOT_BALANCE'] = $userPoints->USER_TOT_BALANCE + $request->adjust_coin;
                        $calcSymbol  = '+';
                    } else {
                         if($request->adjust_coin > $userPoints->USER_DEPOSIT_BALANCE){
                            return response()->json(['status'=>301, 'message'=>'Enter Amount Must Be Less Than Deposit Balance']);
                         } else{
                             $data['USER_DEPOSIT_BALANCE'] = $userPoints->USER_DEPOSIT_BALANCE - $request->adjust_coin;
                             $data['VALUE'] = $userPoints->USER_TOT_BALANCE - $request->adjust_coin;
                             $data['USER_TOT_BALANCE'] = $userPoints->USER_TOT_BALANCE - $request->adjust_coin;
                             $calcSymbol  = '-';
                         }
                    }
                } else if($request->adjust_balance_type == 2){
                    // For Promo Balance
                    $action    =	"Adjustment Promo";
                    $utransactionStatusId = "107";
                    $utransactionTypeId= $request->technical_loss_register=="Yes"?"88":"21";
                    if($request->coin_adjust_type==1){
                        $data['USER_PROMO_BALANCE'] = $userPoints->USER_PROMO_BALANCE +  $request->adjust_coin;
                        $data['VALUE'] = $userPoints->USER_TOT_BALANCE + $request->adjust_coin;
                        $data['USER_TOT_BALANCE'] = $userPoints->USER_TOT_BALANCE + $request->adjust_coin;
                        $calcSymbol  = '+';
                     } else {
                         if($request->adjust_coin > $userPoints->USER_PROMO_BALANCE){
                            return response()->json(['status'=>301, 'message'=>'Enter Amount Must Be Less Than Promo Balance']);
                         } else{
                             $data['USER_PROMO_BALANCE'] = $userPoints->USER_PROMO_BALANCE - $request->adjust_coin;
                             $data['VALUE'] = $userPoints->USER_TOT_BALANCE - $request->adjust_coin;
                             $data['USER_TOT_BALANCE'] = $userPoints->USER_TOT_BALANCE - $request->adjust_coin;
                             $calcSymbol  = '-';
                         }
                     }
    
                } else {
                    // for WIN Balance
                    $action   =	"Adjustment Win";
                    $utransactionStatusId = "102";
                    $utransactionTypeId= $request->technical_loss_register=="Yes"?"88":"23";
                    if($request->coin_adjust_type==1){
                        $data['USER_WIN_BALANCE'] = $userPoints->USER_WIN_BALANCE +  $request->adjust_coin;
                        $data['VALUE'] = $userPoints->USER_TOT_BALANCE + $request->adjust_coin;
                        $data['USER_TOT_BALANCE'] = $userPoints->USER_TOT_BALANCE + $request->adjust_coin;
                        $calcSymbol  = '+';
                     } else {
                         if($request->adjust_coin > $userPoints->USER_WIN_BALANCE){
                            return response()->json(['status'=>301, 'message'=>'Enter Amount Must Be Less Than Win Balance']);
                         } else{
                             $data['USER_WIN_BALANCE'] = $userPoints->USER_WIN_BALANCE - $request->adjust_coin;
                             $data['VALUE'] = $userPoints->USER_TOT_BALANCE - $request->adjust_coin;
                             $data['USER_TOT_BALANCE'] = $userPoints->USER_TOT_BALANCE - $request->adjust_coin;
                             $calcSymbol  = '-';
                         }
                     }
                }
            }
        
    
            if($request->adjust_balance_type == 1){
                $CLOSING_TOT_BALANCE = $data['USER_TOT_BALANCE'];
            } else if ($request->adjust_balance_type == 3){
                $CLOSING_TOT_BALANCE = $data['USER_TOT_BALANCE'];
            }else if ($request->adjust_balance_type == 2){
                $CLOSING_TOT_BALANCE = $data['USER_TOT_BALANCE'];
            }else {
                $CLOSING_TOT_BALANCE = '';
            }
        }
        
        if(!is_array($requestData)){
            return $this->finalInsert($where,$data,$request,$utransactionTypeId,$internalRefNo,$utransactionStatusId,$CLOSING_TOT_BALANCE,$calcSymbol,$userPoints,$action,$reasionId);
        }else{
            return response()->json(['status'=>201, 'message'=>'All Data is valid']);
        }
    }


    public function finalInsert($where,$data,$request,$utransactionTypeId,$internalRefNo,$utransactionStatusId,$CLOSING_TOT_BALANCE,$calcSymbol,$userPoints,$action,$reasionId){
        DB::transaction(function() use ($where,$data,$request,$utransactionTypeId,$internalRefNo,$utransactionStatusId,$CLOSING_TOT_BALANCE,$calcSymbol,$userPoints,$action,$reasionId){
            
            $this->updateUserPoint($where, $data);
            $this->insertAdjustmentTransaction($insertAdjustdata = [
                'USER_ID'  => $request->user_id,
                'REASON_ID' => $reasionId,
                'TRANSACTION_TYPE_ID'  => $utransactionTypeId,
                'INTERNAL_REFERENCE_NO'  => $internalRefNo,
                'ADJUSTMENT_CREATED_BY'  => 'admin',
                'ADJUSTMENT_CREATED_ON'  => now(),
                'ADJUSTMENT_AMOUNT'  => $request->adjust_coin,
                'ADJUSTMENT_ACTION'  => ($request->coin_adjust_type==1)?'Add':'Subtract',
                'ADJUSTMENT_COMMENT'  => $request->adjust_coin_comment?? '',
                'COIN_TYPE_ID'  => $request->adjust_coin_type
            ]);
            $this->insertMasterTransactionHistory($request->adjust_coin_type,$insertData=[
                'USER_ID' => $request->user_id,
                'BALANCE_TYPE_ID' => $request->adjust_balance_type,
                'TRANSACTION_STATUS_ID' => $utransactionStatusId,
                'TRANSACTION_TYPE_ID' => $utransactionTypeId,
                'TRANSACTION_AMOUNT' => $request->adjust_coin,
                'TRANSACTION_DATE' => now(),
                'INTERNAL_REFERENCE_NO' => $internalRefNo,
                'CURRENT_TOT_BALANCE' => $userPoints->USER_TOT_BALANCE,
                'CLOSING_TOT_BALANCE' => $CLOSING_TOT_BALANCE,
                //'PARTNER_ID' => $request->PARTNER_ID
                'PARTNER_ID' => Auth::user()->fk_partner_id
            ]);
    
            $activityData = [
                'pay_trans_id'  => $internalRefNo,
                'userid'    =>  $request->user_id,
                'type'  =>  $calcSymbol,
                //'adminuserid'    => $request->PARTNER_ID,
                'amount'    =>  $request->adjust_coin
            ];
            
            \PokerBaazi::storeActivityWithParams($action, json_encode($activityData), $module_id="NULL");  
        }, 5);
        return response()->json(['status'=>200, 'message'=>'Amount updated successfully']);
    }

    // Get User Coins , Partner Name
    function balanceAdjust(request $request){
        if($request->input('username') != ''){
            $user_id = getUserIdFromUsername($request->input('username'));
            if(!empty($user_id)){
                $data['user_points'] = UserPoint::where('USER_ID','=',$user_id)
                ->wherein('COIN_TYPE_ID',[1,3,12,16])
                ->get();
                $data['user']= User::select('PARTNER_ID','USER_ID','FIRSTNAME','LASTNAME')->find($user_id);
                $data['partner_name'] = $data['user']->partner->PARTNER_NAME;
                return response()->json(['status'=>200, 'message'=>'success', 'data'=>$data]);
            } else {
                return response()->json(['status'=>301, 'message'=>'User does not exist!']);
            }
        } else {
            return response()->json(['status'=>301, 'message'=>'Enter User Name']);
        }
    }

    //Update UserPoints Table
    public function updateUserPoint($where, $data){
        UserPoint::where($where)
        ->update($data);
    }

    //get user points balance
    public function getUserPoints($where){
        return UserPoint::where($where)->first();
    }

    // Insert data in master Transaction History
    public function insertMasterTransactionHistory($coinType,$data){ 
        if($coinType == 3){
            $data = new MasterTransactionHistoryFppbonus($data);
            return $data->save();
        }else if($coinType == 12){
            $data = new MasterTransactionHistoryCoins($data);
            return $data->save();
        }else if($coinType == 16){
            $data = new MasterTransactionHistoryPokercommission($data);
            return $data->save();
        }else{
            $data = new MasterTransactionHistory($data);
            return $data->save();
        }
    }

    // Insert Adjustment Transaction 
    public function insertAdjustmentTransaction($data){
        $data = new AdjustmentTransaction($data);
        return $data->save();
    }


    // This Function Match Excel File (Data) is corect or not
    public function adjustCoinExcel(request $request, Importer $importer){
        try{
            if($request->hasFile('excel_file')){
                if($request->file('excel_file')->extension() == 'xlsx'){
                    $importer->import($data = new UserImport, $request->excel_file);
                    $notExistflag=false;
                    if($data->data){
                        foreach($data->data as $key => $excelData){
                            $checkReasion[] = $excelData['Reason'];
                            $getResion = $this->getActionReasonId($excelData['Reason'])->toArray();
                            $validdata['heading'] = $data->heading;
                            $validdata['data'][]= $excelData;
                            if( !($userId = getUserIdFromUsername($excelData['Username']))){
                                $notExistflag=true;
                                //$userNameNotExist[]=$excelData['Username'];
                                $userNameNotExist[]='Invalid Username';
                                $flags[]=false;
                            } 

                            else if($excelData['Reason'] == ''){
                                $notExistflag=true;
                                $userNameNotExist[]='Reason Must Be Required';
                                $flags[]=false;
                            }
                            
                            else if(!is_numeric($excelData['Amount'])){
                                $notExistflag=true;
                                $userNameNotExist[]='Amount Must Be Required Integer Type';
                                $flags[]=false;
                            }

                            else if($excelData['Balance Type'] == ''){
                                $notExistflag=true;
                                $userNameNotExist[]='Balance Type Must Be Required';
                                $flags[]=false;
                            } 

                            else if($excelData['Adjust Type'] == ''){
                                $notExistflag=true;
                                $userNameNotExist[]='Adjust Type Must Be Required';
                                $flags[]=false;
                            } 

                            else if($excelData['Coin Type'] == ''){
                                $notExistflag=true;
                                $userNameNotExist[]='Coin Type Must Be Required';
                                $flags[]=false;
                            } 

                            
                            else if(!$getResion){
                                $notExistflag=true;
                                //$userNameNotExist[] = $excelData['Reason'];
                                $userNameNotExist[] = 'Invalid Reason';
                                $flags[]=false;
                            }

                            else if(!preg_match("/^[a-zA-Z0-9-_'.$1\n& ]+$/",$excelData['Comments'])){
                                $notExistflag=true;
                                $userNameNotExist[] = "Invalid Comment Format";
                                $flags[]=false;
                            }

                            
                            
                            else if(!(strtoupper($excelData['Balance Type']) == strtoupper('Adjust Promo') || strtoupper($excelData['Balance Type']) == strtoupper('Adjust Win') || strtoupper($excelData['Balance Type']) == strtoupper('Adjust Deposit'))){
                                $notExistflag=true;
                                //$userNameNotExist[]=$excelData['Balance Type'];
                                $userNameNotExist[]='Invalid Balance Type';
                                $flags[]=false;
                            }
                            else if(!(strtoupper($excelData['Adjust Type']) == strtoupper("Add") || strtoupper($excelData['Adjust Type']) == strtoupper("Subtract"))){
                                $notExistflag=true;
                                //$userNameNotExist[]=$excelData['Adjust Type'];
                                $userNameNotExist[]='Invalid Adjust Type';
                                $flags[]=false;
                            }

                            else if(strtoupper($excelData['Adjust Type']) == strtoupper("Subtract")){

                                if(strtoupper($excelData['Coin Type']) == strtoupper('Cash')){
                                    $excelData['adjust_coin_type'] = 1;
                                }else if(strtoupper($excelData['Coin Type']) == strtoupper('FPP Bonus')){
                                    $excelData['adjust_coin_type'] = 3;
                                }else if(strtoupper($excelData['Coin Type']) == strtoupper('Rewards Points')){
                                    $excelData['adjust_coin_type'] = 12;
                                }else if(strtoupper($excelData['Coin Type']) == strtoupper('Poker Commission')){
                                    $excelData['adjust_coin_type'] = 16;
                                } else {
                                    $excelData['adjust_coin_type'] = '';
                                }
                                
                                if($excelData['adjust_coin_type'] !=''){
                                    $where = [
                                        'USER_ID' => getUserIdFromUsername($excelData['Username']),
                                        'COIN_TYPE_ID'  => $excelData['adjust_coin_type']
                                    ];
                                    $userPoints = $this->getUserPoints($where);
                                }
                                
                              
                                if(strtoupper($excelData['Balance Type']) == strtoupper('Adjust Promo')){
                                    //Balance type 2 for Promo
                                    $excelData['adjust_balance_type'] = 2;
                                    if($excelData['Amount'] > $userPoints->USER_PROMO_BALANCE){
                                        $notExistflag=true;
                                        $userNameNotExist[]='Enter Amount Must Be Less Than Promo Balance('.$userPoints->USER_PROMO_BALANCE.')';
                                        $flags[]=false;
                                        //return response()->json(['status'=>301, 'message'=>'Enter Amount Must Be Less Than Win Balance']);
                                    }else{
                                        $flags[]=true;
                                        $userNameNotExist[] = 'Ok';
                                    }

                                }else if(strtoupper($excelData['Balance Type']) == strtoupper('Adjust Win')){
                                    //Balance type 3 for WIN
                                    $excelData['adjust_balance_type'] = 3;
                                    if($excelData['Amount'] > $userPoints->USER_WIN_BALANCE){
                                        $notExistflag=true;
                                        $userNameNotExist[]='Enter Amount Must Be Less Than Win Balance('.$userPoints->USER_WIN_BALANCE.')';
                                        $flags[]=false;
                                        //return response()->json(['status'=>301, 'message'=>'Enter Amount Must Be Less Than Win Balance']);
                                    }else{
                                        $flags[]=true;
                                        $userNameNotExist[] = 'Ok';
                                    }
                                }else if(strtoupper($excelData['Balance Type']) == strtoupper('Adjust Deposit')){
                                    //Balance type 1 for deposit
                                    $excelData['adjust_balance_type'] = 1;
                                    if($excelData['Amount'] > $userPoints->USER_DEPOSIT_BALANCE){
                                        $notExistflag=true;
                                        $userNameNotExist[]='Enter Amount Must Be Less Than Deposit Balance('.$userPoints->USER_DEPOSIT_BALANCE.')';
                                        $flags[]=false;
                                        //return response()->json(['status'=>301, 'message'=>'Enter Amount Must Be Less Than Deposit Balance']);
                                    }else{
                                        $flags[]=true;
                                        $userNameNotExist[] = 'Ok';
                                    }
                                }
                            

                            }

                            else if(!(strtoupper($excelData['Coin Type']) == strtoupper('Cash') || strtoupper($excelData['Coin Type']) == strtoupper('FPP Bonus') || strtoupper($excelData['Coin Type']) == strtoupper('Rewards Points') || strtoupper($excelData['Coin Type']) == strtoupper('Poker Commission'))){
                                $notExistflag=true;
                                //$userNameNotExist[]=$excelData['Coin Type'];
                                $userNameNotExist[]='Invalid Coin Type';
                                $flags[]=false;
                            }else{
                            $flags[]=true;
                            $userNameNotExist[] = 'Ok';
                            }
                        }
                        
                        return response()->json(['status'=>200, 'message'=>'success','status'=>$userNameNotExist,'flags' => $flags,'data'=>(object)$validdata]);
                    }else{
                        return response()->json(['status'=>302, 'message'=>'No Data Exist!','data'=>'Current File '.$request->file('excel_file')->extension()]);
                    }
                } else {
                        return response()->json(['status'=>302, 'message'=>'Only excel file accept','data'=>'Current File '.$request->file('excel_file')->extension()]);
                }

            }
        }catch(\Exception $e){
            return response()->json(['status'=>500, 'message'=>$e->getMessage()]);
        }   
    }


    // Inserting Excel Data
    function insertAdjustExcel(request $request,Importer $importer){
        if(\PokerBaazi::checkTransactionPassword($request->input('transcation_passsword'))){

            if($request->hasFile('excel_file')){
                $importer->import($data = new UserImport, $request->excel_file);
                $requestData = [];
                foreach($data->data as $excelData){
                    
                    $excelData['user_id'] = getUserIdFromUsername($excelData['Username']);
                    
                    $getResion = $this->getActionReasonId($excelData['Reason'])->toArray();
                    if($getResion){
                        $excelData['adjust_coin_reason'] = $getResion[0]['ACTIONS_REASONS_ID'];
                    }else{
                        $excelData['adjust_coin_reason'] = '';
                    }

                    if(strtoupper($excelData['Technical Loss']) == strtoupper('Yes')){
                        $excelData['technical_loss_register'] = 'Yes';
                    }else{
                        $excelData['technical_loss_register'] = 'No';
                    }
                    
                    if(strtoupper($excelData['Balance Type']) == strtoupper('Adjust Promo')){
                        $excelData['adjust_balance_type'] = 2;
                    }else if(strtoupper($excelData['Balance Type']) == strtoupper('Adjust Win')){
                        $excelData['adjust_balance_type'] = 3;
                    }else if(strtoupper($excelData['Balance Type']) == strtoupper('Adjust Deposit')){
                        $excelData['adjust_balance_type'] = 1;
                    } else {
                        $excelData['adjust_balance_type'] = '';
                    }

                    if(strtoupper($excelData['Adjust Type']) == strtoupper("Add")){
                        $excelData['coin_adjust_type'] = 1;
                    }else if(strtoupper($excelData['Adjust Type']) == strtoupper("Subtract")){
                        $excelData['coin_adjust_type'] = 2;
                    } else {
                        $excelData['coin_adjust_type'] = '';
                    } 
                    
                    if(strtoupper($excelData['Coin Type']) == strtoupper('Cash')){
                        $excelData['adjust_coin_type'] = 1;
                    }else if(strtoupper($excelData['Coin Type']) == strtoupper('FPP Bonus')){
                        $excelData['adjust_coin_type'] = 3;
                    }else if(strtoupper($excelData['Coin Type']) == strtoupper('Rewards Points')){
                        $excelData['adjust_coin_type'] = 12;
                    }else if(strtoupper($excelData['Coin Type']) == strtoupper('Poker Commission')){
                        $excelData['adjust_coin_type'] = 16;
                    } else {
                        $excelData['adjust_coin_type'] = '';
                    } 

                    if(is_numeric($excelData['Amount'])){
                        $excelData['adjust_coin'] = $excelData['Amount'];
                    }else{
                        $excelData['adjust_coin'] = '';
                    }

                    if(preg_match("/^[a-zA-Z0-9-_'.$1\n& ]+$/",$excelData['Comments'])){
                        $excelData['adjust_coin_comment'] = $excelData['Comments'];
                    }else{
                        $excelData['adjust_coin_comment'] = '';
                    }

                    if(strtoupper($excelData['Adjust Type']) == strtoupper("Subtract") && $excelData['user_id'] !='' && $excelData['adjust_coin_type']){

                        if($excelData['adjust_coin_type'] !=''){
                            $where = [
                                'USER_ID' => getUserIdFromUsername($excelData['Username']),
                                'COIN_TYPE_ID'  => $excelData['adjust_coin_type']
                            ];
                            $userPoints = $this->getUserPoints($where);
                        }
                        
                        if(strtoupper($excelData['Balance Type']) == strtoupper('Adjust Promo')){
                            //Balance type 2 for Promo
                            $excelData['adjust_balance_type'] = 2;
                            if($excelData['Amount'] > $userPoints->USER_PROMO_BALANCE){
                                $excelData['user_id'] = '';
                            }else{
                                $excelData['user_id'] = getUserIdFromUsername($excelData['Username']);
                            }

                        }else if(strtoupper($excelData['Balance Type']) == strtoupper('Adjust Win')){
                            //Balance type 3 for WIN
                            $excelData['adjust_balance_type'] = 3;
                            if($excelData['Amount'] > $userPoints->USER_WIN_BALANCE){
                                $excelData['user_id'] = '';
                            }else{
                                $excelData['user_id'] = getUserIdFromUsername($excelData['Username']);
                            }
                        }else if(strtoupper($excelData['Balance Type']) == strtoupper('Adjust Deposit')){
                            //Balance type 1 for deposit
                            $excelData['adjust_balance_type'] = 1;
                            if($excelData['Amount'] > $userPoints->USER_DEPOSIT_BALANCE){
                                $excelData['user_id'] = '';
                            }else{
                                $excelData['user_id'] = getUserIdFromUsername($excelData['Username']);
                            }
                        }
                    
                    }

                    if($excelData['user_id'] && $excelData['adjust_coin_reason'] && $excelData['coin_adjust_type'] && $excelData['adjust_coin_type'] && $excelData['adjust_balance_type'] && $excelData['adjust_coin'] && $excelData['adjust_coin_comment']){
                        $requestData[]= (object)$excelData;
                    }
                }
                if($requestData){
                    $validationResponse = $this->insertUpdateAdjustCoins($requestData);
                    if($validationResponse->getData()->status == 201){
                        foreach($requestData as $request){
                            $this->insertUpdateAdjustCoins($request);
                        }
                        return response()->json(['status'=>200, 'message'=>'success']);                
                    }else if($validationResponse->getData()->status == 301){
                        return $validationResponse;
                    }else{
                        return response()->json(['status'=>500, 'message'=>'Something Went wrong']);
                    }
                }else{
                    return response()->json(['status'=>301, 'message'=>'No Correct Data Available!']);
                }
            }
        }else{
            return response()->json(['status'=>301, 'message'=>'Transaction password not matched']);
        }

    }

    //Match Reasion Action
    public function getActionReasonId($reasion){
       return $result = ActionReason::select('ACTIONS_REASONS_ID','ACTIONS_REASON')
        ->whereIn('REASON_CATEGORY_ID',[1])
        ->where('ACTIONS_REASON',$reasion)
        ->active()->get();
    }
}
