<?php

namespace App\Http\Controllers\bo\users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Importer;
use App\Imports\UserImport;
use Illuminate\Support\Facades\DB;

class PasswordExpiry extends Controller {

    public function index(Request $request) {
        return view('bo.views.users.passwordexpiry.index');
    }

    public function verifydata(Request $request, Importer $importer) {
        try {
            $enable2SV = "No";
            $expirePwd = "No";
            if ($request->has('enable2SV')) {
                $enable2SV = "Yes";
            }
            if ($request->has('expirePwd')) {
                $expirePwd = "Yes";
            }
            if ($enable2SV == "No" && $expirePwd == "No") {
                return response()->json(['status' => 302, 'message' => 'Error.', 'data' => 'At least one should be enable from expire password or enable 2 step verification']);
            }
            if ($request->hasFile('excel_file')) {
                if ($request->file('excel_file')->extension() == 'xlsx') {
                    $userIds = "";
                    $importer->import($data = new UserImport, $request->excel_file);
                    /* $notExistflag = false; */
                    /* $userNameNotExist = ""; */
                    $responseData = array();
                    $uniqueUserNames = array();
                    $count = 0;
//array_push($responseData, ["#", "User ID", "Username", "Expire Password", "Enable 2 SV"]);
                    foreach ($data->data as $key => $excelData) {
                        $count++;
                        $userId = getUserIdFromUsername($excelData['Username']);
                        if (!$userId) {
                            /* $notExistflag = true; */
                            /* $userNameNotExist .= $excelData['Username'] . ","; */
                            array_push($responseData, ["userId" => "Not Found", "Username" => $excelData['Username'], "ExpirePwd" => $expirePwd, "Enable2SV" => $enable2SV, "status" => 'Invalid User']);
                        } else {
                            if (in_array($userId, $uniqueUserNames)) {
                                array_push($responseData, ["userId" => $userId, "Username" => $excelData['Username'], "ExpirePwd" => $expirePwd, "Enable2SV" => $enable2SV, "status" => 'Repeated Username']);
                            } else {
                                array_push($uniqueUserNames, $userId);
                                $userIds .= $userId . ',';
                                array_push($responseData, ["userId" => $userId, "Username" => $excelData['Username'], "ExpirePwd" => $expirePwd, "Enable2SV" => $enable2SV, "status" => 'Ok']);
                            }
                        }
                    }
                    if ($count > 0) {
                        return response()->json(['status' => 200, 'message' => 'Data validated successfully', "userIds" => $userIds, "Enable2SV" => $enable2SV, "ExpirePwd" => $expirePwd, 'data' => (object) $responseData]);
                    } else {
                        return response()->json(['status' => 302, 'message' => 'Error.', 'data' => 'None of the uploaded usernames match to database. Kindly re-check and upload again']);
                    }
                } else {
                    return response()->json(['status' => 302, 'message' => 'Only excel file is allowed', 'data' => 'Current File ' . $request->file('excel_file')->getClientOriginalExtension()]);
                }
            } else {
                return response()->json(['status' => 500, 'message' => 'Data changed. Re-select file.']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }

    public function update(Request $request) {
        $userIdsArr = explode(',', rtrim($request->userIds, ","));
        /* return response()->json(['status' => 500, 'message' => count($userIdsArr)]); */
        if (count($userIdsArr) <= 0) {
            return response()->json(['status' => 302, 'message' => 'Unable to fetch user ids. Kindly refresh and try again.']);
        } elseif (count($userIdsArr) > 500) {
            return response()->json(['status' => 302, 'message' => 'Maximum 500 user ids allowed to updated this data at a time.']);
        } else {
            try {
                if ($request->Enable2SV == "No" && $request->ExpirePwd == "No") {
                    return response()->json(['status' => 302, 'message' => 'Error.', 'data' => 'At least one should be enable from expire password or enable 2 step verification']);
                }
                if ($request->Enable2SV == "Yes") {
                    DB::table('user')->whereIn('USER_ID', $userIdsArr)->where('TWO_STEP_VERIFICATION', '!=', '2')->update(['TWO_STEP_VERIFICATION' => 1]);
                }
                if ($request->ExpirePwd == "Yes") {
                    DB::table('user')->whereIn('USER_ID', $userIdsArr)->update(['PASSWORD_EXPIRED' => 1]);
                }
                return response()->json(['status' => 200, 'message' => 'Data updated successfully. Total number records affected: ' . count($userIdsArr)]);
            } catch (\Exception $e) {
                return response()->json(['status' => 500, 'message' => $e->getMessage()]);
            }
        }
    }

}
