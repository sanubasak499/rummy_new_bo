<?php

namespace App\Http\Controllers\bo\staticblockmodule;

use App\Http\Controllers\bo\BaseController;
use Illuminate\Http\Request;
use App\Models\StaticBlock;
use DB;
use Auth;
use Carbon\Carbon;
use App\Facades\PokerBaazi;

/**
 * Author: Nitesh Kumar Jha
 * Purpose: Get all funtion related to related to static block model which are use into website management
 * Created Date: 8-9-2020 
 */
class StaticblockController extends BaseController
{


    public function __construct()
    {
    }


    // View Static Block
    public function index()
    {
        return view('bo.views.staticblockmodule.index');
    }

    // Insert 
    public function insertStaticblock(Request $request)
    {
        request()->page;
        $request->validate([
            "BLOCK_ID" => 'required',
            "TITLE"    => 'required',
            "DESCRIPTION" => 'required',

        ]);

        $insert = [
            'BLOCK_ID' => $request->BLOCK_ID,
            'TITLE' => $request->TITLE,
            'DESCRIPTION' => $request->DESCRIPTION,
            'CREATED_DATE' => DB::raw('NOW()'),
            'UPDATED_BY' => Auth::user()->username
        ];

        $data = StaticBlock::insert($insert);
        // Start User Activity Tracking 
        $activity = [
            'admin' => \Auth::user()->id,
            'module_id' => 93,
            'action' => "Add Static Block",
            'data' => json_encode($insert)
        ];
        PokerBaazi::storeActivity($activity);
        //End User Activity Tracking

        if ($data == 1) {
            return redirect()->back()->with('success', "Static Block Module has been successfully Inserted.");
        } else {
            return redirect()->back()->with('error', "Static Block Module could not be Inserted. Please try again.");
        }
    }


    public function checkStaticBlockTitle(Request $request)
    {
        if (StaticBlock::where('BLOCK_ID', $request->BLOCK_ID)->first()) {
            return response()->json(['status' => 200]);
        } else {
            return response()->json(['status' => 201]);
        }
    }

    // Mannage Static block list
    public function StaticblockList()
    {

        return view('bo.views.staticblockmodule.StaticblockList');
    }

    public function StaticBlockResult()
    {
        $StaticBlockResult = StaticBlock::select('STATIC_BLOCK_ID', 'BLOCK_ID', 'TITLE', 'DESCRIPTION')
            ->orderBy('STATIC_BLOCK_ID', 'DESC')
            ->get();
        return $StaticBlockResult;
    }


    // Edit static block
    public function editStaticBlock($STATIC_BLOCK_ID)
    {

        $StaticBlockResult = StaticBlock::select('STATIC_BLOCK_ID', 'BLOCK_ID', 'TITLE', 'DESCRIPTION')
        ->find($STATIC_BLOCK_ID);

        return view('bo.views.staticblockmodule.EditStaticBlock', ['StaticBlockResult' => $StaticBlockResult]);
    }


    // Update Static block
    public function updateStaticBlock($STATIC_BLOCK_ID, Request $request)
    {
        request()->page;
        $request->validate([
            "BLOCK_ID" => 'required',
            "TITLE"    => 'required',
            "DESCRIPTION" => 'required',

        ]);
        $StaticBlockCount = StaticBlock::where('BLOCK_ID', $request->BLOCK_ID)
            ->where('STATIC_BLOCK_ID', '!=', $STATIC_BLOCK_ID)
            ->groupBy('BLOCK_ID')
            ->count();

        if ($STATIC_BLOCK_ID != '' && $StaticBlockCount == 0) {

            $update = [
                'BLOCK_ID' => $request->BLOCK_ID,
                'TITLE' => $request->TITLE,
                'DESCRIPTION' => $request->DESCRIPTION,
                'UPDATED_BY' => Auth::user()->username
            ];

            $data = StaticBlock::where('STATIC_BLOCK_ID', $STATIC_BLOCK_ID)
                ->update($update);
            // Start User Activity Tracking 
            $activity = [
                'admin' => Auth::user()->id,
                'module_id' => 93,
                'action' => "Update Static block",
                'data' => json_encode($update)
            ];

            PokerBaazi::storeActivity($activity);
            //End User Activity Tracking
            if ($data == 1) {
                return redirect()->back()->with('success', "Static block has been successfully uploaded.");
            } else {
                return redirect()->back()->with('error', "Static block could not be updated. Please try again.");
            }
            return redirect()->back()->with('error', "Static block could not be updated. Please try again.");
        } else {
            return redirect()->back()->with('error', "Static block id already exists. Please try again.");
        }
    }

    public function searchResult(Request $request)
    {

        $page = request()->page;
        $perPage = config('poker_config.paginate.per_page');
        if (!$request->isMethod('post')) {
            return view('bo.views.staticblockmodule.StaticblockList');
        }

        $BLOCK_ID = $request->BLOCK_ID;
        $TITLE = $request->TITLE;

        $StaticBlockResult = StaticBlock::select('STATIC_BLOCK_ID', 'BLOCK_ID', 'TITLE', 'DESCRIPTION')

            ->when(!empty($BLOCK_ID), function ($query) use ($BLOCK_ID) {
                return $query->where('BLOCK_ID', 'LIKE', "%{$BLOCK_ID}%");
            })

            ->when(!empty($TITLE), function ($query) use ($TITLE) {
                return $query->where('TITLE', 'LIKE', "%{$TITLE}%");
            })

            ->orderBy('STATIC_BLOCK_ID', 'DESC')
            ->paginate($perPage, ['*'], 'page', $page);
        $params = $request->all();
        $params['page'] = $page;
        return view('bo.views.staticblockmodule.StaticblockList', ['StaticBlockResult' => $StaticBlockResult, 'params' => $params]);
    }
}
