<?php

namespace App\Http\Controllers\bo\notification;

use Illuminate\Http\Request;
use App\Http\Controllers\bo\BaseController;
use Carbon\Carbon;
use App\Models\SegmentType;
use App\Models\SegmentToUser;
use Session;
use DB;
use Auth;
use App\Facades\PokerBaazi;
use App\Imports\UserImport;
use Maatwebsite\Excel\Importer;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Facades\Excel;


/**
 * Author: Nitesh Kumar Jha
 * Purpose: add Manage Segment title description  and import username in excel formate and list manage sigment data and export segment to user 
 * Created Date: 28-2-2020
 */

class ManageSegmentsController extends BaseController
{
    //
    public function index()
    {
        //echo "hello";die();
        return view('bo.views.notification.managesegment.index');
    }

    public function createSegments()
    {
        return view('bo.views.notification.managesegment.createSegments');
    }
    public function insertCreateSegments(Request $request, Importer $importer)
    {

        request()->page;
        $SEGMENT_TITLE = $request->SEGMENT_TITLE;
        $SEGMENT_DESCRIPTION = $request->SEGMENT_DESCRIPTION;
        $STATUS = 1;
        $CREATED_BY = Auth::user()->id;

        $insert = [
            'SEGMENT_TITLE' => $SEGMENT_TITLE,
            'SEGMENT_DESCRIPTION' => $SEGMENT_DESCRIPTION,
            'STATUS' => $STATUS,
            'CREATED_BY' => $CREATED_BY,
            'CREATED_DATE' => DB::raw('NOW()')
        ];
     
        try {
            DB::transaction(function () use ($insert, $request, $importer) {
                
                $LastInsertedId = SegmentType::insertGetId($insert);
                //Start user Activity Tracking 
                $activity = [
                    'admin' => Auth::user()->id,
                    'module_id' => 73,
                    'action' => "Add Segment type",
                    'data' => json_encode($insert)
                ];
                PokerBaazi::storeActivity($activity, $request);
                $importer->import($data = new UserImport, $request->excel_file);
                $data = $data;
                $notExistflag = false;
                foreach ($data->data as $key => $excelData) {
                    $excelData = array_change_key_case($excelData, CASE_LOWER);
                    $UserId = getUserIdFromUsername($excelData['username']);
                    if (trim($excelData['username']) != '') {
                        if (!$UserId) {
                            $notExistflag = true;
                            $wrongusername[] =  $excelData['username'];
                        }
                    }

                    $LastInsertedId = $LastInsertedId;
                    $userNameNotExist = $UserId;
                    if ($UserId) {
                        $insertSegment[$UserId] = [
                            'SEGMENT_ID' => $LastInsertedId,
                            'USER_ID' => $userNameNotExist
                        ];
                    }
                }

                SegmentToUser::insert($insertSegment);
                $activity = [
                    'admin' => Auth::user()->id,
                    'module_id' => 73,
                    'action' => "Add Segment To User",
                    'data' => json_encode($insertSegment)
                ];
                PokerBaazi::storeActivity($activity);
                // //End User Activity Tracking

            }, 5);
            return response()->json(['status' => 200, 'message' => 'success', 'data' => '']);
        } catch (Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }



    // Load  search result
    public function search(Request $request)
    {

        $page = request()->page;
        $perPage = config('poker_config.paginate.per_page');
        if (!$request->isMethod('post')) {
            return view('bo.views.notification.managesegment.index');
        }

        $SEGMENT_TITLE = $request->SEGMENT_TITLE;
        $SEGMENT_DESCRIPTION = $request->SEGMENT_DESCRIPTION;

        $ManageSegmentsResult = SegmentType::from(app(SegmentType::class)->getTable() . " as st")
            ->select(
                'st.SEGMENT_ID',
                'st.SEGMENT_TITLE',
                'st.SEGMENT_DESCRIPTION',
                'st.STATUS',
                'st.CREATED_BY',
                'st.CREATED_DATE',
                'bou.username'
            )
            ->join('bo_admin_users as bou', 'bou.id', '=', 'st.CREATED_BY')

            ->when(!empty($SEGMENT_TITLE), function ($query) use ($SEGMENT_TITLE) {
                return $query->where('st.SEGMENT_TITLE', 'like', '%' . $SEGMENT_TITLE . '%');
            })

            ->when(!empty($SEGMENT_DESCRIPTION), function ($query) use ($SEGMENT_DESCRIPTION) {
                return $query->where('st.SEGMENT_DESCRIPTION', 'like', '%' . $SEGMENT_DESCRIPTION . '%');
            })

            ->when(!empty($request->date_from), function ($query) use ($request) {
                $dateFrom = Carbon::parse($request->date_from)->format('Y-m-d H:i:s');
                return $query->whereDate('st.CREATED_DATE', ">=", $dateFrom);
            })
            ->when(!empty($request->date_to), function ($query) use ($request) {

                $dateTo = Carbon::parse($request->date_to)->format('Y-m-d H:i:s');
                return $query->whereDate('st.CREATED_DATE', "<=", $dateTo);
            })
            ->when(!empty($request->STATUS), function ($query) use ($request) {
                $STATUS = $request->STATUS == 2 ? 0 : 1;
                return $query->where('st.STATUS', $STATUS);
            })
            ->orderBy('st.SEGMENT_ID', 'DESC')
            ->paginate($perPage, ['*'], 'page', $page);
        $params = $request->all();
        $params['page'] = $page;

        return view('bo.views.notification.managesegment.index', ['ManageSegmentsResult' => $ManageSegmentsResult, 'params' => $params]);
    }

    // change status of  manage segments

    public function manageSegmentsChangeStatus(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'STATUS' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->all()]);
        }
        $statusChange = SegmentType::find($request->id);
        $status = $statusChange->STATUS = ($request->STATUS) ? 1 : 0;
        $statusChange->save();

        $activity = [
            'admin' => Auth::user()->id,
            'module_id' => 73,
            'action' => "manage segments type Status Change",
            'data' => json_encode($status)
        ];
        PokerBaazi::storeActivity($activity);

        Session::flash('message', 'Status ' . (($request->STATUS) ? "enabled" : "disabled") . ' successfully');
        return response()->json(['status' => true, 'message' => 'Status ' . (($request->STATUS) ? "<b><i>enabled</i></b>" : "<b><i>disabled</i></b>") . ' successfully']);
    }


    public function chaakeSegmentTitle(Request $request)
    {
        if (SegmentType::where('SEGMENT_TITLE', $request->SEGMENT_TITLE)->first()) {
            return response()->json(['status' => 200]);
        } else {
            return response()->json(['status' => 201]);
        }
    }
    // Export To Excel 
    public function exportExcel($SEGMENT_ID)
    {

        $segmentTypeTitle = SegmentType::from(app(SegmentType::class)->getTable() . " as st")
            ->select('st.SEGMENT_TITLE', 'st.CREATED_DATE')
            ->where('st.SEGMENT_ID', $SEGMENT_ID)
            ->first();
        $SEGMENT_TITLE = $segmentTypeTitle['SEGMENT_TITLE'];
        $CREATED_DATE = $segmentTypeTitle['CREATED_DATE'];


        $newArray = SegmentToUser::from(app(SegmentToUser::class)->getTable() . " as sto")
            ->select('sto.USER_ID', 'u.USERNAME')
            ->join('user as u', 'u.USER_ID', '=', 'sto.USER_ID')
            ->where('sto.SEGMENT_ID', $SEGMENT_ID)
            ->orderBy('sto.SEGMENT_USER_ID', 'DESC')
            ->get();

        $data[0] = array("USER_ID" => "", "USERNAME" => "");
        $name = $SEGMENT_TITLE . '-' . $CREATED_DATE;
        $fileName = $name . ".xlsx";
        $excelData = $newArray;
        $headings = array_keys($data[0]);
        return Excel::download(new CollectionExport($excelData, $headings), $fileName);
    }
    public function filterArr($data)
    {
        return $data;
    }


    public function SegmentMatchExcel(Request $request, Importer $importer)
    {
        try {
            if ($request->hasFile('excel_file')) {

                if ($request->file('excel_file')->extension() == 'xlsx') {
                    $importer->import($datanew = new UserImport, $request->excel_file);
                    $notExistflag = false;

                    $arrayvalue = array_keys($datanew->data[1]);
                    $HeadingCount = count($arrayvalue);
                    if (strtolower($arrayvalue[0]) != 'username' || $HeadingCount > 1) {
                        return response()->json(['status' => 304, 'message' => 'Heading Must Be "Username" and not more then one', 'data' => 'Current File ']);
                    }

                    if (empty($datanew->data)) {

                        return response()->json(['status' => 303, 'message' => 'Data is empty.', 'data' => 'Current File ']);
                    }

                    if (strtolower($arrayvalue[0]) != 'username') {
                        return response()->json(['status' => 301, 'message' => 'Heading Must Be "Username"', 'data' => 'Current File ' . $request->file('excel_file')->extension()]);
                    }

                    foreach ($datanew->data as $key => $excelData) {
                        $excelData = array_change_key_case($excelData, CASE_LOWER);
                        if (trim($excelData['username']) != '') {
                            if (!($userId = getUserIdFromUsername(trim($excelData['username'])))) {
                                $notExistflag = true;
                                $userIdsnew[] = 'Not Found';
                                $userNameNotExistnew[] = trim($excelData['username']);
                                $flagsnew[] = false;
                            } else {
                                $userIdsnew[] = getUserIdFromUsername(trim($excelData['username']));
                                $userNameNotExistnew[] = trim($excelData['username']);
                                $flagsnew[] = true;
                            }
                        }
                    }

                    return response()->json(['status' => 200, 'message' => 'success', 'data' => $userNameNotExistnew, 'flags' => $flagsnew, 'user_ids' => $userIdsnew]);
                } else {
                    return response()->json(['status' => 302, 'message' => 'Only excel file accept', 'data' => 'Current File ' . $request->file('excel_file')->extension()]);
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
    }
}
