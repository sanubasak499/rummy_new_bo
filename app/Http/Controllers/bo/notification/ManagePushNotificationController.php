<?php

namespace App\Http\Controllers\bo\notification;

use App\Exports\CollectionExport;
use App\Http\Controllers\bo\BaseController;
use Illuminate\Http\Request;
use App\Models\PushNotificationToUser;
use App\Models\PushNotification;
use App\Imports\UserImport;
use App\Models\SegmentType;
use App\Models\SegmentToUser;
use App\Models\FirebaseToken;
use App\Models\Tournament;
use Maatwebsite\Excel\Importer;
use App\Traits\s3Bucket;
use App\Traits\Models\common_methods;
use Illuminate\Support\Facades\Validator;
use DB;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ManagePushNotificationController extends BaseController
{

    use s3Bucket, common_methods;
    
    protected $perPage;

    protected $queryObj;

    public function __construct(){
        parent::__construct();
        $this->perPage = config('poker_config.paginate.per_page');
    }

    public function index(Request $request){
        $data['segments'] = SegmentType::active()->get();
        $data['pushedByUsers'] = PushNotification::select('PUSHED_BY')->distinct()->get();
        $data['tournaments'] = Tournament::from(app(Tournament::class)->getTable()." AS t")
            ->distinct('p.TOURNAMENT_ID')
            ->select('t.TOURNAMENT_ID', 't.TOURNAMENT_NAME')
            ->join(app(PushNotification::class)->getTable()." AS p", 'p.TOURNAMENT_ID', '=', 't.TOURNAMENT_ID')
            ->get();
        
        if(! $request->isMethod('post')){
            return view('bo.views.notification.push_notification.index', $data);
        }
        $page = request()->page;
        
        $this->buildConditionalQuery($request);
        // $statsQueryObj = clone $this->queryObj;

        $data['notifications'] = $this->filerDataObj($request)->orderBy('PUSH_NOTIFICATION_ID','desc')->paginate($this->perPage,['*'],'page',$page);
        // $data['count'] = $statsQueryObj->select(DB::raw("count(PUSH_NOTIFICATION_ID) as `total`"))->pluck('total')->first() ?? 0;
        $data['params'] = $request->all();
        $data['params']['page'] = $page;
        return view('bo.views.notification.push_notification.index', $data);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data=PushNotification::findOrFail($id);
            $data->getSegments();
            $data->getTournaments();
            return response()->json(['status'=>200, 'message'=>'success','data'=>$data]);
        } catch (\Exception $e) {
            return response()->json(['status'=>500, 'message'=>$e->getMessage()],500);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function exportExcel($id, $seg_id=null)
    {
        try {
            $data=PushNotification::findOrFail($id);
            if($data->SENT_TO==1){
                if(!empty($seg_id)){
                    $userList=SegmentToUser::from(app(SegmentToUser::class)->getTable().' AS stu')
                        ->select('u.USER_ID','u.USERNAME')
                        ->join('user AS u', 'stu.USER_ID','=','u.USER_ID')
                        ->where('stu.SEGMENT_ID', $seg_id)
                        ->get();
                    return $this->UserListExport($userList,"SegmentExport".date("Y-m-d-H-i-s").".xlsx");
                }else{
                    return back()->with('error', "Something went wrong");
                }
            }elseif($data->SENT_TO==2){
                $userList=PushNotificationToUser::from(app(PushNotificationToUser::class)->getTable().' AS pntu')
                    ->select('u.USER_ID','u.USERNAME')
                    ->join('user AS u', 'pntu.USER_ID','=','u.USER_ID')
                    ->where('pntu.PUSH_NOTIFICATION_ID', $data->PUSH_NOTIFICATION_ID)
                    ->get();
                    
                return $this->UserListExport($userList,"UserListExport".date("Y-m-d-H-i-s").".xlsx");  
            }else{
                return back()->with('error', "Something went wrong");
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>500, 'message'=>$e->getMessage()],500);
        }
    }

    private function UserListExport($userList, $fileName=null){
        if(count($userList)>0){
            foreach($userList as $key => $item){
                $item['USER_ID']=$item->USER_ID;
                $item['USERNAME']=$item->USERNAME;
            }

            $heading = collect($userList->first()->toArray())->keys()->toArray();
            $fileName = $fileName ?? "UsersListExport".date("Y-m-d-H-i-s").".xlsx";
            return Excel::download(new CollectionExport($userList,$heading), $fileName);
        }else{
            return back()->with('info',"The segment does not contain any user.");
        }
    }

    public function buildConditionalQuery($request){
        
        $this->queryObj = PushNotification::query();
        $this->queryObj->when(!empty($request->PUSH_NOTIFICATION_NAME), function($query) use ($request){
            return $query->where('PUSH_NOTIFICATION_NAME', 'LIKE', "%{$request->PUSH_NOTIFICATION_NAME}%");
        })
        ->when(!empty($request->PUSH_NOTIFICATION_TITLE), function($query) use ($request){
            return $query->where('PUSH_NOTIFICATION_TITLE','LIKE', "%{$request->PUSH_NOTIFICATION_TITLE}%");
        })
        
        ->when(!empty($request->PUSHED_BY), function($query) use ($request){
            return $query->where('PUSHED_BY', $request->PUSHED_BY);
        })
        ->when(!empty($request->REDIRECTION_TYPE_ID) && $request->REDIRECTION_TYPE_ID != 0, function($query) use ($request){
            return $query
            ->when(!empty($request->REDIRECTION_URL) && $request->REDIRECTION_TYPE_ID == 1, function($q) use ($request){
                return $q->when(!empty($request->REDIRECTION_URL), function($que) use ($request){
                    return $que->where('REDIRECTION_URL','LIKE', "%{$request->REDIRECTION_URL}%");
                });
            })->when(!empty($request->TOURNAMENT_ID) && $request->REDIRECTION_TYPE_ID == 2, function($q) use ($request){
                return $q->when(!empty($request->TOURNAMENT_ID), function($que) use ($request){
                    return $que->where('TOURNAMENT_ID', $request->TOURNAMENT_ID);
                });
            })
            ->where('REDIRECTION_TYPE_ID', $request->REDIRECTION_TYPE_ID);
        })
        ->when($request->SENT_TO !=null && (!empty($request->SENT_TO) || $request->SENT_TO == 0), function($query) use ($request){
            return $query->when($request->SENT_TO == "1" && count($request->SEGMENT_IDS ?? [])>0, function($q) use ($request){
                $segments = $request->SEGMENT_IDS;
                return $q->where(function($que) use ($segments){
                    foreach($segments as $k => $v){
                        if($k==0){
                            $que->whereRaw(\DB::raw("FIND_IN_SET($v, SEGMENT_IDS)"));
                        }else{
                            $que->orWhereRaw(\DB::raw("FIND_IN_SET($v, SEGMENT_IDS)"));
                        }
                    }
                });
            })
            ->where('SENT_TO', $request->SENT_TO);
        })
        ->when(!empty($request->date_from), function($query) use ($request){
            $date_from = Carbon::parse($request->date_from);
            return $query->whereDate('PUSHED_DATE', ">=", $date_from);
        })
        ->when(!empty($request->date_to), function($query) use ($request){
            $date_to = Carbon::parse($request->date_to);
            return $query->whereDate('PUSHED_DATE', "<=", $date_to);
        });
        return $this->queryObj;
    }
    
    public function filerDataObj($request){
        return $this->queryObj->select("PUSH_NOTIFICATION_ID","PUSHED_DATE","PUSH_NOTIFICATION_NAME","PUSH_NOTIFICATION_TITLE","NOTIFICATION_BODY","SENT_TO","PUSHED_BY");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
       $segmentTypes = SegmentType::active()->get(); 
       $tournaments = app(Tournament::class)->getAllUpcomingTournaments(); 
       return view('bo.views.notification.push_notification.create',['segmentTypes' => $segmentTypes, 'tournaments' => $tournaments]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $dataVal = $request->all();
        $dataVal = array_change_key_case($dataVal,CASE_UPPER);
        $dataVal['PUSH_NOTIFICATION_NAME'] = base64_decode($dataVal['PUSH_NOTIFICATION_NAME']);
        $dataVal['PUSH_NOTIFICATION_TITLE'] = base64_decode($dataVal['PUSH_NOTIFICATION_TITLE']);
        $dataVal['NOTIFICATION_BODY'] = base64_decode($dataVal['NOTIFICATION_BODY']);
        $dataVal = array_change_key_case($dataVal,CASE_LOWER);
        $validator = Validator::make($dataVal, [
            'push_notification_title' => 'required|string',
            'push_notification_name' => 'required|string',
            'notification_body' => 'required|string',
            'redirection_url' =>'required_if:redirection_type_id,1' ,
            'sent_to' => 'required|numeric',
            'segment_ids' => 'required_if:sent_to,1',
            'upload_user' => 'required_if:sent_to,2'
        ]);
        
        /*
        Getting Distinct User Ids From Segment Type
        */
        if ($validator->fails()) {
            return response()->json(['status' => '200' ,'msg' => 'Validation failed.', 'type' => 'error']);
        }

        
        if(!$request->has('upload_user') && $request->sent_to!=0){
            $segmentToUser = SegmentToUser::usersIdFromSegmentId(explode(',',$request->segment_ids))->pluck('USER_ID')->toArray();
            $sentToFlag = false;
            /*
            Fetching FireBase Token By User ID
            */
            $tokens = FirebaseToken::tokenByUserId($segmentToUser)->pluck('TOKEN')->toArray();
        }
        elseif($request->sent_to==2){
            $segmentToUser = $this->reviewExcel($request,app(Importer::class), true);
            $sentToFlag = false;
            /*
            Fetching FireBase Token By User ID
            */

            $tokens = FirebaseToken::tokenByUserId($segmentToUser)->pluck('TOKEN')->toArray();


            if(empty($tokens)){
                return response()->json(['status' => '200' ,'msg' => 'Failed to send notification,Tokens not found', 'type' => 'error']);
            }
        }
        else{
            $sentToFlag = true;
            $segmentToUser = [];
            $tokens=[];
        }
        
        /*
        Fetching Server Id (On which tournament will run) Based On Tournament Id
        */
        $tournamentId = "";
        $serverId = "";
        $serverURL = "";
        if($request->has('tournament_id') && $request->tournament_id!=null){
            $tournamentDetails = app(Tournament::class)->tournamentDetailsWithServerDetails($request->tournament_id)->first();
            $tournamentId = $tournamentDetails->TOURNAMENT_ID;
            $serverId = $tournamentDetails->SERVER_ID;
            $serverURL = $tournamentDetails->SERVER_URL;

            // $tournamentId = 205477;
            // $serverId = 1;
            // $serverURL = "qa.alphabetabox.com:7000";
        }


        /*
        Uploading Image
        */
        if($request->hasFile('image_path')){
            $file = $request->file('image_path');
            $allowedFormats = array("Jpeg", "jpeg", "JPEG", "JPG", "jpg", "png", "PNG","gif","GIF");
            $filePath = 'push_notification/images/';
            $hasFile = $request->hasFile('image_path');
            $imageResult = $this->s3FileUpload($file, $filePath, $allowedFormats, $hasFile);

            if(strtoupper($imageResult->message)=='SUCCESS'){
                $imagePath = $imageResult->url;
            }
            else{
                return response()->json(['status' => '200' ,'msg' => 'Failed to upload image', 'type' => 'error']);
            }
        }    

        
        $insertData = $request->all();

        $insertData= array_change_key_case($insertData,CASE_UPPER);
        $insertData['PUSH_NOTIFICATION_NAME'] = base64_decode($insertData['PUSH_NOTIFICATION_NAME']);
        $insertData['PUSH_NOTIFICATION_TITLE'] = base64_decode($insertData['PUSH_NOTIFICATION_TITLE']);
        $insertData['NOTIFICATION_BODY'] = base64_decode($insertData['NOTIFICATION_BODY']);
        $insertData['IS_EXTERNAL'] = strtoupper($request->is_external)=='ON' ? 1 : 0;
        $insertData['IS_RELATIVE'] = strtoupper($request->is_relative)=='ON' ? 1 : 0;
        $insertData['SEGMENT_IDS'] = !empty($request->segment_ids) ? $request->segment_ids : '';
        $insertData['IMAGE_PATH'] = $imagePath ?? '';
        $insertData['UPDATED_BY'] = $insertData['PUSHED_BY'] = \Auth::user()->username;
        $insertData['UPDATE_DATE'] = $insertData['CREATED_DATE'] = $insertData['PUSHED_DATE'] = date('Y-m-d H:i:s');
        
        

        DB::transaction(function () use ($insertData,$segmentToUser,$sentToFlag) {
            $push = new PushNotification($insertData);
            $push->save();
            $insertId = $push->PUSH_NOTIFICATION_ID;

            if(!$sentToFlag){
                foreach ($segmentToUser as $key => $value) {
                    $insertByUser = array();
                    $insertByUser['PUSH_NOTIFICATION_ID'] = $insertId;
                    $insertByUser['USER_ID'] = $value;
                    $pushToUser = new PushNotificationToUser($insertByUser);
                    $pushToUser->save();
                }
            }    

        }, 3);
      
        $payLoadObject = $this->payLoadJsonGenerator(
            $tokens,
            base64_decode($request->push_notification_title),
             base64_decode($request->notification_body),
             $request->redirection_type_id,
             $request->redirection_url ?? '',
             $serverURL,
             $tournamentId ?? '',
             $serverId ?? '',
             $request->has('is_relative') && strtoupper($request->is_relative)=='ON' ? '1' : '0',
             $request->has('is_external') && strtoupper($request->is_external)=='ON' ? '1' : '0',
             $imagePath ?? ''
         );

        if(!$sentToFlag){
            if($this->pushNotification($payLoadObject)){
                return response()->json(['status' => '200' ,'msg' => 'Notification Sent Successfully.', 'type' => 'success']);
            }
            else{
               return response()->json(['status' => '200' ,'msg' => 'Failed to send notification.', 'type' => 'error']);
            }
        }
        else{
            return response()->json(['status' => '200' ,'msg' => 'Saved for later successfully.', 'type' => 'success']);
        }   

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = decrypt($id);
            $data['segmentTypes'] = SegmentType::active()->get(); 
            $data['tournaments'] = app(Tournament::class)->getAllUpcomingTournaments();
            $push_notification=PushNotification::findOrFail($id);
            $params= array_change_key_case($push_notification->toArray(),CASE_LOWER);
            $params['segment_ids'] = array_map('trim', explode(',', $params['segment_ids']));
            $data['params']=$params;
            
            return view('bo.views.notification.push_notification.create',$data);
        } catch (\Throwable $th) {
            return back()->with('error', "Something went wrong");
        }
    }

    
    private function payLoadJsonGenerator($tokens=array(),$notificationTitle="",$notificationDescription="",$redirectionTypeID="0",$redirectionURL="", $serverUrl="",$tournamentID="",$serverID="",$relative="",$external="",$imageUrl=""){

        $notificationData['redirectionTypeID'] =  (String)$redirectionTypeID;
        $notificationData['tournamentID'] =  (String)$tournamentID;
        $notificationData['serverID'] =  (String)$serverID;
        $notificationData['serverURL'] =   (String)$serverUrl;
        $notificationData['redirectionURL'] =  (String)$redirectionURL;
        $notificationData['relative'] =  (String)$relative;
        $notificationData['external'] =  (String)$external;

        $payLoad['notificationTitle'] = (String)$notificationTitle;
        $payLoad['imageUrl'] = (String)$imageUrl;
        $payLoad['notificationDescription'] = (String)$notificationDescription;
        $payLoad['deviceToken'] = $tokens;
        $payLoad['notificationData']=$notificationData;

        return json_encode($payLoad);
    }
    
    private function pushNotification($payLoad){
        $url = config('poker_config.push_notification.send_url') ?? '';
        $ch = curl_init($url);

        # Setup request to send json via POST.
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payLoad );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        $result = curl_exec($ch);
        $errors = curl_error($ch);
        $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        /*removing tokens from payload*/
        $payLoad = json_decode($payLoad);
        unset($payLoad->deviceToken);
        $payLoad = json_encode($payLoad);
        /*removing tokens from payload end*/

        $trackingData = 'url: '.$url.' --- payLoad: '.$payLoad.' --- result: '.$result.' --- errors: '.$errors.' --- response: '.$response;
        $action = "PUSH_NOTIFICATION";
        $data   = json_encode($trackingData);
        \PokerBaazi::storeActivityWithParams($action, $data);

        # Send request.
        if($result){
            if($response==500)
                return false;
            else
                return true;
        }
        else{
            return false;
        }
    }

    public function reviewExcel(Request $request, Importer $importer, $onlyUserFlag=false){
        try{
            if($request->hasFile('upload_user')){
                if($request->file('upload_user')->extension() == 'xlsx'){
                        $importer->import($data = new UserImport, $request->upload_user);
                        $notExistflag=false;

                        foreach($data->data as $key => $excelData){
                            $excelData = array_change_key_case($excelData);
                            if(trim($excelData['username'])!='' || trim($excelData['username'])!=null){
                                if( !($userId = getUserIdFromUsername(trim($excelData['username'])))){
                                    $notExistflag=true;
                                    $onlyUserIds[]=0;
                                    $userIds[] = 'Not Found';
                                    $userNameNotExist[]=trim($excelData['username']);
                                    $flags[]=false;
                                }
                                else{

                                    $onlyUserIds[]=$userIds[] = getUserIdFromUsername(trim($excelData['username']));
                                    $userNameNotExist[]=trim($excelData['username']);
                                    $flags[]=true;
                                } 
                            }    

                        }

                        if($onlyUserFlag){
                            return $onlyUserIds;
                        }
                         
                        return response()->json(['status'=>200, 'message'=>'reviewed','data'=>$userNameNotExist,'flags' => $flags, 'user_ids' => $userIds ]);       
                }else {
                        return response()->json(['status'=>302, 'message'=>'Excel Format Mismatched','data'=>'Current File '.$request->file('upload_user')->extension()]);
                }
            }
        }catch(\Exception $e){
            return response()->json(['status'=>500, 'message'=>$e->getMessage()]);
        } 
    }

}
