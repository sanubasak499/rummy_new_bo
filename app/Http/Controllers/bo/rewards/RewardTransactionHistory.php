<?php
/*
    |--------------------------------------------------------------------------
    | Package Name: Reward Transaction search
    | Class Name: RewardTransactionHistory
	| Author: Dharminder Singh
	| Purpose: Track all rewards, claimed or switched program
	| Callers: 
	| Created Date: Dec 31 2019
	| Modified Date: 
	| Modified By: 
	| Modified Details: 
	|--------------------------------------------------------------------------
*/
namespace App\Http\Controllers\bo\rewards;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\RewardPointsConversionHistory;
use App\Models\User;
use App\Models\RewardTracking;
use App\Traits\Models\common_methods;
use Carbon\Carbon;
use App\Exports\CollectionExport;
use Maatwebsite\Excel\Facades\Excel;

class RewardTransactionHistory extends Controller
{
    public function index(Request $request)
    {
        if($request->isMethod('post'))
		{	
           
			$searhResult['params'] = $request->all();
			$searhResult['claimed_transactions'] = $this->getAllRewardTransactions($request);
			
			return view('bo.views.rewards.allRewardTransactionHistory',$searhResult);
		}
        return view('bo.views.rewards.allRewardTransactionHistory');
    }

    public function getAllRewardTransactions($request)
    {
        $perPage = config('poker_config.paginate.per_page');
        $rewardPointHistory = RewardPointsConversionHistory::query();
        $rewardPointHistory->from(app(RewardPointsConversionHistory::class)->getTable()." as rpch");
        $rewardPointHistory->leftJoin('user as u', 'u.USER_ID', '=', 'rpch.USER_ID');
        $rewardPointHistory->leftJoin('tournament as t', 't.TOURNAMENT_ID', '=', 'rpch.TOURNAMENT_ID');
        $rewardPointHistory->leftJoin('reward_tracking as rt', 'rt.INTERNAL_REFERENCE_NO', '=', 'rpch.INTERNAL_REFERENCE_NO');
    
        
        if(!empty($request->username)){
            $userId = getUserIdFromUsername($request->username);
            $rewardPointHistory->where('rpch.USER_ID' ,$userId);
        }

        if(!empty($request->INTERNAL_REFERENCE_NO)){
            $rewardPointHistory->where('rpch.INTERNAL_REFERENCE_NO',$request->INTERNAL_REFERENCE_NO);
        }
        
        if(!empty($request->REWARD_STATUS)){
            $rewardPointHistory->where('rpch.REWARD_STATUS',$request->REWARD_STATUS);
        }

        if(!empty($request->CUSTOM_PRICE_NAME)){
            if($request->CUSTOM_PRICE_NAME == "yes"){
                $rewardPointHistory->whereNotIn('rpch.CUSTOM_PRICE_NAME',[" ",'null','0']);
            }elseif($request->CUSTOM_PRICE_NAME == "no"){
                $rewardPointHistory->whereIn('rpch.CUSTOM_PRICE_NAME',[" ",'null','0']);
            }
        }
        
        if(!empty($request->date_from)){
            $rewardPointHistory->where('rpch.CREATED_DATE', ">=",Carbon::parse($request->date_from)->format('Y-m-d H:i:s'));
        }
        if(!empty($request->date_to)){
            $rewardPointHistory->where('rpch.CREATED_DATE', "<=",Carbon::parse($request->date_to)->format('Y-m-d H:i:s'));
        }

        if(!empty($request->TRACKING_STATUS)){
            $rewardPointHistory->where('rt.REWARD_TRACKING_STATUS_ID',$request->TRACKING_STATUS);
        }

        $rewardPointHistory->select('rpch.*','u.USERNAME','t.TOURNAMENT_NAME','rt.REWARD_TRACKING_STATUS_ID');
        $rewardPointHistory->orderBy('rpch.S_NO','DESC');
        return $rewardPointHistory->paginate($perPage, ['*'], 'page', $request->page);
    }

    public function export(Request $request)
    {
        
		if($request->isMethod('get'))
		{		
           $data = $this->getAllRewardTransactions($request);
           $excelData = array();
           foreach($data as $key => $row)
           {
            // $excelData[$key]['S_NO'] = $row->S_NO;
            $excelData[$key]['USERNAME'] = $row->USERNAME;
            $excelData[$key]['LEVEL_ID'] = $row->LEVEL_ID;
            $excelData[$key]['RP_CONVERTED'] = $row->RP_CONVERTED;
            $excelData[$key]['CASHBACK_AMOUNT'] = $row->CASHBACK_AMOUNT;
            $excelData[$key]['INTERNAL_REFERENCE_NO'] = $row->INTERNAL_REFERENCE_NO;
            $excelData[$key]['REAL_PRICE'] = $row->REAL_PRICE;
            $excelData[$key]['PROMO_PRICE'] = $row->PROMO_PRICE;
            $excelData[$key]['TOURNAMENT_NAME'] = $row->TOURNAMENT_NAME;
            $excelData[$key]['CUSTOM_PRICE_NAME'] = $row->CUSTOM_PRICE_NAME;
            switch ($row->REWARD_STATUS) {
                case 1:
                    $excelData[$key]['REWARD_STATUS'] = "LRP";
                    break;
                case 2:
                    $excelData[$key]['REWARD_STATUS'] = "CB";
                    break;
                case 3:
                    $excelData[$key]['REWARD_STATUS'] = "SWITCHED";
                    break;                
                default:
                $excelData[$key]['REWARD_STATUS'] = "Unknown/Status";
            }	
            $excelData[$key]['CREATED_DATE'] = Carbon::parse($row->CREATED_DATE)->format('Y-m-d H:i:s');
            // $excelData[$key]['UPDATED_DATE'] = Carbon::parse($row->UPDATED_DATE)->format('Y-m-d H:i:s');
            switch ($row->REWARD_TRACKING_STATUS_ID) {
                case 1:
                    $excelData[$key]['REWARD_TRACKING_STATUS_ID'] = "Order Recieved";
                    break;
                case 2:
                    $excelData[$key]['REWARD_TRACKING_STATUS_ID'] = "Processing Order";
                    break;
                case 3:
                    $excelData[$key]['REWARD_TRACKING_STATUS_ID'] = "Order Dispatched";
                    break;
                case 4:
                    $excelData[$key]['REWARD_TRACKING_STATUS_ID'] = "Order Delivered";
                    break;
                default:
                    $excelData[$key]['REWARD_TRACKING_STATUS_ID'] = "Unknown/Status";
            }
           }
           
           $fileName = $request->type.".xlsx";

           $headings = array_keys($excelData[0]);	
           return Excel::download(new CollectionExport($excelData,$headings), $fileName);
        }
    }

    public function getTrackingStatusByRefNo(Request $request)
    {
        if($request->isMethod('post'))
        {
            $refNo = $request->ref_no;
            $status = RewardTracking::where('INTERNAL_REFERENCE_NO',$refNo)->get();

            if(!empty($status[0])){
                $tStatus = $status[0]->REWARD_TRACKING_STATUS_ID;
                $response = "Success";
                return response()->json(['status' => 501, 'message' => $response, 'trackingStatus' => $tStatus]);
            }
            
        }
    }
    public function updateTrackingStatusByRefNo(Request $request)
    {
        if($request->isMethod('post'))
        {
            $refNo = $request->ref_no;
            $statusTobeUpdate = $request->newStatus;
            $trnPwd = $request->trans_pwd;
            $admin = \Auth::user()->id;
            $status = RewardTracking::where('INTERNAL_REFERENCE_NO',$refNo)->get();
            $currentStatus = $status[0]->REWARD_TRACKING_STATUS_ID;

            //check transaction password
            $pwdRes = \PokerBaazi::checkTransactionPassword($trnPwd);

            if ($pwdRes == FALSE) {
                $response = "Opps ! your transaction password is wrong .";
                //activity entry
                // $data['Wrong Transaction Password'] = "Wrong Transaction Password";
                // $action = "Wrong Transaction Password";
                // $this->insertAdminActivity($data, $action);
                return response()->json(['status' => 203, 'message' => $response]);
            }
            
            if($statusTobeUpdate <= $currentStatus || $statusTobeUpdate == " ") {
                $response = "Please select correct status.";
                return response()->json(['status' => 203, 'message' => $response]);
            }

            $updatePreviousWithdrawalCriteria = RewardTracking::where(['INTERNAL_REFERENCE_NO' => $refNo])->update(['REWARD_TRACKING_STATUS_ID' => $statusTobeUpdate, 'UPDATED_BY' => $admin]);
           
            if($updatePreviousWithdrawalCriteria){
                if($statusTobeUpdate == 2){
                    $response = "Tracking status updated successfully.";
                    return response()->json(['status' => 200, 'message' => $response]);
                }else if($statusTobeUpdate == 3){
                    $response = "Tracking status updated successfully.";
                    return response()->json(['status' => 201, 'message' => $response]);
                }else if($statusTobeUpdate == 4){
                    $response = "Tracking status updated successfully.";
                    return response()->json(['status' => 202, 'message' => $response]);
                }
                
            }
        
           
        }else {
            $response = "Updation Failed";
            return response()->json(['status' => 206, 'message' => $response]);
          }
    }
}
