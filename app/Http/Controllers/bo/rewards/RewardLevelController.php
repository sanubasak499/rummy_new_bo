<?php
/*
    |--------------------------------------------------------------------------
    | Package Name: LRP Configuration/LRP level search
    | Class Name: RewardLevelController
	| Author: Dharminder Singh
	| Purpose: Track all rewards, create and update existing rewards
	| Callers: 
	| Created Date: Dec 28 2019
	| Modified Date: 
	| Modified By: 
	| Modified Details: 
	|--------------------------------------------------------------------------
*/
namespace App\Http\Controllers\bo\rewards;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Redirect;
use App\Models\RewardLevel;
use App\Models\Tournament;
use DB;
use Input;
use Carbon\Carbon;

use Illuminate\Support\Facades\Storage;
use Session;

class RewardLevelController extends Controller
{

	public function index(Request $request)
	{
		if($request->isMethod('post'))
		{	
			$rewardLevels['params'] = $request->all();
			$rewardLevel = RewardLevel::query();
			if(!empty($request->prize_name)){
				$rewardLevel->where('CUSTOM_PRIZE', 'like', '%' . $request->prize_name . '%');
			}
			
			if(!empty($request->required_points)){
				$rewardLevel->where('REWARD_POINTS_REQUIRED',$request->required_points);
			}

			if(!empty($request->reward_status)){
				if($request->reward_status == 1){
					$rewardLevel->where('STATUS','1');
				}elseif($request->reward_status == 2){
					$rewardLevel->where('STATUS','0');
				}
			}
			
			if(isset($request->prize_type)){
				foreach($request->prize_type as $prize_type){
					
					if($prize_type == "REAL_PRIZE"){
						$rewardLevel->whereNotIn('REAL_PRIZE',[0]);

					}
					if($prize_type == "PROMO_PRIZE"){
						$rewardLevel->whereNotIn('PROMO_PRIZE',[0]);
					}
					if($prize_type == "CUSTOM_PRIZE_NAME"){
						$rewardLevel->whereNotNull('CUSTOM_PRIZE_NAME');
					}
				}								
			}
			
			if($request->searchByDate == "start"){

				$rewardLevel->whereBetween('START_DATE',[Carbon::parse($request->date_from)->format('Y-m-d H:i:s'),Carbon::parse($request->date_to)->format('Y-m-d H:i:s')]);
				
			}elseif($request->searchByDate == "end"){

				$rewardLevel->whereBetween('END_DATE',[Carbon::parse($request->date_from)->format('Y-m-d H:i:s'),Carbon::parse($request->date_to)->format('Y-m-d H:i:s')]);
				
			}
			
			$rewardLevel->where('STATUS',"!=",'2');
			$rewardLevel->orderBy('REWARD_POINTS_REQUIRED','ASC');
			$rewardLevels['rewards_level'] = $rewardLevel->get();
			
			return view('bo.views.rewards.allRewardLevel',$rewardLevels);
		}
		return view('bo.views.rewards.allRewardLevel');
	}


	public function updateRewardLevel(Request $Request,$id)
	{
		$LEVEL_ID = $id;
		$rewardLevelData = RewardLevel::where('LEVEL_ID', $LEVEL_ID)->get();
		$tournamentNames = app(Tournament::class)->getAllUpcomingTournaments();

		return view('bo.views.rewards.editRewardLevel', ['rewardLevelData' => $rewardLevelData, 'tournamentNames' => $tournamentNames,'LEVEL_ID'=>$LEVEL_ID]);
	}



	public function editRewardLevelfields(Request $request)
	{
		if ($request->frmSubmit == 'Update') {
			$folderName = config('poker_config.imageUrl.REWARD_IMAGE_FOLDER');
			// $LEVELID = decrypt($request->LEVEL_ID);
			$LEVELID = $request->LEVEL_ID;
			if ($request->hasFile('CUSTOM_PRIZE_IMAGE_LINK')) {
				$file = $request->file('CUSTOM_PRIZE_IMAGE_LINK');
				$randNum = rand(100, 999);
				$randAlpha = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), -1);
				$name = date('mdYHis') . $randNum . $randAlpha . '.' . $file->getClientOriginalExtension();
				$filePath = $folderName . $name;
				$allowedFormats = array("Jpeg", "jpeg", "JPEG", "JPG", "jpg", "png", "PNG");
				if (!array($filePath)) {

					$msg = "4"; //Invalid Format
					$request->session()->flash('image_error', 'Invalid Image format.');
					return Redirect::to("rewards/rewardConfig/updateRewardLevel/$LEVELID");					
				} else {
					$s3 = \Storage::disk('s3CDN')->getDriver();
					$s3->put($filePath, file_get_contents($file), array('ACL' => 'private'));
					$imageLink = $name;
					
				}
			}

			$StatusRewardLevel = RewardLevel::where('LEVEL_ID', $LEVELID)->select('STATUS','LEVEL_ORDER')->first();
		
			if ($request->CUSTOM_PRIZE_IMAGE_LINK) {
				$updatereward = RewardLevel::where('LEVEL_ID', $LEVELID)->update([
					'REWARD_POINTS_REQUIRED' => $request->REWARD_POINTS_REQUIRED,
					'TOURNAMENT_NAME' => $request->TOURNAMENT_NAME,
					'REAL_PRIZE' => $request->REAL_PRIZE,
					'PROMO_PRIZE' => $request->PROMO_PRIZE,
					'CUSTOM_PRIZE' => $request->CUSTOM_PRIZE,
					'CUSTOM_PRIZE_NAME' => $request->CUSTOM_PRIZE_NAME,
					'CUSTOM_PRIZE_DESC' => $request->CUSTOM_PRIZE_DESC,
					'CUSTOM_PRIZE_IMAGE_LINK' => $imageLink,
					'LEVEL_ORDER' => $StatusRewardLevel->LEVEL_ORDER,
					'STATUS' => $StatusRewardLevel->STATUS,
					'CREATED_DATE'	=>	date('Y-m-d h:i:s'),
					'UPDATED_DATE'	=>	date('Y-m-d h:i:s'),
					'START_DATE'	=>	date('Y-m-d H:i:s', strtotime($request->date_from)),
					'END_DATE'	=>	date('Y-m-d H:i:s', strtotime($request->date_to))
				]);
			} else {
				$updatereward = RewardLevel::where('LEVEL_ID', $LEVELID)->update([
					'REWARD_POINTS_REQUIRED' => $request->REWARD_POINTS_REQUIRED,
					'TOURNAMENT_NAME' => $request->TOURNAMENT_NAME,
					'REAL_PRIZE' => $request->REAL_PRIZE,
					'PROMO_PRIZE' => $request->PROMO_PRIZE,
					'CUSTOM_PRIZE' => $request->CUSTOM_PRIZE,
					'CUSTOM_PRIZE_NAME' => $request->CUSTOM_PRIZE_NAME,
					'CUSTOM_PRIZE_DESC' => $request->CUSTOM_PRIZE_DESC,
					'LEVEL_ORDER' => $StatusRewardLevel->LEVEL_ORDER,
					'STATUS' => $StatusRewardLevel->STATUS,
					'UPDATED_DATE'	=>	date('Y-m-d h:i:s'),
					'START_DATE'	=>	date('Y-m-d H:i:s', strtotime($request->date_from)),
					'END_DATE'	=>	date('Y-m-d H:i:s', strtotime($request->date_to))
				]);
			}

			if ($updatereward) {
				return  Redirect::back()->with('rewardupdatemessage', 'Reward Level Updated successfully.');
			} else {
				return  Redirect::back()->with('rewardupdatemessageerror', 'Reward Level Updated UnSuccessfully.');
			}
		}
	}


	public function addrewardlevel()
	{
		$tournaments = app(Tournament::class)->getAllUpcomingTournaments();
		return view('bo.views.rewards.addRewardLevel', ['tournamentNames' => $tournaments]);
	}



	public function updateRewardLevelfields(Request $request)
	{
		
		$validator = \Validator::make($request->all(), [			
			'REWARD_POINTS_REQUIRED' => 'required|numeric',
			'CUSTOM_PRIZE' => 'required',
			'CUSTOM_PRIZE_DESC' => 'required',
			'REAL_PRIZE' => 'nullable|numeric',
			'PROMO_PRIZE' => 'nullable|numeric',
			'CUSTOM_PRIZE_IMAGE_LINK' =>'required|image|mimes:jpeg,png,jpg,gif',
			'date_from' => 'required',
			'date_to' => 'required'
		],[
			'REWARD_POINTS_REQUIRED.required' => 'The Required Reward points field is required.',
			'REWARD_POINTS_REQUIRED.numeric'   => 'The Required Reward points should be number only.',
			'CUSTOM_PRIZE.required' => 'The Prize Name is required.',
			'CUSTOM_PRIZE_DESC.required' => 'The Prize description is required.',
			'CUSTOM_PRIZE_IMAGE_LINK.required' => 'The Prize image is required.',
			'REAL_PRIZE.numeric'   => 'The Real Prize should be number only.',
			'PROMO_PRIZE.numeric'   => 'The Promo Prize should be number only.'
		])->validate();
		
			if ($request->frmSubmit == 'Save') {
				$max = RewardLevel::max('LEVEL_ORDER');
				$LEVEL_ORDER = $max + 1;
				$imageLink = "";
				$folderName = config('poker_config.imageUrl.REWARD_IMAGE_FOLDER');
				if ($request->hasFile('CUSTOM_PRIZE_IMAGE_LINK') != '') {
					$file = $request->file('CUSTOM_PRIZE_IMAGE_LINK');
					$randNum = rand(100, 999);
					$randAlpha = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), -1);
					$name = date('mdYHis') . $randNum . $randAlpha . '.' . $file->getClientOriginalExtension();
					$filePath = $folderName . $name;
					$allowedFormats = array("Jpeg", "jpeg", "JPEG", "JPG", "jpg", "png", "PNG");
					if (!array($filePath)) {

						$msg = "4"; //Invalid Format
						$request->session()->flash('image_error', 'Invalid Image format.');
						return Redirect::to("rewards/rewardConfig/addRewardLevel");	
					} else {

						$s3 = \Storage::disk('s3CDN')->getDriver();
						$s3->put($filePath, file_get_contents($file), array('ACL' => 'private'));
						$imageLink = $name;
					}
				}

				try {
					$insertReward = RewardLevel::insertGetId([
						'REWARD_POINTS_REQUIRED' => $request->REWARD_POINTS_REQUIRED,
						'TOURNAMENT_NAME' => $request->TOURNAMENT_NAME,
						'REAL_PRIZE' => !empty($request->REAL_PRIZE) ? $request->REAL_PRIZE : "0.00",
						'PROMO_PRIZE' => !empty($request->PROMO_PRIZE) ? $request->PROMO_PRIZE : "0.00",
						'CUSTOM_PRIZE' => $request->CUSTOM_PRIZE,
						'CUSTOM_PRIZE_NAME' => $request->CUSTOM_PRIZE_NAME,
						'CUSTOM_PRIZE_DESC' => $request->CUSTOM_PRIZE_DESC,
						'CUSTOM_PRIZE_IMAGE_LINK' => $imageLink,
						'LEVEL_ORDER' => $LEVEL_ORDER,
						'STATUS' => '0',
						'CREATED_DATE'	=>	date('Y-m-d h:i:s'),
						'UPDATED_DATE'	=>	date('Y-m-d h:i:s'),
						'START_DATE'	=>	date('Y-m-d H:i:s', strtotime($request->date_from)),
						'END_DATE'	=>	date('Y-m-d H:i:s', strtotime($request->date_to))
					]);
					if ($insertReward) {
						return  Redirect::back()->with('rewardInsertmessage', 'Reward Level Added successfully.');
					} else {
						return  Redirect::back()->with('rewardnotInsertmessage', 'Reward Level Added UnSuccessfully.');
					}
				} catch (\Exception $e) {
					echo $e->getMessage();
					return response()->json(['rewardInsertmessageerror', 'Reward Level Added Operation failed']);
				}
			}

			echo "failed";exit;
	}

	//enable or disable reward level

	public function rewardlevelstatus(Request $request)
	{
		$validator = \Validator::make($request->all(), [
			'status' => 'required|integer',
		]);
		
		if ($validator->fails()) {
			return response()->json(['status' => false, 'message' => $validator->errors()->all()]);
		}
		
		if($request->status == 1){
			$date = date('Y-m-t 23:59:59',strtotime(date('Y-m-d')));
		}else{
			$date = date('Y-m-d h:i:s');
		}

		#Prepare Status Change data
		$statusChange = RewardLevel::find($request->id);
		$statusChange->STATUS = $request->status;
		$statusChange->UPDATED_DATE = date('Y-m-d h:i:s');
		$statusChange->END_DATE = $date;
		$statusChange->save();

		if($statusChange->STATUS == 2){
			Session::flash('message', "Level Deleted Successfully.");
			return response()->json(['status' => true, 'message' => "Level Deleted Successfully."]);
		}else{
			Session::flash('message', 'Status ' . (($request->status) ? "<b><i>Active</i></b>" : "<b><i>Inactive</i></b>") . ' successfully');
			return response()->json(['status' => true, 'message' => 'Status ' . (($request->status) ? "<b><i>Active</i></b>" : "<b><i>Inactive</i></b>") . ' successfully']);
		}
	}

}
