<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\PokerBaazi\Contracts\Authentication as PokerBaaziAuthentication;
use Auth;

use App\PokerBaazi\Traits\Authentication as PokerBaaziAuthTraits;

class Controller extends BaseController implements PokerBaaziAuthentication
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(){
        $this->user = Auth::user();
        $this->superUsers = config('poker_config.admin.super_admins');
    }

    public function hasCustomPermissionOrFail($access, $module){
        return \PokerBaazi::hasCustomPermissionOrFail($access, $module);
    }
    public function hasPermission($access, $module){
        return \PokerBaazi::hasPermission($access, $module);
    }
    
    public function hasEditPermission($module){
        return \PokerBaazi::hasEditPermission($module);
    }

    public function hasViewPermission($module){
        return \PokerBaazi::hasViewPermission($module);
    }   
    
    public function getAllPermissions(){
        return \PokerBaazi::getAllPermissions();
    }
    public function menuPermission($access, $menu_id){
        return \PokerBaazi::menuPermission($access, $menu_id);
    }
    
}
