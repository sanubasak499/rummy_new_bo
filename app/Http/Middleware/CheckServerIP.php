<?php

namespace App\Http\Middleware;

use Closure;

class CheckServerIP
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd($request->ip());
        if(in_array($request->ip(),config('poker_config.server_ip.ip'),true)){
            return $next($request);
        }else{
            echo "You don't have rights to access this url, please contact to your service provider.";exit;
        }
    }
}
