<?php

namespace App\Http\Middleware;

use Closure;

class AccessAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $access=null, $module=null)
    {
        \PokerBaazi::hasCustomPermissionOrFail($access, $module);
        return $next($request);
    }
}
