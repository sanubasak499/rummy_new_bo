<?php

namespace App\Http\Requests\bo\tournament;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

/**
 * Create Tournament validation 
 * 
 * In This file I am validating the request data
 * to check the required data found or not, 
 * If Not getting requirment full fill then 
 * @throws ValidationException with the errorBag and Inputs
 * 
 * Class CreateTournamentValidation
 * 
 * @category    Manage Tournament
 * @package     Tournament
 * @copyright   PokerBaazi
 * @author      Nitin Sharma
 * Created At:  03/02/2020
 */

class EditTournamentValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'TOURNAMENT_NAME'                       => 'required',
            'MINI_GAME_TYPE_ID'                     => 'required',
            'SERVER_ID'                             => 'required',
            'TOURNAMENT_DESC'                       => 'required',
            'CATEGORY'                              => 'required',
            'TOURNAMENT_LIMIT_ID'                   => 'required',
            'WIN_THE_BUTTON'                        => 'required',
            'TOURNAMENT_TYPE_ID'                    => 'required',
            // 'TOURNAMENT_SUB_TYPE_ID'                => 'required',
            // 'TOURNAMENT_PARENT_SATELLITE_ID'        => 'required',
            // 'TOURNAMENT_PARENT_MULTI_DAY_ID'        => 'required',
            // 'IS_ACTIVE'                             => 'required',

            // 'REGISTER_START_TIME'                   => 'required',
            // 'TOURNAMENT_START_TIME'                 => 'required',
            // 'LATE_REGISTRATION_ALLOW'               => 'required',
            // 'LATE_REGISTRATION_END_TIME'            => 'required',

            'PLAYER_PER_TABLE'                      => 'required:numeric',
            'T_MIN_PLAYERS'                         => 'required:numeric',
            'T_MAX_PLAYERS'                         => 'required:numeric',

            // 'COIN_TYPE_ID'                          => 'required:numeric',
            // 'DEPOSIT_BALANCE_ALLOW'                 => 'required:numeric',
            // 'PROMO_BALANCE_ALLOW'                   => 'required:numeric',
            // 'WIN_BALANCE_ALLOW'                     => 'required:numeric',
            // 'BUYIN'                                 => 'required:numeric',
            // 'ENTRY_FEE'                             => 'required:numeric',
            // 'BOUNTY_AMOUNT'                         => 'required:numeric',
            // 'BOUNTY_ENTRY_FEE'                      => 'required:numeric',
            // 'PROGRESSIVE_BOUNTY_PERCENTAGE'         => 'required:numeric',

            'BLIND_STRUCTURE_ID'                    => 'required:numeric',
            'TOURNAMENT_LEVEL'                      => 'required:numeric',
            'LEVEL_PERIOD'                          => 'required:numeric',
            // 'TOURNAMENT_CHIPS'                      => 'required:numeric',
            // 'TIMER_TOURNAMENT_END_TIME'             => 'required:numeric',

            'PLAYER_HAND_TIME'                      => 'required:numeric',
            'DISCONNECT_TIME'                       => 'required:numeric',
            'EXTRA_TIME'                            => 'required:numeric',
            'LOBBY_DISPLAY_INTERVAL'                => 'required:numeric',
            // 'PLAYER_MAX_EXTRATIME'                  => 'required:numeric',
            // 'ADDITIONAL_EXTRATIME_LEVEL_INTERVAL'   => 'required:numeric',
            // 'ADDITIONAL_EXTRATIME'                  => 'required:numeric',
            
            // 'REBUY_ADDON_RE_ENTRY'                  => 'required:numeric',
            // 'REBUY_CHIPS'                           => 'required:numeric',
            // 'REBUY_END_TIME'                        => 'required:numeric',
            // 'REBUY_COUNT'                           => 'required:numeric',
            // 'REBUY_IN'                              => 'required:numeric',
            // 'REBUY_ENTRY_FEE'                       => 'required:numeric',
            // 'DOUBLE_REBUYIN'                        => 'required:numeric',
            // 'ADDON_CHIPS'                           => 'required:numeric',
            // 'ADDON_BREAK_TIME'                      => 'required:numeric',
            // 'ADDON_AMOUNT'                          => 'required:numeric',
            // 'ADDON_ENTRY_FEE'                       => 'required:numeric',
            
            // 'PRIZE_STRUCTURE_ID'                    => 'required:numeric',
            // 'PRIZE_STRUCTURE_TYPE_ID'               => 'required:numeric',
            'PRIZE_BALANCE_TYPE'                    => 'required:numeric',
            // 'FIXED_PRIZE'                           => 'required:numeric',
            // 'NO_OF_WINNERS_CUSTOM'                  => 'required:numeric',
            // 'NO_OF_WINNERS_CUSTOM_MIX'              => 'required:numeric',
            // 'GUARENTIED_PRIZE'                      => 'required:numeric',
            // 'SATELLITES_GUARANTEED_PLACES_PAID'     => 'required:numeric',
            // 'MULTIDAY_PLAYER_PERCENTAGE'            => 'required:numeric',
        ];
    }

    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages()
    {
        return [
            // 'TOURNAMENT_NAME.required' => 'A title is required',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $keys = $this->request->keys();
        $values = collect($keys)->attributeTitleCase()->toArray();
        $attributes = array_combine($keys, $values);
        return $attributes;
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function failedValidation(Validator $validator){
        // dd($validator->errors()->add('field', 'Something is wrong with this field!'));
        session()->flash('error', 'Something is wrong with this field!, Please validate');
        throw (new ValidationException($validator))
                    ->errorBag($this->errorBag)
                    ->redirectTo($this->getRedirectUrl());
        // return $validator;
    }
    
    /**
     * Handle a passed validation attempt.
     *
     * @return void
     */
    protected function passedValidation()
    {
        //
    }
}
