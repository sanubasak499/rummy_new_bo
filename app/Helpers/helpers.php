<?php

use Illuminate\Support\Facades\Storage;

if (!function_exists('menu')) {
    function menu($menuName, $type = null, array $options = [])
    {
        return app(\App\Models\Menu::class)->all();
    }
}

if (!function_exists('getUserIdFromUsername')) {
    function getUserIdFromUsername($username)
    {
        $user = app(\App\Models\User::class)->select('USER_ID')->where('USERNAME', $username)->first();
        if ($user) {
            return $user->USER_ID;
        }
        return false;
    }
}
if (!function_exists('getUsernameFromUserId')) {
    function getUsernameFromUserId($user_id)
    {
        $user = app(\App\Models\User::class)->find($user_id);
        if ($user) {
            return $user->USERNAME;
        }
        return false;
    }
}
if (!function_exists('changeDateFormate')) {
    function changeDateFormate($date)
    {
        return date('j<\s\up>S</\s\up> M y H:i:s', strtotime($date));
    }
}

if (!function_exists('getEmailFromUserId')) {
    function getEmailFromUserId($user_id)
    {
        $user = app(\App\Models\User::class)->find($user_id);
        if ($user) {
            return $user->EMAIL_ID;
        }
        return false;
    }
}

if (!function_exists('getImageUrlFromName')) {
    function getImageUrlFromName($imageName)
    {
        $fileContents = Storage::disk('s3KYC')->url($imageName);
        return $fileContents;
    }
}


