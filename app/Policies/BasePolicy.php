<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Menu;

class BasePolicy
{
    use HandlesAuthorization;

    protected static $datatypes = [];

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        // dd("dfghj");
    }

    /**
     * Handle all requested permission checks.
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return bool
     */
    // public function browse(){
    //     // dd("df");
    // }
    public function __call($name, $arguments)
    {
        // dd($name, $arguments);
        if (count($arguments) < 2) {
            throw new \InvalidArgumentException('not enough arguments');
        }
        /** @var \App\Contracts\User $user */
        $user = $arguments[0];

        /** @var $model */
        $model = $arguments[1];
        // dd($user, $arguments);
        // return $this->checkPermission($user, $model, $name);
    }

    /**
     * Check if user has an associated permission.
     *
     * @param \App\Contracts\User $user
     * @param object                      $model
     * @param string                      $action
     *
     * @return bool
     */
    // protected function checkPermission(Admin $user, $model, $action)
    // {
    //     if($user->id == config('poker_config.admin.super_admin_id')){
    //         return true;
    //     }
        
    //     if (!isset(self::$datatypes[get_class($model)])) {
    //         $dataType = app(Menu::class);
    //         self::$datatypes[get_class($model)] = $dataType->where('model', get_class($model))->first();
    //     }

    //     $dataType = self::$datatypes[get_class($model)];
        
    //     return $user->hasPermission($action.'_'.$dataType->name);
    // }
}
