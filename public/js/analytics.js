(function($, global) {
    'use strict';

    class Analytics {
        constructor(data = {}) {
            this.initOptions();
            this.chartsObjectInit();
        }
        initOptions() {
            this.options = this.options || {};
            let o = {};
            o.baseUrl = global.pageData.baseUrl;
            this.options = o;
        }
        chartsObjectInit() {
            var _ = this;
            const { baseUrl } = _.options;
            _.charts = {};
            _.charts = {
                channelChart: {
                    id: '#channelChart',
                    chartObject: null,
                    formParams: {},
                    chartData: [],
                    apiUrl: `${baseUrl}/marketing/analytics/channelChart`,
                    ajaxMethod: _.ajaxChannelChart,
                    loadMethod: _.loadChannelChart,
                    creatChartObjectMethod: _.createChannelChart,
                },
                campaignChart: {
                    id: '#campaignChart',
                    chartObject: null,
                    formParams: {},
                    chartData: [],
                    apiUrl: `${baseUrl}/marketing/analytics/campaignChart`,
                    ajaxMethod: _.ajaxCampaignChart,
                    loadMethod: _.loadCampaignChart,
                    creatChartObjectMethod: _.createCampaignChart,
                }
            }
        }
        initEvents() {
            var _ = this;
            $(document).on('click', `.channelChartFilterBtn`, e => _.channelChartHandle(e));
            $(document).on('click', `.campaignChartFilterBtn`, e => _.campaignChartHandle(e));
        }

        chartsInit() {
            var _ = this;
            $.each(this.charts, (i, e) => {
                e.ajaxMethod.call(_, e);
            });
        }

        ajaxChannelChart(e) {
            var _ = this;
            $.ajax({
                type: "POST",
                url: e.apiUrl,
                data: e.formParams
            }).done((response) => {
                var responseData = response.data;
                var $data = [];
                $data.push(responseData.channels.sort());
                var chartDataByChannel = responseData.chartDataByChannel;
                var chartDataByRange = responseData.chartDataByRange;
                $.each(chartDataByRange, (i, element) => {
                    var channel = {},
                        availableChannel = {};
                    responseData.channels.forEach(elem => {
                        channel[elem] = 0;
                    });
                    $.each(element, (i, e) => {
                        availableChannel[e.CHANNEL] = e.userCount;
                    })
                    Object.assign(channel, availableChannel);
                    $data.push(Object.values(channel));
                });
                e.chartData = $data;
                // e.chartObject ? e.loadMethod.call(_, e) : e.creatChartObjectMethod.call(_, e);
                e.creatChartObjectMethod.call(_, e)
            }).fail((e, err) => {
                console.log(e, err);
            });
        }
        createChannelChart(e) {
            e.chartObject = c3.generate({
                bindto: e.id,
                transition: {
                    duration: 500
                },
                data: {
                    rows: e.chartData,
                    type: 'bar',
                }
            });
        }
        loadChannelChart(e) {
            e.chartObject.unload();
            e.chartObject.load({
                rows: e.chartData
            })
        }
        channelChartHandle(e) {
            var _ = this,
                $e = $(e.target);
            const { channelChart } = _.charts;
            channelChart.formParams = {};
            var flag = false;
            $(`[data-filter="channelChartFilter"]`).each((i, v) => {
                if ($.trim($(v).val()) != "") {
                    channelChart.formParams[$(v).data('type')] = $(v).val();
                } else {
                    flag = true;
                }
            });
            if (flag) {
                $.NotificationApp.send("Info", "Please Fill all the Fileds", 'top-right', '#3b98b5', 'info');
            } else {
                channelChart.ajaxMethod.call(_, channelChart);
            }
        }

        ajaxCampaignChart(e) {
            var _ = this;
            $.ajax({
                type: "POST",
                url: e.apiUrl,
                data: e.formParams
            }).done((response) => {
                var responseData = response.data;
                var $data = [];
                $data.push(responseData.compaign.sort());
                var chartDataByCampaign = responseData.chartDataByCampaign;
                var chartDataByRange = responseData.chartDataByRange;
                $.each(chartDataByRange, (i, element) => {
                    var compaign = {},
                        availableChannel = {};
                    responseData.compaign.forEach(elem => {
                        compaign[elem] = 0;
                    });
                    $.each(element, (i, e) => {
                        availableChannel[e.ATTR_CAMPAIGN] = e.userCount;
                    })
                    Object.assign(compaign, availableChannel);
                    $data.push(Object.values(compaign));
                });
                e.chartData = $data;
                // e.chartObject ? e.loadMethod.call(_, e) : e.creatChartObjectMethod.call(_, e);
                e.creatChartObjectMethod.call(_, e)
            }).fail((e, err) => {
                console.log(e, err);
            });
        }
        createCampaignChart(e) {
            e.chartObject = c3.generate({
                bindto: e.id,
                transition: {
                    duration: 500
                },
                data: {
                    rows: e.chartData,
                    type: 'bar',
                }
            });
        }
        loadCampaignChart(e) {
            e.chartObject.unload();
            e.chartObject.load({
                rows: e.chartData
            });
        }
        campaignChartHandle(e) {
            var _ = this,
                $e = $(e.target);
            const { campaignChart } = _.charts;
            campaignChart.formParams = {};
            var flag = false;
            $(`[data-filter="campaignChartFilter"]`).each((i, v) => {
                if ($.trim($(v).val()) != "") {
                    campaignChart.formParams[$(v).data('type')] = $(v).val();
                } else {
                    flag = true;
                }
            });
            if (flag) {
                $.NotificationApp.send("Info", "Please Fill all the Fileds", 'top-right', '#3b98b5', 'info');
            } else {
                campaignChart.ajaxMethod.call(_, campaignChart);
            }
        }

        init() {
            this.initEvents();
            this.chartsInit();
        }
    }

    $.Analytics = new Analytics;
}(window.jQuery, window));

(function($) {
    "use strict";
    $.Analytics.init();
}(window.jQuery));