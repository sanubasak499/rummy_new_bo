(function($) {
    $('#adminUserRegister').validate({
        rules: {
            firstname: {
                required: true,
                fieldTrim: true
            },
            lastname: {
                required: true,
                fieldTrim: true
            },
            username: {
                required: true,
                fieldTrim: true,
                remote: {
                    url: `${window.pageData.baseUrl}/WebApi/isAdminUserNameExist`,
                    type: "post",
                    dataFilter: function(response) {
                        response = JSON.parse(response);
                        let result = false;
                        result = response.status == 201 ? true : `"Username Already Exist"`;
                        return result;
                    }
                }
            },
            email: {
                required: true,
                fieldTrim: true,
                email: true,
                remote: {
                    url: `${window.pageData.baseUrl}/WebApi/isAdminEmailExist`,
                    type: "post",
                    dataFilter: function(response) {
                        response = JSON.parse(response);
                        let result = false;
                        result = response.status == 201 ? true : `"Email Already Exist"`;
                        return result;
                    }
                }
            },
            password: {
                required: true,
                fieldTrim: true,
                minlength: 4,
            },
            transcation_password: {
                required: true,
                fieldTrim: true,
                minlength: 4,
            },
            role_id: {
                required: true,
                fieldTrim: true
            },
            mobile: {
                required: true,
                fieldTrim: true,
                minlength: 10,
                maxlength: 10,
                number: true,
                remote: {
                    url: `${window.pageData.baseUrl}/WebApi/isAdminMobileExist`,
                    type: "post",
                    dataFilter: function(response) {
                        response = JSON.parse(response);
                        let result = false;
                        result = response.status == 201 ? true : `"Mobile Number Already Exist"`;
                        return result;
                    }
                }
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    $('#adminUserUpdate').validate({
        rules: {
            firstname: {
                required: true,
                fieldTrim: true
            },
            lastname: {
                required: true,
                fieldTrim: true
            },
            username: {
                required: true,
                fieldTrim: true,
            },
            email: {
                required: true,
                fieldTrim: true,
                email: true,
            },
            role_id: {
                required: true,
                fieldTrim: true
            },
            mobile: {
                required: true,
                fieldTrim: true,
                minlength: 10,
                maxlength: 10,
                number: true,
            },
            password: {
                minlength: 4,
            },
            transcation_password: {
                minlength: 4,
            },
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    $(document).on('click', `[data-toggle-password]`, function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        $(this).next('input').attr("type") == "password" ? $(this).next('input').attr("type", "text") : $(this).next('input').attr("type", "password");
    });
}(jQuery))