
$("#update_submit").click(function(){
	var user_kyc_id = $('#user_kyc_id').val();
	var doc_status = $('#doc_status').val();
	var dataString = 'user_kyc_id=' +user_kyc_id+'&doc_status=' +doc_status;
	console.log(dataString);
	$.ajax({
			"url": `${window.pageData.baseUrl}/helpdesk/kyc/updateKycStatus`,
			"method":"POST",
			"dataType":"json",
			"data": dataString,
			success:function(data){
				var _el = $("#setuserflagpopup");
				if(data.status){
					$.NotificationApp.send("Well Done", data.message, 'top-right', '#5ba035', 'success');
					_el.remove();	
					location.reload();					
				}else{
					$.NotificationApp.send("Sorry", data.message, 'top-right', '#FF0000', 'error');
					location.reload();
				}
		   }
		});
	});
	$("#update_submit_bank").click(function(){
	var name = $('#user_acc_id').val();
	var bank_status = $('#bank_status').val();
	var dataString = 'user_acc_id=' +name+'&bank_status=' +bank_status;
	
	$.ajax({
			"url": `${window.pageData.baseUrl}/helpdesk/kyc/updateBankStatus`,
			"method":"POST",
			"dataType":"json",
			"data": dataString,
			success:function(data){
				var _el = $("#bankstatuspopup");
				if(data.status){
					$.NotificationApp.send("Well Done", data.message, 'top-right', '#5ba035', 'success');
					_el.remove();	
					location.reload();					
				}else{
					$.NotificationApp.send("Sorry", data.message, 'top-right', '#FF0000', 'error');
					location.reload();	
				}
		   }
		});
	});

function viewkycImage(id){
	var dataString = 'id=' +id;
	$.ajax({
	   //"url": "{{url('/helpdesk/kyc/viewKycImg')}}",
	   
	   "url": `${window.pageData.baseUrl}/helpdesk/kyc/viewKycImg`,
	   "method":"post",
		dataType: "json",
		data: dataString,
		success:function(data){
			$(this).parent().parent('tr').remove();
			$("#UserData").empty();
			for(let i = 0;i<data.userInfo.length;i++){
				if(data.userInfo[i].DOCUMENT_TYPE == 1){
					if(data.userInfo[i].DOCUMENT_STATUS == 0){  
					//in case Failed
					
						if(data.userInfo[i].DOCUMENT_SUB_TYPE == 1){ //aadhar
							
							$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_SUB_TYPE === 1 ? 'Aadhar' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_BACK_URL!= '' ? '<a data-toggle="modal"  data-target="#aadharcardimg-1"><img src="' + data.userInfo[i].DOCUMENT_BACK_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 0 ? '<span class="badge bg-soft-success text-danger shadow-none">Failed</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 0 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
							
						}else if(data.userInfo[i].DOCUMENT_SUB_TYPE == 2){ //DL
						
							$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_SUB_TYPE === 2 ? 'Driving License' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_BACK_URL+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 0 ? '<span class="badge bg-soft-success text-danger shadow-none">Failed</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 0 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
							
						}else if(data.userInfo[i].DOCUMENT_SUB_TYPE == 3){ //VoterID
						
							$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_SUB_TYPE === 3 ? 'VoterID Card' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_BACK_URL+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 0 ? '<span class="badge bg-soft-success text-danger shadow-none">Failed</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 0 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
							
						}
						
					}
					if(data.userInfo[i].DOCUMENT_STATUS == 1){    
					//in case Success
						if(data.userInfo[i].DOCUMENT_SUB_TYPE == 1){ //aadhar
							
							$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_SUB_TYPE === 1 ? 'Aadhar' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_BACK_URL!= '' ? '<a data-toggle="modal"  data-target="#aadharcardimg-1"><img src="' + data.userInfo[i].DOCUMENT_BACK_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 1 ? '<span class="badge bg-soft-success text-success shadow-none">Success</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 1 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
							
						}else if(data.userInfo[i].DOCUMENT_SUB_TYPE == 2){ //DL
						
							$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_SUB_TYPE === 2 ? 'Driving License' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_BACK_URL+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 1 ? '<span class="badge bg-soft-success text-success shadow-none">Success</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 1 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
							
						}else if(data.userInfo[i].DOCUMENT_SUB_TYPE == 3){ //VoterID
						
							$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_SUB_TYPE === 3 ? 'VoterID Card' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_BACK_URL+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 1 ? '<span class="badge bg-soft-success text-success shadow-none">Success</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 1 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
							
						}
					}
					if(data.userInfo[i].DOCUMENT_STATUS == 2){		
						//in case Pending
						if(data.userInfo[i].DOCUMENT_SUB_TYPE == 1){ //aadhar
							
							$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_SUB_TYPE === 1 ? 'Aadhar' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_BACK_URL!= '' ? '<a data-toggle="modal"  data-target="#aadharcardimg-1"><img src="' + data.userInfo[i].DOCUMENT_BACK_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 2 ? '<span class="badge bg-warning text-white shadow-none">Pending</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 2 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
							
						}else if(data.userInfo[i].DOCUMENT_SUB_TYPE == 2){ //DL
						
							$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_SUB_TYPE === 2 ? 'Driving License' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_BACK_URL+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 2 ? '<span class="badge bg-warning text-white shadow-none">Pending</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 2 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
							
						}else if(data.userInfo[i].DOCUMENT_SUB_TYPE == 3){ //VoterID
						
							$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_SUB_TYPE === 3 ? 'VoterID Card' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_BACK_URL+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 2 ? '<span class="badge bg-warning text-white shadow-none">Pending</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 2 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
							
						}
					}
					if(data.userInfo[i].DOCUMENT_STATUS == 3){       			//in case Reject
						 
						if(data.userInfo[i].DOCUMENT_SUB_TYPE == 1){ //aadhar
							
							$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_SUB_TYPE === 1 ? 'Aadhar' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_BACK_URL!= '' ? '<a data-toggle="modal"  data-target="#aadharcardimg-1"><img src="' + data.userInfo[i].DOCUMENT_BACK_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 3 ? '<span class="badge bg-soft-danger text-danger shadow-none">Reject</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 3 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
							
						}else if(data.userInfo[i].DOCUMENT_SUB_TYPE == 2){ //DL
						
							$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_SUB_TYPE === 2 ? 'Driving License' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_BACK_URL+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 3 ? '<span class="badge bg-soft-danger text-danger shadow-none">Reject</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 3 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
							
						}else if(data.userInfo[i].DOCUMENT_SUB_TYPE == 3){ //VoterID
						
							$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_SUB_TYPE === 3 ? 'VoterID Card' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_BACK_URL+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 3 ? '<span class="badge bg-soft-danger text-danger shadow-none">Reject</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 3 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
							
						}
					}
				}
				if(data.userInfo[i].DOCUMENT_TYPE == 2){   //pan card case
					
					if(data.userInfo[i].DOCUMENT_STATUS == 0){
						
						$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_TYPE === 2 ? 'Pan Card' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_BACK_URL!= '' ? '<a data-toggle="modal"  data-target="#aadharcardimg-1"><img src="' + data.userInfo[i].DOCUMENT_BACK_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 0 ? '<span class="badge bg-soft-success text-danger shadow-none">Failed</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 0 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
					}
					else if(data.userInfo[i].DOCUMENT_STATUS == 1){
						$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_TYPE === 2 ? 'Pan Card' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_BACK_URL!= '' ? '<a data-toggle="modal"  data-target="#aadharcardimg-1"><img src="' + data.userInfo[i].DOCUMENT_BACK_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 1 ? '<span class="badge bg-soft-success text-success shadow-none">Success</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 1 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
					}
					else if(data.userInfo[i].DOCUMENT_STATUS == 2){
						
						$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_TYPE === 2 ? 'Pan Card' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_BACK_URL+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 2 ? '<span class="badge bg-warning text-white shadow-none">Pending</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 2 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
						
					}
					else if(data.userInfo[i].DOCUMENT_STATUS == 3){
						$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_TYPE === 2 ? 'Pan Card' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_BACK_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_BACK_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_BACK_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 3 ? '<span class="badge bg-soft-danger text-danger shadow-none">Reject</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 3 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
					}
				}	
				if(data.userInfo[i].DOCUMENT_TYPE == 3){   //support doc case
					
					if(data.userInfo[i].DOCUMENT_STATUS == 0){
						
						$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_TYPE === 3 ? 'Support Doc' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_BACK_URL!= '' ? '<a data-toggle="modal"  data-target="#aadharcardimg-1"><img src="' + data.userInfo[i].DOCUMENT_BACK_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 0 ? '<span class="badge bg-soft-success text-danger shadow-none">Failed</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 0 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
					}
					else if(data.userInfo[i].DOCUMENT_STATUS == 1){
						$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_TYPE === 3 ? 'Support Doc' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_BACK_URL!= '' ? '<a data-toggle="modal"  data-target="#aadharcardimg-1"><img src="' + data.userInfo[i].DOCUMENT_BACK_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 1 ? '<span class="badge bg-soft-success text-success shadow-none">Success</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 1 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
					}
					else if(data.userInfo[i].DOCUMENT_STATUS == 2){
						
						$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_TYPE === 3 ? 'Support Doc' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_BACK_URL+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 2 ? '<span class="badge bg-warning text-white shadow-none">Pending</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 2 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
						
					}
					else if(data.userInfo[i].DOCUMENT_STATUS == 3){
						$("#UserData").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_AT+'</td><td>'+(data.userInfo[i].DOCUMENT_TYPE === 3 ? 'Support Doc' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_FRONT_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_FRONT_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_BACK_URL != '' ? ' <a href="' + data.userInfo[i].DOCUMENT_BACK_URL + '" class="image-popup"><img src="' + data.userInfo[i].DOCUMENT_BACK_URL + '" alt="backimage" style="width: 150px;"/></a>' : '')+'</td><td>'+data.userInfo[i].DOCUMENT_NUMBER+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 3 ? '<span class="badge bg-soft-danger text-danger shadow-none">Reject</span>' : '')+'</td><td>'+(data.userInfo[i].DOCUMENT_STATUS === 3 ? '<a href="#" onclick="viewStatusKyc('+data.userInfo[i].USER_KYC_ID+')" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
					}
					
				}
			
			
			
			}
		},	
	});
}
function viewBankDetails(id){
	var dataString = 'id=' +id;
	$.ajax({
	   "url": `${window.pageData.baseUrl}/helpdesk/kyc/viewUserBankDetails`,
	   "method":"post",
		dataType: "json",
		data: dataString,
		success:function(data){
			console.log(data.userInfo.length);
			//$("#accTable").DataTable().destroy();
			$("#UserDataBank").empty();
			for(let i = 0;i<data.userInfo.length;i++){
					if(data.userInfo[i].STATUS == 0 || data.userInfo[i].STATUS == null ){  
					//in case Failed
						$("#UserDataBank").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_DATE+'</td><td>'+data.userInfo[i].ACCOUNT_TYPE+'</td><td>'+data.userInfo[i].IFSC_CODE+'</td><td>'+data.userInfo[i].ACCOUNT_NUMBER+'</td><td>'+data.userInfo[i].ACCOUNT_HOLDER_NAME+'</td><td>'+(data.userInfo[i].STATUS === 0 ? '<span class="badge bg-soft-success text-danger shadow-none">Failed</span>' : '')+'</td><td>'+(data.userInfo[i].STATUS === 0 ? '<a href="#" onclick="viewStatusBank('+data.userInfo[i].USER_ACCOUNT_ID+')" data-toggle="modal" title="Set User Flag" data-target="#bankstatuspopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
					}
					if(data.userInfo[i].STATUS == 1){    
					//in case Success
						$("#UserDataBank").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_DATE+'</td><td>'+data.userInfo[i].ACCOUNT_TYPE+'</td><td>'+data.userInfo[i].IFSC_CODE+'</td><td>'+data.userInfo[i].ACCOUNT_NUMBER+'</td><td>'+data.userInfo[i].ACCOUNT_HOLDER_NAME+'</td><td>'+(data.userInfo[i].STATUS === 1 ? '<span class="badge bg-soft-success text-success shadow-none">Success</span>' : '')+'</td><td>'+(data.userInfo[i].STATUS === 1 ? '<a href="#" onclick="viewStatusBank('+data.userInfo[i].USER_ACCOUNT_ID+')" data-toggle="modal" title="Set User Flag" data-target="#bankstatuspopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
					}
					if(data.userInfo[i].STATUS == 2){		
						//in case Pending
						$("#UserDataBank").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_DATE+'</td><td>'+data.userInfo[i].ACCOUNT_TYPE+'</td><td>'+data.userInfo[i].IFSC_CODE+'</td><td>'+data.userInfo[i].ACCOUNT_NUMBER+'</td><td>'+data.userInfo[i].ACCOUNT_HOLDER_NAME+'</td><td>'+(data.userInfo[i].STATUS === 2 ? '<span class="badge bg-warning text-white shadow-none">Pending</span>' : '')+'</td><td>'+(data.userInfo[i].STATUS === 2 ? '<a href="#" onclick="viewStatusBank('+data.userInfo[i].USER_ACCOUNT_ID+')" data-toggle="modal" title="Set User Flag" data-target="#bankstatuspopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
					}
					if(data.userInfo[i].STATUS == 3){       			//in case Reject
						$("#UserDataBank").append('<tr><td>'+(i)+'</td><td>'+data.userInfo[i].UPDATED_DATE+'</td><td>'+data.userInfo[i].ACCOUNT_TYPE+'</td><td>'+data.userInfo[i].IFSC_CODE+'</td><td>'+data.userInfo[i].ACCOUNT_NUMBER+'</td><td>'+data.userInfo[i].ACCOUNT_HOLDER_NAME+'</td><td>'+(data.userInfo[i].STATUS === 3 ? '<span class="badge bg-soft-danger text-danger shadow-none">Reject</span>' : '')+'</td><td>'+(data.userInfo[i].STATUS === 3 ? '<a href="#" onclick="viewStatusBank('+data.userInfo[i].USER_ACCOUNT_ID+')" data-toggle="modal" title="Set User Flag" data-target="#bankstatuspopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>' : '')+'</td></tr>');
					}
				
			}
		},
	});
}
function viewStatusBank(id){
	var dataString = 'id=' +id;
	$.ajax({
	  "url": `${window.pageData.baseUrl}/helpdesk/kyc/viewBankStatus`,
	   "method":"post",
		dataType: "json",
		data: dataString,
		success:function(data){
			console.log(data);
			$("#bankstatuspopup").find('input[name="user_acc_id"]').val(id);
			customSelect($("#bank_status"),data.status.STATUS);
		},
	});
}
function viewStatusKyc(id){
	var dataString = 'id=' +id;
	$.ajax({
	  "url": `${window.pageData.baseUrl}/helpdesk/kyc/viewKycStatus`,
	   "method":"post",
		dataType: "json",
		data: dataString,
		success:function(data){
			$("#setuserflagpopup").find('input[name="user_kyc_id"]').val(id);
			customSelect($("#doc_status"),data.doc_status.DOCUMENT_STATUS);
		},
	});
}
function customSelect(_el,value){
	_ulElement = _el.next(".customselect").find("ul.list");
	_ulElement.find("li").removeClass("selected");
	if(_ulElement.find("[data-value='" + value +"']").length > 0){
		var label = _ulElement.find("[data-value='" + value +"']").addClass("selected").text();
	}else{
		var label =  _ulElement.find("li:first").addClass("selected").text();
	}
    _el.next(".customselect").find(".current").text(label);
}
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

