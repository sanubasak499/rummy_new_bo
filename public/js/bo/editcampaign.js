
//var form_validator = $("#editcampaign").validate();
var form_validator = $("#editcampaign").validate({		 
		errorPlacement: function(error, element) {
		  var placement = $(element).data('error');
		   if (placement) {
			 $(placement).append(error)
		   } else {
			   if($(element).attr('id') == "p_promochips" ||$(element).attr('id') == "p_unclaimedbonus"  ){
				   error.insertAfter($(element).next('label'));
			   }else if($(element).attr('id') == "campaigncode" || $(element).attr('id') == "campaignurl"){
				   error.insertAfter($(element).siblings('.input-group-append'));
			   }else if($(element).attr('id') == "s_promochips"){
				    error.insertAfter($(element).next('label'));
			   }else{
				   error.insertAfter(element);
			   }
		   }
		},
		messages: {
		max_depositamount: {
			min: "Please enter a value greater than or equal to minimum deposit amount"
		}
		}
});
 $( "[name='partnername']" ).rules( "add", {
  required: true,
  messages: {
    required: "Please select the partner",
    //minlength: jQuery.validator.format("Please, at least {0} characters are necessary")
  }
});

 $( "[name='campaignType']" ).rules( "add",{
  required: true,
  messages: {
    required: "Please select the campaign type",
  }
});
$( "[name='p_promochips']" ).rules( "add",{ 
		require_from_group: [1, ".check-pay-group"]


});
$( "[name='p_unclaimedbonus']" ).rules( "add",{ 
		require_from_group: [1, ".check-pay-group"]
	
});

$( "[name='campaignname']" ).rules( "add",{
  required: true,
  messages: {
    required: "campaign name is required",
  },
  pattern: /^[a-zA-Z0-9-_'.$1\n& ]+$/
});
$( "[name='campaigncode']" ).rules( "add",{
  required: true,
  messages: {
    required: "campaign code is required",
  }
});
$( "[name='campaignurl']" ).rules( "add",{
  required: true,
  messages: {
    required: "url is required",
  }
});
$( "[name='startdate']" ).rules( "add",{
  required: true,
  messages: {
    required: "Select the campaign start date",
  }
});
$( "[name='promominvalue']" ).rules( "add",{
		number: true		
});
$( "[name='coins_required']" ).rules( "add",{
		number: true		
});
$( "[name='promovalue2']" ).rules( "add",{
		number: true,
		min: 0		
});
$( "[name='promomaxvalue']" ).rules( "add",{
		number: true		
});
$( "[name='enddate']" ).rules( "add",{
  required: true,
  messages: {
    required: "Select the campaign end date",
  }
});
$( "[name='p_promovalue']" ).rules( "add",{
	required: {
		depends: function(element){
			return $('#p_promochips').is(":checked");
		}
	},
	messages: {
    required: "promo value is required",
  },
}); 
$( "[name='p_promocapvalue']" ).rules( "add",{
	required: {
		depends: function(element){
			return $('#p_promochips').is(":checked");
		}				
	},
	messages: {
    required: "Promo Cap Value is required",
  },
	number: true
});
$( "[name='promovalue1']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#campaignType').val() == '2';
			}
		},
		number: true		
});
$( "[name='promovalue1']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#campaignType').val() == '3';
			}
		},
		number: true		
});
$( "[name='p_creditdebitcardvalue']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#p_unclaimedbonus').is(":checked");
			}				
		},
		messages: {
			required: "Card Value is required",
		},
		number: true
});
$( "[name='p_netbankingvalue']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#p_unclaimedbonus').is(":checked");
			}				
		},
		messages: {
			required: "Netbanking Value is required",
		},
		number: true
});
$( "[name='p_bonuscapvalue']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#p_unclaimedbonus').is(":checked");
			}				
		},
		messages: {
			required: "Enter the Bonus Cap Value",
		},
		number: true
});
$( "[name='p_tournamentticketid']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#p_tournamenttickets').is(":checked");
			}	
	    }	
});
$( "[name='s_promovalue']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#s_promochips').is(":checked");
			}				
		},
		messages: {
			required: "Enter the Promo Value",
		},
		number: true
});
$( "[name='s_promochips']" ).rules( "add",{ 
	required: true,
});
$( "[name='s_tournamentticketid']" ).rules( "add",{ 
		required: {
			depends: function(element){
				return $('#s_tournamenttickets ').is(":checked");
			}	
		},
		messages: {
			required: "Select Tournament",
		},		
});
$( "[name='p_tournamentticketid']" ).rules( "add",{ 
		required: {
			depends: function(element){
				return $('#p_tournamenttickets ').is(":checked");
			}	
		},
		messages: {
			required: "Select Tournament",
		},		
});

$( "[name='expusermin']" ).rules( "add",{
  required: true,
  number: true,
  messages: {
    required: "Expected user minimum total",
  }
});	

$( "[name='expusermax']" ).rules( "add",{
  required: true,
  number: true,
  min: function() {
		return parseInt($('#expusermin').val());
	},
  messages: {
    required: "Expected user maximum total",
  }
});
$( "[name='noOfUniqueCode']" ).rules( "add",{
	required: true,
	number: true,
	messages: {
    required: "Enter Unique Code",
  }
});	

$( "[name='cost']" ).rules( "add",{
  number: true,
	min: 0
});
$( "[name='expecteduser']" ).rules( "add",{
   number: true,
	min: 0
});
$( "[name='m_depositamount']" ).rules( "add",{
  number: true,
	min: 0
	
});
$( "[name='max_depositamount']" ).rules( "add",{
  number: true,
	min: 0,
	min: function() {
		return parseInt($('#m_depositamount').val());
	}
});
$( "[name='s_promovalue']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#s_promochips').is(":checked");
			}				
		},
		messages: {
			required: "Enter the Promo Value",
		},
		number: true
});
$( "[name='campaign_remark']" ).rules( "add",{
  pattern: /^[a-zA-Z0-9-_'.$1\n& ]+$/
	
});
$( "[name='campaign_description']" ).rules( "add",{
  pattern: /^[a-zA-Z0-9-_'.$1\n& %]+$/
});
function isValidInputs(curStepBtn){
	isValid = true;
	if(curStepBtn == "step-1")
	{	
		$("[name='campaigncode'],[name='partner_name'],[name='campaignurl'],[name='campaignType'],[name='campaignname'],[name='startdate'],[name='campaign_remark'],[name='campaign_description']").each( function(index) {
			var xy = form_validator.element(this);
			isValid = isValid && (typeof xy == 'undefined' || xy);
		});
	}else if(curStepBtn == "step-2"){
		$("[name='p_promovalue'],[name='p_promocapvalue'],[name='promovalue1'],[name='p_creditdebitcardvalue'],[name='p_netbankingvalue'],[name='p_bonuscapvalue'],[name='p_tournamentticketid'],[name='s_promovalue'],[name='s_tournamentticketid'],[name='m_depositamount'],[name='max_depositamount'],[name='s_promochips'],[name='promominvalue'],[name='p_promochips'],[name='p_unclaimedbonus'],[name='coins_required'],[name='promomAXvalue']").each( function(index) {
			var xy = form_validator.element(this);
			isValid = isValid && (typeof xy == 'undefined' || xy);
		});
	}else if(curStepBtn == "step-3"){
		$("[name='expusermin'],[name='expusermax'],[name='noOfUniqueCode'],[name='cost'],[name='expecteduser']").each( function(index) {
			var xy = form_validator.element(this);
			isValid = isValid && (typeof xy == 'undefined' || xy);
		});
	}
	return isValid;
}

$(document).ready(function(){
 var navListItems = $('.setup-panel li a'),
	allWells = $('.setup-content'),
	allPrevBtn = $('.previousbtn'),
	allNextBtn = $('.nextBtn');

    allWells.hide();
	allPrevBtn.click(function(e){
		
		var curStep = $(this).closest(".setup-content");
		var curStepBtn = curStep.attr("id");
		prevStepWizard = $('.setup-panel li a[href="#' + curStepBtn + '"]').parent().prev().children("a");
		// curInputs = curStep.find("input[type='text'],input[type='url']");
		prevStepWizard.trigger('click');
		return false;

	});
    navListItems.click(function (e) {
		e.preventDefault();
		var $target = $($(this).attr('href')),
		 $item = $(this);
		if (!$item.hasClass('disabled')) {
            navListItems.removeClass('active').addClass('btndefault');
            $item.addClass('active');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
            $target.find('select:eq(0)').focus();
        }
		
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content");
		//console.log(curStep);
		curStepBtn = curStep.attr("id");
		nextStepWizard = $('.setup-panel li a[href="#' + curStepBtn + '"]').parent().next().children("a");
		curInputs = curStep.find("input[type='text'],input[type='url']");
		isValid = true;
		
		isValid = isValidInputs(curStepBtn);
		// if Tab One

		if (isValid){nextStepWizard.removeAttr('disabled').trigger('click');}
		else if (curStepBtn == 'step-3'){$("#edit").submit();}
	});

    $('.setup-panel li  a.active').trigger('click');
});		



$('.select').on( 'hide.bs.select', function ( ) {
    $(this).trigger("focusout");
});

function findCostPerUserValue() {
	var cCost = document.getElementById('cost').value;
	var eUser = document.getElementById('expecteduser').value;	
	console.log(eUser);
	if(!isNaN(cCost) && !isNaN(eUser)) {
		var cPUserVal = cCost / eUser;
		document.getElementById('costperuser').value=cPUserVal;
	}
}