function getUniqueUser(id){
	$("#uniqueUserData").html('');
	$.ajax({
		   "url": `${window.pageData.baseUrl}/marketing/managecampaign`,
		   "method":"GET",
		   "data":{
			   "id":id,
			   "action":"getUniqueUser"
			   },
			success:function(data){
				for(let i = 0;i<data.campaignUserUsed.length;i++){
					$("#uniqueUserData").append('<tr><td>'+(i+1)+'</td><td>'+data.campaignUserUsed[i].USERNAME+'</td><td>'+data.campaignUserUsed[i].CONTACT+'</td><td>'+data.campaignUserUsed[i].EMAIL_ID+'</td><td>'+data.campaignUserUsed[i].UPDATED_DATE+'</td></tr>');
				}
				
				var uuci = data.unique_user_cam_info;
				$("#v_campaign_code").text(uuci.PROMO_CAMPAIGN_CODE);
				$("#v_campaign_name").text(uuci.PROMO_CAMPAIGN_NAME);
				$("#v_campaign_desc").text(uuci.PROMO_CAMPAIGN_DESC);
				$("#v_campaign_type").text(uuci.PROMO_CAMPAIGN_TYPE);
				$("#v_campaign_partner_name").text(uuci.PARTNER_NAME);
				$("#v_campaign_s_date").text(uuci.START_DATE_TIME);
				$("#v_campaign_e_date").text(uuci.END_DATE_TIME);
				$("#v_campaign_created").text(uuci.PARTNER_ID);
				
				 
				 
				}
	});
}
function viewCampaignData(id){
	$("#viewCampaign").html('');
	$.ajax({
		   "url": `${window.pageData.baseUrl}/marketing/managecampaign`,
		   "method":"GET",
		   "data":{
			   "id":id,
			   "action":"viewCampaignData"
			   },
			success:function(data){
				//console.log(data);
				var newData = [];
				newData[0] = data;
				var htmldata  = getViewHtml(data);
					$("#exampleModal").find("table").html(htmldata);
			}
	});
}

var heading = {
		  "PROMO_CAMPAIGN_TYPE": "Campaign Type",
		  "PARTNER_NAME": "Partner Name",
		  "PROMO_CAMPAIGN_NAME": "Campaign Name",
		  "PROMO_CAMPAIGN_CODE": "Campaign Code",
		  "PROMO_CAMPAIGN_URL": "Campaign URL",
		  "PROMO_STATUS_ID": "Campaign Status",
		  "START_DATE_TIME": "Start Date",
		  "END_DATE_TIME": "End Date",
		  "PROMO_PAYMENT_TYPE_ID": "Payment Type",
		  "PROMO_TYPE_ID": "Promo Type",
		  "P_PROMO_CHIPS": "Promo Chips",
		  "P_PROMO_VALUE": "Promo (%)",
		  "P_PROMO_CAP_VALUE": "Promo Cap",
		  "S_PROMO_CHIPS"	: "Promo Chips",
		  "S_PROMO_CHIP_VALUE": "Promo Amount",
		  "S_TOURNAMENT_TICKETS": "Tournament Tickets",
		  "PROMO_VALUE1": "Promo Value1",
		  "MINIMUM_DEPOSIT_AMOUNT": "Minimum Deposit Amount",
		  "P_UNCLAIMED_BONUS": "Unclaimed Bonus",
		  "P_CREDIT_DEBITCARD_VALUE": "Credit / Debit Card (%)",
		  "P_NETBANKING_VALUE": "Netbanking (%)",
		  "P_BONUS_CAP_VALUE": "Bonus Cap",
		  "P_TOURNAMENT_TICKETS": "Tournament Tickets",
		  "PROMO_VALUE1": "Promo Value1",
		  "REFERRAL_USERNAME": "RAF Username",
		  "RAF_PERCENTAGE": "RAF Per (%)",
		  "EXP_USER_MIN": "Exp User Min",
		  "EXP_USER_MAX": "Exp User Max"
		};
	
	
	var default_values = ["PROMO_CAMPAIGN_TYPE","PARTNER_NAME","PROMO_CAMPAIGN_NAME","PROMO_CAMPAIGN_CODE","PROMO_CAMPAIGN_URL","PROMO_STATUS_ID","START_DATE_TIME","END_DATE_TIME"];
	var default_values_second = ["EXP_USER_MIN","EXP_USER_MAX"];
	var PROMO_CAMPAIGN = [
	"P_PROMO_CHIPS","P_PROMO_VALUE","P_PROMO_CAP_VALUE","MINIMUM_DEPOSIT_AMOUNT","P_UNCLAIMED_BONUS",
	"P_CREDIT_DEBITCARD_VALUE","P_NETBANKING_VALUE","P_BONUS_CAP_VALUE","P_TOURNAMENT_TICKETS",
	"PROMO_VALUE1"
	];
	var PROMO_CAMPAIGN_OTHER = [
	"S_PROMO_CHIPS","S_PROMO_CHIP_VALUE",
	"S_TOURNAMENT_TICKETS","PROMO_VALUE1",
	];
	var PROMO_CAMPAIGN_TWO_ID = [
	"PROMO_PAYMENT_TYPE_ID","PROMO_TYPE_ID",
	"PROMO_VALUE1",
	];
	var RAF_Settings = ["REFERRAL_USERNAME","RAF_PERCENTAGE"];

function getViewHtml(data){
	var rowHtml='';
	
	rowHtml = rowHtml + "<tr><th colspan='2' class='text-center btn-primary white-text'> Campaign Settings</th></tr>";
	for(i in default_values){
			rowHtml = rowHtml + getRows(default_values[i],data[default_values[i]]);
	}
	if(inArray(data.PROMO_CAMPAIGN_TYPE_ID,['1','6','8']))
	{
		rowHtml = rowHtml + "<tr><th colspan='2' class='text-center btn-primary white-text'>Payment &amp; Rule Settings</th></tr>";
		for(i in PROMO_CAMPAIGN){
			rowHtml = rowHtml + getRows(PROMO_CAMPAIGN[i],data[PROMO_CAMPAIGN[i]]);
		}
	}
	if(inArray(data.PROMO_CAMPAIGN_TYPE_ID,['5']))
	{
		rowHtml = rowHtml + "<tr><th colspan='2' class='text-center btn-primary white-text'>Payment &amp; Rule Settings</th></tr>";
		for(i in PROMO_CAMPAIGN_OTHER){
			rowHtml = rowHtml + getRows(PROMO_CAMPAIGN_OTHER[i],data[PROMO_CAMPAIGN_OTHER[i]]);
		}
	}
	if(inArray(data.PROMO_CAMPAIGN_TYPE_ID,['3','2']))
	{
		rowHtml = rowHtml + "<tr><th colspan='2' class='text-center btn-primary white-text'>Payment &amp; Rule Settings</th></tr>";
		for(i in PROMO_CAMPAIGN_TWO_ID){
			rowHtml = rowHtml + getRows(PROMO_CAMPAIGN_TWO_ID[i],data[PROMO_CAMPAIGN_TWO_ID[i]]);
		}
	}
	if(inArray(data.PROMO_CAMPAIGN_TYPE_ID,['2','3','5','6','7']))
	{
		rowHtml = rowHtml + "<tr><th colspan='2' class='text-center btn-primary white-text'>RAF Settings</th></tr>";
		for(i in RAF_Settings){
			rowHtml = rowHtml + getRows(RAF_Settings[i],data[RAF_Settings[i]]);
		}
	}
	rowHtml = rowHtml + "<tr><th colspan='2' class='text-center btn-primary white-text'>Cost &amp; User Range Settings</th></tr>";
	for(i in default_values_second){
		rowHtml = rowHtml + getRows(default_values_second[i],data[default_values_second[i]]);
	}
	// var tbodyOpen = "<tbody>";
		// rowHtml = rowHtml + "<tr><th>" + heading[i]  + "</th><td>" + data[i]  + "</td></tr>";
	// var tbodyClose = "</tbody>";
	return "<tbody>" + rowHtml + "</tbody>";
	
}
function getRows(key,value){
	if(typeof value == 'undefined' || value == null ){
		value = '';
	}
	//console.log("key" + heading[key] +" val" + value );
	return "<tr><th>" +  heading[key]  + "</th><td>" + value  + "</td></tr>";;
} 
function clearFormList(){
	 document.getElementById("campaigndata").reset();
	 document.getElementById("basic-datatable").reset();
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle)
            return true;
    }
    return false;
}
 $(document).ready(function (){
	 var switchStatus = false;
		$(document).on('change',".status_switch", function() {
			
			if($(this).is(':checked')){
				var status = 1; 
			}else{
				var status = 2; 
			}
			$.ajax({
				"url": `${window.pageData.baseUrl}/marketing/managecampaign/changeCampaignStatus`,
				"method":"POST",
				"data":{"id":$(this).data('id'),"status":status},
				"dataType":"json",
				success:function(data){
					if(data.status){
						$.NotificationApp.send("Well Done", data.message, 'top-right', '#5ba035', 'success');
					}else{
						$.NotificationApp.send("Sorry", data.message, 'top-right', '#FF0000', 'error');
					}
			   }
			});
		});
 });
 
deleteReason = (id) => {
	Swal.fire({
	title: 'Are you sure?',
	text: "You won't be able to revert this!",
	type:"warning",
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	confirmButtonText: 'Yes, delete it!'
	}).then((result) => {
          if (result.value) {
			var _el = $("#delete_" + id).closest("tr");
			$.ajax({
				"url":`${window.pageData.baseUrl}/marketing/managecampaign/deleteDataPromoCampaign`, 
				"method":"post",
				"data":{"id":id},
				"dataType":"json",
				success:function(data){
					if(data.status){
						$.NotificationApp.send("Success", data.message, 'top-right', '#5ba035', 'success');
					 _el.remove();
					}else{
						$.NotificationApp.send("error", data.message, 'top-right', '#FF0000', 'error');
					}
								
				}
			});
          }
        })
}
