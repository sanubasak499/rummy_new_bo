$(document).ready(function(){
	$("input[name='excel_camp_regist']").on("focus",function(){
		$("#excelErrorReg").text('');
	});
	$("input[name='excel_camp']").on("focus",function(){
		$("#excelError").text('');
	});
	$(".modalclose").click(function(){
		$('#accordion-modal-user').hide();
	});
	$(".modalclose").click(function(){
		$('#accordion-modal-contact').hide();
	}); 
	
	$("#partner_name,#campaignType").change(function(){
		var partner_name = $("#partner_name").val();
		var campaignType = $("#campaignType").val();
		if(inArray(campaignType, ['1','3','4','5','6','8']) && partner_name == '10001'){
			$("#download_sam_for").show();
			$("#download_sam_for_regist").hide();
		}else if((inArray(campaignType, ['1','2','3','4','5','6','8']) && partner_name != '10001') || campaignType == '7'){
			$("#download_sam_for,#download_sam_for_regist").hide();
		}else if(campaignType == 2 && partner_name == '10001'){
			$("#download_sam_for_regist").show();
			$("#download_sam_for").hide();
		}else {
			$("#download_sam_for").show();
			$("#download_sam_for_regist").hide();
		}
	});
	
	$("#campaignType").change(function(){
		var campaignID = $(this).val();
		if(campaignID == 1){
			$("#generate-button,#Payment-first-deposite-tab,#remark_setting,#withdrawl_setting,#default_campaignurlhead,#campaigncodeurl").show();
			$("#referafriendSetting,#campaignurlhead,#registration-tab,#campaignurl,#noofUniqueCode,#uniquecode-tab").hide();
			$("#campaignurl-error").hide();
		}else if(campaignID == 2){
			$("#campaignurl,#remark_setting,#withdrawl_setting,#campaignurlhead,#registration-tab,#referafriendSetting,#campaigncodeurl").show();
			$("#Payment-first-deposite-tab,#noofUniqueCode,#uniquecode-tab,#default_campaignurlhead").hide();
			$("#campaignurl-error").show();
		}else if(campaignID == 3){
			$("#registration-tab,#remark_setting,#withdrawl_setting,#default_campaignurlhead,#campaigncodeurl").show();
			$("#campaignurl,#campaignurlhead,#referafriendSetting,#noofUniqueCode,#uniquecode-tab,#Payment-first-deposite-tab").hide();
			$("#campaignurl-error").hide();
		}else if(campaignID == 5){
			$("#uniquecode-tab,#remark_setting,#withdrawl_setting,#referafriendSetting,#default_campaignurlhead,#campaigncodeurl").show();
			$("#campaignurl,#campaignurlhead,#registration-tab,#Payment-first-deposite-tab,#noofUniqueCode").hide();
			$("#campaignurl-error").hide();
		}else if(campaignID == 6){
			$("#referafriendSetting,#withdrawl_setting,#remark_setting,#Payment-first-deposite-tab,#default_campaignurlhead,#campaigncodeurl").show();
			$("#campaignurl,#campaignurlhead,#registration-tab,#uniquecode-tab,#noofUniqueCode").hide();
			$("#campaignurl-error").hide();
		}else if(campaignID == 7){
			$("#uniquecode-tab,#noofUniqueCode").show();
			$("#campaigncodeurl,#remark_setting,#withdrawl_setting,#default_campaignurlhead,#registration-tab,#Payment-first-deposite-tab").hide();
			$("#campaignurl-error").hide();
		}else if(campaignID == 8){
			$("#Payment-first-deposite-tab,#remark_setting,#withdrawl_setting,#referafriendSetting,#default_campaignurlhead,#campaigncodeurl").show();
			$("#campaignurl,#campaignurlhead,#uniquecode-tab,#registration-tab,#noofUniqueCode").hide();
			$("#campaignurl-error").hide();
		}else{
			$("#campaignurl,#campaignurlhead,#registration-tab,#noofUniqueCode,#Payment-first-deposite-tab,#referafriendSetting,#generate-button,#remark_setting,#withdrawl_setting,#campaigncodeurl").show();
			$("#default_campaignurlhead").hide();
			$("#campaignurl-error").show();
		}
	});

});

var form_validator = $("#createcampaign").validate({		 
		errorPlacement: function(error, element) {
		  var placement = $(element).data('error');
		   if (placement) {
			 $(placement).append(error)
		   } else {
			   if($(element).attr('id') == "p_promochips" ||$(element).attr('id') == "p_unclaimedbonus"  ){
				   error.insertAfter($(element).next('label'));
			   }else if($(element).attr('id') == "campaigncode" || $(element).attr('id') == "campaignurl"){
				   error.insertAfter($(element).siblings('.input-group-append'));
			   }else if($(element).attr('id') == "s_promochips"){
				    error.insertAfter($(element).next('label'));
			   }else{
				   error.insertAfter(element);
			   }
		   }
		},
		messages: {
			max_depositamount: {
				min: "Please enter a value greater than or equal to minimum deposit amount"
			},
			expusermax: {
				min: "Please enter a value greater than or equal to expected user minimum"
			}
		
		}
});
 $( "[name='partner_name']" ).rules( "add", {
  required: true,
  messages: {
    required: "Please select the partner",
  }
});

 $( "[name='campaignType']" ).rules( "add",{
  required: true,
  messages: {
    required: "Please select the campaign type",
  }
});
$( "[name='campaignname']" ).rules( "add",{
  required: true,
  
  messages: {
    required: "campaign name is required",
  },
  pattern: /^[a-zA-Z0-9-_'.$1\n& ]+$/
});
$( "[name='campaign_description']" ).rules( "add",{
  pattern: /^[a-zA-Z0-9-_'.$1\n& %]+$/
});
$( "[name='campaigncode']" ).rules( "add",{
  required: true,
  messages: {
    required: "campaign code is required",
  },
  pattern: /^[a-zA-Z0-9_'.1\n]+/
});
$( "[name='campaignurl']" ).rules( "add",{
  required: true,
  messages: {
    required: "url is required",
  }
});
$( "[name='startdate']" ).rules( "add",{
  required: true,
  messages: {
    required: "Select the campaign start date",
  }
});

$( "[name='p_promovalue']" ).rules( "add",{
	required: {
		depends: function(element){
			return $('#p_promochips').is(":checked");
		}
	},
	messages: {
    required: "promo value is required",
  },
  number: true
}); 
$( "[name='p_promocapvalue']" ).rules( "add",{
	required: {
		depends: function(element){
			return $('#p_promochips').is(":checked");
		}				
	},
	messages: {
    required: "Promo Cap Value is required",
  },
	number: true
});
$( "[name='promominvalue']" ).rules( "add",{
		number: true		
});
$( "[name='coins_required']" ).rules( "add",{
		number: true		
});
$( "[name='promomaxvalue']" ).rules( "add",{
		number: true		
});
$( "[name='promovalue1']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#campaignType').val() == '2';
			}
		},
		number: true		
});
$( "[name='promovalue1']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#campaignType').val() == '3';
			}
		},
		number: true		
});
$( "[name='p_creditdebitcardvalue']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#p_unclaimedbonus').is(":checked");
			}				
		},
		messages: {
			required: "Card Value is required",
		},
		number: true
});
$( "[name='p_netbankingvalue']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#p_unclaimedbonus').is(":checked");
			}				
		},
		messages: {
			required: "Netbanking Value is required",
		},
		number: true
});
$( "[name='p_bonuscapvalue']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#p_unclaimedbonus').is(":checked");
			}				
		},
		messages: {
			required: "Enter the Bonus Cap Value",
		},
		number: true
});
$( "[name='p_tournamentticketid']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#p_tournamenttickets').is(":checked");
			}	
	    }	
});
$( "[name='s_promovalue']" ).rules( "add",{
		required: {
			depends: function(element){
				return $('#s_promochips').is(":checked");
			}				
		},
		messages: {
			required: "Enter the Promo Value",
		},
		number: true
});
$( "[name='s_promochips']" ).rules( "add",{ 
	required: true,
});
$( "[name='s_tournamentticketid']" ).rules( "add",{ 
		required: {
			depends: function(element){
				return $('#s_tournamenttickets ').is(":checked");
			}	
		},
		messages: {
			required: "Select Tournament",
		},		
});
$( "[name='p_tournamentticketid']" ).rules( "add",{ 
		required: {
			depends: function(element){
				return $('#p_tournamenttickets ').is(":checked");
			}	
		},
		messages: {
			required: "Select Tournament",
		},		
});

$( "[name='expusermin']" ).rules( "add",{
	required: true,
	number: true,
	
  messages: {
    required: "This field is required",
  }
});	
$( "[name='expusermax']" ).rules( "add",{
  required: true,
  number: true,
  min: function() {
		return parseInt($('#expusermin').val());
	},
  messages: {
    required: "This field is required",
  }
});
$( "[name='noOfUniqueCode']" ).rules( "add",{
  required: true,
  number: true,
  messages: {
    required: "Enter number of Unique Code",
  }
});
$( "[name='excel_camp']" ).rules( "add",{
  extension: "xlsx|xls|csv",
  messages: {
    required: "Enter select valid mime type",
  }
});	
$( "[name='excel_camp_regist']" ).rules( "add",{
  extension: "xlsx|xls|csv",
  messages: {
    required: "Enter select valid mime type",
  }
});	
$( "[name='cost']" ).rules( "add",{
  number: true,
	min: 0
});
$( "[name='costperuser']" ).rules( "add",{
  number: true,
	min: 0
});

$( "[name='expecteduser']" ).rules( "add",{
  number: true,
	min: 0
});
$( "[name='promovalue2']" ).rules( "add",{
  number: true,
	min: 0
});
$( "[name='m_depositamount']" ).rules( "add",{
  number: true,
	min: 0
	
});
$( "[name='max_depositamount']" ).rules( "add",{
  number: true,
	//min: 0,
	min: function() {
			return parseInt($('#m_depositamount').val());
		}
	
});
$( "[name='s_promovalue']" ).rules( "add",{
  number: true,
	min: 0
});
$( "[name='campaign_remark']" ).rules( "add",{
  pattern: /^[a-zA-Z0-9-_'.$1\n& ]+$/
	
});
	

$( "[name='ref_username']" ).rules( "add",{ 
	required: {
			depends: function(element){
				return $("#ref_precentage").val()!="";
			}					
	},
	remote: {
				"url": `${window.pageData.baseUrl}/marketing/managecampaign/chkUserValid`,
				type: "post",
				data: {
				  ref_username: function() {
					return $( "#ref_username" ).val();
				  },
				}
	},
	messages: { 
		remote: "Invalid Username"
	}
});
$( "[name='ref_precentage']" ).rules( "add",{ 
		required: {
			depends: function(element){
				return $("#ref_username").val()!="";
			}					
		},
		number: true

});

$( "[name='p_promochips']" ).rules( "add",{ 
		require_from_group: [1, ".check-pay-group"]


});
$( "[name='p_unclaimedbonus']" ).rules( "add",{ 
		require_from_group: [1, ".check-pay-group"]
	
});


 
function isValidInputs(curStepBtn){
	isValid = true;
			if(curStepBtn == "step-1")
			{	
				$("[name='campaigncode'],[name='partner_name'],[name='campaignurl'],[name='campaignType'],[name='campaignname'],[name='startdate'],[name='campaign_remark'],[name='campaign_description']").each( function(index) {
					var xy = form_validator.element(this);
					isValid = isValid && (typeof xy == 'undefined' || xy);
				});
			}else if(curStepBtn == "step-2"){
				$("[name='p_promovalue'],[name='p_promocapvalue'],[name='promovalue1'],[name='p_creditdebitcardvalue'],[name='p_netbankingvalue'],[name='p_bonuscapvalue'],[name='p_tournamentticketid'],[name='s_tournamentticketid'],[name='ref_username'],[name='m_depositamount'],[name='max_depositamount'],[name='s_promovalue'],[name='promominvalue'],[name='p_promochips'],[name='p_unclaimedbonus'],[name='coins_required'],[name='promomaxvalue'],[name='s_promochips']").each( function(index) {
					var xy = form_validator.element(this);
					isValid = isValid && (typeof xy == 'undefined' || xy);
				});
			}else if(curStepBtn == "step-3"){
				$("[name='expusermin'],[name='expusermax'],[name='noOfUniqueCode'],[name='cost'],[name='costperuser'],[name='expecteduser']").each( function(index) {
					var xy = form_validator.element(this);
					isValid = isValid && (typeof xy == 'undefined' || xy);
				});
			}else if(curStepBtn == "step-4"){
				var file = $('#excel_camp').val();
				var fileContact = $('#excel_camp_regist').val();
				if((file == "" || file == null) && (fileContact == "" || fileContact == null)) {
					$("#createcampaign").submit();
				}else{
					
					var form_data = new FormData(); 
					
					
					//form_data.append("isUploadExcel", 1);
					/**
					* If payment Campaign type == register then execute this code 
					*/
					if(typeof $('input[name="excel_camp_regist"]')[0].files[0] != 'undefined'){
						form_data.append("excel_camp_regist", $('input[name="excel_camp_regist"]')[0].files[0]);
						// 					`${window.pageData.baseUrl}/marketing/managecampaign/chkExcelValidContact`
						$.ajax({
						"url": `${window.pageData.baseUrl}/marketing/managecampaign/chkExcelValidContact`,
						"type": "POST",
						"data": form_data,
						"cache": false,
						"contentType": false, 
						"processData": false, 
						beforeSend:function(){
							$('#frmsubmit').text('Loading...').prop('disabled',true);
						},
						success: function (data) {
						  if(data){
							$('#basic-datatable-modal-contact').DataTable().destroy();
							if(data.status == 101){
								$("#excelErrorReg").html(data.message).addClass("InvalidRow");
								$('#frmsubmit').text('Save').prop('disabled',false);
							}else if(data.status == 102){
								$("#excelErrorReg").html(data.message).addClass("InvalidRow");
								$('#frmsubmit').text('Save').prop('disabled',false);
							}else{
								var newData = [];
								newData[0] = data;
								var htmldata  = getUserExcelDataContact(data);  
								$("#accordion-modal-contact").find("table").html(htmldata);
								$('#basic-datatable-modal-contact').DataTable();
								$('#accordion-modal-contact').modal('show');
								$('#frmsubmit').text('Save').prop('disabled',false);
							}
							
						  }else{
							$('#accordion-modal-contact').modal('hide');
						  }
						}
					});
					}
					/**
					* If payment Campaign type == payment,firstdeposite etc then execute this code 
					*/
					if(typeof $('input[name="excel_camp"]')[0].files[0] != 'undefined'){
						
						form_data.append("excel_camp", $('input[name="excel_camp"]')[0].files[0]);
						
						$.ajax({
						"url": `${window.pageData.baseUrl}/marketing/managecampaign/chkUsersExcelValid`,
						"type": "POST",
						"data": form_data,
						"cache": false,
						"contentType": false, 
						"processData": false, 
						beforeSend:function(){
							$('#frmsubmit').text('Loading..').prop('disabled',true);
						},
						success: function (data) {
						  if(data){
							$('#basic-datatable-modal').DataTable().destroy();
							if(data.status == 101){
								$("#excelError").html(data.message).addClass("InvalidRow");
								$('#frmsubmit').text('Save').prop('disabled',false);
							}else if(data.status == 102){
								$("#excelError").html(data.message).addClass("InvalidRow");
								$('#frmsubmit').text('Save').prop('disabled',false);
							}else{
								var newData = [];
								newData[0] = data;
								var htmldata = getUsersExcelData(data);
								$("#accordion-modal-user").find("table").html(htmldata);
								$('#basic-datatable-modal').DataTable();
								$('#accordion-modal-user').modal('show');
								$('#frmsubmit').text('Save').prop('disabled',false);
							}
							
						  }else{
							$('#accordion-modal-user').modal('hide');
						  }
						}
					});//ajax close
					}//if close
				}//else close
			
			}//if close
			return isValid;
}

$(document).ready(function(){
 var navListItems = $('.setup-panel li a'),
	allWells = $('.setup-content'),
	allPrevBtn = $('.previousbtn'),
	allNextBtn = $('.nextBtn');

    allWells.hide();
	allPrevBtn.click(function(e){
		
		var curStep = $(this).closest(".setup-content");
		var curStepBtn = curStep.attr("id");
		prevStepWizard = $('.setup-panel li a[href="#' + curStepBtn + '"]').parent().prev().children("a");
		// curInputs = curStep.find("input[type='text'],input[type='url']");
		prevStepWizard.trigger('click');
		return false;

	});
	
	allNextBtn.click(function(){
		console.log("button click");
		$("ul.parsley-errors-list.filled").remove();
        var curStep = $(this).closest(".setup-content");
            curStepBtn = curStep.attr("id");
            nextStepWizard = $('.setup-panel li a[href="#' + curStepBtn + '"]').parent().next().children("a");
            curInputs = curStep.find("input[type='text'],input[type='url']");
            isValid = true;
			
			isValid = isValidInputs(curStepBtn);
			// if Tab One

			if (isValid){nextStepWizard.removeAttr('disabled').trigger('click');
			}
			else if (curStepBtn == 'step-4'){
				
			}
	});

    navListItems.click(function (e) {
		e.preventDefault();
		var $target = $($(this).attr('href')),
		 $item = $(this);
		if (!$item.hasClass('disabled')) {
            navListItems.removeClass('active').addClass('btndefault');
            $item.addClass('active');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
            $target.find('select:eq(0)').focus();
        }
	 });

   
    $('.setup-panel li  a.active').trigger('click');
	//$("createcampaign").submit();

});		

$('.select').on( 'hide.bs.select', function ( ) {
    $(this).trigger("focusout");
});
function refreshBTN(){
	$("#campaigncode").val('');
}
function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle)
            return true;
    }
    return false;
}
function findCostPerUserValue() {
	var cCost = document.getElementById('cost').value;
	var eUser = document.getElementById('expecteduser').value;	
	if(!isNaN(cCost) && !isNaN(eUser)) {
		var cPUserVal = cCost / eUser;
		document.getElementById('costperuser').value= parseInt(cPUserVal);
;
	}
}
// modal close
$(".card-widgets,.card-button").on("click", function(){
    $('#accordion-modal-user').css('display', 'none');
});
$(".card-widgets,.card-button").on("click", function(){
    $('#accordion-modal-contact').css('display', 'none');
});
function getUsersExcelData(data){
	if (data!='') {
		var element = '';
		for (let i = 0;i<data.userStatusResponse.length;i++) {
			$('#basic-datatable tbody').html('');
			//$('#basic-datatable-').DataTable().destroy();
			
			element += `<tr class="${data.userStatusResponse[i].styleClass}">
			<td>${(i+1)}</td>
			<td>${data.userStatusResponse[i].userName}</td>
			<td>${data.userStatusResponse[i].STATUS}</td>
			
		</tr>`;
		}
		$("#getUserExcelData").html(element);
		
	}
}
function getUserExcelDataContact(data){
	if (data!='') {
		
		var element = '';
		for (let i = 0;i<data.userStatusResponse.length;i++) {
			$('#basic-datatable tbody').html('');
			//$('#basic-datatable').DataTable().destroy();
			
			element += `<tr class="${data.userStatusResponse[i].styleClass}">
			<td>${(i+1)}</td>
			<td>${data.userStatusResponse[i].userContact}</td>
			<td>${data.userStatusResponse[i].STATUS}</td>
			
		</tr>`;
		}

		$("#getUserExcelDataContact").html(element);
	}
	
}
$('#confirmExcelUpload').on('click', function(){
	$("#createcampaign").submit();
	$(this).prop('disabled',true);
});
$('#confirmExcelUploadContact').on('click', function(){
	$("#createcampaign").submit();
	$(this).prop('disabled',true);
});
    
