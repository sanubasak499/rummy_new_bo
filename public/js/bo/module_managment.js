$(document).ready(function() {

    $(document).on('click', '.moduleEditViewButton', function(e) {
        var $form = $("#addEditModuleForm");
        var $e = $(e.target);
        var $moduleItem = $e.closest('.moduleItem');
        var $objData = $moduleItem.find('[data-obj]').data('obj');
        var $data = JSON.parse(atob($objData));

        $form.find(`[name="id"]`).val($data.id);
        $form.find(`[name="module_key"]`).val($data.module_key);
        $form.find(`[name="display_name"]`).val($data.display_name).prop('readonly', true);
        $form.find(`[name="description"]`).val($data.description);
        $form.find(`[name="menu_id"]`).val($data.menu_id).trigger('change');
        $form.attr('action', `${window.pageData.baseUrl}/settings/module/store/${$data.id}`);
    });

    $('#addEditModuleForm').validate({
        rules: {
            module_key: {
                required: true,
                fieldTrim: true,
                noSpace: true,
                remote: {
                    url: `${window.pageData.baseUrl}/WebApi/isModuleKeyExist`,
                    type: "post",
                    data: {
                        module_key: function() {
                            return $('#addEditModuleForm [name="module_key"]').val();
                        },
                        id: function() {
                            return $('#addEditModuleForm [name="id"]').val();
                        }
                    },
                    dataFilter: function(response) {
                        response = JSON.parse(response);
                        let result = false;
                        result = response.status == 201 ? true : `"Module key already exist"`;
                        return result;
                    }
                }
            },
            display_name: {
                required: true,
                fieldTrim: true
            },
            description: {
                required: true
            },
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});