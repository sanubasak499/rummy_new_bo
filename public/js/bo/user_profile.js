function responsibleGaming(user_id, type) {
    $('#nodata').hide();
    $('#commonid').show();
    $.ajax({
        type: "POST",
        url: `${window.pageData.baseUrl}/user/account/responsiblegaming`,
        data: {
            "USER_ID": user_id,
            "tab_type": type,
        },

        success: function(response) {
            $('#commonid').hide();
            console.log(response);
            var data = response.data || {};
            var depositlimit = response.depositlimit || {};

            if (type == 'deposit_withdra_details') {
                $('#total_succesfull_deposit').text(data['deposit']);
                $('#total_approved_withdrawals').text(data['rcWithdraw']);
                $('#total_comm_withdrawals').text(data['commissionWithdraw']);
                $('#deposit_withdraw').text(data['dwresult']);
            }

            if (type == 'baazi_rewards') {
                $('#accumulated_Coins').text(data.accumulated_coins);
                $('#coins_balance').text(data.coins_balance.USER_TOT_BALANCE);
                $('#accumulated_reward_points').text(data.accumulated_reward_points);
                $('#reward_points_spent').text(data.reward_points_spent);
                $('#reward_points_balance').text(data.reward_points_balance.USER_TOT_BALANCE);
                $('#current_rewards_program').text(data.current_rewards_program);
            }

            if (type == 'tax_details') {
                if (data == '') {
                    $('#nodata').show();
                } else {
                    var $tablebody = $('#taxTable tbody');
                    $tablebody.html('');
                    $.each(data, (i, e) => {

                        $tablebody.append(`
                            <tr>
                                <td>${i}</td>
                                <td>${e.quarter.apr_jun}</td>
                                <td>${e.quarter.july_sep}</td>
                                <td>${e.quarter.oct_dec}</td>
                                <td>${e.quarter.jan_mar}</td>
                                <td>${e.total}</td>
                            </tr>
                        `);
                    });
                }
            }

            if (type == 'reason_details') {
                if (data == '') {
                    $('#nodata').show();
                } else {
                    var $tablebody = $('#ReasonData tbody');
                    $tablebody.html('');
                    $.each(data, (i, e) => {
                          if(e.ACCOUNT_STATUS==0){
                            var $status='Inactive';
                          }else if(e.ACCOUNT_STATUS==1){
                            var $status='Active';
                          }else if(e.ACCOUNT_STATUS==2){
                            var $status='Blocked'; 
                          }
                        $tablebody.append(`
                            <tr>
                                <td>${e.UPDATED_ON}</td>
                                <td> ${$status}</td>
                                <td>${e.UPDATED_BY }</td>
                                <td>${e.ACTIONS_REASON }</td>
                            </tr>
                        `);
                    });
                }
            }

            if (type == 'resp_gaming') {
                $('.responsibleGamingData').html(`
                    <tr>
                        <td>Cash Limit</td>
                        <td>${data.CASH_MAX_BIG_BLIND || '--'}</td>
                        <td>${data.CASH_ACTIVE ? 'Active' : 'Inactive'}</td>
                    </tr>
                    <tr>
                        <td>OFC Limit</td>
                        <td>${data.OFC_MAX_POINT || '--'}</td>
                        <td>${data.OFC_ACTIVE ? 'Active' : 'Inactive'}</td>
                    </tr>
                    <tr>
                        <td>Tournament Buy In</td>
                        <td>${data.TOUR_TOTAL_BUYIN || '--'}</td>
                        <td>${data.TOUR_ACTIVE ? 'Active' : 'Inactive'}</td>
                    </tr>
                    <tr>
                        <td>Poker Break</td>
                        <td>--</td>
                        <td>${data.BREAK_ACTIVE ? 'Active' : 'Inactive'}</td>
                    </tr>
                    <tr>
                        <td>Withdraw Reversal</td>
                        <td>--</td>
                        <td>${data.WITHDRAWAL_REVERSAL_RESTRICTION ? 'Active' : 'Inactive'}</td>
                    </tr>
                `);

                $('.depositLimitData').html(`
                    <tr>
                        <td>Txn Limit</td>
                        <td>${depositlimit.DEP_AMOUNT || '--'}</td>
                        <td>--</td>
                            
                    </tr>
                    <tr>
                        <td>Daily Limit</td>
                        <td>${depositlimit.DEP_AMOUNT_PER_DAY || '--'}</td>
                        <td>${depositlimit.TRANSACTIONS_PER_DAY || '--'}</td>
                            
                    </tr>
                    <tr>
                        <td>Weekly Limit</td>
                        <td>${depositlimit.DEP_AMOUNT_PER_WEEK || '--'}</td>
                        <td>${depositlimit.TRANSACTIONS_PER_WEEK || '--'}</td>
                    </tr>
                    <tr>
                        <td>Monthly Limit</td>
                        <td>${depositlimit.DEP_AMOUNT_PER_MONTH || '--'}</td>
                        <td>${depositlimit.TRANSACTIONS_PER_MONTH || '--'}</td>
                    </tr>
                `);
            }

        }
    })
}


// function saveToDatabase(editableObj, column, userid) {
//     $(editableObj).css("background", "#FFF url(loaderIcon.gif) no-repeat right");
//     $.ajax({
//         url: `${window.pageData.baseUrl}/user/account/updateuserprofile`,
//         type: "POST",
//         data: $("form").serialize(),
//         data: 'column=' + column + '&editval=' + editableObj + '&userid=' + userid,
//         success: function(data) {
//             $(editableObj).css("background", "#FDFDFD");
//         }
//     });
// }

function hideContactInfo() {
    document.getElementById("hideContactInfo").style.display = "none";
    document.getElementById("editUpdate").style.display = "block";
    $('.formEditViewBtn').toggle();
}

function hideEditUpdate() {
    document.getElementById("hideContactInfo").style.display = "block";
    document.getElementById("editUpdate").style.display = "none";
    $('.formEditViewBtn').toggle();
}

$('#showreason').hide();
$('#showsuspendDate').hide();

function changeStatus() {
    if (document.getElementById('changeAcStatus').value != 3) {
        $('#showreason').hide();
        $('#showsuspendDate').hide();
    } else {
        $('#showreason').show();
        $('#showsuspendDate').show();
    }
}

$(document).ready(function() {
    $("form").each(function() {
        $(this).validate({

        })
    });
})

function uperCase(id) {
    var upercaseid = document.getElementById(id);
    upercaseid.value = upercaseid.value.toUpperCase();
    var panVal = $('#PAN_NO').val();
    var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;

    if (regpan.test(panVal)) {
        // valid pan card number
        $('#valid_pan').html("Valid Pan Card No").css("color", "green");
        return true;
    } else {
        // invalid pan card number
        $('#valid_pan').html("Invalid Pan Card No").css("color", "red");
        return false;
    }
}

//update password expiry
function passwordExpiry(password_id, userid) {
    Swal.fire({
        title: 'Are you sure? You want to update.',
        //text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Proceed!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                method: "post",
                url: `${window.pageData.baseUrl}/user/account/updatepasswordexpiry`,
                data: {
                    "user_id": userid,
                    "password_expiry": password_id,
                },
                success: function(response) {
                    if (response.status == 200) {
                        $.NotificationApp.success(`${response.message}`, `${response.title}`);
                        location.reload();
                    } else {
                        $.NotificationApp.error(`${response.message}`, `${response.title}`);
                    }
                }
            });
        }
    });
}

//update two step verification
function twoStepVerification(two_step_id, userid) {
    Swal.fire({
        title: 'Are you sure? You want to update.',
        //text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Proceed!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                method: "post",
                url: `${window.pageData.baseUrl}/user/account/twostepverification`,
                data: {
                    "user_id": userid,
                    "two_step_id": two_step_id,
                },
                success: function(response) {
                    if (response.status == 200) {
                        $.NotificationApp.success(`${response.message}`, `${response.title}`);
                        location.reload();
                    } else {
                        $.NotificationApp.error(`${response.message}`, `${response.title}`);
                    }
                }
            })
        }
    });
}


// Get Address When search user by address or city or pincode
var getCity = (function getCityRef(state = null, wherecond = null, CITY = null, selectedVal = null) {
    if ($.trim(state.value) != "") {
        $.ajax({
            method: "post",
            url: `${window.pageData.baseUrl}/user/account/getcity`,
            data: {
                "getstate": state.value,
                "wherecond": wherecond,
                "selectcond": CITY,
            },
            success: function(data) {
                if (wherecond == 'STATE') {
                    $('#city').find('option').text(data);
                    $('#city').html('');
                    $('#city').append(`<option value="">Select City</option>`);
                    $.each(data, (i, e) => {
                        $('#city').append(`<option value="${e}">${e}</option>`);
                    });
                    // $("#city").selectpicker("refresh");
                    $("#city").val(selectedVal);
                } else {
                    $('#pincode').find('option').text(data);
                    $('#pincode').html('');
                    $('#pincode').append(`<option value="">Select Pin Code</option>`);
                    $.each(data, (i, e) => {
                        $('#pincode').append(`<option value="${e}">${e}</option>`);
                    });
                    // $("#pincode").selectpicker("refresh");
                }
            }
        });
    } else {
        $('#city').html('');
        $('#city').append(`<option value="">Select City</option>`);
    }
    return getCityRef;
}($('#state').get(0), 'STATE', 'CITY', $("#city").data('city')));