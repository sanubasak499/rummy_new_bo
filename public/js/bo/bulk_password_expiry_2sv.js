$(document).ready(function () {
    $("#import-excel-reset").click(function () {
        $("#bulkPwdExpiryUploadForm").trigger("reset");
        $('#bulkPwdExpirySubmitForm-success').remove();
        $('#bulkPwdExpirySubmitForm-error').remove();
    });
    $('#bulkPwdExpiryUploadForm').validate({
        rules: {
            adjustExcel: {
                required: true
            }
        }
    });
    $('#bulkPwdExpiryReUpload').click(function () {
        $('#bulkPwdExpiryUploadpopup table tbody').html('');
        $("#bulkPwdExpiryUploadForm").trigger("reset");
        $('#show-import-excel').hide();
        $('#bulkPwdExpiryUploadpopup').modal('hide');
        $('#bulkPwdExpirySubmitForm-success').remove();
        $('#bulkPwdExpirySubmitForm-error').remove();
    });
    $("#bulkPwdExpirySubmitForm").on('click', (e) => {
        e.preventDefault();
    });

    $("#bulkPwdExpiryUploadBtn").on('click', (e) => {
        e.preventDefault();
        $('#bulkPwdExpiryUploadpopup table tbody').html('');
        $('#bulkPwdExpiryUploadpopup table').DataTable().destroy();
        $('#bulkPwdExpiryUploadpopup table').DataTable();
        if ($("#bulkPwdExpiryUploadForm").valid()) {
            e.preventDefault();
            var form = $('#bulkPwdExpiryUploadForm');
            var data = new FormData(jQuery('#bulkPwdExpiryUploadForm')[0]);
            data.append('excel_file', $('#bulkPwdExpiryExcelUpload').get(0).files[0]);
            $("#bulkPwdExpiryUploadBtn").hide();
            $('#show-import-excel').show();
            $("#import-excel-reset").hide();
            $('#bulkPwdExpirySubmitForm-success').remove();
            $('#bulkPwdExpirySubmitForm-error').remove();
            $.ajax({
                //url: `${window.pageData.baseUrl}/WebApi/excelToArray`,
                url: `${window.pageData.baseUrl}/users/passwordexpiry/verifydata`,
                type: "post",
                data: data,
                enctype: 'multipart/form-data',
                processData: false, // Important!
                contentType: false,
                cache: false,
                success: function (response) {
                    $("#bulkPwdExpiryUploadBtn").show();
                    $('#show-import-excel').hide();
                    $("#import-excel-reset").show();
                    if (response.status == 200) {
                        $('#bulkPwdExpiryUploadpopup').modal('show');
                        $('#bulkPwdExpirySubmitForm-error').remove();
                        $("#bulkPwdExpirySubmitBtn").show();
                        $("#bulkPwdExpiryReUpload").show();
                        /*console.log(response.data);*/
                        let tableData = "";
                        $x = 0;
                        $('#userIds').attr('value', response.userIds);
                        $('#Enable2SV').attr('value', response.Enable2SV);
                        $('#ExpirePwd').attr('value', response.ExpirePwd);
                        $.each(response.data, function (i, member) {
                            /*for (var i in member) */
                            {
                                $x++;
                                var color = "";
                                var reviewColor = "";
                                var enablePwdExpiry = "";
                                var enable2sv = "";
                                if (member['status'] == "Ok" || member['status'] == "Repeated Username") {
                                    color = "Green";
                                } else {
                                    color = "Red";
                                }
                                if (member['status'] == "Invalid User") {
                                    enablePwdExpiry = "--";
                                    enable2sv = "--";
                                } else {
                                    if (member['status'] == "Repeated Username") {
                                        reviewColor = "Red";
                                    }

                                    enablePwdExpiry = member['ExpirePwd'];
                                    enable2sv = member['Enable2SV'];
                                }
                                tableData += '<tr style="color:' + color + '"><td>' + $x + '</td><td>' + member['userId'] + '</td><td>' + member['Username'] + '</td><td style="color: ' + reviewColor + '">' + member['status'] + '</td><td>' + enablePwdExpiry + '</td><td>' + enable2sv + '</td></tr>';
                                /*console.log("userId: " + member['userId']);*/
                            }
                        });
                        $('#bulkPwdExpiryUploadpopup table tbody').html('');
                        $('#bulkPwdExpiryUploadpopup table').DataTable().destroy();
                        $('#bulkPwdExpiryUploadpopup table tbody').html(tableData);
                        $('#bulkPwdExpiryUploadpopup table').DataTable();
                    }
                    if (response.status == 302) {
                        $('#bulkPwdExpiryExcelUpload-error').remove();
                        $('#bulkPwdExpiryExcelUpload').addClass('error').removeClass('valid');
                        $('#bulkPwdExpiryExcelUpload').after(`<label id="bulkPwdExpiryExcelUpload-error" class="error" for="bulkPwdExpiryExcelUpload">${response.message} "${response.data}"</label>`);
                    }
                    if (response.status == 500) {
                        $('#bulkPwdExpiryExcelUpload-error').remove();
                        $('#bulkPwdExpiryExcelUpload').addClass('error').removeClass('valid');
                        $('#bulkPwdExpiryExcelUpload').after(`<label id="bulkPwdExpiryExcelUpload-error" class="error" for="bulkPwdExpiryExcelUpload">${response.message}</label>`);
                    }
                },
                error: function (response) {
                    $("#bulkPwdExpiryUploadBtn").show();
                    $('#show-import-excel').hide();
                    $("#import-excel-reset").show();
                    /*console.log(response);*/
                    $('#bulkPwdExpiryExcelUpload-error').remove();
                    $('#bulkPwdExpiryExcelUpload').addClass('error').removeClass('valid');
                    $('#bulkPwdExpiryExcelUpload').after(`<label id="bulkPwdExpiryExcelUpload-error" class="error" for="bulkPwdExpiryExcelUpload">${response.message}</label>`);
                }

            });

        }
    });


    $("#bulkPwdExpirySubmitBtn").on('click', (e) => {
        if ($("#bulkPwdExpirySubmitForm").valid()) {
            var form = $('#bulkPwdExpiryUploadForm');
            var data = new FormData(jQuery('#bulkPwdExpirySubmitForm')[0]);
            $("#bulkPwdExpirySubmitBtn").hide();
            $("#bulkPwdExpiryReUpload").hide();
            $('#show-bulk-upload-submit').show();
            $('#bulkPwdExpirySubmitForm-success').remove();
            $('#bulkPwdExpirySubmitForm-error').remove();
            $.ajax({
                //url: `${window.pageData.baseUrl}/WebApi/excelToArray`,
                url: `${window.pageData.baseUrl}/users/passwordexpiry/verifydata/update`,
                type: "post",
                data: data,
                enctype: 'multipart/form-data',
                processData: false, // Important!
                contentType: false,
                cache: false,
                success: function (response) {
                    $("#bulkPwdExpirySubmitBtn").show();
                    $("#bulkPwdExpiryReUpload").show();
                    $('#show-bulk-upload-submit').hide();
                    $('#bulkPwdExpirySubmitForm').append(response);
                    if (response.status == 200) {
                        $("#bulkPwdExpirySubmitBtn").hide();
                        $("#bulkPwdExpiryReUpload").hide();
                        $('#show-bulk-upload-submit').hide();
                        /*$('#bulkPwdExpiryUploadpopup').modal('hide');*/
                        $("#bulkPwdExpiryUploadForm").trigger("reset");
                        $.NotificationApp.send("Success", "Updated Successfully", 'top-right', '#5ba035', 'success');
                        $('#bulkPwdExpirySubmitForm-success').remove();
                        $('#bulkPwdExpirySubmitForm').addClass('success').removeClass('valid');
                        $('#bulkPwdExpirySubmitForm').after(`<label id="bulkPwdExpirySubmitForm-success" class="success" for="bulkPwdExpirySubmitForm" style='color:green'>${response.message} </label>`);
                    }
                    if (response.status == 302) {
                        $('#bulkPwdExpirySubmitForm-error').remove();
                        $('#bulkPwdExpirySubmitForm').addClass('error').removeClass('valid');
                        $('#bulkPwdExpirySubmitForm').after(`<label id="bulkPwdExpirySubmitForm-error" class="error" for="bulkPwdExpirySubmitForm">${response.message} "${response.data}"</label>`);
                    }
                    if (response.status == 500) {
                        $('#bulkPwdExpirySubmitForm-error').remove();
                        $('#bulkPwdExpirySubmitForm').addClass('error').removeClass('valid');
                        $('#bulkPwdExpirySubmitForm').after(`<label id="bulkPwdExpirySubmitForm-error" class="error" for="bulkPwdExpirySubmitForm">${response.message}</label>`);
                    }
                },
                error: function (response) {
                    $("#bulkPwdExpirySubmitBtn").show();
                    $('#show-bulk-upload-submit').hide();
                    $("#bulkPwdExpiryReUpload").show();
                    /*console.log(response);*/
                    $('#bulkPwdExpirySubmitForm-error').remove();
                    $('#bulkPwdExpirySubmitForm').addClass('error').removeClass('valid');
                    $('#bulkPwdExpirySubmitForm').after(`<label id="bulkPwdExpirySubmitForm-error" class="error" for="bulkPwdExpirySubmitForm">${response.message}</label>`);
                }

            });
        }
    });
});