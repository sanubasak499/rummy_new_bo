// $(document).ready(function() {
//     $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
//         disableOn: 700,
//         type: 'iframe',
//         mainClass: 'mfp-fade',
//         removalDelay: 160,
//         preloader: false,
//         fixedContentPos: false
//     });
// });


function checkContest(contest_id) {
    //$('#managecontestdata, #managecontestdata div').show().css({ 'z-index': 99 });
    $('#managecontestdata, #managecontestdata div').show();
    $("#model_term_cond1").html("");
    $.ajax({
        url: `${window.pageData.baseUrl}/marketing/contest/getContestModelData`,
        type: "post",
        data: {
            CONTEST_ID: contest_id
        },
        success: function(response) {
            //$('#managecontestdata, #managecontestdata div').hide().css({ 'z-index': -1 });
            $('#managecontestdata, #managecontestdata div').hide();
            if (response.status == 200) {
                $('#model_contest_title').html(response.data.CONTEST_TITLE);
                $('#model_contest_desc').html(response.data.CONTEST_DESCRIPTION);
                $('#model_video_link').html(response.data.CONTEST_VIDEO_URL);
                //$("#model_video_link").attr("href", response.data.CONTEST_VIDEO_URL);
                $("#model_video_link").attr("src", response.data.CONTEST_VIDEO_URL);
                $('#model_video_desc').html(response.data.CONTEST_VIDEO_DESCRIPTION);
                var term = response.data.CONTEST_TERMS_CONDITIONS.split('.');
                $.each(term, function(key, value) {
                    $("#model_term_cond1").append(`<li>${value}</li>`);
                });
            }
        }
    })
}

function myfunction(videoUrl) {
    var url = $("#youtube_video_model" + videoUrl).attr('src');
    $("#myModal").on('hide.bs.modal', function() {
        $("#embedvideo").attr('src', '');
    });

    $("#myModal").on('show.bs.modal', function() {
        $("#embedvideo").attr('src', url);
    });
}

function myfunctionvideo() {
    var url = $("#model_video_link").attr('src');
    $("#myModal").on('hide.bs.modal', function() {
        $("#embedvideo").attr('src', '');
    });

    $("#myModal").on('show.bs.modal', function() {
        $("#embedvideo").attr('src', url);
    });
}

function changeStatus(contest_id, msg, status_id) {
    //if (confirm(msg)) 
    Swal.fire({
        title: msg,
        //text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Proceed!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: `${window.pageData.baseUrl}/marketing/contest/updateStatus`,
                type: "post",
                data: {
                    CONTEST_ID: contest_id,
                    STATUS: status_id
                },
                success: function(response) {
                    if (response.status == 200) {
                        if (status_id == 0) {
                            $.NotificationApp.success("Contest  is in-active", "Congrats");
                        } else {
                            $.NotificationApp.success("Contest  is active", "Congrats");
                        }
                        $('#search_contest').trigger("click");
                    } else if (response.status == 302) {
                        $.NotificationApp.error(`${response.message}`, "Sorry");
                    } else {
                        $.NotificationApp.error("Status updation failed", "Sorry");
                    }
                }
            })
        }
    });
}

$(document).ready(function() {
    $('#userContestForm').validate({
        rules: {
            contest_name: {
                required: true,
                maxlength: 100
            },
            contest_title: {
                required: true,
                maxlength: 100
            },
            contest_video_url: {
                required: true,
                url: true
            },
            contest_description: {
                maxlength: 250
            },
            contest_video_description: {
                maxlength: 250
            },
            // contest_terms_conditions:{
            //    pattern: /^[a-zA-Z0-9-_'.$1\n& ]+$/
            // }
        }

    });
});

$(document).ready(function() {
    $("#userContestForm").submit(function(e) {
        if ($('#userContestForm').valid()) {
            $("#create_contest_form").prop("disabled", true);
            $("#update_create_contest").prop("disabled", true);
            return true;
        }
    });
});