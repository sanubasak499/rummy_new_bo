$(document).ready(function() {
    $("#updateAdjustCoinData").validate({
        rules: {
            transactionpassword: {
                required: true,
            }
        }
    });

    $("#confirmUpdateExcelUpload").validate({
        rules: {
            tpassword: {
                required: true
            }
        }
    });

    $("#reset-btn").click(function() {
        $("#userSearchForm").trigger("reset");
    });

    $("#import-excel-reset").click(function() {
        $("#adjustmentExcelImport").trigger("reset");
    });

    $("#reset-confirm-excel").click(function() {
        $("#adjustmentExcelImport").trigger("reset");
    });

    function blurTable() {
        $("#AdjustPointTable").find(`[data-balance-type="USER_DEPOSIT_BALANCE"]`).text('');
        $("#AdjustPointTable").find(`[data-balance-type="USER_PROMO_BALANCE"]`).text('');
        $("#AdjustPointTable").find(`[data-balance-type="USER_WIN_BALANCE"]`).text('');
        $("#AdjustPointTable").find(`[data-balance-type="USER_TOT_BALANCE"]`).text('');
        $("#AdjustPointTable").find(`[data-partner-name]`).text('');
        $("#fillFullName").find(`[data-user-id]`).text('');
        $("#fillFullName").find(`[data-user-fullname]`).text('');
        $("#AdjustPointTable").addClass('blureffect');
        $("#PARTNER_ID").val('');
    }
    $(`#userSearchForm [name="username"]`).on('blur', function() {
        if ($.trim($(this).val()) != "") {
            $.ajax({
                url: `${window.pageData.baseUrl}/users/adjustcoins/balanceadjust`,
                type: "post",
                data: {
                    username: $(this).val()
                },
                success: function(response) {
                    if (response.status == 200) {
                        var $user_points = response.data.user_points;
                        let trTemp = null;
                        $.each($user_points, ($key, $userPointsData) => {
                            trTemp = $("#AdjustPointTable").find(`[data-coin-type-id="${$userPointsData.COIN_TYPE_ID}"]`);
                            trTemp.find(`[data-balance-type="USER_DEPOSIT_BALANCE"]`).text($userPointsData.USER_DEPOSIT_BALANCE);
                            trTemp.find(`[data-balance-type="USER_PROMO_BALANCE"]`).text($userPointsData.USER_PROMO_BALANCE);
                            trTemp.find(`[data-balance-type="USER_WIN_BALANCE"]`).text($userPointsData.USER_WIN_BALANCE);
                            trTemp.find(`[data-balance-type="USER_TOT_BALANCE"]`).text($userPointsData.USER_TOT_BALANCE);
                        });
                        $("#AdjustPointTable").find(`[data-partner-name]`).text(response.data.partner_name);
                        $("#AdjustPointTable").removeClass('blureffect');
                        $("#fillFullName").find(`[data-user-id]`).text(response.data.user.USER_ID);
                        $("#fillFullName").find(`[data-user-id-value]`).val(response.data.user.USER_ID);
                        $("#fillFullName").find(`[data-user-fullname]`).text(response.data.user.FULLNAME);
                        $("#PARTNER_ID").val(response.data.user.PARTNER_ID);
                        result = true;
                    } else {
                        blurTable();
                    }
                }
            })
        }
    })
    $("#updateAdjustCoinData").submit((e) => {
        e.preventDefault();
    })
    $('#confirm').on('click', (e) => {
        var $e = $(e.target);
        if ($("#updateAdjustCoinData").valid()) {
            var data = $("#updateAdjustCoinData").serialize();
            $('#confirm').hide();
            $('.formActionButton').show();
            $.ajax({
                type: "POST",
                url: `${window.pageData.baseUrl}/users/adjustcoins`,
                data: data,
                success: function(response) {
                    $('#confirm').show();
                    $('.formActionButton').hide();
                    if (response.status == 301) {
                        $('#transactionpassword-error').remove();
                        $('#transactionpassword').addClass('error').removeClass('valid');
                        $('#transactionpassword').after(`<label id="transactionpassword-error" class="error" for="transactionpassword" style="">${response.message}</label>`);
                    } else if (response.status == 200) {
                        $(`#userSearchForm [name="username"]`).trigger('blur');
                        $('#aadharcardpopup').modal('hide');
                        $.NotificationApp.send("Success", "Amount Updated Successfully", 'top-right', '#5ba035', 'success');
                        window.location.reload();
                    }

                },
                error: function(response, e, er) {
                    console.log(response, e, er);
                }
            });
        }
    });

    $('#confirmExcelUpload').on('click', (e) => {
        var $e = $(e.target);
        if ($("#confirmUpdateExcelUpload").valid()) {
            e.preventDefault();
            var form = $('#adjustmentExcelImport');
            var data = new FormData(); // Currently empty
            data.append('excel_file', $('#adjustExcel').get(0).files[0]);
            data.append('transcation_passsword', $('#tpassword').val());
            $('#confirmExcelUpload').hide();
            $('#confirm-hide-excel').show();
            $.ajax({
                //url: `${window.pageData.baseUrl}/WebApi/excelToArray`,
                url: `${window.pageData.baseUrl}/users/adjustcoins/insertadjustexcel`,
                type: "post",
                data: data,
                enctype: 'multipart/form-data',
                processData: false, // Important!
                contentType: false,
                cache: false,
                success: function(response) {
                    $('#confirmExcelUpload').show();
                    $('#confirm-hide-excel').hide();
                    if (response.status == 301) {
                        $('#tpassword-error').remove();
                        $('#tpassword').addClass('error').removeClass('valid');
                        $('#tpassword').after(`<label id="tpassword-error" class="error" for="tpassword" style="">${response.message}</label>`);
                    } else if (response.status == 200) {
                        $.NotificationApp.send("Success", "Excel Data Uploaded", 'top-right', '#5ba035', 'success');
                        window.location.reload();
                    } else {
                        console.log(response);
                    }
                },
                error: function(err, e) {
                    console.log(err, e);
                }
            });
        }
    });

    $('#adjustmentExcelImport').validate({
        rules: {
            adjustExcel: {
                required: true
            }
        }
    });

    $("#importAdjustExcel").on('click', (e) => {
        if ($("#adjustmentExcelImport").valid()) {
            e.preventDefault();
            var form = $('#adjustmentExcelImport');
            var data = new FormData(); // Currently empty
            data.append('excel_file', $('#adjustExcel').get(0).files[0]);
            $("#importAdjustExcel").hide();
            $('#show-import-excel').show();

            $('#adjustmentcontenttable table').DataTable().clear();
            $('#adjustmentcontenttable table').DataTable().destroy();
            $('#adjustmentcontenttable table').DataTable({});

            $.ajax({
                //url: `${window.pageData.baseUrl}/WebApi/excelToArray`,
                url: `${window.pageData.baseUrl}/users/adjustcoins/adjustcoinexcel`,
                type: "post",
                data: data,
                enctype: 'multipart/form-data',
                processData: false, // Important!
                contentType: false,
                cache: false,
                success: function(response) {
                    if (response.status == 302) {
                        $('#adjustExcel-error').remove();
                        $('#adjustExcel').addClass('error').removeClass('valid');
                        $('#adjustExcel').after(`<label id="adjustExcel-error" class="error" for="adjustExcel">${response.message} "${response.data}"</label>`);
                        $.NotificationApp.send("error", response.message, 'top-right', '#FF9494', 'Sorry');
                        $('#show-import-excel').hide();
                        $('#importAdjustExcel').show();
                    }
                    if (response.status == 500) {
                        $('#adjustExcel-error').remove();
                        $('#adjustExcel').addClass('error').removeClass('valid');
                        $('#adjustExcel').after(`<label id="adjustExcel-error" class="error" for="adjustExcel">${response.message}</label>`);
                        $.NotificationApp.send("error", response.message, 'top-right', '#FF9494', 'Sorry');
                        $('#show-import-excel').hide();
                        $('#importAdjustExcel').show();
                    }
                    if (response.message == 'success') {
                        $("#importAdjustExcel").show();
                        $('#show-import-excel').hide();
                        //if (response.status == 200) {
                        dataHeadings = dataArray = [];
                        dataHeadings = response.data.heading;
                        dataArray = response.data.data;
                        if (dataHeadings == '#,Username,Balance Type,Adjust Type,Coin Type,Amount,Comments,Reason,Technical Loss') {
                            $('#adjustmentcontenttable table tbody').html('');
                            $('#adjustmentcontenttable table').DataTable().destroy()
                            let count = 0;
                            $.each(dataArray, (index, value) => {
                                var trData = $(`<tr class="${response.flags[index] ? 'text-success' : 'text-danger'}"></tr>`).appendTo('#adjustmentcontenttable table tbody');
                                $.each(dataHeadings, (i, v) => {
                                    if (i == 0) {
                                        trData.append(`<td>${++count}</td>`);
                                    } else {
                                        trData.append(`<td>${value[v]}</td>`);
                                    }
                                })
                                trData.append(`<td>${response.status[index]}</td>`);
                            });

                            $('#adjustmentcontenttable table').DataTable({

                            });
                            $('#adjustmentcontenttable').modal('show');
                        } else {
                            $.NotificationApp.send("error", "Excel Format Not Match", 'top-right', '#FF9494', 'error');
                        }
                    }

                }
            });

        }
    });

    $("#userSearchForm").validate({
        //ignore: [],
        rules: {
            username: {
                required: true,
                remote: {
                    url: `${window.pageData.baseUrl}/users/adjustcoins/balanceadjust`,
                    type: "post",
                    dataFilter: function(response) {
                        response = JSON.parse(response);
                        let result = false;
                        if (response.status == 200) {
                            $(`#userSearchForm [name="username"]`).trigger('blur');
                            result = true;
                        } else {
                            blurTable();
                            result = `"Username Not Exist"`;
                        }
                        return result;
                    }
                }
            },
            balanceType: {
                required: true
            },
            coin_type: {
                required: true
            },
            comment: {
                pattern: /^[a-zA-Z0-9-_'.$1\n& ]+$/
            }
        }
    });
    $('#singleAdjustSubmit').on('click', (e) => {
        if ($("#userSearchForm").valid()) {
            $('#username1').text($("#username").val());
            $("#coin_type1").text($("#coin_type").find(`option[value="${$("#coin_type").val()}"]`).text());
            $("#amount1").text($("#amount").val());
            $("#adjust_coin").val($("#amount").val());
            $("#coin_adjust_type").val($('#adjust_type').val());
            $("#adjust_type1").text($('#adjust_type').find(`option[value="${$("#adjust_type").val()}"]`).text());
            $("#adjust_coin_reason").val($("#reason").val());
            $("#adjust_coin_comment").val($("#comment").val());
            $("#adjust_coin_type").val($("#coin_type").val());
            $("#reason1").text($("#reason").find(`option[value="${$("#reason").val()}"]`).text());
            $("#comment1").text($("#comment").val());
            $("#adjust_balance_type").val($("#balanceType").val());
            if ($("#technical_loss_reg").is(':checked')) { $("#technical_loss_register").val('Yes') } else { $("#technical_loss_register").val('No') };
            $('#aadharcardpopup').modal('show');
        }
    })
});