function changeStatus(QUESTION_ID, status_id, contest_id) {
    var status = (status_id == 1) ? 0 : 1;
    $.ajax({
        url: `${window.pageData.baseUrl}/marketing/contest/updateQuesStatus`,
        type: "post",
        data: {
            QUESTION_ID: QUESTION_ID,
            status: status,
            CONTEST_ID: contest_id
        },
        success: function(response) {
            if (response.status == 200) {
                $.NotificationApp.send("Success", "Status Change Successfully", 'top-right', '#5ba035', 'success');
            } else {
                $.NotificationApp.send("error", "Status updation failed", 'top-right', '#FF9494', 'error');
            }
        }
    })
}

function checkContest(question_id) {
    $('#managecontestdata, #managecontestdata div').show().css({ 'z-index': 99 });
    $.ajax({
        url: `${window.pageData.baseUrl}/marketing/contest/getQuestionModelData`,
        type: "post",
        data: {
            QUESTION_ID: question_id
        },
        success: function(response) {
            $('#managecontestdata, #managecontestdata div').hide().css({ 'z-index': -1 });
            if (response.status == 200) {
                $('#contest_title_view').html(response.data.CONTEST_TITLE);
                $('#contest_name_view').html(response.data.CONTEST_NAME);
                $('#question').html(response.data.QUESTION);
                $('#option_a').html(response.data.OPTION_A);
                $('#option_b').html(response.data.OPTION_B);
                $('#option_c').html(response.data.OPTION_C);
                $('#option_d').html(response.data.OPTION_D);
                $('#ques_status').html(response.data.QUESTION_STATUS == 1 ? 'Active' : 'Inactive');
                $('#added_by').html(response.data.CREATED_BY);
                $('#created_date').html(response.data.CREATED_ON);
            }
        }
    })
}

function editQuestion(question_id) {
    $('#managecontestdata, #managecontestdata div').show().css({ 'z-index': 99 });
    $.ajax({
        url: `${window.pageData.baseUrl}/marketing/contest/getQuestionModelData`,
        type: "post",
        data: {
            QUESTION_ID: question_id
        },
        success: function(response) {
            $('#managecontestdata, #managecontestdata div').hide().css({ 'z-index': -1 });
            if (response.status == 200) {
                $('#edit_question_id').val(response.data.QUESTION_ID);
                $('#contest_title_edit').html(response.data.CONTEST_TITLE);
                $('#contest_name_edit').html(response.data.CONTEST_NAME);
                $('#question_edit').html(response.data.QUESTION);
                $('#option_a_edit').val(response.data.OPTION_A);
                $('#option_b_edit').val(response.data.OPTION_B);
                $('#option_c_edit').val(response.data.OPTION_C);
                $('#option_d_edit').val(response.data.OPTION_D);
            }
        }
    })
}

function updateQuestion() {
    $('#edit_question').on('submit', function(e) {
        if ($('#edit_question').valid()) {
            e.preventDefault();
            $("#update_question").prop("disabled", true);
            $.ajax({
                type: 'post',
                url: `${window.pageData.baseUrl}/marketing/contest/updatequestion`,
                data: $('#edit_question').serialize(),
                success: function(response) {
                    if (response.status == 200) {
                        $('#editfrompopup').modal('hide');
                        $("#search_question_form").submit();
                        $.NotificationApp.send("Success", "Update Successfully", 'top-right', '#5ba035', 'success');
                    } else {
                        $("#update_question").prop("disabled", false);
                        $.NotificationApp.send("error", "Updation failed", 'top-right', '#FF9494', 'error');
                    }
                }
            });
        }
    });

}

$('#edit_question').validate({
    rules: {
        question_edit: 'required',
        option_a_edit: 'required',
        option_b_edit: 'required',
        option_c_edit: 'required',
        option_d_edit: 'required'
    }

});

function contesttitle() {
    document.getElementById('titleloader').style.display = "block";
    var contestName = document.getElementById("contest_name").value;
    $.ajax({
        url: `${window.pageData.baseUrl}/marketing/contest/getContestTitle`,
        type: "post",
        data: {
            CONTEST_ID: contestName
        },
        success: function(response) {
            document.getElementById('titleloader').style.display = "none";
            if (response.status == 200) {
                if (response.data) {
                    $('#contest_title').val(response.data.CONTEST_TITLE);
                } else {
                    $('#contest_title').val('');
                }

            }
        }
    })
}

$('#create_questions').validate({
    rules: {
        contest_name: 'required',
        contest_title: 'required',
        "ques1[QUESTION]": 'required',
        "ques1[OPTION_A]": 'required',
        "ques1[OPTION_B]": 'required',
        "ques1[OPTION_C]": 'required',
        "ques1[OPTION_D]": 'required',
        "ques2[QUESTION]": 'required',
        "ques2[OPTION_A]": 'required',
        "ques2[OPTION_B]": 'required',
        "ques2[OPTION_C]": 'required',
        "ques2[OPTION_D]": 'required',
        "ques3[QUESTION]": 'required',
        "ques3[OPTION_A]": 'required',
        "ques3[OPTION_B]": 'required',
        "ques3[OPTION_C]": 'required',
        "ques3[OPTION_D]": 'required'
    }
});

$(document).ready(function() {
    $("#create_questions").submit(function(e) {
        if ($('#create_questions').valid()) {
            $("#submit_create_questions").prop("disabled", true);
            return true;
        }
    });
});