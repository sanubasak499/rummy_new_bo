//Poker Break Start
$('#updatePokerBreak').validate({
    rules: {
        model_status: "required",
        model_duration: {
            required: function() {
                return $("#model_status").val() != ""
            }
        },
        date_from_model: {
            required: function() {
                return $("#model_duration").val() == "custom"
            }
        },
        date_to_model: {
            required: function() {
                return $("#model_duration").val() == "custom"
            }
        }
    }

});


$("#updatePokerBreak").submit(function(e) {
    e.preventDefault();
    if ($("#updatePokerBreak").valid()) {
        $('#btnSubmit').attr('disabled', 'disabled');
        $.ajax({
            url: `${window.pageData.baseUrl}/responsiblegaming/pokerbreak/updatePokerBreak`,
            type: "post",
            data: $('#updatePokerBreak').serialize(),
            success: function(response) {
                $('#btnSubmit').removeAttr('disabled');
                if (response.status == 200) {
                    $.NotificationApp.success(`${response.message}`, `${response.title}`);
                    $('#setuserflagpopup').modal('hide');
                    $("#submit_form_search").trigger("click");
                } else {
                    $.NotificationApp.error(`${response.message}`, `${response.title}`);
                }
            },
            error: function(data) {
                $('#btnSubmit').removeAttr('disabled');
                $.each(data.responseJSON.errors, function(key, value) {
                    $(`#${key}-error`).remove();
                    $(`#${key}`).addClass('error').removeClass('valid');
                    $(`#${key}`).after(`<label id="${key}-error" class="error" for="${key}" style="">${value}</label>`);
                });
            }
        });
    }
});

function showTextDate(selecteDateValue) {
    var currentDate = new Date();
    if (selecteDateValue == '1-y') {
        var year = 1;
    } else if (selecteDateValue == '1-m') {
        var month = 1;
    } else if (selecteDateValue == '3-m') {
        var month = 3;
    } else if (selecteDateValue == '6-m') {
        var month = 6;
    } else if (selecteDateValue == '1-w') {
        var weak = 1;
    } else if (selecteDateValue == '2-w') {
        var weak = 2;
    } else if (selecteDateValue == 'custom') {
        var custom = '';
    } else {
        var blank = '';
    }
    if (year || month || weak) {
        $(".pbsvaluecustom").css("display", "none");
        var dyanamicDate = new Date(currentDate.getFullYear() + (year ? year : ''), currentDate.getMonth() + (month ? month : ''), currentDate.getDate() + (weak ? (7 * weak) : ''));
        $(".pbsvalue1-w").css("display", "block").text(moment(dyanamicDate).format("DD MMM YYYY"));
        $('#model_duration_date').val(moment(dyanamicDate).format("DD MMM YYYY"));
    } else if (custom == '') {
        $(".pbsvalue1-w").css("display", "none");
        $(".pbsvaluecustom").css("display", "block");
    } else {
        $(".pbsvalue1-w").css("display", "none");
        $(".pbsvaluecustom").css("display", "none");
    }
}

function pokerbreakModel(pokerBreakId) {
    $('#setuserflagpopup').modal('show');
    $('#getWayloader').show();
    $("#model_status option").removeAttr("selected");
    $("#model_duration option").removeAttr("selected");
    $.ajax({
        url: `${window.pageData.baseUrl}/responsiblegaming/pokerbreak/show`,
        type: 'post',
        data: { pokerbreakid: pokerBreakId },
        success: function(response) {
            $('#getWayloader').hide();
            if (response.status == 200) {
                $("#model_status option[value=" + response.data.BREAK_ACTIVE + "]").attr("selected", "selected");
                var fromDate = moment(response.data.BREAK_FROM_DATE).format('DD-MMM-YYYY H:m:s');
                $("#date_from_model").val(fromDate != 'Invalid date' ? fromDate : '');
                var toDate = moment(response.data.BREAK_TO_DATE).format('DD-MMM-YYYY H:m:s');
                $("#date_to_model").val(toDate != 'Invalid date' ? toDate : '');
                $("#model_userid").val(response.data.USER_ID);
                showHideCommon(response.data.BREAK_ACTIVE, response.data.BREAK_FROM_DATE, response.data.BREAK_TO_DATE);
            }

        }
    });
}

function showHideCommon(statusValue, datefrom, dateto) {
    if (statusValue == 1) {
        $(".pbsvalue1").css("display", "block");
        if (datefrom != '' && dateto != '') {
            dateDiffrence(datefrom, dateto);
            $(".pbsvaluecustom").css("display", "block");
        } else {
            $(".pbsvaluecustom").css("display", "none");
        }
    } else {
        $(".pbsvalue1").css("display", "none");
        $(".pbsvaluecustom").css("display", "none");
    }
}

function dateDiffrence(datefrom, dateto) {
    var diffDate = (new Date(new Date(dateto) - new Date(datefrom)) / 1000 / 60 / 60 / 24 | 0);
    if (diffDate == '7') {
        selectDateDiff = '1-w';
    } else if (diffDate == 14) {
        selectDateDiff = '2-w';
    } else if (diffDate == 30) {
        selectDateDiff = '1-m';
    } else if (diffDate == 90) {
        selectDateDiff = '3-m';
    } else if (diffDate == 180) {
        selectDateDiff = '6-m';
    } else if (diffDate == 360) {
        selectDateDiff = '1-y';
    } else {
        selectDateDiff = 'custom';
    }
    $("#model_duration option[value=" + selectDateDiff + "]").attr("selected", "selected");
}
//Poker Break end
//cash table start
$('#cashtablelimitmodelform').validate({
    rules: {
        model_status: "required",
        cash_table_limit: {
            required: function() {
                return $("#model_status").val() != ""
            }
        }
    }

});

function cashTableModelModel(cashtableid, userid) {
    $('#cashtablelimitmodel').modal('show');
    $('#getWayloader').show();
    $("#model_status option").removeAttr("selected");
    $("#cash_table_limit option").removeAttr("selected");
    $.ajax({
        url: `${window.pageData.baseUrl}/responsiblegaming/showcashtable`,
        type: 'post',
        data: { cashtableid: cashtableid, USER_ID: userid },
        success: function(response) {
            $('#getWayloader').hide();
            if (response.status == 200) {
                $("#model_status option[value=" + response.data.CASH_ACTIVE + "]").attr("selected", "selected");
                $("#cash_table_limit option[value='" + response.data.CASH_MAX_BIG_BLIND + "']").attr("selected", "selected");
                $("#model_userid").val(response.data.USER_ID);
                $("#old_cash_limit").val(response.data.CASH_MAX_BIG_BLIND);
                $(".pbsvalue1").css('display', (response.data.CASH_ACTIVE == '1') ? 'block' : 'none');
            }

        }
    });
}

$("#cashtablelimitmodelform").submit(function(e) {
    e.preventDefault();
    if ($("#cashtablelimitmodelform").valid()) {
        $('#btnSubmit').attr('disabled', 'disabled');
        $.ajax({
            url: `${window.pageData.baseUrl}/responsiblegaming/updatecashlimit`,
            type: "post",
            data: $('#cashtablelimitmodelform').serialize(),
            success: function(response) {
                $('#btnSubmit').removeAttr('disabled');
                if (response.status == 200) {
                    $.NotificationApp.success(`${response.message}`, `${response.title}`);
                    $('#cashtablelimitmodel').modal('hide');
                    $("#cash_limit_submit_form_search").trigger("click");
                } else {
                    $.NotificationApp.error(`${response.message}`, `${response.title}`);
                }
            },
            error: function(data) {
                $('#btnSubmit').removeAttr('disabled');
                $.each(data.responseJSON.errors, function(key, value) {
                    $(`#${key}-error`).remove();
                    $(`#${key}`).addClass('error').removeClass('valid');
                    $(`#${key}`).after(`<label id="${key}-error" class="error" for="${key}" style="">${value}</label>`);
                });
            }
        });
    }
});
//cash table end
//OFC Limit start
$('#ofctablelimitmodelform').validate({
    rules: {
        model_status: "required",
        ofc_table_limit: {
            required: function() {
                return $("#model_status").val() != ""
            }
        }
    }
});

function ofcTableModel(ofctableid, userid) {
    $('#ofctablelimitmodel').modal('show');
    $('#getWayloader').show();
    $("#model_status option").removeAttr("selected");
    $("#ofc_table_limit option").removeAttr("selected");
    $.ajax({
        url: `${window.pageData.baseUrl}/responsiblegaming/showofctable`,
        type: 'post',
        data: { ofctableid: ofctableid, USER_ID: userid },
        success: function(response) {
            $('#getWayloader').hide();
            if (response.status == 200) {
                $("#model_status option[value=" + response.data.OFC_ACTIVE + "]").attr("selected", "selected");
                $("#ofc_table_limit option[value='" + response.data.OFC_MAX_POINT + "']").attr("selected", "selected");
                $("#model_userid").val(response.data.USER_ID);
                $("#old_ofc_limit").val(response.data.OFC_MAX_POINT);
                $(".pbsvalue1").css('display', (response.data.OFC_ACTIVE == '1') ? 'block' : 'none');
            }

        }
    });
}

$("#ofctablelimitmodelform").submit(function(e) {
    e.preventDefault();
    if ($("#ofctablelimitmodelform").valid()) {
        $('#btnSubmit').attr('disabled', 'disabled');
        $.ajax({
            url: `${window.pageData.baseUrl}/responsiblegaming/updateofclimit`,
            type: "post",
            data: $('#ofctablelimitmodelform').serialize(),
            success: function(response) {
                $('#btnSubmit').removeAttr('disabled');
                if (response.status == 200) {
                    $.NotificationApp.success(`${response.message}`, `${response.title}`);
                    $('#ofctablelimitmodel').modal('hide');
                    $("#ofc_limit_submit_form_search").trigger("click");
                } else {
                    $.NotificationApp.error(`${response.message}`, `${response.title}`);
                }
            },
            error: function(data) {
                $('#btnSubmit').removeAttr('disabled');
                $.each(data.responseJSON.errors, function(key, value) {
                    $(`#${key}-error`).remove();
                    $(`#${key}`).addClass('error').removeClass('valid');
                    $(`#${key}`).after(`<label id="${key}-error" class="error" for="${key}" style="">${value}</label>`);
                });
            }
        });
    }
});
//OFC limit end
//Start all request

$('#requestmodelform').validate({
    rules: {
        model_status: "required",
        model_reasion: {
            required: function() {
                return $("#model_status").val() != ""
            }
        }
    }
});

function showrequestModel(userid, requestid, oldstatus) {
    $('#requestModelPopup').modal('show');
    $('#model_userid').val(userid);
    $('#request_id').val(requestid);
    $('#old_status').val(oldstatus);
}

$("#requestmodelform").submit(function(e) {
    e.preventDefault();
    if ($("#requestmodelform").valid()) {
        $('#btnSubmit').attr('disabled', 'disabled');
        $.ajax({
            url: `${window.pageData.baseUrl}/responsiblegaming/updaterequest`,
            type: "post",
            data: $('#requestmodelform').serialize(),
            success: function(response) {
                $('#btnSubmit').removeAttr('disabled');
                if (response.status == 200) {
                    $.NotificationApp.success(`${response.message}`, `${response.title}`);
                    $('#requestModelPopup').modal('hide');
                    $("#request_submit_form_search").trigger("click");
                } else {
                    $.NotificationApp.error(`${response.message}`, `${response.title}`);
                }
            },
            error: function(data) {
                $('#btnSubmit').removeAttr('disabled');
                $.each(data.responseJSON.errors, function(key, value) {
                    $(`#${key}-error`).remove();
                    $(`#${key}`).addClass('error').removeClass('valid');
                    $(`#${key}`).after(`<label id="${key}-error" class="error" for="${key}" style="">${value}</label>`);
                });
            }
        });
    }
});
//end all request
//Start Add user
$('#adduserForm').validate({
    rules: {
        username: "required",
    }
});
//End Add User

// add js for deposit limit

function changeDepositDaily(dailyoldvalue, weeklyoldvalue, monthlylyoldvalue) {
    var dailyLimit = document.getElementById('dep_amount_per_day').value;
    var weekly = (dailyLimit * 5);
    var montlyvalue = (weekly * 4);
    if (dailyLimit >= dailyoldvalue) {
        if (weekly >= weeklyoldvalue) {
            document.getElementById('deposit_limit_per_week').value = weekly;
            if (montlyvalue >= monthlylyoldvalue) {
                document.getElementById('deposit_limit_per_month').value = montlyvalue;
            }
        } else {
            document.getElementById('deposit_limit_per_week').value = weeklyoldvalue;
            if (montlyvalue >= monthlylyoldvalue) {
                document.getElementById('deposit_limit_per_month').value = montlyvalue;
            } else {
                document.getElementById('deposit_limit_per_month').value = monthlylyoldvalue;
            }
        }
    }

    if (dailyLimit <= dailyoldvalue) {
        if (weekly <= weeklyoldvalue) {
            document.getElementById('deposit_limit_per_week').value = weekly;
            if (montlyvalue <= monthlylyoldvalue) {
                document.getElementById('deposit_limit_per_month').value = montlyvalue;
            }
        } else {
            document.getElementById('deposit_limit_per_week').value = weeklyoldvalue;
            if (montlyvalue <= monthlylyoldvalue) {
                document.getElementById('deposit_limit_per_month').value = montlyvalue;
            } else {
                document.getElementById('deposit_limit_per_month').value = monthlylyoldvalue;
            }
        }
    }
}

function changeDepositCountDaily(dailyoldcount, weeklyoldcount, montluoldcount) {
    var dailyTxn = document.getElementById('no_of_transaction_per_day').value;
    var weeklyTxn = (dailyTxn * 5);
    var montlyTxn = (weeklyTxn * 4);
    if (dailyTxn >= dailyoldcount) {
        if (weeklyTxn >= weeklyoldcount) {
            document.getElementById('no_of_transaction_per_week').value = weeklyTxn;
            if (montlyTxn >= montluoldcount) {
                document.getElementById('no_of_transaction_per_month').value = montlyTxn;
            }
        } else {
            document.getElementById('no_of_transaction_per_week').value = weeklyoldcount;
            if (montlyTxn >= montluoldcount) {
                document.getElementById('no_of_transaction_per_month').value = montlyTxn;
            } else {
                document.getElementById('no_of_transaction_per_month').value = montluoldcount;
            }
        }
    }

    if (dailyTxn <= dailyoldcount) {
        if (weeklyTxn <= weeklyoldcount) {
            document.getElementById('no_of_transaction_per_week').value = weeklyTxn;
            if (montlyTxn <= montluoldcount) {
                document.getElementById('no_of_transaction_per_month').value = montlyTxn;
            }
        } else {
            document.getElementById('no_of_transaction_per_week').value = weeklyoldcount;
            if (montlyTxn <= montluoldcount) {
                document.getElementById('no_of_transaction_per_month').value = montlyTxn;
            } else {
                document.getElementById('no_of_transaction_per_month').value = montluoldcount;
            }
        }
    }
}


function changeDepositWeekly(weeklyoldvalue, monthlyoldvalue) {
    var weeklyLimit = document.getElementById('deposit_limit_per_week').value;

    var montly = (weeklyLimit * 4);
    if (weeklyLimit >= weeklyoldvalue) {
        if (montly >= monthlyoldvalue) {
            document.getElementById('deposit_limit_per_month').value = montly;
        } else {
            document.getElementById('deposit_limit_per_month').value = monthlyoldvalue;
        }
    }

    if (weeklyLimit <= weeklyoldvalue) {
        if (montly <= monthlyoldvalue) {
            document.getElementById('deposit_limit_per_month').value = montly;
        } else {
            document.getElementById('deposit_limit_per_month').value = monthlyoldvalue;
        }
    }
}

function changeDepositCountWeekly(weeklyoldcount, monthlyoldcount) {
    var weeklyTxn = document.getElementById('no_of_transaction_per_week').value;
    var montlytxn = (weeklyTxn * 4);
    if (weeklyTxn >= weeklyoldcount) {
        if (montlytxn >= monthlyoldcount) {
            document.getElementById('no_of_transaction_per_month').value = montlytxn;
        } else {
            document.getElementById('no_of_transaction_per_month').value = monthlyoldcount;
        }
    }

    if (weeklyTxn <= weeklyoldcount) {
        if (montlytxn <= monthlyoldcount) {
            document.getElementById('no_of_transaction_per_month').value = montlytxn;
        } else {
            document.getElementById('no_of_transaction_per_month').value = monthlyoldcount;
        }
    }
}
//end deposit limit

//deposit limit form validation
//Poker Break Start
$('#updateDepositLimit').validate({
    rules: {
        deposit_amount: {
            required: true,
            number: true,
        },
        dep_amount_per_day: {
            required: true,
            number: true,
        },
        deposit_limit_per_week: {
            required: true,
            number: true,
        },
        deposit_limit_per_month: {
            required: true,
            number: true,
        },
        no_of_transaction_per_day: {
            required: true,
            number: true,
        },
        no_of_transaction_per_week: {
            required: true,
            number: true,
        },
        no_of_transaction_per_month: {
            required: true,
            number: true,
        },
    }

});