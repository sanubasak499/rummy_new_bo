<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoAdminUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bo_admin_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('role_id')->unsigned();
            $table->string('firstname', 64)->nullable();
            $table->string('lastname',64)->nullable();
            $table->string('username',64)->unique();
            $table->string('email', 64)->unique();
            $table->string('password');
            $table->string('transcation_password')->nullable();
            $table->bigInteger('fk_partner_id')->unsigned()->nullable();
            $table->string('mobile', 12)->nullable()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->tinyInteger('account_status')->default(1)->nullable();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->rememberToken();
            $table->timestamps();
            
            $table->foreign('role_id')->references('id')->on('bo_admin_roles')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('created_by')->references('id')->on('bo_admin_users')->onDelete('cascade')->onUpdate('cascade');
            
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bo_admin_users');
    }
}
