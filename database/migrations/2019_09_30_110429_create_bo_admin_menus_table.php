<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoAdminMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bo_admin_menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parent_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->string('display_name')->nullable();
            $table->text('description')->nullable();
            $table->string('target')->default("_self")->nullable();
            $table->string('icon_class')->nullable();
            $table->string('color')->nullable();
            $table->string('url')->nullable();
            $table->string('route')->nullable();
            $table->string('model')->nullable();
            $table->string('policy')->nullable();
            $table->text('parameters')->nullable();
            $table->integer('menu_order')->nullable();
            $table->tinyInteger('absolute_url')->default(0)->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('bo_admin_menus')->onDelete('cascade')->onUpdate('cascade');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bo_admin_menus');
    }
}
