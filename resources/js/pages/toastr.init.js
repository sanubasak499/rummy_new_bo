/*
Template Name: Ubold - Responsive Bootstrap 4 Admin Dashboard
Author: CoderThemes
Version: 3.0.0
Website: https://coderthemes.com/
Contact: support@coderthemes.com
File: Toastr init js
*/

! function($) {
    'use strict';

    var NotificationApp = function() {};


    /**
     * Send Notification
     * @param {*} heading heading text
     * @param {*} body body text
     * @param {*} position position e.g top-right, top-left, bottom-left, etc
     * @param {*} loaderBgColor loader background color
     * @param {*} icon icon which needs to be displayed
     * @param {*} hideAfter automatically hide after seconds
     * @param {*} stack 
     */
    NotificationApp.prototype.send = function(heading, body, position, loaderBgColor, icon, hideAfter, stack, showHideTransition) {
            // default      
            if (!hideAfter)
                hideAfter = 3000;
            if (!stack)
                stack = 1;

            var options = {
                heading: heading,
                text: body,
                position: position,
                loaderBg: loaderBgColor,
                icon: icon,
                hideAfter: hideAfter,
                stack: stack
            };

            if (showHideTransition)
                options.showHideTransition = showHideTransition;
            $.toast().reset('all');
            $.toast(options);
        },
        /**
         * Send Notification
         * @param {*} message body text
         * @param {*} heading heading text
         */
        NotificationApp.prototype.success = function(message, heading = "Success") {
            $.NotificationApp.send(heading, message, 'top-right', '#5ba035', 'success');
        },
        /**
         * Send Notification
         * @param {*} message body text
         * @param {*} heading heading text
         */
        NotificationApp.prototype.error = function(message, heading = "Error") {
            $.NotificationApp.send(heading, message, 'top-right', '#bf441d', 'error');
        },
        /**
         * Send Notification
         * @param {*} message body text
         * @param {*} heading heading text
         */
        NotificationApp.prototype.info = function(message, heading = "Info") {
            $.NotificationApp.send(heading, message, 'top-right', '#3b98b5', 'info');
        },
        /**
         * Send Notification
         * @param {*} message body text
         * @param {*} heading heading text
         */
        NotificationApp.prototype.warning = function(message, heading = "Warning") {
            $.NotificationApp.send(heading, message, 'top-right', '#da8609', 'warning');
        },
        $.NotificationApp = new NotificationApp, $.NotificationApp.Constructor = NotificationApp


}(window.jQuery),
//initializing main application module
function($) {
    "use strict";
}(window.jQuery);