/*
Template Name: Ubold - Responsive Bootstrap 4 Admin Dashboard
Author: CoderThemes
Version: 3.0.0
Website: https://coderthemes.com/
Contact: support@coderthemes.com
File: Nestable init js
*/

! function($) {
    "use strict";

    var Nestable = function() {};

    Nestable.prototype.updateOutput = function(e, firstCall = null) {
            var list = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                output.val(btoa(window.JSON.stringify(list.nestable('serialize')))); //, null, 2));
                // if (!firstCall) {
                //     $.Nestable.ajaxOrder(btoa(window.JSON.stringify(list.nestable('serialize'))));
                // }
            } else {
                output.val('JSON browser support required for this demo.');
            }
        },
        //init
        Nestable.prototype.init = function() {

            $('#nestable_list_menu').on('click', function(e) {
                var target = $(e.target),
                    action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });

            window.testNest = $('#menuOrderNestable').nestable({
                group: 1,
                maxDepth: 7,
                collapseAll: true,
            }).on('change', this.updateOutput).nestable('collapseAll');
            this.updateOutput($('#menuOrderNestable').data('output', $('#menuOrder')), 'firstCall');

            window.testNest = $('#moduleNestableOther').nestable({
                group: 1,
                maxDepth: 7,
                collapseAll: true,
            }).nestable('collapseAll');

        },
        Nestable.prototype.ajaxOrder = function($order) {
            window.pageData.orderRequest = window.pageData.orderRequest || [];

            var xhr = $.ajax({
                    method: "POST",
                    url: `${window.pageData.baseUrl}/settings/menu/order`,
                    data: {
                        order: $order,
                        '_token': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                .done((response) => {
                    if (response.status == 200) {
                        $.NotificationApp.send("Success", response.message, 'top-right', '#5ba035', 'success');
                    } else {
                        console.log(response);
                    }
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    // alert("Unable to save new list order: " + errorThrown);
                });
            window.pageData.orderRequest.push(xhr);

            for (let index = 0; index < (window.pageData.orderRequest.length - 1); index++) {
                window.pageData.orderRequest[index].abort();
            }
        },
        //init
        $.Nestable = new Nestable,
        $.Nestable.Constructor = Nestable;
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.Nestable.init()
}(window.jQuery);