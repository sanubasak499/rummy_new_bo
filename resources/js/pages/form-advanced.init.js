/**
 * Author: Nitin Sharma
 */

! function($) {
    "use strict";

    var FormAdvanced = function() {};

    //initializing tooltip
    FormAdvanced.prototype.initSelect2 = function() {
            // Select2
            $('[data-toggle="select2"]').select2();
        },

        //initializing popover
        //Max Length
        FormAdvanced.prototype.initMaxLength = function() {
            //Bootstrap-MaxLength
            $('input#defaultconfig, .maxlengthdefaultconfig').maxlength({
                warningClass: "badge badge-success",
                limitReachedClass: "badge badge-danger"
            });

            $('input#thresholdconfig, .thresholdconfig').maxlength({
                threshold: 20,
                warningClass: "badge badge-success",
                limitReachedClass: "badge badge-danger"
            });

            $('input#alloptions, .maxlengthalloptions').maxlength({
                alwaysShow: true,
                separator: ' out of ',
                preText: 'You typed ',
                postText: ' chars available.',
                validate: true,
                warningClass: "badge badge-success",
                limitReachedClass: "badge badge-danger"
            });

            $('textarea#textarea, .maxlengtherror').maxlength({
                alwaysShow: true,
                warningClass: "badge badge-success",
                limitReachedClass: "badge badge-danger"
            });

            $('input#placement, .maxlengthplacement').maxlength({
                alwaysShow: true,
                placement: 'top-left',
                warningClass: "badge badge-success",
                limitReachedClass: "badge badge-danger"
            });
        },

        //initializing Custom Select
        FormAdvanced.prototype.initCustomSelect = function() {
            $('[data-plugin="customselect"]').each(function(idx, obj) {
                if (!$(this).data('niceSelectPlugin')) {
                    let niceSelect = $(this).niceSelect();
                    $(this).data('niceSelectPlugin', niceSelect);
                }
            });
        },

        //initializing Slimscroll
        FormAdvanced.prototype.initSwitchery = function() {
            $('[data-plugin="switchery"]').each(function(idx, obj) {
                if (!$(this).data('switcheryPlugin')) {
                    let switchery = new Switchery($(this)[0], $(this).data());
                    $(this).data('switcheryPlugin', switchery);
                }
            });
        },

        //initializing form validation
        FormAdvanced.prototype.initMultiSelect = function() {
            $('[data-plugin="multiselect"]').each(function(idx, obj) {
                if (!$(this).data('multiselect')) {
                    $(this).multiSelect($(this).data());
                }
            });
        },

        // touchspin
        FormAdvanced.prototype.initTouchspin = function() {
            var defaultOptions = {};

            // touchspin
            $('[data-toggle="touchspin"]').each(function(idx, obj) {
                var objOptions = $.extend({}, defaultOptions, $(obj).data());
                $(obj).TouchSpin(objOptions);
            });
        },


        //initilizing
        FormAdvanced.prototype.init = function() {
            var $this = this;
            $.prototype.select2 ? this.initSelect2() : null;
            $.prototype.maxlength ? this.initMaxLength() : null;
            $.prototype.niceSelect ? this.initCustomSelect() : null;
            this.initSwitchery();
            $.prototype.multiSelect ? this.initMultiSelect() : null;
            $.prototype.TouchSpin ? this.initTouchspin() : null;
        },

        $.FormAdvanced = new FormAdvanced, $.FormAdvanced.Constructor = FormAdvanced;

}(window.jQuery),
//initializing main application module
function($) {
    "use strict";
    $.FormAdvanced.init();
}(window.jQuery);