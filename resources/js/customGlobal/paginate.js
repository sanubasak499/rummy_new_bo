(function($, global) {
    'use strict';

    class CustomPaginate {
        constructor(data = {}) {

        }

        onClickPaginateLink() {
            $('.customPaginationRender').on('click', '.pagination .page-link', function(e) {
                e.preventDefault();
                let $this = $(e.currentTarget);
                let $customPaginationRender = $this.parents('.customPaginationRender');
                let $formId = $customPaginationRender.data('formId');
                let $url = $this.attr('href');
                $($formId).attr("action", $url).submit();
            });
        }
        init() {
            this.onClickPaginateLink();
        }
    }

    $.CustomPaginate = new CustomPaginate;
}(window.jQuery, window));

(function($) {
    "use strict";
    $.CustomPaginate.init();
}(window.jQuery));