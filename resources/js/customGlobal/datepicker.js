(function($, window) {
    'use strict';

    class CustomDatePicker {
        constructor(data = {}) {
            this.todayDate = new Date();
            this.year = this.todayDate.getFullYear();
            this.month = this.todayDate.getMonth();
            this.day = this.todayDate.getDay();
            this.date = this.todayDate.getDate();
            this.diff = this.todayDate.getDate() - this.day + (this.day == 0 ? -6 : 1); // adjust when day is sunday

            this.yesterday = new Date();
            this.yesterday.setDate(this.yesterday.getDate() - 1);

            this.todayStart = new Date(this.todayDate.setHours(0, 0, 0));
            // this.todayEnd = new Date(this.todayDate.setHours(23, 59, 59));
            this.todayEnd = new Date(this.todayDate.setHours(23, 59, 59));

            this.yesterDayStart = new Date(this.yesterday.setHours(0, 0, 0));
            this.yesterDayEnd = new Date(this.yesterday.setHours(23, 59, 59));

            this.firstDateThisWeek = new Date(new Date(new Date().setDate(this.diff)).setHours(0, 0, 0)); // first week date on behalf of monday
            this.lastDateThisWeek = new Date(new Date(new Date().setDate(this.diff + 6)).setHours(23, 59, 59));
            this.lastDateThisWeekByToday = new Date(new Date(new Date().setDate(this.diff + this.day - 1)).setHours(23, 59, 59));

            this.lastWeekDiff = this.todayDate.getDate() - this.day + (this.day == 0 ? -6 : 1) - 7; // adjust when day is sunday

            this.firstDateLastWeek = new Date(new Date(new Date().setDate(this.lastWeekDiff)).setHours(0, 0, 0)); // first week date on behalf of monday
            this.lastDateLastWeek = new Date(new Date(new Date().setDate(this.lastWeekDiff + 6)).setHours(23, 59, 59));

            this.firstDateThisMonth = new Date(this.year, this.month, 1, 0, 0);
            this.lastDateThisMonth = new Date(this.year, this.month + 1, 0, 23, 59, 59);
            this.lastDateThisMonthByToday = new Date(this.year, this.month, this.date, 23, 59, 59);

            this.firstDateLastMonth = new Date(this.year, this.month - 1, 1, 0, 0, 0);
            this.lastDateLastMonth = new Date(this.year, this.month, 0, 23, 59, 59);
        }

        initDatePicker() {

            $('.customDatePickerWrapper').each(function(i, e) {
                // console.log(e);
                var $customDatePickerWrapper = $(e);
                var $customDatePickerNotToFrom = $customDatePickerWrapper.find(`.customDatePicker:not(.from, .to)`);
                let flatpikcerInstance = $customDatePickerNotToFrom.flatpickr({
                    autoclose: true,
                    // maxDate: new Date(),
                    enableTime: true,
                    changeMonth: true,
                    changeYear: true,
                    defaultHour: 23,
                    defaultMinute: 59,
                    defaultSecond: 59,
                    minuteIncrement: 1,
                    time_24hr: true,
                    enableSeconds: true,
                    disableMobile: "true",
                });
                $customDatePickerNotToFrom.data('flatpickr', flatpikcerInstance);

                var $customDatePickerFrom = $customDatePickerWrapper.find(".customDatePicker.from");
                let flatpikcerInstanceFrom = $customDatePickerFrom.flatpickr({
                    autoclose: true,
                    dateFormat: 'd-M-Y H:i:S',
                    // maxDate: new Date(),
                    enableTime: true,
                    changeMonth: true,
                    changeYear: true,
                    defaultHour: 0,
                    defaultMinute: 0,
                    defaultSecond: 0,
                    minuteIncrement: 1,
                    time_24hr: true,
                    enableSeconds: true,
                    disableMobile: "true",
                    onClose: function(selectedDates, dateStr, instance) {
                        let minDate = new Date(dateStr).setHours("23", "59", "59");

                        // $customDatePickerWrapper.find(".customDatePicker.to").flatpickr("minDate", minDate).focus();
                        let customDatePickerFrom = $customDatePickerWrapper.find(".customDatePicker.to");
                        var flatpickrFrom = customDatePickerFrom.data('flatpickr');
                        flatpickrFrom.set("minDate", minDate);
                    },
                })
                $customDatePickerFrom.data('flatpickr', flatpikcerInstanceFrom);

                var $customDatePickerTo = $customDatePickerWrapper.find(".customDatePicker.to");
                let flatpikcerInstanceTo = $customDatePickerTo.flatpickr({
                    autoclose: true,
                    dateFormat: 'd-M-Y H:i:S',
                    // maxDate: new Date(),
                    enableTime: true,
                    changeMonth: true,
                    changeYear: true,
                    defaultHour: 23,
                    defaultMinute: 59,
                    minuteIncrement: 1,
                    time_24hr: true,
                    enableSeconds: true,
                    disableMobile: "true",
                    onClose: function(selectedDates, dateStr, instance) {
                        let maxDate = new Date(dateStr).setHours("00", "00", "00");
                        // $customDatePickerWrapper.find(".customDatePicker.from").flatpickr('maxDate', maxDate);
                        let customDatePickerTo = $customDatePickerWrapper.find(".customDatePicker.from");
                        var flatpickrTo = customDatePickerTo.data('flatpickr');
                        flatpickrTo.set("maxDate", maxDate);
                    },
                });
                $customDatePickerTo.data('flatpickr', flatpikcerInstanceTo);

                $customDatePickerWrapper.on('change', '.formatedDateRange', function(e) {
                    $.CustomDatePicker.formatedDateEventHandler(e);
                });
            });
        }
        formatedDateEventHandler(e) {
            const { todayDate } = this;
            var $this = $(e.currentTarget);
            var $val = $this.val();
            var sDate = "";
            var eDate = "";

            var $customDatePickerWrapper = $this.parents('.customDatePickerWrapper');

            let customDatePickerFrom = $customDatePickerWrapper.find(".customDatePicker.from");
            var flatpickrFrom = customDatePickerFrom.data('flatpickr');

            let customDatePickerTo = $customDatePickerWrapper.find(".customDatePicker.to");
            var flatpickrTo = customDatePickerTo.data('flatpickr');

            switch ($val) {
                case "1":
                    flatpickrFrom.setDate(this.todayStart);
                    flatpickrTo.setDate(this.todayEnd);
                    break;
                case "2":
                    flatpickrFrom.setDate(this.yesterDayStart);
                    flatpickrTo.setDate(this.yesterDayEnd);
                    break;
                case "3":
                    flatpickrFrom.setDate(this.firstDateThisWeek);
                    flatpickrTo.setDate(this.lastDateThisWeek);
                    break;
                case "4":
                    flatpickrFrom.setDate(this.firstDateLastWeek);
                    flatpickrTo.setDate(this.lastDateLastWeek);
                    break;
                case "5":
                    flatpickrFrom.setDate(this.firstDateThisMonth);
                    flatpickrTo.setDate(this.lastDateThisMonth);
                    break;
                case "6":
                    flatpickrFrom.setDate(this.firstDateLastMonth);
                    flatpickrTo.setDate(this.lastDateLastMonth);
                    break;
                default:
                    customDatePickerFrom.val("");
                    customDatePickerTo.val("");

            }
        }

        init() {
            this.initDatePicker()
        }
    }

    $.CustomDatePicker = new CustomDatePicker;
}(window.jQuery, window));

(function($) {
    "use strict";
    $.CustomDatePicker.init();
}(window.jQuery));