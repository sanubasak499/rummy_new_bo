(function($, window) {
    'use strict';

    class CustomDataTable {
        constructor(data = {}) {

        }
        initBasicDataTable() {
            $('#basic-datatable,.basic-datatable').DataTable({
                "order": [],
                "language": {
                    "paginate": {
                        "previous": "<i class='mdi mdi-chevron-left'>",
                        "next": "<i class='mdi mdi-chevron-right'>"
                    }
                },
                "drawCallback": function() {
                    $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                }
            });
        }

        /**
         * Common funcation used for the data table re-init
         * with the round pagination buttons
         * 
         * @param { any } $tableRef refrence of table element
         */
        reInit($tableRef) {
            $tableRef.DataTable({
                "order": [],
                "language": {
                    "paginate": {
                        "previous": "<i class='mdi mdi-chevron-left'>",
                        "next": "<i class='mdi mdi-chevron-right'>"
                    }
                },
                "drawCallback": function() {
                    $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                }
            });
        }

        /**
         * Common methond used for clean and destory table
         * 
         * @param { any } $tableRef refrence of table element
         */
        cleanDestroy($tableRef, $isClean = true) {
            if ($isClean) $tableRef.DataTable().clear();
            $tableRef.DataTable().destroy();
        }

        init() {
            this.initBasicDataTable();
        }
    }

    $.CustomDataTable = new CustomDataTable;
}(window.jQuery, window));

(function($) {
    "use strict";
    $.CustomDataTable.init();
}(window.jQuery));