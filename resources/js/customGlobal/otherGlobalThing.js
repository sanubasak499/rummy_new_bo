(function($, global) {
    'use strict';

    class OtherGlobalThing {
        constructor(data = {}) {

        }

        showHideByTargetClass() {
            $(document).on('change', `[data-target-class]`, function() {
                var inputValue = $(this).val();
                var targetClass = $(this).data('targetClass');
                $(`.boxhideshow[class*="${targetClass}"]`).hide();
                $(`.boxhideshow.${targetClass}${inputValue}`).show();
            });
            $(`[data-target-class]`).each((i, e) => {
                if ($(e).attr('type', 'radio')) {
                    $(e).prop('checked') ? $(e).trigger('change') : '';
                } else if ($(e).attr('type', 'checkbox')) {
                    $(e).prop('checked') ? $(e).trigger('change') : '';
                } else {
                    $(e).trigger('change');
                }
            });
        }

        ordinal_suffix_of(i) {
            var j = i % 10,
                k = i % 100;
            if (j == 1 && k != 11) {
                return i + "st";
            }
            if (j == 2 && k != 12) {
                return i + "nd";
            }
            if (j == 3 && k != 13) {
                return i + "rd";
            }
            return i + "th";
        }

        init() {
            this.showHideByTargetClass();
        }
    }

    $.OtherGlobalThing = new OtherGlobalThing;
}(window.jQuery, window));

(function($) {
    "use strict";
    $.OtherGlobalThing.init();
}(window.jQuery));