(function($, global) {
    'use strict';

    class CustomFilterForm {
        constructor(data = {}) {

        }

        ajaxSetupMethod() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

        jqueryValidationGlobal() {
            $.validator.addMethod("fieldTrim", function(value, element) {
                return $.trim(value);
            }, function(params, element) {
                return "This field is required.";
            });

            $.validator.addMethod("noSpace", function(value, element) {
                return value.indexOf(" ") < 0 && value != "";
            }, "No space please and don't leave it empty");

            $.validator.addMethod("strongPassword", function(value) {
                return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/.test(value);
            }, "Password should be like Abcd@1234");
        }

        onResetFilterForm() {
            $(document).on('click', `[data-toggle="reload"]`, function(e) {
                e.preventDefault();
                let $this = $(e.currentTarget);
                let $formId = $this.data('formId');
                $formId = $.trim($formId) != "" ? $formId : $this.parents('form');

                if ($formId.length > 0) {
                    $($formId).get(0).reset();
                    $($formId).find(`[default-prop-readonly]`).prop('readonly', true);
                    $($formId).find(`[default-prop-readonly-clear]`).prop('readonly', false);
                    $($formId).find(`input[type="text"]`).val("");
                    $($formId).find(`input[type="radio"][data-default-checked]`).prop('checked', true);
                    $($formId).find(`select option[data-default-selected]`).prop('selected', true);
                    $($formId).attr('action', $($formId).data('defaultUrl'));
                    $.each($($formId).find(`[data-default-reset]`), (i, e) => {
                        $(e).val($(e).data('defaultReset'));
                    });
                }
            });
        }

        conditionalField() {
            $(document).on('change', `[data-target-class]`, function() {
                var inputValue = $(this).val();
                var targetClass = $(this).data('targetClass');
                $(`.boxhideshow[class*="${targetClass}"]`).hide();
                $(`.boxhideshow.${targetClass}${inputValue}`).show();
            });
            $(`[data-target-class]`).each((i, e) => {
                if ($(e).attr('type', 'radio')) {
                    $(e).prop('checked') ? $(e).trigger('change') : '';
                } else {
                    $(e).trigger('change');
                }
            });
        }

        /**
         * @param {*} $url link to call ajax
         * @param {*} $data $data is passed as json object or formData 
         * @param { method, refObj } $optional optional argument default value is post
         * 
         * @returns { Promise } xhr return $.ajax promise object
         */
        ajaxCall($url, $data = {}, $optional = {}) {
            let $method = $optional.method || 'POST';
            let refObj = $optional.refObj || {};
            refObj.requestQueue = refObj.requestQueue || [];

            let xhr = $.ajax({
                url: $url,
                type: $method,
                data: $data,
                beforeSend: function(jqXHR, settings) {
                    for (var i = 0; i < (refObj.requestQueue.length); i++) {
                        refObj.requestQueue[i].abort();
                    }
                }
            }).fail((failCallback, errorStatus, errorMessage) => {
                if (errorStatus != "abort") {
                    $.isPlainObject(failCallback.responseJSON) ? console.error(failCallback.responseJSON, errorStatus, errorMessage) : console.error(failCallback.responseText, errorStatus, errorMessage)
                }
            }).always(function(jqXHR, textStatus, errorThrown) {
                refObj.requestQueue.splice(0, refObj.requestQueue.length - 1);
            });
            refObj.requestQueue.push(xhr);

            return xhr;
        }

        init() {
            this.ajaxSetupMethod();
            this.onResetFilterForm();
            this.conditionalField();
            $.validator ? this.jqueryValidationGlobal() : null;
        }
    }

    $.CustomFilterForm = new CustomFilterForm;
}(window.jQuery, window));

(function($) {
    "use strict";
    $.CustomFilterForm.init();
}(window.jQuery));