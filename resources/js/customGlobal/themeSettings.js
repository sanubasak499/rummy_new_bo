(function($, global) {
    'use strict';

    class ThemeSettings {
        constructor(data = {}) {
            window.themeData = window.themeData || {};
            window.themeData.themeMode = window.themeData.themeMode || 'dark';
        }
        initEvents() {
            var _ = this;
            $(document).on('click', `[data-change-theme-mode]`, e => _.themeMode(e));
        }
        showThemeModeIcon() {
            var themeData = null;
            if (themeData = localStorage.getItem('themeData')) {
                themeData = JSON.parse(themeData);
                themeMode = themeData.themeMode || 'dark';
                $(document).find(`[data-change-theme-mode]`).hide();
                if (themeMode == "dark") {
                    $(document).find(`[data-theme-mode="dark"]`).show()
                } else {
                    $(document).find(`[data-theme-mode="light"]`).show()
                }
            }
        }
        themeMode(e) {
            var _ = this;
            var $e = $(e.target);
            let themeMode = $e.data('themeMode');
            if (themeMode == 'dark') {
                document.body.classList.remove('left-side-menu-dark');
                $(document).find(`[data-change-theme-mode]`).hide();
                $(document).find(`[data-theme-mode="light"]`).show();
                window.themeData.themeMode = 'light';
            } else {
                document.body.classList.add('left-side-menu-dark');
                $(document).find(`[data-change-theme-mode]`).hide();
                $(document).find(`[data-theme-mode="dark"]`).show();
                window.themeData.themeMode = 'dark';
            }
            localStorage.setItem('themeData', JSON.stringify(window.themeData));
        }
        init() {
            this.initEvents();
            this.showThemeModeIcon();
        }
    }

    $.ThemeSettings = new ThemeSettings;
}(window.jQuery, window));

(function($) {
    "use strict";
    $.ThemeSettings.init();
}(window.jQuery));