$(document).ready(function() {

    var setRouteFields = (function routeType(e) {
        let $select = $(`#routeType`);
        $(`[data-route-type]`).hide();
        $(`[data-route-type="${$select.val()}"]`).show();
        return routeType;
    }());

    $(`#routeType`).on('change', function(e) {
        setRouteFields();
    });

    $('[data-toggle="select2"]').select2();
    $(document).on('click', '.menuEditViewButton', function(e) {
        var $form = $("#addEditMenuForm");
        var $e = $(e.target);
        var $menuItem = $e.closest('.menuItem');
        var $objData = $menuItem.find('[data-obj]').data('obj');
        var $data = JSON.parse(atob($objData));

        $form.find(`[name="display_name"]`).val($data.display_name);
        $form.find(`[name="description"]`).val($data.description);
        $form.find(`[name="target"]`).val($data.target);
        $form.find(`[name="icon_class"]`).val($data.icon_class);
        $form.find(`[name="url"]`).val($data.url);
        $form.find(`[name="route"]`).val($data.route);
        $form.find(`[name="parameters"]`).val($data.parameters);
        $form.find(`[name="status"]`).prop('checked', $data.status == 1 ? true : false);
        $form.find(`[name="absolute_url"]`).val($data.absolute_url);
        $form.find(`[name="parent_id"]`).val($data.parent_id).trigger('change');
        $form.attr('action', `${window.pageData.baseUrl}/settings/menu/store/${$data.id}`);
        setRouteFields();
    });

    $('#addEditMenuForm').validate({
        rules: {
            name: {
                required: true,
                fieldTrim: true
            },
            display_name: {
                required: true,
                fieldTrim: true
            },
            description: {
                required: true
            },
            target: {
                required: true,
                fieldTrim: true
            },
            // url: {
            //     required: {
            //         depends: function () { 
            //             return $('#addEditMenuForm [name="absolute_url"]').val() == 1;
            //         }
            //     }
            // },
            // route: {
            //     required: {
            //         depends: function () { 
            //             return $('#addEditMenuForm [name="absolute_url"]').val() == 0;
            //         }
            //     }
            // }
        },
        errorElement: "div",
        highlight: function(element, errorClass) {
            $(element).next().addClass('invalid-feedback');
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
            $(element).next().addClass('invalid-feedback');
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});