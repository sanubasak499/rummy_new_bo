@php 

$template = \PokerBaazi::getMailTemplate($mailData['tempId']);
// print_r($mailData); exit;
// print_r($template->description);
$userName = $mailData['username'];
$mailContent =$template->description ;
$siteURL = config('poker_config.cdn_url') . "/";
//$siteURL = "https://www.pokerbaazi.com/";
$headerImgURL = $siteURL."images/Home_logo.png";
$downArrowImgURL = $siteURL."images/dwnarw.png";
$bottomLineImgURL = $siteURL."images/email-bottom.png";
$addCashImgURL=$siteURL."images/add_cash.png";
$downloadImgURL=$siteURL."images/download_now.png";
$playNowImgURL=$siteURL."images/play_now.png";
$socialFBImgURL=$siteURL."images/facebook_icon_2.png";
$socialTWEETImgURL=$siteURL."images/twitter_icon_2.png";
$socialYOUImgURL=$siteURL."images/youtube_icon_2.png";
$unlocked	= $siteURL."images/mailer/unlocked.png";
$locked		= $siteURL."images/mailer/locked.png";

$subject = $mailData['subject'];


$mailContent = str_replace('_HEADER_IMG_URL_',$headerImgURL,$mailContent);
$mailContent = str_replace('_DOWN_ARROW_IMG_URL_',$downArrowImgURL,$mailContent);
$mailContent = str_replace('_BOTTOM_LINE_URL_',$bottomLineImgURL,$mailContent);
$mailContent = str_replace('_ADD_CASH_IMG_URL_',$addCashImgURL,$mailContent);
$mailContent = str_replace('_DOWN_IMG_URL_',$downloadImgURL,$mailContent);
$mailContent = str_replace('_PLAY_IMG_URL_',$playNowImgURL,$mailContent);
$mailContent = str_replace('_SOCIAL_FB_ICON_',$socialFBImgURL,$mailContent);
$mailContent = str_replace('_SOCIAL_TWEET_ICON_',$socialTWEETImgURL,$mailContent);
$mailContent = str_replace('_SOCIAL_YOU_ICON_',$socialYOUImgURL,$mailContent);
$mailContent = str_replace( "_USER_NAME_",$userName, $mailContent );
$mailContent = str_replace( "_USER_NAME_",$userName, $mailContent );

$mailContent = str_replace('_UNLOCKED_',$unlocked,$mailContent);
$mailContent = str_replace('_LOCKED_',$locked,$mailContent);

// to resolve undefined index
$mailData['GAME_TYPE'] = $mailData['GAME_TYPE'] ?? null;

//Modify By Kamlesh (For Responsible Gaming)
// for Self Exclusion
if(!empty($mailData['GAME_TYPE']) && $mailData['GAME_TYPE']=='POKER_BREAK'){
    if($mailData['MAIL_STATUS'] =='PB_START'){
        $mailContent = str_replace('_SELF_EXCLUSION_START_',$mailData['BREAK_FROM_DATE'],$mailContent);
        $mailContent = str_replace('_SELF_EXCLUSION_END_',$mailData['BREAK_TO_DATE'],$mailContent);
    }
}
//for ofc table limit
if(!empty($mailData['GAME_TYPE']) && $mailData['GAME_TYPE']=='OFC'){
    $mailContent = str_replace( "_OFC_INCREASE_",$mailData['OFC_MAX_POINT'], $mailContent );
    $mailContent = str_replace( "_OFC_DECREASE_",$mailData['OFC_MAX_POINT'], $mailContent );
}
//for cash table limit
if(!empty($mailData['GAME_TYPE']) && $mailData['GAME_TYPE']=='CASH'){
    $mailContent = str_replace('_CASH_LIMIT_DECREASE_',$mailData['CASH_MAX_BIG_BLIND'],$mailContent);
    $mailContent = str_replace('_CASH_LIMIT_INCREASE_',$mailData['CASH_MAX_BIG_BLIND'],$mailContent);
}
// for deposit limit Increase
if(!empty($mailData['GAME_TYPE']) && $mailData['GAME_TYPE']=='DEPOSIT_LIMIT'){
    $mailContent = str_replace('_RAI_D_L_TRANSACTION_',$mailData['dep_amount'],$mailContent);
    $mailContent = str_replace('_RAI_D_L_DAY_',$mailData['DEP_AMOUNT_PER_DAY'],$mailContent);
    $mailContent = str_replace('_RAI_D_L_WEEK_',$mailData['DEP_AMOUNT_PER_WEEK'],$mailContent);
    $mailContent = str_replace('_RAI_D_L_MONTH_',$mailData['DEP_AMOUNT_PER_MONTH'],$mailContent);
    $mailContent = str_replace('_RAI_T_L_DAY_',$mailData['transactions_per_day'],$mailContent);
    $mailContent = str_replace('_RAI_T_L_WEEK_',$mailData['transactions_per_week'],$mailContent);
    $mailContent = str_replace('_RAI_T_L_MONTH_',$mailData['transactions_per_month'],$mailContent);

    // for deposit limit Decrease
    $mailContent = str_replace('_RAD_D_L_TRANSACTION_',$mailData['dep_amount'],$mailContent);
    $mailContent = str_replace('_RAD_D_L_DAY_',$mailData['DEP_AMOUNT_PER_DAY'],$mailContent);
    $mailContent = str_replace('_RAD_D_L_WEEK_',$mailData['DEP_AMOUNT_PER_WEEK'],$mailContent);
    $mailContent = str_replace('_RAD_D_L_MONTH_',$mailData['DEP_AMOUNT_PER_MONTH'],$mailContent);
    $mailContent = str_replace('_RAD_T_L_DAY_',$mailData['transactions_per_day'],$mailContent);
    $mailContent = str_replace('_RAD_T_L_WEEK_',$mailData['transactions_per_week'],$mailContent);
    $mailContent = str_replace('_RAD_T_L_MONTH_',$mailData['transactions_per_month'],$mailContent);
}


$pokerbaaziLogoWhite	= $siteURL."images/mailer/pokerbaazi-logo-white.png";
$socialMediaInstagram	= $siteURL."images/mailer/social-media-instagram_gs.png";
$socialMediaFbGs		= $siteURL."images/mailer/social-media-facebook_gs.png";
$socialMediaTwitterGs	= $siteURL."images/mailer/social-media-twitter_gs.png";
$socialMediaYoutubGS	= $siteURL."images/mailer/social-media-youtube_gs.png";
$downloadAndroidURL = $siteURL."images/mailer/android.png";
$downloadiOSURL		= $siteURL."images/mailer/apple.png";

$requestAccepted	= $siteURL."images/mailer/request_accepted.png";
$requestReceived	= $siteURL."images/mailer/request_received.png";
$requestRejected	= $siteURL."images/mailer/request_rejected.png";
        
$mailContent = str_replace('_POKERBAAZI_LOGO_WHITE_',$pokerbaaziLogoWhite,$mailContent);
$mailContent = str_replace('_SOCIAL_MEDIA_INSTAGRAM_GS_',$socialMediaInstagram,$mailContent);
$mailContent = str_replace('_SOCIAL_MEDIA_FB_GS_',$socialMediaFbGs,$mailContent);
$mailContent = str_replace('_SOCIAL_MEDIA_TWITTER_GS_',$socialMediaTwitterGs,$mailContent);
$mailContent = str_replace('_SOCIAL_MEDIA_YOUTUB_GS_',$socialMediaYoutubGS,$mailContent);

$mailContent = str_replace('_DOWNLOAD_ANDROID_URL_',$downloadAndroidURL,$mailContent);
$mailContent = str_replace('_DOWNLOAD_IOS_URL_',$downloadiOSURL,$mailContent);

$mailContent = str_replace('_REQUEST_ACCEPTED_',$requestAccepted,$mailContent);
$mailContent = str_replace('_REQUEST_RECEIVED_',$requestReceived,$mailContent);
$mailContent = str_replace('_REQUEST_REJECTED_',$requestRejected,$mailContent);

//Modify By Kamlesh (For Responsible Gaming)
// for Self Exclusion
if($mailData['GAME_TYPE']=='POKER_BREAK'){
    if($mailData['MAIL_STATUS'] =='PB_START'){
        $mailContent = str_replace('_SELF_EXCLUSION_START_',$mailData['BREAK_FROM_DATE'],$mailContent);
        $mailContent = str_replace('_SELF_EXCLUSION_END_',$mailData['BREAK_TO_DATE'],$mailContent);
    }
}
//for ofc table limit
if($mailData['GAME_TYPE']=='OFC'){
    $mailContent = str_replace( "_OFC_INCREASE_",$mailData['OFC_MAX_POINT'], $mailContent );
    $mailContent = str_replace( "_OFC_DECREASE_",$mailData['OFC_MAX_POINT'], $mailContent );
}
//for cash table limit
if($mailData['GAME_TYPE']=='CASH'){
    $mailContent = str_replace('_CASH_LIMIT_DECREASE_',$mailData['CASH_MAX_BIG_BLIND'],$mailContent);
    $mailContent = str_replace('_CASH_LIMIT_INCREASE_',$mailData['CASH_MAX_BIG_BLIND'],$mailContent);
}
// for deposit limit Increase
if($mailData['GAME_TYPE']=='DEPOSIT_LIMIT'){
    $mailContent = str_replace('_RAI_D_L_TRANSACTION_',$mailData['dep_amount'],$mailContent);
    $mailContent = str_replace('_RAI_D_L_DAY_',$mailData['DEP_AMOUNT_PER_DAY'],$mailContent);
    $mailContent = str_replace('_RAI_D_L_WEEK_',$mailData['DEP_AMOUNT_PER_WEEK'],$mailContent);
    $mailContent = str_replace('_RAI_D_L_MONTH_',$mailData['DEP_AMOUNT_PER_MONTH'],$mailContent);
    $mailContent = str_replace('_RAI_T_L_DAY_',$mailData['transactions_per_day'],$mailContent);
    $mailContent = str_replace('_RAI_T_L_WEEK_',$mailData['transactions_per_week'],$mailContent);
    $mailContent = str_replace('_RAI_T_L_MONTH_',$mailData['transactions_per_month'],$mailContent);

    // for deposit limit Decrease
    $mailContent = str_replace('_RAD_D_L_TRANSACTION_',$mailData['dep_amount'],$mailContent);
    $mailContent = str_replace('_RAD_D_L_DAY_',$mailData['DEP_AMOUNT_PER_DAY'],$mailContent);
    $mailContent = str_replace('_RAD_D_L_WEEK_',$mailData['DEP_AMOUNT_PER_WEEK'],$mailContent);
    $mailContent = str_replace('_RAD_D_L_MONTH_',$mailData['DEP_AMOUNT_PER_MONTH'],$mailContent);
    $mailContent = str_replace('_RAD_T_L_DAY_',$mailData['transactions_per_day'],$mailContent);
    $mailContent = str_replace('_RAD_T_L_WEEK_',$mailData['transactions_per_week'],$mailContent);
    $mailContent = str_replace('_RAD_T_L_MONTH_',$mailData['transactions_per_month'],$mailContent);
}


$pokerbaaziLogoWhite	= $siteURL."images/mailer/pokerbaazi-logo-white.png";
$socialMediaInstagram	= $siteURL."images/mailer/social-media-instagram_gs.png";
$socialMediaFbGs		= $siteURL."images/mailer/social-media-facebook_gs.png";
$socialMediaTwitterGs	= $siteURL."images/mailer/social-media-twitter_gs.png";
$socialMediaYoutubGS	= $siteURL."images/mailer/social-media-youtube_gs.png";
$downloadAndroidURL = $siteURL."images/mailer/android.png";
$downloadiOSURL		= $siteURL."images/mailer/apple.png";

$requestAccepted	= $siteURL."images/mailer/request_accepted.png";
$requestReceived	= $siteURL."images/mailer/request_received.png";
$requestRejected	= $siteURL."images/mailer/request_rejected.png";
        
$mailContent = str_replace('_POKERBAAZI_LOGO_WHITE_',$pokerbaaziLogoWhite,$mailContent);
$mailContent = str_replace('_SOCIAL_MEDIA_INSTAGRAM_GS_',$socialMediaInstagram,$mailContent);
$mailContent = str_replace('_SOCIAL_MEDIA_FB_GS_',$socialMediaFbGs,$mailContent);
$mailContent = str_replace('_SOCIAL_MEDIA_TWITTER_GS_',$socialMediaTwitterGs,$mailContent);
$mailContent = str_replace('_SOCIAL_MEDIA_YOUTUB_GS_',$socialMediaYoutubGS,$mailContent);

$mailContent = str_replace('_DOWNLOAD_ANDROID_URL_',$downloadAndroidURL,$mailContent);
$mailContent = str_replace('_DOWNLOAD_IOS_URL_',$downloadiOSURL,$mailContent);

$mailContent = str_replace('_REQUEST_ACCEPTED_',$requestAccepted,$mailContent);
$mailContent = str_replace('_REQUEST_RECEIVED_',$requestReceived,$mailContent);
$mailContent = str_replace('_REQUEST_REJECTED_',$requestRejected,$mailContent);

//for tournament register player
if(!empty($mailData['tourName'])){
$mailContent = str_replace('_TOURNAMENTID_',$mailData['tourName'],$mailContent);
}
if(!empty($mailData['tourEntryFee'])){
$mailContent = str_replace('_ENTRYFEE_',$mailData['tourEntryFee'],$mailContent);
}
if(!empty($mailData['tourBuyIn'])){
$mailContent = str_replace('_BUYIN_',$mailData['tourBuyIn'],$mailContent);
}
if(!empty($mailData['tourStartTime'])){
$mailContent = str_replace('_TSTARTDTIME_',$mailData['tourStartTime'],$mailContent);
}
if(!empty($mailData['tourRegStartTime'])){
$mailContent = str_replace('_RSTARTDTIME_',$mailData['tourRegStartTime'],$mailContent);
}
if(!empty($mailData['tourRegEndTime'])){
$mailContent = str_replace('_RENDTIME_',$mailData['tourRegEndTime'],$mailContent);
}
// payment approve

if(!empty($mailData['refNo'])){
$mailContent = str_replace('_TRANSID_',$mailData['refNo'],$mailContent);
}
if(!empty($mailData['amount'])){
$mailContent = str_replace('_AMOUNT_',$mailData['amount'],$mailContent);
}
if(!empty($mailData['date'])){
$mailContent = str_replace('_DATE_',$mailData['date'],$mailContent);
}
if(!empty($mailData['time'])){
$mailContent = str_replace('_TIME_',$mailData['time'],$mailContent);
}
if(!empty($mailData['time'])){
$mailContent = str_replace('_TIME_',$mailData['time'],$mailContent);
}
if(!empty($mailData['getway_status'])){
$mailContent = str_replace('_GETWAYSTATUS_',$mailData['getway_status'],$mailContent);
}
if(!empty($mailData['approve_by'])){
$mailContent = str_replace('_APPROVEDBY_',$mailData['approve_by'],$mailContent);
}


@endphp

{!! $mailContent !!}