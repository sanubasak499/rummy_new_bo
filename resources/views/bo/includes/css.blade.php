<meta name="csrf-token" content="{{ csrf_token() }}" />

<link rel="shortcut icon" href="{{ asset('/favicon.ico') }}" type="image/x-icon">
<link rel="icon" href="{{ asset('/favicon.ico') }}" type="image/x-icon">

{{-- add Additional css before given css --}}
@yield('pre_css')

<link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/jquery-toast/jquery-toast.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.css') }}">
@yield('css')

{{-- App css --}}
<link href="{{ URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/css/bo.min.css')}}" rel="stylesheet" type="text/css" />

{{-- add Additional css after given css --}}
@yield('post_css')