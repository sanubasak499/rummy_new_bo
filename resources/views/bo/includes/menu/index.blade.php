<ul class="metismenu" id="side-menu">
    @foreach ($items as $item)
    @if($item->hasPermission('view'))
    <li>
        <a href="{{ $item->link() }}" target="{{ $item->target }}"> 
            <i class="{{ $item->icon_class ?? "fe-folder-plus" }}"></i>
            <span>{{ $item->display_name }}</span>
            @if(!$item->children->isEmpty())
            <span class="menu-arrow"></span>
            @endif
        </a> 
        @if(!$item->children->isEmpty())
            <ul class="nav-second-level nav" aria-expanded="false">
                @include('bo.includes.menu.items', ['items' => $item->children])
            </ul>
        @endif
    </li>
    @endif
    @endforeach
</ul>