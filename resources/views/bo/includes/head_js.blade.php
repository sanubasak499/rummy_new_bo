{{-- add Additional css after given css --}}
@yield('pre_head_js')

<script>
    window.pageData = window.themeData = {};
    window.pageData.baseUrl = "{{ url('/') }}";
    window.pageData.csrfToken = "{{ csrf_token() }}";
</script>

{{-- add Additional css after given css --}}
@yield('post_head_js')