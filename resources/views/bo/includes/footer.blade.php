<!-- Footer Start -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6"> 
                  &copy; 2015 - {{ date('Y')}} Moonshine Technology Pvt. Ltd. All Rights Reserved.
            </div>
        </div>
    </div>
</footer>
<!-- end Footer -->