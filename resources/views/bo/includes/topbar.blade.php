<!-- Topbar Start -->
<div class="navbar-custom">
    <ul class="list-unstyled topnav-menu float-right mb-0">
        
        <li class="notification-list">
            <a class="nav-link waves-effect waves-light h3 mt-0">
                <i class="mdi mdi-weather-sunny" data-change-theme-mode data-theme-mode="light" style="display:none;"></i>
                <i class="mdi mdi-weather-night" data-change-theme-mode data-theme-mode="dark"></i>
            </a>
        </li>
        
        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                {{-- <img src="{{ asset('assets/images/users/user-1.jpg') }}" alt="user-image" class="rounded-circle"> --}}
                <div class="user-icon">{{ ucfirst(substr(Auth::user()->firstname, 0,1)) }}{{ ucfirst(substr(Auth::user()->lastname,0,1)) }}</div>
                <span class="pro-user-name ml-1">
                    {{ ucfirst(Auth::user()->username) }} <i class="mdi mdi-chevron-down"></i> 
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                <!-- item-->
                <div class="dropdown-header noti-title">
                    <h6 class="text-overflow m-0">Welcome ! {{ ucfirst(Auth::user()->firstname) }}</h6>
                </div>

                <!-- item-->
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item notify-item">
                    <i class="fe-log-out"></i>
                    <span>{{ __('Logout') }}</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>

    </ul>

    <!-- LOGO -->
    <div class="logo-box">
        <a href="{{ url('/') }}" class="logo text-center">
            <span class="logo-lg">
                <img src="{{ asset('assets/images/logo-light.png') }}" alt="" height="40">
                {{-- <span class="logo-lg-text-light">PokerBaazi</span> --}}
            </span>
            <span class="logo-sm">
                {{-- <span class="logo-sm-text-dark" style="font-size: 40px;color: #fff;">P</span> --}}
                <img src="{{ asset('assets/images/logo-sm.png') }}" alt="" height="24">
            </span>
        </a>
    </div>

    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <button class="button-menu-mobile waves-effect waves-light float-left">
                <i class="fe-menu"></i>
            </button>
            
            <a href="{{ url('/') }}" class="logo text-center float-left  d-sm-block d-md-none">
                <span class="logo-lg">
                    <img alt="" src="{{ asset('assets/images/logo-sm.png') }}" height="40">
                </span>
            </a>
        </li>
    </ul>
</div>
<!-- end Topbar -->