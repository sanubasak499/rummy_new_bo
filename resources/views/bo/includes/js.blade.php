{{-- add Additional js after given js --}}
@yield('pre_js')

<!-- Vendor js -->
<script src="{{ URL::asset('assets/js/vendor.min.js')}}"></script>
<script src="{{ asset('assets/libs/jquery-validation/jquery-validation.min.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-toast/jquery-toast.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/toastr.init.js') }}"></script>
<!-- Tippy js-->
<script src="{{ asset('assets/libs/tippy-js/tippy-js.min.js') }}"></script>

@if(Session::has('success'))
<script>
$.NotificationApp.send("Success", "{{ Session::get('success') }}", 'top-right', '#5ba035', 'success');
</script>
@endif

@if(Session::has('error'))
<script>
$.NotificationApp.send("Error", "{{ Session::get('error') }}", 'top-right', '#bf441d', 'error');
</script>
@endif

@if(Session::has('info'))
<script>
$.NotificationApp.send("Info", "{{ Session::get('info') }}", 'top-right', '#3b98b5', 'info');
</script>
@endif

@if(Session::has('warning'))
<script>
    $.NotificationApp.send("Warning", "{{ Session::get('error') }}", 'top-right', '#da8609', 'warning');
</script>
@endif

@if(Session::has('custom_message'))
@php
    $custom_messageArr = Session::get('custom_message');
    $_tosterTitle = ($custom_messageArr['title'])?$custom_messageArr['title'] : 'Custom message';
    $_tosterText  = ($custom_messageArr['text'])?$custom_messageArr['text'] : 'Custom text';

@endphp
<script>
$.NotificationApp.send("{{ $_tosterTitle }}", "{{ $_tosterText }}", 'top-right', '#da8609', 'info');
</script>
@endif

@yield('js')

{{-- Global libary used --}}
<script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{ asset('assets/libs/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.js') }}"></script>
{{-- ./Global libary used --}}

{{-- Global JS init --}}
<script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>
{{-- ./Global JS init --}}

<!-- App js -->
<script src="{{ URL::asset('assets/js/app.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/customGlobal.min.js')}}"></script>

{{-- add Additional js after given js --}}
@yield('post_js')