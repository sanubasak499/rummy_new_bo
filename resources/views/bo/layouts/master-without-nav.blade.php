<!DOCTYPE html>
<html lang="en">
    <head>
        {{-- Default meta tags for all the pages --}}
        @section('meta')
        @include('bo.includes.meta')
        @show

        {{-- default title --}}
        <title>@yield('title', 'Pokerbaazi')</title>

        {{-- Includes All css --}}
        @include('bo.includes.css')
        
        {{-- Include All js which need to add in head --}}
        @include('bo.includes.head_js')

    </head>
    @section('body')
    <body id="app">
    @show
        
        @yield('content')
        
        {{-- Include All js which need to add in footer --}}
        @include('bo.includes.js')
        
    </body>
</html>