<!DOCTYPE html>
<html lang="en">
    <head>
        {{-- Default meta tags for all the pages --}}
        @section('meta')
        @include('bo.includes.meta')
        @show

        {{-- default title --}}
        <title>@yield('title', 'Pokerbaazi')</title>

        {{-- Includes All css --}}
        @include('bo.includes.css')
        
        {{-- Include All js which need to add in head --}}
        @include('bo.includes.head_js')

    </head>
    @section('body')
    <body id="app" class="left-side-menu-dark">
    @show
        <script>
            var themeData=null;
            if(themeData = localStorage.getItem('themeData')){
                themeData = JSON.parse(themeData);
                themeMode = themeData.themeMode || 'dark';
                themeMode == "dark" ? document.body.classList.add('left-side-menu-dark') : document.body.classList.remove('left-side-menu-dark');
            }
            </script>
            
        {{-- Begin page Wrapper --}}
        <div id="wrapper">
            {{-- Include topbar/header --}}
            @include('bo.includes.topbar')

            {{-- Include left_sidebar --}}
            @include('bo.includes.left_sidebar')

            {{-- Start Content here --}}
            <div class="content-page">
                {{-- Start content --}}
                <div class="content">
                    <!-- Start Content-->
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                    {{-- Include Right Sidebar --}}
                    @include('bo.includes.right_sidebar')
                </div>
                {{-- .// content --}}

                {{-- Include Footer --}}
                @include('bo.includes.footer') 
            </div>

            {{-- End content here --}}
        </div>
        {{-- .// End page Wrapper--}}

        {{-- Include All js which need to add in footer --}}
        @include('bo.includes.js')
        
    </body>
</html>