@extends('bo.layouts.master')
@section('title', "Live Tournamet Search | PB BO")
@section('pre_css')
@endsection
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Live Tournament Search</h4>
        </div>
    </div>
</div>
<!-- end page title --> 

<div class="row">
    <div class="col-lg-12">
        <!-- Portlet card -->
        <div class="card">
            <div class="card-header bg-blue py-2 text-white">
                <div class="card-widgets">
                    <a href="javascript:;" data-toggle="reload" data-form-id="#userSearchForm"><i class="mdi mdi-refresh"></i></a>
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class="mdi mdi-minus"></i></a>
                </div>
                <h5 class="card-title mb-0 text-white">Live Tournament Search</h5>
            </div>
            <div id="cardCollpase5" class="collapse show">
                <div class="card-body">
                    <form id="userSearchForm" data-default-url="{{ route('livetournament.tournament.search_post') }}" action="{{ route('livetournament.tournament.search_post') }}{{ !empty($params) ? empty($params['page']) ? "" : "?page=".$params['page'] : '' }}" method="post">
                        @csrf
                        <div class="form-row customDatePickerWrapper">
                            <div class="form-group col-md-4">
                                <label>From: </label>
                                <input name="date_from" type="text" class="form-control customDatePicker from" placeholder="Select From Date" value="{{ !empty($params) ? $params['date_from']  : ''  }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>To: </label>
                                <input name="date_to" type="text" class="form-control customDatePicker to" placeholder="Select To Date" value="{{ !empty($params) ? $params['date_to']  : ''  }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Date Range: </label>
                                <select name="date_range" id="date_range" class="form-control formatedDateRange">
                                    <option data-default-selected value="" {{ !empty($params) ? $params['date_range'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                                    <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                                    <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                                    <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                                    <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                                    <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                                    <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                        <button data-toggle="reload" data-form-id="#userSearchForm"  type="button" class="btn btn-secondary waves-effect waves-light">Clear</button>
                    </form> 
                </div>
            </div>
        </div> <!-- end card-->
    </div><!-- end col -->
</div>

@if(!empty($liveResult))
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            <div class="row mb-2">
            <div class="col-sm-4">  <h5 class="font-18 mt-0">Tournament List </h5></div>  
            <div class="col-sm-8">
                <div class="text-sm-right">
                @if(!empty($liveResult[0]))   
                    <a href="{{ route('livetournament.tournament.exportExcel') }}" > <button type="button" class="btn btn-light mb-2">Export</button> </a>
                    @endif
                </div>
            </div>
            </div>     
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Tournament Start Time</th>
                            <th>Reg. Player Count</th>
                            <th>Tournament Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($liveResult as $key => $user)
                        <tr>
                            <td>{{ $user->TOURNAMENT_NAME }}</td>
                            <td>{{ $user->TOURNAMENT_START_TIME }}</td>
                            <td>{{ $user->REGISTRATED_PLAYER_COUNT }}</td>
                            <?php 
                            $status_id=$user->TOURNAMENT_STATUS;
                            $tournamentStatusName=app\Http\Controllers\bo\livetournament\LiveTournamentController::getTournamentStatusName($status_id); 
                           ?>
                           <td>{{ $tournamentStatusName }}</td>
                            <td><a href="{{ route('livetournament.tournament.liveTournamentDetails', $user->TOURNAMENT_ID) }}" target="_blank">{{ $user->USERNAME }}<i class="fe-edit" title="Details"> </a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" style="display:flex; justify-content: space-between;" class="mt-2">
                    <div>More Records</div>
                        @if(!empty($liveResult))
                            <div>
                            {{ $liveResult->render("pagination::customPaginationView") }}
                            </div>
                        @endif
                    </div>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endif
@endsection

@section('js')
@endsection

