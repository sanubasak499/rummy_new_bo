@extends('bo.layouts.master')
@section('title', "Live Tournament Details  | PB BO")
@section('pre_css')
@endsection
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Edit Live Tournament</h4>
        </div>
    </div>
</div>
<!-- end page title --> 
<?php 
date_default_timezone_set("Asia/Calcutta"); $datetime = date('Y-m-d H:i:s');?>
<div class="row">
    <div class="col-lg-12">
        <!-- Portlet card -->
     
        <div class="card">
            <div class="card-header bg-blue py-2 text-white">
                <div class="card-widgets">
                    <a href="javascript:;" data-toggle="reload" data-form-id="#userSearchForm"><i class="mdi mdi-refresh"></i></a>
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class="mdi mdi-minus"></i></a>
                </div>
                <h5 class="card-title mb-0 text-white">Edit Live Tournament</h5>
            </div>
            <div id="cardCollpase5" class="collapse show">
                <div class="card-body">
                   <div class="form-row customDatePickerWrapper">
                    @if(isset($results[0]) && $results[0]!='' )
                            <div class="form-group col-md-4">
                                <label>Tournament Name :{{ $results[0]->TOURNAMENT_NAME }} </label>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Start Time : {{$results[0]->TOURNAMENT_START_TIME  }} </label>
                            </div>
                            <div class="form-group col-md-4">
                           @php 
                            $status_id= $results[0]->TOURNAMENT_STATUS;
                            
                            $tournamentStatusName=app\Http\Controllers\bo\livetournament\LiveTournamentController::getTournamentStatusName($status_id); 
                           @endphp
                                <label>Status : {{ $tournamentStatusName }}</label>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Reg Players : {{count($results)}} </label>
                            </div>
                            @if($datetime < $results[0]->TOURNAMENT_START_TIME)
                            <div class="form-group col-md-4">
                             Tournament Not Started Yet
                            </div>
                            @else 
                            <div class="form-group col-md-4">
                                <a  onclick="return confirm('Are you sure you want to close late registration of <?php echo $results[0]->TOURNAMENT_NAME; ?>? This action cannot be undone and will result in finishing of the tournament in the game lobby.');" href="{{ route('livetournament.tournament.lateregistrationClosed', $results[0]->TOURNAMENT_ID) }}">Close Late Registration</a>
                            </div>
                             @endif
                            @else
                            <div class="form-group col-md-4">
                                <label>Tournament Name : </label>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Start Time :  </label>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Status :</label>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Reg Players :  </label>
                            </div>
                            <div class="form-group col-md-4">
                            </div>
                            @endif
                        </div>
                </div>
            </div>
        </div> <!-- end card-->
    </div><!-- end col -->
</div>

@if(!empty($results[0]))
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
             <form id="userSearchForm" data-default-url="{{ route('livetournament.tournament.updateAssignTable') }}" action="{{ route('livetournament.tournament.updateAssignTable') }}"  method="post">
                        @csrf

                <h4 class="header-title">User List</h4>
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>USER ID</th>
                            <th>USERNAME NAME</th>
                            <th>Re-Entry Count</th>
                            <th>Buy-in</th>
                            <th>EMAIL</th>
                            <th>CONTACT NO</th>
                            <th>REGISTERED ON</th>
                            <th>ASSIGN TABLE</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                        @foreach ($results as $key =>  $res)
                          <?php //print_r($results);die();?>
                        <tr>
                           <td>{{ $res->USER_ID }}</td>
                            <input type="hidden" name="user_count" value="{{ count($results)}}">
                            <input type="hidden" name="USER_ID{{$key }}" value="{{ $res->USER_ID }}">
                            <input type="hidden" name="tournament_id" value="{{ $res->TOURNAMENT_ID }}">
                           <!-- <td><a href="{{ route('user.account.user_profile', encrypt($res->USER_ID)) }}" target="_blanck" >{{ $res->PLAYER_NAME }}</a></td> -->
                           <td><a href="#" target="_blanck" >{{ $res->PLAYER_NAME }}</a></td>
                           <?php 
                            $tournamnetid = $res->TOURNAMENT_ID;
                            $userid=$res->USER_ID;
                            $reentryreg=app\Http\Controllers\bo\livetournament\LiveTournamentController::countReEntryRegistration($tournamnetid,$userid); 
                           ?>
                           <td>{{ $reentryreg[0]->reentry }}</td>
                           <td>{{$res->Buyin }}</td>
                           <!-- <td></td> -->
                           <td>{{ $res->EMAIL_ID }}</td>
                           <td>{{ $res->CONTACT }}</td>
                           <td>{{ $res->REGISTERED_DATE }}</td>
                           <td><input type="number" min="0" name="live_table_no<?php echo $key; ?>"  value="<?php echo $res->LIVE_TABLE_NO; ?>"></td>
                           <td>
                           @if(strtotime($res->TOURNAMENT_START_TIME) < strtotime($datetime))
                           <a href="{{ route('livetournament.tournament.updateRebuy', ['tid'=>$res->TOURNAMENT_ID, 'id'=>$res->USER_ID]) }}" onclick="return confirm('Are you sure you want to bust {{ $res->PLAYER_NAME }} from {{ $res->TOURNAMENT_NAME }}  and allow re-buys for him?');" style="color:#FF0000;" >Allow Re-Buy</a>
                           @else
                           <a href="{{ route('livetournament.tournament.updateUnRegistration', ['tid'=>$res->TOURNAMENT_ID, 'id'=>$res->USER_ID]) }}" onclick="return confirm('Are you sure you want to unregister {{ $res->PLAYER_NAME }} from {{ $res->TOURNAMENT_NAME}}  ?');" >UnRegister</a>
                           @endif
                            </td>
                        </tr>
                        @endforeach
                       
                    </tbody>
                </table>
                <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" style="display:flex; justify-content: space-between;" class="mt-2">
                    <!-- <div>More Records</div> -->
                    <div>
                    </div>
                </div>
                <button type="submit" id="button" name="keyword" class="btn btn-primary waves-effect waves-light">Update</button>&nbsp;
                <a href='' onClick="refreshPage()" class="btn btn-secondary waves-effect waves-light" >Refresh</a>
            </div> <!-- end card body-->
  
          
         </form>
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endif
@endsection

@section('js')
<script>
function refreshPage(){
    window.location.reload();
} 
</script>
@endsection