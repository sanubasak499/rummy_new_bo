@extends('bo.layouts.master')
@section('title', "Live Tornament Entries | PB BO")

@section('pre_css')
    <!--<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">-->
    <link href="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">.customselect{ float: none; height: auto !important; line-height: 35px; border: 1px solid #ced4da;border-radius: .2rem;}.customselect ul.list{ width: 100% }</style>
@endsection
@section('content')
<!--Live Entries Breadcrumb Start-->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active">Live Entries</li>
                </ol>
            </div>
            <h4 class="page-title">Live Entries </h4>
        </div>
    </div>
</div>
<!--Live Entries Breadcrumb End-->
<!--Live Entries Form Start-->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form id="live-entries" class="" method="post"  action="{{url('/livetournament/live-entries') }}">
                    @csrf
                    <div class="form-row mb-2">
                        <div class="form-group col-md-6">
                            <label for="username">User Name:</label>
                            <input type="text" required="" placeholder="Username" name="username" id="username" class="form-control  @error('username') parsley-error @enderror" value="{{ old('username') }}" placeholder="  ">
                            @error('username')
                                <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="action">Action </label>
                            <select name="action" class="customselect  @error('action') parsley-error @enderror" id="action" data-plugin="customselect">
                                <option value="">Select One</option>
                                <option value="1" {{ (old('action') == 1)? 'selected' : ''}}>Buy In</option>
                                <option value="2" {{ (old('action') == 2)? 'selected' : ''}}>Tournament Win</option>
                                <option value="3" {{ (old('action') == 3)? 'selected' : ''}}>Bounty Win</option>
                            </select>
                            @error('action')
                                <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                            @enderror
                        </div>
                    </div>

                    <div class="form-row ">
                        <div class="form-group col-md-6">
                            <label for="tournament_list">Select Tournament </label>
                            <!--<input type="hidden" name='tournament_buyin' id="tournament_buy_list" value="">-->
                            <select data-parsley-validate name="tournament" id='tournament_list' class="customselect @error('tournament') parsley-error @enderror" data-plugin="customselect" >
                                <option value="">Select</option>
                                @foreach($tournament_list as $tournament)
                                        @php
                                            $selected='';
                                            if(old('tournament') == $tournament->TOURNAMENT_ID){
                                            $selected='selected';
                                            }
                                        @endphp
                                    <option {{ $selected }}  data-tournament_amount="{{ $tournament->TOURNAMENT_AMOUNT }}" value="{{ $tournament->TOURNAMENT_ID }}">{{ $tournament->TOURNAMENT_NAME }} - {{ date("d/m/Y H:i:s",strtotime($tournament->TOURNAMENT_START_TIME)) }} - {{ $tournament->TOURNAMENT_ID }} </option>
                                @endforeach
                            </select>
                            @error('tournament')
                                <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                            @enderror
                        </div>
                        

                        <div class="form-group col-md-6">
                            <label for="amount">Amount</label>
                            <input name="amount" type="text"  id="amount" class="form-control  @error('amount') parsley-error @enderror" value="{{ old('amount') }}" placeholder="  " >
                            @error('amount')
                                <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                            @enderror
                        </div>
                    </div>
                    <div class="button-list">
                        <button type="submit" class="btn btn-success waves-effect waves-light"><i class=" fa fa-save mr-1"></i> Submit</button>
                        <a href="{{ url('/livetournament/live-entries') }}" class="btn btn-dark waves-effect waves-light ml-2"><i class="mdi mdi-replay mr-1"></i>Reset </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<!--Live Entries Form End-->
@endsection

@section('js')
<script src="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.js') }}"></script>
<script>
    
    $(document).ready(function() {
        $('.customselect').niceSelect();
        changeInputAmountStatus();
        $("#action").change(function(){
            var $actionVal = $(this).val();
        if($actionVal == null || $actionVal == ""){
            $(this).siblings('.nice-select').after('<ul id="' + $(this).attr('id') + '-error" class="parsley-error parsley-errors-list filled"><li class="parsley-required">This field is required.</li></ul>');
        }else{
             $("#" + $(this).attr('id') + "-error" ).remove();
        }
            changeInputAmountStatus();
        });
    });
    function changeInputAmountStatus(){
        var $actionVal = $("#action").val();
        if($actionVal == '1'){
          $("#amount").val($("#tournament_list").find(':selected').data('tournament_amount'));
          $("#amount").attr('readonly','readonly').closest(".form-group").hide();  
          resetError('amount');
        }else{
            
          $("#amount").removeAttr('readonly','readonly').val('');
          $("#amount").removeAttr('readonly','readonly').closest(".form-group").show();
        }
    }


    $(document).on("blur","[name='username']",function(){
              var username = $(this).val();
              var _el = $(this);
              console.log(username);
               $.ajax({
                   "url":`${window.pageData.baseUrl}/livetournament/live-entries/ajax-live-entries-getuser`,
                   "method" : "POST",
                   "data" : { "username" : username },
                   "dataType" : "json",
                   success:function(data){
                       if(data.status){
                           if(_el.siblings('.parsley-errors-list').length){
                               _el.siblings('.parsley-errors-list').remove();
                               _el.removeClass('parsley-error');
                           }
                           _el.addClass('parsley-success');
                       }else{
                           if(_el.siblings('.parsley-errors-list').length){
                               _el.siblings('.parsley-errors-list').html('<li class="parsley-required">' + data.message + '</li>');
                               _el.addClass('parsley-error');
                           }else{
                                _el.removeClass('parsley-success');
                                //_el.removeClass('valid');
                                _el.addClass('parsley-error');
                                _el.after('<ul class="parsley-errors-list filled" ><li class="parsley-required">' + data.message + '</li></ul>');
                           }
                       }
                   },
                   error:function(){
                       _el.val(''); 
                   }
               });//ajaxClose
            });//document on change close
            
    $("#tournament_list").change(function(){
      var $tournament_amount = $(this).find(':selected').data('tournament_amount');
        if($tournament_amount == null || $tournament_amount == ""){
            $tournament_amount='0';
            $(this).siblings('.nice-select').after('<ul id="' + $(this).attr('id') + '-error" class="parsley-error parsley-errors-list filled"><li class="parsley-required">This field is required.</li></ul>');
        }else{
            $("#" + $(this).attr('id') + "-error" ).remove();
            resetError('amount');
        }
        $("#amount").val($tournament_amount);
        changeInputAmountStatus();

    });//document on change close

    function resetError(id){
        $("#"+ id + "-error" ).remove();
        $("#" + id).removeClass('parsley-error');
    }
    function addError(_el,id){
        _el.siblings('.nice-select').after('<ul id="' + id + '-error" class="parsley-error parsley-errors-list filled"><li class="parsley-required">This field is required.</li></ul>');
    }
    
    $.validator.setDefaults({
        ignore: []
    });
    $("#live-entries").validate({
        errorContainer: $("li.parsley-required"),
        rules: {
            username:{
                 "required":true,
            },
            action: {
              required : true,
              number: true
            },
            tournament: {
              required :true,
              number:true,
            },
            amount: {
              required: true,
              number: true
            },
        },
//        messages: {
//            username: {
//                required: "The username may only contain letters and numbers.",
//            }
//        },
        success: function(label,element) {
            
            $(element).siblings('ul.parsley-errors-list').remove();

        },
        errorClass: 'parsley-error',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            var id = $(element).attr('id');
            var error_msg='';
            error_msg = '<ul id="'+ id + '-error" class="parsley-error parsley-errors-list filled"><li class="parsley-required">' + error.text() + '</li></ul>';
            if (placement) {
              $(placement).append(error_msg);
            } else {
                if(element.siblings('.parsley-errors-list').length){
                    element.siblings('.parsley-errors-list').find("li").text(error.text());
                    element.addClass('parsley-error');
                }else if(element.data('plugin')=='customselect'){
                    element.next('.nice-select').after(error_msg);  
                }else{
                    element.after(error_msg);  
                }
            }
        }
        
});
        
    
</script>

@endsection

