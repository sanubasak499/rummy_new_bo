@extends('bo.layouts.master')

@section('title', "Create Segment| PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/c3/c3.min.css') }}" rel="stylesheet" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('post_css')
<style>
    .formtext {
        font-size: 11px;
        font-style: italic;
        color: #999198;
        padding: 6px 0 0 0;
        margin: 0;
        line-height: 13px;
    }
</style>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"> Notifications </li>
                            <li class="breadcrumb-item"><a href="{{ route('notification.manage-segments.index') }}"> Manage Segments </a> </li>
                            <li class="breadcrumb-item active"> Create Segment</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Create Segment</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-lg-8">
                                <h5 class="font-18"></h5>
                            </div>
                            <div class="col-lg-4">
                                <div class="text-lg-right mt-3 mt-lg-0">
                                    <a href="{{ route('notification.manage-segments.index') }}" class="btn btn-dark waves-effect waves-light"><i class="fa fa-bars mr-1"></i> View Segment</a>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane show" id="profile" style="min-height: 300px;">
                            <form id="adjustmentExcelImport" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group ">
                                            <label>Title:<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" name="SEGMENT_TITLE" id="SEGMENT_TITLE" maxlength="50" placeholder="" value="" required="">
                                        </div>
                                        <div class="form-group">
                                            <label>Upload User's List: <span class="text-danger">*</span></label>
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <input type="file" class="form-control" name="adjustExcel" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" id="adjustExcel" required>
                                                </div>
                                                <div class="col-md-5">
                                                    <a href="{{ asset('/data/bo/Sample-Manage-Segment.xlsx') }}" class="btn btn-success waves-effect waves-light d-block"><i class="fa fa-download mr-1"></i> Download Sample</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group ">
                                            <label>Description:</label>
                                            <textarea name="SEGMENT_DESCRIPTION" id="SEGMENT_DESCRIPTION" class="form-control" rows="5" maxlength="100"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <button type="button" name="importAdjustExcel" id="importAdjustExcel" class="btn btn-success waves-effect waves-light formActionButton"><i class="fa fa-save mr-1"></i> Save</button>
                                <button style="display:none" class="btn btn-primary formActionButton mr-1" type="button" disabled>
                                    <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                                </button>
                                <a href="{{ route('notification.manage-segments.createSegments') }}" id="import-excel-reset" class="btn btn-secondary waves-effect waves-light ml-1">
                                    <i class="mdi mdi-replay mr-1"></i> Reset
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="adjustmentcontenttable">
        <div class="modal-dialog modal-lg">
            <div class="modal-content bg-transparent shadow-none">
                <div class="card">
                    <div class="card-header bg-dark py-3 text-white">
                        <div class="card-widgets">
                            <a href="#" data-dismiss="modal">
                                <i class="mdi mdi-close"></i>
                            </a>
                        </div>
                        <h5 class="card-title mb-0 text-white font-18">Review Users</h5>
                    </div>

                    <form id="confirmUpdateExcelUpload" method="post">
                        <div class="card-body">
                            <table id="basic-datatable" class="basic-datatable table dt-responsive nowrap w-100">
                                <thead class="thead-dark">
                                    <tr>
                                        <th class="text-white">#</th>
                                        <th class="text-white">Username</th>
                                        <th class="text-white">User ID</th>
                                        <th class="text-white">Review Status</th>
                                    </tr>
                                </thead>
                                <tbody id="review-data">
                                </tbody>
                            </table>
                            <a href="#" data-dismiss="modal" class="btn btn-warning waves-effect waves-light mr-1 ViewModelSubmitBtn"><i class="fa fa-upload mr-1"></i> Reupload</a>

                            <button type="button" name="confirmExcelUpload" id="confirmExcelUpload" class="btn btn-success waves-effect waves-light formActionButtonvv ViewModelSubmitBtn"><i class="fa fa-save mr-1"></i> Continue Anyway</button>
                            <button style="display:none" class="btn btn-primary formActionButtonvv mr-1" type="button" disabled>
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                Loading...
                            </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('js')
<!--C3 Chart-->
<script src="{{ asset('assets/libs/d3/d3.min.js') }}"></script>
<script src="{{ asset('assets/libs/c3/c3.min.js') }}"></script>
@endsection

@section('post_js')

@if(Session::has('custom_message'))
<script>
    $.NotificationApp.send("Info", "{{ Session::get('custom_message')['text'] }}", 'top-right', '#bf441d', 'Error', 50000);
</script>
@endif

<script>
    function onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }

    $(document).ready(function() {
        $("#adjustmentExcelImport").validate({
            rules: {
                SEGMENT_TITLE: {
                    required: true,
                    remote: {
                        url: `${window.pageData.baseUrl}/notification/manage-segments/chaakeSegmentTitle`,
                        type: "post",
                        dataFilter: function(response) {
                            console.log("Sdfgh");
                            response = JSON.parse(response);
                            let result = false;
                            result = response.status == 201 ? true : `"Title already exist"`;
                            return result;
                        }
                    }
                }
            }
        });
        
        $("#importAdjustExcel").on('click', (e) => {
            if ($("#adjustmentExcelImport").valid()) {
                $('.formActionButton').toggle();
                e.preventDefault();
                var data = new FormData();
                data.append('excel_file', $('#adjustExcel').get(0).files[0]);
                $.ajax({
                    url: "{{url('/notification/manage-segments/SegmentMatchExcel')}}",
                    type: "post",
                    data: data,
                    enctype: 'multipart/form-data',
                    processData: false, // Important!
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        if (response.status == 200) {
                            
                            let unqiueflag = response.flags.filter(onlyUnique);
                            if(unqiueflag.length > 0){
                                $('#confirmExcelUpload').show();
                                if(unqiueflag.length == 1){
                                    if(!unqiueflag[0]){
                                        $.NotificationApp.send("Error", "All the users ara invalid.", 'top-right', '#FF9494', 'Error');
                                        $('#confirmExcelUpload').hide();
                                    }
                                }

                                var element = '';
                                for (let i = 0; i < response.data.length; i++) {
                                    $('#basic-datatable tbody').html('');
                                    $('#basic-datatable').DataTable().destroy();
                                    let cssClass = '';
                                    let txt = '';
                                    if (response.flags[i]) {
                                        cssClass = 'text-success';
                                        txt = 'Ok';
                                    } else {
                                        cssClass = 'text-danger';
                                        txt = 'Invalid User';
                                    }

                                    element += `<tr class="${cssClass}">
                                    <td>${(i+1)}</td>
                                    <td>${response.data[i]}</td>
                                    <td>${response.user_ids[i]}</td>
                                    <td>${txt}</td>
                                </tr>`;
                                }

                                $("#review-data").html(element);
                                $('#basic-datatable').DataTable({
                                    "language": {
                                        "paginate": {
                                            "previous": "<i class='mdi mdi-chevron-left'>",
                                            "next": "<i class='mdi mdi-chevron-right'>"
                                        }
                                    },
                                    "drawCallback": function() {
                                        $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                                    }
                                });

                                $("#adjustmentcontenttable").modal('show');
                            }else{
                                $.NotificationApp.send("Error", "File is empty", 'top-right', '#FF9494', 'Error');
                            }
                        } else if (response.status == 301) {

                            $.NotificationApp.send("Error", "Heading Must Be 'Username'", 'top-right', '#FF9494', 'Error');

                        } else if (response.status == 302) {

                            $.NotificationApp.send("Error", "Only excel file accept", 'top-right', '#FF9494', 'Error');

                        } else if (response.status == 303) {

                            $.NotificationApp.send("Error", "Data is empty.", 'top-right', '#FF9494', 'Error');

                        } else if (response.status == 304) {
                            $.NotificationApp.send("Error", "Sample Not Match", 'top-right', '#FF9494', 'Error');
                        } else if (response.status == 500) {
                            $.NotificationApp.send("Error", "Data is Empty", 'top-right', '#FF9494', 'Error');
                        }

                    }
                }).always(function() {
                    $('.formActionButton').toggle();
                });
            }
        });


        $('#confirmExcelUpload').on('click', (e) => {
            var $e = $(e.target);
            if ($("#confirmUpdateExcelUpload").valid()) {
                e.preventDefault();
                var form = $('#adjustmentExcelImport');
                var data = new FormData(); // Currently empty
                data.append('excel_file', $('#adjustExcel').get(0).files[0]);
                data.append('SEGMENT_TITLE', $('#SEGMENT_TITLE').val());
                data.append('SEGMENT_DESCRIPTION', $('#SEGMENT_DESCRIPTION').val());
                $('.formActionButtonvv').toggle();
                $('#confirmExcelUpload').hide();
                $.ajax({
                    url: "{{url('/notification/manage-segments/insertCreateSegments')}}",
                    type: "post",
                    data: data,
                    dataType: 'json',
                    enctype: 'multipart/form-data',
                    processData: false, // Important!
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        $('#confirm-hide-excel').hide();
                        if (response.status == 200) {
                            $.NotificationApp.send("Success", "Excel Data Uploaded", 'top-right', '#5ba035', 'success');
                            window.location.reload();
                        } else if (response.status ==500) {
                          $.NotificationApp.send("Error", "User does not exist!.", 'top-right', '#FF9494', 'Error');
                            // console.log(response);
                        }
                    },
                    error: function(err, e) {
                        console.log(err, e);
                    }
                });
            }
        });

    });
    
    $('#SEGMENT_TITLE').bind('keypress', function(e) {
        console.log(e.which);
        if ($('#SEGMENT_TITLE').val().length == 0) {
            var k = e.which;
            var ok = k >= 65 && k <= 90 || // A-Z
                k >= 97 && k <= 122 || // a-z
                k >= 48 && k <= 57; // 0-9

            if (!ok) {
                e.preventDefault();
            }
        }
    });

    function cancel() {
        c = confirm("Do you really want to Clear ?");
        if (c == true) {
            location.reload(true);
        } else {
            return false;
        }
    }
</script>

<script src="{{ asset('assets/js/pages/form-validation.init.js') }}"></script>
@endsection