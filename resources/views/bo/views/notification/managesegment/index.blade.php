@extends('bo.layouts.master')

@section('title', "Manage Segments | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/c3/c3.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('post_css')
    
@endsection

@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item">Notifications</li>
                     <li class="breadcrumb-item active">Manage Segments</li>
                  </ol>
               </div>
               <h4 class="page-title">
                  Manage Segments
               </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <div class="row mb-3">
                     <div class="col-lg-8">
                        <h5 class="font-18"> </h5>
                     </div>
                     <div class="col-lg-4">
                        <div class="text-lg-right mt-3 mt-lg-0"><a href="{{ route('notification.manage-segments.createSegments') }}" class="btn btn-success waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add Segment</a></div>
                     </div>
                  </div>
                  <form id="userSearchForm" data-default-url="{{ route('notification.manage-segments.search') }}" action="{{ route('notification.manage-segments.search') }}{{ !empty($params) ? empty($params['page']) ? "" : "?page=".$params['page'] : '' }}" method="post">
                    @csrf
                     <div class="row">
                        <div class="form-group col-md-4">
                           <label>Title: </label>
                           <input type="text" class="form-control" name="SEGMENT_TITLE" id="SEGMENT_TITLE" placeholder="Title" value="{{ !empty($params) ? $params['SEGMENT_TITLE']  : ''  }}">
                        </div>
                        <div class="form-group col-md-4">
                           <label>Description:</label>
                           <input type="text" class="form-control" name="SEGMENT_DESCRIPTION" id="SEGMENT_DESCRIPTION" placeholder="Description" value="{{ !empty($params) ? $params['SEGMENT_DESCRIPTION']  : ''  }}">
                        </div>
                        <div class="form-group col-md-4">
                           <label>Status: </label>
                           <select name="STATUS" class="customselect" data-plugin="customselect">
                              <option data-default-selected value="" {{ !empty($params) ? $params['STATUS'] == "" ? "selected" : '' :  'selected'  }}>Select Status</option>
                              <option value="1" {{ !empty($params) ? $params['STATUS'] == 1 ? "selected" : ''  : ''  }} >Active</option>
                              <option value="2" {{ !empty($params) ? $params['STATUS'] == 2 ? "selected" : ''  : ''  }}>Inactive</option>
                           </select>
                        </div>
                     </div>
                     <div class="row  customDatePickerWrapper">
                        <div class="form-group col-md-4">
                           <label>From: </label>
                           <input name="date_from" id="date_from" type="text" class="form-control customDatePicker from flatpickr-input" placeholder="Select From Date" value="{{ !empty($params) ? $params['date_from']  : ''  }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-4">
                           <label>To: </label>
                           <input name="date_to" id="date_to" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="{{ !empty($params) ? $params['date_to']  : ''  }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-4">
                           <label>Date Range: </label>
                           <select name="date_range" id="date_range"  class="formatedDateRange customselect" data-plugin="customselect" >
                              <option data-default-selected="" value="" selected="">Select Date Range</option>
                              <option value="1">Today</option>
                              <option value="2">Yesterday</option>
                              <option value="3">This Week</option>
                              <option value="4">Last Week</option>
                              <option value="5">This Month</option>
                              <option value="6">Last Month</option>
                           </select>
                        </div>
                     </div>
                     <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                     <a href="{{ route('notification.manage-segments.index') }}" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
         </div>
         <!-- end col -->
      </div>
      @if(!empty($ManageSegmentsResult))
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-dark text-white">
                  <div class="card-widgets">
                     <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>                                 
                  </div>
                  <h5 class="card-title mb-0 text-white">Segment List</h5>
               </div>
               <div id="cardCollpase7" class="collapse show">
                  <div class="card-body">
                     <table id="example" class="table dt-responsive nowrap w-100">
                        <thead>
                           <tr>
                              <th>Title</th>
                              <th>Description</th>
                              <th>Users</th>
                              <th>Status</th>
                              <th>Created On</th>
                              <th>Created By</th>
                           </tr>
                        </thead>
                        <tbody>
                           
                        @foreach ($ManageSegmentsResult as $key => $result)
                           <tr>
                              <td>{{ $result->SEGMENT_TITLE }} </td>
                              <td>{{ $result->SEGMENT_DESCRIPTION }}</td>
                              <td><a href="{{ route('notification.manage-segments.exportExcel',$result->SEGMENT_ID ) }}" ><u>Download</u></td>
                              
                              <td>
                              <label class="switches">
                                @if($result->STATUS == 1)
                                <input type="checkbox" data-id="{{ $result->SEGMENT_ID }}" class='status_switch' checked />
                                @else
                                <input type="checkbox" data-id="{{ $result->SEGMENT_ID }}" class='status_switch'   />
                                @endif 
                                <span class="slider round"></span>
                            </label>
                              </td>

                              <td>{{ $result->CREATED_DATE }}</td>
                              <td>{{ $result->username }}</td>
                           </tr>
                         
                        @endforeach
                          
                        </tbody>
                     </table>
                     <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" style="display:flex; justify-content: space-between;" class="mt-2">
                        <div>More Records</div>
                        @if(!empty($ManageSegmentsResult))
                            <div>
                            {{ $ManageSegmentsResult->render("pagination::customPaginationView") }}
                            </div>
                        @endif
                     </div>
                  </div>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
      @endif
   </div>
</div>

@endsection


@section('js')
<!--C3 Chart-->
<script src="{{ asset('assets/libs/d3/d3.min.js') }}"></script>
<script src="{{ asset('assets/libs/c3/c3.min.js') }}"></script>

<script>

$(document).ready( function() {
    $('#example').dataTable({
        /* No ordering applied by DataTables during initialisation */
        "order": []
    });
})
// This Line use for Switech on or off
// $('[data-plugin="switchery"]').each(function(a,e){new Switchery($(this)[0],$(this).data())}) 

// This Function Use to Change status
var switchStatus = false;
		$(document).on('change',".status_switch", function() {

			if($(this).is(':checked')){
				var STATUS = 1; 
			}else{
				var STATUS = 0; 
			}
			$.ajax({
			   "url": "{{url('/notification/manage-segments/manageSegmentsChangeStatus')}}",
			   "method":"POST",
			   "data":{"id":$(this).data('id'),"STATUS":STATUS},
			   "dataType":"json",
			   success:function(data){
                $.toast({
					heading: 'Well Done !!',
					text: data.message,
					showHideTransition: 'slide',
					icon: 'success',
					loader: true,
					position: 'top-right',
					hideAfter: 6000
				});
			   }
			});
		});
</script>

@endsection

@section('post_js')


<script src="{{ asset('js/bo/analytics.js') }}"></script>
@endsection