@extends('bo.layouts.master')

@section('title', "Push Notifications | PB BO")

@section('pre_css')
    <link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('post_css')
<style type="text/css">
    .blurClass {
        filter: blur(1px);
        -webkit-filter: blur(1px);
        position: relative;
    }

    .blurClass::after {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        content: "";
    }
    .select2W+span{
        width: 100% !important;
    }
    .text-decoration-underline{
        text-decoration: underline !important;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="#">Notifications</a></li>
                    <li class="breadcrumb-item active"> Push Notifications</li>
                </ol>
            </div>
            <h4 class="page-title">
                Push Notifications
            </h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-lg-8">
                        <h5 class="font-18"> </h5>
                    </div>
                    <div class="col-lg-4">
                        <div class="text-lg-right mt-3 mt-lg-0"><a
                                href="{{ route('notification.push-notification.create') }}"
                                class="btn btn-success waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add
                                Notification</a></div>
                    </div>
                </div>
                <form id="notificationFilterForm" action="{{ route('notification.push-notification.index') }}"
                    method="post">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label>Name: </label>
                            <input type="text" class="form-control" name="PUSH_NOTIFICATION_NAME"
                                id="PUSH_NOTIFICATION_NAME"
                                placeholder="Enter Name"
                                value="{{ old('PUSH_NOTIFICATION_NAME', $params['PUSH_NOTIFICATION_NAME'] ?? '') }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Title:</label>
                            <input type="text" class="form-control" name="PUSH_NOTIFICATION_TITLE"
                                id="PUSH_NOTIFICATION_TITLE"
                                placeholder="Enter Title"
                                value="{{ old('PUSH_NOTIFICATION_TITLE', $params['PUSH_NOTIFICATION_TITLE'] ?? '') }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Pushed By: </label>
                            <select name="PUSHED_BY" class="customselect" data-plugin="customselect">
                                <option data-default-selected="" value="" selected="">Select Pushed By</option>
                                @foreach ($pushedByUsers as $pushedByUser)
                                <option value="{{ $pushedByUser->PUSHED_BY }}"
                                    {{ $pushedByUser->PUSHED_BY ==  old('PUSHED_BY', $params['PUSHED_BY'] ?? '') ? 'selected' : '' }}>
                                    {{ $pushedByUser->PUSHED_BY }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row  ">
                        <div class="form-group col-md-4">
                            <label>Redirection: </label>
                            <select name="REDIRECTION_TYPE_ID" class="customselect Redirection"
                                data-plugin="customselect">
                                <option value="0"
                                    {{ "0" == old('REDIRECTION_TYPE_ID', $params['REDIRECTION_TYPE_ID'] ?? '0') ? 'selected' : '' }}>
                                    Select Redirection</option>
                                <option value="1"
                                    {{ "1" == old('REDIRECTION_TYPE_ID', $params['REDIRECTION_TYPE_ID'] ?? '') ? 'selected' : '' }}>
                                    Webpage</option>
                                <option value="2"
                                    {{ "2" == old('REDIRECTION_TYPE_ID', $params['REDIRECTION_TYPE_ID'] ?? '') ? 'selected' : '' }}>
                                    Tournament</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <div class='urlsection' style='display:none;'>
                                <div class="form-group ">
                                    <label>URL:</label>
                                    <input type="text" class="form-control" name="REDIRECTION_URL" id="REDIRECTION_URL"
                                        placeholder="Enter URL"
                                        value="{{ old('REDIRECTION_URL', $params['REDIRECTION_URL'] ?? '') }}">
                                </div>
                            </div>

                            <div style='display:none;' class='tournamentsection'>
                                <div class="form-group">
                                    <label for="TOURNAMENT_ID" class="w-100">Tournament: </label>
                                    <select name="TOURNAMENT_ID" id="TOURNAMENT_ID" class="form-control select2W"
                                        data-toggle="select2">
                                        <option value="" selected="">Select Tournament</option>
                                        @foreach ($tournaments as $tournament)
                                        <option value="{{ $tournament->TOURNAMENT_ID }}"
                                            {{ $tournament->TOURNAMENT_ID == old('TOURNAMENT_ID', $params['TOURNAMENT_ID'] ?? '') ? 'selected' : '' }}>
                                            {{ $tournament->TOURNAMENT_NAME }}</option>
                                        @endforeach
                                    </select>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Sent To: </label>
                                    <select name="SENT_TO" id="SENT_TO" class="customselect"
                                        data-plugin="customselect" data-target-class="selected_">
                                        <option value="" {{ "" == old('SENT_TO', $params['SENT_TO'] ?? '') ? 'selected' : '' }}>Select Sent To</option>
                                        <option value="1" {{ "1" == old('SENT_TO', $params['SENT_TO'] ?? '') ? 'selected' : '' }}>Segment</option>
                                        <option value="2" {{ "2" == old('SENT_TO', $params['SENT_TO'] ?? '') ? 'selected' : '' }}>User</option>
                                        <option value="0" {{ "0" == old('SENT_TO', $params['SENT_TO'] ?? '') ? 'selected' : '' }}>Save For Later</option>
                                    </select>
                                </div>
                                <div class="col-md-6 boxhideshow selected_1" style="display: {{ '1' == old('SENT_TO', $params['SENT_TO'] ?? '') ? 'block' : 'none' }}">
                                    <label>&nbsp;</label>
                                    <select name="SEGMENT_IDS[]" class="form-control select2W" data-toggle="select2" data-placeholder="Select Segment" multiple="multiple">
                                        {{-- <option data-default-selected="" value=""
                                            {{ in_array("",old('SEGMENT_IDS', $params['SEGMENT_IDS'] ?? [])) ? 'selected' : '' }}>
                                            Select Segment </option> --}}
                                        @foreach ($segments as $segment)
                                        <option value="{{ $segment->SEGMENT_ID }}"
                                            {{ in_array($segment->SEGMENT_ID,old('SEGMENT_IDS', $params['SEGMENT_IDS'] ?? [])) ? 'selected' : '' }}>
                                            {{ $segment->SEGMENT_TITLE }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row  customDatePickerWrapper">
                        <div class="form-group col-md-4">
                            <label>From: </label>
                            <input name="date_from" id="date_from" type="text"
                                class="form-control customDatePicker from flatpickr-input"
                                placeholder="Select From Date"
                                value="{{ old('date_from', $params['date_from'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-4">
                            <label>To: </label>
                            <input name="date_to" id="date_to" type="text"
                                class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date"
                                value="{{ old('date_to', $params['date_to'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Date Range: </label>
                            <select name="date_range" id="date_range" class="formatedDateRange customselect"
                                data-plugin="customselect">
                                <option data-default-selected="" value="" selected="">Select Date Range</option>
                                <option value="1">Today</option>
                                <option value="2">Yesterday</option>
                                <option value="3">This Week</option>
                                <option value="4">Last Week</option>
                                <option value="5">This Month</option>
                                <option value="6">Last Month</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect waves-light"><i
                            class="fas fa-search mr-1"></i> Search</button>
                    <a href="{{ route('notification.push-notification.index') }}"
                        class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i>
                        Reset</a>
                </form>
            </div>
            <!-- end card-body-->
        </div>
    </div>
    <!-- end col -->
</div>
@if(!empty($notifications))
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-dark text-white">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false"
                        aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>
                </div>
                <h5 class="card-title mb-0 text-white">View Push Notifications</h5>
            </div>
            <div id="cardCollpase7" class="collapse show">
                <div class="card-body">
                    <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Pushed On</th>
                                <th>Name</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Sent To</th>
                                <th>Pushed By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i=0; @endphp
                            @foreach ($notifications as $notification)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{!! changeDateFormate($notification->PUSHED_DATE) !!}</td>
                                <td>{{ $notification->PUSH_NOTIFICATION_NAME }}</td>
                                <td>{{ $notification->PUSH_NOTIFICATION_TITLE }}</td>
                                <td>{{ $notification->append('DescriptionLimit')->DescriptionLimit }}</td>
                                <td>{{ $notification->SENT_TO == 1 ? "Segment" : ($notification->SENT_TO == 2 ? "User" : "Save For Later") }}</td>
                                <td>{{ $notification->PUSHED_BY }}</td>
                                <td>
                                    <a href="#" class="action-icon font-18 tooltips" data-toggle="modal"
                                        data-target="#pushnotificationinfo"
                                        data-push-notification-id="{{ $notification->PUSH_NOTIFICATION_ID }}"> <i
                                            class=" far fa-eye"></i><span class="tooltiptext">View Details</span></a>
                                    <a href="{{ route('notification.push-notification.edit', encrypt($notification->PUSH_NOTIFICATION_ID)) }}"
                                        class="action-icon font-18 tooltips" target="_blank">
                                        <i class=" far fa-clone"></i><span class="tooltiptext">Clone</span>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if($notifications->total()>100)
                    <div class="customPaginationRender mt-2" data-form-id="#notificationFilterForm" class="mt-2">
                        <div>More Records</div>
                        <div>
                            {{ $notifications->render("pagination::customPaginationView") }}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <!-- end card body-->
        </div>
        <!-- end card -->
    </div>
    <!-- end col-->
</div>
@endif

<div class="modal fade" id="pushnotificationinfo">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-transparent shadow-none">
            <div class="card">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal">
                            <i class="mdi mdi-close"></i>
                        </a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18">View: <span
                            data-view-column="PUSH_NOTIFICATION_NAME"></span></h5>
                </div>
                <div class="card-body font-14 position-relative">
                    <div id="pushnotificationinfoloader" class="loader d-flex justify-content-center hv_center">
                        <div class="spinner-border" role="status"></div>
                    </div>
                    <div class="mb-2 row">
                        <div class="col-md-6">
                            <strong class="font-15">Name:</strong>
                            <span data-view-column="PUSH_NOTIFICATION_NAME"></span>
                        </div>
                        <div class="col-md-6">
                            <strong class="font-15">Title:</strong>
                            <span data-view-column="PUSH_NOTIFICATION_TITLE"></span>
                        </div>
                    </div>
                    <div class="mb-2">
                        <strong class="font-15">Description:</strong>
                        <span data-view-column="NOTIFICATION_BODY"></span>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div>
                                <strong class="font-15">Redirection:</strong>
                                <span data-view-column="REDIRECTION_TYPE_ID"> </span>
                            </div>
                            <div data-redirect-type="1">
                                <div data-redirect-url>
                                    <strong class="font-15">URL Type:</strong>
                                    <span data-url-type="IS_RELATIVE" class="badge bg-soft-success text-success shadow-none">RELATIVE</span>
                                    <span data-url-type="IS_EXTERNAL" class="badge bg-soft-info text-info shadow-none">EXTERNAL</span>
                                </div>
                                <div>
                                    <strong class="font-15">URL:</strong>
                                    <span data-view-column="REDIRECTION_URL"> </span>
                                </div>
                            </div>
                            <div data-redirect-type="2">
                                <div>
                                    <strong class="font-15">Tournament:</strong>
                                    <span data-view-column="TOURNAMENT_ID"> </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" data-image>
                            <strong class="font-15">Image:</strong>
                            <div data-view-column="IMAGE_PATH"></div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <strong class="font-15">Send To:</strong>
                        <span data-view-column="SENT_TO"> </span>
                    </div>
                    <div class="row">
                        <div class="mb-1 col-md-6">
                            <strong class="font-15">Pushed On:</strong>
                            <span data-view-column="PUSHED_DATE"> </span>
                        </div>
                        <div class="mb-1 col-md-6">
                            <strong class="font-15">Pushed By:</strong>
                            <span data-view-column="PUSHED_BY"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
@endsection

@section('post_js')
<script>
    $('.Redirection').on('change', function () {
        var value =$('.Redirection').val();
        
        if(value == 1){
            $(".urlsection").show();
            $(".tournamentsection").hide();
            $(".urlsection").removeClass('blurClass');
            $('#TOURNAMENT_ID').val('');
        }else if(value == 2){
            $(".tournamentsection").show();
            $(".urlsection").hide();
            $('#REDIRECTION_URL').val('');
        }else if(value == 0){
            $(".tournamentsection").hide();
            $(".urlsection").show();
            $(".urlsection").addClass('blurClass');
            $('#REDIRECTION_URL, #TOURNAMENT_ID').val('');
        }
    });
    
    $('.Redirection').trigger('change');

    $('#pushnotificationinfo').on('show.bs.modal', function(e){
        window.pageData = window.pageData || {};
        window.pageData.showNotificationDetails = window.pageData.showNotificationDetails || {};
        
        var $relatedTarget = $(e.relatedTarget);
        var $this = $(this);
        $('#pushnotificationinfoloader, #pushnotificationinfoloader div').show().css({'z-index':99});
        $this.find(`[data-view-column]`).html('');
        $this.find(`[data-redirect-type], [data-url-type], [data-image], [data-redirect-url]`).hide();

        var $id = $relatedTarget.data('pushNotificationId');
        var $url = `${window.pageData.baseUrl}/notification/push-notification/${$id}/show`;
        ajaxCall($url, {}, {'refObj':window.pageData.showNotificationDetails, 'method':'GET'})
        .then((response)=>{
            if(response.status==200){
                var $resData = response.data;
                $this.find(`[data-view-column="PUSH_NOTIFICATION_TITLE"]`).text($resData.PUSH_NOTIFICATION_TITLE);
                $this.find(`[data-view-column="PUSH_NOTIFICATION_NAME"]`).text($resData.PUSH_NOTIFICATION_NAME);
                $this.find(`[data-view-column="NOTIFICATION_BODY"]`).text($resData.NOTIFICATION_BODY);
                if($resData.SENT_TO == 1){
                    $resData.segmentDetails.forEach((seg,ind) => {
                        $this.find(`[data-view-column="SENT_TO"]`).append(`${ind==0?'':', '}<a class="text-decoration-underline" href="${window.pageData.baseUrl}/notification/push-notification/exportExcel/${$resData.PUSH_NOTIFICATION_ID}/${seg.SEGMENT_ID}">${seg.SEGMENT_TITLE}</a>`);
                    });
                }else if($resData.SENT_TO == 2){
                    $this.find(`[data-view-column="SENT_TO"]`).html(`<a class="text-decoration-underline" href="${window.pageData.baseUrl}/notification/push-notification/exportExcel/${$resData.PUSH_NOTIFICATION_ID}">Users</a>`);
                }else if($resData.SENT_TO == 0){
                    $this.find(`[data-view-column="SENT_TO"]`).html("Save For Later");
                }

                if($resData.REDIRECTION_TYPE_ID == 2){
                    $this.find(`[data-view-column="REDIRECTION_TYPE_ID"]`).text("Tournament");
                    if($resData.tournamentDetails.length>0){
                        $this.find(`[data-view-column="TOURNAMENT_ID"]`).text($resData.tournamentDetails[0].TOURNAMENT_NAME || "");
                    }
                    $this.find(`[data-redirect-type="2"]`).show();
                }else if($resData.REDIRECTION_TYPE_ID == 1){
                    $this.find(`[data-view-column="REDIRECTION_TYPE_ID"]`).text("Webpage");
                    $this.find(`[data-view-column="REDIRECTION_URL"]`).text($resData.REDIRECTION_URL);
                    if($resData.IS_RELATIVE || $resData.IS_EXTERNAL){
                        $resData.IS_RELATIVE ? $this.find(`[data-url-type="IS_RELATIVE"]`).show() : '';
                        $resData.IS_EXTERNAL ? $this.find(`[data-url-type="IS_EXTERNAL"]`).show() : '';
                        $this.find(`[data-redirect-url]`).show();
                    }
                    $this.find(`[data-redirect-type="1"]`).show();
                }else{
                    $this.find(`[data-view-column="REDIRECTION_TYPE_ID"]`).text("None");
                }

                if($.trim($resData.IMAGE_PATH) !=""){
                    $this.find(`[data-view-column="IMAGE_PATH"]`).html(`<img style="max-width:100%;" src="${$resData.IMAGE_PATH}">`);
                    $this.find(`[data-image]`).show();
                }

                $this.find(`[data-view-column="PUSHED_DATE"]`).text($resData.PUSHED_DATE);
                $this.find(`[data-view-column="PUSHED_BY"]`).text($resData.PUSHED_BY);
                $('#pushnotificationinfoloader, #pushnotificationinfoloader div').hide().css({'z-index':-1});
            }else{
                $.NotificationApp.error("Something went wrong");
                setTimeout(()=>{
                    window.reload();
                },1000);
            }
        });
    });

    /**
     * @param {*} $url link to call ajax
     * @param {*} $data $data is passed as json object or formData 
     * @param { method, refObj } $optional optional argument default value is post 
     */
    function ajaxCall($url, $data={}, $optional = {}) {
        let $method = $optional.method || 'POST';
        let refObj = $optional.refObj || {};
        refObj.requestQueue = refObj.requestQueue || [];
        
        let xhr = $.ajax({
            url: $url,
            type: $method,
            data: $data,
            beforeSend: function(jqXHR, settings) {
                for (var i = 0; i < (refObj.requestQueue.length); i++) {
                    refObj.requestQueue[i].abort();
                }
            }
        }).fail((failCallback, errorStatus, errorMessage) => {
            if (errorStatus != "abort") {
                $.isPlainObject(failCallback.responseJSON) ? console.error(failCallback.responseJSON, errorStatus, errorMessage) : console.error(failCallback.responseText, errorStatus, errorMessage)
            }
        }).always(function(jqXHR, textStatus, errorThrown) {
            refObj.requestQueue.splice(0, refObj.requestQueue.length - 1);
        });
        refObj.requestQueue.push(xhr);

        return xhr;
    }
</script>
@endsection