@extends('bo.layouts.master')

@section('title', "Add Notifications | PB BO")

@section('pre_css')


<style type="text/css">
    .dropdown.bootstrap-select.show .dropdown-menu.show.inner{ min-width: 100% !important; transform: translate(0) !important; max-height: 150px ; width: 100% !important; overflow-x: hidden; overflow-y: scroll !important; }
    .dropdown.bootstrap-select.show .dropdown-menu.show.inner a.dropdown-item{ white-space: normal !important;  }
    .mandetory{ color:red;font-weight: bold; }
</style>

<link href="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item">
                                Notifications
                            </li>
                            <li class="breadcrumb-item ">
                                Push Notifications
                            </li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Push Notifications</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-lg-8">
                                <h5 class="font-18"></h5>
                            </div>
                            <div class="col-lg-4">
                                <div class="text-lg-right mt-3 mt-lg-0">
                                    <a
                                        href="{{route('notification.push-notification.index')}}"
                                        class="btn btn-success waves-effect waves-light"
                                        ><i class="fa fa-bars mr-1"></i> Manage
                                        Notifications</a
                                    >
                                </div>
                            </div>
                        </div>
                        <form id="userSearchForm" method="POST" enctype="multipart/form-data" action="{{route('notification.push-notification.store')}}" method="post">
                            @csrf
                            <input type="hidden" value="0" id="check-for-continue-anyway">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label>Name: <span class="mandetory">*</span></label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="push_notification_name"
                                            id="push_notification_name"
                                            placeholder=""
                                            value="{{ old('push_notification_name',$params['push_notification_name'] ?? '') }}"
                                        />
                                        @error('push_notification_name')
                                            <label id="push_notification_name-error" class="error" for="push_notification_name">
                                                {{ $message }}
                                            </label>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Browse Image: </label>
                                        <input
                                            type="file"
                                            class="form-control"
                                            name="image_path"
                                            id="image_path"
                                            placeholder=""
                                            value="{{ old('image_path',$params['image_path'] ?? '') }}"
                                            accept="image/*"
                                        />
                                        @error('image_path')
                                            <label id="image_path-error" class="error" for="image_path">
                                                {{ $message }}
                                            </label>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label>Title: <span class="mandetory">*</span></label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            name="push_notification_title"
                                            id="push_notification_title"
                                            placeholder=""
                                            value="{{ old('push_notification_title',$params['push_notification_title'] ?? '') }}"
                                        />
                                        @error('push_notification_title')
                                            <label id="push_notification_title-error" class="error" for="push_notification_title">
                                                {{ $message }}
                                            </label>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Redirection: </label>
                                        <select
                                            id="redirection_type_id"
                                            name="redirection_type_id"
                                            class="form-control"

                                        >
                                            <option value="">Select Redirection</option
                                            >
                                            <option value="1" {{ "1" == old('redirection_type_id', $params['redirection_type_id'] ?? '') ? 'selected' : ''}}>Webpage</option>
                                            <option value="2" {{ "2" == old('redirection_type_id', $params['redirection_type_id'] ?? '') ? 'selected' : '' }}
                                                >Tournament</option
                                            >
                                        </select>
                                        @error('redirection_type_id')
                                            <label id="redirection_type_id-error" class="error" for="redirection_type_id">
                                                {{ $message }}
                                            </label>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label>Description: <span class="mandetory">*</span></label>
                                        <textarea
                                            class="form-control"
                                            name="notification_body"
                                            id="notification_body"
                                            rows="5"
                                        >{{ !empty($params)? $params['notification_body']:old('notification_body') }}</textarea>
                                        @error('notification_body')
                                            <label id="notification_body-error" class="error" for="notification_body">
                                                {{ $message }}
                                            </label>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="w-100"
                                                    >Send to:
                                                </label>
                                                <div
                                                    class="radio radio-info form-check-block mb-1"
                                                >
                                                    <input
                                                        type="radio"
                                                        id="sent_to_segment"
                                                        value="1"
                                                        class="tabsonclick"
                                                        data-target-class="selected_"
                                                        name="sent_to"

                                                        {{ !empty($params)? ($params['sent_to']=='1'? 'checked':''):'checked' }}
                                                    />
                                                    <label for="sent_to_segment">
                                                        Segment
                                                    </label>
                                                </div>
                                                <div
                                                    class="radio radio-info form-check-block mb-1"
                                                >
                                                    <input
                                                        type="radio"
                                                        id="sent_to_user"
                                                        class="tabsonclick"
                                                        data-target-class="selected_"
                                                        value="2"
                                                        name="sent_to"
                                                        {{ !empty($params)? ($params['sent_to']=='2'? 'checked':''):'' }}
                                                    />
                                                    <label for="sent_to_user"
                                                        >User
                                                    </label>
                                                </div>
                                                @error('sent_to')
                                                    <label id="sent_to-error" class="error" for="sent_to">
                                                        {{ $message }}
                                                    </label>
                                                @enderror
                                                <div
                                                    class="radio radio-info form-check-block"
                                                >
                                                    <input
                                                        type="radio"
                                                        id="save_for_later"
                                                        class="tabsonclick"
                                                        data-target-class="selected_"
                                                        value="0"
                                                        name="sent_to"
                                                        {{ !empty($params)? ($params['sent_to']=='0'? 'checked':''):'' }}
                                                    />
                                                    <label for="save_for_later"
                                                        >Save for later
                                                    </label>
                                                </div>
                                                @error('sent_to')
                                                    <label id="sent_to-error" class="error" for="sent_to">
                                                        {{ $message }}
                                                    </label>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="boxhideshow selected_1">
                                                <div class="form-group">
                                                    <label>Select Segment: <span class="mandetory">*</span></label>
                                                    <select
                                                        name="segment_ids[]"
                                                        id="segment_ids"
                                                        multiple
                                                        class="form-control selectpicker"
                                                    >
                                                    <option value="" checked>Select Segment</option>
                                                    @if(!empty($segmentTypes))
                                                        @foreach($segmentTypes as $segmentType)
                                                            <option  {{ !empty($params) ? (in_array($segmentType->SEGMENT_ID, $params['segment_ids']) ? 'selected':''):'' }} value="{{$segmentType->SEGMENT_ID}}">{{$segmentType->SEGMENT_TITLE}}</option>
                                                        @endforeach
                                                    @endif
                                                    </select>

                                                    @error('segment_ids')
                                                        <label id="segment_ids-error" class="error" for="segment_ids">
                                                            Please Select Segment Id(s).
                                                        </label>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div
                                                class="boxhideshow selected_2 "
                                            >
                                                <div class="form-group ">
                                                    <label>Upload User: <span class="mandetory">*</span></label>
                                                    <input
                                                        type="file"
                                                        class="form-control"
                                                        name="upload_user"
                                                        id="upload_user"
                                                        placeholder=""
                                                        value=""
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div
                                        class="urlsection"
                                        style="display:none;"
                                    >
                                        <div class="form-group ">
                                            <label>URL: <span class="mandetory">*</span></label>
                                            <input
                                                type="text"
                                                class="form-control"
                                                name="redirection_url"
                                                id="redirection_url"
                                                placeholder=""
                                                value="{{ old('redirection_url',$params['redirection_url'] ?? '') }}"
                                            />
                                            @error('redirection_url')
                                                <label id="redirection_url-error" class="error" for="redirection_url">
                                                    {{ $message }}
                                                </label>
                                            @enderror
                                        </div>
                                    </div>
                                    <div
                                        style="display:none;"
                                        class="tournamentsection"
                                    >
                                        <div class="form-group">
                                            <label>Tournament: <span class="mandetory">*</span></label>
                                            <select
                                                name="tournament_id"
                                                id="tournament_id"
                                                class="form-control"
                                            >
                                                <option
                                                    data-default-selected=""
                                                    value=""
                                                    selected=""
                                                    >Select Tournament</option
                                                >
                                                @if(!empty($tournaments))
                                                    @foreach($tournaments as $tournament)    
                                                    <option {{ $tournament->TOURNAMENT_ID == old('tournament_id' ,$params['tournament_id'] ?? '') ? 'selected' : '' }} value="{{$tournament->TOURNAMENT_ID}}">{{$tournament->TOURNAMENT_NAME}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="col-md-4 urlsection"
                                    style="display:none;"
                                >
                                    <div class="row mb-1">
                                        <div class="col-md-6 mb-1">
                                            <div class="form-group mb-0">
                                                <label>Relative</label>
                                            </div>
                                            <label class="switches">
                                                <input
                                                    name="is_relative"
                                                    type="checkbox"
                                                    {{ "on" == old('is_relative',$params['is_relative'] ?? '') ? 'checked' : '' }}
                                                />
                                                <span
                                                    class="slider round"
                                                ></span>
                                            </label>
                                        </div>
                                        <div class="col-md-6 mb-1">
                                            <div class="form-group mb-0">
                                                <label>External</label>
                                            </div>
                                            <label class="switches">
                                                <input
                                                    name="is_external"
                                                    type="checkbox"
                                                    {{ "on" == old('is_external',$params['is_external'] ?? '') ? 'checked' : '' }}
                                                />
                                                <span
                                                    class="slider round"
                                                ></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button
                                type="submit"
                                id="btnSubmit"
                                class="btn btn-success waves-effect waves-light  mr-1"
                            >
                                <i class="fa fa-save mr-1"></i> Submit
                            </button>
                            <button style="display: none;"  class="btn btn-primary formActionButton mr-1" id="loaderSubmit" type="button" disabled="disabled">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                Loading...
                            </button>
                            <a class="btn btn-secondary waves-effect waves-light text-white" href="{{route('notification.push-notification.create')}}"  >
                                <i class="fas fa-eraser mr-1"></i> Clear
                            </a>
                        </form>
                    </div>
                </div>
                <br /><br /><br />
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="uploaduserexcel">
   <div class="modal-dialog modal-lg">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Upload users</h5>
            </div>
            <div class="card-body">
               <table id="basic-datatable"  class="table dt-responsive nowrap w-100">
                  <thead class="thead-dark">
                     <tr  >
                        <th class="text-white">#</th>
                        <th class="text-white">User ID</th>
                        <th class="text-white">Username</th>
                        <th class="text-white">Review Status</th>
                     </tr>
                  </thead>
                  <tbody id="review-data">
                  </tbody>
               </table>
               <button type="button" id="reuploadExcel" class="btn btn-warning waves-effect waves-light mr-1" data-dismiss="modal"><i class="fa fa-upload mr-1"></i> Reupload</button>
               <button style="display: none;" class="btn btn-primary formActionButton mr-1" id="loaderExcel" type="button" disabled="disabled">
                   <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                   Loading...
               </button>
               <button type="button" id="continue-anyway" class="btn btn-success waves-effect waves-light"><i class="fa fa-save mr-1"></i> Continue Anyway</button>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('js')
<script src="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
@endsection


@section('post_js')
<script type="text/javascript">

    /*
     toggling the elements based on choices.   
    */
    (function($){
        $(document).on('change', `[data-target-class]`,function(){
            var inputValue = $(this).val();
            var targetClass = $(this).data('targetClass');
            $(`.boxhideshow[class*="${targetClass}"]`).hide();
            $(`.boxhideshow.${targetClass}${inputValue}`).show();
        });
        $(`[data-target-class]`).each((i,e)=>{
            if($(e).attr('type','radio')){
                $(e).prop('checked') ? $(e).trigger('change') : '';
            }else{
                $(e).trigger('change');
            }
        });
    }(jQuery));

    $('#redirection_type_id').on('change', function () {
        $(".urlsection").css('display', (this.value == '1') ? 'block' : 'none');
        $(".tournamentsection").css('display', (this.value == '2') ? 'block' : 'none');
    });


    $('#redirection_type_id').trigger('change');
    

    /*
    Validating the form.
    */
    $("#userSearchForm").validate({
        ignore:[],
        rules:{
            push_notification_name:{
                required:true,
                // pattern :   /^[a-zA-Z0-9-_'.$1\n& ]+$/
            },
            push_notification_title:{
                required:true,
                // pattern :   /^[a-zA-Z0-9-_'.$1\n& ]+$/
            },
            notification_body:{
                required:true,
                // pattern :   /^[a-zA-Z0-9-_'.$1\n& ]+$/
            },
            redirection_url:{
                required:{
                    depends:function(element){
                        return $("#redirection_type_id").val() == 1 ? true : false;
                    }
                },
                pattern :   /^[/:a-zA-Z0-9-_'.$1\n& ]+$/
            },
            tournament_id:{
                required:{
                    depends:function(element){
                        return $("#redirection_type_id").val() == 2 ? true : false;
                    }
                }
            },
            upload_user:{
                required:{
                    depends:function(element){
                        return $("input[name='sent_to']:checked").val() == 2 ? true : false;
                    }
                },
                extension:"xlsx"
            },
            'segment_ids[]':{
                required:{
                    depends:function(element){
                        return $("input[name='sent_to']:checked").val() == 1 ? true : false;
                    }
                },
            }
        }
    });

    /*
    Reading Excel Through Excel
    */
    $("#userSearchForm").submit(function(e){
        if($("#userSearchForm").valid()){
            $("#btnSubmit").hide();
            $("#loaderSubmit").show();
            if($("input[name='sent_to']:checked").val()==2 && $("#check-for-continue-anyway").val()==0){
                e.preventDefault();
                var data = new FormData();
                data.append('upload_user', $('#upload_user').get(0).files[0]);
                $.ajax({
                    url: `${window.pageData.baseUrl}/notification/push-notification/review-excel`,
                    type: "post",
                    data: data,
                    enctype: 'multipart/form-data',
                    processData: false, // Important!
                    contentType: false,
                    cache: false,
                    success: function(response) {
                        if(response.status==200){
                            var element ='';
                            $('#basic-datatable tbody').html('');
                            $('#basic-datatable').DataTable().destroy();
                            for(let i=0; i < response.data.length ; i++){
                               
                              let cssClass = '';
                              let txt = '';
                              if(response.flags[i]){
                                cssClass = 'text-success';
                                txt = 'Ok';
                              } 
                              else{
                                 cssClass = 'text-danger';
                                 txt = 'Invalid User';
                              } 

                              element += `<tr class="${cssClass}">
                                <td>${(i+1)}</td>
                                <td>${response.user_ids[i]}</td>
                                <td>${response.data[i]}</td>
                                <td>${txt}</td>
                             </tr>`;
                            }
                            
                            $("#review-data").html(element);
                            $('#basic-datatable').DataTable({
                                "language": {
                                    "paginate": {
                                        "previous": "<i class='mdi mdi-chevron-left'>",
                                        "next": "<i class='mdi mdi-chevron-right'>"
                                    }
                                },
                                "drawCallback": function() {
                                    $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                                }
                            });
                            
                            $("#uploaduserexcel").modal('show');
                        }
                        else{
                            $.NotificationApp.send("error", "Excel Format Not Match", 'top-right', '#FF9494', 'error');
                            $("#btnSubmit").show();
                            $("#loaderSubmit").hide();
                        }    
                    }
                });
            }
            else if($("input[name='sent_to']:checked").val()==1){
                e.preventDefault();
                let formData = new FormData();
                formData.append('image_path', $('#image_path').get(0).files[0]);
                formData = getFormData(formData);
                submitFrom(formData);
            }
            else{
                e.preventDefault();
                let formData = new FormData();
                formData.append('image_path', $('#image_path').get(0).files[0]);
                formData.append('upload_user', $('#upload_user').get(0).files[0]);

                formData = getFormData(formData);
                submitFrom(formData);
            } 
        }
    });

    $("#continue-anyway").click(function (){
        $("#continue-anyway").hide();
        $("#check-for-continue-anyway").val(1);
        $("#userSearchForm").submit();
        $("#loaderExcel").show();
        $("#check-for-continue-anyway").val(0);
    });

    $('#uploaduserexcel').on('hidden.bs.modal', function (e) {
        $("#btnSubmit").show();
        $("#loaderSubmit").hide();
        $('#continue-anyway').show();
        $('#loaderExcel').hide();
    })

    function getFormData(formObj){
        let formData = formObj;
        formData.append('push_notification_name',btoa($('input[name="push_notification_name"]').val()));
        formData.append('push_notification_title',btoa($('input[name="push_notification_title"]').val()));
        formData.append('notification_body',btoa($('textarea[name="notification_body"]').val()));
        formData.append('redirection_type_id',$('select[name="redirection_type_id"]').val());
        formData.append('redirection_url',$('input[name="redirection_url"]').val());
        formData.append('tournament_id',$('select[name="tournament_id"]').val());
        formData.append('is_relative',$('input[name="is_relative"]:checked').val());
        formData.append('is_external',$('input[name="is_external"]:checked').val());
        formData.append('sent_to',$("input[name='sent_to']:checked").val());
        formData.append('segment_ids',$('select[name="segment_ids[]"]').val());
        
        return formData;
    }

    function submitFrom(formData){
        $.ajax({
            url: `${window.pageData.baseUrl}/notification/push-notification/store`,
            type: "POST",
            data: formData,                    
            enctype: 'multipart/form-data',
            processData: false, // Important!
            contentType: false,
            cache: false,
            success: function(response) {
                if(response.status==200){
                    $.NotificationApp.send(response.type, response.msg, 'top-right', '#FF9494', response.type);
                    $("#btnSubmit").show();
                    $("#loaderSubmit").hide();
                    
                    setTimeout(function(){
                        window.location.href = `${window.pageData.baseUrl}/notification/push-notification`;
                    },3000)
                    
                }

            }
        });
    }

</script>
@endsection