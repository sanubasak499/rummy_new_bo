@extends('bo.layouts.master')

@section('title', "Manage Tourneys | PB BO")

@section('pre_css')

@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a>Tournament</a></li>
                    <li class="breadcrumb-item"><a>Manage Tourneys</a></li>
                    <li class="breadcrumb-item active">{{ ucwords($tournament->TOURNAMENT_NAME) }}</li>
                </ol>
            </div>
            <h4 class="page-title">{{ ucwords($tournament->TOURNAMENT_NAME) }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row mb-1">
                    <div class="col-sm-4 mb-1 "><strong>Game Type:</strong> {{ $tournament->MINIGAMES_TYPE_NAME }} </div>
                    <div class="col-sm-4 mb-1"><strong>Display Tab:</strong> {{ $tournament->DISPLAY_TAB }} </div>
                    <div class="col-sm-4 mb-1"><strong>Buy In + Fees:</strong> {{ $tournament->BUYIN }}+{{$tournament->ENTRY_FEE}} </div>
                </div>
                <div class="row mb-1"><td></td>
                    <div class="col-sm-4 mb-1 "><strong>Start Date & Time:</strong> {{ $tournament->TOURNAMENT_START_TIME }}</div>
                    <div class="col-sm-4 mb-1"><strong>End Date & Time:</strong> {{ $tournament->TOURNAMENT_END_TIME }}</div>
                    <div class="col-sm-4 mb-1 "><strong>Tournament Type:</strong> {{ $tournament->TOURNAMENT_TYPE_DESCRIPTION }} </div>
                </div>
                <div class="row mb-1"><td></td>
                    <div class="col-sm-4 mb-1"><strong>Total Rubuy Count:</strong> {{ $tournament->TOTAL_REBUY_COUNT }}</div>
                    <div class="col-sm-4 mb-1"><strong>Total Addon Count:</strong> {{ $tournament->TOTAL_ADDON_COUNT }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><strong>Description:</strong> {{ $tournament->TOURNAMENT_DESC }}</div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-sm-4">
                        <h5 class="font-18">Registrations </h5>
                    </div>
                    <div class="col-sm-8">
                        <div class="text-sm-right">
                            <a href="{{ route('tournaments.manage-tourneys.userRegisteredExport', encrypt($tournament->TOURNAMENT_ID)) }}" class="btn btn-light mb-1">
                                <i class="fas fa-file-excel" style="color: #34a853;"></i> Export Excel
                            </a>
                        </div>
                    </div>
                    <!-- end col-->
                </div>
                <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>User ID</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Source</th>
                            <th>Reason</th>
                            <th>REBUY</th>
                            <th>ADDON</th>
                            <th>Registered On</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $totalCount = $tournament_reg_users->total();
                            $perPage = $tournament_reg_users->perPage();    
                            if($totalCount>$perPage){
                                $indexMultiplier =  ($tournament_reg_users->currentPage() - 1) * $perPage;
                            }else {
                                $indexMultiplier = 0;
                            }
                        @endphp
                        @foreach ($tournament_reg_users as $key => $tournament_reg_user)    
                        <tr>
                            <td>{{ $indexMultiplier + $key + 1 }}</td>
                            <td>{{ $tournament_reg_user->USER_ID }}</td>
                            <td>{{ $tournament_reg_user->PLAYER_NAME }}</td>
                            <td>{{ $tournament_reg_user->EMAIL_ID }}</td>
                            <td>{{ $tournament_reg_user->CONTACT }}</td>
                            <td>{{ $tournament_reg_user->TICKET_SOURCE ?? "" }}</td>
                            <td>{{ $tournament_reg_user->TICKET_REASON ?? "" }}</td>
                            <td>{{ $tournament_reg_user->total_rebuy_user ?? "" }}</td>
                            <td>{{ $tournament_reg_user->total_addon_user ?? "" }}</td>
                            <td>{{ $tournament_reg_user->REGISTERED_DATE }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @if($totalCount>$perPage)
                <div class="mt-2" class="mt-2" style="display:flex; justify-content: space-between;">
                    <div class="pt-2">More Records</div>
                    <div>
                        {{ $tournament_reg_users->render("pagination::customPaginationView") }}
                    </div>
                </div>
                @endif
            </div>
        </div>
        <!-- end card body-->
    </div>
    <!-- end card -->
</div>
@endsection

@section('post_js')

@endsection