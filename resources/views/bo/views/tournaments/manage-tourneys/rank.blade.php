@extends('bo.layouts.master')

@section('title', "Manage Tourneys | PB BO")

@section('pre_css')

@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a>Tournament</a></li>
                    <li class="breadcrumb-item"><a>Manage Tourneys</a></li>
                    <li class="breadcrumb-item active">Rank</li>
                </ol>
            </div>
            <h4 class="page-title">Rank</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row mb-1">
                    <div class="col-sm-4 mb-1 "><strong>Tournament Name:</strong> {{ $tournament->TOURNAMENT_NAME }}
                    </div>
                    <div class="col-sm-4 mb-1"><strong>Game Type:</strong> {{ $tournament->MINIGAMES_TYPE_NAME }}</div>
                    <div class="col-sm-4 mb-1"><strong>Tourney Type:</strong>
                        {{ $tournament->TOURNAMENT_TYPE_DESCRIPTION }}</div>
                </div>
                <div class="row mb-1">
                    <div class="col-sm-4 mb-1 "><strong>Start Date &
                            Time:</strong> {{ $tournament->TOURNAMENT_START_TIME }}</div>
                    <div class="col-sm-4 mb-1"><strong>End Date & Time:</strong> {{ $tournament->TOURNAMENT_END_TIME }}
                    </div>

                    <div class="col-sm-4"><strong>Winners: </strong> {{ $tournament->NO_OF_WINNERS }}</div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-sm-4">
                        <h5 class="font-18">Ranking List </h5>
                    </div>
                    <div class="col-sm-8">
                        <div class="text-sm-right">
                            <a href="{{ route('tournaments.manage-tourneys.rankExport', encrypt($tournament->TOURNAMENT_ID)) }}"
                                class="btn btn-light mb-1">
                                <i class="fas fa-file-excel" style="color: #34a853;"></i> Export Excel
                            </a>
                        </div>
                    </div>
                    <!-- end col-->
                </div>
                <table class="basic-datatable table dt-responsive nowrap w-100">
                    <thead>
                        <tr>
                            <th>Place</th>
                            <th>User ID</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Prize Amount</th>
                            <th>Percentage</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($ranks as $rank)
                        <tr>
                            <td>{{ $rank->RANK }}</td>
                            <td>{{ $rank->USER_ID }}</td>
                            <td>{{ $rank->USERNAME }}</td>
                            <td>{{ $rank->EMAIL_ID }}</td>
                            <td>{{ $rank->CONTACT }}</td>
                            <td>{{ $rank->PRIZE_VALUE ?? 0 }}</td>
                            <td>{{ $rank->WINNER_PERCENTAGE ?? 0 }}%</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @if($ranks->total()>$ranks->perPage())
                <div class="mt-2" class="mt-2" style="display:flex; justify-content: space-between;">
                    <div class="pt-2">More Records</div>
                    <div>
                        {{ $ranks->render("pagination::customPaginationView") }}
                    </div>
                </div>
                @endif
            </div>
        </div>
        <!-- end card body-->
    </div>
    <!-- end card -->
</div>
@endsection

@section('post_js')

@endsection