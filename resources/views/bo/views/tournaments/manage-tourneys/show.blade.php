<div class="card">
    <div class="card-header bg-dark py-2 text-white">
        <div class="card-widgets">
            <a href="#" data-dismiss="modal">
                <i class="mdi mdi-close"></i>
            </a>
        </div>
        <h5 class="card-title mb-0 text-white">
            <span data-view-column="TOURNAMENT_NAME"> {{ $tournament->TOURNAMENT_NAME }}
                ({{ $tournament->TOURNAMENT_ID }})</span>
        </h5>
    </div>
    {{-- Information Tab --}}
    <div class="card-body position-relative">
        <div class="row mb-1">
            <div class="col-sm-3 mb-1"><strong>Game Type:</strong>
                <span data-view-column="MINIGAMES_TYPE_NAME">{{ $tournament->MINIGAMES_TYPE_NAME }}</span>
            </div>
            <div class="col-sm-3 mb-1"><strong>Display Tab:</strong>
                <span data-view-column="DISPLAY_TAB">{{ $tournament->DISPLAY_TAB }}</span>
            </div>
            <div class="col-sm-3 mb-1"><strong>Server:</strong>
                <span data-view-column="SERVER_NAME">{{ $tournament->SERVER_NAME }}</span>
            </div>
            <div class="col-sm-3 mb-1"><strong>Limit:</strong>
                <span data-view-column="TOURNAMENT_LIMIT_NAME">{{ $tournament->TOURNAMENT_LIMIT }}
                    ({{ $tournament->TOURNAMENT_LIMIT_NAME }})</span>
            </div>
        </div>
        <div class="row mb-1">

            <div class="col-sm-3 mb-1"><strong>Tournament Type:</strong>
                <span
                    data-view-column="TOURNAMENT_TYPE_DESCRIPTION">{{ $tournament->TOURNAMENT_TYPE_DESCRIPTION }}</span>
            </div>
            <div class="col-sm-3 mb-1"><strong>Tournament Sub Type:</strong>
                <span data-view-column="TOURNAMENT_SUB_TYPE_DESCRIPTION">
                    @if($tournament->TOURNAMENT_SUB_TYPE_ID == 1)
                    Normal
                    @elseif($tournament->TOURNAMENT_SUB_TYPE_ID == 2)
                    Satellite
                    @elseif($tournament->TOURNAMENT_SUB_TYPE_ID == 3)
                    Multi-Day Child
                    @elseif($tournament->TOURNAMENT_SUB_TYPE_ID == 4)
                    Timer
                    @endif
                </span>
            </div>
            @if($tournament->TOURNAMENT_SUB_TYPE_ID == 2 || $tournament->TOURNAMENT_SUB_TYPE_ID == 3)
            <div class="col-sm-3 mb-1"><strong>Parent Tournament:</strong>
                <span
                    data-view-column="PARENT_TOURNAMENT">{{ $tournament->PARENT_TOURNAMENT  }}({{ $tournament->PARENT_TOURNAMENT_ID }})</span>
            </div>
            @endif
        </div>
        <div class="row mb-1">
            <div class="col-sm-3 mb-1"><strong>Win The Button:</strong>
                <span data-view-column="WIN_THE_BUTTON">{{ $tournament->WIN_THE_BUTTON ? "Yes" : "No" }}</span>
            </div>
            <div class="col-sm-3 mb-1"><strong>Stop on final table:</strong>
                <span data-view-column="FINAL_TABLE_STOP">{{ $tournament->FINAL_TABLE_STOP ? "Yes" : "No" }}</span>
            </div>
            <div class="col-sm-3 mb-1"><strong>Featured:</strong>
                <span data-view-column="FEATURED">{{ $tournament->FEATURED ? "Yes" : "No" }}</span>
            </div>
            <div class="col-sm-3 mb-1"><strong>Private Table:</strong>
                <span data-view-column="PRIVATE_TABLE">{{ $tournament->PRIVATE_TABLE ? "Yes" : "No" }}</span>
            </div>
        </div>
        <div class="row ">
            <div class="col-sm-12"><strong>Description</strong>
                <span data-view-column="TOURNAMENT_DESC">{{ $tournament->TOURNAMENT_DESC }}</span>
            </div>
        </div>
    </div>
    {{-- EO: Information Tab --}}
</div>
<div class="row">
    {{-- Timings --}}
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-secondary  py-2 text-white">
                <h5 class="card-title mb-0 text-white">Timings</h5>
            </div>
            <div class="card-body position-relative">
                @if($tournament->TOURNAMENT_TYPE_ID != 1)
                <div class="row mb-1">
                    <div class="col-sm-6 mb-1"><strong>Registeration Start Time:</strong>
                        <span data-view-column="REGISTER_START_TIME">{{ $tournament->REGISTER_START_TIME }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Registeration End Time:</strong>
                        <span data-view-column="REGISTER_END_TIME">{{ $tournament->REGISTER_END_TIME }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Tournament Start Time:</strong>
                        <span data-view-column="TOURNAMENT_START_TIME">{{ $tournament->TOURNAMENT_START_TIME }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Tournament End Time:</strong>
                        <span data-view-column="TOURNAMENT_END_TIME">{{ $tournament->TOURNAMENT_END_TIME }}</span>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-sm-6 mb-1"><strong>Late Registration:</strong>
                        <span
                            data-view-column="LATE_REGISTRATION_ALLOW">{{ $tournament->LATE_REGISTRATION_ALLOW ? "Yes" : "No" }}</span>
                    </div>
                    @if($tournament->LATE_REGISTRATION_ALLOW)
                    <div class="col-sm-6 mb-1"><strong>Late Registration End Time:</strong>
                        <span
                            data-view-column="LATE_REGISTRATION_END_TIME">{{ $tournament->LATE_REGISTRATION_END_TIME }}</span>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    {{-- EO: Timings --}}

    {{-- Player Infomation --}}
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-secondary  py-2 text-white">
                <h5 class="card-title mb-0 text-white">Players</h5>
            </div>
            <div class="card-body position-relative">
                <div class="row">
                    <div class="col-sm-6 mb-1"><strong>Player Per table:</strong>
                        <span data-view-column="PLAYER_PER_TABLE">{{ $tournament->PLAYER_PER_TABLE }}</span>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-sm-6 mb-1"><strong>Minimum Player:</strong>
                        <span data-view-column="T_MIN_PLAYERS">{{ $tournament->T_MIN_PLAYERS }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Maximum Player:</strong>
                        <span data-view-column="T_MAX_PLAYERS">{{ $tournament->T_MAX_PLAYERS }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- EO: Player Infomation --}}

    @if($tournament->TOURNAMENT_TYPE_ID != 9)
    {{-- Entry Criteria --}}
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-secondary  py-2 text-white">
                <h5 class="card-title mb-0 text-white">Entry Criteria</h5>
            </div>
            <div class="card-body position-relative">
                <div class="row">
                    <div class="col-sm-6 mb-1"><strong>Entry Type:</strong>
                        <span data-view-column="COIN_TYPE_NAME">{{ $tournament->COIN_TYPE_NAME }}</span>
                    </div>
                    @if($tournament->COIN_TYPE_ID != 8)
                    <div class="col-sm-6 mb-1"><strong>Balance Type:</strong>
                        <span data-view-column="PROMO_BALANCE_ALLOW">
                            {{ $tournament->DEPOSIT_BALANCE_ALLOW ? "Deposit, " : "" }}
                            {{ $tournament->PROMO_BALANCE_ALLOW ? "Promo, " : "" }}
                            {{ $tournament->WIN_BALANCE_ALLOW ? "Win" : "" }}
                        </span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Amount:</strong>
                        <span data-view-column="BUYIN">{{ $tournament->BUYIN }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Fee (Abs. Value):</strong>
                        <span data-view-column="ENTRY_FEE">{{ $tournament->ENTRY_FEE }}</span>
                    </div>

                    @if($tournament->TOURNAMENT_TYPE_ID == 10 || $tournament->TOURNAMENT_TYPE_ID == 11)
                    <div class="col-sm-6 mb-1"><strong>Bounty Amount:</strong>
                        <span data-view-column="BOUNTY_AMOUNT">{{ $tournament->BOUNTY_AMOUNT }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Bounty Fee:</strong>
                        <span data-view-column="BOUNTY_ENTRY_FEE">{{ $tournament->BOUNTY_ENTRY_FEE }}</span>
                    </div>
                    @endif

                    @if($tournament->TOURNAMENT_TYPE_ID == 11)
                    <div class="col-sm-6 mb-1"><strong>PKO %:</strong>
                        <span
                            data-view-column="PROGRESSIVE_BOUNTY_PERCENTAGE">{{ $tournament->PROGRESSIVE_BOUNTY_PERCENTAGE }}</span>
                    </div>
                    @endif

                    @endif
                </div>
            </div>
        </div>
    </div>
    {{-- EO: Entry Criteria --}}
    @endif

    {{-- Blind Structure Section --}}
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-secondary  py-2 text-white">
                <h5 class="card-title mb-0 text-white">Blind Structure</h5>
            </div>
            <div class="card-body position-relative">
                <div class="row">
                    <div class="col-sm-6 mb-1"><strong>Blind Structure:</strong>
                        <span
                            data-view-column="TOURNAMENT_BLIND_STRUCTURE_NAME">{{ $tournament->TOURNAMENT_BLIND_STRUCTURE_NAME }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Starting Blinds:</strong>
                        <span
                            data-view-column="TOURNAMENT_LEVEL">{{ $tournament->TOURNAMENT_LEVEL.' - '.$tournament->SMALL_BLIND.'/' .$tournament->BIG_BLIND }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Binds Increment Time (min):</strong>
                        <span data-view-column="LEVEL_PERIOD">{{ $tournament->LEVEL_PERIOD }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong> Chips Count:</strong>
                        <span data-view-column="TOURNAMENT_CHIPS">{{ $tournament->TOURNAMENT_CHIPS }}</span>
                    </div>
                    @if($tournament->TOURNAMENT_SUB_TYPE_ID == 4)
                    <div class="col-sm-6 mb-1"><strong>Timer Tournament End Level:</strong>
                        <span
                            data-view-column="TIMER_TOURNAMENT_END_TIME">{{ $tournament->TIMER_TOURNAMENT_END_TIME }}</span>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    {{-- EO: Blind Structure Section --}}

    {{-- Time Settings Section --}}
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-secondary  py-2 text-white">
                <h5 class="card-title mb-0 text-white">Time Settings</h5>
            </div>
            <div class="card-body position-relative">
                <div class="row">
                    <div class="col-sm-6 mb-1"><strong>Turn Time (sec):</strong>
                        <span data-view-column="PLAYER_HAND_TIME">{{ $tournament->PLAYER_HAND_TIME }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Disconnect Time (sec):</strong>
                        <span data-view-column="DISCONNECT_TIME">{{ $tournament->DISCONNECT_TIME }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Extra Time (sec):</strong>
                        <span data-view-column="EXTRA_TIME">{{ $tournament->EXTRA_TIME }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Lobby Display Interval Time (Hours):</strong>
                        <span data-view-column="LOBBY_DISPLAY_INTERVAL">{{ $tournament->LOBBY_DISPLAY_INTERVAL }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Extra Time (Max Cap)(Sec):</strong>
                        <span data-view-column="PLAYER_MAX_EXTRATIME">{{ $tournament->PLAYER_MAX_EXTRATIME }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Extra Time (Blind Levels):</strong>
                        <span
                            data-view-column="ADDITIONAL_EXTRATIME_LEVEL_INTERVAL">{{ $tournament->ADDITIONAL_EXTRATIME_LEVEL_INTERVAL }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Extra Time (Add-on) (Sec):</strong>
                        <span data-view-column="ADDITIONAL_EXTRATIME">{{ $tournament->ADDITIONAL_EXTRATIME }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- EO: Time Settings Section --}}

    @if($tournament->IS_REBUY == 1)
    {{-- Re-Buy Settings Section --}}
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-secondary  py-2 text-white">
                <h5 class="card-title mb-0 text-white">Re-Buy Settings</h5>
            </div>
            <div class="card-body position-relative">
                <div class="row">
                    <div class="col-sm-6 mb-1"><strong>Chips to be granted:</strong>
                        <span data-view-column="REBUY_CHIPS">{{ $tournament->REBUY_CHIPS }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Player Max. Eligible Chips:</strong>
                        <span data-view-column="REBUY_ELIGIBLE_CHIPS">{{ $tournament->REBUY_ELIGIBLE_CHIPS }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Time Period (Level):</strong>
                        <span data-view-column="REBUY_END_TIME">{{ $tournament->REBUY_END_TIME }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Num. Of Re-Buy:</strong>
                        <span data-view-column="REBUY_COUNT">{{ $tournament->REBUY_COUNT }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Re-Buy Amount:</strong>
                        <span data-view-column="REBUY_IN">{{ $tournament->REBUY_IN }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Entry Fee:</strong>
                        <span data-view-column="REBUY_ENTRY_FEE">{{ $tournament->REBUY_ENTRY_FEE }}</span>
                    </div>
                    @if($tournament->REBUY_ADDON_RE_ENTRY == 1)
                    <div class="col-sm-6 mb-1"><strong>Double Rebuy:</strong>
                        <span data-view-column="DOUBLE_REBUYIN">{{ $tournament->DOUBLE_REBUYIN ? "Yes" : "No" }}</span>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    {{-- EO: Re-Buy Settings Section --}}

    @if($tournament->IS_ADDON == 1)
    {{-- Addon Settings Section --}}
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-secondary  py-2 text-white">
                <h5 class="card-title mb-0 text-white">Addon Settings</h5>
            </div>
            <div class="card-body position-relative">
                <div class="row">
                    <div class="col-sm-6 mb-1"><strong>Chips to be granted:</strong>
                        <span data-view-column="ADDON_CHIPS">{{ $tournament->ADDON_CHIPS }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Addon Time Interval: (Minutes):</strong>
                        <span data-view-column="ADDON_BREAK_TIME">{{ $tournament->ADDON_BREAK_TIME }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Addon Amount:</strong>
                        <span data-view-column="ADDON_AMOUNT">{{ $tournament->ADDON_AMOUNT }}</span>
                    </div>
                    <div class="col-sm-6 mb-1"><strong>Entry Fee:</strong>
                        <span data-view-column="ADDON_ENTRY_FEE">{{ $tournament->ADDON_ENTRY_FEE }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- EO: Addon Settings Section --}}
    @endif
    @endif

    {{-- Prize Structure Section --}}
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-secondary  py-2 text-white">
                <h5 class="card-title mb-0 text-white">Prize Structure</h5>
            </div>
            <div class="card-body position-relative">
                <div class="row">
                    @if($tournament->TOURNAMENT_SUB_TYPE_ID != 2 && $tournament->TOURNAMENT_SUB_TYPE_ID != 3)
                    <div class="col-sm-6 mb-1"><strong>Prize Structure:</strong>
                        <span data-view-column="PRIZE_STRUCTURE_ID">
                            @if($tournament->PRIZE_STRUCTURE_ID == 1) Custom @endif
                            @if($tournament->PRIZE_STRUCTURE_ID == 2) Default @endif
                            @if($tournament->PRIZE_STRUCTURE_ID == 4) Custom Mix @endif
                        </span>
                    </div>

                    <div class="col-sm-6 mb-1"><strong>Prize Structure Type:</strong>
                        <span
                            data-view-column="PRIZE_STRUCTURE_TYPE_NAME">{{ $tournament->PRIZE_STRUCTURE_TYPE_NAME }}</span>
                    </div>
                    @endif

                    <div class="col-sm-6 mb-1"><strong>Prize Balance Type:</strong>
                        <span data-view-column="PRIZE_BALANCE_TYPE">
                            @if($tournament->PRIZE_BALANCE_TYPE == 3) Win @endif
                            @if($tournament->PRIZE_BALANCE_TYPE == 2) Promo @endif
                        </span>
                    </div>

                    @if($tournament->TOURNAMENT_SUB_TYPE_ID != 2 && $tournament->TOURNAMENT_SUB_TYPE_ID != 3)
                    <div class="col-sm-6 mb-1"><strong>Prize Pool Type:</strong>
                        <span data-view-column="FIXED_PRIZE">
                            {{ $tournament->FIXED_PRIZE == 1 ? "Fixed" : "Dynamic" }}
                        </span>
                    </div>
                    @endif
                </div>

                <div class="row">
                    @if($tournament->TOURNAMENT_SUB_TYPE_ID != 2 && $tournament->TOURNAMENT_SUB_TYPE_ID != 3)
                    <div class="col-sm-6 mb-1"><strong>GTD Prize Pool:</strong>
                        <span data-view-column="GUARENTIED_PRIZE">
                            {{ $tournament->GUARENTIED_PRIZE }}
                        </span>
                    </div>
                    @endif

                    @if($tournament->TOURNAMENT_SUB_TYPE_ID == 2)
                    <div class="col-sm-6 mb-1"><strong>GTD Seats:</strong>
                        <span
                            data-view-column="SATELLITES_GUARANTEED_PLACES_PAID">{{ $tournament->SATELLITES_GUARANTEED_PLACES_PAID }}</span>
                    </div>
                    @endif
                    @if($tournament->TOURNAMENT_SUB_TYPE_ID == 3)
                    <div class="col-sm-6 mb-1"><strong>Qualifying Players: (%):</strong>
                        <span
                            data-view-column="MULTIDAY_PLAYER_PERCENTAGE">{{ $tournament->MULTIDAY_PLAYER_PERCENTAGE }}</span>
                    </div>
                    @endif

                    @if($tournament->TOURNAMENT_SUB_TYPE_ID != 2 && $tournament->TOURNAMENT_SUB_TYPE_ID != 3)
                    @if($tournament->PRIZE_STRUCTURE_ID==1)
                    <div class="col-sm-6 mb-1"><strong>No of places paid:</strong>
                        <span data-view-column="NO_OF_WINNERS_CUSTOM">{{ $tournament->NO_OF_WINNERS_CUSTOM }}</span>
                    </div>

                    @foreach ($tournament->WINNER_PERCENTAGE as $position => $winner)
                    <div><strong>{{ $position }}: </strong> <span data-view-column="WINNER_PERCENTAGE">{{ $winner }}</span>
                    </div>
                    @endforeach
                    @endif

                    @if($tournament->PRIZE_STRUCTURE_ID==4)
                    <div class="col-sm-6 mb-1"><strong>No of places paid:</strong>
                        <span data-view-column="NO_OF_WINNERS_CUSTOM_MIX">{{ $tournament->NO_OF_WINNERS_CUSTOM_MIX }}</span>
                    </div>
                    @foreach ($tournament->CUSTOM_MIX_PRIZE_TYPE as $key => $winner)
                    <div><strong>{{ $key }}: </strong>
                        @if($winner == 8)
                        <span data-view-column="PRIZE_VALUE">
                            Amount: {{ $tournament->PRIZE_VALUE[$key] }}
                        </span>
                        @else
                        <span data-view-column="PRIZE_TOURNAMENT_NAME">
                            Ticket: {{ $tournament->PRIZE_TOURNAMENT_NAME[$key] }}
                        </span>
                        @endif
                    </div>
                    @endforeach
                    @endif

                    @endif
                </div>
            </div>
        </div>
    </div>
    {{-- EO: Prize Structure Section --}}
</div>