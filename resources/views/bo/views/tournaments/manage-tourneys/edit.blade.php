@extends('bo.layouts.master')

@section('title', "Manage Tourneys | PB BO")

@section('pre_css')
<link rel="stylesheet" href="{{ asset('assets/libs/switchery/switchery.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/libs/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('post_css')
<link rel="stylesheet" href="{{ asset('assets/bo/pages/tournament/tournament.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item">Tournaments</li>
                    <li href="{{ route('tournaments.manage-tourneys.index') }}" class="breadcrumb-item">Manage Tourneys
                    </li>
                    <li class="breadcrumb-item active">Edit Tourneys</li>
                </ol>
            </div>
            <h4 class="page-title">Edit Tourneys </h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-lg-4">
                <h5 class="font-18">Edit Tourneys </h5>
            </div>
            <div class="col-lg-8">
                <div class="float-lg-right d-flex">
                    <div class="dropdown show">
                        <a class="btn btn-light waves-effect waves-light dropdown-toggle mr-2"
                            style="white-space:unset;" href="#" role="button" id="selectTemplateBtn"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                class="fas fa-align-left mr-1"></i> Select Template </a>
                        <div id="templatesList" class="dropdown-menu" aria-labelledby="selectTemplateBtn">
                            @forelse ($templates as $template)
                            <a class="dropdown-item d-flex justify-content-between"
                                href="{{ route('tournaments.manage-tourneys.create', encrypt($template->template_id)) }}">
                                {{ ucfirst($template->template_name) }}
                                <span class="deleteTemplete ml-2"
                                    data-template-id="{{ encrypt($template->template_id) }}">
                                    <i class="fa fa-trash" data-template-id="{{ encrypt($template->template_id) }}"></i>
                                </span>
                            </a>
                            @empty
                            <a class="dropdown-item" href="javascript:;">Templates Not Found</a>
                            @endforelse
                        </div>
                    </div>

                    <div class="text-lg-right mt-lg-0">
                        <a href="{{ route('tournaments.manage-tourneys.create') }}"
                            class="btn btn-success waves-effect waves-light mr-2"><i class="fa fa-plus mr-1"></i> Add
                            Tournament </a>
                    </div>

                    <div class="text-lg-right mt-lg-0">
                        <a href="{{ route('tournaments.manage-tourneys.index') }}"
                            class="btn btn-dark waves-effect waves-light"><i class="fa fa-list mr-1"></i> View Tourneys
                            List</a>
                    </div>
                </div>
            </div>
        </div>
        <form id="DeleteTemplateForm" action="" method="post">
            {{ csrf_field() }}
            @method('delete')
        </form>

        <form id="editTournamentForm" method="POST"
            action="{{ route('tournaments.manage-tourneys.update', encrypt($tournament->TOURNAMENT_ID)) }}">
            {{ csrf_field() }}
            @method('put')
            <div id="editTournamentWizard" class="wizardStyle">
                <ul class="nav nav-pills bg-light nav-justified form-wizard-header m-0 mb-3">
                    <li class="nav-item" data-wizard-index="0">
                        <a href="#TournamentInformationSection" data-toggle="tab"
                            class="nav-link rounded-0 pt-2 pb-2 active">
                            <i class="mdi mdi-gamepad-variant mr-1"></i>
                            <span class="d-none d-sm-inline">Tournament Information</span>
                        </a>
                    </li>
                    <li class="nav-item" data-wizard-index="1">
                        <a href="#TimingSection" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                            <i class="mdi mdi-av-timer   mr-1"></i>
                            <span class="d-none d-sm-inline">Timings <br />...</span>
                        </a>
                    </li>
                    <li class="nav-item" data-wizard-index="2">
                        <a href="#PlayerSection" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                            <i class="fa fa-user mr-1"></i>
                            <span class="d-none d-sm-inline">Players <br />...</span>
                        </a>
                    </li>
                    <li class="nav-item" data-wizard-index="3">
                        <a href="#EntryCriteriaSection" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                            <i class="mdi mdi-progress-check  mr-1"></i>
                            <span class="d-none d-sm-inline">Entry Criteria<br />...</span>
                        </a>
                    </li>
                    <li class="nav-item" data-wizard-index="4">
                        <a href="#BlindStructureSection" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                            <i class="mdi mdi-eye-off-outline  mr-1"></i>
                            <span class="d-none d-sm-inline">Blind Structure <br />...</span>
                        </a>
                    </li>
                    <li class="nav-item" data-wizard-index="5">
                        <a href="#TimeSettingsSection" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                            <i class="mdi mdi-timer  mr-1"></i>
                            <span class="d-none d-sm-inline">Time Settings<br />...</span>
                        </a>
                    </li>
                    <li class="nav-item" data-wizard-index="6">
                        <a href="#RebuyAddonReEntrySection" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                            <i class="mdi mdi-cards  mr-1"></i>
                            <span class="d-none d-sm-inline">Rebuy/Addon/<br>Re-Entry </span>
                        </a>
                    </li>
                    <li class="nav-item" data-wizard-index="7">
                        <a href="#PrizeStructureSection" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                            <i class="ti-gift mr-1"></i>
                            <span class="d-none d-sm-inline"> Prize Structure <br>...</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content b-0 mb-0 p-0">
                    {{-- PROGRESS BAR --}}
                    <div id="bar" class="progress mb-3" style="height: 7px;">
                        <div class="bar progress-bar progress-bar-striped progress-bar-animated bg-success"></div>
                    </div>
                    {{-- ./ PROGRESS BAR --}}

                    {{-- Tournament Information Section --}}
                    <div class="tab-pane active" id="TournamentInformationSection" data-wizard-index="0">
                        <h5 class="font-17">Tournament Information </h5>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label class="mandatory">Name</label>
                                <input type="text" name="TOURNAMENT_NAME"
                                    value="{{ old('TOURNAMENT_NAME', $tournament->TOURNAMENT_NAME ?? '') }}"
                                    id="TOURNAMENT_NAME"
                                    class="form-control @error('TOURNAMENT_NAME') is-invalid @enderror">
                                @error('TOURNAMENT_NAME') <label id="TOURNAMENT_NAME-error" class="invalid-feedback"
                                    for="TOURNAMENT_NAME">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label class="mandatory">Game Type</label>
                                <input type="hidden" name="MINI_GAME_TYPE_ID"
                                    value="{{ $tournament->MINI_GAME_TYPE_ID }}">
                                <select class="form-control @error('MINI_GAME_TYPE_ID') is-invalid @enderror"
                                    id="MINI_GAME_TYPE_ID" disabled>
                                    <option value="1"
                                        {{ '1'==old('MINI_GAME_TYPE_ID', $tournament->MINI_GAME_TYPE_ID ?? '1') ? 'selected' : '' }}>
                                        Texas Hold'em</option>
                                    <option value="2"
                                        {{ '2'==old('MINI_GAME_TYPE_ID', $tournament->MINI_GAME_TYPE_ID ?? "") ? 'selected' : '' }}>
                                        Omaha</option>
                                    <option value="17"
                                        {{ '17'==old('MINI_GAME_TYPE_ID', $tournament->MINI_GAME_TYPE_ID ?? "") ? 'selected' : '' }}>
                                        5 Card Omaha</option>
                                </select>
                                @error('MINI_GAME_TYPE_ID') <label id="MINI_GAME_TYPE_ID-error" class="invalid-feedback"
                                    for="MINI_GAME_TYPE_ID">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label class="mandatory">Server (Requires Restart)</label>
                                @if($tournament->TOURNAMENT_STATUS != 0)
                                <input type="hidden" name="SERVER_ID" value="{{ $tournament->SERVER_ID }}">
                                @endif
                                <select class="form-control @error('SERVER_ID') is-invalid @enderror" id="SERVER_ID" {!!
                                    $tournament->TOURNAMENT_STATUS == 0 ? 'name="SERVER_ID"' : 'disabled'
                                    !!}>
                                    <option value=""
                                        {{ ''==old('SERVER_ID', $tournament->SERVER_ID ?? "") ? 'selected' : '' }}>
                                        Select Server</option>
                                    @foreach($servers as $server)
                                    <option value="{{ $server->SERVER_ID }}"
                                        {{ $server->SERVER_ID==old('MINI_GAME_TYPE_ID', $tournament->SERVER_ID ?? "") ? 'selected' : '' }}>
                                        {{ $server->SERVER_NAME }}</option>
                                    @endforeach
                                </select>
                                @error('SERVER_ID') <label id="SERVER_ID-error" class="invalid-feedback"
                                    for="SERVER_ID">{{ $message }}</label> @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="mandatory">Tournament Description</label>
                            <textarea class="form-control @error('TOURNAMENT_DESC') is-invalid @enderror"
                                name="TOURNAMENT_DESC" id="TOURNAMENT_DESC"
                                rows="2">{{ old('TOURNAMENT_DESC', $tournament->TOURNAMENT_DESC ?? '') }}</textarea>
                            @error('TOURNAMENT_DESC') <label id="TOURNAMENT_DESC-error" class="invalid-feedback"
                                for="TOURNAMENT_DESC">{{ $message }}</label> @enderror
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label class="mandatory">Display Tab</label>
                                @if($tournament->TOURNAMENT_STATUS != 0)
                                <input type="hidden" name="CATEGORY" value="{{ $tournament->CATEGORY }}">
                                @endif
                                <select class="form-control categoryEvent @error('CATEGORY') is-invalid @enderror"
                                    id="CATEGORY" {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="CATEGORY"' :
                                    'disabled'
                                    !!}>
                                    @foreach($categories as $category)
                                    <option value="{{ $category->PK_TOURNAMENT_SUBTAB_CATEGORY_ID }}"
                                        {{ $category->PK_TOURNAMENT_SUBTAB_CATEGORY_ID==old('CATEGORY',$tournament->CATEGORY ?? '1') ? 'selected' : '' }}>
                                        {{ $category->CATEGORY_DESC }}</option>
                                    @endforeach
                                    <option value="99" {{ '99'==old('CATEGORY') ? 'selected' : '' }}>Freeroll</option>
                                </select>
                                @error('CATEGORY') <label id="CATEGORY-error" class="invalid-feedback"
                                    for="CATEGORY">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label class="mandatory">Limit (Requires Restart)</label>

                                <input type="hidden" name="TOURNAMENT_LIMIT_ID"
                                    value="{{ $tournament->TOURNAMENT_LIMIT_ID }}">
                                <select class="form-control @error('TOURNAMENT_LIMIT_ID') is-invalid @enderror"
                                    id="TOURNAMENT_LIMIT_ID" disabled>
                                    @foreach($tournamentLimits as $limit)
                                    <option value="{{ $limit->TOURNAMENT_LIMIT_ID }}"
                                        {{ $limit->TOURNAMENT_LIMIT_ID==old('TOURNAMENT_LIMIT_ID',$tournament->TOURNAMENT_LIMIT_ID ?? '') ? 'selected' : '' }}>
                                        {{ $limit->DESCRIPTION }}</option>
                                    @endforeach
                                </select>
                                @error('TOURNAMENT_LIMIT_ID') <label id="TOURNAMENT_LIMIT_ID-error"
                                    class="invalid-feedback" for="TOURNAMENT_LIMIT_ID">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label>Win The Button (Requires Restart)</label>
                                @if($tournament->TOURNAMENT_STATUS != 0)
                                <input type="hidden" name="WIN_THE_BUTTON" value="{{ $tournament->WIN_THE_BUTTON }}">
                                @endif
                                <select class="form-control @error('WIN_THE_BUTTON') is-invalid @enderror"
                                    id="WIN_THE_BUTTON" {!! $tournament->TOURNAMENT_STATUS == 0 ?
                                    'name="WIN_THE_BUTTON"' :
                                    'disabled'
                                    !!}>
                                    <option value="0"
                                        {{ "0"==old('WIN_THE_BUTTON', $tournament->WIN_THE_BUTTON ?? "0") ? 'selected' : '' }}>
                                        No</option>
                                    <option value="1"
                                        {{ "1"==old('WIN_THE_BUTTON', $tournament->WIN_THE_BUTTON ?? "0") ? 'selected' : '' }}>
                                        Yes</option>
                                </select>
                                @error('WIN_THE_BUTTON') <label id="WIN_THE_BUTTON-error" class="invalid-feedback"
                                    for="WIN_THE_BUTTON">{{ $message }}</label> @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label class="mandatory">Tournament Type</label>
                                <input type="hidden" name="TOURNAMENT_TYPE_ID"
                                    value="{{ $tournament->TOURNAMENT_TYPE_ID }}">
                                <select
                                    class="form-control tournamentTypeIdEvent @error('TOURNAMENT_TYPE_ID') is-invalid @enderror"
                                    id="TOURNAMENT_TYPE_ID" disabled>
                                    <option value="4"
                                        {{ "4"==old('TOURNAMENT_TYPE_ID', $tournament->TOURNAMENT_TYPE_ID ?? "4") ? 'selected' : '' }}>
                                        Normal</option>
                                    <option value="9" {{ "9"==old('TOURNAMENT_TYPE_ID') ? 'selected' : '' }}>Multi-Day
                                        Main</option>
                                    <option value="10" {{ "10"==old('TOURNAMENT_TYPE_ID') ? 'selected' : '' }}>Normal
                                        Bounty</option>
                                    <option value="11" {{ "11"==old('TOURNAMENT_TYPE_ID') ? 'selected' : '' }}>
                                        Progressive Bounty</option>
                                    <option value="12" {{ "12"==old('TOURNAMENT_TYPE_ID') ? 'selected' : '' }}>Offline
                                    </option>
                                    <option value="1" {{ "1"==old('TOURNAMENT_TYPE_ID') ? 'selected' : '' }}>Sit & Go
                                    </option>
                                </select>
                                @error('TOURNAMENT_TYPE_ID') <label id="TOURNAMENT_TYPE_ID-error"
                                    class="invalid-feedback" for="TOURNAMENT_TYPE_ID">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-4 tournamentTypeIdCommon" id="tournamentSubTypeIdWrapper">
                                <label class="mandatory">Tournament Sub Type:</label>
                                <input type="hidden" name="TOURNAMENT_SUB_TYPE_ID"
                                    value="{{ $tournament->TOURNAMENT_SUB_TYPE_ID }}">
                                <select
                                    class="form-control TournamentSubTypeIdEvent @error('TOURNAMENT_SUB_TYPE_ID') is-invalid @enderror"
                                    id="TOURNAMENT_SUB_TYPE_ID" disabled>
                                    <option value="1"
                                        {{ "1"==old('TOURNAMENT_SUB_TYPE_ID', $tournament->TOURNAMENT_SUB_TYPE_ID ?? 1) ? 'selected' : '' }}>
                                        Normal</option>
                                    <option value="2"
                                        {{ "2"==old('TOURNAMENT_SUB_TYPE_ID', $tournament->TOURNAMENT_SUB_TYPE_ID ?? "") ? 'selected' : '' }}>
                                        Satellite</option>
                                    <option value="3"
                                        {{ "3"==old('TOURNAMENT_SUB_TYPE_ID', $tournament->TOURNAMENT_SUB_TYPE_ID ?? "") ? 'selected' : '' }}>
                                        Multi-Day Child</option>
                                    <option value="4"
                                        {{ "4"==old('TOURNAMENT_SUB_TYPE_ID', $tournament->TOURNAMENT_SUB_TYPE_ID ?? "") ? 'selected' : '' }}>
                                        Timer</option>
                                </select>
                                @error('TOURNAMENT_SUB_TYPE_ID') <label id="TOURNAMENT_SUB_TYPE_ID-error"
                                    class="invalid-feedback" for="TOURNAMENT_SUB_TYPE_ID">{{ $message }}</label>
                                @enderror
                            </div>
                            <div class="form-group col-md-4 tournamentTypeIdCommon tournamentSubTypeIdCommon"
                                id="TournamentParentSatelliteIdWrapper" style="display:none;">
                                <label class="mandatory">Parent Tournament (Satellite):*</label>
                                <input type="hidden" name="TOURNAMENT_PARENT_SATELLITE_ID"
                                    value="{{ $tournament->TOURNAMENT_PARENT_SATELLITE_ID }}">
                                <select
                                    class="form-control @error('TOURNAMENT_PARENT_SATELLITE_ID') is-invalid @enderror"
                                    id="TOURNAMENT_PARENT_SATELLITE_ID" disabled>
                                    <option value=""
                                        {{ ""==old('TOURNAMENT_PARENT_SATELLITE_ID', $tournament->TOURNAMENT_PARENT_SATELLITE_ID ?? "") ? 'selected' : '' }}>
                                        Select Parent Tournament</option>
                                    @foreach($satalliteTournaments as $maintournament)
                                    <option value="{{ $maintournament->TOURNAMENT_ID }}"
                                        {{ $maintournament->TOURNAMENT_ID==old('TOURNAMENT_PARENT_SATELLITE_ID', $tournament->TOURNAMENT_PARENT_SATELLITE_ID ?? "") ? 'selected' : '' }}>
                                        {{ $maintournament->TOURNAMENT_START_TIME }} -
                                        ({{ $maintournament->TOURNAMENT_ID }}){{ $maintournament->TOURNAMENT_NAME }}
                                        ₹{{ $maintournament->TOT_BUYIN }}</option>
                                    @endforeach
                                </select>
                                @error('TOURNAMENT_PARENT_SATELLITE_ID') <label
                                    id="TOURNAMENT_PARENT_SATELLITE_ID-error" class="invalid-feedback"
                                    for="TOURNAMENT_PARENT_SATELLITE_ID">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-4 tournamentTypeIdCommon tournamentSubTypeIdCommon"
                                id="TournamentParentMultiDayIdWrapper" style="display:none;">
                                <label class="mandatory">Parent Tournament (Multi-Day):</label>
                                <input type="hidden" name="TOURNAMENT_PARENT_MULTI_DAY_ID"
                                    value="{{ $tournament->TOURNAMENT_PARENT_MULTI_DAY_ID }}">
                                <select
                                    class="form-control @error('TOURNAMENT_PARENT_MULTI_DAY_ID') is-invalid @enderror"
                                    id="TOURNAMENT_PARENT_MULTI_DAY_ID" disabled>
                                    <option value=""
                                        {{ ""==old('TOURNAMENT_PARENT_MULTI_DAY_ID', $tournament->TOURNAMENT_PARENT_MULTI_DAY_ID ?? "") ? 'selected' : '' }}>
                                        Select Parent Tournament</option>
                                    @foreach($multiDayChildTournaments as $multi_maintour)
                                    <option value="{{ $multi_maintour->TOURNAMENT_ID }}"
                                        {{ $multi_maintour->TOURNAMENT_ID==old('TOURNAMENT_PARENT_MULTI_DAY_ID', $tournament->TOURNAMENT_PARENT_MULTI_DAY_ID ?? "") ? 'selected' : '' }}>
                                        {{ $multi_maintour->TOURNAMENT_START_TIME }} -
                                        ({{ $multi_maintour->TOURNAMENT_ID }}){{ $multi_maintour->TOURNAMENT_NAME }}
                                        ₹{{ $multi_maintour->TOT_BUYIN }}</option>
                                    @endforeach
                                </select>
                                @error('TOURNAMENT_PARENT_MULTI_DAY_ID') <label
                                    id="TOURNAMENT_PARENT_MULTI_DAY_ID-error" class="invalid-feedback"
                                    for="TOURNAMENT_PARENT_MULTI_DAY_ID">{{ $message }}</label> @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Stop on final table</label>
                                @if($tournament->TOURNAMENT_STATUS != 0)
                                <input type="hidden" name="FINAL_TABLE_STOP"
                                    value="{{ $tournament->FINAL_TABLE_STOP }}">
                                @endif
                                <select class="form-control @error('FINAL_TABLE_STOP') is-invalid @enderror"
                                    name="FINAL_TABLE_STOP" id="FINAL_TABLE_STOP" {!! $tournament->TOURNAMENT_STATUS ==
                                    0 ? 'name="FINAL_TABLE_STOP"' : 'disabled' !!}>
                                    <option value="0"
                                        {{ "0"==old('FINAL_TABLE_STOP', $tournament->FINAL_TABLE_STOP ?? "0") ? 'selected' : '' }}>
                                        No</option>
                                    <option value="1"
                                        {{ "1"==old('FINAL_TABLE_STOP', $tournament->FINAL_TABLE_STOP ?? "0") ? 'selected' : '' }}>
                                        Yes</option>
                                </select>
                                @error('FINAL_TABLE_STOP') <label id="FINAL_TABLE_STOP-error" class="invalid-feedback"
                                    for="FINAL_TABLE_STOP">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label>Featured</label>
                                @if($tournament->TOURNAMENT_STATUS != 0)
                                <input type="hidden" name="FEATURED" value="{{ $tournament->FEATURED }}">
                                @endif
                                <select class="form-control @error('FEATURED') is-invalid @enderror" name="FEATURED"
                                    id="FEATURED" {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="FEATURED"' :
                                    'disabled' !!}>
                                    <option value="0"
                                        {{ "0"==old('FEATURED', $tournament->FEATURED ?? "0") ? 'selected' : '' }}>No
                                    </option>
                                    <option value="1"
                                        {{ "1"==old('FEATURED', $tournament->FEATURED ?? "0") ? 'selected' : '' }}>Yes
                                    </option>
                                </select>
                                @error('FEATURED') <label id="FEATURED-error" class="invalid-feedback"
                                    for="FEATURED">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label>Private Table</label>
                                <input type="hidden" name="PRIVATE_TABLE" value="{{ $tournament->PRIVATE_TABLE }}">
                                <select
                                    class="form-control privateTableEvent @error('PRIVATE_TABLE') is-invalid @enderror"
                                    id="PRIVATE_TABLE" disabled>
                                    <option value="0"
                                        {{ "0"==old('PRIVATE_TABLE', $tournament->PRIVATE_TABLE ?? "0") ? 'selected' : '' }}>
                                        No</option>
                                    <option value="1"
                                        {{ "1"==old('PRIVATE_TABLE', $tournament->PRIVATE_TABLE ?? "0") ? 'selected' : '' }}>
                                        Yes</option>
                                </select>
                                @error('PRIVATE_TABLE') <label id="PRIVATE_TABLE-error" class="invalid-feedback"
                                    for="PRIVATE_TABLE">{{ $message }}</label> @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Status</label>
                                    <div>
                                        <input type="checkbox" name="IS_ACTIVE" id="IS_ACTIVE"
                                            {{ "1"==old('IS_ACTIVE', $tournament->IS_ACTIVE ?? "1") ? 'checked' : '' }}
                                            data-plugin="switchery" data-color="#1bb99a" data-size="small" value="1" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                            </div>
                            <div class="form-group col-md-4 privateTableCommon passwordWrapper" style="display:none;">
                                <label>Password</label>
                                <input type="password" name="PASSWORD"
                                    value="{{ old('PASSWORD', $tournament->PASSWORD ?? '') }}" id="PASSWORD"
                                    class="form-control @error('PASSWORD') is-invalid @enderror"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('PASSWORD') <label id="PASSWORD-error" class="invalid-feedback"
                                    for="PASSWORD">{{ $message }}</label> @enderror
                            </div>
                        </div>
                    </div>
                    {{-- ./ Tournament Information Section --}}

                    {{-- Timing Section --}}
                    <div class="tab-pane" id="TimingSection" data-wizard-index="1">
                        <h5 class="font-17 mb-2">Timings </h5>
                        <div class="row customDatePickerWrapper tournamentTypeIdCommon" id="registerStartTimeWrapper">
                            <div class="form-group col-md-6">
                                <label class="mandatory"> Registration Start Date:</label>
                                <input type="hidden" name="REGISTER_START_TIME"
                                    value="{{ $tournament->REGISTER_START_TIME }}">
                                <input type="text"
                                    class="form-control customDatePicker from @error('REGISTER_START_TIME') is-invalid @enderror"
                                    name="REGISTER_START_TIME" id="REGISTER_START_TIME"
                                    value="{{ old('REGISTER_START_TIME',(isset($tournament) ? \Carbon\Carbon::parse($tournament->REGISTER_START_TIME)->format('d-M-Y H:i:s') : \Carbon\Carbon::now()->format('d-M-Y H:i:s'))) }}"
                                    disabled>
                                @error('REGISTER_START_TIME') <label id="REGISTER_START_TIME-error"
                                    class="invalid-feedback" for="REGISTER_START_TIME">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label class="mandatory"> Tournament Start Date:</label>
                                <input type="hidden" name="TOURNAMENT_START_TIME"
                                    value="{{ $tournament->TOURNAMENT_START_TIME }}">
                                <input type="text"
                                    class="form-control customDatePicker to @error('TOURNAMENT_START_TIME') is-invalid @enderror"
                                    name="TOURNAMENT_START_TIME" id="TOURNAMENT_START_TIME"
                                    value="{{ old('TOURNAMENT_START_TIME',(isset($tournament) ? \Carbon\Carbon::parse($tournament->TOURNAMENT_START_TIME)->format('d-M-Y H:i:s') : \Carbon\Carbon::now()->format('d-M-Y H:i:s'))) }}"
                                    disabled>
                                @error('TOURNAMENT_START_TIME') <label id="TOURNAMENT_START_TIME-error"
                                    class="invalid-feedback" for="TOURNAMENT_START_TIME">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label class="white-text">&nbsp;</label>
                                <div class="checkbox checkbox-purple">
                                    <input type="hidden" name="LATE_REGISTRATION_ALLOW"
                                        value="{{ $tournament->LATE_REGISTRATION_ALLOW }}">
                                    <input class="lateRegistrationAllowEvent" type="checkbox"
                                        id="LATE_REGISTRATION_ALLOW" value="1"
                                        {{ "1"==old('LATE_REGISTRATION_ALLOW', $tournament->LATE_REGISTRATION_ALLOW ?? "1") ? 'checked' : '' }}
                                        disabled>
                                    <label for="LATE_REGISTRATION_ALLOW">Late Registration</label>
                                </div>
                            </div>
                            <div class="form-group col-md-3" id="lateRegistrationEndTimeWrapper">
                                <label class="mandatory"> Registration Ends (min):</label>
                                <input type="number" min="0" max="999" name="LATE_REGISTRATION_END_TIME"
                                    value="{{ old('LATE_REGISTRATION_END_TIME',$tournament->LATE_REGISTRATION_END_TIME ?? '') }}"
                                    id="LATE_REGISTRATION_END_TIME"
                                    class="form-control @error('LATE_REGISTRATION_END_TIME') is-invalid @enderror"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('LATE_REGISTRATION_END_TIME') <label id="LATE_REGISTRATION_END_TIME-error"
                                    class="invalid-feedback" for="LATE_REGISTRATION_END_TIME">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                    </div>
                    {{-- ./ Timing Section --}}

                    {{-- Player Section --}}
                    <div class="tab-pane" id="PlayerSection" data-wizard-index="2">
                        <h5 class="font-17 mb-2">Players</h5>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label class="mandatory">Players per table:</label>
                                @if($tournament->TOURNAMENT_STATUS != 0)
                                <input type="hidden" name="PLAYER_PER_TABLE"
                                    value="{{ $tournament->PLAYER_PER_TABLE }}">
                                @endif
                                <select class="form-control @error('PLAYER_PER_TABLE') is-invalid @enderror"
                                    id="PLAYER_PER_TABLE" {!! $tournament->TOURNAMENT_STATUS == 0 ?
                                    'name="PLAYER_PER_TABLE"' : 'disabled' !!}>
                                    <option value="3"
                                        {{ "3"==old('PLAYER_PER_TABLE',$tournament->PLAYER_PER_TABLE ?? '') ? 'selected' : '' }}>
                                        3</option>
                                    <option value="4"
                                        {{ "4"==old('PLAYER_PER_TABLE',$tournament->PLAYER_PER_TABLE ?? '') ? 'selected' : '' }}>
                                        4</option>
                                    <option value="5"
                                        {{ "5"==old('PLAYER_PER_TABLE',$tournament->PLAYER_PER_TABLE ?? '') ? 'selected' : '' }}>
                                        5</option>
                                    <option value="6"
                                        {{ "6"==old('PLAYER_PER_TABLE',$tournament->PLAYER_PER_TABLE ?? '') ? 'selected' : '' }}>
                                        6</option>
                                    <option value="7"
                                        {{ "7"==old('PLAYER_PER_TABLE',$tournament->PLAYER_PER_TABLE ?? '') ? 'selected' : '' }}>
                                        7</option>
                                    <option value="8"
                                        {{ "8"==old('PLAYER_PER_TABLE',$tournament->PLAYER_PER_TABLE ?? '') ? 'selected' : '' }}>
                                        8</option>
                                    <option value="9"
                                        {{ "9"==old('PLAYER_PER_TABLE',$tournament->PLAYER_PER_TABLE ?? "9") ? 'selected' : '' }}>
                                        9</option>
                                </select>
                                @error('PLAYER_PER_TABLE') <label id="PLAYER_PER_TABLE-error" class="invalid-feedback"
                                    for="PLAYER_PER_TABLE">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label class="mandatory">Minimum:</label>
                                <input type="number" min="2" name="T_MIN_PLAYERS" id="T_MIN_PLAYERS"
                                    class="form-control @error('T_MIN_PLAYERS') is-invalid @enderror"
                                    value="{{ old('T_MIN_PLAYERS', $tournament->T_MIN_PLAYERS ?? 5) }}"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('T_MIN_PLAYERS') <label id="T_MIN_PLAYERS-error" class="invalid-feedback"
                                    for="T_MIN_PLAYERS">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label class="mandatory">Maximum:</label>
                                <input type="number" min="2" name="T_MAX_PLAYERS" id="T_MAX_PLAYERS"
                                    class="form-control @error('T_MAX_PLAYERS') is-invalid @enderror"
                                    value="{{ old('T_MAX_PLAYERS',$tournament->T_MAX_PLAYERS ??1500) }}"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('T_MAX_PLAYERS') <label id="T_MAX_PLAYERS-error" class="invalid-feedback"
                                    for="T_MAX_PLAYERS">{{ $message }}</label> @enderror
                            </div>
                        </div>
                    </div>
                    {{-- ./ Player Section --}}

                    {{-- Entry Criteria Section --}}
                    <div class="tab-pane" id="EntryCriteriaSection" data-wizard-index="3">
                        <h5 class="font-17 mb-2">Entry Criteria</h5>
                        <div class="align-items-center">
                            <div class="form-group ">
                                <label class="mr-2 mb-0 mandatory">Entry Type: </label>
                                <input type="hidden" name="COIN_TYPE_ID" value="{{ $tournament->COIN_TYPE_ID }}">
                                <div class="radio radio-info form-check-inline">
                                    <input class="coinTypeIdEvent" name="COIN_TYPE_ID" type="radio"
                                        id="COIN_TYPE_ID_REAL_CHIPS" value="1"
                                        {{ "1"==old('COIN_TYPE_ID', $tournament->COIN_TYPE_ID ?? "1") ? 'checked':'' }}
                                        disabled>
                                    <label for="COIN_TYPE_ID_REAL_CHIPS">Real Chips </label>
                                </div>
                                <div class="radio radio-info form-check-inline">
                                    <input class="coinTypeIdEvent" name="COIN_TYPE_ID" type="radio"
                                        id="COIN_TYPE_ID_TICKET" value="8"
                                        {{ "8"==old('COIN_TYPE_ID', $tournament->COIN_TYPE_ID ?? '') ? 'checked':'' }}
                                        disabled>
                                    <label for="COIN_TYPE_ID_TICKET">Ticket </label>
                                </div>
                            </div>
                            <div>
                                <div class="form-group" id="BalanceTypeWrapper">
                                    <label class="mr-2 mb-0 mandatory">Balance Type: </label>

                                    <input type="hidden" name="DEPOSIT_BALANCE_ALLOW"
                                        value="{{ $tournament->DEPOSIT_BALANCE_ALLOW }}">
                                    <input type="hidden" name="PROMO_BALANCE_ALLOW"
                                        value="{{ $tournament->PROMO_BALANCE_ALLOW }}">
                                    <input type="hidden" name="WIN_BALANCE_ALLOW"
                                        value="{{ $tournament->WIN_BALANCE_ALLOW }}">

                                    <div class="checkbox checkbox-success form-check-inline">
                                        <input type="checkbox" id="DEPOSIT_BALANCE_ALLOW" name="DEPOSIT_BALANCE_ALLOW"
                                            value="1"
                                            {{ "1"==old('DEPOSIT_BALANCE_ALLOW', $tournament->DEPOSIT_BALANCE_ALLOW ?? "1") ? 'checked':'' }}
                                            disabled>
                                        <label for="DEPOSIT_BALANCE_ALLOW"> Deposit</label>
                                    </div>
                                    <div class="checkbox checkbox-success form-check-inline">
                                        <input type="checkbox" id="PROMO_BALANCE_ALLOW" name="PROMO_BALANCE_ALLOW"
                                            value="1"
                                            {{ "1"==old('PROMO_BALANCE_ALLOW', $tournament->PROMO_BALANCE_ALLOW ?? "1") ? 'checked':'' }}
                                            disabled>
                                        <label for="PROMO_BALANCE_ALLOW">Promo </label>
                                    </div>
                                    <div class="checkbox checkbox-success form-check-inline">
                                        <input type="checkbox" id="WIN_BALANCE_ALLOW" name="WIN_BALANCE_ALLOW" value="1"
                                            {{ "1"==old('WIN_BALANCE_ALLOW', $tournament->WIN_BALANCE_ALLOW ?? "1") ? 'checked':'' }}
                                            disabled>
                                        <label for="WIN_BALANCE_ALLOW"> Win </label>
                                    </div>
                                </div>
                                <div class="row pt-1" id="EntryCriteriaAmountWrapper">
                                    <div class="form-group col-md-4" id="BuyInWrapper">
                                        <label class="mandatory">Amount:</label>
                                        <input type="number" min="0" max="99999999"
                                            class="form-control @error('BUYIN') is-invalid @enderror" name="BUYIN"
                                            id="BUYIN" value="{{ old('BUYIN',$tournament->BUYIN ?? 1500) }}" readonly>
                                        @error('BUYIN') <label id="BUYIN-error" class="invalid-feedback"
                                            for="BUYIN">{{ $message }}</label> @enderror
                                    </div>
                                    <div class="form-group col-md-4" id="EntryFeeWrapper">
                                        <label class="mandatory">Fee (Abs. Value):</label>
                                        <input type="number" min="0" max="99999"
                                            class="form-control @error('ENTRY_FEE') is-invalid @enderror"
                                            name="ENTRY_FEE" id="ENTRY_FEE"
                                            value="{{ old('ENTRY_FEE',$tournament->ENTRY_FEE ?? 1500) }}" readonly>
                                        @error('ENTRY_FEE') <label id="ENTRY_FEE-error" class="invalid-feedback"
                                            for="ENTRY_FEE">{{ $message }}</label> @enderror
                                    </div>
                                    <div class="form-group col-md-4 categoryCommon tournamentTypeIdCommon tournamentSubTypeIdCommon RebuyAddonReEntryCommon"
                                        id="BountyAmountWrapper">
                                        <label class="mandatory">Bounty Amount:</label>
                                        <input type="number" min="0" max="99999999"
                                            class="form-control @error('BOUNTY_AMOUNT') is-invalid @enderror"
                                            name="BOUNTY_AMOUNT" id="BOUNTY_AMOUNT"
                                            value="{{ old('BOUNTY_AMOUNT',$tournament->BOUNTY_AMOUNT ?? 1500) }}"
                                            readonly>
                                        @error('BOUNTY_AMOUNT') <label id="BOUNTY_AMOUNT-error" class="invalid-feedback"
                                            for="BOUNTY_AMOUNT">{{ $message }}</label> @enderror
                                    </div>
                                    <div class="form-group col-md-4 categoryCommon tournamentTypeIdCommon tournamentSubTypeIdCommon RebuyAddonReEntryCommon"
                                        id="BountyEntryFeeWrapper">
                                        <label class="mandatory">Bounty Fee: </label>
                                        <input type="number" min="0" max="99999999"
                                            class="form-control @error('BOUNTY_ENTRY_FEE') is-invalid @enderror"
                                            name="BOUNTY_ENTRY_FEE" id="BOUNTY_ENTRY_FEE"
                                            value="{{ old('BOUNTY_ENTRY_FEE',$tournament->BOUNTY_ENTRY_FEE ?? 1500) }}"
                                            readonly>
                                        @error('BOUNTY_ENTRY_FEE') <label id="BOUNTY_ENTRY_FEE-error"
                                            class="invalid-feedback" for="BOUNTY_ENTRY_FEE">{{ $message }}</label>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-4 categoryCommon tournamentTypeIdCommon tournamentSubTypeIdCommon RebuyAddonReEntryCommon"
                                        id="ProgressiveBountyPercentageWrapper">
                                        <label class="mandatory">PKO %: </label>
                                        <input type="number" min="0" max="99999999"
                                            class="form-control @error('PROGRESSIVE_BOUNTY_PERCENTAGE') is-invalid @enderror"
                                            name="PROGRESSIVE_BOUNTY_PERCENTAGE" id="PROGRESSIVE_BOUNTY_PERCENTAGE"
                                            value="{{ old('PROGRESSIVE_BOUNTY_PERCENTAGE',$tournament->PROGRESSIVE_BOUNTY_PERCENTAGE ?? 1500) }}"
                                            readonly>
                                        @error('PROGRESSIVE_BOUNTY_PERCENTAGE') <label
                                            id="PROGRESSIVE_BOUNTY_PERCENTAGE-error" class="invalid-feedback"
                                            for="PROGRESSIVE_BOUNTY_PERCENTAGE">{{ $message }}</label> @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- ./ Entry Criteria Section --}}

                    {{-- Blind Structure Section --}}
                    <div class="tab-pane" id="BlindStructureSection" data-wizard-index="4">
                        <h5 class="font-17 mb-2">Blind Structure</h5>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="mandatory">Select Blind Structure:</label>
                                @if($tournament->TOURNAMENT_STATUS != 0)
                                <input type="hidden" name="BLIND_STRUCTURE_ID"
                                    value="{{ $tournament->BLIND_STRUCTURE_ID }}">
                                @endif
                                <select
                                    class="form-control blindStructureIdEvent @error('BLIND_STRUCTURE_ID') is-invalid @enderror"
                                    id="BLIND_STRUCTURE_ID" {!! $tournament->TOURNAMENT_STATUS == 0 ?
                                    'name="BLIND_STRUCTURE_ID"' : 'disabled' !!}>
                                    <option value=""
                                        {{ ""==old('BLIND_STRUCTURE_ID', $tournament->BLIND_STRUCTURE_ID ?? "") ? 'checked':'' }}>
                                        Select Blind Structure</option>
                                    @foreach ($blindStructures as $blindInfo)
                                    <option value="{{ $blindInfo->BLIND_STRUCTURE_ID }}"
                                        {{ $blindInfo->BLIND_STRUCTURE_ID==old('BLIND_STRUCTURE_ID', $tournament->BLIND_STRUCTURE_ID ?? "") ? 'checked':'' }}>
                                        {{ $blindInfo->DESCRIPTION }}</option>
                                    @endforeach
                                </select>
                                @error('BLIND_STRUCTURE_ID') <label id="BLIND_STRUCTURE_ID-error"
                                    class="invalid-feedback" for="BLIND_STRUCTURE_ID">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label class="mandatory">Starting Blinds:</label>
                                @if($tournament->TOURNAMENT_STATUS != 0)
                                <input type="hidden" name="TOURNAMENT_LEVEL"
                                    value="{{ $tournament->TOURNAMENT_LEVEL }}">
                                @endif
                                <select
                                    class="form-control tournamentLevelEvent @error('TOURNAMENT_LEVEL') is-invalid @enderror"
                                    id="TOURNAMENT_LEVEL" {!! $tournament->TOURNAMENT_STATUS == 0 ?
                                    'name="TOURNAMENT_LEVEL"' : 'disabled'
                                    !!}>
                                    <option value=""> Select Blind Level </option>
                                </select>
                                @error('TOURNAMENT_LEVEL') <label id="TOURNAMENT_LEVEL-error" class="invalid-feedback"
                                    for="TOURNAMENT_LEVEL">{{ $message }}</label> @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="mandatory">Binds Increment Time (min):</label>
                                <input type="number" min="1" max="99" name="LEVEL_PERIOD" id="LEVEL_PERIOD"
                                    class="form-control @error('LEVEL_PERIOD') is-invalid @enderror"
                                    value="{{ old('LEVEL_PERIOD',$tournament->LEVEL_PERIOD ?? "") }}"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('LEVEL_PERIOD') <label id="LEVEL_PERIOD-error" class="invalid-feedback"
                                    for="LEVEL_PERIOD">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-6 tournamentTypeIdCommon" id="TournamentChipsWrapper">
                                <label class="mandatory">Starting Chips Count:</label>
                                <input type="number" min="0" max="99999999" name="TOURNAMENT_CHIPS"
                                    id="TOURNAMENT_CHIPS"
                                    class="form-control @error('TOURNAMENT_CHIPS') is-invalid @enderror"
                                    value="{{ old('TOURNAMENT_CHIPS',(int)$tournament->TOURNAMENT_CHIPS ?? "") }}"
                                    readonly>
                                @error('TOURNAMENT_CHIPS') <label id="TOURNAMENT_CHIPS-error" class="invalid-feedback"
                                    for="TOURNAMENT_CHIPS">{{ $message }}</label> @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 tournamentTypeIdCommon tournamentSubTypeIdCommon"
                                id="TimerTournamentEndTimeWrapper">
                                <label class="mandatory">Timer Tournament End Level:</label>
                                <input type="number" min="0"
                                    class="form-control timerTournamentEndTimeEvent @error('TIMER_TOURNAMENT_END_TIME') is-invalid @enderror"
                                    name="TIMER_TOURNAMENT_END_TIME"
                                    value="{{ old('TIMER_TOURNAMENT_END_TIME', $tournament->TIMER_TOURNAMENT_END_TIME ?? '') }}"
                                    id="TIMER_TOURNAMENT_END_TIME" readonly>
                                @error('TIMER_TOURNAMENT_END_TIME') <label id="TIMER_TOURNAMENT_END_TIME-error"
                                    class="invalid-feedback" for="TIMER_TOURNAMENT_END_TIME">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                    </div>
                    {{-- ./ Blind Structure Section --}}

                    {{-- Time Settings Section --}}
                    <div class="tab-pane" id="TimeSettingsSection" data-wizard-index="5">
                        <h5 class="font-17 mb-2">Time Settings</h5>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label class="mandatory">Turn Time (sec): </label>
                                <input type="number" min="1" max="99" name="PLAYER_HAND_TIME" id="PLAYER_HAND_TIME"
                                    value="{{ old('PLAYER_HAND_TIME', $tournament->PLAYER_HAND_TIME ?? '') }}"
                                    class="form-control @error('PLAYER_HAND_TIME') is-invalid @enderror"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('PLAYER_HAND_TIME') <label id="PLAYER_HAND_TIME-error" class="invalid-feedback"
                                    for="PLAYER_HAND_TIME">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-3">
                                <label class="mandatory">Disconnect Time (sec): </label>
                                <input type="number" min="1" max="99" name="DISCONNECT_TIME" id="DISCONNECT_TIME"
                                    value="{{ old('DISCONNECT_TIME', $tournament->DISCONNECT_TIME ?? '') }}"
                                    class="form-control @error('DISCONNECT_TIME') is-invalid @enderror"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('DISCONNECT_TIME') <label id="DISCONNECT_TIME-error" class="invalid-feedback"
                                    for="DISCONNECT_TIME">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-3">
                                <label class="mandatory">Extra Time (sec):</label>
                                <input type="number" min="1" max="99" name="EXTRA_TIME" id="EXTRA_TIME"
                                    value="{{ old('EXTRA_TIME', $tournament->EXTRA_TIME ?? '') }}"
                                    class="form-control @error('EXTRA_TIME') is-invalid @enderror"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('EXTRA_TIME') <label id="EXTRA_TIME-error" class="invalid-feedback"
                                    for="EXTRA_TIME">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-3">
                                <label class="mandatory">Lobby Display Interval Time (Hours):</label>
                                <input type="number" min="18" max="999" name="LOBBY_DISPLAY_INTERVAL"
                                    id="LOBBY_DISPLAY_INTERVAL"
                                    value="{{ old('LOBBY_DISPLAY_INTERVAL',$tournament->LOBBY_DISPLAY_INTERVAL ?? 18) }}"
                                    class="form-control @error('LOBBY_DISPLAY_INTERVAL') is-invalid @enderror"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('LOBBY_DISPLAY_INTERVAL') <label id="LOBBY_DISPLAY_INTERVAL-error"
                                    class="invalid-feedback" for="LOBBY_DISPLAY_INTERVAL">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label class="mandatory">Extra Time (Max Cap)(Sec):</label>
                                <input type="number" min="0" max="99" name="PLAYER_MAX_EXTRATIME"
                                    id="PLAYER_MAX_EXTRATIME"
                                    value="{{ old('PLAYER_MAX_EXTRATIME',$tournament->PLAYER_MAX_EXTRATIME ?? '') }}"
                                    class="form-control @error('PLAYER_MAX_EXTRATIME') is-invalid @enderror"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('PLAYER_MAX_EXTRATIME') <label id="PLAYER_MAX_EXTRATIME-error"
                                    class="invalid-feedback" for="PLAYER_MAX_EXTRATIME">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-3">
                                <label class="mandatory">Extra Time (Blind Levels)</label>
                                <input type="number" min="0" max="99" name="ADDITIONAL_EXTRATIME_LEVEL_INTERVAL"
                                    id="ADDITIONAL_EXTRATIME_LEVEL_INTERVAL"
                                    value="{{ old('ADDITIONAL_EXTRATIME_LEVEL_INTERVAL',$tournament->ADDITIONAL_EXTRATIME_LEVEL_INTERVAL ?? '') }}"
                                    class="form-control @error('ADDITIONAL_EXTRATIME_LEVEL_INTERVAL') is-invalid @enderror"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('ADDITIONAL_EXTRATIME_LEVEL_INTERVAL') <label
                                    id="ADDITIONAL_EXTRATIME_LEVEL_INTERVAL-error" class="invalid-feedback"
                                    for="ADDITIONAL_EXTRATIME_LEVEL_INTERVAL">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-3">
                                <label class="mandatory">Extra Time (Add-on) (Sec):</label>
                                <input type="number" min="0" max="99" name="ADDITIONAL_EXTRATIME"
                                    id="ADDITIONAL_EXTRATIME"
                                    value="{{ old('ADDITIONAL_EXTRATIME', $tournament->ADDITIONAL_EXTRATIME ?? '') }}"
                                    class="form-control @error('ADDITIONAL_EXTRATIME') is-invalid @enderror"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('ADDITIONAL_EXTRATIME') <label id="ADDITIONAL_EXTRATIME-error"
                                    class="invalid-feedback" for="ADDITIONAL_EXTRATIME">{{ $message }}</label> @enderror
                            </div>
                        </div>
                    </div>
                    {{-- ./ Time Settings Section --}}

                    {{-- Rebuy/Addon/ReEntry Section --}}
                    <div class="tab-pane" id="RebuyAddonReEntrySection" data-wizard-index="6">
                        <h5 class="font-17 mb-2">Rebuy/Addon/Re-Entry</h5>
                        @if($tournament->TOURNAMENT_STATUS != 0)
                        <input type="hidden" name="REBUY_ADDON_RE_ENTRY"
                            value="{{ $tournament->REBUY_ADDON_RE_ENTRY }}">
                        @endif
                        <div class="form-group ">
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" id="REBUY_ADDON_RE_ENTRY_NONE" value="0"
                                    class="tabsonclick RebuyAddonReEntryEvent"
                                    {{ "0"==old('REBUY_ADDON_RE_ENTRY', $tournament->REBUY_ADDON_RE_ENTRY ?? "0") ? 'checked':'' }}
                                    {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="REBUY_ADDON_RE_ENTRY"' : 'disabled'
                                !!}>
                                <label for="REBUY_ADDON_RE_ENTRY_NONE"> None </label>
                            </div>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" id="REBUY_ADDON_RE_ENTRY_REBUY_ADDON"
                                    class="tabsonclick RebuyAddonReEntryEvent" value="1"
                                    {{ "1"==old('REBUY_ADDON_RE_ENTRY',$tournament->REBUY_ADDON_RE_ENTRY ?? "") ? 'checked':'' }}
                                    {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="REBUY_ADDON_RE_ENTRY"' : 'disabled'
                                !!}>
                                <label for="REBUY_ADDON_RE_ENTRY_REBUY_ADDON">Rebuy/Addon </label>
                            </div>
                            <div class="radio radio-info form-check-inline">
                                <input type="radio" id="REBUY_ADDON_RE_ENTRY_REENRY"
                                    class="tabsonclick RebuyAddonReEntryEvent" value="2"
                                    {{ "2"==old('REBUY_ADDON_RE_ENTRY',$tournament->REBUY_ADDON_RE_ENTRY ?? "") ? 'checked':'' }}
                                    {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="REBUY_ADDON_RE_ENTRY"' : 'disabled'
                                !!}>
                                <label for="REBUY_ADDON_RE_ENTRY_REENRY">Re-Entry </label>
                            </div>
                        </div>

                        {{-- Re-Buy Settings Section --}}
                        <div id="ReBuySettingsWrapper">
                            <div class="border-bottom mb-3 mt-1"></div>
                            <h5 class="font-17 mb-2">Re-Buy Settings</h5>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label class="mandatory">Chips to be granted: </label>
                                    <input type="number" min="1" name="REBUY_CHIPS" id="REBUY_CHIPS"
                                        value="{{ old('REBUY_CHIPS',$tournament->REBUY_CHIPS ?? "") }}"
                                        class="form-control @error('REBUY_CHIPS') is-invalid @enderror"
                                        {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                    @error('REBUY_CHIPS') <label id="REBUY_CHIPS-error" class="invalid-feedback"
                                        for="REBUY_CHIPS">{{ $message }}</label> @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="mandatory">Player Max. Eligible Chips: </label>
                                    <input type="number" min="1" name="REBUY_ELIGIBLE_CHIPS" id="REBUY_ELIGIBLE_CHIPS"
                                        value="{{ old('REBUY_ELIGIBLE_CHIPS',$tournament->REBUY_ELIGIBLE_CHIPS ?? "") }}"
                                        class="form-control @error('REBUY_ELIGIBLE_CHIPS') is-invalid @enderror"
                                        {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                    @error('REBUY_ELIGIBLE_CHIPS') <label id="REBUY_ELIGIBLE_CHIPS-error"
                                        class="invalid-feedback" for="REBUY_ELIGIBLE_CHIPS">{{ $message }}</label>
                                    @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="mandatory">Time Period (Level):</label>
                                    @if($tournament->TOURNAMENT_STATUS != 0)
                                    <input type="hidden" name="REBUY_END_TIME"
                                        value="{{ $tournament->REBUY_END_TIME }}">
                                    @endif
                                    <select class="form-control @error('REBUY_END_TIME') is-invalid @enderror"
                                        id="REBUY_END_TIME" {!! $tournament->TOURNAMENT_STATUS == 0 ?
                                        'name="REBUY_END_TIME"' : 'disabled' !!}>
                                        <option value="">Select Registration Starts Hour</option>
                                    </select>
                                    @error('REBUY_END_TIME') <label id="REBUY_END_TIME-error" class="invalid-feedback"
                                        for="REBUY_END_TIME">{{ $message }}</label> @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="mandatory">Num. Of Re-Buy:</label>
                                    @if($tournament->TOURNAMENT_STATUS != 0)
                                    <input type="hidden" name="REBUY_COUNT" value="{{ $tournament->REBUY_COUNT }}">
                                    @endif
                                    <select class="form-control @error('REBUY_COUNT') is-invalid @enderror"
                                        id="REBUY_COUNT" {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="REBUY_COUNT"'
                                        : 'disabled' !!}>
                                        <option value="9999"
                                            {{ "9999"==old('REBUY_COUNT',$tournament->REBUY_COUNT ?? "9999") ? 'selected' : '' }}>
                                            Unlimited</option>
                                        <option value="1"
                                            {{ "1"==old('REBUY_COUNT',$tournament->REBUY_COUNT ?? "") ? 'selected' : '' }}>
                                            1</option>
                                        <option value="2"
                                            {{ "2"==old('REBUY_COUNT',$tournament->REBUY_COUNT ?? "") ? 'selected' : '' }}>
                                            2</option>
                                        <option value="3"
                                            {{ "3"==old('REBUY_COUNT',$tournament->REBUY_COUNT ?? "") ? 'selected' : '' }}>
                                            3</option>
                                        <option value="4"
                                            {{ "4"==old('REBUY_COUNT',$tournament->REBUY_COUNT ?? "") ? 'selected' : '' }}>
                                            4</option>
                                        <option value="5"
                                            {{ "5"==old('REBUY_COUNT',$tournament->REBUY_COUNT ?? "") ? 'selected' : '' }}>
                                            5</option>
                                    </select>
                                    @error('REBUY_COUNT') <label id="REBUY_COUNT-error" class="invalid-feedback"
                                        for="REBUY_COUNT">{{ $message }}</label> @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label class="mandatory">Re-Buy Amount: </label>
                                    <input type="number" min="1" name="REBUY_IN" id="REBUY_IN"
                                        value="{{ old('REBUY_IN',$tournament->REBUY_IN ?? '') }}"
                                        class="form-control @error('REBUY_IN') is-invalid @enderror"
                                        {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                    @error('REBUY_IN') <label id="REBUY_IN-error" class="invalid-feedback"
                                        for="REBUY_IN">{{ $message }}</label> @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="mandatory">Entry Fee: </label>
                                    <input type="number" min="0" name="REBUY_ENTRY_FEE" id="REBUY_ENTRY_FEE"
                                        value="{{ old('REBUY_ENTRY_FEE',$tournament->REBUY_ENTRY_FEE ?? '') }}"
                                        class="form-control @error('REBUY_ENTRY_FEE') is-invalid @enderror"
                                        {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                    @error('REBUY_ENTRY_FEE') <label id="REBUY_ENTRY_FEE-error" class="invalid-feedback"
                                        for="REBUY_ENTRY_FEE">{{ $message }}</label> @enderror
                                </div>
                                <div class="form-group col-md-3" id="DoubleReBuyInWrapper">
                                    <label class="white-text">.</label>
                                    @if($tournament->TOURNAMENT_STATUS != 0)
                                    <input type="hidden" name="DOUBLE_REBUYIN"
                                        value="{{ $tournament->DOUBLE_REBUYIN }}">
                                    @endif
                                    <div class="checkbox checkbox-purple">
                                        <input type="checkbox" id="DOUBLE_REBUYIN" value="1"
                                            {{ "1"==old('DOUBLE_REBUYIN',$tournament->DOUBLE_REBUYIN ?? "0") ? 'checked' : '' }}
                                            {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="DOUBLE_REBUYIN"' :
                                        'disabled' !!}>
                                        <label for="DOUBLE_REBUYIN">Double Rebuy</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- ./ Re-Buy Settings Section --}}

                        <div id="AddonSettingsWrapper">
                            <h5 class="font-17 mb-2">Addon Settings</h5>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label class="mandatory">Chips to be granted: </label>
                                    <input type="number" min="1" name="ADDON_CHIPS"
                                        value="{{ old('ADDON_CHIPS',$tournament->ADDON_CHIPS ?? '') }}" id="ADDON_CHIPS"
                                        class="form-control @error('ADDON_CHIPS') is-invalid @enderror"
                                        {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                    @error('ADDON_CHIPS') <label id="ADDON_CHIPS-error" class="invalid-feedback"
                                        for="ADDON_CHIPS">{{ $message }}</label> @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="mandatory">Addon Time Interval: (Minutes)</label>
                                    <input type="number" min="1" name="ADDON_BREAK_TIME"
                                        value="{{ old('ADDON_BREAK_TIME',$tournament->ADDON_BREAK_TIME ?? '') }}"
                                        id="ADDON_BREAK_TIME"
                                        class="form-control @error('ADDON_BREAK_TIME') is-invalid @enderror"
                                        {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                    @error('ADDON_BREAK_TIME') <label id="ADDON_BREAK_TIME-error"
                                        class="invalid-feedback" for="ADDON_BREAK_TIME">{{ $message }}</label> @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="mandatory"> Addon Amount: </label>
                                    <input type="number" min="1" name="ADDON_AMOUNT"
                                        value="{{ old('ADDON_AMOUNT',$tournament->ADDON_AMOUNT ?? '') }}"
                                        id="ADDON_AMOUNT"
                                        class="form-control @error('ADDON_AMOUNT') is-invalid @enderror"
                                        {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                    @error('ADDON_AMOUNT') <label id="ADDON_AMOUNT-error" class="invalid-feedback"
                                        for="ADDON_AMOUNT">{{ $message }}</label> @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="mandatory">Entry Fee: </label>
                                    <input type="number" min="0" name="ADDON_ENTRY_FEE"
                                        value="{{ old('ADDON_ENTRY_FEE',$tournament->ADDON_ENTRY_FEE ?? '') }}"
                                        id="ADDON_ENTRY_FEE"
                                        class="form-control @error('ADDON_AMOUNT') is-invalid @enderror"
                                        {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                    @error('ADDON_ENTRY_FEE') <label id="ADDON_ENTRY_FEE-error" class="invalid-feedback"
                                        for="ADDON_AMOUNT">{{ $message }}</label> @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- ./ Rebuy/Addon/ReEntry Section --}}

                    {{-- Prize Structure Section --}}
                    <div class="tab-pane" id="PrizeStructureSection" data-wizard-index="7">
                        <h5 class="font-17 mb-2">Prize Structure</h5>
                        <div class="row">
                            <div class="form-group col-md-12" id="PrizeStructureWrapper">
                                <label class="mr-2 mb-0 mandatory">Prize Structure: </label>
                                @if($tournament->TOURNAMENT_STATUS != 0)
                                <input type="hidden" name="PRIZE_STRUCTURE_ID"
                                    value="{{ $tournament->PRIZE_STRUCTURE_ID }}">
                                @endif
                                <div class="radio radio-info form-check-inline">
                                    <input type="radio" class="tabsonclick prizeStructureIdEvent"
                                        id="PRIZE_STRUCTURE_ID_CUSTOM" value="1"
                                        {{ "1"==old('PRIZE_STRUCTURE_ID',$tournament->PRIZE_STRUCTURE_ID ?? '') ? 'checked' : '' }}
                                        {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="PRIZE_STRUCTURE_ID"' :
                                    'disabled' !!}>
                                    <label for="PRIZE_STRUCTURE_ID_CUSTOM"> Custom </label>
                                </div>
                                <div class="radio radio-info form-check-inline">
                                    <input type="radio" id="PRIZE_STRUCTURE_ID_DEFAULT"
                                        class="tabsonclick prizeStructureIdEvent" value="2"
                                        {{ "2"==old('PRIZE_STRUCTURE_ID',$tournament->PRIZE_STRUCTURE_ID ?? 2) ? 'checked' : '' }}
                                        {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="PRIZE_STRUCTURE_ID"' :
                                    'disabled' !!}>
                                    <label for="PRIZE_STRUCTURE_ID_DEFAULT">Default </label>
                                </div>
                                <div class="radio radio-info form-check-inline">
                                    <input type="radio" id="PRIZE_STRUCTURE_ID_CUSTOM_MIX"
                                        class="tabsonclick prizeStructureIdEvent" value="4"
                                        {{ "4"==old('PRIZE_STRUCTURE_ID',$tournament->PRIZE_STRUCTURE_ID ?? '') ? 'checked' : '' }}
                                        {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="PRIZE_STRUCTURE_ID"' :
                                    'disabled' !!}>
                                    <label for="PRIZE_STRUCTURE_ID_CUSTOM_MIX">Custom Mix </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4" id="PrizeStructureTypeWrapper">
                                <label class="mandatory">Prize Structure Type:</label>
                                @if($tournament->TOURNAMENT_STATUS != 0)
                                <input type="hidden" name="PRIZE_STRUCTURE_TYPE_ID"
                                    value="{{ $tournament->PRIZE_STRUCTURE_TYPE_ID }}">
                                @endif
                                <select class="form-control @error('PRIZE_STRUCTURE_TYPE_ID') is-invalid @enderror"
                                    id="PRIZE_STRUCTURE_TYPE_ID" {!! $tournament->TOURNAMENT_STATUS == 0 ?
                                    'name="PRIZE_STRUCTURE_TYPE_ID"' :
                                    'disabled' !!}>
                                    @foreach($multiplePrizeStructures as $multiplePrizeStructure)
                                    <option value="{{ $multiplePrizeStructure->PRIZE_STRUCTURE_TYPE_ID }}"
                                        {{ $multiplePrizeStructure->PRIZE_STRUCTURE_TYPE_ID==old('PRIZE_STRUCTURE_TYPE_ID',$tournament->PRIZE_STRUCTURE_TYPE_ID ?? '')?'selected' : '' }}>
                                        {{ $multiplePrizeStructure->DESCRIPTION }}</option>
                                    @endforeach
                                </select>
                                @error('PRIZE_STRUCTURE_TYPE_ID') <label id="PRIZE_STRUCTURE_TYPE_ID-error"
                                    class="invalid-feedback" for="PRIZE_STRUCTURE_TYPE_ID">{{ $message }}</label>
                                @enderror
                            </div>
                            <div class="col-md-4" id="PrizeBalanceTypeWrapper">
                                <div class="form-group ">
                                    <label class="w-100 mb-2 mandatory">Prize Balance Type: </label>
                                    @if($tournament->TOURNAMENT_STATUS != 0)
                                    <input type="hidden" name="PRIZE_BALANCE_TYPE"
                                        value="{{ $tournament->PRIZE_BALANCE_TYPE }}">
                                    @endif
                                    <div class="radio radio-info form-check-inline ml-1">
                                        <input type="radio" id="PRIZE_BALANCE_TYPE_WIN" value="3"
                                            {{ "3"==old('PRIZE_BALANCE_TYPE',$tournament->PRIZE_BALANCE_TYPE ?? "3") ? 'checked':'' }}
                                            {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="PRIZE_BALANCE_TYPE"' :
                                        'disabled' !!}>
                                        <label for="PRIZE_BALANCE_TYPE_WIN">Win </label>
                                    </div>
                                    <div class="radio radio-info form-check-inline">
                                        <input type="radio" id="PRIZE_BALANCE_TYPE_PROMO" value="2"
                                            name="PRIZE_BALANCE_TYPE"
                                            {{ "2"==old('PRIZE_BALANCE_TYPE',$tournament->PRIZE_BALANCE_TYPE ?? "") ? 'checked':'' }}
                                            {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="PRIZE_BALANCE_TYPE"' :
                                        'disabled' !!}>
                                        <label for="PRIZE_BALANCE_TYPE_PROMO">Promo </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" id="PrizePoolTypeWrapper">
                                <div class="form-group ">
                                    <label class="w-100 mb-2 mandatory">Prize Pool Type: </label>
                                    @if($tournament->TOURNAMENT_STATUS != 0)
                                    <input type="hidden" name="FIXED_PRIZE" value="{{ $tournament->FIXED_PRIZE }}">
                                    @endif
                                    <div class="radio radio-info form-check-inline ml-1">
                                        <input type="radio" id="FIXED_PRIZE_DYNAMIC" value="0" name="FIXED_PRIZE"
                                            {{ "0"==old('FIXED_PRIZE',$tournament->FIXED_PRIZE ?? "0") ? 'checked':'' }}
                                            {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="FIXED_PRIZE"' :
                                        'disabled' !!}>
                                        <label for="FIXED_PRIZE_DYNAMIC"> Dynamic </label>
                                    </div>
                                    <div class="radio radio-info form-check-inline">
                                        <input type="radio" id="FIXED_PRIZE_FIXED" value="1" name="FIXED_PRIZE"
                                            {{ "1"==old('FIXED_PRIZE',$tournament->FIXED_PRIZE ?? "") ? 'checked':'' }}
                                            {!! $tournament->TOURNAMENT_STATUS == 0 ? 'name="FIXED_PRIZE"' :
                                        'disabled' !!}>
                                        <label for="FIXED_PRIZE_FIXED"> Fixed </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4" id="noOfWinnersCustomWrapper">
                                <label class="mandatory">No of places paid: </label>
                                <input type="number" min="0" name="NO_OF_WINNERS_CUSTOM" id="NO_OF_WINNERS_CUSTOM"
                                    class="form-control noOfWinnersCustomEvent @error('NO_OF_WINNERS_CUSTOM') is-invalid @enderror"
                                    value="{{ old('NO_OF_WINNERS_CUSTOM',$tournament->NO_OF_WINNERS_CUSTOM ?? 0) }}"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('NO_OF_WINNERS_CUSTOM') <label id="NO_OF_WINNERS_CUSTOM-error"
                                    class="invalid-feedback" for="NO_OF_WINNERS_CUSTOM">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-4" id="noOfWinnersCustomMixWrapper">
                                <label class="mandatory">No of places paid: </label>
                                <input type="number" min="0" name="NO_OF_WINNERS_CUSTOM_MIX"
                                    id="NO_OF_WINNERS_CUSTOM_MIX"
                                    class="form-control noOfWinnersCustomMixEvent @error('NO_OF_WINNERS_CUSTOM_MIX') is-invalid @enderror"
                                    value="{{ old('NO_OF_WINNERS_CUSTOM_MIX',$tournament->NO_OF_WINNERS_CUSTOM_MIX ?? 0) }}"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('NO_OF_WINNERS_CUSTOM_MIX') <label id="NO_OF_WINNERS_CUSTOM_MIX-error"
                                    class="invalid-feedback" for="NO_OF_WINNERS_CUSTOM_MIX">{{ $message }}</label>
                                @enderror
                            </div>
                            <div class="form-group col-md-4" id="GTDPrizePoolWrapper">
                                <label class="mandatory">GTD Prize Pool: </label>
                                <input type="text" name="GUARENTIED_PRIZE" id="GUARENTIED_PRIZE"
                                    value="{{ old('GUARENTIED_PRIZE',$tournament->GUARENTIED_PRIZE ?? '') }}"
                                    class="form-control @error('GUARENTIED_PRIZE') is-invalid @enderror"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('GUARENTIED_PRIZE') <label id="GUARENTIED_PRIZE-error" class="invalid-feedback"
                                    for="GUARENTIED_PRIZE">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-4" id="GTDSeatsWrapper">
                                <label class="mandatory">GTD Seats </label>
                                <input type="text" name="SATELLITES_GUARANTEED_PLACES_PAID"
                                    value="{{ old('SATELLITES_GUARANTEED_PLACES_PAID',$tournament->SATELLITES_GUARANTEED_PLACES_PAID ?? '') }}"
                                    id="SATELLITES_GUARANTEED_PLACES_PAID"
                                    class="form-control @error('SATELLITES_GUARANTEED_PLACES_PAID') is-invalid @enderror"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('SATELLITES_GUARANTEED_PLACES_PAID') <label
                                    id="SATELLITES_GUARANTEED_PLACES_PAID-error" class="invalid-feedback"
                                    for="SATELLITES_GUARANTEED_PLACES_PAID">{{ $message }}</label> @enderror
                            </div>
                            <div class="form-group col-md-4" id="QualifyingPlayersWrapper">
                                <label class="mandatory">Qualifying Players: (%)</label>
                                <input type="text" name="MULTIDAY_PLAYER_PERCENTAGE"
                                    value="{{ old('MULTIDAY_PLAYER_PERCENTAGE',$tournament->MULTIDAY_PLAYER_PERCENTAGE ?? '') }}"
                                    id="MULTIDAY_PLAYER_PERCENTAGE"
                                    class="form-control @error('MULTIDAY_PLAYER_PERCENTAGE') is-invalid @enderror"
                                    {{ $tournament->TOURNAMENT_STATUS == 0 ? '' : 'readonly' }}>
                                @error('MULTIDAY_PLAYER_PERCENTAGE') <label id="MULTIDAY_PLAYER_PERCENTAGE-error"
                                    class="invalid-feedback" for="MULTIDAY_PLAYER_PERCENTAGE">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div id="prizeAdditionalFieldsCustom"></div>
                        <div id="prizeAdditionalFieldsCustomMix"></div>
                    </div>
                    {{-- ./ PrizeStructureSection --}}

                    {{-- wizard button section --}}
                    <ul class="pager list-inline mb-0 wizard">
                        <li class="list-inline-item disabled">
                            <button type="button" href="javascript: void(0);"
                                class="btn btn-secondary waves-effect waves-light button-first" style="display:none;"><i
                                    class=" mdi mdi-skip-previous   mr-1"></i> First </button>
                            <button type="button" href="javascript: void(0);"
                                class="btn btn-secondary waves-effect waves-light button-previous"
                                style="display:none;"><i class=" mdi mdi-skip-previous mr-1"></i> Previous </button>
                        </li>
                        <li class="list-inline-item float-right">
                            <button type="button" href="javascript: void(0);"
                                class="btn btn-secondary waves-effect  waves-light button-next">Next <i
                                    class="mdi mdi-skip-next  ml-1"></i></button>
                            <button type="button" class="btn btn-secondary waves-effect waves-light button-last"
                                style="display:none;">Last <i class="mdi mdi-skip-next  ml-1"></i></button>
                            <button disabled type="button"
                                class="btn btn-success waves-effect waves-light button-finish formSubmitButtom"
                                style="display:none;"><i class="fa fa-save mr-1"></i> Update Tournament</button>
                        </li>
                    </ul>
                    {{-- ./ wizard button section --}}
                </div>
                <!-- tab-content -->
            </div>
            <!-- end #progressbarwizard-->
        </form>
    </div>
    <!-- end card-body -->
</div>
<!-- end card-->
<div class="mb-5 row"></div>
@endsection
@section('js')
<script src="{{ asset('assets/libs/switchery/switchery.min.js') }}"></script>
<script src="{{ asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/libs/twitter-bootstrap-wizard/twitter-bootstrap-wizard.min.js') }}"></script>
@endsection

@section('post_js')
<script src="{{ asset('assets/bo/pages/tournament/tournament.min.js') }}"></script>
<script>
    (function($) {
        "use strict";
        const option={};
        option['satalliteTournaments'] = "{!! base64_encode(json_encode($satalliteTournaments)) !!}";
        option['tournament'] = "{!! !empty($tournament) ? base64_encode(json_encode($tournament)) : null !!}";

        $.EditTournament = new EditTournament(option);
        $.EditTournament.triggerEventsOnInit();
        $.EditTournament.triggerAfterInit();
    }(window.jQuery));
    
</script>
@endsection