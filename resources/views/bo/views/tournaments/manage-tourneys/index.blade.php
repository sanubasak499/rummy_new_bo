@extends('bo.layouts.master')

@section('title', "Manage Tourneys | PB BO")

@section('pre_css')
<link rel="stylesheet" href="{{ asset('assets/libs/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('post_css')
<link rel="stylesheet" href="{{ asset('assets/bo/pages/tournament/tournament.min.css') }}">@endsection
@section('content')

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item">Tournaments</li>
                    <li class="breadcrumb-item active">Manage Tourneys</li>
                </ol>
            </div>
            <h4 class="page-title">Manage Tourneys </h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-sm-4">
                        <h4 class="header-title mt-1">View Manage Tourneys </h4>
                    </div>
                    <div class="col-sm-8">
                        <div class="text-sm-right">
                            <a href="{{ route('tournaments.manage-tourneys.create') }}"
                                class="btn btn-success waves-effect waves-light" title="Create Tourneys"><i
                                    class="fa fa-plus mr-1"></i> Create Tourneys</a>
                        </div>
                    </div><!-- end col-->
                </div>
                <form id="tournamentFilterForm" data-default-url="{{ route('tournaments.manage-tourneys.filter') }}"
                    action="{{ route('tournaments.manage-tourneys.filter') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label>Tournament Name: </label>
                            <input type="text" name="TOURNAMENT_NAME" class="form-control"
                                placeholder="Enter Tournament Name"
                                value="{{ old('TOURNAMENT_NAME', $params->TOURNAMENT_NAME ?? '') }}"
                                id="TOURNAMENT_NAME">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Game Type: </label>
                            <select name="MINI_GAME_TYPE_ID" class="form-control">
                                <option data-default-selected="" value=""
                                    {{ ""==old('STATUS',$params->MINI_GAME_TYPE_ID ?? "") ? 'selected' : '' }}>
                                    Select Game Type</option>
                                <option value="1"
                                    {{ "1"==old('MINI_GAME_TYPE_ID',$params->MINI_GAME_TYPE_ID ?? "") ? 'selected' : '' }}>
                                    Texas</option>
                                <option value="2"
                                    {{ "2"==old('MINI_GAME_TYPE_ID',$params->MINI_GAME_TYPE_ID ?? "") ? 'selected' : '' }}>
                                    PLO</option>
                                <option value="3"
                                    {{ "3"==old('MINI_GAME_TYPE_ID',$params->MINI_GAME_TYPE_ID ?? "") ? 'selected' : '' }}>
                                    5 PLO</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Tournament Status: </label>
                            <select name="TOURNAMENT_STATUS" class="form-control">
                                <option data-default-selected="" value=""
                                    {{ ""==old('TOURNAMENT_STATUS',$params->TOURNAMENT_STATUS ?? "") ? 'selected' : '' }}>
                                    Select
                                    Tournament Status</option>
                                <option value="0"
                                    {{ "0"==old('TOURNAMENT_STATUS',$params->TOURNAMENT_STATUS ?? "") ? 'selected' : '' }}>
                                    Announced</option>
                                <option value="1"
                                    {{ "1"==old('TOURNAMENT_STATUS',$params->TOURNAMENT_STATUS ?? "") ? 'selected' : '' }}>
                                    Registering</option>
                                <option value="2,3"
                                    {{ "2,3"==old('TOURNAMENT_STATUS',$params->TOURNAMENT_STATUS ?? "") ? 'selected' : '' }}>
                                    Started</option>
                                <option value="6"
                                    {{ "6"==old('TOURNAMENT_STATUS',$params->TOURNAMENT_STATUS ?? "") ? 'selected' : '' }}>
                                    Cancelled</option>
                                <option value="5"
                                    {{ "5"==old('TOURNAMENT_STATUS',$params->TOURNAMENT_STATUS ?? "") ? 'selected' : '' }}>
                                    Finished</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 d-none">
                            <label class="mandatory">Tournament Type</label>
                            <select class="form-control 
                                {{-- disabled style="display:none;" --}}
                                @error('TOURNAMENT_TYPE_ID') error @enderror" name="TOURNAMENT_TYPE_ID[]"
                                id="TOURNAMENT_TYPE_ID">
                                <option data-default-selected="" value=""
                                    {{ ""==old('TOURNAMENT_TYPE_ID',$params->TOURNAMENT_TYPE_ID ?? "") ? 'selected' : '' }}>
                                    Select Tournament Type</option>
                                <option value="4"
                                    {{ "4"==old('TOURNAMENT_TYPE_ID', $params->TOURNAMENT_TYPE_ID ?? "") ? 'selected' : '' }}>
                                    Normal</option>
                                <option value="9"
                                    {{ "9"==old('TOURNAMENT_TYPE_ID', $params->TOURNAMENT_TYPE_ID ?? "") ? 'selected' : '' }}>
                                    Multi-Day Main
                                </option>
                                <option value="10"
                                    {{ "10"==old('TOURNAMENT_TYPE_ID', $params->TOURNAMENT_TYPE_ID ?? "") ? 'selected' : '' }}>
                                    Normal Bounty
                                </option>
                                <option value="11"
                                    {{ "11"==old('TOURNAMENT_TYPE_ID', $params->TOURNAMENT_TYPE_ID ?? "") ? 'selected' : '' }}>
                                    Progressive
                                    Bounty</option>
                                <option value="12"
                                    {{ "12"==old('TOURNAMENT_TYPE_ID', $params->TOURNAMENT_TYPE_ID ?? "") ? 'selected' : '' }}>
                                    Offline
                                </option>
                            </select>
                            @error('TOURNAMENT_TYPE_ID') <label id="TOURNAMENT_TYPE_ID-error" class="error"
                                for="TOURNAMENT_TYPE_ID">{{ $message }}</label> @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label>Buy-in (Excl.Fee): </label>
                            <input name="BUYIN" type="text" class="form-control" placeholder="Enter Buy In"
                                value="{{ old('BUYIN', $params->BUYIN ?? '') }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Currency Type: </label>
                            <select name="COIN_TYPE_ID" class="form-control">
                                <option data-default-selected=""
                                    {{ ""==old('COIN_TYPE_ID',$params->COIN_TYPE_ID ?? "") ? 'selected' : '' }}
                                    value="">Select Currency Type</option>
                                @foreach ($currencyTypes as $coinType)
                                <option
                                    {{ $coinType->COIN_TYPE_ID==old('COIN_TYPE_ID',$params->COIN_TYPE_ID ?? "") ? 'selected' : '' }}
                                    value="{{ $coinType->COIN_TYPE_ID }}">
                                    {{ $coinType->NAME }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Status: </label>
                            <select name="IS_ACTIVE" class="form-control">
                                <option value="" {{ ""==old('IS_ACTIVE', $params->IS_ACTIVE ?? "") ? 'selected' : '' }}>
                                    Select
                                </option>
                                <option data-default-selected="" value="1"
                                    {{ "1"==old('IS_ACTIVE', $params->IS_ACTIVE ?? "1") ? 'selected' : '' }}>Active
                                </option>
                                <option value="0"
                                    {{ "0"==old('IS_ACTIVE', $params->IS_ACTIVE ?? "") ? 'selected' : '' }}>Inactive
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="row customDatePickerWrapper">
                        <div class="form-group col-md-4">
                            <label>From: </label>
                            <input name="date_from" type="text"
                                class="form-control customDatePicker from flatpickr-input"
                                placeholder="Select From Date" value="{{ old('date_from',$params->date_from ?? "") }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label>To: </label>
                            <input name="date_to" type="text" class="form-control customDatePicker to flatpickr-input"
                                placeholder="Select To Date" value="{{ old('date_to',$params->date_to ?? "") }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Date Range: </label>
                            <select name="date_range" id="date_range" class="form-control formatedDateRange">
                                <option data-default-selected="" value=""
                                    {{ ""==old('date_range', $params->date_range ?? "") ? 'selected' : '' }}>Select
                                </option>
                                <option value="1"
                                    {{ "1"==old('date_range', $params->date_range ?? "") ? 'selected' : '' }}>Today
                                </option>
                                <option value="2"
                                    {{ "2"==old('date_range', $params->date_range ?? "") ? 'selected' : '' }}>Yesterday
                                </option>
                                <option value="3"
                                    {{ "3"==old('date_range', $params->date_range ?? "") ? 'selected' : '' }}>This Week
                                </option>
                                <option value="4"
                                    {{ "4"==old('date_range', $params->date_range ?? "") ? 'selected' : '' }}>Last Week
                                </option>
                                <option value="5"
                                    {{ "5"==old('date_range', $params->date_range ?? "") ? 'selected' : '' }}>This Month
                                </option>
                                <option value="6"
                                    {{ "6"==old('date_range', $params->date_range ?? "") ? 'selected' : '' }}>Last Month
                                </option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect waves-light"><i
                            class="fas fa-search mr-1"></i> Search</button>
                    <a href="#" class="btn btn-secondary waves-effect waves-light ml-1" data-toggle="reload"
                        data-form-id="#tournamentFilterForm"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                </form>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

@if(!empty($tournaments))
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-dark text-white">
                <div class="card-widgets">
                    <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false"
                        aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>
                </div>
                <h5 class="card-title mb-0 text-white">Tourneys List</h5>
            </div>
            <div id="cardCollpase7" class="collapse show">
                <div class="card-body">
                    <table class="basic-datatable table dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>Tournament Id</th>
                                <th>Tournament Name</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                                <th>Game Type</th>
                                <th>Buy-In</th>
                                <th>Regis.</th>
                                <th>Tour. Status</th>
                                <th>Details Options</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tournaments as $tournament)
                            <tr>
                                <td>{{ $tournament->TOURNAMENT_ID }}</td>
                                <td>{{ $tournament->TOURNAMENT_NAME }}</td>
                                <td>{{ $tournament->TOURNAMENT_START_TIME }}</td>
                                <td>{{ $tournament->TOURNAMENT_END_TIME }}</td>
                                <td>{{ $tournament->MINIGAMES_TYPE_NAME }}</td>
                                <td>{{ $tournament->BUYIN }}+{{$tournament->ENTRY_FEE}}</td>
                                <td>
                                    <a href="{{ route('tournaments.manage-tourneys.userRegistered', encrypt($tournament->TOURNAMENT_ID)) }}"
                                        target="_blank" class="card-link">
                                        <u>{{ $tournament->tournamentRegistrationsCount }}</u>
                                    </a>
                                </td>
                                <td>
                                    @php
                                    $colors = ['success', 'warning', 'info', 'blue', 'pink', 'secondary',
                                    'danger','success'];
                                    @endphp
                                    <span
                                        class="badge bg-{{ !empty($tournament->TOURNAMENT_STATUS || $tournament->TOURNAMENT_STATUS===0) ? ($colors[$tournament->TOURNAMENT_STATUS] ?? 'success') : 'light' }} text-white">{{ $tournament->getTournamentStatusName() }}</span>
                                </td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#tournamentDetailsModal"
                                        data-tournament-id="{{ $tournament->TOURNAMENT_ID }}" class="card-link">Info</a>

                                    @if($tournament->TOURNAMENT_STATUS === 5)
                                    <a href="{{ route('tournaments.manage-tourneys.rank', encrypt($tournament->TOURNAMENT_ID)) }}"
                                        target="_blank" data-tournament-id="{{ $tournament->TOURNAMENT_ID }}"
                                        class="card-link ml-1">Rank</a>
                                    @else
                                    <a href="#" data-toggle="modal" data-target="#tournamentPrizeInfoModal"
                                        data-tournament-status="{{ $tournament->TOURNAMENT_STATUS }}"
                                        data-tournament-id="{{ $tournament->TOURNAMENT_ID }}"
                                        class="card-link  ml-1">Prize</a>
                                    @endif

                                    <a href="#" data-toggle="modal" data-target="#tournamentBlindStructureInfoModel"
                                        data-tournament-id="{{ $tournament->TOURNAMENT_ID }}"
                                        class="card-link  ml-1">Blind</a>
                                </td>
                                <td>
                                    <a class="action-icon font-18 tooltips cancelTournamentEvent"
                                        href="javascript:void(0);" data-tournament-id="{{ $tournament->TOURNAMENT_ID }}"
                                        data-tournament-encrypted-id="{{ encrypt($tournament->TOURNAMENT_ID) }}"> <i
                                            class="mdi mdi-cancel"
                                            data-tournament-encrypted-id="{{ encrypt($tournament->TOURNAMENT_ID) }}"></i><span
                                            class="tooltiptext">Cancel</span></a>

                                    @if($tournament->TOURNAMENT_STATUS != 0 && $tournament->TOURNAMENT_STATUS != 1)
                                    <a class="action-icon font-18 tooltips StuckResumeTournamentEvent"
                                        href="javascript:void(0);" data-tournament-id="{{ $tournament->TOURNAMENT_ID }}"
                                        data-tournament-encrypted-id="{{ encrypt($tournament->TOURNAMENT_ID) }}"> <i
                                            class="mdi mdi-skip-forward"
                                            data-tournament-encrypted-id="{{ encrypt($tournament->TOURNAMENT_ID) }}"></i><span
                                            class="tooltiptext">Stuck &
                                            Resume</span></a>
                                    @endif

                                    <a class="action-icon font-18 tooltips" target="_blank"
                                        href="{{ route('tournaments.manage-tourneys.edit', encrypt($tournament->TOURNAMENT_ID)) }}"
                                        data-tournament-id="{{ $tournament->TOURNAMENT_ID }}"> <i
                                            class="fa fa-edit"></i><span class="tooltiptext">Edit</span></a>

                                    @if($tournament->STUCK_STATUS != 1)
                                    <a href="{{ route('tournaments.manage-tourneys.clone', encrypt($tournament->TOURNAMENT_ID)) }}"
                                        target="_blank" data-tournament-id="{{ $tournament->TOURNAMENT_ID }}"
                                        class="action-icon font-18 tooltips"> <i class=" far fa-clone"></i><span
                                            class="tooltiptext">Clone</span></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if($tournaments->total()>100)
                    <div class="customPaginationRender mt-2" data-form-id="#tournamentFilterForm" class="mt-2"
                        style="display:flex; justify-content: space-between;">
                        <div class="pt-2">More Records</div>
                        <div>
                            {{ $tournaments->render("pagination::customPaginationView") }}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <!-- end card body-->
        </div>
        <!-- end card -->
    </div>
    <!-- end col-->
</div>
@endif


<div class="modal fade" id="tournamentDetailsModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content bg-transparent shadow-none">
            <div class="loaderWrapper" style="background-color: #f5f5f5 !important; padding: 10px 10px 0 10px !important;">
                <div class="loader d-flex justify-content-center hv_center">
                    <div class="spinner-border" role="status"></div>
                </div>
            </div>
            <div class="mainWrapper"
                style="background-color: #f5f5f5 !important; padding: 10px 10px 0 10px !important;">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tournamentBlindStructureInfoModel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content bg-transparent shadow-none">
            <div class="card">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal">
                            <i class="mdi mdi-close"></i>
                        </a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18">Blind Structure</h5>
                </div>
                <div class="card-body font-14 position-relative">
                    <div class="loader d-flex justify-content-center hv_center">
                        <div class="spinner-border" role="status"></div>
                    </div>
                    <div class="row mb-1">
                        <div class="col-sm-4 mb-1"><strong>Tournament Name:</strong>
                            <span data-view-column="TOURNAMENT_NAME"></span>
                        </div>
                        <div class="col-sm-4 mb-1"><strong>Game Type:</strong>
                            <span data-view-column="MINIGAMES_TYPE_NAME"></span>
                        </div>
                        <div class="col-sm-4 mb-1"><strong>Buy In + Fees:</strong>
                            <span data-view-column="BUYIN+ENTRY_FREE"></span>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-4 mb-1"><strong>Start Date & Time:</strong>
                            <span data-view-column="TOURNAMENT_START_TIME"></span>
                        </div>
                        <div class="col-sm-4 mb-1"><strong>End Date & Time:</strong>
                            <span data-view-column="TOURNAMENT_END_TIME"></span>
                        </div>
                        <div class="col-sm-4"><strong>Tournament Type</strong>
                            <span data-view-column="TOURNAMENT_TYPE_DESCRIPTION"></span>
                        </div>
                    </div>
                    <div class="border-bottom  mb-3"></div>
                    <table class="basic-datatable table dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>Level</th>
                                <th>Blind</th>
                                <th>Ante</th>
                                <th>Minutes</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="tournamentPrizeInfoModal">
    <div class="modal-dialog">
        <div class="modal-content bg-transparent shadow-none">
            <div class="card">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal">
                            <i class="mdi mdi-close"></i>
                        </a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18">Default Prize Structure</h5>
                </div>
                <div class="card-body font-14 position-relative">
                    <div class="loader d-flex justify-content-center hv_center">
                        <div class="spinner-border" role="status"></div>
                    </div>
                    <div class="table-responsive defaultPrizeStructure prizeStructureCommon default-prize-structure"
                        data-pattern="priority-columns">
                        <table class="table dt-responsive nowrap w-100 dataTable">
                            <thead></thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="prizeStructure prizeStructureCommon">
                        <table class="basic-datatable table dt-responsive nowrap w-100">
                            <thead>
                                <tr>
                                    <th>Place</th>
                                    <th>Prize Value</th>
                                    <th>Winner Percentage</th>
                                    <th>Prize Tournament Id</th>
                                    <th>Prize Tournament Name</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form id="StuckResumeTournamentForm" action="" method="post">
    {{ csrf_field() }}
    @method('put')
</form>

<form id="cancelTournamentForm" action="" method="post">
    {{ csrf_field() }}
    @method('delete')
</form>
@endsection

@section('js')
<script src="{{ asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
@endsection

@section('post_js')
<script src="{{ asset('assets/bo/pages/tournament/tournament.min.js') }}"></script>
<script>
    (function($) {
        "use strict";
        $.FilterTournament = new FilterTournament();
    }(window.jQuery));
</script>
@endsection