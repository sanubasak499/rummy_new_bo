@extends('bo.layouts.master')

@section('title', "Menu | PB BO")

@section('pre_css')
    <link href="{{ asset('assets/libs/nestable2/nestable2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Menu</h4>
        </div>
    </div>
</div>     
<!-- end page title --> 

<div class="row">
    <div class="col-lg-12">
        <div class="text-left" id="nestable_list_menu">
            <button type="button" class="btn btn-blue btn-sm waves-effect mb-3 waves-light" data-action="expand-all">Expand All</button>
            <button type="button" class="btn btn-pink btn-sm waves-effect mb-3 waves-light" data-action="collapse-all">Collapse All</button>
            <button type="button" class="btn btn-primary btn-sm waves-effect mb-3 waves-light" data-toggle="reload" data-form-id="#addEditMenuForm">Add Menu</button>
        </div>
    </div> <!-- end col -->
</div>
<!-- End row -->

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="row">
                <div class="col-md-6 mb-4">
                    <h4 class="header-title">Menu List</h4>
                    <form action="{{ route('settings.menu.order') }}" method="post">
                        @csrf
                        <input type="hidden" name="order" id="menuOrder">
                        <div class="custom-dd-empty dd card" id="menuOrderNestable">
                            <div class="card-body">
                                @include('bo.views.settings.menu.partials.index', ['items'=>$menus])
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-2">Change Order</button>
                    </form>
                </div><!-- end col -->
                <div class="col-md-6 mb-4">
                    <h4 class="header-title">Add/Update Menu</h4>
                    <div class="card">
                        <div class="card-body">
                            <form data-default-url="{{ route('settings.menu.store') }}" action="{{ route('settings.menu.store') }}" method="post" id="addEditMenuForm">
                                @csrf
                                <div class="form-group mb-3">
                                    <label>Display Name</label>
                                    <input type="text" name="display_name" class="form-control" placeholder="Display Name">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="parent_id">Parent</label>
                                    <select class="form-control" name="parent_id" id="parent_id" data-toggle="select2">
                                        <option value="">Select Parent</option>
                                        @foreach (\PokerBaazi::completeMenuNameId() as $item)
                                        <option value="{{ $item->id }}">{{ $item->display_name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group mb-3">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description" rows="5"></textarea>
                                </div>
                                <div class="form-group mb-3">
                                    <label class="w-100">Icon Class <span class="float-right"><a href="{{ route('icons.fontawesome') }}" target="_blank">View Icons</a></span></label>
                                    <input type="text" name="icon_class" class="form-control">
                                </div>
                                <div class="form-group mb-3">
                                    <label>Target</label>
                                    <select class="form-control" name="target">
                                        <option value="_self">Same Tab</option>
                                        <option value="_blank">New Tab</option>
                                    </select>
                                </div>
                                <div class="form-group mb-3">
                                    <label>Route Type</label>
                                    <select class="form-control" name="absolute_url" id="routeType">
                                        <option value="0" selected>Dynamic Route</option>
                                        <option value="1">Static Url</option>
                                    </select>
                                </div>
                                
                                <div class="form-group mb-3" data-route-type="1">
                                    <label>Static URL</label>
                                    <input type="text" name="url" class="form-control">
                                </div>
                                <div class="form-group mb-3" data-route-type="0">
                                    <label>Dynamic Route</label>
                                    <input type="text" name="route" class="form-control">
                                </div>
                                <div class="form-group mb-3" data-route-type="0">
                                    <label>Parameters</label>
                                    <textarea class="form-control" name="parameters" rows="5"></textarea>
                                </div>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="status" class="custom-control-input" id="status" value="1" checked>
                                    <label class="custom-control-label" for="status">Status</label>
                                </div>
                                <button type="submit" class="btn btn-primary mt-2">Submit</button>
                                <a href="javascript:;" class="btn btn-primary mt-2" data-toggle="reload" data-form-id="#addEditMenuForm"><i class="mdi mdi-refresh"></i></a>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- end row -->
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>
@endsection

@section('js')
<!-- Plugins js-->
<script src="{{ asset('assets/libs/nestable2/nestable2.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>

<!-- Nestable init-->
<script src="{{ asset('assets/js/pages/nestable.init.js') }}"></script>
<script src="{{ asset('assets/bo/pagesJs/menu_module.js') }}"></script>
@endsection