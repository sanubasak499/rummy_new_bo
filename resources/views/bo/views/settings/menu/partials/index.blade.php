<ol class="dd-list menuList">
    @foreach ($items as $key => $item)
    <li class="dd-item dd3-item menuItem" data-id="{{ $item->id }}" data-parent-id="{{ $item->parent_id }}">
        <div class="dd-handle dd3-handle"></div>
        <div class="dd3-content" data-obj="{{ base64_encode(json_encode($item)) }}">
            {{ $item->display_name }}
            <div class="float-right ">
                @if($item->status == 1)
                <span class="badge bg-soft-success text-success shadow-none">Active</span>
                @else
                <span class="badge bg-soft-danger text-danger shadow-none">Inactive</span>
                @endif
                <a href="javascript:;"><span class="pl-1 menuEditViewButton"><i class="fas fa-edit"></i></span></a>
            </div>
        </div>
        @if(!$item->children->isEmpty())
        @include('bo.views.settings.menu.partials.index', ['items' => $item->children])
        @endif
        @endforeach
    </li>
</ol>