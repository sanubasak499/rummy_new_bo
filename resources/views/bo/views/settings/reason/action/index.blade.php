@extends('bo.layouts.master')

@section('title', "Manage Reasons | PB BO")

@section('pre_css')
    <link href="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
    <style type="text/css">
      .bootstrap-select  .dropdown-menu.inner.show{height: 200px; overflow-y: scroll !important;}

      .mandetory{ color:red;font-weight: bold; }
    </style>
@endsection


@section('content')


<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item">
                                <a>Settings </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a>Manage Reasons </a>
                            </li>
                        </ol>
                    </div>
                    <h4 class="page-title">Reasons</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <h5 class="font-18">Manage Reasons</h5>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <a
                                        href="#"
                                        class="btn btn-success waves-effect waves-light"
                                        data-toggle="modal"
                                        title="Edit"
                                        data-target="#AddrolemyModal"
                                    >
                                        <i class="fa fa-plus mr-1"></i>Add
                                        Reason
                                    </a>
                                </div>
                            </div>
                            <!-- end col-->
                        </div>
                        <table
                            class="table table-centered table-striped dt-responsive nowrap w-100"
                            id="basic-datatable"
                        >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category Name</th>
                                    <th>Reasons</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Added By</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@if(!empty($dataSets))
                            		<?php $sno=1; ?>
									@foreach($dataSets as $dataSet)
	                                <tr>
	                                    <td>{{ $sno++ }}</td>
	                                    <td>
                                            <ul>
                                            @forelse($dataSet->categories() as $category)
                                                <li>{{ $category->CATEGORY }}</li>
                                            @empty
                                                N/A         
                                            @endforelse
                                            </ul> 
                                        </td>
	                                    <td class="ellipsisWrapper">
                                            <div class="ellipsis">
                                                {{ $dataSet->ACTIONS_REASON }}
                                            </div>   
                                        </td>
	                                    <td class="ellipsisWrapper">
                                            <div class="ellipsis">
                                                 {{ $dataSet->ACTIONS_REASON_DESCRIPTION }}
                                            </div>
                                        </td>
	                                    <td>
	                                        <label class="switches"
	                                            ><input
                                                    value="1"
                                                    {{ $dataSet->STATUS==1 ? 'checked':'' }}
                                                    id="status_{{$dataSet->ACTIONS_REASONS_ID}}"
                                                    onChange="toggleStatus(this.id,{{$dataSet->ACTIONS_REASONS_ID}});"
	                                                type="checkbox"
	                                                /><span
	                                                class="slider round"

	                                            ></span
	                                        ></label>
	                                    </td>
	                                    <td>{{ $dataSet->CREATED_BY }}</td>
	                                    <td class="text-center">
	                                        <a
	                                            class="action-icon font-14 tooltips"
	                                            style="cursor:pointer;"
	                                            title="Edit"
                                                onclick="show({{ $dataSet->ACTIONS_REASONS_ID }});"
	                                        >
	                                            <span class="tooltiptext"
	                                                >Edit</span
	                                            >
	                                            <i class="fa fa-edit"></i>
	                                        </a>
	                                        <a
                                                onclick="deleteReason('deleteForm{{$dataSet->ACTIONS_REASONS_ID}}');"
	                                            type="button"
	                                            class="action-icon font-14   tooltips"
	                                        > <span class="tooltiptext"
                                                    >Delete</span
                                                >
	                                          
                                                    <i class="fa fa-trash"></i>
                                                
                                                <form id="deleteForm{{$dataSet->ACTIONS_REASONS_ID}}" action="{{ route('settings.reason-action.update',$dataSet->ACTIONS_REASONS_ID) }}" method="post">
                                                    @csrf
                                                    @method('PATCH')
                                                    <input type="hidden" name="value" value="2">
                                                    <input type="hidden" name="action" value="toggleStatus">
                                                </form>
	                                        </a>
	                                    </td>
	                                </tr>
	                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- end card-body-->
                </div>
                <!-- end card-->
            </div>
            <!-- end col -->
        </div>
    </div>
</div>
{{-- {{ dd($dataSets->where('ACTIONS_REASONS_ID', 21)) }} --}}
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content bg-transparent shadow-none position-relative">
            <!---<div class="d-flex justify-content-center hv_center">
            <div class="spinner-border" role="status"></div>
            </div>
            <div class="card  blureffect">--->
            <div class="card">
                <div class="card-header bg-secondary py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal">
                            <i class="mdi mdi-close"></i>
                        </a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18">
                        Edit Reason
                    </h5>
                </div>
                <div class="card-body position-relative">
                    <form id="categoryEditForm"  method="POST" >
                        @csrf
                        @method('PATCH')
                        <div class="form-group multiplecustom">
                            <label>Category Name: <span class="mandetory">*</span></label>
                            <select
                                name="reason_category_id[]"
                                class="form-control"
                                multiple="multiple"
                                id="categoryEdit"
                            >
                                @if(!empty($activeCategories))
                                    @foreach($activeCategories as $activeCategory)
                                        <option   value="{{ $activeCategory->REASON_CATEGORY_ID }}"> {{ $activeCategory->REASON_CATEGORY_NAME }} </option>
                                    @endforeach
                                @else
                                    <option value=''>Reason Categories Not Found</option>
                                @endif   
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Reason: <span class="mandetory">*</span></label>
                            <input
                                type="text"
                                class="form-control maxlengtherror"
                                name="actions_reason"
                                maxlength="250"
                                placeholder=""
                                required
                            />
                        </div>
                        <div class="form-group mb-3">
                            <label> Description</label>
                            <textarea
                                class="form-control maxlengtherror"
                                name="actions_reason_description"
                                rows="5"
                                maxlength="500"
                            ></textarea>
                        </div>
                        <div class="text-right">
                            <button
                                type="submit"
                                class="btn btn-warning waves-effect waves-light"
                            >
                                <i class="far fa-edit mr-1"></i> Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="AddrolemyModal">
    <div class="modal-dialog">
        <div class="modal-content bg-transparent shadow-none position-relative">
            <!----<div class="d-flex justify-content-center hv_center">
            <div class="spinner-border" role="status"></div>
            </div>
            <div class="card  blureffect">--->
            <div class="card ">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal">
                            <i class="mdi mdi-close"></i>
                        </a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18">
                        Add Reason
                    </h5>
                </div>
                <div class="card-body">
                    <form id="reasonForm" method="post" action="{{ route('settings.reason-action.store') }}">
                    	@csrf
                        <div class="form-group multiplecustom">
                            <label>Category Name: <span class="mandetory">*</span></label>
                            <select
                                name="reason_category_id[]"
                                class="form-control selectpicker"
                                multiple="multiple" 
                                id="categoryAdd"
                            >
                               @if(!empty($activeCategories))
                               		<option value='' >Select Reason Category</option>
                               		@foreach($activeCategories as $activeCategory)
                               			<option   value="{{ $activeCategory->REASON_CATEGORY_ID }}"> {{ $activeCategory->REASON_CATEGORY_NAME }} </option>
                               		@endforeach
                               	@else
                               		<option value=''>Reason Categories Not Found</option>
                               	@endif
                            </select>
                            @error('reason_category_id')
                            	<label id="reason_category_id-error" class="error" for="reason_category_id">
                            	    Please Select Category.
                            	</label>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="name"> Reason: <span class="mandetory">*</span></label>
                            <input
                                type="text"
                                class="form-control maxlengtherror"
                                maxlength="250"
                                name="actions_reason"
                                placeholder=""
                            />
                        </div>
                        <div class="form-group mb-3">
                            <label> Description</label>
                            <textarea
                                class="form-control maxlengtherror"
                                name="actions_reason_description"
                                maxlength="500"
                                rows="5"
                            ></textarea>
                        </div>
                        <div class="text-right" id="submitButtonWrap">
                            <button
                                type="submit"
                                class="btn btn-success waves-effect waves-light"
                            >
                                <i class="fa fa-save mr-1"></i> Save
                            </button>
                            {{-- <button
                                type="button"
                                class="btn btn-dark waves-effect waves-light ml-2"
                                onclick="Custombox.close();"
                            >
                                <i class="mdi mdi-replay mr-1"></i>Reset
                            </button> --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

<!-- Sweet alert init js-->
<script src="{{asset('assets/js/pages/sweet-alerts.init.js')}}"></script>
<script src="{{asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
@endsection

@section('post_js')

<script type="text/javascript">
	
    var validateRules = {
        ignore:[],
        rules:{
            actions_reason:{
                required:true,
                pattern :   /^[a-zA-Z0-9-_'.$1\n& ]+$/
            },
            reason_category_id:{
                required:true
            },
            actions_reason_description:{
                pattern :   /^[a-zA-Z0-9-_'.$1\n& ]+$/
            }
        }
    };

    $("#reasonForm").validate(validateRules);
    $("#categoryEditForm").validate(validateRules);

    show = (id) => {
        $.ajax({
            url :`${window.pageData.baseUrl}/settings/reason-action/${id}`,
            dataType : 'JSON',
            type:'GET',
            success : function(data){

                let catArr = data.REASON_CATEGORY_ID.split(',');

                if(catArr.length > 0 ){
                    $('#categoryEdit').selectpicker('val',catArr);
                }
                else{
                    $('#categoryEdit').selectpicker('deselectAll');
                }    

                $('#categoryEditForm input[name="actions_reason"]').val('').val(data.ACTIONS_REASON);
                $('#categoryEditForm textarea[name="actions_reason_description"]').val('').val(data.ACTIONS_REASON_DESCRIPTION);
                $('#categoryEditForm').attr('action',`${window.pageData.baseUrl}/settings/reason-action/${id}`);
                $('#myModal').modal('show');
            }  
        });
    }

    toggleStatus = (target_id, id) => {
        $.ajax({
            url :`${window.pageData.baseUrl}/settings/reason-action/${id}`,
            type:'POST',
            data:{
                action : 'toggleStatus',
                value  :  $('#'+target_id).prop('checked'),
                _method : 'PATCH'
            },
            success:() => {
                $.NotificationApp.send("Well Done", ($('#'+target_id).prop('checked')?'Status <i><b>Enabled</b></i> Successfully.':'Status <i><b>Disabled</b></i> Successfully.'), 'top-right', '#3b98b5', 'success');
            }
        });
    }

    $("#reasonForm").submit(() => {
        let loaderBtn = `<button  class="btn btn-primary formActionButton mr-1" id="confirm-hide-excel" type="button" disabled="disabled">
                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                Loading...
              </button>`;
        if($("#reasonForm").valid()){
            $("#submitButtonWrap").html(loaderBtn);
        }
    });

    deleteReason = (target_id) => {
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type:"warning",
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            $("#"+target_id).submit();
          }
        })
    }

    @error('reason_category_id')
        $.NotificationApp.send("Error", 'Category Not Selected.', 'top-right', '#3b98b5', 'warning');
    @enderror

</script>
@endsection