@extends('bo.layouts.master')

@section('title', "Manage Category | PB BO")

@section('pre_css')
<style type="text/css">
    .mandetory{ color:red;font-weight: bold; }
</style>
    
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item">
                                <a>Settings </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a>Manage Category</a>
                            </li>
                        </ol>
                    </div>
                    <h4 class="page-title">Reasons Category</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <h5 class="font-18">
                                    Manage Reason's Category
                                </h5>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-right">
                                    <a
                                        href="#"
                                        class="btn btn-success waves-effect waves-light"
                                        data-toggle="modal"
                                        title="Edit"
                                        data-target="#AddrolemyModal"
                                    >
                                        <i class="fa fa-plus mr-1"></i>Add
                                        Reason's Category
                                    </a>
                                </div>
                            </div>
                            <!-- end col-->
                        </div>
                        <table
                            class="table table-centered table-striped dt-responsive nowrap w-100"
                            id="basic-datatable"
                        >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category Name</th>
                                    <th>Category Description</th>
                                    <th>Added By</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $sNo = 1; ?>
                                @if(!empty($dataSets))

                                    @foreach($dataSets as $dataSet)
                                        <tr>
                                            <td>{{ $sNo++ }}</td>
                                            <td>{{ $dataSet->REASON_CATEGORY_NAME }}</td>
                                            <td class="ellipsisWrapper">
                                                <div class="ellipsis">
                                                    {{ $dataSet->REASON_CATEGORY_DESCRIPTION }}
                                                </div>    
                                            </td>
                                            <td>{{ $dataSet->CREATED_BY }}</td>
                                            <td class="text-center">
                                                <a
                                                    id="{{ $dataSet->REASON_CATEGORY_ID }}"
                                                    class="action-icon font-14 tooltips"  
                                                    data-plugin="tippy"
                                                    style="cursor:pointer;"
                                                    data-tippy-interactive="true"
                                                    onClick="show(this.id);"
                                                > <span class="tooltiptext">Edit</span>
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach    
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- end card-body-->
                </div>
                <!-- end card-->
            </div>
            <!-- end col -->
        </div>
    </div>
</div>
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content bg-transparent shadow-none position-relative">
            <!---<div class="d-flex justify-content-center hv_center">
            <div class="spinner-border" role="status"></div>
            </div>
            <div class="card  blureffect">--->
            <div class="card">
                <div class="card-header bg-secondary py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal">
                            <i class="mdi mdi-close"></i>
                        </a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18">
                        Edit Reason's Category
                    </h5>
                </div>
                <div class="card-body position-relative">
                    <form id="categoryEditForm" method="post" >
                        @csrf
                        @method('patch')
                        <div class="form-group">
                            <label for="name">Category Name: <span class="mandetory">*</span></label>
                            <input
                                type="text"
                                class="form-control maxlengtherror"
                                name="category_name"
                                 maxlength="50"
                                placeholder=""
                                required
                            />
                        </div>
                        <div class="form-group mb-3">
                            <label>Category Description:</label>
                            <textarea
                                class="form-control maxlengtherror"
                                maxlength="225"
                                name="category_description"
                                rows="5"
                            ></textarea>
                        </div>
                        <div class="text-right">
                            <button
                                type="submit"
                                class="btn btn-warning waves-effect waves-light "
                            >
                                <i class="far fa-edit mr-1"></i> Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="AddrolemyModal">
    <div class="modal-dialog">
        <div class="modal-content bg-transparent shadow-none position-relative">
            <!----<div class="d-flex justify-content-center hv_center">
            <div class="spinner-border" role="status"></div>
            </div>
            <div class="card  blureffect">--->
            <div class="card ">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal">
                            <i class="mdi mdi-close"></i>
                        </a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18">
                        Add Reason's Category
                    </h5>
                </div>
                <div class="card-body">
                    <form id="categoryForm" method="post" action="{{ route('settings.reason-category.store') }}" name="categoryForm" data-parsley-validate="">
                        @csrf
                        <div class="form-group">
                            <label for="name">Category Name: <span class="mandetory">*</span></label>
                            <input
                                type="text"
                                class="form-control maxlengtherror"
                                maxlength="50"
                                value="{{ old('category_name') }}"
                                name="category_name"
                            />
                            @error('category_name')
                            <label id="category_name-error" class="error" for="category_name">{{ $message }}</label>
                            @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label>Category Description:</label>
                            <textarea
                                name="category_description"
                                rows="5"
                                
                                class="form-control maxlengtherror"
                                maxlength="225"
                            ></textarea>
                        </div>
                        <div class="text-right" id="submitButtonWrap">
                            <button
                                type="submit"
                                class="btn btn-success waves-effect waves-light"
                            >
                                <i class="fa fa-save mr-1"></i> Save
                            </button>

                            <!-- <button
                                type="button"
                                class="btn btn-dark waves-effect waves-light ml-2"
                                onclick="Custombox.close();"
                            >
                                <i class="mdi mdi-replay mr-1"></i>Reset
                            </button> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
<script type="text/javascript">

    $(document).ready(function(){
        var validRules = {
            rules:{
                category_name:{
                    required : true,
                    pattern :   /^[a-zA-Z0-9-_'.$1\n& ]+$/
                },
                category_description:{
                    pattern :   /^[a-zA-Z0-9-_'.$1\n& ]+$/
                }
            }
        };
        
        $("#categoryForm").validate(validRules);
        $("#categoryEditForm").validate(validRules);

        show = (id) => {
            $.ajax({
                url :`${window.pageData.baseUrl}/settings/reason-category/${id}`,
                dataType : 'JSON',
                success : function(data){
                    $('#categoryEditForm input[name="category_name"]').val('').val(data.REASON_CATEGORY_NAME);
                    $('#categoryEditForm textarea[name="category_description"]').val('').val(data.REASON_CATEGORY_DESCRIPTION);
                    $('#categoryEditForm').attr('action',`${window.pageData.baseUrl}/settings/reason-category/${id}`);
                    $('#myModal').modal('show');
                }  
            });
        }

        toggleStatus = (target_id, id) => {
            $.ajax({
                url :`${window.pageData.baseUrl}/settings/reason-category/${id}`,
                type:'POST',
                data:{
                    action : 'toggleStatus',
                    value  :  $('#'+target_id).prop('checked'),
                    _method : 'PATCH'
                },
                success : ()=> {
                   $.NotificationApp.send("Well Done", ($('#'+target_id).prop('checked')?'Status <i><b>Enabled</b></i> Successfully.':'Status <i><b>Disabled</b></i> Successfully.'), 'top-right', '#3b98b5', 'success');
                }
            });
        }

        $("#categoryForm").submit(() => {
            let loaderBtn = `<button  class="btn btn-primary formActionButton mr-1" id="confirm-hide-excel" type="button" disabled="disabled">
                    <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                    Loading...
                  </button>`;
            if($("#categoryForm").valid()){
                $("#submitButtonWrap").html(loaderBtn);
            }
        });

    });

    @error('category_name')
        $.NotificationApp.send("Error", 'This Category Already Exists.', 'top-right', '#3b98b5', 'warning');
    @enderror
    
</script>
@endsection
