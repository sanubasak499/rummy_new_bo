<!-- 
	|--------------------------------------------------------------------------
	| Author: Nitesh Kumar Jha
	| Purpose: Add banner
	| Created Date: 13-12-2019 To 16-12-2019
	| Modified Date: 
	| Modified By: 
	| Modified Details: 
	|--------------------------------------------------------------------------
-->
@extends('bo.layouts.master')
@section('title', "Add Manage Banner | PB BO")
@section('pre_css')
<!-- Plugins css -->
<link href="{{ asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/dropify/dropify.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('post_css')
<!-- Plugins css -->
<link href="{{ asset('assets/libs/nestable2/nestable2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />

<style>
    .select2-container .select2-selection--single {
        height: auto;
        border: 1px solid #ced4da;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 36px;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow b {
        margin-top: 2px;
    }
</style>
@endsection

@section('content')
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->
<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('settings.banner.managebanner') }}">Manage Banner</a></li>
                        <li class="breadcrumb-item active">Add App Banner</li>
                    </ol>
                </div>
                <h4 class="page-title">Add App Banner</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="header-title m-t-0">Add App Banner</h4>
                <!-- Error massge -->
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <!-- end Error Massage -->

                <form id="myFormId" enctype="multipart/form-data" data-default-url="{{route('settings.banner.insertbanner')}}" action="{{route('settings.banner.insertbanner')}}" method="post" onSubmit="return ImageValidate()">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="userName">App Type<span class="text-danger">*</span></label>
                            <select id="APP_TYPE_ID" class="form-control" name="APP_TYPE_ID" required="">
                                <option value="">Select Type</option>
                                @foreach ($AppTypeResult as $key => $AppResult)
                                <option value="{{$AppResult->APP_TYPE_ID}}">{{$AppResult->APP_TYPE_DESC}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="userName">Banner Category<span class="text-danger">*</span></label>
                            <select id="BANNER_CATEGORY_ID" class="form-control" name="BANNER_CATEGORY_ID" required="">
                                <option value="">Select Category</option>
                                @foreach ($BannerCategoriesResult as $key => $BannerCatResult)
                                <option value="{{$BannerCatResult->BANNER_CATEGORY_ID}}">{{$BannerCatResult->BANNER_CATEGORY_DESC}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="userName">Banner Type<span class="text-danger">*</span></label>
                            <select id="BANNER_TYPE_ID" class="BannerType form-control" name="BANNER_TYPE_ID" required="">
                                <option value="">Select Banner Type</option>
                                @foreach ($BannerTypeResult as $key => $BannerTypeResult)
                                <option value="{{$BannerTypeResult->BANNER_TYPE_ID}}">{{$BannerTypeResult->BANNER_TYPE_DESC}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <div id="1" class="drop-down-show-hide">
                                <label for="userName">Button Redirect Link<span class="text-danger"></span></label>
                                <input type="text" name="BANNER_REDIRECTION_URL" parsley-trigger="change" placeholder="Button Redirect Link" class="form-control" id="userName">
                                <div class="form-row" style="padding-top: 10px; padding-left: 10px;">
                                    <div class="form-group col-md-6">
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" name="RELATIVE_URL" class="custom-control-input" id="RELATIVE_URL" value="1" checked>
                                            <label class="custom-control-label" for="RELATIVE_URL">Relative Url</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" name="EXTERNAL_URL" class="custom-control-input" id="EXTERNAL_URL" value="1" checked>
                                            <label class="custom-control-label" for="EXTERNAL_URL">External Url</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="2" class="drop-down-show-hide">
                                <label for="userName">Select Tounament<span class="text-danger"></span></label>
                                <select id="TOURNAMENT_ID" class="form-control" name="TOURNAMENT_ID">
                                    <option value="">Select Tounament</option>
                                    @foreach ($TournamentResult as $key => $TournamentRes)
                                    <option value="{{$TournamentRes->TOURNAMENT_ID}}">{{$TournamentRes->TOURNAMENT_START_TIME.' - '.$TournamentRes->TOURNAMENT_NAME}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="userName">Select Image (Width=mobile:684PX, Desktop:682PX)<span class="text-danger">*</span></label>
                            <div class="has-preview">
                                <input type="file" name="image" id="file" class="dropify" accept="image/jpg,image/png,image/jpeg"  value="" required="" />
                                <div class="demoInputBox"> <span style="color:red" id="file_error"></span> </div>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="userName">Button Text<span class="text-danger">*</span></label>
                            <input type="text" name="BANNER_LINK_BUTTON_TEXT" id="BANNER_LINK_BUTTON_TEXT" parsley-trigger="change" required placeholder="Button Text" class="form-control" id="userName">
                            <div class="customDatePickerWrapper" style="padding-top: 10px;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="userName">Start Date & Time:<span class="text-danger">*</span></label>
                                        <input name="START_TIME" id="date_from" type="text" class="form-control" placeholder="Select Start Date" value="{{ !empty($params) ? $params['date_from']  : ''  }}" required="">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="userName">End Date & Time:<span class="text-danger">*</span></label>
                                        <input name="END_TIME" type="text" id="date_to" class="form-control" placeholder="Select End Date" value="{{ !empty($params) ? $params['date_from']  : ''  }}" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-2"><label for="userName">Segments<span class="text-danger"></span></label>
                                <select class="form-control" name="SEGMENT_ID" id="SEGMENT_ID" data-toggle="select2">
                                    <option value="0">All Segment</option>
                                    @foreach ($SegmentType as $key => $SegmentTypeVal)
                                    <option value="{{$SegmentTypeVal->SEGMENT_ID}}">{{$SegmentTypeVal->SEGMENT_TITLE}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="custom-control custom-switch" style="padding-top: 10px;">
                                <input type="checkbox" name="STATUS" class="custom-control-input" id="status" value="1" checked>
                                <label class="custom-control-label" for="status">Status</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" name="submit" id="submitbtn" class="btn btn-success waves-effect waves-light formActionButton"><i class="fa fa-save mr-1"></i> Save</button>
                    <button style="display:none" class="btn btn-primary formActionButton mr-1" type="button" disabled>
                        <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                    <button type="reset" onclick="return cancel()" class="btn btn-secondary waves-effect waves-light">Reset</button>
                </form>
            </div> <!-- end card-box -->
        </div>
    </div>
    <!-- end col -->
</div>
<!-- end row -->
</div> <!-- container -->

@endsection
@section('post_js')
<!--  Start Switchery Vendor js -->
<script src="{{ asset('assets/libs/nestable2/nestable2.min.js') }}"></script>

@endsection
@section('js')
<script type="text/javascript">
    // past date disabal 
    $(document).ready(function() {
        var dateToday = new Date();
        var maxDate = $('#date_from').val();
        var date1 = $("#date_from").flatpickr({
            enableTime: !0,
            // defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            minDate: dateToday,
            maxDate: maxDate,
            dateFormat: "Y-m-d H:i:S",
            onChange: function(selectedDates, dateStr, instance) {
                date2.set('minDate', dateStr)
            }
        });

        var date2 = $("#date_to").flatpickr({
            enableTime: !0,
            // defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            dateFormat: "Y-m-d H:i:S",
            onChange: function(selectedDates, dateStr, instance) {
                date1.set('maxDate', dateStr)
            }
        });

    });

    $(document).ready(function() {
        $("#myFormId").validate({

        });
    });

    function ImageValidate() {
        $("#file_error").html("");
        $(".demoInputBox").css("border-color", "#F0F0F0");
        var file_size = $('#file')[0].files[0].size;
        if ($("#myFormId").valid()) {
            if (file_size > 1048576) {
                $("#file_error").html("File size is greater than 1MB");
                $(".demoInputBox").css("border-color", "#FF0000");
                return false;
            } else {
                $('.formActionButton').toggle();
            }
            return true;
        }
    }

    function cancel() {
        c = confirm("Do you really want to Reset?");
        if (c == true) {
            location.reload(true);
        } else {
            return false;
        }
    } 

    $('.drop-down-show-hide').hide();
    $('.BannerType').change(function() {
        if ($('#BannerType option:selected').text() != 'Select Banner Type' && $('#BannerType option:selected').text() != 'Dont Redirect Anywhere') {
            $('.drop-down-show-hide').hide()
            $('#' + this.value).show();
            $('#spacehide').show();
        } else {
            $('#spacehide').hide();
            $('.drop-down-show-hide').hide()
            $('#' + this.value).hide();
        }
    });


    $('#BANNER_LINK_BUTTON_TEXT').bind('keypress', function(e) {
        console.log(e.which);
        if ($('#BANNER_LINK_BUTTON_TEXT').val().length == 0) {
            var k = e.which;
            var ok = k >= 65 && k <= 90 || // A-Z
                k >= 97 && k <= 122 || // a-z
                k >= 48 && k <= 57; // 0-9

            if (!ok) {
                e.preventDefault();
            }
        }
    });

</script>

<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
<script src="{{ asset('assets/libs/dropify/dropify.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/form-fileuploads.init.js') }}"></script>
<script src="{{ asset('assets/libs/parsleyjs/parsley.min.js') }}"></script>
<!-- Validation init js-->
<script src="{{ asset('assets/js/pages/form-validation.init.js') }}"></script>
@endsection