<!--
	|--------------------------------------------------------------------------
	| Author: Nitesh Kumar Jha
	| Purpose: Get all the Manage Banner details and switch on and off for status ,image zoom ,edit banner info ,and add banner
	| Created Date: 13-12-2019 To 16-12-2019
	|--------------------------------------------------------------------------
-->
@extends('bo.layouts.master')
@section('title', "Manage Banner List | PB BO")
@section('pre_css')
<link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('post_css')
<style>
    .bannerimg {
        height: 50px;
    }
</style>
@endsection
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a>Marketing</a></li>
                    <li class="breadcrumb-item active">Manage Banner</li>
                </ol>
            </div>
            <h4 class="page-title">Manage Banner</h4>
        </div>
    </div>
</div>
@if ($errors->any())
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<!-- end page title -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row mb-2">
                    <div class="col-sm-8">
                        <h5 class="font-15 mt-1">Total Banners: <span class="text-danger">@if(!empty($TotalBanner))({{ $TotalBanner }})@else 0 @endif </span>&nbsp;&nbsp;|&nbsp;&nbsp;Active Banners: <span class="text-danger">@if(!empty($countAppBannerStatus))({{ $countAppBannerStatus }})@else 0 @endif </span> </h5>
                    </div>
                    <div class="col-sm-4">
                        <div class="text-sm-right">
                            <a href="{{ route('settings.banner.index') }}" title="Add Banner" class="btn btn-success waves-effect waves-light" data-overlaycolor="#566676"><i class="fa fa-plus mr-1"></i>Add Banner</a>
                        </div>
                    </div>
                    <!-- end col-->
                </div>
                <div class="row mb-3">
                    <div class="col-sm-4">
                        <h5 class="font-18"> </h5>
                    </div>

                    <!-- end col-->
                </div>
                <h4 class="header-title">Manage Banner List</h4>
                <table id="example" class="table dt-responsive nowrap roleTable">
                    <thead>
                        <tr>
                            <th>App Type</th>
                            <th>Banner Category</th>
                            <th>Banner Type</th>
                            <th>Segment</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($AppBannerResult as $key => $AppBannerResu)
                        <tr>
                            <td>{{$AppBannerResu->APP_TYPE_DESC}}</td>
                            <td>{{$AppBannerResu->BANNER_CATEGORY_DESC}}</td>

                            <td>{{$AppBannerResu->BANNER_TYPE_DESC}}</td>
                            <td>
                                <?php $SEGMENT_ID = $AppBannerResu->SEGMENT_ID;
                                $SegmentTypesRes = app\Http\Controllers\bo\settings\ManegeBannerController::getsegmentType($SEGMENT_ID); ?>
                                @if(isset($SegmentTypesRes['SEGMENT_TITLE']) && $SegmentTypesRes['SEGMENT_TITLE']!='')
                                {{ $SegmentTypesRes['SEGMENT_TITLE'] }}
                                @else
                                {{ "-" }}
                                @endif
                            </td>
                            <td>
                                <div class="gal-box">
                                    <a href="{{$AppBannerResu->BANNER_IMAGE_URL}}" class="image-popup" title="">
                                        <img src="{{$AppBannerResu->BANNER_IMAGE_URL}}" class="img-fluid" width="100" alt="work-thumbnail">
                                    </a>
                                </div>
                            </td>
                            <td>
                                @if($AppBannerResu->STATUS == 1)
                                <input type="checkbox" data-id="{{ $AppBannerResu->APP_BANNER_ID }}" class='status_switch' checked data-plugin="switchery" data-color="#64b0f2" data-size="small" />
                                @else
                                <input type="checkbox" data-id="{{ $AppBannerResu->APP_BANNER_ID }}" class='status_switch' data-plugin="switchery" data-color="#64b0f2" data-size="small" />
                                @endif
                            </td>
                            <td>

                                <a href="" class="action-icon font-14 tooltips" data-toggle="modal" data-target="#exampleModalScrollable_{{ $AppBannerResu->APP_BANNER_ID }}" data-plugin="tippy" data-tippy-interactive="true" data-tippy="" data-original-title="Close">
                                    <span class="tooltiptext">View</span>
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a class="action-icon font-14 tooltips" href="{{ route('settings.banner.editappbanner', $AppBannerResu->APP_BANNER_ID) }}" title="Edit"> <span class="tooltiptext">Edit</span>
                                    <i class="fa fa-edit"></i> </a>

                                <!--- Model for enable fiels -->
                                <div class="modal fade" id="exampleModalScrollable_{{ $AppBannerResu->APP_BANNER_ID }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-scrollable" role="document" style="max-width: 971px;">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalScrollableTitle">Manage Banner List Details</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <table id="example" class="table table-striped  nowrap">
                                                    <thead>
                                                        <tr>
                                                            <th>Banner Link Button Text</th>
                                                            <th>Banner Redirection Url</th>
                                                            <th>Relative Url</th>
                                                            <th>External Url</th>
                                                            <th>Tournament Name</th>
                                                            <th>Server Id</th>
                                                            <th>Server Url</th>
                                                            <th>Start Time</th>
                                                            <th>End Time</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>{{$AppBannerResu->BANNER_LINK_BUTTON_TEXT}}</td>
                                                            <td>{{$AppBannerResu->BANNER_REDIRECTION_URL}}</td>

                                                            @if($AppBannerResu->RELATIVE_URL==1)
                                                            <td><span class="badge bg-soft-success text-success shadow-none">Active</span></td>
                                                            @else
                                                            <td><span class="badge bg-warning text-white shadow-none">Deactive</span></td>
                                                            @endif

                                                            @if($AppBannerResu->EXTERNAL_URL==1)
                                                            <td><span class="badge bg-soft-success text-success shadow-none">Active</span></td>
                                                            @else
                                                            <td><span class="badge bg-warning text-white shadow-none">Deactive</span></td>
                                                            @endif
                                                            <td>{{$AppBannerResu->TOURNAMENT_NAME}}</td>
                                                            <td>{{$AppBannerResu->SERVER_ID}}</td>
                                                            <td>{{$AppBannerResu->SERVER_URL}}</td>
                                                            <td>{{$AppBannerResu->START_TIME}}</td>
                                                            <td>{{$AppBannerResu->END_TIME}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!---Model for enable fiels-->
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="mt-2" style="display:flex; justify-content: space-between;" class="mt-2">
                    <div>More Records</div>
                    @if(!empty($AppBannerResult))
                    <div>
                        {{ $AppBannerResult->render("pagination::customPaginationView") }}
                    </div>
                    @endif
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection

@section('js')
<script src="{{ asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/gallery.init.js') }}"></script>
<script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/libs/custombox/custombox.min.js') }}"></script>
<script src="{{ asset('assets/libs/switchery/switchery.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#example').dataTable({
            /* No ordering applied by DataTables during initialisation */
            "order": []
        });
    })

    // This Function Use to Change status
    var switchStatus = false;
    $(document).on('change', ".status_switch", function() {

        if ($(this).is(':checked')) {
            var STATUS = 1;
        } else {
            var STATUS = 0;
        }
        $.ajax({
            "url": "{{url('/settings/banner/bannerChangeStatus')}}",
            "method": "POST",
            "data": {
                "id": $(this).data('id'),
                "STATUS": STATUS
            },
            "dataType": "json",
            success: function(data) {
                $.toast({
                    heading: 'Congrats!!',
                    text: data.message,
                    showHideTransition: 'slide',
                    icon: 'success',
                    loader: true,
                    position: 'top-right',
                    hideAfter: 6000
                });
            }
        });
    });
</script>
@endsection