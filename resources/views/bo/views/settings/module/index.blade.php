@extends('bo.layouts.master')

@section('title', "Module | PB BO")

@section('pre_css')
    <link href="{{ asset('assets/libs/nestable2/nestable2.min.css') }}" rel="stylesheet" />
    <style>
        #menuOrderNestable .dd3-content,
        #moduleNestableOther .dd3-content{
            padding-left: 16px;
        }
    </style>
@endsection

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Module</h4>
        </div>
    </div>
</div>     
<!-- end page title --> 

<div class="row">
    <div class="col-lg-12">
        <div class="text-left" id="nestable_list_menu">
            <button type="button" class="btn btn-primary btn-sm waves-effect mb-3 waves-light" data-toggle="reload" data-form-id="#addEditModuleForm">Add Module</button>
        </div>
    </div> <!-- end col -->
</div>
<!-- End row -->

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="row">
                <div class="col-md-6 mb-4">
                    {{-- <div>
                        <h4 class="header-title">Module List</h4>
                        <div class="custom-dd-empty dd card" id="menuOrderNestable">
                            <div class="card-body">
                                @include('bo.views.settings.module.partials.index', ['items'=>$module_menu])
                            </div>
                        </div>
                    </div> --}}
                    
                    <div>
                        <h4 class="header-title">Internal Modules List</h4>
                        <div class="custom-dd-empty dd card" id="moduleNestableOther">
                            <div class="card-body">
                                @if(!$module_none_menu->isEmpty())
                                @include('bo.views.settings.module.partials.index', ['items'=>$module_none_menu])
                                @else
                                <div class="text-center">
                                    No Internal Modules Found
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div><!-- end col -->
                <div class="col-md-6 mb-4">
                    <h4 class="header-title">Add/Update Module</h4>
                    <div class="card">
                        <div class="card-body">
                            <form data-default-url="{{ route('settings.module.store') }}" action="{{ route('settings.module.store') }}" method="post" id="addEditModuleForm">
                                @csrf
                                <input type="hidden" name="id" data-default-reset="">
                                <div class="form-group mb-3">
                                    <label>Module Key</label>
                                    <input type="text" name="module_key" class="form-control" placeholder="Module key">
                                </div>
                                <div class="form-group mb-3">
                                    <label>Module Name</label>
                                    <input type="text" name="display_name" class="form-control" placeholder="Module Name" default-prop-readonly-clear>
                                </div>
                                
                                <div class="form-group mb-3">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description" rows="5"></textarea>
                                </div>
                                
                                {{-- <div class="custom-control custom-switch">
                                    <input type="checkbox" name="status" class="custom-control-input" id="status" value="1" checked>
                                    <label class="custom-control-label" for="status">Status</label>
                                </div> --}}
                                <button type="submit" class="btn btn-primary mt-2">Submit</button>
                                <a href="javascript:;" class="btn btn-primary mt-2" data-toggle="reload" data-form-id="#addEditModuleForm"><i class="mdi mdi-refresh"></i> Reset</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- end row -->
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>
@endsection

@section('js')
<!-- Plugins js-->
<script src="{{ asset('assets/libs/nestable2/nestable2.min.js') }}"></script>

<!-- Nestable init-->
<script src="{{ asset('assets/js/pages/nestable.init.js') }}"></script>
<script src="{{ asset('js/bo/module_managment.js') }}"></script>
@endsection