<ol class="dd-list moduleList">
    @foreach ($items as $key => $item)
    @php
        $module = $item->getTable() == "bo_admin_menus" ? $item->module : $item;
    @endphp
    <li class="dd-item dd3-item moduleItem">
        <div class="dd3-content" data-obj="{{ base64_encode(json_encode($module)) }}">
            {{ $module->display_name }}
            <div class="float-right ">
                <a href="javascript:;"><span class="pl-1 moduleEditViewButton"><i class="fas fa-edit"></i></span></a>
            </div>
        </div>
        @if($item->getTable() == "bo_admin_menus")
        @if(!$item->childrenWithModule->isEmpty())
        @include('bo.views.settings.module.partials.index', ['items' => $item->childrenWithModule])
        @endif
        @endif
        @endforeach
    </li>
</ol>