@extends('bo.layouts.master')
@section('title', "Affiliate Tax Summary| PB BO")
@section('pre_css')
    <link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Plugins css -->
    <link href="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">.customselect{ float: none; height: auto !important; line-height: 35px; border: 1px solid #ced4da;border-radius: .2rem;}.customselect ul.list{ width: 100% }
.textwarpcontainer{ max-width: 200px }
 .textoverflow{-webkit-line-clamp: 1;-webkit-box-orient: vertical;overflow: hidden;
text-overflow: ellipsis;display: -webkit-box;width: 100%;}
.toggle-password {position: absolute; top: 11px; right: 10px;    z-index: 4;}
.multiplecustom .btn-light{ background-color: transparent !important;-webkit-box-shadow: 0 1px 4px 0 rgba(0,0,0,.1) !important;box-shadow: 0 1px 4px 0 rgba(0,0,0,.1) !important;  border: 1px solid #ced4da !important;}
.imgcontainer{ padding: 5px !important }
</style>
@endsection
@section('content')
<div class="row">
<div class="col-12">
<div class="page-title-box">
<div class="page-title-right">
<ol class="breadcrumb m-0">
<li class="breadcrumb-item"><a href="">Affiliate</a></li>
<li class="breadcrumb-item active">Affiliate Tax Summary</li>
</ol>
</div>
<h4 class="page-title">Affiliate Tax Summary</h4></div>
</div>
</div>
<div class="row">
    <div class="col-12">
            <div class="card">
                    <div class="card-body">
                        <form id="userSearchForm" action="{{ route('affiliate.affiliateTaxSummary.affiliateTdsSearch') }}{{ !empty($params) ? empty($params['page']) ? '' : "?page=".$params['page'] : '' }}" method="post" >
                         @csrf
                            <div class="row align-items-center">
                                    <div class="form-group col-md-2">
                                        <label class="mb-1">Search By: </label>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div class="radio radio-info form-check-inline">
                                            <input data-default-checked="" type="radio" id="USERNAME" value="USERNAME" name="search_by" checked="" {{ !empty($params) ? $params['search_by'] == "USERNAME" ? "checked" : '' : "" }}>
                                            <label for="USERNAME"> Username </label>
                                        </div>
                                        <div class="radio radio-info form-check-inline">
                                            <input type="radio" id="EMAIL_ID" value="EMAIL_ID" name="search_by" {{ !empty($params) ? $params['search_by'] == "EMAIL_ID" ? "checked" : '' : "" }}>
                                            <label for="EMAIL_ID"> Email </label>
                                        </div>
                                        <div class="radio radio-info form-check-inline">
                                            <input type="radio" id="CONTACT" value="CONTACT" name="search_by" {{ !empty($params) ? $params['search_by'] == "CONTACT" ? "checked" : '' : "" }}>
                                            <label for="CONTACT"> Contact No </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="sr-only" for="search_by_value">Search Input</label>
                                        <input type="text" class="form-control" name="search_by_value" id="search_by_value" placeholder="Search by Username, Email, Contact No " value="{{ !empty($params) ? $params['search_by_value']  : ''  }}">
                                    </div>
                            </div>
                            <div class="row align-items-center">

                            <div class="form-group col-md-3">
                            <label > Partner Name</label>
                                <select class="form-control select2-multiple" data-toggle="select2" name="PARTNER_ID[]" id="PARTNER_ID" multiple="multiple" data-placeholder="Select">
                                @foreach($PartnerResult as $key => $presult)

                                    <option value="{{ $presult->PARTNER_ID }}" {{ !empty($params['PARTNER_ID']) ? in_array($presult->PARTNER_ID,$params['PARTNER_ID']) ? "selected" : ''  : ''  }}>{{ $presult->PARTNER_NAME }}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                    <label> Pan No:</label>
                                        <input type=""  class="form-control" name="PAN_NUMBER" id="PAN_NUMBER" placeholder="" value="{{ !empty($params) ? $params['PAN_NUMBER']  : ''  }}">
                                    </div>
                                    <div class="form-group col-md-3">
                                    <label> Internal Reference No:</label>
                                        <input type=""  class="form-control" name="INTERNAL_REFERENCE_NO" id="INTERNAL_REFERENCE_NO" placeholder="" value="{{ !empty($params) ? $params['INTERNAL_REFERENCE_NO']  : ''  }}">
                                    </div>
                                    <div class="form-group col-md-3 multiplecustom">
                                        <label>TDS Status:</label>
                                        <select class="form-control selectpicker" name="transaction_type[]"  id="transaction_type" multiple="" data-style="btn-light">
                                        <option value="231" {{ !empty($params['transaction_type']) ? in_array('231',$params['transaction_type']) ? "selected" : ''  : ''  }}>Pending</option>
                                        <option value="233" {{ !empty($params['transaction_type']) ? in_array('233',$params['transaction_type']) ? "selected" : ''  : ''  }}>Approved</option>
                                        <option value="232" {{ !empty($params['transaction_type']) ? in_array('232',$params['transaction_type']) ? "selected" : ''  : ''  }}>Rejected</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="row customDatePickerWrapper">
                                    <div class="form-group col-md-4">
                                        <label>From: </label>
                                        <input name="START_DATE_TIME" type="text" class="form-control customDatePicker from flatpickr-input active" placeholder="Select From Date" value="{{ !empty($params) ? $params['START_DATE_TIME']  : ''  }}" readonly="readonly">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>To: </label>
                                        <input name="END_DATE_TIME" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="{{ !empty($params) ? $params['END_DATE_TIME']  : ''  }}" readonly="readonly">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Date Range: </label>
                                        <select name="date_range" id="date_range" class="customselect   formatedDateRange" data-plugin="customselect">
                                            <option data-default-selected="" value="" selected="">Select</option>
                                            <option value="1">Today</option>
                                            <option value="2">Yesterday</option>
                                            <option value="3">This Week</option>
                                            <option value="4">Last Week</option>
                                            <option value="5">This Month</option>
                                            <option value="6">Last Month</option>
                                        </select>
                                    </div>
                            </div>
                                <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                                <a href="{{ url('affiliate/affiliateTaxSummary') }}" class="btn btn-dark waves-effect waves-light ml-2"><i class="mdi mdi-replay mr-1"></i>Reset</a>
                        </form>
                    </div> <!-- end card-body-->
            </div> <!-- end card-->
    </div> <!-- end col -->
</div>
<?php //print_r($allTransactionSearchData); ?>
@if(!empty($allTransactionSearchData))
<div class="row">
    <div class="col-12">
        <div class="card">

        <div class="card-header bg-dark text-white">
        <div class="card-widgets">
               <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>                                             
            </div>
                            <h5 class="card-title mb-0 text-white">Affiliate Tax Summary</h5>
                            </div>
                            <div id="cardCollpase7" class="collapse show">
            <div class="card-body">

   <div class="row mb-2">
        <div class="col-sm-8">
       <h5 class="font-15 mt-1">Total Withdraw Amount: <span class="text-danger">@if(!empty($totalAmount))({{ $totalAmount }})@else 0 @endif </span>&nbsp;&nbsp;|&nbsp;&nbsp;TDS Amount: <span class="text-danger">({{ $TDSAmount }})</span> </h5> 
      
    </div>
        <div class="col-sm-4">
            <div class="text-sm-right">
            @if(!empty($allTransactionSearchData[0]))   
                <a href="{{ route('affiliate.affiliateTaxSummary.exportExcel')."?dateFrom=".$params['START_DATE_TIME']."&dateTo=".$params['END_DATE_TIME']."&search_by=".$params['search_by']."&search_by_value=".$params['search_by_value']."&PAN_NUMBER=".$params['PAN_NUMBER']."&INTERNAL_REFERENCE_NO=".$params['INTERNAL_REFERENCE_NO']}}" > <button type="button" class="btn btn-light mb-1"><i class="fas fa-file-excel mr-1" style="color: #34a853;"></i>Export Excel</a>
             @endif
            </div>
        </div>
        <!-- end col-->
    </div>
        <table id="basic-datatable" class="table dt-responsive nowrap w-100">
        <thead>
            <tr>
                <th>Username</th>
                <th>Partner Name</th>
                <th>Reference No</th>
                <th>Name</th>
                <th>Email ID</th>
                <th>Contact</th>
                <th>PAN Number</th>
                <th>Withdraw Amount</th>
                <th>TDS Amount</th>
                <th>Date & Time</th>
                <th>TDS Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($allTransactionSearchData as $key => $result)
            <tr>
                <td>{{ $result->USERNAME }}</td>
                <td>{{ $result->PARTNER_NAME }}</td>
                <td>{{ $result->INTERNAL_REFERENCE_NO }}</td>
                <td>{{ $result->FIRSTNAME }} &nbsp; {{ $result->LASTNAME }}  </td>
                <td>{{ $result->EMAIL_ID }}</td>
                <td>{{ $result->CONTACT }}</td>
                <td>{{ $result->PAN_NUMBER }}</td>
                <td>{{ $result->PAYMENT_TRANSACTION_AMOUNT }}</td>
                <td>{{ $result->AMOUNT }}</td>
                <td>{{ $result->CREATED_TIMESTAMP }}</td>
                <td>
                @if($result->PAYMENT_TRANSACTION_STATUS=='231')
                        <span class="badge bg-warning text-white shadow-none">Pending</span>
                   @elseif($result->PAYMENT_TRANSACTION_STATUS=='233')
                    <span class="badge bg-soft-success text-success shadow-none">Approved</span>
                   @elseif($result->PAYMENT_TRANSACTION_STATUS=='232')
                      <span class="badge bg-soft-danger text-danger shadow-none">Rejected</span>
                @endif
              </td>
            </tr>
            @endforeach  
         </tbody>
        </table>
                <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" style="display:flex; justify-content: space-between;" class="mt-2">
                    @if(!empty($allTransactionSearchData))
                    <div>More Records</div>
                        <div>
                        {{ $allTransactionSearchData->render("pagination::customPaginationView") }}
                        </div>
                    @endif
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->  </div>
</div>
 @endif
@endsection
@section('js')
<script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script> 
<!---  Nice Select menu -->
<script src="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.js') }}"></script>
<script src="{{ asset('assets/libs/switchery/switchery.min.js') }}"></script>
<script src="{{ asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-mockjax/jquery-mockjax.min.js') }}"></script>
<script src="{{ asset('assets/libs/autocomplete/autocomplete.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
<!-- Init js-->
<script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>
@endsection