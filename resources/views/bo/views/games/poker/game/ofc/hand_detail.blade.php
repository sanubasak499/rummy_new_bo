@extends('bo.layouts.master')

@section('title', "OFC Game Hand Detail | PB BO")

@section('pre_css')
    
@endsection
    
@section('post_css')
    <link href="{{ asset('assets/bo/pages/ofc_game_hand_history/ofc_game_hand_history.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">OFC Game Hand History</h4>
        </div>
    </div>
</div>
<!-- end page title --> 

<div class="row">
    @foreach ($ofGameHandHistory as $hand)
    {{-- {{ dd(json_decode($ofGameHandHistory->first()->PLAYER_DATA)) }} --}}
    @php
        $player_data = json_decode($hand->PLAYER_DATA);
        $exceptNumber = ['1'=>'A','11'=>"J",'12'=>"Q",'13'=>"K"];
    @endphp    
    <div class="col-lg-4">
        <div class="card-box ofc-user-hand-data">
            <div class="font-weight-bold">{{ $hand->user->USERNAME }}</div>
            <div>{{ $hand->INTERNAL_REFERENCE_NO }}</div>
            <div>rake: {{ $hand->user->ofc_game_transaction_history->first()->REAL_REVENUE }} ({{ $hand->user->ofc_game_transaction_history->first()->ACTUAL_REVENUE."+".$hand->user->ofc_game_transaction_history->first()->SERVICE_TAX }})</div>
            {{-- <div>{{ $hand->STAKE }}</div>
            <div>{{ $hand->WIN !='0.00' ? "LOSS" : "WIN(".$hand->WIN.")" }}</div>
            <div>{{ $hand->REVENUE }}</div> --}}
            <div class="row">
                <div class="col-sm-8">
                    <div class="card-hand-data mt-2">
                        <div class="playing-card-wrapper">
                            @foreach ($player_data->frontHand->cards as $card)     
                            <div class="playing-card card--{{ $card->suitAsString }}" data-card-value="{{ array_key_exists($card->valueAsString,$exceptNumber) ? $exceptNumber[$card->valueAsString] : $card->valueAsString }}">
                                <div class="card-symbol"></div>
                            </div>
                            @endforeach
                        </div>
                        <div class="hand-win-type">{{ str_replace('_', ' ', $player_data->frontHand->handResult->winType) }} ({{ $player_data->frontHand->royaltyPoints }})</div>
                    </div>
                    <div class="card-hand-data">
                        <div class="playing-card-wrapper">
                            @foreach ($player_data->middleHand->cards as $card)     
                            <div class="playing-card card--{{ $card->suitAsString }}" data-card-value="{{ array_key_exists($card->valueAsString,$exceptNumber) ? $exceptNumber[$card->valueAsString] : $card->valueAsString }}">
                                <div class="card-symbol"></div>
                            </div>
                            @endforeach
                        </div>
                        <div class="hand-win-type">{{ str_replace('_', ' ', $player_data->middleHand->handResult->winType) }} ({{ $player_data->middleHand->royaltyPoints }})</div>
                    </div>
                    <div class="card-hand-data">
                        <div class="playing-card-wrapper">
                            @foreach ($player_data->backHand->cards as $card)     
                            <div class="playing-card card--{{ $card->suitAsString }}" data-card-value="{{ array_key_exists($card->valueAsString,$exceptNumber) ? $exceptNumber[$card->valueAsString] : $card->valueAsString }}">
                                <div class="card-symbol"></div>
                            </div>
                            @endforeach
                        </div>
                        <div class="hand-win-type">{{ str_replace('_', ' ', $player_data->backHand->handResult->winType) }} ({{ $player_data->backHand->royaltyPoints }})</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="discard-card-container-wrapper">
                        <div class="card-hand-data discard-card-wrapper">
                            <div class="playing-card-wrapper">
                                @php
                                    $DISCARD_CARD = explode(',',$hand->DISCARD_CARD);
                                @endphp
                                @foreach ($DISCARD_CARD as $card)
                                @php
                                    $suit= explode('_',$card)[0];
                                    $number=explode('_',$card)[1];
                                @endphp     
                                <div class="playing-card card--{{ $suit }}" data-card-value="{{ array_key_exists($number,$exceptNumber) ? $exceptNumber[$number] : $number }}">
                                    <div class="card-symbol"></div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <span style="white-space: nowrap;">Discard Cards</span>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
    @endforeach
    
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            @foreach ($ofcGameHistory as $item)    
            <div class="searchWrap1 mb-2">
                <div class="hand-common-data"><span><b>Win Amount:</b> </span><span class="text-success pr-2">{{ $item->TOTAL_WIN }}, </span></div>
                <div class="hand-common-data"><span><b>Revenue:</b> </span><span class="text-danger  pr-2">{{ $item->TOTAL_REVENUE }}, </span></div>
                <div class="hand-common-data"><span><b>Rake Cap:</b> </span><span class="text-danger  pr-2">{{ $ofGameHandHistory->first()->tournament->RAKE_CAP }}, </span></div>
                <div class="hand-common-data"><span><b>Point Value:</b> </span><span class="text-danger  pr-2">{{ $ofGameHandHistory->first()->tournament->POINT_VALUE }}, </span></div>
                <div class="hand-common-data"><span><b>Rake:</b> </span><span class="text-danger  pr-2">{{ $ofGameHandHistory->first()->tournament->RAKE }},</span></div>
                <div class="hand-common-data"><span><b>Rake Applied:</b> </span><span class="text-danger  pr-2">{{ $item->APP_RAKE_PERCENTAGE }},</span></div>
                <div class="hand-common-data"><span><b>Buy-In:</b> </span><span class="text-danger  pr-2">{{ $ofGameHandHistory->first()->tournament->MAXBUYIN }}</span></div>
            </div>
            @endforeach
            <table id="basic-datatable" class="table dt-responsive nowrap">
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Start Balance</th>
                        <th>Royalities</th>
                        <th>Hand points</th>
                        <th>Scoop</th>
                        {{-- <th>Total</th> --}}
                        <th>P/L points</th>
                        <th>PL Stake</th>
                        <th>PL Win</th>
                        <th>Real Revenue</th>
                        <th>Bonus Revenue</th>
                        <th>Actual Revenue</th>
                        <th>Service Tax</th>
                        <th>End Balance</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($ofGameHandHistory as $hand)
                    @php
                        $player_data = json_decode($hand->PLAYER_DATA);
                        // dd($player_data);
                    @endphp
                    <tr>
                        <td>{{ $hand->user->USERNAME }}</td>
                        <td>{{ $player_data->previousBalance }}</td>
                        <td>{{ $player_data->totalRoyaltyPoints }}</td>
                        <td>{{ $player_data->totalHandPoints }}</td>
                        <td>{{ $player_data->scoopPoints }}</td>
                        {{-- <td>{{ $player_data->totalWinPoints }}</td> --}}
                        <td>{{ $player_data->totalPoints }}</td>
                        <td>{{ $player_data->stake }}</td>
                        <td>{{ $hand->user->ofc_game_transaction_history->first()->WIN ?? "Something went wrong"}}</td>
                        <td>{{ $hand->user->ofc_game_transaction_history->first()->REAL_REVENUE ?? "Something went wrong"}}</td>
                        <td>{{ $hand->user->ofc_game_transaction_history->first()->BONUS_REVENUE ?? "Something went wrong"}}</td>
                        <td>{{ $hand->user->ofc_game_transaction_history->first()->ACTUAL_REVENUE ?? "Something went wrong"}}</td>
                        <td>{{ $hand->user->ofc_game_transaction_history->first()->SERVICE_TAX ?? "Something went wrong"}}</td>
                        <td>{{ $player_data->currentBalance }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('js')
@endsection