@extends('bo.layouts.master')

@section('title', "OFC Game Hand History | PB BO")

@section('pre_css')    
@endsection

@section('content')

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">OFC Game Hand History</h4>
        </div>
    </div>
</div>
<!-- end page title --> 

<div class="row">
    <div class="col-lg-12">
        <!-- Portlet card -->
        <div class="card">
            <div class="card-header bg-blue py-2 text-white">
                <div class="card-widgets">
                    <a href="javascript:;" data-toggle="reload" data-form-id="#userSearchForm"><i class="mdi mdi-refresh"></i></a>
                    <a data-toggle="collapse" href="#cardCollpase5" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class="mdi mdi-minus"></i></a>
                </div>
                <h5 class="card-title mb-0 text-white">OFC Game Hand History</h5>
            </div>
            <div id="cardCollpase5" class="collapse show">
                <div class="card-body">
                    <form id="userSearchForm" data-default-url="{{ route('games.poker.game.ofc.viewofcgames') }}" action="{{ route('games.poker.game.ofc.viewofcgames') }}{{ !empty($params) ? empty($params['page']) ? "" : "?page=".$params['page'] : '' }}" method="post">
                        @csrf
                        <div class="form-row align-items-center">
                            <div class="form-group col-md-4">
                                <label for="TABLE_ID">Table Name</label>
                                <input type="text" class="form-control" name="TABLE_ID" id="TABLE_ID" placeholder="Search Input" value="{{ !empty($params) ? $params['TABLE_ID'] : '' }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="GAME_ID">Game ID</label>
                                <input type="text" class="form-control" name="GAME_ID" id="GAME_ID" placeholder="Search Input" value="{{ !empty($params) ? $params['GAME_ID'] : '' }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Fantasy Type</label>
                                <select name="FANTASY_TYPE" id="FANTASY_TYPE" class="form-control formatedDateRange">
                                    <option data-default-selected value="" {{ !empty($params) ? $params['FANTASY_TYPE'] == "" ? "selected" : ''  : 'selected'  }}>No Fantasy</option>
                                    <option value="1" {{ !empty($params) ? $params['FANTASY_TYPE'] == 1 ? "selected" : ''  : ''  }}>Limited Fantasy</option>
                                    <option value="2" {{ !empty($params) ? $params['FANTASY_TYPE'] == 2 ? "selected" : ''  : ''  }}>Unlimited Fantasy</option>
                                    <option value="3" {{ !empty($params) ? $params['FANTASY_TYPE'] == 3 ? "selected" : ''  : ''  }}>Progressive</option>
                                    <option value="4" {{ !empty($params) ? $params['FANTASY_TYPE'] == 4 ? "selected" : ''  : ''  }}>Ultimate</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row align-items-center">
                            <div class="form-group col-md-4">
                                <label for="PLAYER_ID">Player Name</label>
                                <input type="text" class="form-control" name="PLAYER_ID" id="PLAYER_ID" placeholder="Search Input" value="{{ !empty($params) ? $params['PLAYER_ID'] : '' }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="HAND_ID">Hand ID</label>
                                <input type="text" class="form-control" name="HAND_ID" id="HAND_ID" placeholder="Search Input" value="{{ !empty($params) ? $params['HAND_ID'] : '' }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Currency Type</label>
                                <select name="CURRENCY_TYPE" id="CURRENCY_TYPE" class="form-control formatedDateRange">
                                    <option data-default-selected value="" {{ !empty($params) ? $params['CURRENCY_TYPE'] == "" ? "selected" : ''  : 'selected'  }}>No Fantasy</option>
                                    @foreach ($coin_types as $type)    
                                    <option value="{{ $type->COIN_TYPE_ID }}" {{ !empty($params) ? $params['CURRENCY_TYPE'] == $type->COIN_TYPE_ID  ? "selected" : ''  : ''  }}>{{ $type->NAME }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row align-items-center">
                            <div class="form-group col-md-4">
                                <label>Status</label>
                                <select name="STATUS" id="STATUS" class="form-control formatedDateRange">
                                    <option data-default-selected value="8" {{ !empty($params) ? $params['STATUS'] == "8" ? "selected" : ''  : 'selected'  }}>Completed</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Fantasy Game</label>
                                <select name="FANTASY_GAME" id="FANTASY_GAME" class="form-control formatedDateRange">
                                    <option data-default-selected value="" {{ !empty($params) ? $params['FANTASY_GAME'] == "" ? "selected" : ''  : 'selected'  }}>No Fantasy</option>
                                    <option value="1" {{ !empty($params) ? $params['FANTASY_GAME'] == 1 ? "selected" : ''  : ''  }}>Fantasy Round</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Dead Hand</label>
                                <select name="DEAD_HAND" id="DEAD_HAND" class="form-control formatedDateRange">
                                    <option data-default-selected value="" {{ !empty($params) ? $params['DEAD_HAND'] == "" ? "selected" : ''  : 'selected'  }}>No Fantasy</option>
                                    <option value="1" {{ !empty($params) ? $params['DEAD_HAND'] == 1 ? "selected" : ''  : ''  }}>Dead Hand</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-row customDatePickerWrapper">
                            <div class="form-group col-md-4">
                                <label>From: </label>
                                <input name="date_from" type="text" class="form-control customDatePicker from" placeholder="Select From Date" value="{{ !empty($params) ? $params['date_from']  : ''  }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>To: </label>
                                <input name="date_to" type="text" class="form-control customDatePicker to" placeholder="Select To Date" value="{{ !empty($params) ? $params['date_to']  : ''  }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Date Range: </label>
                                <select name="date_range" id="date_range" class="form-control formatedDateRange">
                                    <option data-default-selected value="" {{ !empty($params) ? $params['date_range'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                                    <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                                    <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                                    <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                                    <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                                    <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                                    <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                                </select>
                            </div>
                        </div>
                        
                        {{-- <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>status: </label>
                                <select name="status" class="form-control">
                                    <option data-default-selected value="" {{ !empty($params) ? $params['status'] == "" ? "selected" : '' :  'selected'  }}>Select</option>
                                    <option value="none" {{ !empty($params) ? $params['status'] == "none" ? "selected" : ''  : ''  }}>Inactive</option>
                                    <option value="1" {{ !empty($params) ? $params['status'] == 1 ? "selected" : ''  : ''  }}>Active</option>
                                    <option value="2" {{ !empty($params) ? $params['status'] == 2 ? "selected" : ''  : ''  }}>Blocked</option>
                                </select>
                            </div>
                        </div> --}}
                        

                        <button type="submit" class="btn btn-primary waves-effect waves-light">Search</button>
                        <button data-toggle="reload" data-form-id="#userSearchForm" type="button" class="btn btn-primary waves-effect waves-light">Clear</button>
                    </form> 
                </div>
            </div>
        </div> <!-- end card-->
    </div><!-- end col -->
</div>

@if(!empty($searchGameData))
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">OFC Game Hand History</h4>
                <p class="searchWrap1">
                    <span><b>Total Win:</b> </span><span class="text-success pr-2">{{ $totalSearchData[0]->TOTAL_WIN }}, </span>
                    <span><b>Total Gross Rake:</b> </span><span class="text-danger  pr-2">{{ $totalSearchData[0]->TOTAL_REAL_RAKE }}, </span>
                    <span><b>Total Bonus Rake:</b> </span><span class="text-danger  pr-2">{{ $totalSearchData[0]->TOTAL_BONUS_RAKE }}, </span>
                    <span><b>Total Net Rake:</b> </span><span class="text-danger  pr-2">{{ ($totalSearchData[0]->TOTAL_REVENUE)-($totalSearchData[0]->TOTAL_BONUS_RAKE)-($totalSearchData[0]->TOTAL_SERVICE_TAX) }}, </span>
                    <span><b>Total GST:</b> </span><span class="text-danger  pr-2">{{ $totalSearchData[0]->TOTAL_SERVICE_TAX }}</span>
                </p>
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Table Id</th>
                            <th>Game Id</th>
                            <th>Status</th>
                            <th>Date & Time</th>
                            <th>Gross Rake</th>
                            <th>Bonus Rake</th>
                            <th>GST</th>
                            <th>Net Rake</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($searchGameData as $key => $game)
                        <tr>
                            <td>{{ $game->TOURNAMENT_NAME }}</td>
                            <td>{{ $game->PLAY_GROUP_ID }}</td>
                            <td>Completed</td>
                            <td>{{ date("d/m/Y h:i:s", strtotime($game->STARTED)) }}</td>
                            <td>{{ $game->TOTAL_REVENUE }}</td>
                            <td>{{ $game->BONUS_REVENUE }}</td>
                            <td>{{ $game->SERVICE_TAX }}</td>
                            <td>{{ ($game->TOTAL_REVENUE)-($game->BONUS_REVENUE)-($game->SERVICE_TAX) }}</td>
                            <td>
                                <a target="_blank" rel="noopener noreferrer" href="{{ route('games.poker.game.ofc.ofc_game_detail', $game->PLAY_GROUP_ID) }}" title="hand details">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" style="display:flex; justify-content: space-between;" class="mt-2">
                    <div>More Records</div>
                    <div>
                        {{ $searchGameData->render("pagination::customPaginationView") }}
                    </div>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endif
@endsection

@section('js')
@endsection