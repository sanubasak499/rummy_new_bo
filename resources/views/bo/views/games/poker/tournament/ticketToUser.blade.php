@extends('bo.layouts.master')

@section('title', "Ticket To User | PB BO")

@section('pre_css')
    <link href="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{ asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/libs/dropify/dropify.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    
@endsection

@section('post_css')
<style>
    .ms-container{
        max-width: 650px;
    } 
    .InvalidRow {
        color:red;
    } 
    .ValidRow {
        color:green;
    }
    .formtext {
    font-size: 11px;
    font-style: italic;
    color: red;
    padding: 6px 0 0 0;
    margin: 0;
    line-height: 13px;
}
      
</style>
@endsection

@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                        <li class="breadcrumb-item active">Register Player</li>
                    </ol>
                </div>
                <h4 class="page-title">Register Player into tournament</h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                <h4 class="header-title">Register Player into tournament</h4>
                
               @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close modalclose" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{ $message }}
                </div>
                @endif

                <!-- popup modal -->
                @if (!empty($sucess))
                <div id="accordion-modal" class="modal fade show" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: block;" aria-modal="true">
                    <div class="modal-dialog modal-full">
                        <div class="modal-content p-0">
                            <div id="accordion">
                                <div class="card mb-0">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="m-0">
                                            <a href="#collapseOne" class="text-dark" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne">
                                                Ticket Status
                                            </a>
                                        </h5>
                                        <button type="button" class="close modalclose" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="table-responsive" style="overflow: auto; height: 400px">
                                    <table class="table table-borderless table-hover table-centered m-0">

                                        <thead class="thead-light">
                                            <tr>
                                                <th>Userame</th>
                                                <th>Tour. Name</th>
                                                <th>Status</th>
                                                <th>User ID</th>
                                                <th>Tour. ID</th>
                                                <th>REASON</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($sucess as $sucessValue)
                                               
                                               
                                                @if($sucessValue['ticketStatus'] == '1')
                                                    <tr class="ValidRow">
                                                @else
                                                    <tr class="InvalidRow">
                                                @endif
                                                    <td>
                                                        {{$sucessValue['userName']}}
                                                    </td>
                                                    <td>
                                                        {{$sucessValue['tournamentName']}}
                                                    </td>
                                                    @switch($sucessValue['ticketStatus'])
                                                        @case(0)
                                                            <td>Invalid user</td>
                                                            @break
                                                        @case(1)
                                                            <td>Ticket issued</td>
                                                            @break
                                                        @case(2)
                                                            <td>Already registered</td>
                                                            @break
                                                        @case(3)
                                                            <td>Invalid tournament</td>
                                                            @break
                                                        @case(4)
                                                            <td>Invalid Reason</td>
                                                            @break
                                                        @default
                                                            <td>NA</td>
                                                    @endswitch

                                                    <td>
                                                        {{$sucessValue['userId']}}
                                                    </td>
                                                    <td>
                                                        {{$sucessValue['tournamentId']}}
                                                    </td>
                                                    <td>
                                                        {{$sucessValue['ticketReason']}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary modalclose" data-dismiss="modal">Close</button>
                        
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endif
                <!-- messgae modal end -->
                

                
                <p class="sub-header">Select Single/Multiuser or Bulk Upload.</p>

                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a href="#home-b2" data-toggle="tab" aria-expanded="false" class="nav-link active">Single/Multi User</a>
                    </li>
                    <li class="nav-item">
                        <a href="#profile-b2" data-toggle="tab" aria-expanded="true" class="nav-link">
                            Bulk Upload
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="home-b2">
                        <div class="row">
                            <div class="col-lg-12">
                                <form method="post" action="{{ route('games.poker.tournament.registerplayer') }}">
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-8">
                                            <label for="simpleinput">Select Tournaments</label>
                                            <select multiple="multiple" class="multi-select" id="my_multi_select2" name="tour_id[]" data-plugin="multiselect" data-selectable-optgroup="true">
                                                <optgroup label="Select All">
                                                    @if($allUpcommingTour)
                                                    @foreach($allUpcommingTour as $tournament)
                                                    <option value={{$tournament->TOURNAMENT_ID}}>{{$tournament->TOURNAMENT_NAME}}({{$tournament->TOURNAMENT_ID}})--{{$tournament->TOURNAMENT_START_TIME}}</option>
                                                    @endforeach
                                                    @endif
                                                </optgroup>
                                                
                                            </select>
                                        </div>

                                        <div class="form-group col-md-4" id="wrap-input">
                                            <label for="example-select">Select User</label>
                                            <div class="input-group "><input name="user_name[]"type="text" class="form-control username" placeholder="Enter Username" aria-label="Recipient's username" required><div class="input-group-append"><button type="button" id="add_field_button" class="btn btn-blue btn-xs waves-effect waves-light mr-1"><i class="fa fa-plus"></i> </button>
                                            </div>
                                            <div class="invalid-feedback">Username Not exist..</div>
                                        </div>&nbsp
                                    

                                        </div>
                                    </div>

                                    <div class="form-group  multiplecustom col-md-4">
                                        <label>Reason: <span class="text-danger">*</span></label>
                                        <select class="customselect" name="reason" id="reason" data-plugin="customselect">
                                            <option value="" selected="">Select</option>
                                            @if($action_reasons)
                                                @foreach($action_reasons as $reasons)
                                                    <option value="{{ $reasons->ACTIONS_REASON }} @if(old('reason')) selected='selected' @endif">{{  $reasons->ACTIONS_REASON  }}</option>
                                                @endForeach
                                            @endif
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label title="Toggle this button to make auto register the selected tournament" data-plugin="tippy" data-tippy-arrow="true" data-tippy-animation="fade">Auto Register</label>
                                        {{-- <input type="checkbox"  name="autoreg" data-plugin="switchery" value="on" data-color="#1bb99a" data-size="small" /> --}}
                                        <label class="switches">
                                            <input name="autoreg" type="checkbox" value ="on">
                                            <span class="slider round"></span>
                                          </label>
                                    </div>
                                    
                                    <button title="Click this button to assign ticket to selected users" data-plugin="tippy" data-tippy-arrow="true" data-tippy-animation="fade" type="submit"  class="btn btn-success waves-effect waves-light add_field_button"><i class="fas fa-save mr-2"></i>Submit</button>
                                    <a href="{{ route('games.poker.tournament.registerplayer') }}"><button title="Click this button to reset all data filled by you" data-plugin="tippy" data-tippy-arrow="true" data-tippy-animation="fade"type="button" class="btn btn-secondary waves-effect waves-light"><i class="fas fa-eraser mr-1"></i> Clear</button></a>
                                    {{-- <button type="button" class="btn btn-warning waves-effect waves-light"><i class="fas fa-redo-alt mr-2"></i> Reset</button> --}}
                                </form>
                            </div> 
                        <!-- end col -->
                        </div>
                    </div>
                    <div class="tab-pane" id="profile-b2">
                        <div class="row">
                            <div class="col-lg-12">
                                <form method="post" action="{{ route('games.poker.tournament.registerplayerExcel') }}" enctype='multipart/form-data'>
                                    @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-8 mr-5">
                                            <label>Upload Excel File</label>
                                            <input type="file" name="tour_excel" class=" form-control" id="inputGroupFile04" style="height:auto">
                                        </div>
                                        {{-- <div class="form-group col-md-8 mr-5">
                                        <div class="col-md-4">
                                            <h5>Single File Upload</h5>
                                            <div class="mt-3">
                                                <input type="file" name="tour_excel" class="dropify"/>
                                                <p class="text-muted text-center mt-2 mb-0">Upload Excle File</p>
                                            </div>
                                        </div>
                                    </div> --}}

                                        <div class="form-group col-md-3">
                                            <label>Download Format</label>
                                            <a title="Click here to download sample format of tournament excel upload" data-plugin="tippy" data-tippy-arrow="true" data-tippy-animation="fade" href="{{ asset('/assets/data/ticket-to-user.xlsx') }}"><button type="button" class="btn btn-blue waves-effect waves-light"><i class="fa fa-download mr-2"></i> Download Sample</button></a>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label title="Toggle this button to make auto register the selected tournament" data-plugin="tippy" data-tippy-arrow="true" data-tippy-animation="fade">Auto Register</label>
                                        {{-- <input type="checkbox"  name="autoreg" data-plugin="switchery" value="on" data-color="#1bb99a" data-size="small" /> --}}
                                        <label class="switches">
                                            <input name="autoreg" type="checkbox" value ="on">
                                            <span class="slider round"></span>
                                          </label>
                                          <p class="formtext clearfix"><i>Please upload excel with chunk of 1000 rows maximum .</i></p>
                                    </div>
                                    <button title="Click this button to assign tickets to all users written in the excel sheet" data-plugin="tippy" data-tippy-arrow="true" data-tippy-animation="fade"type="submit" class="btn btn-success waves-effect waves-light"><i class="fas fa-save mr-2"></i>Submit</button>
                                    <a href="{{ route('games.poker.tournament.registerplayer') }}"><button title="Click this button to reset all data filled by you" data-plugin="tippy" data-tippy-arrow="true" data-tippy-animation="fade"type="button" class="btn btn-secondary waves-effect waves-light"><i class="fas fa-eraser mr-1"></i> Clear</button></a>
                                </form>                                                    
                            </div> <!-- end col -->
                        </div>
                    </div>
                </div>
            </div> <!-- end card-box-->
        </div> <!-- end col -->
    </div>
@endsection

@section('js')
   
    <script>
        $(document).ready(function(){
            $(".modalclose").click(function(){
                $('#accordion-modal').hide();
                location.replace(`${window.pageData.baseUrl}/games/poker/tournament/registerplayer`);
                // alert('dasdsa');
            });
        });
    </script>
   
    
    <script src="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.js') }}"></script>
    <script src="{{ asset('assets/libs/switchery/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery-mockjax/jquery-mockjax.min.js') }}"></script>
    <script src="{{ asset('assets/libs/autocomplete/autocomplete.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>
    <script src="{{ asset('assets/bo/pagesJs/tickettouser.js') }}"></script>
    {{-- <script src="{{ asset('assets/libs/dropify/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form-fileuploads.init.js') }}"></script> --}}
@endsection