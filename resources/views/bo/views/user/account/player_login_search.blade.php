@extends('bo.layouts.master')

@section('title', "Player Login Search | PB BO")

@section('pre_css')
<!-- <link href="{{ asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs//rwd-table/rwd-table.min.css') }}" rel="stylesheet" type="text/css" /> -->
<link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('post_css')
<style>
    .ms-container{
        max-width: 650px;
    }    
</style>
@endsection

@section('content')

<!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
            <div class="page-title-right">
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item">Players</li>
                  <li class="breadcrumb-item active">Players Login</li>
               </ol>
            </div>
            <h4 class="page-title">Players Login</h4>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-12">
         <div class="card">
            <div class="card-body">
               <form class="" novalidate="" method="POST" id="playerLoginSearch" action="{{ route('user.player_login_search.search_user') }}">
                    {{ csrf_field() }}
                  <div class="form-row align-items-center">
                     <div class="form-group col-md-2">
                        <label class="mb-1">Search By: </label>
                     </div>
                     <div class="form-group col-md-5">
                        <div class="radio radio-info form-check-inline">
                              <input  type="radio" id="search_by_username" value="username" name="search_by" {{ !empty($params) ? $params['search_by'] == 'username' ? "checked" : ''  : ''  }} checked>
                           <label for="search_by_username"> Username </label>
                        </div>
                        <div class="radio radio-info form-check-inline">
                              <input type="radio" id="search_by_email" value="email"{{ !empty($params) ? $params['search_by'] == 'email' ? "checked" : ''  : ''  }} name="search_by">
                           <label for="search_by_email"> Email </label>
                        </div>
                        <div class="radio radio-info form-check-inline">
                              <input type="radio" id="search_by_contact_no" value="contact_no" {{ !empty($params) ? $params['search_by'] == 'contact_no' ? "checked" : ''  : ''  }} name="search_by">
                           <label for="search_by_contact_no"> Contact No </label>
                        </div>
                     </div>
                     <div class="form-group col-md-5">
                        <label class="sr-only" for="search_by_value">Search Input</label>
                        <input type="text" class="form-control" name="username" id="username" placeholder="Search by Username, Email & Contact No" value="{{ !empty($params) ? $params['username']  : ''  }}">
                     </div>
                  </div> 
                  <div class="form-row">
                     <!-- <div class="form-group col-md-4">
                        <label for="validationCustom01">User Name:</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{{ !empty($params) ? $params['username'] : '' }}" >
                     </div> -->
                     <div class="form-group col-md-4">
                        <label for="validationCustom01">IP Address:</label>
                        <input type="text" class="form-control" id="ip" name="ip" placeholder="User Ip" value="{{ !empty($params) ? $params['ip'] : '' }}">
                     </div>
                     <div class="form-group col-md-4">
                        <label for="validationCustom01">Action:</label>
                        <select class="customselect" data-plugin="customselect" id="action_type" name="action_type">
                           <option value="" {{ !empty($params) ? $params['action_type'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                           <option value="login" {{ !empty($params) ? $params['action_type'] == 'login' ? "selected" : ''  : ''  }}>Login</option>
                           <option value="Register" {{ !empty($params) ? $params['action_type'] == 'Register' ? "selected" : ''  : ''  }}>Register</option>
                        </select>
                     </div>
                     <div class="form-group col-md-4">
                        <label for="inputPassword4" >
                        Device Unique Id:</label>
                        <input type="text" class="form-control" id="device_unique_id" name="device_unique_id" placeholder="Device Unique Id" value="{{ !empty($params) ? $params['device_unique_id'] : '' }}">
                     </div>
                  </div>
                  <div class="form-row">
                     <div class="form-group col-md-4">
                        <label for="inputPassword4" class="col-form-label">Device Type:</label>
                        <select class="customselect" data-plugin="customselect" name="device_type" id="device_type" >
                           <option value="" {{ !empty($params) ? $params['device_type'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                           <option value="1" {{ !empty($params) ? $params['device_type'] == 1 ? "selected" : ''  : ''  }}>Desktop</option>
                           <option value="2" {{ !empty($params) ? $params['device_type'] == 2 ? "selected" : ''  : ''  }}>Mobile</option>
                        </select>
                     </div>
                     <div class="form-group col-md-4">
                        <label for="inputPassword4" class="col-form-label">APP Type:
                        </label>
                        <select class="customselect" data-plugin="customselect" name="app_type" id="app_type">
                           <option value="" {{ !empty($params) ? $params['app_type'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                           <option value="1" {{ !empty($params) ? $params['app_type'] == 1 ? "selected" : ''  : ''  }} >PC Instant Play</option>
                           <option value="2" {{ !empty($params) ? $params['app_type'] == 2 ? "selected" : ''  : ''  }} >EXE</option>
                           <option value="3" {{ !empty($params) ? $params['app_type'] == 3 ? "selected" : ''  : ''  }} >DMG</option>
                           <option value="4" {{ !empty($params) ? $params['app_type'] == 4 ? "selected" : ''  : ''  }} >Mobile Instant Play</option>
                           <option value="5" {{ !empty($params) ? $params['app_type'] == 5 ? "selected" : ''  : ''  }} >APK</option>
                           <option value="6" {{ !empty($params) ? $params['app_type'] == 6 ? "selected" : ''  : ''  }} >IPA</option>
                           <option value="7" {{ !empty($params) ? $params['app_type'] == 7 ? "selected" : ''  : ''  }} >Website</option>
                        </select>
                     </div>
                     <div class="form-group col-md-4">
                        <label for="inputPassword4" class="col-form-label">Status:</label>
                        <select class="customselect" data-plugin="customselect" name="status" id="status" >
                           <option value="" {{ !empty($params) ? $params['status'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                           @foreach($tracking_status as $status)
                           <option value="{{ $status->TRACKING_STATUS_ID }}" {{ !empty($params) ? $params['status'] == $status->TRACKING_STATUS_ID ? "selected" : ''  : ''  }}>{{ $status->TRACKING_STATUS_DESC }}</option>
                           @endforeach
                        </select>
                     </div>                     
                  </div>
                  <div class="form-row">                     
                     <div class="form-group col-md-4">
                        <label for="inputPassword4" class="col-form-label">
                        Pack Version:</label>
                        <input type="text" class="form-control" id="pack_version" name="pack_version" placeholder="Pack Version" value="{{ !empty($params) ? $params['pack_version'] : '' }}">
                     </div>
                     <div class="form-group col-md-4">
                        <label for="inputPassword4" class="col-form-label">
                        Build Version:</label>
                        <input type="text" class="form-control" id="build_version" name="build_version" placeholder="Build Version" value="{{ !empty($params) ? $params['build_version'] : '' }}">
                     </div>
                     <div class="form-group col-md-4">
                        <label for="inputPassword4" class="col-form-label">
                        Device Model:</label>
                        <input type="text" class="form-control" id="device_model" name="device_model" placeholder="Device Model" value="{{ !empty($params) ? $params['device_model'] : '' }}">
                     </div>
                  </div>
                  <div class="form-row">                     
                     <div class="form-group col-md-4">
                        <label for="inputPassword4" class="col-form-label">
                        Browser Name:</label>
                        <input type="text" class="form-control" id="browser_name" name="browser_name" placeholder="Browser Name" value="{{ !empty($params) ? $params['browser_name'] : '' }}">
                     </div>
                     <div class="form-group col-md-4">
                        <label for="inputPassword4" class="col-form-label">
                        Operating System:</label>
                        <input type="text" class="form-control" id="operating_system" name="operating_system" placeholder="Operating System" value="{{ !empty($params) ? $params['operating_system'] : '' }}">
                     </div>
                     <div class="form-group col-md-4">
                        <label for="inputPassword4" class="col-form-label">
                        Screen Size:</label>
                        <input type="text" class="form-control" id="screen_size" name="screen_size" placeholder="Screen Size" value="{{ !empty($params) ? $params['screen_size'] : '' }}">
                     </div>
                  </div>
                  <div class="form-row customDatePickerWrapper">
                     <div class="form-group col-md-3">
                        <label>From:</label>
                        <div class="input-group">
                           <input name="date_from" type="text" class="form-control customDatePicker from" placeholder="Select From Date" value="{{ !empty($params) ? $params['date_from'] : \Carbon\Carbon::today()->setTime("00","00","00")->format('d-M-Y H:i:s') }}">
                        </div>
                     </div>
                     <div class="form-group col-md-3">
                        <label>To:</label>
                        <div class="input-group">
                        <input name="date_to" type="text" class="form-control customDatePicker to" placeholder="Select To Date" value="{{ !empty($params) ? $params['date_to'] : \Carbon\Carbon::today()->setTime("23","59","59")->format('d-M-Y H:i:s') }}">
                        </div>
                     </div>
                     <div class="form-group col-md-3">
                        <label for="inputPassword4">Date Range: </label>
                        <select  class="customselect formatedDateRange" data-plugin="customselect" name="date_range" id="date_range">
                           <option value="" {{ !empty($params) ? $params['date_range'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                           <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                           <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                           <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                           <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                           <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                           <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                        </select>
                     </div>

                     <div class="form-group col-md-3">
                        <label for="inputPassword4" >
                        Fingerprint Id:</label>
                        <input type="text" class="form-control" id="fingerprint" name="fingerprint" placeholder="Fingerprint" value="{{ !empty($params) ? $params['fingerprint'] : '' }}">
                     </div>
                  </div>
                  <div class="button-list">
                     <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                        <a href="{{ route('user.player_login_search.index') }}" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                     <br><br>
                     </div>
                     <br><br>
               </form>
            </div>
         </div>
      </div>
   </div>

@if(!empty($playerLoginInfo) )
<div class="row">
   <div class="col-12">
      <div class="card">
         <div class="card-header bg-dark text-white">
            <div class="card-widgets">
               <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>                               
            </div>
            <h5 class="card-title mb-0 text-white">PLAYER LOGIN INFO</h5>
         </div>
      @if(count($playerLoginInfo) > 0)
         <div class="card-body">            
            <p class="text-muted font-13 mb-3"> </p>
            <div class="row mb-2">
               <div class="col-sm-6">
                  <h5 class="font-18 mt-0">   </h5> 
               </div>
               <div class="col-sm-6">
                  <div class="text-sm-right">                    
                     @if(count($playerLoginInfo) > 0)
                        <a href="{{ route('user.player_login_search.export')."?username=".$params['username']."&ip=".$params['ip']."&action_type=".$params['action_type']."&device_type=".$params['device_type']."&app_type=".$params['app_type']."&browser_name=".$params['browser_name']."&operating_system=".$params['operating_system']."&screen_size=".$params['screen_size']."&date_from=".$params['date_from']."&date_to=".$params['date_to']."&fingerprint=".$params['fingerprint']."&type=playerLoginInfo" }}"  class="btn btn-light mb-1"><i class="fas fa-file-excel mr-1" style="color: #34a853;"></i>Excel Export</a>
                     @else
                     <button type="button" class="btn btn-light mb-1"><i class="fas fa-file-excel mr-1" style="color: #34a853;"></i>Excel Export</button>
                     @endif
                  </div>
               </div>
            </div>
            <table class="basic-datatable table dt-responsive nowrap"  data-order=''>
               <thead>
                 <tr >
                    <th>USERNAME</th>
                    <th>SYSTEM IP</th>
                    <th>ACTION NAME</th>
                    <th>DATE TIME</th>
                    <th>DEVICE TYPE</th>
                    <th>APP TYPE</th>
                    <th>STATUS</th>
                    <th>DEVICE MODEL</th>
                    <th>OPERATING SYSTEM</th>
                    <th>BROWSER NAME</th>
                    <th>SCREEN SIZE</th>
                    <th>PACK VERSION</th>
                    <th>BUILD VERSION</th>
                    <th>DEVICE ID</th>
                    <th>FINGERPRINT ID</th>
                </tr>
               </thead>
               <tbody>

               @foreach($playerLoginInfo as $playerLogin)                    
               <tr>
                  <td>{{ $playerLogin->USERNAME}}</td>
                  <td>{{ $playerLogin->SYSTEM_IP}}</td>
                  <td>{{ $playerLogin->ACTION_NAME}}</td>
                  <td>{{ $playerLogin->DATE_TIME}}</td>
                  <td>{{ $playerLogin->DEVICE_TYPE == 1 ? "Desktop" : ( $playerLogin->DEVICE_TYPE == 2 ? "Mobile" : "Unknown/Websites")}}</td>
                  <td>
                  @switch($playerLogin->APP_TYPE)
                  @case(1)
                     PC Instant Play
                     @break
                  @case(2)
                     EXE
                     @break
                  @case(3)
                     DMG
                     @break 
                  @case(4)
                     Mobile Instant Play
                     @break
                  @case(5)
                     APK
                     @break
                  @case(6)
                     IPA
                     @break
                  @case(7)
                     Website
                     @break

                  @default
                     Unknown/Websites
               @endswitch                        
                  </td>
                  <td>{{ $playerLogin->TRACKING_STATUS_DESC}}</td>
                  <td>{{ $playerLogin->DEVICE_MODEL}}</td>
                  <td>{{ $playerLogin->OPERATING_SYSTEM}}</td>
                  <td>{{ $playerLogin->BROWSER_NAME}}</td>
                  <td>{{ $playerLogin->SCREEN_SIZE}}</td>
                  <td>{{ $playerLogin->PACK_VERSION}}</td>
                  <td>{{ $playerLogin->BUILD_VERSION}}</td>
                  <td>{{ $playerLogin->DEVICE_UNIQUE_ID}}</td>                     
                  <td>{{ $playerLogin->FINGERPRINT_ID}}</td>                     
               </tr>  
               @endforeach    
         </tbody>
      </table>
   </div>
   @else
   <div>
      <p style="text-align:center;">No records found!</p>
   </div>
   @endif
</div>
@endif
@if(!empty($playerWithCommonIp))
         <div class="card">
            <div class="card-header bg-dark text-white">
               <div class="card-widgets">
                  <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>                               
               </div>
               <h5 class="card-title mb-0 text-white">MATCHING PLAYERS IP ADDRESS</h5>
            </div>
            @if(count($playerWithCommonIp) > 0)
            <div class="card-body">
               <div class="row mb-2 mt-3">
               <div class="col-sm-7"><h5 class="font-18 "></h5></div>
                  <div class="col-sm-5">
                  <div class="text-sm-right">   
                     @if(count($playerWithCommonIp) > 0)
                     <a href="{{ route('user.player_login_search.export')."?username=".$params['username']."&ip=".$params['ip']."&action_type=".$params['action_type']."&device_type=".$params['device_type']."&app_type=".$params['app_type']."&browser_name=".$params['browser_name']."&operating_system=".$params['operating_system']."&screen_size=".$params['screen_size']."&date_from=".$params['date_from']."&date_to=".$params['date_to']."&fingerprint=".$params['fingerprint']."&type=playerWithCommonIp" }}"  class="btn btn-light mb-1"><i class="fas fa-file-excel mr-1" style="color: #34a853;"></i>Excel Export</a>
                     @else
                        <button type="button" class="btn btn-light mb-1"><i class="fas fa-file-excel mr-1" style="color: #34a853;"></i>Excel Export</button>
                     @endif
                  </div>
               </div>
            </div>
               <table class="basic-datatable table dt-responsive nowrap"  data-order=''>
                  <thead>
                  <tr >
                     <th>USERNAME</th>
                     <th>SYSTEM IP</th>
                     <th>ACTION NAME</th>
                     <th>DATE TIME</th>
                     <th >DEVICE TYPE</th>
                     <th >APP TYPE</th>
                     <th >STATUS</th>
                     <th >DEVICE MODEL</th>
                     <th >OPERATING SYSTEM</th>
                     <th >BROWSER NAME</th>
                     <th >SCREEN SIZE</th>
                     <th >PACK VERSION</th>
                     <th >BUILD VERSION</th>
                     <th >DEVICE ID</th>
                     <th >FINGERPRINT ID</th>
                  </tr>
                  </thead>
                  <tbody>
                     @foreach($playerWithCommonIp as $playerCommonIpLogin)
                     <tr>
                           <td>{{ $playerCommonIpLogin->USERNAME}}</td>
                           <td>{{ $playerCommonIpLogin->SYSTEM_IP}}</td>
                           <td>{{ $playerCommonIpLogin->ACTION_NAME}}</td>
                           <td>{{ $playerCommonIpLogin->DATE_TIME}}</td>
                           <td>{{ $playerCommonIpLogin->DEVICE_TYPE == 1 ? "Desktop" : ( $playerCommonIpLogin->DEVICE_TYPE == 2 ? "Mobile" : "Unknown/Websites")}}</td>
                           <td>
                           @switch($playerCommonIpLogin->APP_TYPE)
                              @case(1)
                                 PC Instant Play
                                 @break
                              @case(2)
                                 EXE
                                 @break
                              @case(3)
                                 DMG
                                 @break 
                              @case(4)
                                 Mobile Instant Play
                                 @break
                              @case(5)
                                 APK
                                 @break
                              @case(6)
                                 IPA
                                 @break
                              @case(7)
                                 Website
                                 @break

                              @default
                                 Unknown/Websites
                           @endswitch
                           </td>
                           <td>{{ $playerCommonIpLogin->TRACKING_STATUS_DESC}}</td>
                           <td>{{ $playerCommonIpLogin->DEVICE_MODEL}}</td>
                           <td>{{ $playerCommonIpLogin->OPERATING_SYSTEM}}</td>
                           <td>{{ $playerCommonIpLogin->BROWSER_NAME}}</td>
                           <td>{{ $playerCommonIpLogin->SCREEN_SIZE}}</td>
                           <td>{{ $playerCommonIpLogin->PACK_VERSION}}</td>
                           <td>{{ $playerCommonIpLogin->BUILD_VERSION}}</td>
                           <td>{{ $playerCommonIpLogin->DEVICE_UNIQUE_ID}}</td>  
                           <td>{{ $playerCommonIpLogin->FINGERPRINT_ID}}</td>                     
                     </tr>  
                     @endforeach
                  </tbody>
               </table>
         </div>
         @else
            <div>
               <p style="text-align:center;"> No Reults found</p>
            </div> 
         @endif
         
      </div>
@endif
@if(!empty($commonGamesPlayed))
      <div class="card">
         <div class="card-header bg-dark text-white">
               <div class="card-widgets">
                  <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>                               
               </div>
               <h5 class="card-title mb-0 text-white">COMMON GAMES PLAYED</h5>
            </div>         
         @if(count($commonGamesPlayed) > 0)
         <div class="card-body">
            <div class="row mb-2 mt-3">
               <div class="col-sm-7"><h5 class="font-18"> </h5></div>
               <div class="col-sm-5">
                  <div class="text-sm-right">
                     @if(count($commonGamesPlayed) > 0)
                     <a href="{{ route('user.player_login_search.export')."?username=".$params['username']."&ip=".$params['ip']."&date_from=".$params['date_from']."&date_to=".$params['date_to']."&fingerprint=".$params['fingerprint']."&type=commonGamesPlayed" }}"  class="btn btn-light mb-1"><i class="fas fa-file-excel mr-1" style="color: #34a853;"></i>Excel Export</a>
                     @else
                        <button type="button" class="btn btn-light mb-1"><i class="fas fa-file-excel mr-1" style="color: #34a853;"></i>Excel Export</button>
                     @endif
                  </div>
               </div>
            </div>  
            <table class="basic-datatable table dt-responsive nowrap"  data-order=''>
            <thead >
               <tr >
                  <th >TableID</th>
                  <th >GAME ID</th>
                  <th >STAKE</th>
                  <th >POT</th>
                  <th >REVENUE</th>
                  <th >REAL REVENUE</th>
                  <th >BONUS REVENUE</th>
                  <th >ACTUAL REVENUE</th>
                  <th >GST</th>
                  <th >USERNAME</th>
               </tr>
            </thead>
            <tbody class="body-table">
               @foreach($commonGamesPlayed as $commonGames)
               <tr>
                  <td>{{ $commonGames['TableId']}}</td>
                  <td>{{ $commonGames['PLAY_GROUP_ID']}}</td>
                  <td>{{ $commonGames['STAKE']}}</td>
                  <td>{{ $commonGames['POT']}}</td>
                  <td>{{ $commonGames['REVENUE']}}</td>
                  <td>{{ $commonGames['REAL_REVENUE']}}</td>
                  <td>{{ $commonGames['BONUS_REVENUE']}}</td>
                  <td>{{ $commonGames['ACTUAL_REVENUE']}}</td>
                  <td>{{ $commonGames['GST']}}</td>
                  <td>{{ $commonGames['USERNAME']}}</td>                     
               </tr>  
               @endforeach             
            </tbody>
         </table>
         </div>
         @else
            <div>
            <p style="text-align:center;">No Reults found</p>
            </div> 
         @endif 
      </div>                    
@endif
         </div>
         <!-- end card body-->
      </div>
      <!-- end card -->
   </div>
   <!-- end col-->
</div>
</div>
@endsection

@section('js')
    
    <script src="{{ asset('assets/libs/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form-pickers.init.js') }}"></script>
    <script src="{{ asset('assets/libs/datatables/datatables.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/bo/pagesJs/transactionSearch.js') }}"></script>  --}}
    <script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script>
 
<script>
   $("#playerLoginSearch").validate({
	rules: {	
         date_from: {
            required: true
         },		
         date_to: {
            required: true
         }
      },	
   });

</script>


@endsection