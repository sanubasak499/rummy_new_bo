@extends('bo.layouts.master')

@section('title', "Player Search | PB BO")

@section('pre_css')
@endsection

@section('content')

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                    <li class="breadcrumb-item active">Player Search</li>
                </ol>
            </div>
            <h4 class="page-title">Player Search</h4>
        </div>
    </div>
</div>
<!-- end page title --> 

<div class="row">
    <div class="col-lg-12">
        <!-- Portlet card -->
        <div class="card">
            <div id="cardCollpase5" class="collapse show">
                <div class="card-body">
                    <form id="userSearchForm" data-default-url="{{ route('user.account.search_post') }}" action="{{ route('user.account.search_post') }}{{ !empty($params) ? empty($params['page']) ? "" : "?page=".$params['page'] : '' }}" method="post">
                        @csrf
                        <div class="form-row align-items-center">
                            <div class="form-group col-md-2">
                                <label class="mb-1">Search By: </label>
                            </div>
                            <div class="form-group col-md-5">
                                <div class="radio radio-info form-check-inline">
                                    <input data-default-checked type="radio" id="search_by_username" value="username" name="search_by" {{ !empty($params) ? $params['search_by'] == "username" ? "checked" : '' : "checked" }}>
                                    <label for="search_by_username"> Username </label>
                                </div>
                                <div class="radio radio-info form-check-inline">
                                    <input type="radio" id="search_by_email" value="email" name="search_by" {{ !empty($params) ? $params['search_by'] == "email" ? "checked" : '' : "" }}>
                                    <label for="search_by_email"> Email </label>
                                </div>
                                <div class="radio radio-info form-check-inline">
                                    <input type="radio" id="search_by_contact_no" value="contact_no" name="search_by" {{ !empty($params) ? $params['search_by'] == "contact_no" ? "checked" : '' : "" }}>
                                    <label for="search_by_contact_no"> Contact No </label>
                                </div>
                            </div>
                            <div class="form-group col-md-5">
                                <label class="sr-only" for="search_by_value">Search Input</label>
                                <input type="text" class="form-control" name="search_by_value" id="search_by_value" placeholder="Search by Username, Email & Contact No" value="{{ !empty($params) ? $params['search_by_value'] : '' }}">
                            </div>
                        </div>
                        
                        <div class="form-row customDatePickerWrapper">
                            <div class="form-group col-md-4">
                                <label>From: </label>
                                <input name="date_from" type="text" class="form-control customDatePicker from" placeholder="Select From Date" value="{{ !empty($params) ? $params['date_from']  : ''  }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>To: </label>
                                <input name="date_to" type="text" class="form-control customDatePicker to" placeholder="Select To Date" value="{{ !empty($params) ? $params['date_to']  : ''  }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Date Range: </label>
                                <select name="date_range" id="date_range" class="form-control formatedDateRange">
                                    <option data-default-selected value value="" {{ !empty($params) ? $params['date_range'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                                    <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                                    <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                                    <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                                    <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                                    <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                                    <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                                </select>
                            </div>
                        </div>

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>State: </label>
                                    <select id="state" name="state" class="form-control" onchange="getCity(this,'STATE','CITY')">
                                        <option data-default-selected value="">Select State</option>
                                        @foreach($state as $key => $value)
                                            <option value="{{ $value->state }}" {{ !empty($params) ? $params['state'] == $value->state ? "selected" : ''  : ''  }}>{{ $value->state }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <label>City: </label>
                                    <select name="city" id="city" class="form-control" onchange="getCity(this,'CITY','PINCODE')" data-city="{{ !empty($params) ? $params['city'] : null }}">
                                        <option data-default-selected value="">Select City</option>
                                    </select>
                                </div>

                                <div class="form-group col-md-4">
                                    <label>Pin Code: </label>
                                    {{-- <select name="pincode[]" id="pincode" class="form-control" multiple>
                                        <option data-default-selected value="">Select City</option>
                                    </select> --}}
                                    <input type="text" name="pincode" id="pincode" placeholder="Enter Pin Code" value="{{ !empty($params) ? $params['pincode'] : '' }}" class="form-control" onchange="AddComma(pincode.value);">
                                </div>
                            </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Status: </label>
                                <select name="status" class="form-control">
                                    <option data-default-selected value="" {{ !empty($params) ? $params['status'] == "" ? "selected" : '' :  'selected'  }}>Select</option>
                                    <option value="none" {{ !empty($params) ? $params['status'] == "none" ? "selected" : ''  : ''  }}>Inactive</option>
                                    <option value="1" {{ !empty($params) ? $params['status'] == 1 ? "selected" : ''  : ''  }}>Active</option>
                                    <option value="2" {{ !empty($params) ? $params['status'] == 2 ? "selected" : ''  : ''  }}>Blocked</option>
                                </select>
                            </div>
                        </div>
                        

                        <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i>Search</button>
                    <a href="" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i>Reset</a>
                    </form> 
                </div>
            </div>
        </div> <!-- end card-->
    </div><!-- end col -->
</div>

@if(!empty($users))
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header bg-dark text-white">
                {{-- <div class="card-widgets">
                    <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                </div> --}}
                <h5 class="card-title mb-0 text-white">Players List</h5>
            </div>
            <div class="card-body">
                <div class="row mb-2">
                    <div class="col-sm-9 mb-1">
                        <h5 class="font-15 mt-0">Total Users: <span class="text-danger">({{ $usercount['total']->total ?? 0 }})</span>
                            &nbsp;&nbsp;|&nbsp;&nbsp;Active Users: <span class="text-danger">({{ $usercount[1]->total ?? 0 }})</span>
                            &nbsp;&nbsp;|&nbsp;&nbsp;Inactive Users: <span class="text-danger">({{ $usercount[0]->total ?? 0 }})</span>
                            &nbsp;&nbsp;|&nbsp;&nbsp;Blocked Users: <span class="text-danger mr-1">({{ $usercount[2]->total ?? 0 }})</span> 
                        </h5>
                    </div>
                    @if($users[0])
                    <div class="col-sm-3 mb-1">
                        <div class="text-sm-right">
                        <a href="{{ url('user/account/excelexport')."?search_by=".$params['search_by']."&search_by_value=".$params['search_by_value']."&date_from=".$params['date_from']."&date_to=".$params['date_to']."&status=".$params['status']."&state=".$params['state']."&city=".$params['city']."&pincode=".$params['pincode'] }}" class="btn btn-light mb-1"><i class="fas fa-file-excel" style="color: #34a853;"></i> Export Excel</a>
                        </div>
                    </div>
                    @endif
                    <!-- end col-->
                </div>
                <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>User Id</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            @if(!empty($params) ? (!empty($params['state'] || !empty($params['pincode'])) ? true : false) : false)
                                <th>Address</th>
                            @endif
                            <th>Real Money</th>
                            <th>Commission</th>
                            <th>Affiliate</th>
                            <th>Status</th>
                            <th>Poker Break</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $key => $user)
                        <tr>
                            <td>
                                <a href="{{ route('user.account.user_profile', encrypt($user->USER_ID)) }}" target="_blank">{{ $user->USERNAME }}</a>
                            </td>
                            <td>{{ $user->USER_ID }}</td>
                            <td>{{ $user->FULLNAME }}</td>
                            <td>{{ $user->CONTACT }}</td>
                            <td>{{ $user->EMAIL_ID }}</td>
                            @if(!empty($params) ? (!empty($params['state'] || !empty($params['pincode'])) ? true : false) : false)
                                @if(!empty($user->STATE) || !empty($user->PINCODE))
                                    <td>
                                        <div>{{ ($user->LOCALE=='none' ? '': $user->LOCALE).' '.$user->STREET}}</div>
                                        <div>{{ $user->CITY.' '.$user->STATE }}</div>
                                        <div>{{ $user->PINCODE   }}</div>
                                    </td>
                                @else
                                    <td>N/A</td>
                                @endif
                            @endif
                            <td>{{ $user->user_points->where('COIN_TYPE_ID', 1)->first()->USER_TOT_BALANCE ?? 0 }}</td>
                            <td>{{ $user->user_points->where('COIN_TYPE_ID', 16)->first()->USER_TOT_BALANCE ?? 0 }}</td>
                            <td>{{ $user->partner->PARTNER_NAME }}</td>
                            <td>{!! $user->ACCOUNT_STATUS==0?'<span class="badge badge-warning badge-pill">Inactive</span>':($user->ACCOUNT_STATUS==1?'<span class="badge badge-success badge-pill">Active</span></td>':'<span class="badge badge-danger badge-pill">Blocked</span>') !!}</td>
                            <td>{!! empty($user->responsible_game_setting) ? '<span class="badge badge-warning badge-pill">N/A</span>' : ($user->responsible_game_setting->BREAK_ACTIVE == 1 ? '<span class="badge badge-danger badge-pill">Active</span>' : '<span class="badge badge-success badge-pill">Inactive</span>') !!}</td>    
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" style="display:flex; justify-content: space-between;" class="mt-2">
                    @if(!empty($users[$key]))
                        <div>More Records</div>
                    @endif
                    <div>
                        {{ $users->render("pagination::customPaginationView") }}
                    </div>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endif
@endsection
@section('js')
@endsection
@section('post_js')
    <script src="{{ asset('js/bo/user_profile.js') }}"></script>
    <script type="text/javascript">

        function AddComma(pinvalue)
        { 
            var MyString = pinvalue.replace(',','');
            var objRegex  = new RegExp('(-?[0-9]+)([0-9]{6})');
            //Check For Criteria....
            while(objRegex.test(MyString))
            {
                //Add Commas After Every Six Digits Of Number...
                MyString = MyString.replace(objRegex, '$1,$2');
            }
            document.getElementById('pincode').value=MyString;
        }
    </script>
@endsection