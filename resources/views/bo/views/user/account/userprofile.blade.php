@extends('bo.layouts.master')

@section('title', "Player Search | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/x-editable/x-editable.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/dropify/dropify.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('post_css')
<style>
	.nav-pills li {
		margin-bottom: 10px;
	}

	.nav-justified .nav-item {
		flex: 25%;
	}

	.error {
		color: #D8000C;
	}
</style>
@endsection

@section('content')
<!-- Start Content-->
<div class="container-fluid">

	<!-- start page title -->
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<!-- <div class="page-title-right">
						<ol class="breadcrumb m-0">
							<li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
							<li class="breadcrumb-item"><a href="javascript: void(0);">Extras</a></li>
							<li class="breadcrumb-item active">Profile</li>
						</ol>
					</div> -->
				<h4 class="page-title"></h4>
				{{Session::get('errorvalidation')}}
			</div>
		</div>
	</div>
	@include('bo.views.commonview.flashmessage')
	<!-- end page title -->
	<div class="row">
		<div class="col-lg-4 col-xl-4">
			<div class="card-box text-center">
				<img src="{{ asset('assets/images/users/user.png') }}" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">

				<h4 class="mb-0">{{ $userprofile->USERNAME }}</h4>
				<p class="text-muted">{{ $userprofile->FULLNAME }} &nbsp &nbsp {{ $userprofile->USER_ID }}</p>

				{{--<button type="button" class="btn btn-success btn-xs waves-effect mb-2 waves-light">Follow</button>
					<button type="button" class="btn btn-danger btn-xs waves-effect mb-2 waves-light">Message</button> --}}
				<div class="text-left mt-3">
					<p class="text-muted mb-2 font-13"><strong>VIP Level :</strong><span class="ml-2">{{ $level_name->LEVEL_NAME }}</span></p>
					<p class="text-muted mb-2 font-13" style="cursor:pointer;" data-toggle="modal" data-target="#con-close-modal"><strong>Status :</strong><span class="ml-2">{!! $userprofile->ACCOUNT_STATUS==0?'<span class="badge badge-warning badge-pill">Inactive</span>':($userprofile->ACCOUNT_STATUS==1?'<span class="badge badge-success badge-pill">Active</span></td>':'<span class="badge badge-danger badge-pill">Blocked</span>') !!}</span></p>

					<p class="text-muted mb-2 font-13"><strong>Poker Break :</strong><span class="ml-2">{!! empty($userprofile->responsible_game_setting) ? '<span class="badge badge-warning badge-pill">N/A</span>' : ($userprofile->responsible_game_setting->BREAK_ACTIVE == 1 ? '<span class="badge badge-danger badge-pill">Active</span>' : '<span class="badge badge-success badge-pill">Inactive</span>') !!}</span></p>

					<p class="text-muted mb-2 font-13"><strong>Player Info :</strong><span class="ml-2"> <a data-toggle="collapse" class="suppbutton" href="#collapse1"> {!! empty($userprofile->player_info) ? '<span class="badge badge-warning badge-pill">N/A</span>' : $userprofile->player_info !!}</a></span></p>

					<div id="collapse1" class="panel-collapse collapse">
						<form class="form-inline editableform" style="" action="{{ url('user/account/updateuserprofile') }}" method="post">
							{{ csrf_field() }}
							<textarea class="form-control w-100" name="player_info" placeholder="Enter Player Info Here..." rows="3" required>{{ $userprofile->player_info }}</textarea>
							<div class="control-group form-group">
								<div>
									<input type="hidden" name="USER_ID" value="{{ encrypt($userprofile->USER_ID) }}">
									<div class="editable-buttons editable-buttons-bottom">
										<input type="submit" name="upDatePlayerInfo" value="submit" class="btn btn-primary editable-submit btn-sm waves-effect waves-light" />
										<button type="button" class="btn btn-danger editable-cancel btn-sm waves-effect" data-toggle="collapse" href="#collapse1">
											<i class="mdi mdi-close"></i>
										</button>
									</div>
								</div>
								<div class="editable-error-block help-block" style="display: none;"></div>
							</div>
						</form><br />
					</div>
					<p class="text-muted mb-2 font-13"><strong>Player Note :</strong> <span class="ml-2 "></span></p>
					<p class="text-muted mb-2 font-13"><strong>Reg. Code : @if(isset($reg_code->PROMO_CAMPAIGN_CODE)){{ $reg_code->PROMO_CAMPAIGN_CODE }}@else{{ 'N/A' }}@endif</strong></p>
					<p class="text-muted mb-2 font-13"><strong>Password : </strong><span onclick="return passwordExpiry({!! $userprofile->PASSWORD_EXPIRED !!},{!! $userprofile->USER_ID !!})" style="cursor: pointer;">{!! $userprofile->PASSWORD_EXPIRED==0?'<span class="badge badge-success badge-pill">Valid</span>':'<span class="badge badge-danger badge-pill">Expired</span>' !!}</span></p>
					<p class="text-muted mb-2 font-13"><strong>2 Step Verification : </strong><span onclick="return twoStepVerification({!! $userprofile->TWO_STEP_VERIFICATION !!},{!! $userprofile->USER_ID !!})" style="cursor: pointer;">{!! $userprofile->TWO_STEP_VERIFICATION==1?'<span class="badge badge-success badge-pill">On</span>':'<span class="badge badge-danger badge-pill">Off</span>' !!}</span></p>
				</div>
				<div class="text-left mt-3">
					<h5 class="mb-2 text-uppercase"></i>Info </h5>
					<p class="text-muted mb-2 font-13"><strong>Reg On :</strong><span class="ml-2">{!! changeDateFormate($userprofile->REGISTRATION_TIMESTAMP) !!}</span></p>
					<p class="text-muted mb-2 font-13"><strong>Last Login :</strong><span class="ml-2">{!! changeDateFormate($userprofile->USER_LAST_LOGIN) !!}</span></p>
					<p class="text-muted mb-2 font-13"><strong>Ref By :</strong><span class="ml-2">{{ $referby->REF_BY_NAME ?? '--' }}</span></p>
					<p class="text-muted mb-2 font-13"><strong>Affiliate :</strong><span class="ml-2">{{ $partnername->PARTNER_NAME }}</span></p>
				</div>

				{{-- <ul class="social-list list-inline mt-3 mb-0">
						<li class="list-inline-item">
							<a href="javascript: void(0);" class="social-list-item border-primary text-primary"><i
									class="mdi mdi-facebook"></i></a>
						</li>
						<li class="list-inline-item">
							<a href="javascript: void(0);" class="social-list-item border-danger text-danger"><i
									class="mdi mdi-google"></i></a>
						</li>
						<li class="list-inline-item">
							<a href="javascript: void(0);" class="social-list-item border-info text-info"><i
									class="mdi mdi-twitter"></i></a>
						</li>
						<li class="list-inline-item">
							<a href="javascript: void(0);" class="social-list-item border-secondary text-secondary"><i
									class="mdi mdi-github-circle"></i></a>
						</li>
					</ul> --}}
			</div> <!-- end card-box -->
		</div> <!-- end col-->

		<div class="col-lg-8 col-xl-8">

			<div class="card-box">
				<ul class="nav nav-pills navtab-bg nav-justified">
					<li class="nav-item">
						<a href="#contactinfo" data-toggle="tab" aria-expanded="true" class="nav-link active">
							Contact Info
						</a>
					</li>
					<li class="nav-item">
						<a href="#balances" data-toggle="tab" aria-expanded="false" class="nav-link">
							Balances
						</a>
					</li>
					<li class="nav-item">
						<a href="#kyc" data-toggle="tab" aria-expanded="false" class="nav-link">
							KYC
						</a>
					</li>
					<li class="nav-item">
						<a href="#depositwithdrawal" data-toggle="tab" aria-expanded="false" class="nav-link" onclick="responsibleGaming({{ $userprofile->USER_ID }},'deposit_withdra_details')">
							D/W Details
						</a>
					</li>
					<li class="nav-item">
						<a href="#taxdetails" data-toggle="tab" aria-expanded="false" class="nav-link" onclick="responsibleGaming({{ $userprofile->USER_ID }},'tax_details')">
							Tax Details
						</a>
					</li>
					<li class="nav-item">
						<a href="#baazirewards" data-toggle="tab" aria-expanded="false" class="nav-link" onclick="responsibleGaming({{ $userprofile->USER_ID }},'baazi_rewards')">
							Baazi Rewards
						</a>
					</li>

					<li class="nav-item">
						<a href="#responsiblegaming" data-toggle="tab" aria-expanded="false" class="nav-link" onclick="responsibleGaming({{ $userprofile->USER_ID }},'resp_gaming')">
							Resp Gaming
						</a>
					</li>

					<li class="nav-item">
						<a href="#reason" data-toggle="tab" aria-expanded="false" class="nav-link" onclick="responsibleGaming({{ $userprofile->USER_ID }},'reason_details')">
						Account logs
						</a>
					</li>

					{{-- <li class="nav-item">
							<a href="#balances" data-toggle="tab" aria-expanded="false" class="nav-link" onclick="responsibleGaming({{ $userprofile->USER_ID }},'request')">
					Request
					</a>
					</li> --}}

				</ul>

				<div class="tab-content">

					<div class="tab-pane show active" id="contactinfo">

						<div class="mb-3 d-flex justify-content-between align-items-center">
							<h5 class="text-uppercase"> Contact Details</h5>
							<i class="fa fa-edit formEditViewBtn" id="editContactInfo" style="cursor: pointer;" onclick="hideContactInfo()"></i>
							<i class="fas fa-times-circle formEditViewBtn" style="display:none; cursor: pointer;" id="editContactInfo" onclick="hideEditUpdate()"></i>
						</div>
						<div id="hideContactInfo">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="firstname">Full Name :</label>
										<span>{{ $userprofile->FULLNAME }}</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="lastname">Mobile :</label>
										<span>{{ $userprofile->CONTACT }}</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="firstname">Gender :</label>
										<span>{{ $userprofile->GENDER }}</span>
									</div>
								</div>
								<!-- end col -->
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="lastname">DOB :</label>
										<span>{{ date('d-m-Y',strtotime($userprofile->DATE_OF_BIRTH)) }}</span>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label for="lastname">Email :</label>
										<span>{{ $userprofile->EMAIL_ID }}</span>
									</div>
								</div>
								<!-- end col -->
							</div>

							<h5 class="mb-3 text-uppercase">Address Details</h5>

							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="firstname">Address :</label>
										<span>{{ $userprofile->ADDRESS }}</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="lastname">Street :</label>
										<span>{{ $userprofile->STREET }}</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="lastname">City :</label>
										<span>{{ $userprofile->CITY }}</span>
									</div>
								</div>
								<!-- end col -->
							</div>

							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="state">State : </label>
										<span>{{ $userprofile->STATE }}</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="pin">PIN :</label>
										<span>{{ $userprofile->PINCODE }}</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="lastname">Country :</label>
										<span>{{ $userprofile->COUNTRY }}</span>
									</div>
								</div>
								<!-- end col -->
							</div>
						</div>

						<!-- Start edit and update Contact info area -->

						<div id="editUpdate" style="display: none">
							<div class="row">
								<div class="col-lg-12">
									<form action="{{ url('user/account/updateuserprofile') }}" method="post" class="formValidate">
										{{ csrf_field() }}
										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="simpleinput">First Name <span class="text-danger">*</span></label>
												<input type="text" name="FIRSTNAME" id="simpleinput" class="form-control" id="FIRSTNAME" value="{{ $userprofile->FIRSTNAME }}" required="required">
											</div>
											<div class="form-group col-md-6">
												<label for="simpleinput">Last Name <span class="text-danger">*</span></label>
												<input type="text" name="LASTNAME" class="form-control" value="{{ $userprofile->LASTNAME }}" required="required">
											</div>
										</div>

										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="example-email">Email <span class="text-danger">*</span></label>
												<input type="email" name="EMAIl_ID" id="validationCustom04validationCustom04" class="form-control" placeholder="Email" value="{{ $userprofile->EMAIL_ID }}" required="required">
											</div>
											<div class="form-group col-md-6">
												<label for="simpleinput">Mobile No. <span class="text-danger">*</span></label>
												<input type="number" name="CONTACT" class="form-control" value="{{ $userprofile->CONTACT }}" required>
											</div>
										</div>

										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="example-date">DOB <span class="text-danger">*</span></label>
												<input class="form-control" name="DATE_OF_BIRTH" type="date" value="{{ $userprofile->DATE_OF_BIRTH }}">
											</div>
											<div class="form-group col-md-6">
												<label for="example-select">Gender <span class="text-danger">*</span></label>
												<select class="form-control" name="GENDER">
													<option>Select Gender</option>
													<option value="male" @if($userprofile->GENDER=='male') {{ 'Selected' }} @endif>Male</option>
													<option value="female" @if($userprofile->GENDER=='female') {{ 'Selected' }} @endif>Female</option>
												</select>
											</div>
										</div>

										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="example-select">Country <span class="text-danger">*</span></label>
												<select class="form-control" name="COUNTRY">
													<option>Select Country</option>
													<?php $countCountry = count($countries);
													for ($i = 0; $i < $countCountry; $i++) { ?>
														<option value="{{ $countries[$i]->CountryName }}" @if($countries[$i]->CountryName == $userprofile->COUNTRY) {{ 'Selected' }} @endif>{{ $countries[$i]->CountryName }}</option>
													<?php }
													?>
												</select>
											</div>
											<div class="form-group col-md-6">
												<label for="example-select">State <span class="text-danger">*</span></label>
												<select class="form-control" name="STATE">
													<option>Select State</option>
													<?php $countState = count($state);
													for ($i = 0; $i < $countState; $i++) { ?>
														<option value="{{ $state[$i]->StateName }}" @if($state[$i]->StateName == $userprofile->STATE) {{ 'Selected' }} @endif>{{ $state[$i]->StateName }}</option>
													<?php }
													?>
												</select>
											</div>
										</div>

										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="simpleinput">City <span class="text-danger">*</span></label>
												<input type="text" name="CITY" class="form-control" value="{{ $userprofile->CITY }}">
											</div>
											<div class="form-group col-md-6">
												<label for="simpleinput">Street <span class="text-danger">*</span></label>
												<input type="text" name="STREET" class="form-control" value="{{ $userprofile->STREET }}">
											</div>
										</div>

										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="simpleinput">Address <span class="text-danger">*</span></label>
												<input type="text" name="ADDRESS" class="form-control" value="{{ $userprofile->ADDRESS }}">
											</div>
											<div class="form-group col-md-6">
												<label for="simpleinput">Pin <span class="text-danger">*</span></label>
												<input type="number" name="PINCODE" class="form-control" value="{{ $userprofile->PINCODE }}">
											</div>
										</div>
										<input type="hidden" name="USER_ID" value="{{ encrypt($userprofile->USER_ID) }}">
										<input type="submit" name="Update" class="btn btn-primary waves-effect waves-light" value="Update">
									</form>
								</div> <!-- end col -->
							</div>
							<!-- end row-->
						</div>



					</div> <!-- end tab-pane -->


					<!-- end about me section content -->
					<!-- Balances section -->
					<div class="tab-pane" id="balances">
						<h5 class="mb-3 text-uppercase">Account Balances</h5>
						<table class="table table-borderless mb-0">
							<thead class="thead-light">
								<tr>
									<th>Coin Types</th>
									<th>Deposit</th>
									<th>Promo</th>
									<th>Win</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
								@php
								$noOfCoin = $userprofile->user_points
								@endphp
								{{-- User Balance based on coin type id --}}
								@foreach($noOfCoin->whereNotIn('COIN_TYPE_ID', [11]) as $key =>$coin)
								<tr>
									<td>{{ $coin->coin_type->NAME }}</td>
									<td>{{ $coin->USER_DEPOSIT_BALANCE }}</td>
									<td>{{ $coin->USER_PROMO_BALANCE }}</td>
									<td>{{ $coin->USER_WIN_BALANCE }}</td>
									<td>{{ $coin->USER_TOT_BALANCE }}</td>

								</tr>
								@endforeach
							</tbody>
						</table>

					</div>
					<!-- end balances section -->
					<!-- start responsible gaming section -->
					<div class="tab-pane" id="responsiblegaming">
						<div class="card-box">
							<h5 class="mb-3 text-uppercase">Responsible Game</h5>
							<div class="table-responsive">
								<table class="table table-borderless table-hover table-centered m-0">

									<thead class="thead-light">
										<tr>
											<th>Game Type</th>
											{{--<th>From Date</th>
												<th>To Date</th>--}}
											<th>Limit </th>
											{{-- <th>Update Date</th>
												<th>Create Date</th> --}}
											<th>Status</th>
											{{-- <th>Updated By</th>--}}

										</tr>
									</thead>
									<tbody class="responsibleGamingData">

									</tbody>
								</table>
								<center>
									<div class="spinner-border text-primary m-2" id="resp_gaming" style="display:none;"></div>
								</center>
							</div>
						</div>

						<div class="card-box">

							<div class="clearfix"><a href="{{ route('responsiblegaming.depositlimit.editDepositLimit', encrypt($userprofile->USER_ID)) }}" class="float-right" target="_blank"><i class="fa fa-edit formEditViewBtn" style="cursor: pointer;"></i> Edit</a>
								<h5 class="mb-3 text-uppercase">Deposit Limit</h5>
							</div>

							<div class="table-responsive">
								<table class="table table-borderless table-hover table-centered m-0">

									<thead class="thead-light">
										<tr>
											<th>Perticulars</th>
											<th>Amount Limit </th>
											<th>Count</th>
										</tr>
									</thead>
									<tbody class="depositLimitData">

									</tbody>
								</table>
							</div>
						</div>

					</div>

					<!-- start reason section -->
					<div class="tab-pane" id="reason">
						<div class="card-box">
							<h5 class="mb-3 text-uppercase">Account activity log</h5>
							<div class="table-responsive">
								<table class="table table-borderless table-hover table-centered m-0" id="ReasonData">

									<thead class="thead-light">
										<tr>
										   <th>Date </th>
											<th>Change Account  Status</th>
											<th>Updated by</th>
											<th>Reasons</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
								<center>
								<div id="nodata" style="display:none;">Data Not Available</div>
								</center>
							</div>
						</div>
					</div>
					<!-- end reason sectionn -->

					<!-- end responsible Gaming Section -->
					<!-- start Baazi Rewards -->
					<div class="tab-pane" id="baazirewards">

						<h5 class="mb-3 text-uppercase"> Baazi Rewards</h5>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstname">Accumulated Coins :</label>
									<span id="accumulated_Coins"></span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="lastname">Coins Balance :</label>
									<span id="coins_balance"></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="lastname">Accumulated Reward Points :</label>
									<span id="accumulated_reward_points"></span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstname">Reward Points Spent :</label>
									<span id="reward_points_spent"></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="lastname">Reward Points Balance :</label>
									<span id="reward_points_balance"></span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="lastname">Current Rewards Program (CashBack or LRP) :</label>
									<span id="current_rewards_program"></span>
								</div>
							</div>
						</div>

					</div>
					<!-- end Baazi rewards -->

					<div class="tab-pane" id="depositwithdrawal">
						<div class="mb-3 d-flex justify-content-between align-items-center">
							<h5 class="mb-3 text-uppercase"> Deposit / Withdrawal Details</h5>
							<button onclick="resetPlayerLedger({{ $userprofile->USER_ID }})" class="btn btn-success resetButton waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Re-correct Ledger</button>
							<button style="display:none" class="btn btn-success resetButton waves-effect waves-light" type="button" disabled>
								<span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
								Processing...
							</button>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstname">Total Successful Deposit :</label>
									<span id="total_succesfull_deposit"></span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="lastname">Total Real Withdrawal :</label>
									<span id="total_approved_withdrawals"></span>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="lastname">Total Commission Withdrawal :</label>
									<span id="total_comm_withdrawals"></span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="lastname">Deposit - Withdrawal :</label>
									<span id="deposit_withdraw"></span>
								</div>
							</div>
						</div>
					</div>
					<!-- end timeline content-->

					<div class="tab-pane" id="taxdetails">

						<h5 class="mb-3 text-uppercase">Tax Details</h5>
						<table class="table table-borderless mb-0" id="taxTable">
							<thead class="thead-light">
								<tr>
									<th>FY</th>
									<th>Q1</th>
									<th>Q2</th>
									<th>Q3</th>
									<th>Q4</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
						<center>
							<div id="nodata" style="display:none;">Data Not Available</div>
						</center>

					</div>
					<!-- end settings content-->
					<!-- start KYC -->
					<div class="tab-pane" id="kyc">
						<h5 class="mb-4 text-uppercase"> User KYC</h5>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="firstname">PAN :</label>
									@if($userprofile->user_kyc->NON_PAN_URL ?? false)
									<a href="{{ $userprofile->user_kyc->NON_PAN_URL }}" class="image-popup" title="PAN Card">
										<span class="img-fluid">{{ $userprofile->user_kyc->PAN_NUMBER }}</span>
										<img src="{{ $userprofile->user_kyc->NON_PAN_URL }}" class="img-fluid" alt="" style="max-width: 0%; height: auto;">
									</a>
									@if($userprofile->user_kyc ?? false)
									@if($userprofile->user_kyc->NON_PAN_URL ?? false)
									{!! ($userprofile->user_kyc->PAN_STATUS==1?'<span style="cursor:pointer;" data-toggle="modal" data-target="#pan-kyc" class="badge badge-warning badge-pill">Pending</span>' : ($userprofile->user_kyc->PAN_STATUS==2?'<span style="cursor:pointer;" data-toggle="modal" data-target="#pan-kyc" class="badge badge-success badge-pill">Approve</span>':'<span style="cursor:pointer;" data-toggle="modal" data-target="#pan-kyc" class="badge badge-danger badge-pill">Reject</span>')) !!}
									@endif
									@endif
									@else
									<span class="badge badge-warning badge-pill" data-toggle="modal" data-target="#panKyc">N/A</span>
									@endif

								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="lastname">Address :</label>

									@if($userprofile->user_kyc->AADHAAR_NUMBER ?? false)
									<a href="{{ $userprofile->user_kyc->AADHAAR_FRONT_URL }}" class="image-popup" title="Address Front">
										<span>{{ $userprofile->user_kyc->AADHAAR_NUMBER }}</span>
										<img src="{{ $userprofile->user_kyc->AADHAAR_FRONT_URL }}" class="img-fluid" alt="" style="max-width: 0%; height: auto;">
									</a>
									<a href="{{ $userprofile->user_kyc->AADHAAR_BACK_URL }}" class="image-popup" title="Address Back" style="display:none;">
										<span>{{ $userprofile->user_kyc->AADHAAR_NUMBER }}</span>
										<img src="{{ $userprofile->user_kyc->AADHAAR_BACK_URL }}" class="img-fluid" alt="" style="max-width: 0%; height: auto;">
									</a>
									@if($userprofile->user_kyc->AADHAAR_NUMBER ?? false)
									{!! ($userprofile->user_kyc->AADHAAR_STATUS==1?'<span style="cursor:pointer;" data-toggle="modal" data-target="#address-kyc" class="badge badge-warning badge-pill">Pending</span>' : ($userprofile->user_kyc->AADHAAR_STATUS==2?'<span style="cursor:pointer;" data-toggle="modal" data-target="#address-kyc" class="badge badge-success badge-pill">Approve</span>':'<span style="cursor:pointer;" data-toggle="modal" data-target="#address-kyc" class="badge badge-danger badge-pill">Reject</span>')) !!}
									@endif
									@else
									<span class="badge badge-warning badge-pill" data-toggle="modal" data-target="#addressKyc">N/A</span>
									@endif
								</div>
							</div>
						</div>
						<h5 class="mb-4 text-uppercase">Bank Details</h5>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="lastname">Account Holder Name :</label>
									<span>{{ $userprofile->user_account_details->ACCOUNT_HOLDER_NAME ?? '--'  }}</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="lastname">Account Number :</label>
									<span>{{ $userprofile->user_account_details->ACCOUNT_NUMBER  ?? '--' }}</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="lastname">IFSC Code :</label>
									<span>{{ $userprofile->user_account_details->IFSC_CODE ?? '--' }}</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="lastname">Bank Name :</label>
									<span>{{ $userprofile->user_account_details->BANK_NAME ?? '--' }}</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="lastname">Branch Name :</label>
									<span>{{ $userprofile->user_account_details->BRANCH_NAME ?? '--' }}</span>
								</div>
							</div>
						</div>
					</div>
					<center>
						<div class="spinner-border text-primary m-2" role="status" id="commonid" style="display:none;"></div>
					</center>
					<!-- END KYC -->
				</div> <!-- end tab-content -->
			</div> <!-- end card-box-->

		</div> <!-- end col -->
	</div>
	<!-- end row-->

</div> <!-- container -->
<!--Start Verify Pan and Address -->
<div id="pan-kyc" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="{{ url('user/account/updateuserprofile') }}" method="post" class="formValidate">
				{{ csrf_field() }}
				<div class="modal-header">
					<h4 class="modal-title">Verify Pan </h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body p-4">
					<div class="row">
						<div class="col-md-12">
							@if($userprofile->user_kyc ?? false)
							<div class="form-group">
								<div class="radio radio-warning mb-2 form-check-inline">
									<input type="radio" name="panverify" id="panverify" value="1" @if($userprofile->user_kyc->PAN_STATUS==1){{ 'checked' }}@endif>
									<label for="radio7">
										Pending
									</label>
								</div>
								<div class="radio radio-success mb-2 form-check-inline">
									<input type="radio" name="panverify" id="panverify" value="2" @if($userprofile->user_kyc->PAN_STATUS==2){{ 'checked' }}@endif>
									<label for="radio4">
										Approve
									</label>
								</div>
								<div class="radio radio-danger mb-2 form-check-inline">
									<input type="radio" name="panverify" id="panverify" value="3" @if($userprofile->user_kyc->PAN_STATUS==3){{ 'checked' }}@endif>
									<label for="radio6">
										Reject
									</label>
								</div>
							</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group no-margin">
								<label for="field-7" class="control-label">Comment <span class="text-danger">*</span></label>
								<textarea class="form-control" name="comment" placeholder="Write Comment" required>@if($userprofile->user_kyc ?? false){{ $userprofile->user_kyc->PAN_COMMENT }}@endif</textarea>
							</div>
						</div>
					</div>
					<input type="hidden" name="USER_ID" value="{{ encrypt($userprofile->USER_ID) }}">
					<input type="hidden" name="kyc_type" value="pan_kyc">
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
					<input type="submit" name="verifyPan" class="btn btn-info waves-effect waves-light" value="Save changes">
				</div>
			</form>
		</div>
	</div>
</div><!-- /.modal -->

<div id="address-kyc" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="{{ url('user/account/updateuserprofile') }}" method="post" class="formValidate">
				{{ csrf_field() }}
				<div class="modal-header">
					<h4 class="modal-title">Verify Address </h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body p-4">
					<div class="row">
						<div class="col-md-12">
							@if($userprofile->user_kyc ?? false)
							<div class="form-group">
								<div class="radio radio-warning mb-2 form-check-inline">
									<input type="radio" name="addressverify" id="addressverify" value="1" @if($userprofile->user_kyc->AADHAAR_STATUS==1){{ 'checked' }}@endif>
									<label for="radio7">
										Pending
									</label>
								</div>
								<div class="radio radio-success mb-2 form-check-inline">
									<input type="radio" name="addressverify" id="addressverify" value="2" @if($userprofile->user_kyc->AADHAAR_STATUS==2){{ 'checked' }}@endif>
									<label for="radio4">
										Approve
									</label>
								</div>
								<div class="radio radio-danger mb-2 form-check-inline">
									<input type="radio" name="addressverify" id="addressverify" value="3" @if($userprofile->user_kyc->AADHAAR_STATUS==3){{ 'checked' }}@endif>
									<label for="radio6">
										Reject
									</label>
								</div>
							</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group no-margin">
								<label for="field-7" class="control-label">Comment <span class="text-danger">*</span></label>
								<textarea class="form-control" name="comment" placeholder="Write Comment" required>@if($userprofile->user_kyc ?? false ){{ $userprofile->user_kyc->AADHAAR_COMMENT }} @endif</textarea>
							</div>
						</div>
					</div>
					<input type="hidden" name="USER_ID" value="{{ encrypt($userprofile->USER_ID) }}">
				</div>
				<input type="hidden" name="kyc_type" value="address_kyc">
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
					<input type="submit" name="verifyPan" class="btn btn-info waves-effect waves-light" value="Save changes">
				</div>
			</form>
		</div>
	</div>
</div><!-- /.modal -->

<!-- End Verify Pan and Address -->
{{-- A/C Status Model --}}
<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<form  id="UserACStatusChange" action="{{ url('user/account/updateuserprofile') }}" method="post" class="formValidate">
				{{ csrf_field() }}
				<div class="modal-header">
					<h4 class="modal-title">Change User A/C Status </h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body p-4">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-3" class="control-label">Change A/C Status <span class="text-danger">*</span></label>
								<select class="form-control" name="changeAcStatus" onchange="changeStatus()" id="changeAcStatus">
									<option value="0" @if($userprofile->ACCOUNT_STATUS==0){{ 'Selected' }} @endif>Inactive</option>
									<option value="1" @if($userprofile->ACCOUNT_STATUS==1){{ 'Selected' }} @endif>Active</option>
									<option value="2" @if($userprofile->ACCOUNT_STATUS==2){{ 'Selected' }} @endif>Blocked</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="field-3" class="control-label">Reason <span class="text-danger">*</span></label>

								<select class="form-control" name="reason" id="reason" required="">
									<option value="" selected="">Select</option>

									@foreach($reasonsResult as $reasons)

									<option value="{{ $reasons->ACTIONS_REASONS_ID }}" {{ $reasons->ACTIONS_REASONS_ID == ($UserAccountLogsRes->REASON_ID ?? null) ? "selected" : '' }}>{{ $reasons->ACTIONS_REASON  }}</option>

									@endForeach
								</select>
								<input type="hidden" name="USER_ID" value="{{ $userprofile->USER_ID }}">
								<input type="hidden" name="USERNAME" value="{{ $userprofile->USERNAME }}">
							</div>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
					<input type="submit" name="updateAcStatus" id="updateAcStatus" class="btn btn-success waves-effect waves-light formActionButtonvv" value="Save changes">
                  <button style="display:none" class="btn btn-primary formActionButtonvv mr-1" type="button" disabled>
                     <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                     Loading...
                  </button>
				</div>
			</form>
		</div>
	</div>
</div><!-- /.modal -->
<!-- PAN KYC MODEL -->
<div id="panKyc" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="formValidate" action="{{ url('user/account/updateuserprofile') }}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="modal-header">
					<h4 class="modal-title" id="myModalLabel">Add PAN Details</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<div>
						<h6>PAN Number</h6>
						<input type="text" class="form-control" maxlength="10" id="PAN_NO" onkeyup="uperCase('PAN_NO')" name="PAN_NO" value="" required>
					</div>
					<span id='valid_pan'></span>
					<div class="mt-3">
						<input type="file" class="dropify" name="PAN_IMG" data-max-file-size="1M" value="">
					</div>
					<input type="hidden" name="USER_ID" value="{{ encrypt($userprofile->USER_ID) }}">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary waves-effect waves-light" name="upload_pan" value="Upload Pan">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Address Kyc Model Start -->
<div id="addressKyc" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Address Details</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="row">
				<div class="card-body pb-0 pt-0">
					<form action="{{ url('user/account/updateuserprofile') }}" method="POST" enctype="multipart/form-data" id="myAwesomeDropzone">
						{{ csrf_field() }}
						<div class="modal-body">
							<div class="form-group">
								<h6>Card Number</h6>
								<input type="text" class="form-control" name="address_no" value="" required>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<div class="mt-3">
										<input type="file" class="dropify" name="addressFront" data-max-file-size="1M" value="">
									</div>
								</div>
								<div class="form-group col-md-6">
									<div class="mt-3">
										<input type="file" class="dropify" name="addressBack" data-max-file-size="1M" value="">
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="USER_ID" value="{{ encrypt($userprofile->USER_ID) }}">
						<div class="modal-footer clearfix">
							<button type="button" class="btn btn-light waves-effect" data-dismiss="modal">Close</button>
							<input type="submit" class="btn btn-primary waves-effect waves-light" name="upload_address" value="Upload Address">
						</div>
				</div>

				</form>
			</div> <!-- end card-body-->
		</div>
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
<!-- Address Kyc Model End -->
@endsection

@section('js')
<script src="{{ asset('assets/libs/dropify/dropify.min.js') }}"></script>
<script src="{{ asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/form-fileuploads.init.js') }}"></script>
<script src="{{ asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/libs/x-editable/x-editable.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/form-xeditable.init.js') }}"></script>
<script src="{{ asset('assets/js/pages/gallery.init.js') }}"></script>
<script src="{{ asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/sweet-alerts.init.js') }}"></script>

@endsection
@section('post_js')
<script src="{{ asset('js/bo/user_profile.js') }}"></script>
<script type="text/javascript">
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$(document).ready(function() {
	$('#updateAcStatus').on('click', (e) => {
         if ($("#UserACStatusChange").valid()) {
            $('.formActionButtonvv').toggle();
            $('#updateAcStatus').hide();
         }
	  });
	});
</script>
<script type="text/javascript">
	function resetPlayerLedger(userId) {
		$('.resetButton').toggle();
		Swal.fire({
			title: 'Are you sure? You want to recorrect.',
			text: "You won't be able to revert this!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Proceed!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					"url": "{{url('/user/account/resetplayerledger')}}",
					"method": "POST",
					"data": {
						user_id: userId,
						_token: '<?php echo csrf_token() ?>'
					},
					success: function(data) {
						$('.resetButton').toggle();
						if (data == "SUCCESS") {
							$.NotificationApp.success(`${'Player ledger recorrected Successfully.'}`, `${'Success'}`);
						} else if (data == "FAILED") {
							$.NotificationApp.error(`${'Failed to re-correct Player Ledger.'}`, `${'Failed'}`);
						} else if (data == "PENDING") {
							$.NotificationApp.error(`${'Failed to re-correct due to pending transactions.'}`, `${'Failed'}`);
						}

					}
				});
			} else {
				$('.resetButton').toggle();
			}
		})
	}
</script>
@endsection
