<html>
<head>

<meta charset="UTF-8">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">
<style type="text/css">


   *{ margin: 0; padding:0;box-sizing:border-box;font-family: 'Roboto', sans-serif; }
      .container{ width:100%; margin: 0 auto }

   .sdilaystarts{ padding:8px 0 0 ;  text-align: left; margin-bottom: 8px; }
   .row{ margin:0 -10px; clear: both;  }
   .row::after{display:block;clear:both;content:""}
   .row .w-50{ width: 100%; float: left ; padding:0 10px;}
   .stablestats{ padding:6px 15px ; background-color:#ccc;  font-size: 17px; font-weight: 700;  text-align: center;text-transform: uppercase;}
   
   .pagebreak{page-break-before: always;}
   
   .tabletop,.fulltable{ width: 100%; margin: 0 0 15px 0;padding:0; border:1px #eee solid !important; ; } 
   .tabletop td{ text-align: center; border:1px #eee solid !important;}
   .tabletop tr:nth-of-type(2n+1) td,.fulltable tr:nth-of-type(2n+1) td{border:1px #eee solid  !important; background-color: #f3f6f9 }

   .fulltable th, .fulltable td{text-align: left;border:1px #eee  solid !important; }
   .fulltable th, .fulltable td,.tabletop td{ padding:4px 5px;   font-size: 13px;border:1px #eee solid  !important; font-weight: 500;} 
   .fulltable th,.firstchu td:first-child{ }
   .fulltable th{background-color: #333 !important;color: #fff; border:1px #eee solid !important;}
   .Overlass{ overflow: auto; }
   @media (min-width:768px){
   .row{ margin:0 -15px; clear: both;  } 
   .row .w-50{ width: 50%; padding:0 15px;}
   .container{ width: 100%; margin: 0 auto }
  
   .fulltable th, .fulltable td,.tabletop td{ padding:6px 15px; border:1px #eee solid !important;} }
   .fulltable td,.tabletop td{   }
	.bgbb-1, .bgbb-1 td {
    background-color: #203764 !important;
    color: #fff;
    border: 1px #eee solid !important;
	
	}
	
</style>
</head><body>
<div class="container">

	<table width="100%" cellspacing="10" cellpadding="10" border="0"  >   
	<tr> 
		<td ><img src="{{ public_path('images/pokerbaazi-logo-blue.png') }}" width="250"  alt="PokerBaazi"></td>
		<td align="cellspacing"></td>
		<td align="right"><h3>{!! $yesterday_date !!} 00:00:00 - 23:59:59</h3></td>
	</tr>
	</table>
  <table width="100%" cellspacing="10" cellpadding="10" border="0"  >
      <tr>
         <td width="50%" style="vertical-align: top"  >
            <div class="stablestats">User KPI's</div>
            <table class="tabletop firstchu" cellpadding="0" cellspacing="0">
               <tr>
                  <td width="55%" class="bgbb-1">Daily Active Users </td>
                  <td width="45%">{{ $userDAULogins }}</td>
               </tr>
               <tr>
                  <td class="bgbb-1">Registrations  </td>
                  <td>{{ $signUp }}</td>
               </tr>
               <tr>
                  <td width="55%"></td>
                  <td width="45%"></td>
               </tr>
               <tr>
                  <td class="bgbb-1">Transferred to PB Wallet User Count </td>
                  <td>{{ $toPbWalletUserCount }}</td>
               </tr>
               <tr>
                  <td class="bgbb-1">Transferred to PB Wallet Amount </td>
                  <td>{{ $toPbWalletAmount}}</td>
               </tr>
               <tr>
                  <td class="bgbb-1">Transferred from PB Wallet User Count </td>
                  <td>{{ $fromPbWalletUserCount }}</td>
               </tr>
               <tr>
                  <td class="bgbb-1">Transferred from PB Wallet Amount </td>
                     <td>{{ $fromPbWalletAmount}}</td>
               </tr>
               <tr>
                  <td width="55%"></td>
                  <td width="45%"></td>
               </tr>
               <tr>
                  <td class="bgbb-1">First Time Transferred to PB Wallet  </td>
                  <td>{{ $firstTimetoPBWalletUserCount }}</td>
               </tr>
               <tr>
                  <td class="bgbb-1">First Time Transferred back to BB Wallet  </td>
                  <td>{{ $firstTimeBacktoBBWalletUserCount }}</td>
               </tr>
               <tr>
                  <td width="55%" ></td>
                  <td width="45%"></td>
               </tr>
               <tr>
                  <td class="bgbb-1">Unique Wagering users (Playing) </td>
                  <td>{{ $uniqueWageringUserPlayingCount }}</td>
               </tr>
               <tr>
                  <td class="bgbb-1">Unique Wagering users (non-depositors) </td>
                     <td>{{ $uniqueWageringUserPlayingCountNonDepositer }}</td>
               </tr>
               <tr>
                  <td class="bgbb-1">Unique freeroll playing users  </td>
                  <td>{{ $uniqueFreerollUserPlayingCount }}</td>
               </tr>
            </table>
      

         </td>
         <td width="50%"  style="vertical-align: top" >
            <div class="stablestats">Revenue KPI's</div>
         
            <table class="tabletop firstchu"  cellpadding="0" cellspacing="0">
               <tr>
                  <td width="55%" class="bgbb-1">Gross Revenue  </td>
                  @if(isset($grossRevenue) && !empty($grossRevenue))
                     <td width="45%">{{ $grossRevenue }}</td>
                  @else
                  <td></td>
                  @endif
               </tr>
               <tr>
                  <td class="bgbb-1">Bonus Revenue   </td>
                  @if(isset($bonusRevenue) && !empty($bonusRevenue))
                  <td>{{ $bonusRevenue }}</td>
                  @else
                  <td></td>
                  @endif
               </tr>
               <tr>
                  <td class="bgbb-1">Net Revenue   </td>
                  @if(isset($netRevenue) && !empty($netRevenue))
                     <td>{{ $netRevenue }}</td>
                  @else
                  <td></td>
                  @endif
               </tr>
               
            </table>
         </td>
      </tr>
      
      <table><tr><td></td></tr></table>
      <table><tr><td></td></tr></table>
      <table><tr><td></td></tr></table>
      <div class="pagebreak"></div>
      <div class="stablestats">Game KPI's</div>
	   <table class="fulltable"  cellpadding="0" cellspacing="0">
         
         <thead>
            <tr>
               <th>Game Type </th>
               <th>GGR  </th>
               <th>NGR  </th>
               <th>Revenue generated through Bonus  </th>
               <th>Unique users  </th>
               <th>Hands  </th>
               <th>ARPU  </th>
            </tr>
         </thead>
		   @if(isset($gameTypeFiveCardtaxesOmaha) && !empty($gameTypeFiveCardtaxesOmaha))
            @foreach($gameTypeFiveCardtaxesOmaha as $key=> $gameType)
               @php
                  if(isset($gameType) && !empty($gameType)){
                     $mini_games_type= $gameType->MINIGAMES_TYPE_NAME;
                     $GGR 		= $gameType->GGR;
                     $NGR 		= $gameType->NGR;
                     $Bonus 	= $gameType->Bonus;
                     $Unique_users 		= $gameType->Unique_users;
                     $hands 		= $gameType->hands;
                     $ARPU 		= $gameType->ARPU;
                  }else{
                     $mini_games_type= $key;
                     $GGR 	= '0';
                     $NGR = '0';
                     $Bonus 		= '0';
                     $Unique_users 	= '0';
                     $hands 	= '0';
                     $ARPU 		= '0';
                  }
               @endphp
               <tr>
                  <td >{{ $mini_games_type }}</td>
                  <td >{{ $GGR }}</td>
                  <td >{{ $NGR }}</td>
                  <td >{{ $Bonus }}</td>
                  <td >{{ $Unique_users }}</td>
                  <td >{{ $hands }}</td>
                  <td >{{ $ARPU }}</td>
               
               </tr>
            @endforeach
         @endif
         @if(isset($gameTypeOfc) && !empty($gameTypeOfc))
            @foreach($gameTypeOfc as $key=> $ofcgameType)
               @php
                  if(isset($ofcgameType) && !empty($ofcgameType)){
                     $ofcmini_games_type= $ofcgameType->MINIGAMES_TYPE_NAME;
                     $ofcGGR 		= $ofcgameType->GGR;
                     $ofcNGR 		= $ofcgameType->NGR;
                     $ofcBonus 	= $ofcgameType->Bonus;
                     $ofcUnique_users 		= $ofcgameType->Unique_users;
                     $ofchands 		= $ofcgameType->hands;
                     $ofcARPU 		= $ofcgameType->ARPU;
                  }else{
                     $ofcmini_games_type= $key;
                     $ofcGGR 	= '0';
                     $ofcNGR = '0';
                     $ofcBonus 		= '0';
                     $ofcUnique_users 	= '0';
                     $ofchands 	= '0';
                     $ofcARPU 		= '0';
                  }
               @endphp
               <tr>
                  <td >{{ $ofcmini_games_type }}</td>
                  <td >{{ $ofcGGR }}</td>
                  <td >{{ $ofcNGR }}</td>
                  <td >{{ $ofcBonus }}</td>
                  <td >{{ $ofcUnique_users }}</td>
                  <td >{{ $ofchands }}</td>
                  <td >{{ $ofcARPU }}</td>
               
               </tr>
            @endforeach
		   @endif
         @if(isset($tournamentGame) && !empty($tournamentGame))
            @foreach($tournamentGame as $tournamentGameType)
               <tr>
                  <td>Tournaments </td>
                  <td>{{ $tournamentGameType->GGR }}</td>
                  <td>{{ $tournamentGameType->NGR }}</td>
                  <td>{{ $tournamentGameType->Bonus }}</td>
                  <td>{{ $tournamentGameType->UniqueUser }}</td>
                  <td>NA</td>
                  <td>{{ $tournamentGameType->ARPU }}</td>
               </tr>
            @endforeach
            @else
            <tr>
            <td>Tournaments </td>
               <td >0</td>
               <td >0</td>
               <td >0</td>
               <td >0</td>
               <td >NA</td>
               <td >0</td>
            </tr>
         @endif
      
		   <tr><td></td></tr>
      </table>

         
   </table>
   </div>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,600"  media="all" type="text/css" >
</body>
</html>
