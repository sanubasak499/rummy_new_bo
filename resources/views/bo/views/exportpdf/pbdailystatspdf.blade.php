<html>
<head>

<meta charset="UTF-8">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">
<style type="text/css">


   *{ margin: 0; padding:0;box-sizing:border-box;font-family: 'Roboto', sans-serif; }
      .container{ width:100%; margin: 0 auto }

   .sdilaystarts{ padding:8px 0 0 ;  text-align: left; margin-bottom: 8px; }
   .row{ margin:0 -10px; clear: both;  }
   .row::after{display:block;clear:both;content:""}
   .row .w-50{ width: 100%; float: left ; padding:0 10px;}
   .stablestats{ padding:6px 15px ; background-color:#ccc;  font-size: 17px; font-weight: 700;  text-align: center;text-transform: uppercase;}
   
   .pagebreak{page-break-before: always;}
   
   .tabletop,.fulltable{ width: 100%; margin: 0 0 15px 0;padding:0; border:1px #eee solid !important; ; } 
   .tabletop td{ text-align: center; border:1px #eee solid !important;}
   .tabletop tr:nth-of-type(2n+1) td,.fulltable tr:nth-of-type(2n+1) td{border:1px #eee solid  !important; background-color: #f3f6f9 }

   .fulltable th, .fulltable td{text-align: left;border:1px #eee  solid !important; }
   .fulltable th, .fulltable td,.tabletop td{ padding:4px 5px;   font-size: 13px;border:1px #eee solid  !important; font-weight: 500;} 
   .fulltable th,.firstchu td:first-child{ }
   .fulltable th{background-color: #333 !important;color: #fff; border:1px #eee solid !important;}
   .Overlass{ overflow: auto; }
   @media (min-width:768px){
   .row{ margin:0 -15px; clear: both;  } 
   .row .w-50{ width: 50%; padding:0 15px;}
   .container{ width: 100%; margin: 0 auto }
  
   .fulltable th, .fulltable td,.tabletop td{ padding:6px 15px; border:1px #eee solid !important;} }
   .fulltable td,.tabletop td{   }
	.bgbb-1, .bgbb-1 td {
    background-color: #203764 !important;
    color: #fff;
    border: 1px #eee solid !important;
	
	}
	
</style>
</head><body>
<div class="container">

	<table width="100%" cellspacing="10" cellpadding="10" border="0"  >   
	<tr> 
		<td ><img src="{{ public_path('images/pokerbaazi-logo-blue.png') }}" width="250"  alt="PokerBaazi"></td>
		<td align="cellspacing"></td>
		<td align="right"><h3>{!! $yesterday_date !!} 00:00:00 - 23:59:59</h3></td>
	</tr>
	</table>
  <table width="100%" cellspacing="10" cellpadding="10" border="0"  >
	<tr>
    <td width="50%" style="vertical-align: top"  >
         <div class="stablestats">User Stats</div>
         <table class="tabletop firstchu" cellpadding="0" cellspacing="0">
            <tr>
               <td width="55%" class="bgbb-1">DAUs (logins) </td>
               <td width="45%">{{ $userDAULogins }}</td>
            </tr>
            <tr>
               <td class="bgbb-1">Signups  </td>
               <td>{{ $signUp }}</td>
            </tr>
            <tr>
               <td class="bgbb-1">FTDs  </td>
               <td>{{ $ftdReports->ftds }}</td>
            </tr>
            <tr>
				<td class="bgbb-1">FTWs  </td>
				<td>{{ $ftwsReport->ftws }}</td>
            </tr>
            <tr>
               <td class="bgbb-1">Unique depositors  </td>
				@if(isset($uniqueDepositer) && !empty($uniqueDepositer))
               <td>{{ $uniqueDepositer->unique_dep}}</td>
				@else
				<td></td>
				@endif
            </tr>
            <tr>
			
				<td class="bgbb-1">Unique Withdrawing users  </td>
				@if(isset($uniqueWithdrawingUser) && !empty($uniqueWithdrawingUser))
				<td>{{ $uniqueWithdrawingUser->Unique_Withdrawing_users }}</td>
				@else
				<td></td>
				@endif
            </tr>
            <tr>
               <td class="bgbb-1">Unique wagering users (players)  </td>
			   <td>{{ $UniqueWageringUser }}</td>
			 </tr>
            <tr>
				<td class="bgbb-1">Lifetime Depositors Wagering( LTDW) </td>
				@if(isset($lifetimeDepositorsWagering) && !empty($lifetimeDepositorsWagering))
				<td>{{ $lifetimeDepositorsWagering->Lifetime_Depositors_Wagering }}</td>
				@else
				<td></td>
				@endif
            </tr>
            <tr>
               <td class="bgbb-1">Wagering users (non-depositors) </td>
			   @if(isset($wageringusers_non_depositors) && !empty($wageringusers_non_depositors))
               <td>{{ $wageringusers_non_depositors->Wagering_users }}</td>
				@else
				<td></td>
				@endif
            </tr>
            <tr>
				<td class="bgbb-1">Unique freeroll playing users  </td>
				<td>{{ $uniqueFreerollPlayingUsers[0]->Unique_freeroll_playing_users }}</td>
			</tr>
         </table>
	

      </td>
	  <td width="50%"  style="vertical-align: top" >
         <div class="stablestats">Financials  </div>
		
         <table class="tabletop firstchu"  cellpadding="0" cellspacing="0">
            <tr>
               <td width="55%" class="bgbb-1">Gross Revenue  </td>
			   @if(isset($grossRevenue) && !empty($grossRevenue))
                <td width="45%">{{ $grossRevenue }}</td>
				@else
				<td></td>
				@endif
            </tr>
            <tr>
               <td class="bgbb-1">Bonus Revenue   </td>
			   @if(isset($bonusRevenue) && !empty($bonusRevenue))
				<td>{{ $bonusRevenue }}</td>
			   @else
				<td></td>
				@endif
            </tr>
            <tr>
               <td class="bgbb-1">Net Revenue (ex-Bonus rev & GST)   </td>
			    @if(isset($netRevenue) && !empty($netRevenue))
               <td>{{ $netRevenue }}</td>
				@else
				<td></td>
				@endif
            </tr>
            <tr>
               <td class="bgbb-1">Deposits   </td>
			   @if(isset($deposit) && !empty($deposit))
               <td>{{ $deposit }}</td>
				@else
				<td></td>
				@endif
            </tr>
            <tr>
               <td class="bgbb-1">Withdrawals</td>
			    @if(isset($withdraw) && !empty($withdraw))
               <td>{{ $withdraw }}</td>
			   @else
				<td></td>
				@endif
            </tr>
			<tr>
               <td class="bgbb-1">Pending Withdrawals</td>
			    @if(isset($pendingWithdrawalAmount) && !empty($pendingWithdrawalAmount))
               <td>{{ $pendingWithdrawalAmount }}</td>
			   @else
				<td></td>
				@endif
            </tr>
            <tr>
               <td class="bgbb-1">Net Cash   </td>
			    @if(isset($netCash) && !empty($netCash))
               <td>{{ $netCash }}</td>
			   @else
				<td></td>
				@endif
            </tr>
			<tr>
               <td class="bgbb-1">Total promo cost</td>
			    @if(isset($TPC) && !empty($TPC))
               <td>{{ $TPC }}</td>
			   @else
				<td></td>
				@endif
            </tr>

			<tr>
               <td class="bgbb-1">NGR/GGR</td>
			   @if(isset($ngrGGR) && !empty($ngrGGR))
			    <td> {{ $ngrGGR }}</td>
				@else
				<td></td>
				@endif
            </tr>

			<tr>
               <td class="bgbb-1">Withdrawable player balance (WPB) </td>
			    @if(isset($WPB) && !empty($WPB))
               <td>{{ $WPB }}</td>
			   @else
				<td></td>
				@endif
            </tr>
             <tr>
               <td class="bgbb-1"> Change in WPB</td>
			   @if(isset($changeInWPB) && !empty($changeInWPB))
               <td>{{ $changeInWPB }}</td>
				@else
				<td></td>
				@endif
            </tr>
            <tr>
               <td class="bgbb-1">% Change in WPB </td>
				@if(isset($percentchangeInWPB) && !empty($percentchangeInWPB))
               <td>{{ $percentchangeInWPB }}</td>
				@else
				<td></td>
				@endif
            </tr>
            <tr>
               <td class="bgbb-1">Net Cash (Including Liabilities)</td>
				@if(isset($netCashIncLia) && !empty($netCashIncLia))
				<td>{{ $netCashIncLia }}</td>
				@else
				<td></td>
				@endif
            </tr>

			<tr>
               <td class="bgbb-1">Current month deposit</td>
			    @if(isset($depo_amount) && !empty($depo_amount))
                <td>{{ $depo_amount }}</td>
				@else
				<td></td>
				@endif
            </tr>
            <tr>
               <td class="bgbb-1">Current month withdrawal </td>
			    @if(isset($withdra_amount) && !empty($withdra_amount))
                <td>{{ $withdra_amount }}</td>
				@else
				<td></td>
				@endif
            </tr>
            <tr>
               <td class="bgbb-1">Current Month Net cash </td>
			    @if(isset($net_amount) && !empty($net_amount))
                <td>{{ $net_amount }}</td>
				@else
				<td></td>
				@endif
            </tr>

            <tr>
               <td class="bgbb-1">Monthly Liability Change</td>
				@if(isset($monthLiaChng) && !empty($monthLiaChng))
				<td>{{ $monthLiaChng }}</td>
				@else
				<td></td>
				@endif
            </tr>
			<tr>
               <td class="bgbb-1">Monthly TDS</td>
               @if(isset($monthlyTDS) && !empty($monthlyTDS))
				<td>{{ $monthlyTDS }}</td>
				@else
				<td></td>
				@endif
            </tr>
			<tr>
               <td class="bgbb-1">Monthly Commission</td>
				@if(isset($monthlyCOMM) && !empty($monthlyCOMM))
				<td>{{ $monthlyCOMM }}</td>
				@else
				<td></td>
				@endif
            </tr>
         </table>
      </td>
	  </tr>
	<table class="fulltable"  cellpadding="0" cellspacing="0">
         <thead>
            <tr>
               <th>Game Type </th>
               <th>GGR  </th>
               <th>NGR  </th>
               <th>Revenue generated through Bonus  </th>
               <th>Unique users  </th>
               <th>Hands  </th>
               <th>ARPU  </th>
            </tr>
        </thead>
		@if(isset($gameTypeTexasPlo5cardPloOfc) && !empty($gameTypeTexasPlo5cardPloOfc))
		@foreach($gameTypeTexasPlo5cardPloOfc as $key=> $gameType)
				@php
					if(isset($gameType) && !empty($gameType)){
						$mini_games_type= $gameType->MINIGAMES_TYPE_NAME;
						$GROSS_RAKE 		= $gameType->GROSS_RAKE;
						$NET_RAKE 	= $gameType->NET_RAKE;
						$BONUS_REVENUE 		= $gameType->BONUS_REVENUE;
						$PLAYING_USER 		= $gameType->PLAYING_USER;
						$NUMBER_OF_HANDS 		= $gameType->NUMBER_OF_HANDS;
						$ARPU 		= $gameType->ARPU;
					}else{
						$mini_games_type= $key;
						$GROSS_RAKE 	= '0';
						$NET_RAKE 		= '0';
						$BONUS_REVENUE 		= '0';
						$PLAYING_USER 		= '0';
						$NUMBER_OF_HANDS 		= '0';
						$ARPU 		= '0';
					}
				@endphp
          <tr>
            <td >{{ $mini_games_type }}</td>
            <td >{{ $GROSS_RAKE }}</td>
            <td >{{ $NET_RAKE }}</td>
            <td >{{ $BONUS_REVENUE }}</td>
            <td >{{ $PLAYING_USER }}</td>
            <td >{{ $NUMBER_OF_HANDS }}</td>
            <td >{{ $ARPU }}</td>
            
         </tr>
		@endforeach
		@endif
		@if(isset($gameTypeTournament) && !empty($gameTypeTournament))
		@foreach($gameTypeTournament as $gameTypetour)
          <tr>
            <td>Tournaments </td>
            <td >{{ $gameTypetour->GGR }}</td>
            <td >{{ $gameTypetour->NGR }}</td>
            <td >{{ $gameTypetour->REVENUE_GENERATED_THROUGH_BONUS }}</td>
			<td >{{ $gameTypetour->PLAYING_USER }}</td>
            <td ></td>
            <td >{{ $gameTypetour->ARPU }}</td>
         </tr>
		@endforeach
		@else
		<tr>
			<td>Tournaments </td>
            <td >0</td>
            <td >0</td>
            <td >0</td>
			<td >0</td>
            <td ></td>
            <td >0</td>
			</tr>
		@endif
      
		 @if(isset($gameTypeHighMidLow) && !empty($gameTypeHighMidLow))
		 @foreach($gameTypeHighMidLow as $key=> $gameTypehml)
			@php
				if(isset($gameTypehml) && !empty($gameTypehml)){
					$BIGBLIND= $gameTypehml->BIG_BLIND_CASE;
					$GROSS_RAKE 		= $gameTypehml->GROSS_RAKE;
					$NET_RAKE 	= $gameTypehml->NET_RAKE;
					$BONUS_REVENUE 		= $gameTypehml->BONUS_REVENUE;
					$PLAYING_USER 		= $gameTypehml->PLAYING_USER;
					$NUMBER_OF_HANDS 		= $gameTypehml->NUMBER_OF_HANDS;
					$ARPU 		= $gameTypehml->ARPU;
				}else{
					$BIGBLIND= $key;
					$GROSS_RAKE 	= '0';
					$NET_RAKE 		= '0';
					$BONUS_REVENUE 		= '0';
					$PLAYING_USER 		= '0';
					$NUMBER_OF_HANDS 		= '0';
					$ARPU 		= '0';
				}
			@endphp
          <tr class="bgbb-1">
			<td >{{ $BIGBLIND }}</td>
            <td >{{ $GROSS_RAKE }}</td>
            <td >{{ $NET_RAKE }}</td>
            <td >{{ $BONUS_REVENUE }}</td>
            <td >{{ $PLAYING_USER }}</td>
            <td >{{ $NUMBER_OF_HANDS }}</td>
            <td >{{ $ARPU }}</td>
         </tr>
		 @endforeach
		 @endif
		<tr><td></td></tr>
      </table>
	
   
	<div class="stablestats">Promo split</div>
   
      <table class="fulltable"  cellpadding="0" cellspacing="0">
         <thead>
            <tr class="bgbb-1">
               <th>Bonus categories  </th>
               <th>Amount</th>
               <th>Unique users  </th>
               <th>No. of txns  </th>
               <th>Avg amt/use  </th>
           </tr>
         </thead>
        @if(isset($partnerToSite) && !empty($partnerToSite))
			@foreach($partnerToSite as $key=> $promo)
				@php
					if(isset($promo) && !empty($promo)){
						$bonusCategories= $promo->Bonus_categories;
						$amount 		= $promo->Amount;
						$uniqueUsers 	= $promo->Unique_users;
						$noOfTxns 		= $promo->No_of_txns;
						$avgAmt 		= $promo->Avg_amt;
					}else{
						$bonusCategories= $key;
						$amount 		= '0';
						$uniqueUsers 	= '0';
						$noOfTxns 		= '0';
						$avgAmt 		= '0';
					}
				@endphp
			<tr class="bgbb-1">
				<td >{{ $bonusCategories }}</td>
				<td >{{ $amount }}</td>
				<td >{{ $uniqueUsers }}</td>
				<td >{{ $noOfTxns }}</td>
				<td >{{ number_format($avgAmt,2) }}</td>
			</tr>
			@endforeach
		@endif
		@if(isset($rCB) && !empty($rCB))
		@foreach($rCB as $key=> $rcb)
        <tr class="bgbb-1">
            <td >Loyalty program RCB</td>
            <td >{{ $rcb->RCB_AMOUNT }}</td>
            <td >{{ $rcb->RCB_UINQUE_USERS }}</td>
            <td >{{ $rcb->RCB_NO_OF_TXN }}</td>
            <td >{{ number_format($rcb->RCB_AVG_PROMO,2) }}</td>
        </tr>
		@endforeach
		@else
		<tr class="bgbb-1">
			<td>Loyalty program RCB</td>
            <td >0</td>
            <td >0</td>
            <td >0</td>
			<td >0</td>
		</tr>
        @endif
		@if(isset($rCC) && !empty($rCC))
		@foreach($rCC as $key=> $rcc)
         <tr class="bgbb-1">
            <td  class="bgbb-1">Loyalty program RCC  </td>
            <td >{{ $rcc->RCC_AMOUNT }}</td>
            <td >{{ $rcc->RCC_UNIQUE_USERS }}</td>
            <td >{{ $rcc->RCC_NO_OF_TXN }}</td>
            <td >{{ number_format($rcc->RCC_AVG_PROMO,2) }}</td>
        </tr>
		@endforeach
		@else
		<tr class="bgbb-1">
			<td>Loyalty program RCC</td>
            <td >0</td>
            <td >0</td>
            <td >0</td>
			<td >0</td>
		</tr>   
		@endif
        @foreach($tournamentTicketsExpense as $key=> $tour_Tickets_Expense)
         <tr class="bgbb-1">
            <td  class="bgbb-1"> Tournament tickets expense  </td>
            <td >{{ $tour_Tickets_Expense->TOURNAMENT_TICKETS_EXPENSE_AMOUNT }}</td>
            <td ></td>
            <td >{{ $tour_Tickets_Expense->TOURNAMENT_TICKETS_EXPENSE_NO_OF_TXN }}</td>
            <td ></td>
        </tr>
		@endforeach
		 @foreach($tournamentOverlays as $key=> $tour_Overlays)
         <tr class="bgbb-1">
            <td  class="bgbb-1"> Tournament Overlays  </td>
            <td >{{ $tour_Overlays->TOURNAMENT_OVERLAYS_AMOUNT }}</td>
            <td >{{ $tour_Overlays->OVERLAY_TOURNAMENT_COUNT }}</td>
            <td >{{ $tour_Overlays->TOTAL_TOURNAMENT }}</td>
            <td ></td>
       </tr>
		@endforeach
      </table>
   
   <div class="pagebreak"></div>
   <table><tr><td></td></tr></table>
   <table><tr><td></td></tr></table>
   <table><tr><td></td></tr></table>
<div class="stablestats">Texas Hold'em</div>
   
      <table class="fulltable"  cellpadding="0" cellspacing="0">
         <thead>
            <tr>
               <th>Stakes (Big Blind)  </th>
               <th>GGR  </th>
               <th>NGR  </th>
               <th>Revenue generated through Bonus </th>
               <th>Unique users  </th>
               <th>Hands </th>
               <th>ARPU </th>
           
            </tr>
         </thead>
		@if(isset($stacks) && !empty($stacks))
		@foreach($stacks as $stack)
        <tr>
            <td >{{ $stack->BIG_BLIND }}</td>
            <td >{{ $stack->GROSS_RAKE }}</td>
            <td >{{ $stack->NET_RAKE }}</td>
            <td >{{ $stack->BONUS_REVENUE }}</td>
            <td >{{ $stack->PLAYING_USER_TEXAS }}</td>
            <td >{{ $stack->NUMBER_OF_HANDS }}</td>
            <td >{{ $stack->ARPU }}</td>
        </tr>  
		@endforeach
		@endif 
		@if(isset($texasHoldemBigBlindStacksSmallHighMid) && !empty($texasHoldemBigBlindStacksSmallHighMid))
         @foreach($texasHoldemBigBlindStacksSmallHighMid as $key=> $stack)
				@php
					if(isset($stack) && !empty($stack)){
						$BIG_BLIND_CASE= $stack->BIG_BLIND_CASE;
						$GROSS_RAKE 		= $stack->GROSS_RAKE;
						$NET_RAKE 	= $stack->NET_RAKE;
						$BONUS_REVENUE 		= $stack->BONUS_REVENUE;
						$PLAYING_USER_TEXAS 		= $stack->PLAYING_USER_TEXAS;
						$NUMBER_OF_HANDS 		= $stack->NUMBER_OF_HANDS;
						$ARPU 		= $stack->ARPU;
					}else{
						$BIG_BLIND_CASE= $key;
						$GROSS_RAKE 		= '0';
						$NET_RAKE 	= '0';
						$BONUS_REVENUE 		= '0';
						$PLAYING_USER_TEXAS 		= '0';
						$NUMBER_OF_HANDS 		= '0';
						$ARPU 		= '0';
					}
				@endphp
		<tr class="bgbb-1">
            <td >{{ $BIG_BLIND_CASE }}</td>
            <td >{{ $GROSS_RAKE }}</td>
            <td >{{ $NET_RAKE }}</td>
            <td >{{ $BONUS_REVENUE }}</td>
            <td >{{ $PLAYING_USER_TEXAS }}</td>
            <td >{{ $NUMBER_OF_HANDS }}</td>
            <td >{{ $ARPU }}</td>
        </tr>
		@endforeach
		@endif
      </table>  
 <table><tr><td></td></tr></table>
 <div class="pagebreak"></div>
   <table><tr><td></td></tr></table>
   <table><tr><td></td></tr></table>
   <table><tr><td></td></tr></table>
<div class="stablestats">Pot Limit Omaha 4 Card</div>
	
      <table class="fulltable"  cellpadding="0" cellspacing="0">
         <thead>
            <tr>
				<th>Stakes (Big Blind) </th>
				<th>GGR  </th>
				<th>NGR  </th>
				<th>Revenue generated through Bonus </th>
				<th>Unique users  </th>
				<th>Hands </th>
				<th>ARPU </th>
           </tr>
        </thead>
		@if(isset($potLimitOmaha4card) && !empty($potLimitOmaha4card))
		
		@foreach($potLimitOmaha4card as $key => $stack)
        <tr>
            <td >{{ $stack->BIG_BLIND }}</td>
            <td >{{ $stack->GROSS_RAKE }}</td>
            <td >{{ $stack->NET_RAKE }}</td>
            <td >{{ $stack->BONUS_REVENUE }}</td>
            <td >{{ $stack->PLAYING_USER }}</td>
            <td >{{ $stack->NUMBER_OF_HANDS}}</td>
            <td >{{ $stack->ARPU }}</td>
        </tr>
		@endforeach
        @endif 
        @if(isset($potLimitOmaha4CardhighMidLowCard) && !empty($potLimitOmaha4CardhighMidLowCard))
		
		@foreach($potLimitOmaha4CardhighMidLowCard as $key => $stack)
			@php
				if(isset($stack) && !empty($stack)){
					$BIG_BLIND_CASE= $stack->BIG_BLIND_CASE;
					$GROSS_RAKE 		= $stack->GROSS_RAKE;
					$NET_RAKE 	= $stack->NET_RAKE;
					$BONUS_REVENUE 		= $stack->BONUS_REVENUE;
					$PLAYING_USER 		= $stack->PLAYING_USER;
					$NUMBER_OF_HANDS 		= $stack->NUMBER_OF_HANDS;
					$ARPU 		= $stack->ARPU;
				}else{
					$BIG_BLIND_CASE= $key;
					$GROSS_RAKE 		= '0';
					$NET_RAKE 	= '0';
					$BONUS_REVENUE 		= '0';
					$PLAYING_USER 		= '0';
					$NUMBER_OF_HANDS 		= '0';
					$ARPU 		= '0';
				}
			@endphp
        <tr class="bgbb-1">
            <td >{{ $BIG_BLIND_CASE }}</td>
            <td >{{ $GROSS_RAKE }}</td>
            <td >{{ $NET_RAKE }}</td>
            <td >{{ $BONUS_REVENUE }}</td>
            <td >{{ $PLAYING_USER }}</td>
            <td >{{ $NUMBER_OF_HANDS }}</td>
            <td >{{ $ARPU }}</td>
        </tr>
		@endforeach
        @endif 
      </table>
  
   <div class="pagebreak"></div>
   
	<table><tr><td></td></tr></table>
	<table><tr><td></td></tr></table>
	<table><tr><td></td></tr></table>
	<div class="stablestats">5 Card Pot Limit Omaha</div>
   
      <table class="fulltable"  cellpadding="0" cellspacing="0">
         <thead>
            <tr>
               <th>Stakes (Big Blind)  </th>
               <th>GGR  </th>
               <th>NGR  </th>
               <th>Revenue generated through Bonus </th>
               <th>Unique users  </th>
               <th>Hands </th>
               <th>ARPU </th>
			</tr>
         </thead>
       @if(isset($cardPotLimit5OmahaByStacks) && !empty($cardPotLimit5OmahaByStacks))
		
		@foreach($cardPotLimit5OmahaByStacks as $key => $stack)
        <tr>
            <td >{{ $stack->STAKES }}</td>
            <td >{{ $stack->GROSS_RAKE }}</td>
            <td >{{ $stack->NET_RAKE }}</td>
            <td >{{ $stack->BONUS_REVENUE }}</td>
            <td >{{ $stack->FIVE_CARD_PLAYING_USER }}</td>
            <td >{{ $stack->NUMBER_OF_HANDS }}</td>
            <td >{{ $stack->ARPU }}</td>
        </tr>
		@endforeach
        @endif 
        @if(isset($cardPotLimit5OmahaByHighMidLow) && !empty($cardPotLimit5OmahaByHighMidLow))
		
		@foreach($cardPotLimit5OmahaByHighMidLow as $key => $stack)
			@php
				if(isset($stack) && !empty($stack)){
					$BIG_BLIND_CASE= $stack->BIG_BLIND_CASE;
					$GROSS_RAKE 		= $stack->GROSS_RAKE;
					$NET_RAKE 	= $stack->NET_RAKE;
					$BONUS_REVENUE 		= $stack->BONUS_REVENUE;
					$PLAYING_USER_OMAHA 		= $stack->PLAYING_USER_OMAHA;
					$NUMBER_OF_HANDS 		= $stack->NUMBER_OF_HANDS;
					$ARPU 		= $stack->ARPU;
				}else{
					$BIG_BLIND_CASE= $key;
					$GROSS_RAKE 		= '0';
					$NET_RAKE 	= '0';
					$BONUS_REVENUE 		= '0';
					$PLAYING_USER_OMAHA 		= '0';
					$NUMBER_OF_HANDS 		= '0';
					$ARPU 		= '0';
				}
			@endphp
        <tr class="bgbb-1">
            <td >{{ $BIG_BLIND_CASE }}</td>
            <td >{{ $GROSS_RAKE }}</td>
            <td >{{ $NET_RAKE }}</td>
            <td >{{ $BONUS_REVENUE }}</td>
            <td >{{ $PLAYING_USER_OMAHA }}</td>
            <td >{{ $NUMBER_OF_HANDS }}</td>
            <td >{{ $ARPU }}</td>
        </tr>
		@endforeach
        @endif 
      </table>
	<div class="pagebreak"></div>
	<table><tr><td></td></tr></table>
	<table><tr><td></td></tr></table>
	<table><tr><td></td></tr></table>
<div class="stablestats">OFC</div>
   
      <table class="fulltable"  cellpadding="0" cellspacing="0">
         <thead>
            <tr class="bgbb-1">
               <th>Stakes (Per point) </th>
               <th>GGR  </th>
               <th>NGR  </th>
               <th>Revenue generated through Bonus </th>
               <th>Unique users  </th>
                <th>Hands </th>
                 <th>ARPU </th>
           </tr>
         </thead>
		 
        @if(isset($ofcByStack) && !empty($ofcByStack))
		
		@foreach($ofcByStack as $key => $stack)
        <tr>
            <td >{{ $stack->POINT_VALUE }}</td>
            <td >{{ $stack->GROSS_RAKE }}</td>
            <td >{{ $stack->NET_RAKE }}</td>
            <td >{{ $stack->BONUS_REVENUE }}</td>
            <td >{{ $stack->PLAYING_USER }}</td>
            <td >{{ $stack->NUMBER_OF_HANDS }}</td>
            <td >{{ $stack->ARPU }}</td>
        </tr>
		@endforeach
        @endif 
        @if(isset($ofcHighMidLow) && !empty($ofcHighMidLow))
		
		@foreach($ofcHighMidLow as $key => $stack)
			@php
				if(isset($stack) && !empty($stack)){
					$BIG_BLIND_CASE= $stack->BIG_BLIND_CASE;
					$GROSS_RAKE 		= $stack->GROSS_RAKE;
					$NET_RAKE 	= $stack->NET_RAKE;
					$BONUS_REVENUE 		= $stack->BONUS_REVENUE;
					$PLAYING_USER 		= $stack->PLAYING_USER;
					$NUMBER_OF_HANDS 		= $stack->NUMBER_OF_HANDS;
					$ARPU 		= $stack->ARPU;
				}else{
					$BIG_BLIND_CASE= $key;
					$GROSS_RAKE 		= '0';
					$NET_RAKE 	= '0';
					$BONUS_REVENUE 		= '0';
					$PLAYING_USER 		= '0';
					$NUMBER_OF_HANDS 		= '0';
					$ARPU 		= '0';
				}
			@endphp
        <tr class="bgbb-1">
            <td class="bgbb-1">{{ $BIG_BLIND_CASE }}</td>
            <td class="bgbb-1">{{ $GROSS_RAKE }}</td>
            <td class="bgbb-1">{{ $NET_RAKE }}</td>
            <td class="bgbb-1">{{ $BONUS_REVENUE }}</td>
            <td class="bgbb-1">{{ $PLAYING_USER }}</td>
            <td class="bgbb-1">{{ $NUMBER_OF_HANDS }}</td>
            <td class="bgbb-1">{{ $ARPU }}</td>
        </tr>
		@endforeach
        @endif 
		</table>
 </table>
</div>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,600"  media="all" type="text/css" >
</body>
</html>
