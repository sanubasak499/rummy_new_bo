<html><body><table width="100%" callpadding="0" cellspacing="0" border="0" class="toptablee">
	<tr>
		<td ><img src="http://www.pokerbaazi.com/images/logo.png" width="200" alt="logo" /></td>
		
		<td align="cellspacing"><h2><u>Daily Stats Report</u></h2></td>
		<td align="right"><h3>{!! $yesterday_date !!} 00:00:00 - 23:59:59</h3></td>
	</tr>
</table>
	<table  width="100%" cellpadding="0" cellspacing="0" border="0" class="table">
		<thead>
			<tr>
				<th align="left" >Stakes</th>
				<th align="left" >T.Hold'em Gr. Rev.</th>
				<th align="left" >T. Hold'em Nt. Rev.</th>
				<th align="left" >4 Card Gr. Rev.</th>
				<th align="left" >4 Card Nt. Rev.</th>
				<th align="left" >5 Card Gr. Rev.</th>
				<th align="left" >5 Card Nt. Rev.</th>
				<th align="left" >OFC Gr. Rev.</th>
				<th align="left" >OFC Nt. Rev.</th>
			</tr>
		</thead>
		<tbody >
			@forelse($data as $key => $search_data)
				<tr>
					<td >{{ $search_data->BIG_BLIND}}</td>
					<td>{{ $search_data->Texas_Holdem_Gross_Revenue }}</td>
					<td>{{ $search_data->Taxs_Holdem_Net_Revenue }}</td>
					<td>{{ $search_data->Omaha_Gross_Revenue }}</td>
					<td>{{ $search_data->Omaha_Net_Revenue }}</td>
					<td>{{ $search_data->{'5card_Gross_Revenue'} }}</td>
					<td>{{ $search_data->{'5card_Net_Revenue'} }}</td>
					<td>{{ $search_data->Ofc_Gross_Revenue }}</td>
					<td>{{ $search_data->Ofc_Net_Revenue }}</td>
					</tr>
					@empty
					<tr>
					<td colspan="9">data not found</td>

				</tr>
			@endforelse
		</tbody>
	</table>
	@foreach($countdata as $key => $reportdatacount)
		<table class="tablecontent"  width="100%" callpadding="0" cellspacing="0" border="0">
			<tr><td valign="top" width="33.33%">
			<h5 class="font-15 mt-0">Revenue Report</h5>
			<table width="100%" cellspacing="0" cellpadding="0" border="0" >
				<tr>
					<td class="py-1 text-black"><strong>Total Gross Revenue</strong></td>
					<td class="p-1 text-black">:</td>
					<td class="p-1 text-black-50">{{ $reportdatacount->U_GROSS_REVENUE ?? '' }}</td>
				</tr>
				<tr>
					<td class="py-1 text-black"><strong>Total Net Revenue</strong></td>
					<td class="p-1 text-black">:</td>
					<td class="p-1 text-black-50">{{ $reportdatacount->U_NET_REVENUE ?? ''}}</td>
				</tr>
				<tr>
					<td class="py-1 text-black"><strong>Total Successful Deposits</strong></td>
					<td class="p-1 text-black">:</td>
					<td class="p-1 text-black-50">{{ $reportdatacount->U_TOTAL_SUCCESSFUL_DEPOSIT ?? ''}}</td>
				</tr>
			</table>
			</td><td valign="top" width="33.33%">
			<h5 class="font-15 mt-0">Total Withdrawals</h5>
			<table width="100%" cellspacing="0" cellpadding="0" border="0" >
				<tr>
					<td class="py-1 text-black"><strong>Pending</strong></td>
					<td class="p-1 text-black">:</td>
					<td class="p-1 text-black-50">{{ $reportdatacount->U_WITHDRAW_PENDING_AMOUNT ?? ''}}</td>
				</tr>
				<tr>
					<td class="py-1 text-black"><strong>Approved</strong></td>
					<td class="p-1 text-black">:</td>
					<td class="p-1 text-black-50">{{ $reportdatacount->U_WITHDRAW_SUCCESSFUL_AMOUNT ?? ''}}</td>
				</tr>
				<tr>
					<td class="py-1 text-black"><strong>Total </strong></td>
					<td class="p-1 text-black">:</td>
					<td class="p-1 text-black-50">{{ $reportdatacount->U_WITHDRAW_TOTAL_AMOUNT ?? ''}}</td>
				</tr>
			</table>
			</td><td valign="top" width="33.33%">
			<h5 class="font-15 mt-0" >First Time Deposit Report</h5>
			<table width="100%" cellspacing="0" cellpadding="0" border="0" >
				<tr>
					<td class="py-1 text-black"><strong>Total First Time Depositors</strong></td>
					<td class="p-1 text-black">:</td>
					<td class="p-1 text-black-50">{{ $reportdatacount->U_TOTAL_FIRST_DEPOSIT }}</td>
				</tr>
				<tr>
					<td class="py-1 text-black"><strong>Total Registration</strong></td>
					<td class="p-1 text-black">:</td>
					<td class="p-1 text-black-50">{{ $reportdatacount->U_NEW_REGISTRATION ?? ''}}</td>
				</tr>                            
				<tr>
					<td class="py-1 text-black"></td>
					<td class="p-1 text-black">:</td>
					<td class="p-1 text-black-50"></td>
				</tr>  
			</table>
		</td>
		</table>
	@endforeach

<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">

  <style type="text/css">*{ margin:0; padding: 0 ;font-family: 'Roboto', sans-serif;
}
         h5{font-family: 'Roboto', sans-serif;}
          .toptablee{ background-color: #eee; border: 2px #ccc solid; border-bottom: 0}
          .toptablee td{ padding:8px ; }
          table{font-family: 'Roboto', sans-serif;
}
          table h2,table h3{ font-size: 18px; }
.table { clear: both; width: 100%;    margin-bottom: 6px !important; max-width: none !important; border-collapse: separate !important;  border-spacing: 0; border:2px #5e3792  solid; margin-bottom: 25px;  }
.table thead th { vertical-align: top; background-color: #5e3792 ; color: #fff}
.table td, .table th { white-space:nowarp; padding: 10px ; padding-right: 5px; font-size: 0.875rem; text-align: left; border:0 !important;    line-height: 1.2;font-family:Roboto,sans-serif; border: 0 }
/*.table td:nth-of-type(1){ background-color:#865fc9; color: #fff; position: relative; z-index: 99 }*/
 .table tbody tr:nth-of-type(2n+1) { background-color:     #f7f8f9;} 
.tablecontent {font-family: 'Roboto', sans-serif;
clear: both; margin-top: 6px !important; width: 100%;    margin-bottom: 6px !important; max-width: none !important; border-collapse: separate !important;  border-spacing: 0; margin: 0 -5px  }
.tablecontent td{ padding: 10px 5px; text-align: center; font-size: 0.875rem; text-align: left;    line-height: 1.2;font-family:Roboto,sans-serif;color: #6c757d;}
.font-15{ font-size:16px;font-family:  Verdana, Geneva, Tahoma, sans-serif; font-weight:600; margin: 0 0 15px 0; color: #333  }
.tablecontent table td{ padding:13px 10px; border-bottom: 1px #ccc solid !important; color: #000; border:0;  }

.tablecontent  table{ background-color: #eee; text-align: left; font-family: 'Roboto', sans-serif;
}
.tablecontent  .font-15{background-color: #5e3792; color: #fff ; margin: 0; padding: 10px;font-family: 'Roboto', sans-serif;
}

</style>
</body>
</html>