@extends('bo.layouts.master')
@section('title', "Ticket Ledgers| PB BO")
@section('pre_css')
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .searchselect .select2 {
        width: 100% !important
    }

    .searchselect .select2-container--default .select2-selection--single {
        border: 1px #ced4da solid;
        height: 36px;
        line-height: 36px;
    }

    .searchselect .select2-container--default .select2-selection__rendered {
        line-height: 36px;
    }

    .multiplecustom .customselect {
        border: 0 !important;
    }
</style>
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{ url('contest/ticket-ledgers') }}"> Tournaments</a></li>
                            <li class="breadcrumb-item active">Ticket Ledger</li>
                        </ol>
                    </div>
                    <h4 class="page-title">
                        Ticket Ledger
                    </h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form id="userSearchForm" action="{{ route('contest.ticket-ledgers.TicketLedgerSearch') }}{{ !empty($params) ? empty($params['page']) ? '' : "?page=".$params['page'] : '' }}" method="post">
                            @csrf
                            <div class="form-row align-items-center">
                                <div class="form-group col-md-2">
                                    <label class="mb-1">Search By: </label>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="radio radio-info form-check-inline">
                                        <input data-default-checked="" type="radio" id="TOURNAMENT_NAME" value="TOURNAMENT_NAME" name="search_by" checked="" {{ !empty($params) ? $params['search_by'] == "TOURNAMENT_NAME" ? "checked" : '' : "" }}>
                                        <label for="TOURNAMENT_NAME"> Tournament Name </label>
                                    </div>
                                    <div class="radio radio-info form-check-inline">
                                        <input type="radio" id="TOURNAMENT_ID" value="TOURNAMENT_ID" name="search_by" {{ !empty($params) ? $params['search_by'] == "TOURNAMENT_ID" ? "checked" : '' : "" }}>
                                        <label for="TOURNAMENT_ID"> Tournament Id</label>
                                    </div>
                                    <!-- <div class="radio radio-info form-check-inline">
                                        <input type="radio" id="CONTACT" value="CONTACT" name="search_by" {{ !empty($params) ? $params['search_by'] == "CONTACT" ? "checked" : '' : "" }}>
                                        <label for="CONTACT"> Contact No </label>
                                    </div> -->
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="sr-only" for="search_by_value">Search Input</label>
                                    <input type="text" class="form-control" name="search_by_value" id="search_by_value" placeholder="Search by Tournament name, Tournament Id" value="{{ !empty($params) ? $params['search_by_value']  : ''  }}">
                                </div>
                            </div>
                            <div class="row  align-items-center">
                               
                                <div class="form-group col-md-6 searchselect">
                                    <label>Source:</label>
                                    <select class="form-control" name="TICKET_SOURCE" data-toggle="select2">
                                        <option data-default-selected="" value="" selected="">Select</option>
                                        <option value="ticket_assign_from_bo" {{ !empty($params) ? $params['TICKET_SOURCE'] == 'ticket_assign_from_bo'  ? "selected" : ''  : ''  }}>ticket_assign_from_bo</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 searchselect">
                                    <label>Reason: </label>
                                    <select class="form-control" name="TICKET_REASON" id="TICKET_REASON" data-toggle="select2">
                                        <option data-default-selected="" value="" selected="">Select</option>
                                        @foreach($ActionReasonResult as $result)
                                        <option value="{{ $result->ACTIONS_REASON }}" {{ !empty($params) ? $params['TICKET_REASON'] == $result->ACTIONS_REASON  ? "selected" : ''  : ''  }}>{{ $result->ACTIONS_REASON}}</option>
                                        @endforeach
                                    </select>
                                </div>
                               
                            </div>
                            <div class="row  customDatePickerWrapper">
                                <div class="form-group col-md-4">
                                    <label>From: </label>
                                    <input name="START_DATE_TIME" id="date_from" type="text" class="form-control customDatePicker from flatpickr-input" placeholder="Select From Date" value="{{ !empty($params) ? $params['START_DATE_TIME']  : ''  }}" readonly="readonly">
                                </div>
                                <div class="form-group col-md-4">
                                    <label>To: </label>
                                    <input name="END_DATE_TIME" id="date_to" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="{{ !empty($params) ? $params['END_DATE_TIME']  : ''  }}" readonly="readonly">
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Date Range: </label>
                                    <select name="date_range" id="date_range" class="formatedDateRange customselect" data-plugin="customselect">
                                        <option data-default-selected="" value="" selected="">Select Date Range</option>
                                        <option value="1" {{ (1==old('date_range',$params['date_range'] ?? '')) ?'selected="selected"':''  }}>Today</option>
                                        <option value="2" {{ (2==old('date_range',$params['date_range'] ?? '')) ?'selected="selected"':''  }}>Yesterday</option>
                                        <option value="3" {{ (3==old('date_range',$params['date_range'] ?? '')) ?'selected="selected"':''  }}>This Week</option>
                                        <option value="4" {{ (4==old('date_range',$params['date_range'] ?? '')) ?'selected="selected"':''  }}>Last Week</option>
                                        <option value="5" {{ (5==old('date_range',$params['date_range'] ?? '')) ?'selected="selected"':''  }}>This Month</option>
                                        <option value="6" {{ (6==old('date_range',$params['date_range'] ?? '')) ?'selected="selected"':''  }}>Last Month</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                            <a href="{{ url('contest/ticket-ledgers') }}" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>

                        </form>
                    </div>
                    <!-- end card-body-->
                </div>
            </div>
            <!-- end col -->
        </div>
        @if(!empty($TicketLedgersResult))
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <div class="card-widgets">
                            <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>
                        </div>
                        <h5 class="card-title mb-0 text-white">Ticket Ledger</h5>
                    </div>
                    <div id="cardCollpase7" class="collapse show">
                        <div class="card-body">
                            <div class="row mb-2">
                                <div class="col-sm-9 mb-1">
                                </div>
                                <div class="col-sm-3 mb-1">
                                    <div class="text-sm-right">
                                        @if(!empty($TicketLedgersResult[0]))
                                        <a href="{{ route('contest.ticket-ledgers.exportExcel')."?START_DATE_TIME=".$params['START_DATE_TIME']."&END_DATE_TIME=".$params['END_DATE_TIME']."&search_by=".$params['search_by']."&search_by_value=".$params['search_by_value']."&TICKET_SOURCE=".$params['TICKET_SOURCE']."&TICKET_REASON=".$params['TICKET_REASON']}}"> <button type="button" class="btn btn-light mb-1"><i class="fas fa-file-excel" style="color: #34a853;"></i>Export Excel</a>
                                        @endif
                                    </div>
                                </div>
                                <!-- end col-->
                            </div>
                            <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tournament Name</th>
                                        <th>Username</th>
                                        <th>Date</th>
                                        <th>Source</th>
                                        <th>Reason </th>
                                        <!-- <th>Status</th> -->
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i=1; @endphp
                                    @foreach($TicketLedgersResult as $key => $result)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ $result->TOURNAMENT_NAME }}</td>
                                        <td>{{ $result->USERNAME }}</td>
                                        <td>{{ $result->CREATED }}</td>
                                        <td>{{ $result->TICKET_SOURCE }}</td>
                                        <td>{{ $result->TICKET_REASON }}</td>
                                        <!-- <td><span class="badge bg-soft-success text-success shadow-none">
                                                @if($result->TICKET_STATUS=='1')
                                                used
                                                @elseif($result->TICKET_STATUS=='2')
                                                unused
                                                @endif
                                            </span></td> -->
                                        <td>
                                            <a data-toggle="modal" data-target="#tournamentInfo{{ $result->TOURNAMENT_USER_TICKET_ID }}" class="action-icon font-18 tooltips"><i class="fa-eye fa"></i> <span class="tooltiptext">View</span></a>
                                        </td>
                                    </tr>
                                    @php $i++; @endphp
                                    <div class="modal fade" id="tournamentInfo{{ $result->TOURNAMENT_USER_TICKET_ID }}">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content bg-transparent shadow-none">
                                                <div style="background-color: #f5f5f5 !important; padding: 15px 15px 0 15px !important;">
                                                    <div class="card">
                                                        <div class="card-header bg-dark py-2 text-white">
                                                            <div class="card-widgets">
                                                                <a href="#" data-dismiss="modal">
                                                                    <i class="mdi mdi-close"></i>
                                                                </a>
                                                            </div>
                                                            <h5 class="card-title mb-0 text-white">Ticket Ledger</h5>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="row ">
                                                                <div class="col-sm-12 mb-1 "><strong>Tournament/Satty Name:</strong> {{ $result->TOURNAMENT_NAME }}</div>
                                                                <div class="col-sm-6 mb-1 "><strong>Username:</strong> {{ $result->USERNAME }} </div>
                                                                <div class="col-sm-6 mb-1"><strong>Date and Time :</strong>{{ $result->CREATED }} </div>
                                                                <div class="col-sm-6 mb-1"><strong>Source:</strong> {{ $result->TICKET_SOURCE }} </div>
                                                                <div class="col-sm-6 mb-1 "><strong>Reason :</strong>{{ $result->ACTIONS_REASON  }}</div>
                                                                <!-- <div class="col-sm-6 mb-1 "><strong>Status:</strong>
                                                                    @if($result->TICKET_STATUS=='1')
                                                                    used
                                                                    @elseif($result->TICKET_STATUS=='2')
                                                                    unused
                                                                    @endif
                                                                </div> -->
                                                                <!-- <div class="col-sm-6 mb-1 "><strong>Ticket Assigned by:</strong> ---</div> -->
                                                                <div class="col-sm-6 mb-1 "><strong>Ticket Assigned on:</strong> {{ $result->UPDATED }}
                                                                    <div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                </tbody>
                            </table>
                            <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" class="mt-2">
                                @if(!empty($TicketLedgersResult))
                                <div>More Records</div>
                                <div>
                                    {{ $TicketLedgersResult->render("pagination::customPaginationView") }}
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- end card body-->
                </div>
                <!-- end card -->
            </div>
            <!-- end col-->
        </div>
        @endif
    </div>
</div>

@endsection
@section('js')
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
@endsection