@extends('bo.layouts.master')

@section('title', "Kyc | PB BO")

@section('pre_css')
    <link href="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/libs/dropify/dropify.min.css') }}" rel="stylesheet" type="text/css"/>

@endsection

@section('post_css')
    <style>
        .ms-container {
            max-width: 650px;
        }

        .dropdown.bootstrap-select .inner {
            overflow: auto !important
        }

        .dropdown.bootstrap-select.show .dropdown-menu.show {
            min-width: 100% !important;
            transform: translate(0) !important;
            height: 200px;
        }

        .has-error input {
            border: 1px #f00 solid
        }

        .has-error span.error {
            color: #f00;
            display: block
        }

        span.error {
            color: #f00;
            display: none;
            width: 100%
        }

    </style>
    <style type="text/css">
        .dropify-wrapper {
            overflow: visible !important;
        }

        .dropify-wrapper label.error {
            position: absolute;
            left: 0;
            top: 101%;
        }

        .formtext {
            font-size: 11px;
            font-style: italic;
            color: #999198;
            padding: 6px 0 0 0;
            margin: 0;
            line-height: 13px;
        }
    </style>
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item "> KYC</li>
                                <li class="breadcrumb-item active"> Add</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Add KYC</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-lg-8">
                                    <div
                                        class="alert alert-info alert-dismissible bg-info text-white border-0 fade show"
                                        role="alert" id="partial_match">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        Partial Match
                                    </div>
                                    <div
                                        class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                                        role="alert" id="full_matched">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        Full Matched
                                    </div>
                                    <div
                                        class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                        role="alert" id="no_match">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        No match
                                    </div>
                                    <div
                                        class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                        role="alert" id="status_500">
                                    </div>
                                    <div
                                        class="alert alert-success alert-dismissible bg-success text-white border-0 fade show"
                                        role="alert" id="status_2001">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="text-lg-right mt-3 mt-lg-0"><a href="{{ url('helpdesk/kyc')}}"
                                                                               class="btn btn-success waves-effect waves-light"><i
                                                class="fa fa-bars mr-1"></i> Manage KYC</a></div>
                                </div>
                            </div>
                            <form id="userSearchForm" action="#" name="myForm" method="post"
                                  enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Doc Type: </label>
                                            <select name="doc_type" class="customselect addkycpage"
                                                    data-plugin="customselect">
                                                <option value="0" data-default-selected="" value="" selected="">Select
                                                    Doc Type
                                                </option>
                                                <option
                                                    value="1" {{ isset($kycDetail->DOCUMENT_TYPE) ? $kycDetail->DOCUMENT_TYPE == 1 ? "selected" : ''  : ''  }}>
                                                    Address
                                                    Proof
                                                </option>
                                                <option
                                                    value="2" {{ isset($kycDetail->DOCUMENT_TYPE) ? $kycDetail->DOCUMENT_TYPE == 2 ? "selected" : ''  : ''  }}>
                                                    PAN
                                                </option>
                                                <option
                                                    value="3" {{ isset($bankDetails) ?  "selected" : ''  }}>
                                                    Bank Details
                                                </option>
                                                <option
                                                    value="4" {{ isset($kycDetail->DOCUMENT_TYPE) ? $kycDetail->DOCUMENT_TYPE == 4 ? "selected" : ''  : ''  }}>
                                                    Other
                                                </option>
                                            </select>
                                            <span class="text-danger" id="error_doctype"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 aaadhaardlvoteraddkyc">
                                        <div class="form-group">
                                            <label>Doc Sub Type: </label>
                                            <select name="doc_sub_type" class="customselect"
                                                    data-plugin="customselect">
                                                <option value="0" data-default-selected="" value="" selected="">Select
                                                    Sub Doc Type
                                                </option>
                                                <option
                                                    value="1" {{ isset($kycDetail->DOCUMENT_SUB_TYPE) ? $kycDetail->DOCUMENT_SUB_TYPE == 1 ? "selected" : ''  : ''  }}>
                                                    AADHAAR
                                                </option>
                                                <option
                                                    value="2" {{ isset($kycDetail->DOCUMENT_SUB_TYPE) ? $kycDetail->DOCUMENT_SUB_TYPE == 2 ? "selected" : ''  : ''  }}>
                                                    DL
                                                </option>
                                                <option
                                                    value="3" {{ isset($kycDetail->DOCUMENT_SUB_TYPE) ? $kycDetail->DOCUMENT_SUB_TYPE == 3 ? "selected" : ''  : ''  }}>
                                                    VOTER_ID
                                                </option>
                                            </select>
                                            <span class="text-danger" id="error_subtype"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label>Username
                                                : </label>
                                            <input type="text" list="browsers" name="userName" id="autocomplete-ajax"
                                                   class="form-control" style=" z-index: 2; background: transparent;"
                                                   value="{{isset($user->USERNAME)?$user->USERNAME:''}}"/>
                                            <span class="text-danger" id="error_user_name"></span>
                                            <datalist id="browsers">
                                            </datalist>

                                            <input type="text" name="country" id="autocomplete-ajax-x"
                                                   value="{{isset($user->COUNTRY)?$user->COUNTRY:''}}"
                                                   disabled="disabled" class="form-control" style="display: none;"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="aaadhaardlvoteraddkyc">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Upload Front Image:<span class="text-danger">* </span> : </label>

                                            @if(isset($kycDetail->DOCUMENT_FRONT_URL) && $kycDetail->DOCUMENT_TYPE==1)
                                                <input type="text" class="dropify " name="front_image"
                                                       data-default-file="{{asset(getImageUrlFromName($kycDetail->DOCUMENT_FRONT_URL))}}"
                                                       value="{{$kycDetail->DOCUMENT_FRONT_URL}}"/>
                                                <p class="text-muted text-center mt-2 mb-0">Front image</p>
                                                <span class="text-danger" id="error_front_image"></span>
                                            @else
                                                <input type="file" class="dropify " name="front_image"
                                                       onclick="printFile(this)"
                                                       data-default-file=""/>
                                                <p class="text-muted text-center mt-2 mb-0">Front image</p>
                                                <span class="text-danger" id="error_front_image"></span>
                                            @endif
                                        </div>
                                        <div class="col-md-4">
                                            <label>Upload Back Image<span class="text-danger">* </span> : </label>
                                            @if(isset($kycDetail->DOCUMENT_BACK_URL))
                                                <input type="text" class="dropify " name="back_image"
                                                       data-default-file="{{asset(getImageUrlFromName($kycDetail->DOCUMENT_BACK_URL))}}"
                                                       value="{{$kycDetail->DOCUMENT_BACK_URL}}"/>
                                            @else
                                                <input type="file" class="dropify " name="back_image"
                                                       onclick="printFile(this)"
                                                       data-default-file=""/>
                                                <span class="text-danger" id="error_front_image"></span>
                                            @endif
                                            <p class="text-muted text-center mt-2 mb-0">Back image</p>
                                            <span class="text-danger" id="error_back_image"></span>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group mb-3">
                                                <label>Doc Number<span class="text-danger">* </span> : </label>
                                                <input type="text" class="form-control" name="doc_number" id="ref_no"
                                                       placeholder=""
                                                       value="{{isset($kycDetail->DOCUMENT_NUMBER)?$kycDetail->DOCUMENT_NUMBER:''}}">
                                                <span class="text-danger" id="error_docnumber"></span>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label>First Name
                                                    : </label>
                                                <input type="text" class="form-control disabled" name="first_name"
                                                       id="first_name" placeholder="" readonly
                                                       value="{{isset($user->FIRSTNAME)?$user->FIRSTNAME:''}}">
                                                <span class="text-danger" id="error_first_name"></span>

                                            </div>
                                            <div class="form-group ">
                                                <label>Last Name
                                                    : </label>
                                                <input type="text" class="form-control disabled" name="last_name"
                                                       id="last_name" placeholder=""
                                                       value="{{isset($user->LASTNAME)?$user->LASTNAME:''}}" readonly>
                                                <span class="text-danger" id="error_last_name"></span>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label>DOB Address
                                                    : </label>
                                                <input type="text" class="form-control disabled" name="dob" id="DOB"
                                                       placeholder=""
                                                       value="{{isset($user->DATE_OF_BIRTH)?$user->DATE_OF_BIRTH:''}}"
                                                       readonly>
                                                <span class="text-danger" id="error_DOB"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label>City
                                                    : </label>
                                                <input type="text" class="form-control disabled" name="city" id="city"
                                                       placeholder="" value="{{isset($user->CITY)?$user->CITY:''}}"
                                                       readonly>
                                                <span class="text-danger" id="error_city"></span>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label>State
                                                    : </label>
                                                <input type="text" class="form-control  disabled" name="state"
                                                       id="state" placeholder=""
                                                       value="{{isset($user->STATE)?$user->STATE:''}}" readonly>
                                                <span class="text-danger" id="error_state"></span>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label>Country
                                                    : </label>
                                                <input type="text" class="form-control disabled" name="country"
                                                       id="country" placeholder=""
                                                       value="{{isset($user->COUNTRY)?$user->COUNTRY:''}}" readonly>
                                                <span class="text-danger" id="error_country"></span>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label>Pin Code
                                                    : </label>
                                                <input type="text" class="form-control disabled" name="pin_code"
                                                       id="pin_code" placeholder=""
                                                       value="{{isset($user->PINCODE)?$user->PINCODE:''}}" readonly>
                                                <span class="text-danger" id="error_pincode"></span>

                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="enableInput">
                                                <label class="custom-control-label" for="enableInput">Edit
                                                    Manually</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pancardsaddkyc" style="display: none;">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label>Pan Number
                                                    : </label>
                                                <input type="text" class="form-control" name="pan_number"
                                                       id="pan_number"
                                                       placeholder=""
                                                       value="{{isset($kycDetail->DOCUMENT_TYPE)?$kycDetail->DOCUMENT_TYPE==2?$kycDetail->DOCUMENT_NUMBER:'':''}}">
                                                <span class="text-danger" id="error_pan_number"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Upload Pan Image:<span class="text-danger">* </span> : </label>
                                            @if(isset($kycDetail->DOCUMENT_FRONT_URL) && $kycDetail->DOCUMENT_TYPE==2)
                                                <input type="text" class="dropify " name="pan_image"
                                                       data-default-file="{{asset(getImageUrlFromName($kycDetail->DOCUMENT_FRONT_URL))}}"
                                                       value="{{$kycDetail->DOCUMENT_FRONT_URL}}"/>
                                            @else
                                                <input type="file" class="dropify " name="pan_image"
                                                       onclick="printFile(this)"
                                                       data-default-file=""/>
                                                <span class="text-danger" id="error_front_image"></span>
                                            @endif

                                            <p class="text-muted text-center mt-2 mb-0">Reward image</p>
                                            <span class="text-danger" id="error_pan_image"></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="bankdetailaddkyc" style="display: none;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label>Account No
                                                    : </label>
                                                <input type="text" class="form-control" name="account_number"
                                                       id="ref_no" placeholder=""
                                                       value="{{isset($bankDetails->ACCOUNT_NUMBER)?$bankDetails->ACCOUNT_NUMBER:''}}">
                                                <span class="text-danger" id="error_acc"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <label>IFSC Code
                                                    : </label>
                                                <input type="text" class="form-control" name="ifsc_code" id="ref_no"
                                                       placeholder=""
                                                       value="{{isset($bankDetails->IFSC_CODE)?$bankDetails->IFSC_CODE:''}}">
                                                <span class="text-danger" id="error_ifsc"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="Othereaddkyc" style="display: none;">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label>Upload Other Image:<span class="text-danger">* </span> : </label>
                                                @if(isset($kycDetail->DOCUMENT_FRONT_URL) && $kycDetail->DOCUMENT_TYPE==4)
                                                    <input type="text" class="dropify " name="support-image"
                                                           data-default-file="{{asset(getImageUrlFromName($kycDetail->DOCUMENT_FRONT_URL))}}"
                                                           value="{{$kycDetail->DOCUMENT_FRONT_URL}}"/>
                                                @else
                                                    <input type="file" class="dropify " name="support-image"
                                                           onclick="printFile(this)"
                                                           data-default-file=""/>
                                                    <span class="text-danger" id="error_front_image"></span>
                                                @endif
                                                <p class="text-muted text-center mt-2 mb-0">Other image</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @isset($kycDetail)
                                    <input value="{{$kycDetail->USER_KYC_ID}}" name="kyc_id" hidden>
                                @endisset

                                @isset($bankDetails)
                                    <input value="{{$bankDetails->USER_ACCOUNT_ID}}" name="account_id" hidden>
                                @endisset

                                <button type="button" id="btnSubmit" onclick="ajaxForm(event, 1)"
                                        class="btn btn-success waves-effect waves-light  mr-1">
                                    <i class="fa fa-save mr-1"></i> Approve
                                </button>
                                <button class="btn btn-success waves-effect waves-light loading-btn"
                                        id="approve-loading" type="button">
                                    <span class="spinner-border spinner-border-sm mr-1" role="status"
                                          aria-hidden="true"></span>
                                    Loading...
                                </button>

                                @if(isset($kycDetail) || isset($bankDetails))

                                @else
                                    <button type="button" id="fetch_details" onclick="ajaxForm(event, 2)"
                                            class="btn btn-warning waves-effect waves-light mr-1 OthereaddkycBtn">
                                        <i class="far fa-edit mr-1"></i> Fetch Details
                                    </button>
                                @endif


                                <button class="btn btn-warning waves-effect waves-light loading-btn" id="fetch-loading"
                                        type="button">
                                    <span class="spinner-border spinner-border-sm mr-1" role="status"
                                          aria-hidden="true"></span>
                                    Loading...
                                </button>
                                <button type="button" class="btn btn-danger waves-effect waves-light" id="reject-btn"
                                        onclick="ajaxForm(event, 3)">
                                    <i class="fas fa-eraser mr-1"></i> Reject
                                </button>
                                <button class="btn btn-danger waves-effect waves-light loading-btn" id="reject-loading"
                                        type="button">
                                    <span class="spinner-border spinner-border-sm mr-1" role="status"
                                          aria-hidden="true"></span>
                                    Loading...
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        addKycPage = () => {
            let kycpage = $(".addkycpage").val();
            $(".aaadhaardlvoteraddkyc").css('display', (kycpage == '1') ? 'block' : 'none');
            $(".pancardsaddkyc").css('display', (kycpage == '2') ? 'block' : 'none');
            $(".bankdetailaddkyc").css('display', (kycpage == '3') ? 'block' : 'none');
            $(".Othereaddkyc").css('display', (kycpage == '4') ? 'block' : 'none');
            $(".OthereaddkycBtn").css('display', (kycpage == '4') ? 'none' : 'inline-block');
        }
        $('.addkycpage').on('change', function () {
            addKycPage();
        });

        $(document).ready(() => {
            addKycPage();
        });

    </script>
    <script type="text/javascript">
        $('.disabledd').attr('text', true);
        $('#customChseck11').on('click', function (e) {
            e.preventDefault();
            $('.disabledd').attr('disabled', true);
        })
    </script>

    <script>
        $('#autocomplete-ajax').keypress(() => {
            $getVal = $('#autocomplete-ajax').val();
            $.get("{{url('/helpdesk/kyc/search-username')}}", {searchQuery: $getVal}, (result) => {
                $('#browsers').empty();
                $.each(result, function (index, value) {
                    $('#browsers').append($('<option />', {
                        value: value.USERNAME,
                        id: index
                    }));
                });
                // $('#autocomplete-ajax').on('input', function () {
                //     var opt = $('option[value="' + $(this).val() + '"]').attr('id');
                //     var get_data = result[opt];
                //     $('#first_name').val(get_data.FIRSTNAME);
                //     $('#last_name').val(get_data.LASTNAME);
                //     $('#DOB').val(get_data.DATE_OF_BIRTH);
                //     $('#city').val(get_data.CITY);
                //     $('#state').val(get_data.STATE);
                //     $('#country').val(get_data.COUNTRY);
                //     $('#pin_code').val(get_data.PINCODE);
                // });
            });
        });
        $('#enableInput').click(() => {
            if ($('#enableInput').is(":checked")) {
                $('.disabled').removeAttr('readonly');
            } else {
                $('.disabled').prop('readonly', true);
            }
        });
    </script>
    <script>
        $('.loading-btn').hide();
        $('#no_match').hide();
        $('#full_matched').hide();
        $('#partial_match').hide();
        $("#status_500").hide();
        $("#status_2001").hide();

        function ajaxForm(e, callUrl) {
            e.preventDefault();
            var route;
            var arr = document.getElementById('userSearchForm');
            var data = new FormData(arr);
            if (callUrl == 1) {
                route = "{{url('/helpdesk/kyc/approve')}}";
                loadStart = () => {
                    $('#btnSubmit').hide();
                    $('#approve-loading').show();
                }
                loadStop = () => {
                    $('#approve-loading').hide();
                    $('#btnSubmit').show();
                }
            } else if (callUrl == 2) {
                route = "{{url('/helpdesk/kyc/fetch-details')}}";
                loadStart = () => {
                    $('#fetch_details').hide();
                    $('#fetch-loading').show();
                    $('#no_match').hide();
                    $('#full_matched').hide();
                    $('#partial_match').hide();
                }
                loadStop = () => {
                    $('#fetch-loading').hide();
                    $('#fetch_details').show();

                }
            } else if (callUrl == 3) {
                route = "{{url('/helpdesk/kyc/reject')}}";
                loadStart = () => {
                    $('#reject-btn').hide();
                    $('#reject-loading').show();
                }
                loadStop = () => {
                    $('#reject-loading').hide();
                    $('#reject-btn').show();
                }
            }
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: route,
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: loadStart,
                success: function (data) {
                    // console.log("SUCCESS : ", data.data);
                    var get_data = data.data;
                    $('#first_name').val(get_data.firstName);
                    $('#last_name').val(get_data.lastName);
                    $('#DOB').val(get_data.dob);
                    $('#city').val(get_data.city);
                    $('#state').val(get_data.state);
                    $('#country').val(get_data.country);
                    $('#pin_code').val(get_data.pinCode);
                    $('#fetch_details').prop('disabled', true);
                    var msg = data.data.message;
                    $('#userSearchForm .text-danger').empty();
                    if (msg === 'No Match') {
                        $('#no_match').show();
                    }
                    if (msg === 'Full Matched') {
                        $('#full_matched').show();
                    }
                    if (msg === 'Partial Match') {
                        $('#partial_match').show();
                    }
                    if (data.code === 2001) {
                        console.log(msg)
                        $("#status_2001").show();
                        $('#status_2001').empty().html(msg);
                            window.location.href = "{{url('helpdesk/kyc')}}";
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var error = jqXHR.responseText;
                    var errors = JSON.parse(error).errors;
                    var msg = JSON.parse(error).message
                    // console.log(JSON.parse(error).message);
                    if (jqXHR.status == 500) {
                        $("#status_500").show();
                        $('#status_500').empty().html(msg);
                    } else {
                        $('#error_user_name').empty().html(errors.userName);
                        $('#error_doctype').empty().html(errors.doc_type);
                        $('#error_subtype').empty().html(errors.doc_sub_type);
                        $('#error_docnumber').empty().html(errors.doc_number);
                        $('#error_pincode').empty().html(errors.pin_code);
                        $('#error_state').empty().html(errors.state);
                        $('#error_first_name').empty().html(errors.first_name);
                        $('#error_last_name').empty().html(errors.last_name);
                        $('#error_DOB').empty().html(errors.dob);
                        $('#error_country').empty().html(errors.country);
                        $('#error_city').empty().html(errors.city);
                        $('#error_front_image').empty().html(errors.front_image);
                        $('#error_back_image').empty().html(errors.back_image);
                        $('#error_pan_number').empty().html(errors.pan_number);
                        $('#error_pan_image').empty().html(errors.pan_image);
                        $('#error_acc').empty().html(errors.account_number);
                        $('#error_ifsc').empty().html(errors.ifsc_code);
                    }
                },
                complete: loadStop,
            });
        }
    </script>
    <!-- File Upload js -->
    <script src="{{asset('assets/libs/dropzone/dropzone.min.js')}}"></script>
    <script src="{{asset('assets/libs/dropify/dropify.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/form-fileuploads.init.js')}}"></script>
    <script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

@endsection
