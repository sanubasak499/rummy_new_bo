@extends('bo.layouts.master')

@section('title', "Kyc | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('post_css')
    <style>
        .ms-container {
            max-width: 650px;
        }

        .dropdown.bootstrap-select .inner {
            overflow: auto !important
        }

        .dropdown.bootstrap-select.show .dropdown-menu.show {
            min-width: 100% !important;
            transform: translate(0) !important;
            height: 200px;
        }
    </style>
@endsection

@section('content')
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="index.php">Helpdesk</a></li>
                     <li class="breadcrumb-item active"> KYC</li>
                  </ol>
               </div>
               <h4 class="page-title">
                  KYC
               </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <div class="row mb-3">
                     <div class="col-sm-4">
                     </div>
                     <div class="col-sm-8">
                        <div class="text-sm-right">
                           <a href="{{ url('helpdesk/kyc/addkyc')}}" class="btn btn-success waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add KYC</a>
                        </div>
                     </div>
                  </div>
                  <form id="userSearchForm" action="{{ route('helpdesk.kyc.search') }}{{ !empty($params) ? empty($params['page']) ? '' : "?page=".$params['page'] : '' }}" method="post" >
				  @csrf
                     <div class="form-row align-items-center">

                        <div class="form-group col-md-2">
                           <label class="mb-1">Search By: </label>
                        </div>
                        <div class="form-group col-md-5">
                           <div class="radio radio-info form-check-inline">
                              <input data-default-checked="" type="radio" id="search_by_username" value="username" {{ !empty($params) ? $params['search_by'] == 'search_by' ? "checked" : ''  : ''  }} name="search_by" checked="">
                              <label for="search_by_username"> Username </label>
                           </div>

                           <div class="radio radio-info form-check-inline">
                              <input type="radio" id="search_by_email" value="email" {{ !empty($params) ? $params['search_by'] == 'email' ? "checked" : ''  : ''  }}  name="search_by">
                              <label for="search_by_email"> Email </label>
                           </div>
                           <div class="radio radio-info form-check-inline">
                              <input type="radio" id="search_by_contact_no"  value="contact_no" {{ !empty($params) ? $params['search_by'] == 'contact_no' ? "checked" : ''  : ''  }} name="search_by">
                              <label for="search_by_contact_no"> Contact No </label>
                           </div>
                        </div>
                        <div class="form-group col-md-5">
                           <label class="sr-only" for="search_by_value">Search Input</label>
                           <input type="text" class="form-control" name="search_by_value" id="search_by_value" placeholder="Search by Username, Email & Contact No" value="{{ !empty($params) ? $params['search_by_value']  : ''  }}">
                        </div>
                     </div>
                     <div class="row  align-items-center">
                        <div class="form-group col-md-4">
                           <label>Document Type: </label>
						   <select name="doc_type" class="form-control selectpicker"  data-style="btn-light" >
							 <option data-default-selected="" value="" selected="">Select Document Type</option>
								<option value ="1"{{ !empty($params['doc_type']) ? $params['doc_type'] == 1 ? "selected" : ''  : ''  }}>Address</option>

								<option value ="2"{{ !empty($params['doc_types']) ? $params['doc_type'] == 2 ? "selected" : ''  : ''  }}>PAN</option>
								<option value ="3"{{ !empty($params['doc_type']) ? $params['doc_type'] == 3 ? "selected" : ''  : ''  }}>Support docs</option>
								<option value ="4"{{ !empty($params['doc_type']) ? $params['doc_type'] == 4 ? "selected" : ''  : ''  }}>Bank Details</option>
                           </select>
                        </div>
                        <div class="form-group col-md-4">
                           <label>Document No: </label>
                           <input type="text" class="form-control" name="doc_num" id="doc_num" placeholder="Document No" value="">
                        </div>
                        <div class="form-group col-md-4">
                           <label>Status: </label>
                           <select name="status" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="" selected="">Select</option>
								<option value="1"{{ !empty($params['status']) ? $params['status'] == 1  ? "selected" : ''  : ''  }}>Success</option>
								<option value="2"{{ !empty($params['status']) ? $params['status'] == 2  ? "selected" : ''  : ''  }}>Pending</option>
								<option value="3"{{ !empty($params['status']) ? $params['status'] == 3  ? "selected" : ''  : ''  }}>Rejected</option>
								<option value="0"{{ !empty($params['status']) ? $params['status'] == 0  ? "selected" : ''  : ''  }}>Failed</option>
                           </select>
                        </div>
                     </div>
                     <div class="row  customDatePickerWrapper">
                        <div class="form-group col-md-4">
                           <label>From: </label>
                           <input name="date_from" id="date_from" type="text" class="form-control customDatePicker from flatpickr-input" placeholder="Select From Date" value="{{ !empty($params) ? $params['date_from'] : '' }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-4">
                           <label>To: </label>
                           <input name="date_to" id="date_to" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="{{ !empty($params) ? $params['date_to'] : '' }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-4">
                           <label>Date Range: </label>
                           <select name="date_range" id="date_range"  class="formatedDateRange customselect" data-plugin="customselect" >
                              <option data-default-selected="" value="" selected="">Select Date Range</option>
                              <option value="1"{{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
							   <option value="2"{{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
							   <option value="3"{{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
							   <option value="4"{{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
							   <option value="5"{{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
							   <option value="6"{{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                           </select>
                        </div>
                     </div>
                     <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                     <a href="{{ url('helpdesk/kyc') }}" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
         </div>
         <!-- end col -->
      </div>
	  @if($pageView)
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-dark text-white">
                  <div class="card-widgets">
                     <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                     <!--<a href="javascript:;"><i class="mdi mdi-plus"></i></a> --->
                  </div>
                  <h5 class="card-title mb-0 text-white">View KYC</h5>
               </div>
               <div class="card-body">
                  <div class="row mb-2">
                     <div class="col-sm-7">

                     </div>
                     <div class="col-sm-5">
                        <div class="text-sm-right">
                           <button type="button" class="btn btn-light mb-1"><i class="fas fa-file-excel" style="color: #34a853;"></i> Export Excel</button>
                        </div>
                     </div>
                  </div>
                  <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Username</th>
                           <th>Date of upload</th>
                           <th>Doc Type</th>
                           <th>Doc No.</th>
                           <th>Status</th>
                           <th>View</th>
                        </tr>
                     </thead>
                     <tbody>@php
					$i = 1;
					@endphp

					@if(isset($kycData) && !empty($kycData))
						@forelse($kycData as $key => $search_kycData)
                        <tr>

							<td>{{ $i++ }}</td>
							<td>{{ $search_kycData->USERNAME }}</td>
							@if(isset($search_kycData->UPDATED_AT) || $search_kycData->UPDATED_AT != '')
								<td>{{ $search_kycData->UPDATED_AT }}</td>
							@else
								<td>{{ $search_kycData->UPDATED_DATE }}</td>
							@endif

							@if($search_kycData->ACCOUNT_TYPE === 'Savings' || $search_kycData->ACCOUNT_TYPE === 'Current')
								<td>BANK DETAILS</td>
							@else
								@if($search_kycData->DOCUMENT_TYPE == 1)
									<td>ADDRESS</td>
								@elseif($search_kycData->DOCUMENT_TYPE == 2)
									<td>PAN</td>
								@elseif($search_kycData->DOCUMENT_TYPE == 3)
									<td>SUPPORT DOC</td>
								@elseif($search_kycData->DOCUMENT_TYPE == 'Savings' || $search_kycData->DOCUMENT_TYPE == 'Current')
									<td>BANK DETAILS</td>
								@else
									<td>BANK DETAILS</td>
								@endif
							@endif

							@if($search_kycData->DOCUMENT_TYPE == 1 || $search_kycData->DOCUMENT_TYPE == 2 || $search_kycData->DOCUMENT_TYPE == 3)
								<td>{{ $search_kycData->DOCUMENT_NUMBER }}</td>
							@elseif($search_kycData->ACCOUNT_NUMBER !='' )
								<td>{{ $search_kycData->ACCOUNT_NUMBER }}</td>
							@else
								<td>{{ $search_kycData->DOCUMENT_NUMBER }}</td>
							@endif

							@if($search_kycData->ACCOUNT_TYPE == 'Savings' || $search_kycData->ACCOUNT_TYPE == 'Current')
								@if($search_kycData->STATUS == 0 )
									<td><span class="badge bg-soft-success text-danger shadow-none">Failed</span></td>
								@elseif($search_kycData->STATUS == 1)
									<td><span class="badge bg-soft-success text-success shadow-none">Success</span></td>
								@elseif($search_kycData->STATUS == 2)
									<td><span class="badge bg-warning text-white shadow-none">Pending</span></td>
								@elseif($search_kycData->STATUS == 3)
									<td><span class="badge bg-soft-danger text-danger shadow-none">Reject</span></td>
								@elseif($search_kycData->STATUS == null)
									<td></td>
								@endif
							@else
								@if($search_kycData->DOCUMENT_STATUS == 0 )
									<td><span class="badge bg-soft-success text-danger shadow-none">Failed</span></td>
								@elseif($search_kycData->DOCUMENT_STATUS == 1)
									<td><span class="badge bg-soft-success text-success shadow-none">Success</span></td>
								@elseif($search_kycData->DOCUMENT_STATUS == 2)
									<td><span class="badge bg-warning text-white shadow-none">Pending</span></td>
								@elseif($search_kycData->DOCUMENT_STATUS == 3)
									<td><span class="badge bg-soft-danger text-danger shadow-none">Reject</span></td>
								@elseif($search_kycData->DOCUMENT_STATUS == null)
								<td></td>
								@endif
							@endif
							<td>
							@if($search_kycData->DOCUMENT_TYPE == '1' || $search_kycData->DOCUMENT_TYPE == '2' || $search_kycData->DOCUMENT_TYPE == '3' )
							<button type="button" onclick="viewkycImage({{ $search_kycData->USER_KYC_ID }})" class="action-icon bg-transparent border-0 font-16 tooltips" data-toggle="modal" data-target="#kycdetails"><i class=" mdi mdi-eye-outline"></i><span class="tooltiptext">View</span>
                            </button>
							@elseif($search_kycData->DOCUMENT_TYPE == 'Savings' || $search_kycData->DOCUMENT_TYPE == 'Current')
							<button type="button" onclick="viewBankDetails({{ $search_kycData->USER_ID }})" class="action-icon bg-transparent border-0 font-16 tooltips" data-toggle="modal" data-target="#useraccountdetails"><i class=" mdi mdi-eye-outline"></i><span class="tooltiptext">View</span>
                            </button>
							@else
							<button type="button" onclick="viewBankDetails({{ $search_kycData->USER_ID }})" class="action-icon bg-transparent border-0 font-16 tooltips" data-toggle="modal" data-target="#useraccountdetails"><i class=" mdi mdi-eye-outline"></i><span class="tooltiptext">View</span>
                            </button>
							@endif
							@if($search_kycData->DOCUMENT_TYPE == '1' || $search_kycData->DOCUMENT_TYPE == '2' || $search_kycData->DOCUMENT_TYPE == '3')
                            <a href="#" onclick="viewStatusKyc({{ $search_kycData->USER_KYC_ID }})"data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>
							@elseif($search_kycData->DOCUMENT_TYPE == 'Savings' || $search_kycData->DOCUMENT_TYPE == 'Current')
                            <a href="#" onclick="viewStatusBank({{ $search_kycData->USER_KYC_ID }})"data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>
							@else
							<a href="#" onclick="viewStatusBank({{ $search_kycData->USER_ACCOUNT_ID }})"data-toggle="modal" title="Set User Flag" data-target="#bankstatuspopup" class="font-17 tooltips"><i class="icon-wrench"></i><span class="tooltiptext">Change Status</span></a>
							@endif
							@if($search_kycData->DOCUMENT_STATUS == 2)
							<a href="{{url('/helpdesk/kyc/edit',[$search_kycData->USERNAME,
											$search_kycData->USER_KYC_ID,$search_kycData->DOCUMENT_TYPE])}}"
							   class="action-icon font-18 tooltips">
								<i class="fa-edit fa"></i>
								<span class="tooltiptext">Edit</span>
							</a>
							@endif
							</td>
                        </tr>
						@empty
						<tr>
							<td>data not found</td>
						</tr>
						@endforelse
					@endif
                     </tbody>
                  </table>

					<div class="customPaginationRender mt-2" data-form-id="#userSearchForm" style="display:flex; justify-content: space-between;"class="mt-2">

                        <div>More Records</div>
                        <div>
						{{ !empty($kycData) ? $kycData->render("pagination::customPaginationView"): '' }}
                        </div>

                    </div>
				</div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
	  @endif

   </div>


<!--model for kyc details user-->

<div class="modal fade" id="kycdetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
         <div class="modal-header border-0 pb-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <h4 class="mt-0">View Kyc Details</h4>
            <div class="">
               <table width="100%" class="table mb-0 basic-datatable" id="kycTable">
					<tr>
                        <th><b>#</b></th>
						<th><b>Date of upload</b></th>
						<th><b>Document Type</b></th>
						<th><b>Document Front Img</b></th>
						<th><b>Document Back Img</b></th>
						<th><b>Document number</b></th>
						<th><b>Document status</b></th>
						<th><b>View</th>
                    </tr>

                  <tbody id="UserData">

                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!--  end Modal acc -->
<!-- Modal acc -->
<div class="modal fade" id="useraccountdetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
         <div class="modal-header border-0 pb-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <h4 class="mt-0">Account Details</h4>
            <div class="">
               <table width="100%" class="table mb-0 basic-datatable" id="accTable">
					<tr>
                        <td><b>#</b> </td>
                        <td><b>Date</b> </td>
						<td><b>ACCOUNT TYPE</b></td>
                        <td><b>IFSC</b></td>
						<td><b>ACCOUNT NUMBER</b></td>
                        <td><b>BANK NAME</b></td>
                        <td><b>Document status</b></td>
                        <td><b>View</b></td>
                    </tr>

                  <tbody id="UserDataBank">

                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!--  end Modal acc -->
<div class="modal fade" id="setuserflagpopup">
   <div class="modal-dialog">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Change Status </h5>
            </div>
            <div class="card-body">
			<form id="kyc_data_update" method="post" action="" enctype="multipart/form-data">
			  @csrf
			  <input type="hidden" name="user_kyc_id" id="user_kyc_id" value="">
               <div class="form-group  multiplecustom">
                  <label>Update Status to:</label>
                  <select name="doc_status" id="doc_status" class="customselect" data-plugin="customselect">
                     <option data-default-selected="" value="" selected="">Select kyc Status</option>
                     <option value="1">Success</option>
                     <option value="2">Pending</option>
                     <option value="3">Rejected</option>
                     <option value="0">Failed</option>
                  </select>
               </div>
               <button type="button" id="update_submit" class="btn btn-warning waves-effect waves-light"><i class="far fa-edit mr-1"></i> Update</button>
			</form>
            </div>
         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="bankstatuspopup">
   <div class="modal-dialog">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Change Status </h5>
            </div>
            <div class="card-body">

			 <form id="bank_data_update" method="post" action="" enctype="multipart/form-data">
			  @csrf
			  <input type="hidden" name="user_acc_id" id="user_acc_id" value="">
               <div class="form-group  multiplecustom">
                  <label>Update Status to:</label>
                  <select name="bank_status" id="bank_status" class="customselect" data-plugin="customselect">
                     <option data-default-selected="" value="" selected="">Select Bank Status</option>
                     <option value="1">Success</option>
                     <option value="2">Pending</option>
                     <option value="3">Rejected</option>
                     <option value="0">Failed</option>
                  </select>
               </div>
               <button type="button" id="update_submit_bank" class="btn btn-warning waves-effect waves-light"><i class="far fa-edit mr-1"></i> Update</button>
			   </form>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
@section('js')
<script src="{{asset('assets/libs/twitter-bootstrap-wizard/twitter-bootstrap-wizard.min.js')}}"></script>
<script src="{{ asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
<script src="{{asset('assets/js/pages/form-wizard.init.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script src="{{asset('assets/libs/magnific-popup/magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/js/pages/gallery.init.js')}}"></script>
<script src="{{ asset('js/bo/kyc.js') }}"></script>
@endsection
