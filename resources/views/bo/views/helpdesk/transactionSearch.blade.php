@extends('bo.layouts.master')

@section('title', "Transaction Search | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('post_css')
<style>
   .ms-container {
      max-width: 650px;
   }

   .dropdown.bootstrap-select .inner {
      overflow: auto !important
   }

   .dropdown.bootstrap-select.show .dropdown-menu.show {
      min-width: 100% !important;
      transform: translate(0) !important;
      height: 200px;
   }
</style>
@endsection

@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                     <li class="breadcrumb-item active">Transaction Search</li>
                  </ol>
               </div>
               <h4 class="page-title">
                  Transaction Search
               </h4>
               {{Session::get('errorvalidation')}}
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <form id="userSearchForm" action="{{ route('helpdesk.transaction_search.search') }}{{ !empty($params) ? empty($params['page']) ? '' : "?page=".$params['page'] : '' }}" method="post">
                     @csrf
                     <div class="form-row align-items-center">
                        <div class="form-group col-md-2">
                           <label class="mb-1">Search By: </label>
                        </div>
                        <div class="form-group col-md-5">
                           <div class="radio radio-info form-check-inline">
                              <input type="radio" id="search_by_username" value="username" name="search_by" {{ !empty($params) ? $params['search_by'] == 'username' ? "checked" : ''  : ''  }} checked>
                              <label for="search_by_username"> Username </label>
                           </div>
                           <div class="radio radio-info form-check-inline">
                              <input type="radio" id="search_by_email" value="email" {{ !empty($params) ? $params['search_by'] == 'email' ? "checked" : ''  : ''  }} name="search_by">
                              <label for="search_by_email"> Email </label>
                           </div>
                           <div class="radio radio-info form-check-inline">
                              <input type="radio" id="search_by_contact_no" value="contact_no" {{ !empty($params) ? $params['search_by'] == 'contact_no' ? "checked" : ''  : ''  }} name="search_by">
                              <label for="search_by_contact_no"> Contact No </label>
                           </div>
                        </div>
                        <div class="form-group col-md-5">
                           <label class="sr-only" for="search_by_value">Search Input</label>
                           <input type="text" class="form-control" name="username" id="username" placeholder="Search by Username, Email & Contact No" value="{{ !empty($params) ? $params['username']  : ''  }}">
                        </div>
                     </div>
                     <div class="row  align-items-center">


                        <div class="form-group col-md-6">
                           <label>Reference No: </label>
                           <input type="text" class="form-control" name="ref_no" id="ref_no" placeholder="Reference No" value="{{ !empty($params) ? $params['ref_no']  : ''  }}">
                        </div>
                        <div class="form-group col-md-6">
                           <label>Amount:</label>
                           <input type="text" class="form-control" name="amount" id="amount" placeholder="Amount" value="{{ !empty($params) ? $params['amount']  : ''  }}">
                        </div>
                     </div>
                     <div class="row  align-items-center">
                        <div class="form-group col-md-4">
                           <label>Coin Type: </label>
                           <select name="coin_type" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="" selected="">Select</option>
                              @foreach($coinTypes as $coinTypes)
                              <option value="{{ $coinTypes->COIN_TYPE_ID }}" {{ !empty($params) ? $params['coin_type'] == $coinTypes->COIN_TYPE_ID  ? "selected" : ''  : ''  }}>{{ $coinTypes->NAME}}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="form-group col-md-4 multiplecustom">
                           <label>Transaction Type:</label>
                           <select name="transaction_type[]" class="form-control selectpicker" multiple data-style="btn-light">
                              @foreach($transTypes as $transType)
                              <option value="{{ $transType->TRANSACTION_TYPE_ID}}" {{ !empty($params['transaction_type']) ? in_array($transType->TRANSACTION_TYPE_ID,$params['transaction_type']) ? "selected" : ''  : ''  }}>{{ $transType->TRANSACTION_TYPE_NAME}}</option>
                              @endforeach

                           </select>
                        </div>
                        <div class="form-group col-md-4">
                           <label>Transaction Status:</label>
                           <select name="transaction_status" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="blank" selected="">Select</option>
                              <option value="1" {{ !empty($params) ? $params['transaction_status'] == 1 ? "selected" : ''  : ''  }}>Success</option>
                              <option value="2" {{ !empty($params) ? $params['transaction_status'] == 2 ? "selected" : ''  : ''  }}>Pending</option>
                              <option value="3" {{ !empty($params) ? $params['transaction_status'] == 3 ? "selected" : ''  : ''  }}>Cancelled</option>
                              <option value="4" {{ !empty($params) ? $params['transaction_status'] == 4 ? "selected" : ''  : ''  }}>Rejected</option>
                              <option value="204" {{ !empty($params) ? $params['transaction_status'] == 204 ? "selected" : ''  : ''  }}>Failed</option>
                              <option value="208" {{ !empty($params) ? $params['transaction_status'] == 208 ? "selected" : ''  : ''  }}>Approved</option>
                              <option value="209" {{ !empty($params) ? $params['transaction_status'] == 209 ? "selected" : ''  : ''  }}>Withdraw Paid</option>
                              <option value="112" {{ !empty($params) ? $params['transaction_status'] == 112 ? "selected" : ''  : ''  }}>Withdraw Rejected</option>
                              <option value="216" {{ !empty($params) ? $params['transaction_status'] == 216 ? "selected" : ''  : ''  }}>Withdraw Failed</option>
                           </select>
                        </div>
                     </div>
                     <div class="row  customDatePickerWrapper">
                        <div class="form-group col-md-4">
                           <label>From: </label>
                           <input name="date_from" id="date_from" type="text" class="form-control customDatePicker from" placeholder="Select From Date" value="{{ !empty($params) ? $params['date_from'] : \Carbon\Carbon::today()->setTime("00","00","00")->format('d-M-Y H:i:s') }}">
                        </div>
                        <div class="form-group col-md-4">
                           <label>To: </label>
                           <input name="date_to" id="date_to" type="text" class="form-control customDatePicker to" placeholder="Select To Date" value="{{ !empty($params) ? $params['date_to'] : \Carbon\Carbon::today()->setTime("23","59","59")->format('d-M-Y H:i:s') }}">
                        </div>
                        <div class="form-group col-md-4">
                           <label>Date Range: </label>
                           <select name="date_range" id="date_range" class="formatedDateRange customselect" data-plugin="customselect">
                              <option data-default-selected="" value="" selected="">Select</option>
                              <option value="1">Today</option>
                              <option value="2">Yesterday</option>
                              <option value="3">This Week</option>
                              <option value="4">Last Week</option>
                              <option value="5">This Month</option>
                              <option value="6">Last Month</option>
                           </select>
                        </div>
                        <div class="form-group col-md-4">
                           <label>Private Table: </label>
                           <select name="private_table" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="" selected="">Select</option>
                              <option value="pvt_yes" {{ !empty($params) ? $params['private_table'] == "pvt_yes" ? "selected" : ''  : ''  }}>Yes</option>
                              <option value="pvt_no" {{ !empty($params) ? $params['private_table'] == "pvt_no" ? "selected" : ''  : ''  }}>No</option>
                           </select>
                        </div>
                     </div>
                     <button type="submit" id="search" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                     <a href="{{ route('helpdesk.transaction_search.index') }}" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
         </div>
         <!-- end col -->
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-dark text-white">
                  <div class="card-widgets">
                     <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>

                     <!--<a href="javascript:;"><i class="mdi mdi-plus"></i></a> --->
                  </div>
                  <h5 class="card-title mb-0 text-white">Transaction Search</h5>
               </div>
               <div class="card-body">
                  <div class="row mb-2">
                     <div class="col-sm-7">
                        @php
                        $totalAmount =0;
                        if(!empty($allTransactionSearchData)){
                        foreach($allTransactionSearchData as $key=>$totalToBePaid){
                        $totalAmount += $totalToBePaid->TRANSACTION_AMOUNT;
                        $count= ++$key;
                        }
                        }
                        @endphp
                        <h5 class="font-15 mt-0">Total Amount : <span class="text-danger">@if(!empty($totalAmount))({{ $totalAmount }})@else 0 @endif</span>&nbsp;&nbsp;
                     </div>
                     <div class="col-sm-5">
                        <div class="text-sm-right">
                           @if(!empty($allTransactionSearchData))
                           @if(!$allTransactionSearchData->isEmpty())
                           <a href="{{ route('helpdesk.getExcelExport') }}"><button type="button" class="btn btn-light mb-1"><i class="fas fa-file-excel" style="color: #34a853;"></i> Export Excel</button></a>
                           @endif
                           @endif
                        </div>
                     </div>
                     <!-- end col-->
                  </div>
                  <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                     <thead>
                        <tr>
                           <th>Username</th>
                           <th>Table Name</th>
                           <th>Reference No</th>
                           <th>Type</th>
                           <th>Status</th>
                           <th>Current Balance</th>
                           <th>Amount</th>
                           <th>Closing Balance</th>
                           <th>Transaction Date</th>
                           <th>Details</th>
                        </tr>
                     </thead>
                     <tbody>
                        @if(!empty($allTransactionSearchData))
                        @foreach($allTransactionSearchData as $transactionSearchData)
                        <tr>
                           <td><a href="#">{{ $transactionSearchData->USERNAME}}</a></td>

                           @if(!empty($transactionSearchData->TOURNAMENT_NAME))
                           @if($transactionSearchData->PRIVATE_TABLE == 2)
                           <td>{{ $transactionSearchData->TOURNAMENT_NAME }} (Private- {{ $transactionSearchData->PVT_USERNAME }})</td>
                           @else
                           <td>{{ $transactionSearchData->TOURNAMENT_NAME }}</td>
                           @endif
                           @else
                           <td>--</td>
                           @endif
                           <td>{{ $transactionSearchData->INTERNAL_REFERENCE_NO}}</td>
                           <td>
                              @if($transactionSearchData->TRANSACTION_DESCRIPTION =='Adjustment_Deposit')
                              <a href="#">Adjustment_Deposit <a>
                                    @else
                                    {{ $transactionSearchData->TRANSACTION_DESCRIPTION }}
                                    @endif
                           </td>
                           <td>
                              @if($transactionSearchData->TRANSACTION_STATUS_DESCRIPTION =='Success' || $transactionSearchData->TRANSACTION_STATUS_DESCRIPTION =='Approved')
                              <span class="badge bg-soft-success text-success shadow-none">{{ $transactionSearchData->TRANSACTION_STATUS_DESCRIPTION}}</span>
                              @elseif($transactionSearchData->TRANSACTION_STATUS_DESCRIPTION =='Pending')
                              <span class="badge bg-soft-warning text-warning shadow-none">{{ $transactionSearchData->TRANSACTION_STATUS_DESCRIPTION}}</span>
                              @else
                              {{ $transactionSearchData->TRANSACTION_STATUS_DESCRIPTION}}
                              @endif
                           </td>
                           <td>{{ $transactionSearchData->CURRENT_TOT_BALANCE}}</td>
                           @php
                           $balanceType =$transactionSearchData->BALANCE_TYPE_ID;
                           switch ($balanceType) {
                           case 1:
                           $balanceType = "(Deposit)";
                           break;
                           case 2:
                           $balanceType = "(Promo)";
                           break;
                           case 3:
                           $balanceType = "(Win)";
                           break;
                           default:
                           $balanceType = "(-)";
                           break;
                           }
                           @endphp
                           <td>{{ $transactionSearchData->TRANSACTION_AMOUNT}} <b>{{ $balanceType}}</b></td>
                           <td>{{ $transactionSearchData->CLOSING_TOT_BALANCE}}</td>
                           <td>{{ $transactionSearchData->TRANSACTION_DATE}}</td>
                           <td>
                              @if($transactionSearchData->TRANSACTION_DESCRIPTION =="Deposit")

                              @if(!empty($transactionSearchData->PROMO_CODE))
                              <a value="{{ $transactionSearchData->PROMO_CODE }}" class="card-link promoButton">{{ $transactionSearchData->PROMO_CODE}}</a>
                              @else
                              --
                              @endif
                              @elseif($transactionSearchData->TRANSACTION_DESCRIPTION =="Refer a Friend Bonus")
                              @php
                              $getRAfName=$transactionSearchData->gerReferFriendName($transactionSearchData->INTERNAL_REFERENCE_NO)->PROMO_CODE ?? null;
                              @endphp
                              @if(!empty($getRAfName))
                              <button type="button" class="btn btn-info waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-center">{{ $getRAfName->USERNAME }}</button>
                              @else
                              --
                              @endif

                              @elseif($transactionSearchData->TRANSACTION_TYPE_ID == 126)
                              <button type="button" data-user_id="{{ $transactionSearchData->USER_ID }}" data-in_ref_no="{{ $transactionSearchData->INTERNAL_REFERENCE_NO }}" class="btn btn-info waves-effect waves-light bountyButton">Bounty</button>

                              @else
                              --
                              @endif
                           </td>
                        </tr>
                        @endforeach
                        @endif
                     </tbody>
                  </table>
                  <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" style="display:flex; justify-content: space-between;">
                     @if(!empty($allTransactionSearchData))
                     <div>More Records</div>
                     <div>
                        {{ $allTransactionSearchData->render("pagination::customPaginationView") }}
                     </div>
                     @endif
                  </div>
                  {{-- Promo info popup statrt--}}
                  <div class="modal fade bs-example-modal-center show detailsModal " tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" style="display: none; padding-right: 17px;" aria-modal="true">
                     <div class="modal-dialog">
                        <div class="modal-content  shadow-none position-relative">
                           <div id="promoLoder" style="display:none">
                              <div class="d-flex justify-content-center hv_center">
                                 <div class="spinner-border" role="status"></div>
                              </div>
                           </div>
                           <div class="card  mb-0">
                              <div class="card-header bg-dark py-3 text-white">
                                 <div class="card-widgets stop">
                                    <a href="#" data-dismiss="modal">
                                       <i class="mdi mdi-close"></i>
                                    </a>
                                 </div>
                                 <h5 class="card-title mb-0 text-white font-18"><span class="text-success" id="getWayPoupType"></span>Details</h5>
                              </div>
                              <div class="card-body " style="position:relative">
                                 <div id="blurr-effect" class="blureffect" style="display:none;z-index:9; position:absolute; left:0; top:0; bottom:0; right:0;"></div>
                                 <div class="popupResul"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>


                  {{-- Promo info popup end--}}
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
   </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.js') }}"></script>
<script src="{{ asset('assets/libs/switchery/switchery.min.js') }}"></script>
<script src="{{ asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
<!-- Init js-->
<script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>
<script>
@if(session()->has('message'))
               $.toast({
                    heading: 'oops!!',
                    text: '{!! session()->get("message") !!}',
                    showHideTransition: 'slide',
                    icon: 'error',
                    loader: true,
                    position: 'top-right',
                    hideAfter: 6000
                });
                {!! session()->forget('message'); !!}
            @endif
            </script>
<script>
   $(document).ready(function() {
      // for get coin details from promo  
      $(document).on('click', '.promoButton', function() {
         var date_from = $('#date_from').val();
         var date_to = $('#date_to').val();
         var userId = $(this).val();
         $('.detailsModal').show();
         $('#promoLoder').show();
         $('.popupResul').hide();

         $.ajax({
            url: `${window.pageData.baseUrl}/helpdesk/getPromoDetails`,
            method: "POST",
            data: {
               userId: userId,
               date_from: date_from,
               // "_token": window.pageData.csrf_token,
               "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
               var points = (response.data.POINTS);
               var coinsData = `<div class="table-responsive">
                                    <table class="table table-borderless table-hover table-centered m-0">
                                    <thead class="thead-light">
                                            <tr>
                                                <th>Coins</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    ${points}
                                                </td>

                                                <td>
                                                    ${date_from}
                                                </td>

                                                <td>
                                                    ${date_to}
                                                </td>
                                            </tr>
                                            </tbody>
                                    </table>
                                </div>`;

               // $('.detailsModal').show();
               $('.popupResul').show();
               $('#promoLoder').hide();
               $('.popupResul').html(coinsData);
            },
            error: function(error) {
               $('#promoLoder').hide();
               $('.detailsModal').hide();

            }
         });
      });

      // get bounty details poup

      $('.bountyButton').click(function() {
         var date_from = $('#date_from').val();
         var date_to = $('#date_to').val();
         // var userId = $(this).val();
         var userId = $(this).data('user_id');
         var inRefNo = $(this).data('in_ref_no');

         $.ajax({
            url: `${window.pageData.baseUrl}/helpdesk/getBountyDetails`,
            method: "POST",
            data: {
               userId: userId,
               in_ref_no: inRefNo,
               // "_token": window.pageData.csrf_token,
               "_token": "{{ csrf_token() }}",
            },
            success: function(response) {
               // console.log(response);
               // var USER_ID = (response.data[0].USER_ID);
               $rowData = '';
               for (let row of response.data) {
                  $rowData += `<tr>
                                            <td>${row.USERNAME}</td>
                                            <td>${row.BURST_USER_BOUNTY_AMOUNT}</td>
                                            <td>${row.OPENING_BALANCE}</td>
                                            <td>${row.BOUNTY_AMOUNT}</td>
                                            <td>${row.CLOSING_BALANCE}</td>
                                        </tr>`;
               }
               var bountyData = `<div class="table-responsive">
                                    <table class="table table-borderless table-hover table-centered m-0">
                                    <thead class="thead-light">
                                            <tr>
                                                <th>Username</th>
                                                <th>Bounty on user</th>
                                                <th>Opening Bal</th>
                                                <th>Amount</th>
                                                <th>Closing Bal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ${$rowData}    
                                        </tbody>
                                    </table>
                                </div>`;

               $('.detailsModal').show();
               $('.popupResul').html(bountyData);
            }
         });
      });

      $('.stop').click(function() {
         $('.detailsModal').hide();
      });
      $("#search").click(function() {
         $("#search").hide();
      });
   });
</script>
@endsection