@extends('bo.layouts.master')

@section('title', "Payment Search | PB BO")

@section('pre_css')
<!-- <link href="{{ asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs//rwd-table/rwd-table.min.css') }}" rel="stylesheet" type="text/css" /> -->
<!-- Plugins css -->
<link href="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('post_css')
<style>
    .ms-container{
        max-width: 650px;
    }    
</style>

@endsection
@section('content')

    <!-- start page title -->

    <div class="content">
        <div class="container-fluid">
           <div class="row">
              <div class="col-12">
                 <div class="page-title-box">
                    <div class="page-title-right">
                       <ol class="breadcrumb m-0">
                          <li class="breadcrumb-item"><a href="">Home</a></li>
                          <li class="breadcrumb-item active"> Payment Search</li>
                       </ol>
                    </div>
                    <h4 class="page-title">
                     Payment Search
                    </h4>
                 </div>
              </div>
           </div>
           <div class="row">
              <div class="col-12">
                 <div class="card">
                    <div class="card-body">
                       <form id="paymentSearchForm" action="{{ route('helpdesk.payment_search.search') }}{{ !empty($params) ? empty($params['page']) ? '' : "?page=".$params['page'] : '' }}" method="post">
                        @csrf
                        <div class="form-row align-items-center">
                             <div class="form-group col-md-2">
                                <label class="mb-1">Search By: </label>
                             </div>
                             <div class="form-group col-md-5">
                                <div class="radio radio-info form-check-inline">
                                   <input  type="radio" id="search_by_username" value="username" name="search_by" {{ !empty($params) ? $params['search_by'] == 'username' ? "checked" : ''  : ''  }} checked>
                                   <label for="search_by_username"> Username </label>
                                </div>
                                <div class="radio radio-info form-check-inline">
                                   <input type="radio" id="search_by_email" value="email"{{ !empty($params) ? $params['search_by'] == 'email' ? "checked" : ''  : ''  }} name="search_by">
                                   <label for="search_by_email"> Email </label>
                                </div>
                                <div class="radio radio-info form-check-inline">
                                   <input type="radio" id="search_by_contact_no" value="contact_no" {{ !empty($params) ? $params['search_by'] == 'contact_no' ? "checked" : ''  : ''  }} name="search_by">
                                   <label for="search_by_contact_no"> Contact No </label>
                                </div>
                             </div>
                             <div class="form-group col-md-5">
                                <label class="sr-only" for="search_by_value">Search Input</label>
                                <input type="text" class="form-control" name="username" id="username" placeholder="Search by Username, Email & Contact No" value="{{ !empty($params) ? $params['username']  : ''  }}">
                             </div>
                          </div>                         
                          <div class="row  align-items-center">
                           
     
                             <div class="form-group col-md-4">
                                <label>Reference No: </label>
                                <input type="text" class="form-control" name="ref_no" id="ref_no" placeholder="" value="{{ !empty($params) ? $params['ref_no']  : ''  }}">
                             </div>
                             <div class="form-group col-md-4">
                                <label>Amount:</label>
                                <input type="text" class="form-control" name="amount" id="amount" placeholder="" value="{{ !empty($params) ? $params['amount']  : ''  }}">
                             </div>
                             <div class="form-group col-md-4">
                                <label>Deposit Code:</label>
                                <input type="text" class="form-control" name="deposit_code" id="deposit_code" placeholder="" value="{{ !empty($params) ? $params['deposit_code']  : ''  }}">
                             </div>
     
                          </div>
                          <div class="row  align-items-center">
                             <div class="form-group col-md-4">
                                <label>Payment Method: </label>
                                <select name="payment_method" class="customselect" data-plugin="customselect">
                                        <option data-default-selected="" value="" selected="">Select</option>
                                        <option value="PAYU"{{ !empty($params) ? $params['payment_method'] == 'PAYU' ? "selected" : ''  : ''  }}>PAYU</option>
                                        <option value="PAY BY CASH"{{ !empty($params) ? $params['payment_method'] == 'PAY BY CASH' ? "selected" : ''  : ''  }}>PAY BY CASH</option>
                                        <option value="PAY BY NEFT"{{ !empty($params) ? $params['payment_method'] == 'PAY BY NEFT' ? "selected" : ''  : ''  }}>PAY BY NEFT/CHEQUE</option>
                                        <option value="Cash Free"{{ !empty($params) ? $params['payment_method'] == 'Cash Free' ? "selected" : ''  : ''  }}>CASHFREE</option>
                                        <option value="PayTM"{{ !empty($params) ? $params['payment_method'] == 'PayTM' ? "selected" : ''  : ''  }}>PAYTM</option>
                                </select>
                             </div>
                              {{-- <div class="form-group col-md-4">
                                <label>Payment Status: </label>
                                <select name="payment_status" class="customselect" data-plugin="customselect">
                                   <option data-default-selected="" value="" selected="">Select Payment Status</option>
                                    <option value="1"{{ !empty($params) ? $params['payment_status'] == 1 ? "selected" : ''  : ''  }}>Success</option>
                                    <option value="2"{{ !empty($params) ? $params['payment_status'] == 2 ? "selected" : ''  : ''  }}>Pending</option>
                                    <option value="3"{{ !empty($params) ? $params['payment_status'] == 3 ? "selected" : ''  : ''  }}>Rejected</option>
                                    <option value="4"{{ !empty($params) ? $params['payment_status'] == 4 ? "selected" : ''  : ''  }}>Cancelled</option>
                                    <option value="5"{{ !empty($params) ? $params['payment_status'] == 5 ? "selected" : ''  : ''  }}>Failed</option>
                                </select>
                             </div> --}}
                             <div class="form-group col-md-4 multiplecustom">
                                <label>Payment Status:</label>
                                <select name="payment_status[]" class="form-control selectpicker" multiple data-style="btn-light">
                                    
                                   {{-- <option  value="blank" selected="">Select Transaction Type</option> --}}
                                   <option value="success"{{ !empty($params['payment_status']) ? in_array('success',$params['payment_status'])? "selected" : ''  : ''  }}>Success</option>
                                    <option value="pending"{{ !empty($params['payment_status']) ? in_array('pending',$params['payment_status'])? "selected" : ''  : ''  }}>Pending</option>
                                    <option value="rejected"{{ !empty($params['payment_status']) ? in_array('rejected',$params['payment_status'])? "selected" : ''  : ''  }}>Rejected</option>
                                    <option value="cancelled"{{ !empty($params['payment_status']) ? in_array('cancelled',$params['payment_status'])? "selected" : ''  : ''  }}>Cancelled</option>
                                    <option value="failed"{{ !empty($params['payment_status']) ? in_array('failed',$params['payment_status'])? "selected" : ''  : ''  }}>Failed</option>
                                </select>
                             </div>
                          </div>
                          <div class="row  customDatePickerWrapper">
                             <div class="form-group col-md-4">
                                <label>From: </label>
                                <input name="date_from" type="text" class="form-control customDatePicker from" placeholder="Select From Date" value="{{ !empty($params) ? $params['date_from'] : \Carbon\Carbon::today()->setTime("00","00","00")->format('d-M-Y H:i:s') }}">
                             </div>
                             <div class="form-group col-md-4">
                                <label>To: </label>
                                <input name="date_to" type="text" class="form-control customDatePicker to" placeholder="Select To Date" value="{{ !empty($params) ? $params['date_to'] : \Carbon\Carbon::today()->setTime("23","59","59")->format('d-M-Y H:i:s') }}">
                             </div>
                             <div class="form-group col-md-4">
                                <label>Date Range: </label>
                                <select name="date_range" id="date_range"  class="formatedDateRange customselect" data-plugin="customselect" >
                                   <option data-default-selected="" value="" selected="">Select Date Range</option>
                                   <option value="1"{{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                                   <option value="2"{{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                                   <option value="3"{{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                                   <option value="4"{{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                                   <option value="5"{{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                                   <option value="6"{{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                                 </select>
                             </div>
                          </div>
                           <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                          <a href="{{ route('helpdesk.payment_search.index') }}" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                       </form>
                    </div>
                    <!-- end card-body-->
                 </div>
                 
                 
              </div>
              <!-- end col -->
           </div>
           <div class="row">
                 <div class="col-12">
                     <div class="card">
                         <div class="card-header bg-dark text-white">
                                     <div class="card-widgets">
                                         <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
     
                                         <!--<a href="javascript:;"><i class="mdi mdi-plus"></i></a> --->                                   
                                     </div>
                                     <h5 class="card-title mb-0 text-white">Manage Payments</h5>
                                 </div>
                         <div class="card-body">
                             <div class="row mb-2">
                                 <div class="col-sm-7">
                                    <h5 class="font-15 mt-0">Total Amount : <span class="text-danger">@if(!empty($totalAmount))({{ $totalAmount }})@else 0 @endif</span>&nbsp;&nbsp;
                                       {{-- &nbsp;&nbsp;Pending Amount : <span class="text-danger">@if(!empty($pendingAmount) &&( $params['payment_status'] == 2 || $params['payment_status'] == ""))({{ $pendingAmount }})@else 0 @endif </span>&nbsp;&nbsp;|
                                       &nbsp;&nbsp;Failed Amount : <span class="text-danger">@if(!empty($failedAmount) &&( $params['payment_status'] == 5 || $params['payment_status'] == ""))({{ $failedAmount }})@else 0 @endif </span>&nbsp;&nbsp;| --}}
                                       
                                 </div>
                                 <div class="col-sm-5">
                                     <div class="text-sm-right">
           <!---                            
     <button type="button" class="btn btn-success mb-1 mr-1"><i class="fas fa-file-pdf mr-1"></i> PDF</button>--->
     {{-- <button type="button" class="btn btn-success mb-1 mr-1"><i class="mdi-refresh mdi mr-1"></i> Referesh All  </button> --}}
     @if(!empty($payTransData))
     @if(!$payTransData->isEmpty())
     <a href="{{ route('helpdesk.payment_search.payExcelExport') }}"><button type="button" class="btn btn-light mb-1"><i class="fas fa-file-excel" style="color: #34a853;"></i> Export Excel</button></a>
     @endif
     @endif
                                     </div>
                                 </div>
                                 <!-- end col-->
                             </div>
                             <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                                 <thead>
                                                     <tr><th>#</th>
                                                         <th>Username</th>
                                                         <th>Internal Ref. No</th>
                                                         <th>Pay Method</th>
                                                         <th>Mode</th>
                                                         <th>Amount</th>
                                                         <th>Pay Timestamp</th>
                                                         <th>Status</th>
                                                         {{-- <th>Gateway Status</th> --}}
                                                         <th>Promocode</th>
                                                         <th>Update Timestamp</th>
                                                         <th>Action</th>
                                                         
                                                     </tr>
                                                 </thead>
                                                 <tbody>
                                                        @if(!empty($payTransData))
                                                        @foreach($payTransData as $allPayTransData)
                                                        <tr> <td>{{ $loop->iteration }}</td>
                                                         <td><a href="#">{{ $allPayTransData->USERNAME}}</a></td>
                                                        <td> {{ $allPayTransData->INTERNAL_REFERENCE_NO }} </td>
                                                        <td> {{ $allPayTransData->PAYPAL_RETURN_VALUES }} </td>
                                                        <td> {{ $allPayTransData->PROVIDER_NAME }} </td>
                                                        <td> {{ $allPayTransData->PAYMENT_TRANSACTION_AMOUNT }}</td>
                                                        <td> {{ $allPayTransData->PAYMENT_TRANSACTION_CREATED_ON }}</td>
                                                        <td>
                                                           <div class="statusBadge" data-ref-no="{{ $allPayTransData->INTERNAL_REFERENCE_NO }}">
                                                               @if($allPayTransData->TRANSACTION_STATUS_DESCRIPTION == "Success" ||$allPayTransData->TRANSACTION_STATUS_DESCRIPTION == "Approved")
                                                               <span class="badge bg-soft-success text-success shadow-none">{{ $allPayTransData->TRANSACTION_STATUS_DESCRIPTION }}</span>
                                                               @elseif($allPayTransData->TRANSACTION_STATUS_DESCRIPTION == "Pending")
                                                               <span class="badge bg-warning text-white shadow-none">{{ $allPayTransData->TRANSACTION_STATUS_DESCRIPTION }}</span>
                                                               @elseif($allPayTransData->TRANSACTION_STATUS_DESCRIPTION == "Rejected")
                                                               <span class="badge bg-soft-danger text-danger shadow-none">{{ $allPayTransData->TRANSACTION_STATUS_DESCRIPTION }}</span>
                                                               @else
                                                               <span class="badge bg-soft-danger text-danger shadow-none">{{ $allPayTransData->TRANSACTION_STATUS_DESCRIPTION }}</span>
                                                               @endif
                                                            </div>
                                                         </td>
                                                        {{-- <td>GetWay Status</td> --}}
                                                        @if(!empty($allPayTransData->PROMO_CODE))
                                                        <td> {{ $allPayTransData->PROMO_CODE }}</td>
                                                        @else
                                                        <td>NULL</td>
                                                        @endif
                                                        <td>{{ $allPayTransData->UPDATED_DATE }}</td>
                                                        @if($allPayTransData->TRANSACTION_STATUS_DESCRIPTION == "Pending" || $allPayTransData->TRANSACTION_STATUS_DESCRIPTION == "Failed")
                                                        <td><a href="#"  title="Check GateWay status" data-target="#getwayStatuspopup" data-ref-no="{{ $allPayTransData->INTERNAL_REFERENCE_NO }}"  data-user-id="{{ $allPayTransData->USER_ID }}" data-getway-type="{{ $allPayTransData->PAYPAL_RETURN_VALUES }}" data-trans-id="{{ $allPayTransData->PAYMENT_TRANSACTION_ID }}" data-trans-type-id="{{ $allPayTransData->TRANSACTION_TYPE_ID }}"data-toggle="modal" data-plugin="tippy" data-tippy-interactive="true" class="font-19 mr-1"><b><i class="mdi-refresh mdi"></i></b></a>
                                                         @elseif($allPayTransData->getTransactionStatus->TRANSACTION_STATUS_DESCRIPTION == "Success")
                                                         <td><a href="{{ route('helpdesk.payment_search.invoiceDownload') }}?username={{$allPayTransData->USERNAME}}&amount={{$allPayTransData->PAYMENT_TRANSACTION_AMOUNT}}&merchant_id={{$allPayTransData->INTERNAL_REFERENCE_NO}}&transaction_date={{$allPayTransData->PAYMENT_TRANSACTION_CREATED_ON}}&transaction_date={{$allPayTransData->PAYMENT_TRANSACTION_CREATED_ON}}" data-plugin="tippy" data-tippy-interactive="true" class="action-icon font-14" data-tippy="" data-original-title="Invoice Download" target="_blank"><i class="mdi mdi-file-pdf-box "></i></a></td>
                                                         @else
                                                         <td>--</td>
                                                         @endif
                                                         {{-- @if($allPayTransData->getTransactionStatus->TRANSACTION_STATUS_DESCRIPTION == "Pending" || $allPayTransData->getTransactionStatus->TRANSACTION_STATUS_DESCRIPTION == "Failed")
                                                                <a href="#" data-target="#setuserflagpopup" data-ref-no="{{ $allPayTransData->INTERNAL_REFERENCE_NO }}"  data-user-id="{{ $allPayTransData->USER_ID }}" data-trans-id="{{ $allPayTransData->PAYMENT_TRANSACTION_ID }}" data-tippy-animation="fade" data-tippy-arrow="true" data-toggle="modal" title="Set User Flag"  data-plugin="tippy" data-tippy-interactive="true" class="font-17"><i class="icon-wrench"></i></a></td>
                                                         @endif --}}
                                                                
                                                     </tr>
                                                     @endforeach
                                                            @endif    
                                                 </tbody>
                                             </table>
                                             <div class="customPaginationRender mt-2" data-form-id="#paymentSearchForm" style="display:flex; justify-content: space-between;">
                                                    @if(!empty($payTransData))
                                                    <div>More Records</div>
                                                    <div>
                                                    {{ $payTransData->render("pagination::customPaginationView") }}
                                                    </div>
                                                    @endif
                                            </div>
                         </div> <!-- end card body-->
                     </div> <!-- end card -->
                 </div><!-- end col-->
             </div>
            </div>
        </div>
       
               {{-- Getway status poup --}}
               <div class="modal fade" id="getwayStatuspopup">
                  <div class="modal-dialog">
                    <div class="modal-content  shadow-none position-relative">
                         <div  id="getWayloader" style="display:none"> 
                          <div class="d-flex justify-content-center hv_center">
                                <div class="spinner-border" role="status"></div>
                          </div>
                       </div>
                      <div class="card  mb-0">
                        <div class="card-header bg-dark py-3 text-white">
                          <div class="card-widgets">
                            <a href="#" data-dismiss="modal">
                              <i class="mdi mdi-close"></i>
                            </a>
                          </div>
                          <h5 class="card-title mb-0 text-white font-18" ><span class="text-success" id="getWayPoupType"></span> Update  Status</h5>
                        </div>
                    <div class="card-body " style="position:relative"><div id="blurr-effect" class="blureffect" style="display:none;z-index:9; position:absolute; left:0; top:0; bottom:0; right:0;"></div>
                       <input type="hidden" class="refNoGetway">
                       <input type="hidden" class="userIdGetway">
                       <input type="hidden" class="getWayType">
                       <input type="hidden" class="transId">
                       <input type="hidden" class="transTypeId"> 
                       <div id="getway-status-div"> 
                       <div class="form-group ">
                           <label>Trans Amount: <span id="txnAmount"></span></label>
                       </div>
                       <div class="form-group ">
                           <label>Order Status: <span id="orderStatus"></span></label>
                       </div>
                       <div class="form-group ">
                           <label>Trans Time: <span id="txnTime"></span></label>
                       </div>
                       <div class="form-group ">
                           <label>Payment Mode: <span id="paymentMode"></span></label>
                       </div>
                       <div class="form-group ">
                           <label>Bank Name: <span id="bankName"></span></label>
                       </div>
                       
                       <div class="form-group ">
                           <label>Current GateWay Status: <span class="" id="txnStatus"></span></label>
                       </div>
                     </div>
                       {{-- <div class="spinner-border text-secondary m-2 " id="loader" role="status" style="display:none"></div>  --}}
                   <div id="statusradio" class="form-group  multiplecustom">
                     <label>Update Status:</label>
                     <label class="switches">
                      <input id="trans_status_getway" type="checkbox">
                      <span class="slider round"></span>
                    </label>
                </div>
                <div class="form-group " id="pwdDiv">
                   <label>Transaction Password:</label>
                   <input type="password" class="form-control" name="transPwd" id="transPwdGetway" placeholder="Transaction Password" value="">
                </div>
                <button type="button" id ="updateGetwayStatus" class="btn btn-warning waves-effect waves-light"><i class="far fa-edit mr-1"></i> Update</button>
                <div style="display:none ;margin-top:15px" class="" id="getWayMessage" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                  </button>
               </div>
                      </div>
                      </div>
                    </div>
                  </div>
        </div>
               
@endsection

@section('js')
<!-- Datatables init -->


<!-- switchery toogle button -->
    <script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.js') }}"></script>
    <script src="{{ asset('assets/libs/switchery/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
    <!-- Init js-->
    <script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>
@endsection

@section('post_js')
<script>
      $(document).ready(function() {
         //getway satus popup start 
          $('#getwayStatuspopup').on('show.bs.modal', function(e){
              $("#txnStatus").val('');
              $("#txnAmount").val('');
              $("#orderStatus").val('');
              $("#txnTime").val('');
              $("#paymentMode").val('');
              $("#getWayloader").val('');
              $("#getWayloader").show();
              $("#blurr-effect").show();
              $("#getWayMessage").hide();
              $("#getway-status-div").hide();
              $("#txnStatus").removeClass();
              $("#updateGetwayStatus").hide();
              $("#pwdDiv").hide();
              $("#statusradio").hide();
              $relatedButton = $(e.relatedTarget);
              $model = $(this);
              $model.find('.refNoGetway').val($relatedButton.data('refNo'));
              $model.find('.userIdGetway').val($relatedButton.data('userId'));
              $model.find('.getWayType').val($relatedButton.data('getwayType'));
              $model.find('.transId').val($relatedButton.data('transId'));
              $model.find('.transTypeId').val($relatedButton.data('transTypeId'));
              
              var ref_no        = $('.refNoGetway').val();
              var user_id       = $('.userIdGetway').val();
              var getway_type   = $('.getWayType').val();
              var trans_type_id  = $('.transTypeId').val();
               // alert(trans_type_id); 
             //  $("#trans_status").attr("checked") ? 1 : 0;
             $.ajax ({
                  url : `${window.pageData.baseUrl}/helpdesk/payment_search/getCurrentGetwayStatus`,
                  method : "POST",
                  data :  {
                      ref_no :ref_no,
                      user_id :user_id,
                      getway_type :getway_type,
                      trans_type_id :trans_type_id,
                     },
                  success:function(response)
                  {
                     
                     if(response.txnStatus == "SUCCESS"){
                        $("#txnStatus").addClass("badge bg-success text-white shadow-none");
                     }else{
                       $("#txnStatus").addClass("badge bg-danger text-white shadow-none"); 
                     }
                     
                     var txnStatus = response.txnStatus;
                     var txnAmount = response.txnAmount;
                     var orderStatus = response.orderStatus;
                     var txnTime = response.txnTime;
                     var paymentMode = response.paymentMode;
                     var bankName = response.bankName;
                     if(trans_type_id == 83 || trans_type_id == 111 ){
                     $("#updateGetwayStatus").show();
                     $("#pwdDiv").show();
                     $("#statusradio").show();
                     }else{
                     $("#getway-status-div").show();
                     $("#updateGetwayStatus").show();
                     $("#pwdDiv").show();
                     $("#statusradio").show();

                     }

                     // if(txnStatus == "SUCCESS" ){
                     //    $("#getway-status-div").show();
                     //    $("#updateGetwayStatus").show();
                     // }else if( txnStatus == "PENDING"){
                     //    $("#getway-status-div").show();
                     //    $("#updateGetwayStatus").show();
                     //    $("#pwdDiv").show();
                     //    $("#statusradio").show();
                     // }else if(trans_type_id == 83){
                     //    $("#updateGetwayStatus").show();
                     //    $("#pwdDiv").show();
                     //    $("#statusradio").show();
                     //  }else{
                     //    $("#getway-status-div").show();
                     //  }
                     
                     $("#txnStatus").html(response.txnStatus);
                     $("#txnAmount").html(response.txnAmount);
                     $("#orderStatus").html(response.orderStatus);
                     $("#txnTime").html(response.txnTime);
                     $("#paymentMode").html(response.paymentMode);
                     $("#bankName").html(response.bankName);
                     $("#getWayPoupType").html(response.getWayType);
                     $("#getWayloader").hide();
                     $("#blurr-effect").hide();
                  }
            });
          });
          // update getway status on databse 
          $('#updateGetwayStatus').click(function(e){
              e.preventDefault();
              $("#changeStatus").hide();
              $("#getWayloader").show();
              $("#blurr-effect").show();
             
              
              var ref_no       = $('.refNoGetway').val();
              var user_id      = $('.userIdGetway').val();
              var pay_trans_id = $('.transId').val();
              var trans_pwd    = $('#transPwdGetway').val();
              var getway_type  = $('.getWayType').val();
              var trans_type_id  = $('.transTypeId').val();
              var getway_status  = $('#txnStatus').text();
              
              //  var trans_status = $( "#trans_status" ).val();
              if ($("#trans_status_getway").is(':checked')) {
                var trans_status = 1;
             }else{
                var trans_status = 0;
             }
              
              $.ajax ({
                  url : `${window.pageData.baseUrl}/helpdesk/payment_search/changePaymentStatus`,
                  method : "POST",
                  data :  {
                      ref_no :ref_no,
                      user_id :user_id,
                      pay_trans_id :pay_trans_id,
                      trans_status :trans_status,
                      trans_pwd    :trans_pwd,
                      getway_type  :getway_type,
                      trans_type_id :trans_type_id,
                      getWaySatus :getway_status,
                  },
                  success:function(response)
                  {
                  document.getElementById('getWayMessage').className = '';
                  // var satausUpdate = $(this).data('refNo');
                  // alert(satausUpdate);
                  
                  // clickRow.find('.statusBadge').html('<spn>');
                   var status = response.status;  
                   $("#getWayMessage").show();
                   if(status == 200){
                     $('table').find(`.statusBadge[data-ref-no="${ref_no}"]`).html('<span class="badge bg-soft-success text-success shadow-none">Approved</span>');
                      $("#getWayMessage").addClass("alert alert-success alert-dismissible bg-success text-white border-0 fade show");
                     //  clickRow.find('.statusBadge').html('Success');
                   }else if(status == 199){
                      $("#getWayMessage").addClass("alert alert-warning alert-dismissible bg-warning text-white border-0 fade show");
                   }else{
                       $("#getWayMessage").addClass("alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"); 
                   }
                   $("#getWayMessage").html(response.message);
                   $("#changeStatus").show();
                   $("#getWayloader").hide();
                   $("#blurr-effect").hide();
                  },
                  error: function (error) {
                     $("#getWayloader").hide();
                     $("#blurr-effect").hide();
                     $("#getWayMessage").show();
                     $("#getWayMessage").html("Something Went Wrong");
                     $("#getWayMessage").addClass("alert alert-danger alert-dismissible bg-danger text-white border-0 fade show");
                     
                  }
            });
          });
            
              
              
         

   });
  </script>
@endsection

