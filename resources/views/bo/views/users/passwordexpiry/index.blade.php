@extends('bo.layouts.master')
@section('pre_css')
<style>
    .multiplecustom .nice-select.customselect span.current{ width: 100%; display: block; overflow: hidden; }
</style> 
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item">
                                <a >Users</a>
                            </li>
                            <li class="breadcrumb-item active">Bulk Password Expiry</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Bulk Password Expiry</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" id="bulkPwdExpiryUploadForm" enctype="multipart/form-data">

                            <div class="row mb-1">
                                <div class="form-group col-md-7">
                                    <label>Upload User's List:</label>
                                    <input type="file" class="form-control" id="bulkPwdExpiryExcelUpload" name="bulkPwdExpiryExcelUpload" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required>
                                    <p class="formtext text-right"><a href="{{ asset('/data/bo/sample_bulk_password_expiry_2sv.xlsx') }}" class="text-primary">Download Sample</a></p>
                                </div>
                                <div class="form-group col-md-2"> <label class="d-block">Expire Password:</label><label class="switches"><input type="checkbox" name="expirePwd" ><span class="slider round"></span></label> </div>
                                <div class="form-group col-md-3"> <label class="d-block">Enable 2-Step Verification:</label><label class="switches"><input type="checkbox" name="enable2SV" ><span class="slider round"></span></label> </div>
                            </div>
                            <button style="display:none" if class="btn btn-primary formActionButton mr-1" id="show-import-excel" type="button" disabled="disabled">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span></button>
                            <a data-toggle="modal" id="bulkPwdExpiryUploadBtn" class="btn btn-success waves-effect waves-light mr-1 text-white">
                                <i class="fa fa-save mr-1"></i> Submit
                            </a>                      
                            <button id="import-excel-reset" type="button" class="btn btn-secondary waves-effect waves-light mr-1"><i class="fas fa-eraser mr-1"></i> Clear</button>
                        </form>
                    </div>
                    <!-- end card-body-->
                </div>
            </div>
            <!-- end col -->
        </div>
    </div>
</div>
<div class="modal fade" id="bulkPwdExpiryUploadpopup">
    <div class="modal-dialog modal-xl">
        <div class="modal-content bg-transparent shadow-none">
            <div class="card">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal">
                            <i class="mdi mdi-close"></i>
                        </a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18">Review</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive mb-2">
                        <table id="basic-datatable" class="table dt-responsive nowrap w-100 mb-1" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User ID</th>
                                    <th>Username</th>
                                    <th>Review Status</th>
                                    <th>Expire Password</th>
                                    <th>Eable 2 SV</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <form method="post" id="bulkPwdExpirySubmitForm">
                        <button type="button" id="bulkPwdExpiryReUpload" class="btn btn-warning waves-effect waves-light mr-1"><i class="fa fa-upload mr-1"></i> Reupload</button>
                        @csrf
                        <input type="hidden"  readonly id="userIds" name="userIds" value="" />
                        <input type="hidden"  readonly id="Enable2SV" name="Enable2SV" value="" />
                        <input type="hidden"  readonly id="ExpirePwd" name="ExpirePwd" value="" />
                        <button type="buttom" id="bulkPwdExpirySubmitBtn" class="btn btn-success waves-effect waves-light"><i class="far fa-save mr-1"></i> Continue Anyway</button>
                        <button style="display:none" if class="btn btn-primary formActionButton mr-1" id="show-bulk-upload-submit" type="button" disabled="disabled">
                            <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('post_js')
<script src="{{ asset('js/bo/bulk_password_expiry_2sv.js') }}"></script>
@endsection