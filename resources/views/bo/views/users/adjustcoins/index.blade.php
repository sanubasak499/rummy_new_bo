@extends('bo.layouts.master')
@section('pre_css')
<style>
  .multiplecustom .nice-select.customselect span.current{ width: 100%; display: block; overflow: hidden; }
</style> 
@endsection
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item">
                        <a >Users</a>
                    </li>
                    <li class="breadcrumb-item active">Adjust Coins</li>
                    </ol>
                </div>
                <h4 class="page-title">Adjust Coins </h4>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="card mb-4">
          <div class="card-body">
            <ul class="nav nav-tabs nav-justified">
                <li class="nav-item">
                    <a href="#home" data-toggle="tab" aria-expanded="false" class="nav-link active">
                        Single Adjustment
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#profile" data-toggle="tab" aria-expanded="true" class="nav-link">
                        Bulk Adjustment
                    </a>
                </li>
            </ul>
            <div class="tab-content mt-2">
                <div class="tab-pane active" id="home">
                    <form id="userSearchForm" action="#" method="post" >
                        {{-- <input type="hidden" name="_token" value="02dsLsi6cGcKyWuKEIe1k7F00sx9HFB6b1LvFiHf"> --}}
                        <div class="row ">
                            <div class="form-group  col-md-4">
                                <label>Username: <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="username" id="username" placeholder="" value="" requierd>
                            </div>
                              <div class="form-group col-md-4">
                                  <label>Balance Type: <span class="text-danger">*</span></label>
                                  <select class="customselect" name="balanceType" id="balanceType" data-plugin="customselect" required>
                                        <option value="" selected>Select</option>
                                        @foreach($balanceType as $type)
                                          <option value="{{ $type->BALANCE_TYPE_ID }}">{{ ucfirst($type->BALANCE_TYPE) }}</option>
                                        @endforeach
                                  </select>
                              </div>
                              <div class="form-group col-md-4 multiplecustom">
                                  <label>Coin Type: <span class="text-danger">*</span></label>
                                  <select class="customselect" name="coin_type" id="coin_type" data-plugin="customselect" required>
                                    <option value="" selected="">Select</option>
                                    @foreach($coinTypes as $coinType)
                                      <option value="{{ $coinType->COIN_TYPE_ID }}">{{  $coinType->NAME  }}</option>
                                    @endForeach
                                  </select>
                              </div>
                        </div>
                        <div class="row ">
                            <div class="form-group col-md-3 multiplecustom">
                                <label>Adjust Type: <span class="text-danger">*</span></label>
                                <select class="customselect" name="adjust_type" id="adjust_type" data-plugin="customselect" required>
                                    <option value="" selected="">Select</option>
                                    <option value="1" >Add</option>
                                    <option value="2" >Deduct</option>
                                </select>
                            </div>

                            <div class="form-group col-md-1">
                              <div>
                                <label title="Please enable this to record adjustment as Technical Loss" data-plugin="tippy" data-tippy-arrow="true" data-tippy-animation="fade">Tech Loss?</label>
                              </div>
                              <div>
                                <label class="switches">
                                  <input name="technical_loss_reg" id="technical_loss_reg" type="checkbox" >
                                  <span class="slider round"></span>
                                </label>
                              </div>
                            </div>

                            <div class="form-group  col-md-4">
                                <label>Amount: <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="amount" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" id="amount" placeholder="Amount" value="" required>
                            </div>
                            <div class="form-group  multiplecustom col-md-4">
                                <label>Reason: <span class="text-danger">*</span></label>
                                <select class="customselect" name="reason" id="reason" data-plugin="customselect">
                                    <option value="" selected="">Select</option>
                                    @foreach($action_reasons as $reasons)
                                    <option value="{{ $reasons->ACTIONS_REASONS_ID }}">{{  $reasons->ACTIONS_REASON  }}</option>
                                  @endForeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row"></div>
                                <div class="form-group">
                                    <label>Comments : </label>
                                    <textarea class="form-control" name="comment" id="comment" rows="10" placeholder=""></textarea>
                                </div>
                            </div>
                            <div class="col-md-6" >
                              <div class="form-group" id="usercoins">
                                <label>
                                  <strong>Current Balance:</strong>
                              </label>
                              <table class="table table-centered table-striped dt-responsive nowrap w-100 border blureffect" id="AdjustPointTable">
                                <thead>
                                  <tr>
                                      <th >Coin Type</th>
                                      <th >Deposit</th>
                                      <th >Promo</th>
                                      <th >Win</th>
                                      <th >Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr data-coin-type-id="1">
                                    <td data-coin-id="1">Cash</td>
                                    <td data-balance-type="USER_DEPOSIT_BALANCE"></td>
                                    <td data-balance-type="USER_PROMO_BALANCE"></td>
                                    <td data-balance-type="USER_WIN_BALANCE"></td>
                                    <td data-balance-type="USER_TOT_BALANCE"></td>
                                  </tr>
                                  <tr data-coin-type-id="3">
                                    <td>FPP Bonus	</td>
                                    <td data-balance-type="USER_DEPOSIT_BALANCE"></td>
                                    <td data-balance-type="USER_PROMO_BALANCE"></td>
                                    <td data-balance-type="USER_WIN_BALANCE"></td>
                                    <td data-balance-type="USER_TOT_BALANCE"></td>
                                  </tr>
                                  <tr data-coin-type-id="12">
                                    <td>Reward Points	</td>
                                    <td data-balance-type="USER_DEPOSIT_BALANCE"></td>
                                    <td data-balance-type="USER_PROMO_BALANCE"></td>
                                    <td data-balance-type="USER_WIN_BALANCE"></td>
                                    <td data-balance-type="USER_TOT_BALANCE"></td>
                                  </tr>
                                  <tr data-coin-type-id="16">
                                    <td>Poker Commission	</td>
                                    <td data-balance-type="USER_DEPOSIT_BALANCE"></td>
                                    <td data-balance-type="USER_PROMO_BALANCE"></td>
                                    <td data-balance-type="USER_WIN_BALANCE"></td>
                                    <td data-balance-type="USER_TOT_BALANCE"></td>
                                  </tr>
                                  <tr>
                                    <td colspan="5"> Affiliate : <span data-partner-name></span></td>
                                  </tr>
                                </tbody>
                            </table>
                              </div>
                            </div>
                        </div>

                        <button type="button" class="btn btn-primary waves-effect waves-light" id="singleAdjustSubmit" data-plugin="tippy" data-tippy-interactive="true" data-tippy="" data-original-title="View">
                            <i class="fa fa-save mr-1"></i> Adjust
                        </button>
                        <a href="" id="reset-btn" class="btn btn-secondary waves-effect waves-light ml-1">
                            <i class="mdi mdi-replay mr-1"></i>Reset      
                        </a>
                    </form>
                </div>
                 
                    <div class="tab-pane show" id="profile" style="min-height: 300px;">
                    <form id="adjustmentExcelImport" method="post" enctype="multipart/form-data">
                      <div class="row ">
                        <div class="form-group col-md-8">
                          <input type="file" class="form-control" name="adjustExcel" id="adjustExcel" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required>
                        </div>
                        <div class="col-md-4 text-md-center text-sm-left">
                          <a title="Click here to download sample format of adjust coins excel upload" href="{{ asset('/data/bo/sample-adjust-users-coins.xlsx') }}">
                          <button type="button" class="btn btn-success waves-effect waves-light d-inline-block">
                            <i class="fas fa-download  mr-3"></i> Download Sample Format
                          </button></a>
                        </div>
                      </div>
                      <p class="formtext mb-3">Note: Please use sample format file only, for bulk adjustment.</p>
                      <button type="button" name="importAdjustExcel" id="importAdjustExcel" class="btn btn-primary waves-effect waves-light" data-plugin="tippy" data-tippy-interactive="true" data-tippy="" data-original-title="View">
                        <i class="fa fa-save mr-1"></i> Adjust
                      </button>
                      <button style="display:none" if class="btn btn-primary formActionButton mr-1" id="show-import-excel" type="button" disabled="disabled">
                        <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                        Loading...
                      </button>
                      <a href="" id="import-excel-reset" class="btn btn-secondary waves-effect waves-light ml-1">
                        <i class="mdi mdi-replay mr-1"></i> Reset
                              
                      </a>
                    </form>
                    </div>
                    
                  </div>
                  </div>
                  <!-- end card-body-->
                </div>
              </div>
              <!-- end col -->
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="adjustmentcontenttable" data-backdrop="static">
        <div class="modal-dialog  modal-xl ">
          <div class="modal-content bg-transparent shadow-none">
            <div class="card">
              <div class="card-header bg-dark py-3 text-white">
                <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                    <i class="mdi mdi-close"></i>
                  </a>
                </div>
                <h5 class="card-title mb-0 text-white font-18">Confirmation</h5>
              </div>
              <form id="confirmUpdateExcelUpload" method="post">
              <div class="card-body">
                <table id="basic-datatable" class="basic-datatable table dt-responsive nowrap w-100">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Username</th>
                      <th>Balance type</th>
                      <th>Adjust type</th>
                      <th>Coin Type</th>
                      <th>Amount</th>
                      <th>Comments</th>
                      <th>Reason</th>
                      <th>Tech Loss</th>
                      <th>Status Review</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
                <div class="row mt-2">
                  <div class="form-group col-md-5 ">
                    <label>Transaction password:</label>
                    <input type="password" class="form-control" name="tpassword" id="tpassword"  value="">
                    </div>
                  </div>
                  <a href="#" data-dismiss="modal" class="btn btn-warning waves-effect waves-light mr-1 ViewModelSubmitBtn"><i class="fa fa-upload mr-1"></i> Reupload</a>

                  <button type="button" name="confirmExcelUpload" id="confirmExcelUpload" class="btn btn-success waves-effect waves-light formActionButtonvv ViewModelSubmitBtn"><i class="fa fa-save mr-1"></i> Continue Anyway</button>

                  <button style="display:none" if class="btn btn-primary formActionButton mr-1" id="confirm-hide-excel" type="button" disabled="disabled">
                    <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                    Loading...
                  </button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="aadharcardpopup" data-backdrop="static">
        <div class="modal-dialog modal-lg">
          <div class="modal-content bg-transparent shadow-none">
            <div class="card">
              <div class="card-header bg-dark py-3 text-white">
                <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                    <i class="mdi mdi-close"></i>
                  </a>
                </div>
                <h5 class="card-title mb-0 text-white font-18">Please Confirm</h5>
              </div>
              <div class="card-body font-14">
              <form method="POST" id="updateAdjustCoinData">{{ csrf_field() }}
                  <div class="row" id="fillFullName">
                    <div class="col-md-4">
                      <strong>Username :</strong> <span id="username1"></span>
                    </div>
                    <div class="col-md-4">
                      <strong>User ID :</strong> <span data-user-id> </span>
                      <input type="hidden" name="user_id" data-user-id-value value="">
                    </div>
                    <div class="col-md-4">
                    <strong>Full Name :</strong> <span data-user-fullname></span>
                    <input type="hidden" name="adjust_balance_type" id="adjust_balance_type" value="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <strong>Balance :</strong> <span id="amount1"></span>
                      <input type="hidden" name="adjust_coin" id="adjust_coin" value="">
                    </div>
                    <div class="col-md-4">
                      <strong>Coint Type  :</strong> <span id="coin_type1"></span>
                      <input type="hidden" name="adjust_coin_type" id="adjust_coin_type" value="">
                    </div>
                    <div class="col-md-4">
                      <strong>Adjustment :</strong> <span id="adjust_type1"></span>
                      <input type="hidden" name="coin_adjust_type" id="coin_adjust_type" value="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <strong>Reason :</strong> <span id="reason1"></span>
                      <input type="hidden" name="adjust_coin_reason" id="adjust_coin_reason" value="">
                      <input type="hidden" name="PARTNER_ID" id="PARTNER_ID" value="">
                      <input type="hidden" name="technical_loss_register" id="technical_loss_register" value="">
                    </div>
                    <div class="col-md-8">
                      <strong>Comments  :</strong><span id="comment1"></span>
                      <input type="hidden" name="adjust_coin_comment" id="adjust_coin_comment" value="">
                    </div>
                  </div>
                  <div class="row mt-2">
                    <div class="form-group col-md-5 ">
                      <label>Transaction password:</label>
                      <input type="password" class="form-control" name="transactionpassword" id="transactionpassword"  value="" required>
                    </div>
                  </div>
                  <button type="button" id="confirm" class="btn btn-primary waves-effect waves-light">
                    <i class="fa fa-save mr-1"></i> Confirm         
                  </button>
                  <button style="display:none" class="btn btn-primary formActionButton mr-1" type="button" disabled="disabled">
                    <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                    Loading...
                  </button>
                  <a href="#" class="btn btn-secondary waves-effect waves-light ml-1" data-dismiss="modal">
                    <i class="mdi mdi-replay mr-1"></i> Cancel          
                  </a>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    @endsection
@section('post_js')
  <script src="{{ asset('js/bo/adjust_coins.js') }}"></script>
@endsection


