@extends('bo.layouts.master')
@section('title', "Add User | PB BO")
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="">Responsible Gaming </a></li>
                     <li class="breadcrumb-item active">Add User</li>
                  </ol>
               </div>
               <h4 class="page-title">Add User</h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <form id="adduserForm" name="adduserForm" action="" method="post">
                     @csrf
                     <div class="row mb-1">
                        <div class="form-group col-md-6">
                           <label class="col-form-label">Username</label>
                           <input type="text" name="username" id="username" class="form-control @error('username') error @enderror" placeholder="Username" value="{{ old('username') }}">
                           @error('username') <label id="username-error" class="error" for="username">{{ $message }}</label> @enderror
                        </div>
                     </div>
                     <button type="submit" id="btnSubmit"  class="btn btn-success waves-effect waves-light mr-1 "><i class="fa fa-save mr-1"></i> Save</button>
                     <button type="reset" class="btn btn-secondary waves-effect waves-light mr-1"><i class="fas fa-eraser mr-1"></i> Clear</button>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
            <!-- end card-->
            <div class="mb-5 clearfix"></div>
            <div class="mb-5 clearfix"></div>
            <div class="mb-5 "></div>
         </div>
         <!-- end col -->
      </div>
   </div>
</div>
@endsection
@section('post_js')
<script src="{{ asset('js/bo/responsiblegaming.js') }}"></script>
@endsection
