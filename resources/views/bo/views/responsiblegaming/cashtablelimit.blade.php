@extends('bo.layouts.master')
@section('title', "Cash Table Limit | PB BO")
@section('pre_css')
<!-- Plugins css -->
<link href="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    .multiselecttextoverflow  .dropdown-menu.show .dropdown-item{ white-space: normal; }
    .formtext {
        font-size: 11px;
        font-style: italic;
        color: 
        #999198;
        padding: 6px 0 0 0;
        margin: 0;
        line-height: 13px;
        display: none;
    }
</style>
<style type="text/css">
   .bootstrap-select  .dropdown-menu.inner.show{height: 200px; overflow-y: scroll !important;}
   .mandetory{ color:red;font-weight: bold; }
 </style>
@endsection
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="">Responsible Gaming </a></li>
                     <li class="breadcrumb-item active">Cash Table Limit</li>
                  </ol>
               </div>
               <h4 class="page-title">
                  Cash Table Limit
               </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <form id="userSearchForm" action="" method="post">
                     @csrf
                     <div class="row  align-items-center">
                        <div class="form-group col-md-3">
                           <label>Username:</label>
                           <input name="username" type="text" class="form-control"  placeholder="Search by Username" value="{{ old('username', $params['username'] ?? '') }}">
                        </div>
                        <div class="form-group col-md-3 multiplecustom">
                           <label>BB Limit: </label>
                           <select name="cashlimit[]" class="form-control selectpicker" multiple data-style="btn-light">
                              <option value="0.50"{{ !empty($params['cashlimit']) ? in_array('0.50', $params['cashlimit']) ? "selected" : ''  : ''  }}>0.25/0.5</option>
                              <option value="1.00"{{ !empty($params['cashlimit']) ? in_array('1.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>0.5/1</option>
                              <option value="2.00"{{ !empty($params['cashlimit']) ? in_array('2.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>1/2</option>
                              <option value="4.00"{{ !empty($params['cashlimit']) ? in_array('4.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>2/4</option>
                              <option value="5.00"{{ !empty($params['cashlimit']) ? in_array('5.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>2/5</option>
                              <option value="6.00"{{ !empty($params['cashlimit']) ? in_array('6.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>3/6</option>
                              <option value="8.00"{{ !empty($params['cashlimit']) ? in_array('8.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>4/8</option>
                              <option value="10.00"{{ !empty($params['cashlimit']) ? in_array('10.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>5/10</option>
                              <option value="20.00"{{ !empty($params['cashlimit']) ? in_array('20.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>10/20</option>
                              <option value="25.00"{{ !empty($params['cashlimit']) ? in_array('25.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>10/25</option>
                              <option value="30.00"{{ !empty($params['cashlimit']) ? in_array('30.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>15/30</option>
                              <option value="40.00"{{ !empty($params['cashlimit']) ? in_array('40.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>20/40</option>
                              <option value="50.00"{{ !empty($params['cashlimit']) ? in_array('50.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>25/50</option>
                              <option value="60.00"{{ !empty($params['cashlimit']) ? in_array('60.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>30/60</option>
                              <option value="100.00"{{ !empty($params['cashlimit']) ? in_array('100.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>50/100</option>
                              <option value="150.00"{{ !empty($params['cashlimit']) ? in_array('150.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>75/150</option>
                              <option value="200.00"{{ !empty($params['cashlimit']) ? in_array('200.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>100/200</option>
                              <option value="300.00"{{ !empty($params['cashlimit']) ? in_array('300.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>150/300</option>
                              <option value="400.00"{{ !empty($params['cashlimit']) ? in_array('400.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>200/400</option>
                              <option value="500.00"{{ !empty($params['cashlimit']) ? in_array('500.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>250/500</option>
                              <option value="600.00"{{ !empty($params['cashlimit']) ? in_array('600.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>300/600</option>
                              <option value="1000.00"{{ !empty($params['cashlimit']) ? in_array('1000.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>500/1000</option>
                              <option value="2000.00"{{ !empty($params['cashlimit']) ? in_array('2000.00', $params['cashlimit']) ? "selected" : ''  : ''  }}>1000/2000</option>
                           </select>
                        </div>
                        <div class="form-group col-md-3">
                           <label>Status: </label>
                           <select name="status" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value=""{{ old('status', $params['status'] ?? '')==""?'selected':'' }}>Select</option>
                              <option value="1"{{ old('status', $params['status'] ?? '')==1?'selected':'' }}>Enabled</option>
                              <option value="0"{{ (old('status', $params['status'] ?? '')==0 && old('status', $params['status'] ?? '')!='')?'selected':'' }}>Disable</option>
                           </select>
                        </div>
                        <div class="form-group col-md-3">
                           <label>Updated By: </label>
                           <select name="updatedBy" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value=""{{ old('updatedBy', $params['updatedBy'] ?? '')==""?'selected':'' }}>All</option>
                              <option value="SELF" {{ old('updatedBy', $params['updatedBy'] ?? '')=="SELF"?'selected':'' }}>Self</option>
                              <option value="Admin" {{ old('updatedBy', $params['updatedBy'] ?? '')=="Admin"?'selected':'' }}>Admin</option>
                           </select>
                        </div>
                     </div>
                     <div class="row  customDatePickerWrapper">
                        <div class="form-group col-md-3">
                           <label>Search By: </label>
                           <select name="searchbydate" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="Start Date" {{ old('searchbydate', $params['searchbydate'] ?? '')=="Start Date"?'selected':'' }}>Start Date</option>
                              <option value="Update Date" {{ old('searchbydate', $params['searchbydate'] ?? '')=="Update Date"?'selected':'' }}>Updated date</option>
                           </select>
                        </div>
                        <div class="form-group col-md-3">
                           <label>From: </label>
                           <input name="date_from" id="date_from" type="text" class="form-control customDatePicker from flatpickr-input" placeholder="Select From Date" value="{{ old('date_from', $params['date_from'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-3">
                           <label>To: </label>
                           <input name="date_to" id="date_to" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="{{ old('date_to', $params['date_to'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-3">
                           <label>Date Range: </label>
                           <select name="date_range" id="date_range"  class="formatedDateRange customselect" data-plugin="customselect" >
                              <option data-default-selected="" value="" {{ !empty($params) ? $params['date_range'] == "" ? "selected" : '' :  'selected'  }}>Select Date Range</option>
                              <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                              <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                              <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                              <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                              <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                              <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                           </select>
                        </div>
                     </div>
                     <button type="submit" id="cash_limit_submit_form_search" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                     <a href="" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
         </div>
         <!-- end col -->
      </div>
      @if($cashData)
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-dark text-white">
                  <div class="card-widgets">
                     <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>                                 
                  </div>
                  <h5 class="card-title mb-0 text-white">Manage Cash Table Limit</h5>
               </div>
               <div id="cardCollpase7" class="collapse show">
                  <div class="card-body">
                     <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Username</th>
                              <th>Status</th>
                              <th>Max BB Limit</th>
                              <th>Limit Set On</th>
                              <th>Updated By</th>
                              <th>Updated On</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @php $autoId = 1 @endphp
                           @foreach($cashData as $cashresult)
                           <tr>
                              <td>{{ $autoId++ }}</td>
                              <td><a href="{{ route('user.account.user_profile', encrypt($cashresult->USER_ID)) }}" target="_blank">{{ getUsernameFromUserId($cashresult->USER_ID) }}</a></td>
                              <td>{!! $cashresult->CASH_ACTIVE==1?'<span class="badge bg-soft-success text-success shadow-none text-white">Enabled</span>':'<span class="badge bg-danger  shadow-none  text-white">Disabled</span>' !!}</td>
                              <td>{{ $cashresult->CASH_MAX_BIG_BLIND }}</td>
                              <td>{{ $cashresult->CASH_FROM_DATE?date('d/m/Y h:i:s A', strtotime($cashresult->CASH_FROM_DATE)):'' }}</td>
                              <td>{{ ucfirst($cashresult->CASH_UPDATED_BY) }}</td>
                              <td>{{ $cashresult->CASH_UPDATED_DATE?date('d/m/Y h:i:s A', strtotime($cashresult->CASH_UPDATED_DATE)):'' }}</td>
                              <td>
                                 <a href="#" onclick="return cashTableModelModel({{ $cashresult->RESPONSIBLE_GAME_SETTINGS_ID }},{{ $cashresult->USER_ID }});" class="action-icon font-18 tooltips float-left  ml-1"><i class="fa fa-edit"></i> <span class="tooltiptext">Change Status</span></a>

                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     <div class="customPaginationRender mt-2" data-form-id="#userSearchForm"  class="mt-2">
                        <div>More Records</div>
                        <div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
      @endif
   </div>
</div>
<div class="modal fade" id="cashtablelimitmodel" style="overflow: hidden;">
   <div class="modal-dialog modal-md">
      <div  id="getWayloader" style="display:none"> 
         <div class="d-flex justify-content-center hv_center">
               <div class="spinner-border" role="status"></div>
         </div>
      </div>
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Cash Table Limit Setting</h5>
            </div>
            <form name="cashtablelimitmodelform" id="cashtablelimitmodelform">
               @csrf
               <div class="card-body" >
                  <div class="form-group ">
                     <label>Status: </label>
                     <select name="model_status" id="model_status" class="form-control pokerbreakstatus">
                        <option value="">Select Status</option>
                        <option value="1">Enable</option>
                        <option value="0">Disable</option>
                     </select>
                  </div>
                  <input type="hidden" name="model_userid" id="model_userid" value="">
                  <input type="hidden" name="old_cash_limit" id="old_cash_limit" value="">
                  <div class="form-group  pbsvalue1 " style='display:none;'>
                     <label>Cash Table Limit: </label>
                     <select class="form-control" name="cash_table_limit" id="cash_table_limit">
                        <option value="">Select Max Stakes</option>
                        <option value="0.50">0.25/0.5</option>
                        <option value="1.00">0.5/1</option>
                        <option value="2.00">1/2</option>
                        <option value="4.00">2/4</option>
                        <option value="5.00">2/5</option>
                        <option value="6.00">3/6</option>
                        <option value="8.00">4/8</option>
                        <option value="10.00">5/10</option>
                        <option value="20.00">10/20</option>
                        <option value="25.00">10/25</option>
                        <option value="30.00">15/30</option>
                        <option value="40.00">20/40</option>
                        <option value="50.00">25/50</option>
                        <option value="60.00">30/60</option>
                        <option value="100.00">50/100</option>
                        <option value="150.00">75/150</option>
                        <option value="200.00">100/200</option>
                        <option value="300.00">150/300</option>
                        <option value="400.00">200/400</option>
                        <option value="500.00">250/500</option>
                        <option value="600.00">300/600</option>
                        <option value="1000.00">500/1000</option>
                        <option value="2000.00">1000/2000</option>
                     </select>
                  </div>
                  <button type="submit" id="btnSubmit"  class="btn btn-success waves-effect waves-light  mr "><i class="fa fa-save mr-1"></i> Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@section('post_js')
<script src="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script>
   $('.pokerbreakstatus').on('change', function () {
      $(".pbsvalue1").css('display', (this.value == '1') ? 'block' : 'none');
   });
</script>
<script src="{{ asset('js/bo/responsiblegaming.js') }}"></script>
@endsection

