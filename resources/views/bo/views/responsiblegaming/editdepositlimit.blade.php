@extends('bo.layouts.master')
@section('title', "Edit Deposit Limit | PB BO")
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="">Deposit Limit</a></li>
                     <li class="breadcrumb-item active">Edit </li>
                  </ol>
               </div>
               <h4 class="page-title">
                  Edit Deposit Limit
               </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <div class="row">
                     <div class="col-md-4 mb-1">
                        <table class="b-0 w-100 d-inline">
                           <tr>
                              <td class="py-1 text-black"><strong>User Name</strong></td>
                              <td class="p-1 text-black">:</td>
                              <td class="p-1 text-black-50">{{ $deposit_limit_user_data->username }}</td>
                           </tr>
                           <tr>
                              <td class="py-1 text-black"><strong>Dep Level</strong></td>
                              <td class="p-1 text-black">:</td>
                              <td class="p-1 text-black-50">{{ $deposit_limit_user_data->dep_level }}</td>
                           </tr>
                        </table>
                     </div>
                     <div class="col-md-4 mb-1">
                        <table class="b-0 w-100 d-inline">
                           <tr>
                              <td class="py-1 text-black"><strong>Created On </strong></td>
                              <td class="p-1 text-black">:</td>
                              <td class="p-1 text-black-50">{{ $deposit_limit_user_data->created_date }}</td>
                           </tr>
                           <tr>
                              <td class="py-1 text-black"><strong>Created By </strong></td>
                              <td class="p-1 text-black">:</td>
                              <td class="p-1 text-black-50">{{ $deposit_limit_user_data->created_by }}</td>
                           </tr>
                        </table>
                     </div>
                     <div class="col-md-4 mb-1">
                        <table class="b-0 w-100 d-inline">
                           <tr>
                              <td class="py-1 text-black"><strong>Updated On </strong></td>
                              <td class="p-1 text-black">:</td>
                              <td class="p-1 text-black-50">{{ $deposit_limit_user_data->updated_on }}</td>
                           </tr>
                           <tr>
                              <td class="py-1 text-black"><strong>Updated By </strong></td>
                              <td class="p-1 text-black">:</td>
                              <td class="p-1 text-black-50">{{ $deposit_limit_user_data->updated_by }}</td>
                           </tr>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
            <div class="card">
               <div class="card-body">
                  <form id="updateDepositLimit" action="{{ route("responsiblegaming.depositlimit.updateDepositLimit",encrypt($deposit_limit_user_data->USER_ID)) }}" method="post">
                     {{ csrf_field() }}
                     <div class="row">
                        <div class="col-md-4">
                           <div class="row  align-items-center">
                              <div class="form-group col-md-12">
                                 <label>Transaction Limit:  </label>
                                 <input type="text" class="form-control @error('deposit_amount') error @enderror" name="deposit_amount" id="deposit_amount" placeholder="Transaction Limit" value="{{ old('deposit_amount') ?? round($deposit_limit_user_data->dep_amount) }}">
                                 @error('deposit_amount') <label id="deposit_amount-error" class="error" for="deposit_amount">{{ $message }}</label> @enderror
                              </div>
                           </div>
                        </div>
                        <div class="col-md-8">
                           <div class="row  align-items-center">
                              @php 
                              $dailylimit = round($deposit_limit_user_data->dep_amount_per_day);
                              $weeklylimit = round($deposit_limit_user_data->dep_amount_per_week);
                              $monthlylimit = round($deposit_limit_user_data->dep_amount_per_month);
                              $transactionPerDay = $deposit_limit_user_data->transactions_per_day;
                              $transactionPerWeek = $deposit_limit_user_data->transactions_per_week;
                              $transactionPerMonth = $deposit_limit_user_data->transactions_per_month;

                              @endphp
                              <div class="form-group col-md-6">
                                 <label>Daily Limit:  </label>
                                 <input type="text" class="form-control @error('dep_amount_per_day') error @enderror" name="dep_amount_per_day" id="dep_amount_per_day" placeholder="Daily Limit" onchange="changeDepositDaily({{$dailylimit}},{{$weeklylimit}},{{$monthlylimit}})" value="{{ old('dep_amount_per_day') ?? $dailylimit }}">
                                 @error('dep_amount_per_day') <label id="dep_amount_per_day-error" class="error" for="dep_amount_per_day">{{ $message }}</label> @enderror
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Daily Count:  </label>
                                 <input type="text" class="form-control @error('no_of_transaction_per_day') error @enderror" name="no_of_transaction_per_day" id="no_of_transaction_per_day" placeholder="Daily Count" onchange="changeDepositCountDaily({{ $transactionPerDay }},{{$transactionPerWeek}},{{$transactionPerMonth}})" value="{{ old('no_of_transaction_per_day') ??  $transactionPerDay }}">
                                 @error('no_of_transaction_per_day') <label id="no_of_transaction_per_day-error" class="error" for="no_of_transaction_per_day">{{ $message }}</label> @enderror
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Weekly Limit:  </label>
                                 <input type="text" class="form-control @error('deposit_limit_per_week') error @enderror" name="deposit_limit_per_week" id="deposit_limit_per_week" placeholder="Weekly Count" onchange="changeDepositWeekly({{ $weeklylimit}},{{ $monthlylimit}})" value="{{ old('deposit_limit_per_week') ?? $weeklylimit }}">
                                 @error('deposit_limit_per_week') <label id="deposit_limit_per_week-error" class="error" for="deposit_limit_per_week">{{ $message }}</label> @enderror
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Weekly Count:  </label>
                                 <input type="text" class="form-control @error('no_of_transaction_per_week') error @enderror" name="no_of_transaction_per_week" id="no_of_transaction_per_week" placeholder="Weekly Limit" onchange="changeDepositCountWeekly({{$transactionPerWeek}},{{$transactionPerMonth}})" value="{{ old('no_of_transaction_per_week') ?? $transactionPerWeek }}">
                                 @error('no_of_transaction_per_week') <label id="no_of_transaction_per_week-error" class="error" for="no_of_transaction_per_week">{{ $message }}</label> @enderror
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Monthly Limit:  </label>
                                 <input type="text" class="form-control @error('deposit_limit_per_month') error @enderror" name="deposit_limit_per_month" id="deposit_limit_per_month" placeholder="Daily Count" value="{{ old('deposit_limit_per_month') ?? $monthlylimit }}">
                                 @error('deposit_limit_per_month') <label id="deposit_limit_per_month-error" class="error" for="deposit_limit_per_month">{{ $message }}</label> @enderror
                              </div>
                              <div class="form-group col-md-6">
                                 <label>Monthly Count:  </label>
                                 <input type="text" class="form-control @error('no_of_transaction_per_month') error @enderror" name="no_of_transaction_per_month" id="no_of_transaction_per_month" placeholder="Daily Limit" value="{{ old('no_of_transaction_per_month') ?? $transactionPerMonth }}">
                                 @error('no_of_transaction_per_month') <label id="no_of_transaction_per_month-error" class="error" for="no_of_transaction_per_month">{{ $message }}</label> @enderror
                              </div>
                           </div>
                        </div>
                     </div>
                     <button type="submit" class="btn btn-warning waves-effect waves-light"><i class="far fa-edit mr-1"></i> Update</button>
                     <a href="" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
         </div>
         <!-- end col -->
      </div>
   </div>
</div>
@endsection
@section('post_js')
<script src="{{ asset('js/bo/responsiblegaming.js') }}"></script>
@endsection

