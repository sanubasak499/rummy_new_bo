@extends('bo.layouts.master')
@section('title', "OFC LIMIT | PB BO")
@section('pre_css')
<!-- Plugins css -->
<link href="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    .multiselecttextoverflow  .dropdown-menu.show .dropdown-item{ white-space: normal; }
    .formtext {
        font-size: 11px;
        font-style: italic;
        color: 
        #999198;
        padding: 6px 0 0 0;
        margin: 0;
        line-height: 13px;
        display: none;
    }
</style>
<style type="text/css">
   .bootstrap-select  .dropdown-menu.inner.show{height: 200px; overflow-y: scroll !important;}
   .mandetory{ color:red;font-weight: bold; }
 </style>
@endsection
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="">Responsible Gaming </a></li>
                     <li class="breadcrumb-item active">OFC Limits</li>
                  </ol>
               </div>
               <h4 class="page-title">
                  OFC Limits
               </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <form id="userSearchForm" action="" method="post">
                     @csrf
                     <div class="row  align-items-center">
                        <div class="form-group col-md-3">
                           <label>Username:</label>
                           <input type="text" name="username" class="form-control"  placeholder="Search by Username" value="{{ old('username', $params['username'] ?? '') }}">
                        </div>
                        <div class="form-group col-md-3">
                           <label>Points: </label>
                           <select class="form-control selectpicker" name="ofclimit[]" multiple data-style="btn-light">
                              <option value="1"{{ !empty($params['ofclimit']) ? in_array('1', $params['ofclimit']) ? "selected" : ''  : ''  }}>1</option>
                              <option value="2"{{ !empty($params['ofclimit']) ? in_array('2', $params['ofclimit']) ? "selected" : ''  : ''  }}>2</option>
                              <option value="5"{{ !empty($params['ofclimit']) ? in_array('5', $params['ofclimit']) ? "selected" : ''  : ''  }}>5</option>
                              <option value="10"{{ !empty($params['ofclimit']) ? in_array('10', $params['ofclimit']) ? "selected" : ''  : ''  }}>10</option>
                              <option value="25"{{ !empty($params['ofclimit']) ? in_array('25', $params['ofclimit']) ? "selected" : ''  : ''  }}>25</option>
                              <option value="50"{{ !empty($params['ofclimit']) ? in_array('50', $params['ofclimit']) ? "selected" : ''  : ''  }}>50</option>
                              <option value="100"{{ !empty($params['ofclimit']) ? in_array('100', $params['ofclimit']) ? "selected" : ''  : ''  }}>100</option>
                              <option value="200"{{ !empty($params['ofclimit']) ? in_array('200', $params['ofclimit']) ? "selected" : ''  : ''  }}>200</option>
                              <option value="500"{{ !empty($params['ofclimit']) ? in_array('500', $params['ofclimit']) ? "selected" : ''  : ''  }}>500</option>
                              <option value="1000"{{ !empty($params['ofclimit']) ? in_array('1000', $params['ofclimit']) ? "selected" : ''  : ''  }}>1000</option>
                              <option value="2000"{{ !empty($params['ofclimit']) ? in_array('2000', $params['ofclimit']) ? "selected" : ''  : ''  }}>2000</option>
                           </select>
                        </div>
                        <div class="form-group col-md-3">
                           <label>Status: </label>
                           <select name="status" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value=""{{ old('status', $params['status'] ?? '')==""?'selected':'' }}>Select</option>
                              <option value="1"{{ old('status', $params['status'] ?? '')==1?'selected':'' }}>Enabled</option>
                              <option value="0"{{ (old('status', $params['status'] ?? '')==0 && old('status', $params['status'] ?? '')!='')?'selected':'' }}>Disable</option>
                           </select>
                        </div>
                        <div class="form-group col-md-3">
                           <label>Updated By: </label>
                           <select name="updatedBy" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value=""{{ old('updatedBy', $params['updatedBy'] ?? '')==""?'selected':'' }}>All</option>
                              <option value="SELF" {{ old('updatedBy', $params['updatedBy'] ?? '')=="SELF"?'selected':'' }}>Self</option>
                              <option value="Admin" {{ old('updatedBy', $params['updatedBy'] ?? '')=="Admin"?'selected':'' }}>Admin</option>
                           </select>
                        </div>
                     </div>
                     <div class="row  customDatePickerWrapper">
                        <div class="form-group col-md-3">
                           <label>Search By: </label>
                           <select name="searchbydate" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="Start Date" {{ old('searchbydate', $params['searchbydate'] ?? '')=="Start Date"?'selected':'' }}>Start Date</option>
                              <option value="Update Date" {{ old('searchbydate', $params['searchbydate'] ?? '')=="Update Date"?'selected':'' }}>Updated date</option>
                           </select>
                        </div>
                        <div class="form-group col-md-3">
                           <label>From: </label>
                           <input name="date_from" id="date_from" type="text" class="form-control customDatePicker from flatpickr-input" placeholder="Select From Date" value="{{ old('date_from', $params['date_from'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-3">
                           <label>To: </label>
                           <input name="date_to" id="date_to" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="{{ old('date_to', $params['date_to'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-3">
                           <label>Date Range: </label>
                           <select name="date_range" id="date_range"  class="formatedDateRange customselect" data-plugin="customselect" >
                              <option data-default-selected="" value="" {{ !empty($params) ? $params['date_range'] == "" ? "selected" : '' :  'selected'  }}>Select Date Range</option>
                              <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                              <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                              <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                              <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                              <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                              <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                           </select>
                        </div>
                     </div>
                     <button type="submit" id="ofc_limit_submit_form_search" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                     <a href="" class="btn btn-secondary waves-effect waves-light ml-1" id="formreset"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
         </div>
         <!-- end col -->
      </div>
      @if($ofcData)
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-dark text-white">
                  <div class="card-widgets">
                     <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>                                 
                  </div>
                  <h5 class="card-title mb-0 text-white">Manage OFC Limits</h5>
               </div>
               <div id="cardCollpase7" class="collapse show">
                  <div class="card-body">
                     <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Username</th>
                              <th>Status</th>
                              <th>Max Points</th>
                              <th>Limit Set On</th>
                              <th>Updated By</th>
                              <th>Updated On</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @php $autoId = 1 @endphp
                           @foreach($ofcData as $ofcresult)
                           <tr>
                              <td>{{ $autoId++ }}</td>
                              <td><a href="{{ route('user.account.user_profile', encrypt($ofcresult->USER_ID)) }}" target="_blank">{{ getUsernameFromUserId($ofcresult->USER_ID) }}</a></td>
                              <td>{!! $ofcresult->OFC_ACTIVE==1?'<span class="badge bg-soft-success text-success shadow-none text-white">Enabled</span>':'<span class="badge bg-danger  shadow-none  text-white">Disabled</span>' !!}</td>
                              <td>{{ $ofcresult->OFC_MAX_POINT }}</td>
                              <td>{{ $ofcresult->OFC_FROM_DATE?date('d/m/Y h:i:s A', strtotime($ofcresult->OFC_FROM_DATE)):'' }}</td>
                              <td>{{ ucfirst($ofcresult->OFC_UPDATED_BY) }}</td>
                              <td>{{ $ofcresult->OFC_UPDATED_DATE?date('d/m/Y h:i:s A', strtotime($ofcresult->OFC_UPDATED_DATE)):'' }}</td>
                              <td>
                                 <a href="#" onclick="return ofcTableModel({{ $ofcresult->RESPONSIBLE_GAME_SETTINGS_ID }},{{ $ofcresult->USER_ID }});" class="action-icon font-18 tooltips float-left  ml-1"><i class="fa-edit fa"></i> <span class="tooltiptext">Edit</span></a>
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     <div class="customPaginationRender mt-2" data-form-id="#userSearchForm"  class="mt-2">
                        <div>More Records</div>
                        <div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
      @endif
   </div>
</div>
<div class="modal fade" id="ofctablelimitmodel">
    <div class="modal-dialog modal-md">
      <div  id="getWayloader" style="display:none"> 
         <div class="d-flex justify-content-center hv_center">
               <div class="spinner-border" role="status"></div>
         </div>
      </div>
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">OFC Limit</h5>
            </div>
            <form name="ofctablelimitmodelform" id="ofctablelimitmodelform">
               <div class="card-body">
                  <div class="form-group ">
                     <label>Status: </label>
                     <select name="model_status" id="model_status" class="form-control pokerbreakstatus">
                        <option value="" selected="">Please Select</option>
                        <option value="1">Enable</option>
                        <option value="0">Disable</option>
                     </select>
                  </div>

                  <input type="hidden" name="model_userid" id="model_userid" value="">
                  <input type="hidden" name="old_ofc_limit" id="old_ofc_limit" value="">
                  <div class="form-group  pbsvalue1" style='display:none;'>
                     <label>Ofc Table Limit: </label>
                     <select class="form-control" name="ofc_table_limit" id="ofc_table_limit">
                        <option value="">Select Max Stakes</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                        <option value="500">500</option>
                        <option value="1000">1000</option>
                        <option value="2000">2000</option>
                     </select>
                  </div>
                  <button type="submit" id="btnSubmit"  class="btn btn-success waves-effect waves-light  mr "><i class="fa fa-save mr-1"></i> Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@section('post_js')
<script src="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script>
   $('.pokerbreakstatus').on('change', function () {
       $(".pbsvalue1").css('display', (this.value == '1') ? 'block' : 'none');
   });
</script>
<script src="{{ asset('js/bo/responsiblegaming.js') }}"></script>
@endsection

