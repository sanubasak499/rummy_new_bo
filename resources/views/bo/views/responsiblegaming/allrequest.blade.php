@extends('bo.layouts.master')
@section('title', "All Request | PB BO")
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="">Responsible Gaming </a></li>
                     <li class="breadcrumb-item active">All Requests</li>
                  </ol>
               </div>
               <h4 class="page-title">
                  All Requests
               </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <form id="userSearchForm" action="" method="post">
                     @csrf
                     <div class="row  align-items-center">
                        <div class="form-group col-md-4">
                           <label>Username:</label>
                           <input type="text" name="username" class="form-control"  placeholder="Search by Username" value="{{ old('username', $params['username'] ?? '') }}">
                        </div>
                        <div class="form-group col-md-4">
                           <label>Request For: </label>
                           <select name="requestfor" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="">Select</option>
                              <option value="DEPOSIT_LIMIT"{{ old('requestfor', $params['requestfor'] ?? '')=='DEPOSIT_LIMIT'?'selected':'' }}>Deposit Limit</option>
                              <option value="CASH"{{ old('requestfor', $params['requestfor'] ?? '')=='CASH'?'selected':'' }}>Cash limit</option>
                              <option value="OFC"{{ old('requestfor', $params['requestfor'] ?? '')=='OFC'?'selected':'' }}>OFC Limit</option>
                           </select>
                        </div>
                        <div class="form-group col-md-4">
                           <label>Status: </label>
                           <select name="status" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="">Select</option>
                              <option value="10"{{ old('status', $params['status'] ?? '')==10?'selected':'' }}>Reject</option>
                              <option value="11" {{ old('status', $params['status'] ?? '')==11?'selected':'' }}>Approve</option>
                              <option value="12" {{ old('status', $params['status'] ?? '')==12?'selected':'' }}>Pending</option>
                              <option value="13"{{ old('status', $params['status'] ?? '')==13?'selected':'' }}>AutoApprove</option>
                           </select>
                        </div>
                     </div>
                     <div class="row  customDatePickerWrapper">
                        <div class="form-group col-md-4">
                           <label>From: </label>
                           <input name="date_from" id="date_from" type="text" class="form-control customDatePicker from flatpickr-input" placeholder="Select From Date" value="{{ old('date_from', $params['date_from'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-4">
                           <label>To: </label>
                           <input name="date_to" id="date_to" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="{{ old('date_to', $params['date_to'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-4">
                           <label>Date Range: </label>
                           <select name="date_range" id="date_range"  class="formatedDateRange customselect" data-plugin="customselect" >
                              <option data-default-selected="" value="" {{ !empty($params) ? $params['date_range'] == "" ? "selected" : '' :  'selected'  }}>Select Date Range</option>
                              <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                              <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                              <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                              <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                              <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                              <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                           </select>
                        </div>
                     </div>
                     <button type="submit" id="request_submit_form_search" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                     <a href="" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
         </div>
         <!-- end col -->
      </div>
      @if($allrequest)
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-dark text-white">
                  <div class="card-widgets">
                     <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>                                 
                  </div>
                  <h5 class="card-title mb-0 text-white">View All Requests</h5>
               </div>
               <div id="cardCollpase7" class="collapse show">
                  <div class="card-body">
                     @if(count($allrequest) > 0)
                        <div class="row mb-2">
                           <div class="col-sm-12 mb-1">
                              <div class="text-sm-right">
                                 <a href="{{ route('responsiblegaming.getExcelExport') }}" class="btn btn-light mb-1"><i class="fas fa-file-excel" style="color: #34a853;"></i> Export Excel</a>
                              </div>
                           </div>
                        </div>
                     @endif
                     <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Username</th>
                              <th>Request For</th>
                              <th>Current Limit</th>
                              <th>Requested Limit </th>
                              <th>Reason</th>
                              <th>Requested Date</th>
                              <th>Status</th>
                              <th>Updated By</th>
                              <th>Updated On</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @php $autoId = 1 @endphp
                           @foreach($allrequest as $requestData)
                           <tr>
                              <td>{{ $autoId++ }}</td>
                              <td><a href="{{ route('user.account.user_profile', encrypt($requestData->USER_ID)) }}" target="_blank">{{ getUsernameFromUserId($requestData->USER_ID) }}</a></td>
                              <td>{{ $requestData->GAME_TYPE }}</td>
                              <td>{{ $requestData->CURRENT_LIMIT }}</td>
                              <td>{{ $requestData->REQUESTED_LIMIT }}</td>
                              <td>{{ $requestData->REASON }}</td>
                              <td>{{ date('d/m/Y h:i:s A', strtotime($requestData->REQUESTED_DATE))}}</td>
                              <td>{!! ($requestData->STATUS==11?'<span class="badge bg-soft-success text-success shadow-none">Approve</span>':($requestData->STATUS==12?'<span class="badge bg-warning text-white shadow-none">Pending</span>':($requestData->STATUS==13?'<span class="badge bg-soft-success text-success shadow-none">AutoApprove</span>':'<span class="badge bg-soft-danger text-danger shadow-none">Reject</span>'))) !!}</td>
                              <td>{{ ucfirst($requestData->STATUS_UPDATED_BY) }}</td>
                              <td>{{ date('d/m/Y h:i:s A', strtotime($requestData->STATUS_UPDATED_ON))}}</td>

                              {{-- <td>{!! $requestData->STATUS==12?'<a href="#" data-toggle="modal" data-target="#requestModelPopup" class="action-icon font-18 tooltips float-left  ml-1"><i class="fa fa-edit"></i> <span class="tooltiptext">Change Status</span></a>':'--' !!}
                              </td> --}}
                              <td>@if($requestData->STATUS==12)
                                 <a href="#" onclick="showrequestModel({{ $requestData->USER_ID }},{{ $requestData->REQUEST_ID }},{{ $requestData->STATUS }});" class="action-icon font-18 tooltips float-left  ml-1"><i class="fa fa-edit"></i> <span class="tooltiptext">Change Status</span>
                                 </a>
                                 @else
                                 {{ "--" }}
                                 @endif
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     <div class="customPaginationRender mt-2" data-form-id="#userSearchForm"  class="mt-2">
                        <div>More Records</div>
                        <div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
      @endif
   </div>
</div>
<div class="modal fade" id="requestModelPopup">
   <div class="modal-dialog modal-md">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Request Update</h5>
            </div>
            <form name="requestmodelform" id="requestmodelform">
               <div class="card-body">
                  <div class="form-group ">
                     <label>Status : </label>
                     <select name="model_status" id="model_status" class="form-control notificationhistory">
                        <option value="" selected="">Select </option>
                        <option value="10">Reject</option>
                        <option value="11">Approve</option>
                     </select>
                  </div>
                  <input type="hidden" name="model_userid" id="model_userid" value="">
                  <input type="hidden" name="request_id" id="request_id" value="">
                  <input type="hidden" name="old_status" id="old_status" vaalue="">
                  <div class="nhvalue1 " style='display:none;'>
                     <div class="form-group ">
                        <label>Reason: </label>
                        <select name="model_reasion" id="model_reasion" class="form-control pokerbreakstatus1">
                           <option value="" selected="">Select </option>
                           <option value="Modified the request with lesser limits">Modified the request with lesser limits</option>
                           <option value="Customer took/convinced for Poker break">Customer took/convinced for Poker break</option>
                           <option value="Did not increase under Responsible Gaming">Did not increase under Responsible Gaming</option>
                           <option value="Customer put it by mistake">Customer put it by mistake</option>
                           <option value="Customer not interested now">Customer not interested now</option>
                           <option value="Waiting for customer declaration">Waiting for customer declaration</option>
                           <option value="Customer refused to send declaration">Customer refused to send declaration</option>
                           <option value="Account Blocked">Account Blocked</option>
                        </select>
                     </div>
                  </div>
                  <button type="submit" id="btnSubmit"  class="btn btn-success waves-effect waves-light  mr "><i class="fa fa-save mr-1"></i> Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@section('post_js')
<script>
   $('.notificationhistory').on('change', function () {
       $(".nhvalue1").css('display', (this.value == '10') ? 'block' : 'none');
   });
</script>
<script src="{{ asset('js/bo/responsiblegaming.js') }}"></script>
@endsection

