@extends('bo.layouts.master')
@section('title', "Poker Break | PB BO")
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="">Responsible Gaming </a></li>
                     <li class="breadcrumb-item active">Poker Break</li>
                  </ol>
               </div>
               <h4 class="page-title">
                  Poker Break
               </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <form id="userSearchForm" action="" method="post">
                     @csrf
                     <div class="row  align-items-center">
                        <div class="form-group col-md-4">
                           <label>Username:</label>
                           <input type="text" name="username" class="form-control"  placeholder="Search by Username" value="{{ old('username', $params['username'] ?? '') }}">
                        </div>
                        <div class="form-group col-md-4">
                           <label>Status: </label>
                           <select name="status" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value=""{{ old('status', $params['status'] ?? '')==""?'selected':'' }}>Select</option>
                              <option value="1" {{ old('status', $params['status'] ?? '')==1?'selected':'' }}>Enabled</option>
                              <option value="0" {{ (old('status', $params['status'] ?? '')==0 && old('status', $params['status'] ?? '')!='')?'selected':'' }}>Disable</option>
                           </select>
                        </div>
                        <div class="form-group col-md-4">
                           <label>Updated By: </label>
                           <select name="updatedBy" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="" {{ old('updatedBy', $params['updatedBy'] ?? '')==""?'selected':'' }}>All</option>
                              <option value="SELF" {{ old('updatedBy', $params['updatedBy'] ?? '')=="SELF"?'selected':'' }}>Self</option>
                              <option value="Admin" {{ old('updatedBy', $params['updatedBy'] ?? '')=="Admin"?'selected':'' }}>Admin</option>
                           </select>
                        </div>
                     </div>
                     <div class="row  customDatePickerWrapper">
                        <div class="form-group col-md-3">
                           <label>Search By: </label>
                           <select name="searchbydate" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="Start Date" {{ old('searchbydate', $params['searchbydate'] ?? '')=="Start Date"?'selected':'' }}>Start Date</option>
                              <option value="End Date" {{ old('searchbydate', $params['searchbydate'] ?? '')=="End Date"?'selected':'' }}>End date</option>
                           </select>
                        </div>
                        <div class="form-group col-md-3">
                           <label>From: </label>
                           <input name="date_from" id="date_from" type="text" class="form-control customDatePicker from flatpickr-input" placeholder="Select From Date" value="{{ old('date_from', $params['date_from'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-3">
                           <label>To: </label>
                           <input name="date_to" id="date_to" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="{{ old('date_to', $params['date_to'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-3">
                           <label>Date Range: </label>
                           <select name="date_range" id="date_range"  class="formatedDateRange customselect" data-plugin="customselect" >
                              <option data-default-selected="" value="" {{ !empty($params) ? $params['date_range'] == "" ? "selected" : '' :  'selected'  }}>Select Date Range</option>
                              <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                              <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                              <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                              <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                              <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                              <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                           </select>
                        </div>
                     </div>
                     <button type="submit" id="submit_form_search" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                     <a href="" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
         </div>
         <!-- end col -->
      </div>
      @if($pokerbreakdata)
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-dark text-white">
                  <div class="card-widgets">
                     <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>                                 
                  </div>
                  <h5 class="card-title mb-0 text-white">Manage Poker Break</h5>
               </div>
               <div id="cardCollpase7" class="collapse show">
                  <div class="card-body">
                     @if(count($pokerbreakdata) > 0)
                        <div class="row mb-2">
                           <div class="col-sm-12 mb-1">
                              <div class="text-sm-right">
                                 <a href="{{ route('responsiblegaming.pokerbreak.getExcelExport') }}" class="btn btn-light mb-1"><i class="fas fa-file-excel" style="color: #34a853;"></i> Export Excel</a>
                              </div>
                           </div>
                        </div>
                     @endif
                     <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Username</th>
                              <th>P.B. Status</th>
                              <th>Break Form</th>
                              <th>Break Till</th>
                              <th>Updated By</th>
                              <th>Updated On</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @php $id = 1 @endphp
                           @foreach($pokerbreakdata as $pokerbreak)
                              <tr>
                                 <td>{{ $id++ }}</td>
                                 <td><a href="{{ route('user.account.user_profile', encrypt($pokerbreak->USER_ID)) }}" target="_blank">{{ getUsernameFromUserId($pokerbreak->USER_ID) }}</a></td>
                                 <td>{!! $pokerbreak->BREAK_ACTIVE==1?'<span class="badge bg-soft-success text-success shadow-none text-white">Enabled</span>':'<span class="badge bg-danger  shadow-none  text-white">Disabled</span>' !!}</td>
                                 <td>{{ $pokerbreak->BREAK_FROM_DATE?date('d/m/Y h:i:s A', strtotime($pokerbreak->BREAK_FROM_DATE)):'' }}</td>
                                 <td>{{ $pokerbreak->BREAK_TO_DATE?date('d/m/Y h:i:s A', strtotime($pokerbreak->BREAK_TO_DATE)):'' }}</td>
                                 <td>{{ ucfirst($pokerbreak->BREAK_UPDATED_BY) }}</td>
                                 <td>{{ $pokerbreak->BREAK_UPDATED_DATE?date('d/m/Y h:i:s A', strtotime($pokerbreak->BREAK_UPDATED_DATE)):'' }}</td>
                                 <td>
                                    <a href="#" onclick="return pokerbreakModel({{ $pokerbreak->RESPONSIBLE_GAME_SETTINGS_ID }});" class="action-icon font-18 tooltips float-left  ml-1"><i class="fa fa-edit"></i> <span class="tooltiptext">Change Status</span></a>
                                 </td>
                              </tr>
                           @endforeach
                        </tbody>
                     </table>
                     <div class="customPaginationRender mt-2" data-form-id="#userSearchForm"  class="mt-2">
                        {{-- <div>More Records</div> --}}
                        <div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
      @endif
   </div>
</div>
<div class="modal fade" id="setuserflagpopup">
   <div class="modal-dialog modal-md">
      <div  id="getWayloader" style="display:none"> 
         <div class="d-flex justify-content-center hv_center">
               <div class="spinner-border" role="status"></div>
         </div>
      </div>
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Poker Break Status</h5>
            </div>
            <form name="updatePokerBreak" id="updatePokerBreak" >
               @csrf
               <div class="card-body">
                  <div class="form-group ">
                     <label>Status: </label>
                     <select name="model_status" id="model_status" class="form-control pokerbreakstatus @error('model_status') error @enderror">
                        <option value="">Please Select</option>
                        <option value="1" >Enable</option>
                        <option value="0">Disable</option>
                     </select>
                     @error('model_status') <label id="model_status-error" class="error" for="model_status">{{ $message }}</label> @enderror
                  </div>

                  <div class="form-group pbsvalue1" style='display:none;'>
                     <div class="form-group ">
                        <label>Duration: </label>
                        <select name="model_duration" onchange="showTextDate(this.value)" id="model_duration" class="form-control pokerbreakstatus1 @error('model_duration') error @enderror">
                           <option value="">Select Duration</option>
                           <option value="1-w">1 Week</option>
                           <option value="2-w">2 Weeks</option>
                           <option value="1-m">1 Month</option>
                           <option value="3-m">3 Months</option>
                           <option value="6-m">6 Months</option>
                           <option value="1-y">1 Year</option>
                           <option value="custom">Custom</option>
                        </select>
                        @error('model_duration') <label id="model_duration-error" class="error" for="model_duration">{{ $message }}</label> @enderror
                        <div class="pbsvalue1-w mt-1" style='display:none;'></div>

                     </div>
                     <input type="hidden" name="model_duration_date" id="model_duration_date" value="">
                     <div class="form-group pbsvaluecustom " style='display:none;'>
                        <div class="row  customDatePickerWrapper">
                           <div class="form-group col-md-6">
                              <label>From: </label>
                              <input name="date_from_model" id="date_from_model" type="text" class="form-control customDatePicker from flatpickr-input" placeholder="Select From Date" value="" readonly="readonly">
                           </div>
                           <input type="hidden" name="model_userid" id="model_userid" value="">
                           <div class="form-group col-md-6">
                              <label>To: </label>
                              <input name="date_to_model" id="date_to_model" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="" readonly="readonly">
                           </div>
                        </div>
                     </div>
                  </div>

                  <button type="buttom" id="btnSubmit"  class="btn btn-success waves-effect waves-light  mr "><i class="fa fa-save mr-1"></i> Save</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@section('post_js')
<script>
   $('.pokerbreakstatus').on('change', function () {
      $("#model_duration option").removeAttr("selected");
       $(".pbsvalue1").css('display', (this.value == '1') ? 'block' : 'none');
   });
</script>
<script src="{{ asset('js/bo/responsiblegaming.js') }}"></script>
@endsection

