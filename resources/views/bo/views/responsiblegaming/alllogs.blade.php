@extends('bo.layouts.master')
@section('title', "All Logs | PB BO")
@section('pre_css')
<!-- Plugins css -->
<link href="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    .multiselecttextoverflow  .dropdown-menu.show .dropdown-item{ white-space: normal; }
    .formtext {
        font-size: 11px;
        font-style: italic;
        color: 
        #999198;
        padding: 6px 0 0 0;
        margin: 0;
        line-height: 13px;
        display: none;
    }
</style>
@endsection
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="">Responsible Gaming </a></li>
                     <li class="breadcrumb-item active">All Logs</li>
                  </ol>
               </div>
               <h4 class="page-title">
                  All Logs
               </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <form id="userSearchForm" action="" method="post">
                     @csrf
                     <div class="row  align-items-center">
                        <div class="form-group col-md-3">
                           <label>Username:</label>
                           <input type="text" name="username" class="form-control"  placeholder="Search by Username" value="{{ old('username', $params['username'] ?? '') }}">
                        </div>
                        <div class="form-group col-md-3 multiplecustom">
                           <label>Settings Type: </label>
                           <select class="form-control selectpicker" name="settingType[]" multiple data-style="btn-light">
                              <option data-default-selected="" value="DEPOSIT_LIMIT"{{ !empty($params['settingType']) ? in_array('DEPOSIT_LIMIT', $params['settingType']) ? "selected" : ''  : ''  }}>Deposit Limit</option>
                              <option value="CASH"{{ !empty($params['settingType']) ? in_array('CASH', $params['settingType']) ? "selected" : ''  : ''  }}>Cash Limit</option>
                              <option value="OFC"{{ !empty($params['settingType']) ? in_array('OFC', $params['settingType']) ? "selected" : ''  : ''  }}>OFC Limit</option>
                              <option value="POKER_BREAK"{{ !empty($params['settingType']) ? in_array('POKER_BREAK', $params['settingType']) ? "selected" : ''  : ''  }}>Poker Break</option>
                           </select>
                        </div>
                        <div class="form-group col-md-3">
                           <label>Action: </label>
                           <select name="action" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="">Select</option>
                              <option value="Increase" {{ old('action', $params['action'] ?? '')=='Increase'?'selected':'' }}>Increase</option>
                              <option value="Decrease" {{ old('action', $params['action'] ?? '')=='Decrease'?'selected':'' }}>Decrease</option>
                              <option value="PB_Start" {{ old('action', $params['action'] ?? '')=='PB_Start'?'selected':'' }}>PB_start</option>
                              <option value="PB_End" {{ old('action', $params['action'] ?? '')=='PB_End'?'selected':'' }}>PB_End</option>
                           </select>
                        </div>
                        <div class="form-group col-md-3">
                           <label>Status: </label>
                           <select name="status" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="">Select</option>                              
                              <option value="10"{{ old('action', $params['status'] ?? '')==10?'selected':'' }}>Reject</option>
                              <option value="11" {{ old('action', $params['status'] ?? '')==11?'selected':'' }}>Approve</option>
                              <option value="12" {{ old('action', $params['status'] ?? '')==12?'selected':'' }}>Pending</option>
                              <option value="13"{{ old('action', $params['status'] ?? '')==13?'selected':'' }}>AutoApprove</option>
                           </select>
                        </div>
                     </div>
                     <div class="row  customDatePickerWrapper">
                        <div class="form-group col-md-3">
                           <label>Search By: </label>
                           <select name="searchByDate" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="start_date" {{ old('searchByDate', $params['searchByDate'] ?? '')=='start_date'?'selected':'' }}>Start Date</option>
                              <option value="update_date" {{ old('searchByDate', $params['searchByDate'] ?? '')=='update_date'?'selected':'' }}>Updated date</option>
                           </select>
                        </div>
                        <div class="form-group col-md-3">
                           <label>From: </label>
                           <input name="date_from" id="date_from" type="text" class="form-control customDatePicker from flatpickr-input" placeholder="Select From Date" value="{{ old('date_from', $params['date_from'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-3">
                           <label>To: </label>
                           <input name="date_to" id="date_to" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="{{ old('date_to', $params['date_to'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-3">
                           <label>Date Range: </label>
                           <select name="date_range" id="date_range"  class="formatedDateRange customselect" data-plugin="customselect" >
                              <option data-default-selected="" value="" {{ !empty($params) ? $params['date_range'] == "" ? "selected" : '' :  'selected'  }}>Select Date Range</option>
                              <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                              <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                              <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                              <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                              <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                              <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                           </select>
                        </div>
                     </div>
                     <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                     <a href="" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
         </div>
         <!-- end col -->
      </div>
      @if($alllogs)
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-dark text-white">
                  <div class="card-widgets">
                     <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>                                 
                  </div>
                  <h5 class="card-title mb-0 text-white">View Logs</h5>
               </div>
               <div id="cardCollpase7" class="collapse show">
                  <div class="card-body">
                     <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Username</th>
                              <th>Setting Type</th>
                              <th>Action</th>
                              <th>Status</th>
                              <th>From Date</th>
                              <th>To Date</th>
                              <th>Value</th>
                              <th>Updated By</th>
                              <th>Updated On</th>
                           </tr>
                        </thead>
                        <tbody>
                           @php $autoId = 1 @endphp
                           @foreach($alllogs as $alllogsDate)
                           <tr>
                              <td>{{ $autoId++}}</td>
                              <td><a href="{{ route('user.account.user_profile', encrypt($alllogsDate->USER_ID)) }}" target="_blank">{{ getUsernameFromUserId($alllogsDate->USER_ID) }}</a></td>
                              <td>{{ $alllogsDate->RESPONSIBLE_SETTINGS_TYPE  }}</td>
                              <td>{{ $alllogsDate->ACTION  }}</td>
                              <td>{!! $alllogsDate->STATUS==1?'<span class="badge bg-soft-success text-success shadow-none">Active</span>':($alllogsDate->STATUS==10?'<span class="badge bg-soft-danger text-danger shadow-none">Reject</span>':($alllogsDate->STATUS==11?'<span class="badge bg-soft-success text-success shadow-none">Approve</span>':($alllogsDate->STATUS==12?'<span class="badge bg-warning text-white shadow-none">Pending</span>':($alllogsDate->STATUS==13?'<span class="badge bg-soft-success text-success shadow-none">AutoApprove</span>':'<span class="badge bg-soft-danger text-danger shadow-none">In-Active</span>')))) !!}</td>
                              <td>{{ date('d/m/Y h:i:s A', strtotime($alllogsDate->FROM_DATE))}}</td>
                              <td>{{ date('d/m/Y h:i:s A', strtotime($alllogsDate->TO_DATE)) }}</td>
                              <td>{{ $alllogsDate->VALUE }}</td>
                              <td>{{ ucfirst($alllogsDate->UPDATED_BY) }}</td>
                              <td>{{ date('d/m/Y h:i:s A', strtotime($alllogsDate->UPDATED_DATE)) }}</td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     <div class="customPaginationRender mt-2" data-form-id="#userSearchForm"  class="mt-2">
                        <div>More Records</div>
                        <div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
      @endif
   </div>
</div>
@endsection
@section('js')
<script src="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
@endsection
