
@extends('bo.layouts.master')
@section('title', "Manage Contest | PB BO")
@section('pre_css')
@endsection
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="{{ route('marketing.contest.index')}}">Marketing </a></li>
                  <li class="breadcrumb-item"><a href="{{ route('marketing.contest.index')}}">Contests </a></li>
                     <li class="breadcrumb-item active">Create Contest</li>
                  </ol>
               </div>
               <h4 class="page-title">
                  Create Contest
               </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <form name="userContestForm" id="userContestForm" action="#" method="post" enctype="multipart/form-data">
                     @csrf
                     <div class="row">
                        <div class="form-group col-md-4">
                           <label>Contest Name: <span class="text-danger">*</span></label>
                           <input type="text" name="contest_name" id="contest_name" class="form-control @error('contest_name') error @enderror"  placeholder="Contest Name" value="{{old('contest_name')}}">
                           @error('contest_name') <label id="contest_name-error" class="error" for="contest_name">{{ $message }}</label> @enderror
                        </div>
                        <div class="form-group col-md-4">
                           <label>Contest Title: <span class="text-danger">*</span></label>
                           <input type="text" class="form-control @error('contest_title') error @enderror" name="contest_title" id="contest_title" placeholder="Contest Title" value="{{old('contest_title')}}">
                           @error('contest_title') <label id="contest_title-error" class="error" for="contest_title">{{ $message }}</label> @enderror
                        </div>
                        <div class="form-group col-md-4">
                           <label>Contest Description: </label>
                           <input type="text" class="form-control @error('contest_description') error @enderror" name="contest_description" id="contest_description" placeholder="Contest Description" value="{{old('contest_description')}}">
                           @error('contest_description') <label id="contest_description-error" class="error" for="contest_description">{{ $message }}</label> @enderror
                        </div>
                     </div>
                     <div class="row">
                        <div class="form-group col-md-4">
                           <label>Video Link: <span class="text-danger">*</span></label>
                           <input type="text" name="contest_video_url" id="contest_video_url" class="form-control @error('contest_video_url') error @enderror" placeholder="Video Link" value="{{old('contest_video_url')}}">
                           @error('contest_video_url') <label id="contest_video_url-error" class="error" for="contest_video_url">{{ $message }}</label> @enderror
                        </div>
                        <div class="form-group col-md-4">
                           <label>Image: </label>
                           <input type="file" class="form-control @error('contest_image_link') error @enderror" name="contest_image_link" id="contest_image_link"  value="{{old('contest_image_link')}}" accept="image/*">
                           @error('contest_image_link') <label id="contest_image_link-error" class="error" for="contest_image_link">{{ $message }}</label> @enderror
                        </div>
                        <div class="form-group col-md-4">
                           <label>Video Description: </label>
                           <input type="text" class="form-control @error('contest_video_description') error @enderror" name="contest_video_description" id="contest_video_description" placeholder="Video Description" value="{{old('contest_video_description')}}">
                           @error('contest_video_description') <label id="contest_video_description-error" class="error" for="contest_video_description">{{ $message }}</label> @enderror
                        </div>
                     </div>
                     <div class="form-group">
                        <label>Terms & Conditions: </label>
                     <textarea class="form-control" rows="6" name="contest_terms_conditions" id="contest_terms_conditions" placeholder="Terms & Conditions">{{old('contest_terms_conditions')}}</textarea>
                     </div>
                     <button type="submit" name="create_contest_form" id="create_contest_form" class="btn btn-success waves-effect waves-light  "><i class="fa fa-save mr-1"></i> Submit</button>

                     <a href="" class="btn btn-dark waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
         </div>
         <!-- end col -->
      </div>
   </div>
</div>
@endsection
@section('post_js')
<script src="{{ asset('js/bo/manage_contest.js') }}"></script>
@endsection
