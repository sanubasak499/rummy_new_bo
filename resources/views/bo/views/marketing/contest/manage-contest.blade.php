@extends('bo.layouts.master')
@section('title', "Manage Contest | PB BO")
@section('pre_css')
<link href="{{ asset('assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="">Marketing </a></li>
                     <li class="breadcrumb-item"><a href="{{ route('marketing.contest.index') }}">Contest </a></li>
                     <li class="breadcrumb-item active">Manage Contest</li>
                  </ol>
               </div>
               <h4 class="page-title">
                  Manage Contest
               </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <div class="row mb-2">
                     <div class="col-lg-8">
                        <h5 class="font-18"> </h5>
                     </div>
                     <div class="col-lg-4">
                     <div class="text-lg-right mt-3 mt-lg-0"><a href="{{route('marketing.contest.createcontest')}}" class="btn btn-success waves-effect waves-light"><i class="fa fa-plus mr-1"></i>Create Contest</a></div>
                     </div>
                  </div>


                  <form id="userSearchForm" action="" method="post">
                     @csrf
                     <div class="row  align-items-center">
                        <div class="form-group col-md-4">
                           <label>Contest Name:</label>
                        <input type="text" class="form-control" name="contest_name" id="contest_name" placeholder="Search by Contest Name" value="{{ old('contest_name', $params['contest_name'] ?? '') }}">
                        </div>
                        <div class="form-group col-md-4">
                           <label>Contest Title:</label>
                           <input type="text" class="form-control" name="contest_title" id="contest_title" placeholder="Search by Contest Title" value="{{ old('contest_title', $params['contest_title'] ?? '') }}">
                        </div>
                        <div class="form-group col-md-4">
                           <label>Status: </label>
                           <select name="status" class="customselect" data-plugin="customselect">
                              <option data-default-selected="" value="" {{ old('status', $params['status'] ?? '')==""?'selected':'' }}>Select</option>
                              <option value="1" {{ old('status', $params['status'] ?? '')==1?'selected':'' }}>Active</option>
                              <option value="0" {{ (old('status', $params['status'] ?? '')==0 && old('status', $params['status'] ?? '')!='')?'selected':'' }}>Inactive</option>
                           </select>
                        </div>
                     </div>
                     <div class="row  customDatePickerWrapper">
                        <div class="form-group col-md-4">
                           <label>From: </label>
                           <input name="date_from" id="date_from" type="text" class="form-control customDatePicker from flatpickr-input" placeholder="Select From Date" value="{{ old('date_from', $params['date_from'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-4">
                           <label>To: </label>
                           <input name="date_to" id="date_to" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="{{ old('date_to', $params['date_to'] ?? '') }}" readonly="readonly">
                        </div>
                        <div class="form-group col-md-4">
                           <label>Date Range: </label>
                           <select name="date_range" id="date_range"  class="formatedDateRange customselect" data-plugin="customselect" >
                              <option data-default-selected="" value="" {{ !empty($params) ? $params['date_range'] == "" ? "selected" : '' :  'selected'  }}>Select Date Range</option>
                              <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                              <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                              <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                              <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                              <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                              <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                           </select>
                        </div>
                     </div>
                     <button type="submit" name="search_contest" id="search_contest" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                     <a href="" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                     {{-- <button type="reset" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</button> --}}
                  </form>
               </div>
               <!-- end card-body-->
            </div>
         </div>
         <!-- end col -->
      </div>
      @if($contest)
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-dark text-white">
                  <div class="card-widgets">
                     <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>                                 
                  </div>
                  <h5 class="card-title mb-0 text-white">Contests</h5>
               </div>
               <div id="cardCollpase7" class="collapse show">
                  <div class="card-body">
                     <table id="example" class="table dt-responsive nowrap w-100">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Title</th>
                              <th>Description</th>
                              <th>Video Link</th>
                              <th>Thumbnail</th>
                              <th>Status</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @php $count = 1; @endphp
                           @foreach($contest as $marketing_contest)
                           <tr>
                              <td>{{$count++}}</td>
                              <td>{{ $marketing_contest['CONTEST_NAME'] }}</td>
                              <td>{{ $marketing_contest['CONTEST_TITLE'] }}</td>
                              <td>{{ $marketing_contest['CONTEST_DESCRIPTION'] }}</td>

                              {{-- <td><a class="popup-youtube" href="{{ $marketing_contest['CONTEST_VIDEO_URL'] }}">{{ $marketing_contest['CONTEST_VIDEO_URL'] }}</a></td> --}}

                              <td><a href="#myModal" id="youtube_video_model{{$marketing_contest['CONTEST_ID']}}" onclick="return myfunction({{$marketing_contest['CONTEST_ID']}})" class="popup-youtube" data-toggle="modal" src="{{ $marketing_contest['CONTEST_VIDEO_URL'] }}">{{ $marketing_contest['CONTEST_VIDEO_URL'] }}</a></td>

                              <td><a href="{{ $marketing_contest['CONTEST_IMAGE_LINK'] }}" class="image-popup" title="{{ $marketing_contest['CONTEST_NAME'] }}"><img src="{{ $marketing_contest['CONTEST_IMAGE_LINK'] }}" width="70" class="img-fluid"  alt="work-thumbnail"/></a>
                              </td>
                              
                              <td><?php if($marketing_contest['CONTEST_STATUS']==1){ ?>
                                 <span class="badge bg-soft-success text-success shadow-none" style="cursor: pointer;" onclick="return changeStatus({{$marketing_contest['CONTEST_ID']}},`Are you sure want to Inactive this contest?`,0);">Active</span> 
                                 <?php }else{ ?>
                                    <span class="badge bg-soft-danger text-danger shadow-none" style="cursor: pointer;" onclick="return changeStatus({{$marketing_contest['CONTEST_ID']}},`Are you sure want to Active this contest?`,1);">Inactive</span>
                                 <?php } ?>
                              </td>
                              <td>
                                 <a href="#"  class="action-icon font-18 tooltips" data-toggle="modal" data-target="#setuserflagpopup" onclick="checkContest({{$marketing_contest['CONTEST_ID']}});"><i class="fa-eye fa"></i> <span class="tooltiptext">View</span></a>
                                 <a href="{{ route('marketing.contest.edit_contest', encrypt($marketing_contest['CONTEST_ID'])) }}" class="action-icon font-18 tooltips"><i class="fa-edit fa"></i> <span class="tooltiptext">Edit</span></a>
                                 
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     <div class="customPaginationRender mt-2" data-form-id="#userSearchForm"  class="mt-2">
                     </div>
                  </div>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
      @endif
   </div>
</div>

<div class="bs-example">
   
   <!-- Modal HTML -->
   <div id="myModal" class="modal fade">
       <div class="modal-dialog">
         <i class="mdi mdi-close"></i>
           <div class="modal-content">
               <div class="modal-body">
                   <iframe id="embedvideo" width="100%" height="315" src="" frameborder="0" allowfullscreen></iframe>
               </div>
           </div>
       </div>
   </div>
</div>     

<div class="modal fade" id="setuserflagpopup">
   <div class="modal-dialog modal-lg">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">View Contest Name</h5>
            </div>
            <div class="card-body">
               <table class="border-0 w-100 mytabless">
                  <div id="managecontestdata" class="loader d-flex justify-content-center hv_center">
                     <div class="spinner-border" role="status"></div>
                 </div>
                  <tr>
                     <td width="35%"> <strong class="font-15">Contest Title:</strong></td>
                     <td width="65%" id="model_contest_title"></td>
                  </tr>
                  <tr>
                     <td width="35%"> <strong class="font-15">Contest Description:</strong></td>
                     <td width="65%" id="model_contest_desc"></td>
                  </tr>
                  <tr>
                     <td width="35%"> <strong class="font-15">Video Link:</strong></td>
                     <td width="65%">
                     {{-- <a id="model_video_link" class="popup-youtube" href="" data-dismiss="modal" data-target="#setuserflagpopup"></a> --}}
                     <a href="#myModal" id="model_video_link" onclick="return myfunctionvideo()" class="popup-youtube" src="" data-dismiss="modal" data-toggle="modal"></a>
                     </td>
                  </tr>
                  <!--<tr>
                     <td width="35%"> <strong class="font-15">Video Thumbnail:</strong></td>
                     <td width="65%"><img src="https://www.pokerbaazi.com/blog/media/k2/items/cache/ffb67c0cbdf3cc4dd2a13b69ce367cd4_L.jpg" width="70" class="img-fluid"  /></td>
                  </tr>--->
                  <tr>
                     <td width="35%"> <strong class="font-15">Video Description:</strong></td>
                     <td width="65%" id="model_video_desc"></td>
                  </tr>
                  <tr>
                     <td colspan="2">
                        <strong class="font-15 mb-2 d-block">Terms & Conditions::</strong>
                        <div class="textcft">
                           <ul id="model_term_cond1"></ul>
                        </div>
                     </td>
                  </tr>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
<style type="text/css">.mytabless td{ padding: 5px 0; vertical-align: top; }.textcft ul{ padding: 0 0  0 10px }.textcft ul li{ list-style:disc; padding: 0 0 5px 0; }</style>
@endsection
@section('post_js')
<script src="{{ asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/sweet-alerts.init.js') }}"></script>
<script src="{{ asset('assets/libs/magnific-popup/magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/js/pages/gallery.init.js')}}"></script>
<script src="{{ asset('js/bo/manage_contest.js') }}"></script>
<script>
   $(document).ready(function() {
       $('#example').dataTable({
           /* No ordering applied by DataTables during initialisation */
           "order": []
       });
   })
   
   </script>
@endsection