@extends('bo.layouts.master')

@section('title', "Analytics | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/c3/c3.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('post_css')
    
@endsection

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">Analytics</h4>
        </div>
    </div>
</div>     
<!-- end page title --> 

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
            <h4 class="header-title mb-3">Channel Breakup</h4>
            <div class="form-row customDatePickerWrapper">
                <div class="form-group col-md-3">
                    <label>From: </label>
                    <input name="date_from" type="text" class="form-control customDatePicker from" value="{{ \Carbon\Carbon::yesterday()->setTime("00","00","00")->format('d-M-Y H:i:s') }}" placeholder="Select From Date" data-filter="channelChartFilter" data-type="from">
                </div>
                <div class="form-group col-md-3">
                    <label>To: </label>
                    <input name="date_to" type="text" class="form-control customDatePicker to" value="{{ \Carbon\Carbon::yesterday()->setTime("23","59","59")->format('d-M-Y H:i:s') }}" placeholder="Select To Date" data-filter="channelChartFilter" data-type="to">
                </div>
                <div class="form-group col-md-3">
                    <label>Group By: </label>
                    <select name="date_range" class="form-control" data-filter="channelChartFilter" data-type="filterBy">
                        <option value="">Select</option>
                        <option value="DATE" selected>DAYS</option>
                        <option value="WEEK">WEEK</option>
                        <option value="MONTH">MONTH</option>
                        <option value="YEAR">YEAR</option>
                    </select>
                </div>
                <div class="form-group col-md-3 d-flex align-items-end">
                    <button class="channelChartFilterBtn btn btn-primary" style="width:100%;">Filter</button>
                </div>
            </div>
            <div id="channelChart" style="height: 300px;" dir="ltr"></div>
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="col-lg-12">
        <div class="card-box">
            <h4 class="header-title mb-3">Campaign Breakup</h4>
            <div class="form-row customDatePickerWrapper">
                <div class="form-group col-md-3">
                    <label>From: </label>
                    <input name="date_from" type="text" class="form-control customDatePicker from" value="{{ \Carbon\Carbon::yesterday()->setTime("00","00","00")->format('d-M-Y H:i:s') }}" placeholder="Select From Date" data-filter="campaignChartFilter" data-type="from">
                </div>
                <div class="form-group col-md-3">
                    <label>To: </label>
                    <input name="date_to" type="text" class="form-control customDatePicker to" value="{{ \Carbon\Carbon::yesterday()->setTime("23","59","59")->format('d-M-Y H:i:s') }}" placeholder="Select To Date" data-filter="campaignChartFilter" data-type="to">
                </div>
                <div class="form-group col-md-3">
                    <label>Group By: </label>
                    <select name="date_range" class="form-control" data-filter="campaignChartFilter" data-type="filterBy">
                        <option value="">Select</option>
                        <option value="DATE" selected>DAYS</option>
                        <option value="WEEK">WEEK</option>
                        <option value="MONTH">MONTH</option>
                        <option value="YEAR">YEAR</option>
                    </select>
                </div>
                <div class="form-group col-md-3 d-flex align-items-end">
                    <button class="campaignChartFilterBtn btn btn-primary" style="width:100%;">Filter</button>
                </div>
            </div>
            <div id="campaignChart" style="height: 300px;" dir="ltr"></div>
        </div> <!-- end card-->
    </div> <!-- end col-->
</div>
<!-- End row -->

@endsection


@section('js')
<!--C3 Chart-->
<script src="{{ asset('assets/libs/d3/d3.min.js') }}"></script>
<script src="{{ asset('assets/libs/c3/c3.min.js') }}"></script>
@endsection

@section('post_js')
<script src="{{ asset('js/bo/analytics.js') }}"></script>
@endsection