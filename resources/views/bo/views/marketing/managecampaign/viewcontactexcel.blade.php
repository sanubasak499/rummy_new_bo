@extends('bo.layouts.master')

@section('title', "Manage Campaign | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/nestable2/nestable2.min.css') }}" rel="stylesheet" />
 <!-- Sweet Alert-->
<link href="{{url('assets/libs/jquery-toast/jquery-toast.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('post_css')
<style>
    .ms-container{
        max-width: 650px;
    } 
    .InvalidRow {
        color:red;
    } 
    .ValidRow {
        color:green;
    }
    .formtext {
    font-size: 11px;
    font-style: italic;
    color: red;
    padding: 6px 0 0 0;
    margin: 0;
    line-height: 13px;
}
      
</style>
@endsection
@section('content')
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item">Marketing</li>
                     <li class="breadcrumb-item"><a href="{{ url('marketing/managecampaign')}}">Manage Campaign</a> </li>
                     <li class="breadcrumb-item active">View Users Contact</li>
                  </ol>
               </div>
               <h4 class="page-title">View Users Contact </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <div class="row">
                      <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign Code: <span class="text-danger">{{ $campaign_info->PROMO_CAMPAIGN_CODE }}</span></h4>
                     </div>
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign Name: <span class="text-danger">{{ $campaign_info->PROMO_CAMPAIGN_NAME }}</span></h4>
                     </div>
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign Type: <span class="text-danger">{{ $campaign_type_id->PROMO_CAMPAIGN_TYPE }}</span></h4>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign Start Date: <span class="text-danger">{{ $campaign_info->START_DATE_TIME }}</span></h4>
                     </div>
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign End Date: <span class="text-danger">{{ $campaign_info->END_DATE_TIME }}</span></h4>
                     </div>
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Created By: <span class="text-danger">{{ $partner_type_id->PARTNER_NAME }}</span></h4>
                     </div>
                  </div>
                  <p class="text-muted font-13 mb-2"> </p>
                  <div class="row mb-1">
                     <div class="col-sm-4"></div>
                     <div class="col-sm-8">
                        <div class="text-sm-right">
                           <button type="button" class="btn btn-light mb-1" data-toggle="modal" data-target="#uploadexcel"><i class="fas fa-file-excel" style="color: #34a853;"></i> Import Excel</button>
                        </div>
                     </div>
                     <!-- end col-->
                  </div>
                  <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                     <thead>
                        <tr>
							<th>#</th>
							<th>Phone</th>
							<th>Status</th>
							<th>Used Date & Time</th>
							<th>Action</th>
                        </tr>
                     </thead>
                     <tbody>@php
					$i = 1;
					@endphp
					@forelse($getUserExcel as $key=> $getUserExcelData)
						<tr>
							<td>{{ $i++ }}</td>
							<td>{{ $getUserExcelData->REG_NUMBERS}}</td>
							<td>{{ $getUserExcelData->STATUS}}</td>
							<td>{{ $getUserExcelData->CREATED_DATE}}</td>
							<td>   
                              <button onclick="getData({{ $getUserExcelData->CAMPAIGN_USER_SPECIFIC_ID }})" type="button" class="action-icon bg-transparent border-0 font-14 tooltips" data-toggle="modal" data-target="#edituserss"><i class="fa fa-edit"></i><span class="tooltiptext">Edit</span></button>
                              <button id="delete_{{ $getUserExcelData->CAMPAIGN_USER_SPECIFIC_ID }}" onclick="dltData({{ $getUserExcelData->CAMPAIGN_USER_SPECIFIC_ID }})" type="button" class="action-icon bg-transparent border-0 font-14 tooltips" title="Delete" data-plugin="tippy" data-tippy-interactive="true"><i class="fa fa-trash"></i><span class="tooltiptext">Delete</span></button>
                           </td>
                        </tr>
						@empty
						<tr><td colspan="9">data not found</td></tr>
						@endforelse
                        
                     </tbody>
                  </table>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
   </div>

<div class="modal fade" id="edituserss">
   <div class="modal-dialog">
      <div class="modal-content bg-transparent shadow-none ">
         <div class="card ">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Edit User Contact</h5>
            </div>
            <div class="card-body">
                <form id="valid-excel-data" method="post" action="{{ url('marketing/managecampaign/updateDataExcelContact') }}" enctype="multipart/form-data">
			  @csrf	
				    <div class="form-group">
					<input type="hidden" name="cus_id" value="">
						 <label for="name">Phone No</label>
						 <input type="text" class="form-control" id="number" name="number" placeholder="" required>
				    </div>
					<div class="text-right">
						<button type="submit" class="btn btn-success waves-effect waves-light">
						<i class="fa fa-save mr-1"></i> Save
						</button>
					</div>
                </form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<div class="modal fade" id="uploadexcel">
   <div class="modal-dialog">
      <div class="modal-content bg-transparent shadow-none ">
         <div class="card ">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Import Excel</h5>
            </div>
            <div class="card-body">
                <form method="post" action="{{ url('marketing/managecampaign/addViewUserExcelContact') }}" id="viewContactExcel" enctype='multipart/form-data'>
				
				@csrf
					<div class="form-group">
					<input type="hidden" name="campaign_id" id="campaign_id" value="{{ $campaign_info->PROMO_CAMPAIGN_ID }}">
					<label for="name">Excel</label>
                    <input type="file" class="form-control" name="excel_viewcontact" id="excel_viewcontact" accept=".xlsx, .xls, .csv"/>
					 <div id="excelError"></div>
					</div>
					<div class="text-right">
                     <button type="button" id="saveContactExcel" class="btn btn-success waves-effect waves-light">
                     <i class="fa fa-save mr-1"></i> Save
                     </button>
					</div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
 <!---- End model box for user excel-->
<!---model for excel mobile number upload-->
<div id="accordion-modal-contact" class="modal fade show" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="" aria-modal="true">
 <div class="modal-dialog modal-lg">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Upload Contact Numbers</h5>
            </div>
            <div class="card-body" id="">
                <table id="basic-datatable-modal" class="basic-datatable- table dt-responsive nowrap w-100">
					<thead class="thead-light">
						<tr>
							<th>Sr no.</th>
							<th>Contact Number</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody id="getUserExcelData">
					</tbody>
				</table>
               <button type="button" class="card-button btn btn-warning waves-effect waves-light mr-1" data-dismiss="modal"><i class="fa fa-upload mr-1"></i> Reupload</button>
               
			   <button type="button" id="confirmExcelUploadContact" name="confirmExcelUploadContact" class="nextBtn btn btn-secondary waves-effect waves-light" ><i class="fa fa-save mr-1"></i>Continue Anyway</button>
            </div>
         </div>
      </div>
   </div>
</div>
<!---model for excel mobile number upload-->

@endsection
@section('js')
<script src="{{url('assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>   
<script src="{{url('assets/js/pages/form-wizard.init.js')}}"></script>
<script src="{{asset('assets/js/pages/sweet-alerts.init.js')}}"></script>
<script src="{{asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script>
$(document).ready(function(){
	$(".modalclose").click(function(){
		$('#accordion-modal').hide();
	});
	$("input[name='excel_viewcontact']").on("focus",function(){
		$("#excelError").text('');
	});
});
$(".card-widgets,.card-button").on("click", function(){
    $('#accordion-modal-contact').css('display', 'none');
});
function getData(id){
	var dataString = 'id=' +id;
	$.ajax({
	   "url": "{{url('/marketing/managecampaign/editDataExcelContact')}}",
	   "method":"post",
		dataType: "json",
		data: dataString,
		success:function(data){
			$('#number').val(data.mobile_number);
			$("#edituserss").find('input[name="cus_id"]').val(id);
			$("#edituserss").find('input.error').removeClass('error');
			$("#edituserss").find('label.error').remove();
			
		},
	});
}

dltData = (id) => {
	Swal.fire({
	title: 'Are you sure?',
	text: "You won't be able to revert this!",
	type:"warning",
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	confirmButtonText: 'Yes, delete it!'
	}).then((result) => {
          if (result.value) {
			 var _el = $("#delete_" + id).closest("tr");
			$.ajax({
				
				"url":`${window.pageData.baseUrl}/marketing/managecampaign/deleteDataExcelContact`, 
				"method":"post",
				"data":{"id":id},
				"dataType":"json",
				success:function(data){
					if(data.status){
						$.NotificationApp.send("Success", data.message, 'top-right', '#5ba035', 'success');
					 _el.remove();
						//location.reload();
					}else{
						$.NotificationApp.send("error", data.message, 'top-right', '#FF0000', 'error');
					}
					window.location.reload();			
				}
			});
          }
        })
}

$('#saveContactExcel').on('click', function(){

var form_data = new FormData(); 

		form_data.append("excel_viewcontact", $('input[name="excel_viewcontact"]')[0].files[0]);
		form_data.append("isUploadExcel", 1);
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});					
		$.ajax({
		"url": `${window.pageData.baseUrl}/marketing/managecampaign/addViewUserExcelContact`,
		"type": "POST",
		"data": form_data,
		"cache": false,
		"contentType": false, 
		"processData": false, 
		beforeSend:function(){
			$('#saveContactExcel').text('Loading..').prop('disabled',true);
		},
		success: function (data) {
		  if(data){
			  $('#basic-datatable-modal').DataTable().destroy();
			//  alert(data);
			if(data.status == 101){
				$("#excelError").html(data.message).addClass("InvalidRow");
				$('#saveContactExcel').text('Save').prop('disabled',false);
			}else if(data.status == 102){
				$("#excelError").html(data.message).addClass("InvalidRow");
				$('#saveContactExcel').text('Save').prop('disabled',false);
			}else{
			var newData = [];
			newData[0] = data;
			var htmldata  = getUserExcelDataContact(data);  
			$("#accordion-modal-contact").find("table").html(htmldata);
			//$('#basic-datatable tbody').html('');
			 $('#basic-datatable-modal').DataTable();
			$('#accordion-modal-contact').modal('show');
			$('#saveContactExcel').text('Save').prop('disabled',false);
			}
			
		  }else{
			$('#accordion-modal-contact').modal('hide');
		  }
		}
	});
					
});
$('#confirmExcelUploadContact').on('click', function(){
	$("#viewContactExcel").submit();
	$(this).prop('disabled',true);
});
function getUserExcelDataContact(data){
	if (data!='') {
		var element = '';
			// $('#basic-datatable tbody').html('');
			// $('#basic-datatable').DataTable().destroy();
		for (let i = 0;i<data.contactStatusResponse.length;i++) {
			
			
			
			element += `<tr class="${data.contactStatusResponse[i].styleClass}">
			<td>${(i+1)}</td>
			<td>${data.contactStatusResponse[i].userContact}</td>
			<td>${data.contactStatusResponse[i].STATUS}</td>
			
		</tr>`;
		}

		$("#getUserExcelData").html(element);
	}
	
}
</script>
@endsection
