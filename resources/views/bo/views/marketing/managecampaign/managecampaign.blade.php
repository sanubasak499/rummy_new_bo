@extends('bo.layouts.master')

@section('title', "Manage Campaign | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/nestable2/nestable2.min.css') }}" rel="stylesheet" />
 <!-- Sweet Alert-->
<link href="{{url('assets/libs/jquery-toast/jquery-toast.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					<ol class="breadcrumb m-0">
						<li class="breadcrumb-item">Marketing</a></li>
						<li class="breadcrumb-item active"><a href="{{ url('marketing/managecampaign')}}">Manage Campaign </a></li>
					</ol>
				</div>
				<h4 class="page-title">Manage Campaign </h4>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<div class="row mb-3">
						<div class="col-lg-8">
							<h5 class="font-18">Search Campaign </h5>
						</div>
						<div class="col-lg-4">
							<div class="text-lg-right mt-3 mt-lg-0"><a href="{{ route('marketing.managecampaign.createcampaign')}}" class="btn btn-success waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add Campaign</a></div>
						</div>
					</div>
					<form class="needs-validation" name="campaigndata" id="campaigndata" enctype='multipart/form-data' method="post" action="{{ url('marketing/managecampaign') }}">
						@csrf
						<input type="hidden" name="is_searched" value="1">
						<div class="form-row">
							<div class="form-group col-md-4">
								<label for="inputPassword4" class="col-form-label">Campaign type</label>
								<select class="custom-select" id="CampaignType" name="CampaignType">
									<option value="">Select type</option>
									@foreach($campaignType as $key=> $campaignType)
									<option value="{{ $campaignType->PROMO_CAMPAIGN_TYPE_ID }}"{{ !empty($old['CampaignType']) ? $old['CampaignType'] ==$campaignType->PROMO_CAMPAIGN_TYPE_ID  ? "selected" : ''  : ''  }}>{{ $campaignType->PROMO_CAMPAIGN_TYPE }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="inputPassword4" class="col-form-label">Status</label>
								<select class="custom-select" id="campaignstatus" name="campaignstatus">
								<option value="" selected="selected">Select </option>
									<option value="1"{{ !empty($old['campaignstatus']) ? $old['campaignstatus'] == 1  ? "selected" : ''  : ''  }}>Active</option>
									<option value="2"{{ !empty($old['campaignstatus']) ? $old['campaignstatus'] == 2  ? "selected" : ''  : ''  }}>Inactive</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="inputPassword4" class="col-form-label">Affiliate</label>
								<select class="custom-select" id="partner_name" name="partner_name">
									<option value="">List of Affiliate</option>
									@foreach($getPartnerNames as $key=> $partnerNames)
									<option value="{{$partnerNames->PARTNER_ID}}"{{ !empty($old['partner_name']) ? $old['partner_name'] == $partnerNames->PARTNER_ID  ? "selected" : ''  : ''  }}>{{$partnerNames->PARTNER_NAME}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
								<label for="validationCustom01">Campaign Name</label>
								<input type="text" class="form-control"name="campaignname" id="campaignname" value="{{ !empty($old['campaignname']) ? $old['campaignname'] : '' }}" placeholder=" ">
								
							</div>
							<div class="form-group col-md-4">
								<label for="validationCustom01">Campaign Code</label>
								<input type="text" value="{{ !empty($old['campaigncode']) ? $old['campaigncode'] : '' }}" class="form-control" name="campaigncode" id="campaigncode" placeholder=" ">
								
							</div>
							<div class="form-group col-md-4">
								<label for="validationCustom01">Created by </label>
								<input type="text" class="form-control" name="created_by" id="created_by" value="{{ !empty($old['created_by']) ? $old['created_by'] : '' }}" placeholder="  ">
								
							</div>
						</div>
						
						<div class="form-row customDatePickerWrapper">
							<div class="form-group col-md-4">
								<label>Start date</label>
								<div class="input-group">
									<input type="text"  value="{{ !empty($old['startdate']) ? $old['startdate'] : '' }}" id="startdate" name="startdate"class="form-control customDatePicker from dateicons" placeholder="Select Start Date" >
								</div>
							</div>
							<div class="form-group col-md-4">
								<label>End date</label>
								<div class="input-group">
									<input type="text" value="{{ !empty($old['enddate']) ? $old['enddate'] : '' }}" id="enddate" name="enddate" class="form-control customDatePicker to dateicons" placeholder="Select End Date" >
								</div>
							</div>
							<div class="form-group col-md-4">
								<label for="inputPassword4">Date Range: </label>
								<select name="date_range" id="SEARCH_LIMIT" name="SEARCH_LIMIT" class="form-control formatedDateRange">
                                    <option data-default-selected value="" {{ !empty($old['SEARCH_LIMIT']) ? $old['SEARCH_LIMIT'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                                    <option value="1" {{ !empty($old['SEARCH_LIMIT']) ? $old['SEARCH_LIMIT'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                                    <option value="2" {{ !empty($old['SEARCH_LIMIT']) ? $old['SEARCH_LIMIT'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                                    <option value="3" {{ !empty($old['SEARCH_LIMIT']) ? $old['SEARCH_LIMIT'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                                    <option value="4" {{ !empty($old['SEARCH_LIMIT']) ? $old['SEARCH_LIMIT'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                                    <option value="5" {{ !empty($old['SEARCH_LIMIT']) ? $old['SEARCH_LIMIT'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                                    <option value="6" {{ !empty($old['SEARCH_LIMIT']) ? $old['SEARCH_LIMIT'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                                </select>
							</div>
						</div>
						<div class="button-list">
							<button type="submit" id="frmsearch" name="frmsearch" class="btn btn-primary waves-effect waves-light"><i class=" fas fa-search mr-1"></i> Search</button>
							<a href="{{ url('marketing/managecampaign') }}" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i>Reset</a>
						
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@if(!$page)
<div class="row">
    <div class="col-12">
        <div class="card">
		<div class="card-header bg-dark text-white">
			<div class="card-widgets">
				<a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
			</div>
			<h5 class="card-title mb-0 text-white">Campaign List</h5>
		</div>
            <div class="card-body">
				<table id="basic-datatable" class="table dt-responsive nowrap">
					<thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Campaign Type</th>
                            <th>Campaign Code</th>
                            <th>Campaign Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
							<th>Clicks</th>
                            <th>Unique Users</th>
							<th>Assigned user</th>
							<th>Status</th>
                            <th>Action</th>
						</tr>
                    </thead>
                    <tbody>@php
					$i = 1;
					@endphp
					@if(isset($CampaignInfo) && !empty($CampaignInfo))
						@forelse($CampaignInfo as $key => $search_campaign)
						<tr>
							<td>{{ $i++ }}</td>
							<td>{{ $search_campaign->PROMO_CAMPAIGN_TYPE }}</td>
							<td>{{ $search_campaign->PROMO_CAMPAIGN_CODE }}</td>
							<td>{{ $search_campaign->PROMO_CAMPAIGN_NAME }}</td>
							<td>{{ $search_campaign->START_DATE_TIME }}</td>
							<td>{{ $search_campaign->END_DATE_TIME }}</td>
							<td>{{ $search_campaign->noOfClicks }}</td>
							<td><a onclick=
								"getUniqueUser({{ $search_campaign->PROMO_CAMPAIGN_ID }})" href="#" data-toggle="modal" data-target="#UniqueUserspopup"> {{$search_campaign->RegUser}}</a></td>
							@if($search_campaign->PROMO_CAMPAIGN_TYPE == 'BULK CODE' || $search_campaign->PARTNER_ID != 10001)
							<td></td>
							@elseif($search_campaign->PROMO_CAMPAIGN_TYPE == 'REGISTRATION' && $search_campaign->PARTNER_ID == 10001)
							<td><a href="{{ route('marketing.managecampaign.viewcontactexcel',encrypt($search_campaign->PROMO_CAMPAIGN_ID))}}" target="_blank">Contact Excel</a></td>
							@elseif(in_array($search_campaign->PROMO_CAMPAIGN_TYPE_ID,['1','3','6','5','8']) && $search_campaign->PARTNER_ID == 10001)
							<td><a href="{{ route('marketing.managecampaign.viewuserexcel',encrypt($search_campaign->PROMO_CAMPAIGN_ID))}}" target="_blank">List Excel</a></td>
							@else
								<td></td>
							@endif
							@if($search_campaign->PROMO_STATUS_ID=="1")
							<td><div style="display:none">{{ $search_campaign->PROMO_STATUS_ID }}</div><label class="switches"><input type="checkbox" class="status_switch" data-id="{{ $search_campaign->PROMO_CAMPAIGN_ID }}" checked><span class="slider round"></span></label></td>
							@else
							<td><div style="display:none">{{ $search_campaign->PROMO_STATUS_ID }}</div><label class="switches"><input type="checkbox" class="status_switch" data-id="{{ $search_campaign->PROMO_CAMPAIGN_ID }}" ><span class="slider round"></span></label></td>	
							@endif
							<td>
							<div style="width:85px;">
							@if($search_campaign->PROMO_CAMPAIGN_TYPE == 'BULK CODE')
							<a href="{{ route('marketing.managecampaign.viewbulkcode',encrypt($search_campaign->PROMO_CAMPAIGN_ID))}}"  class="action-icon font-15  tooltips" target="_blank"><i class="mdi mdi-eye-outline"></i><span class="tooltiptext">View Bulk Campaign</span></a>
							@else
								<button onclick="viewCampaignData({{ $search_campaign->PROMO_CAMPAIGN_ID }})" type="button" class="action-icon bg-transparent border-0 font-15 tooltips" 	data-toggle="modal" data-target="#exampleModal" ><i class=" mdi mdi-eye-outline"></i><span class="tooltiptext">View Campaign</span></button>
							@endif
								<a href="{{ route('marketing.managecampaign.editcampaign',encrypt($search_campaign->PROMO_CAMPAIGN_ID))}}" type="button" class="action-icon  tippy-active font-15  tooltips"><i class="fa fa-edit"></i>
								<span class="tooltiptext">Edit Campaign</span></a>
								<button type="button"  id="delete_{{ $search_campaign->PROMO_CAMPAIGN_ID }}" onclick="deleteReason({{ $search_campaign->PROMO_CAMPAIGN_ID }})" class="action-icon bg-transparent border-0 font-15  tooltips"><i class="fa fa-trash"></i><span class="tooltiptext">Delete Campaign</span></button>
								
								</div>
							</td>
						</tr>
						@empty
						<tr>
							<td>data not found</td>
							
						</tr>
						@endforelse
						@endif
                    </tbody>
                </table>
				
			</div>
            <!-- end card body-->
        </div>
        <!-- end card -->
    </div>
    <!-- end col-->
</div>
@endif
<!-- Modal for unique users -->
<div class="modal fade" id="ModalUniqueUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
		<div class="modal-content">
			  <div class="modal-header border-0 pb-0">
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			<div class="modal-body"><h4 class="mt-0"></h4>
			<div class="table-responsive">
			   <table width="100%" class="table mb-0">
			   <thead>
				   <tr>
				   <td width="50%"><b>SNo.</b> </td>
				   <td width="50%"><b>USERNAME</b></td>
				   </tr>
				</thead>
				<tbody id="">
				
				</tbody>
				</table>
			</div>
			</div>
	   </div>
	</div>
</div>

<!-- Modal for view -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header border-0 pb-0">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  <div class="modal-body">
		<div class="table-responsive">
			<table width="100%" class="table mb-0 table-striped">
				
			</table>
		</div>
	</div>
   </div>
  </div>
</div>
<!--model for unique user-->
<div class="modal fade" id="UniqueUserspopup">
   <div class="modal-dialog modal-xl">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">View Users </h5>
            </div>
            <div class="card-body">
               <div class="row">
                  <div class="col-md-4">
                     <h4 class="font-15 mt-0"> Campaign Code: 
					 <span id="v_campaign_code" class="text-danger"></span></h4>
                  </div>
				  
                  <div class="col-md-4">
                     <h4 class="font-15 mt-0"> Campaign Name: 
					 <span id="v_campaign_name" class="text-danger"></span></h4>
                  </div>
                  <div class="col-md-4">
                     <h4 class="font-15 mt-0"> Campaign Type: 
					 <span id="v_campaign_type" class="text-danger"></span></h4>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-4">
                     <h4 class="font-15 mt-0"> Campaign Start Date: 
					 <span id="v_campaign_s_date" class="text-danger"></span></h4>
                  </div>
                  <div class="col-md-4">
                     <h4 class="font-15 mt-0"> Campaign End Date: <span id="v_campaign_e_date" class="text-danger"></span></h4>
                  </div>
                  <div class="col-md-4">
                     <h4 class="font-15 mt-0"> Created By: <span id="v_campaign_partner_name" class="text-danger"></span></h4>
                  </div>
               </div>
               <p class="text-muted font-13 mb-3"> </p>
			   <table id="basic-datatable" class="table dt-responsive nowrap w-100">
					<thead>
					 <tr>
						<th>#</th>
						<th>Username</th>
						<th>Phone</th>
						<th>Email</th>
						<th>Used Date & Time</th>
					 </tr>
					</thead>
					<tbody id="uniqueUserData">
					
					</tbody>
			   </table>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Modal -->
<!--end-->
@endsection
@section('js')
<script src="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.js') }}"></script>
<script src="{{ asset('assets/libs/switchery/switchery.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.jS') }}"></script>
<script src="{{ asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-mockjax/jquery-mockjax.min.js') }}"></script>
<script src="{{ asset('assets/libs/autocomplete/autocomplete.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script>
<script src="{{ url('js/bo/managecampaign.js') }}"></script>  
<script src="{{asset('assets/js/pages/sweet-alerts.init.js')}}"></script>
<script src="{{asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>


<!-- end row-->
@endsection