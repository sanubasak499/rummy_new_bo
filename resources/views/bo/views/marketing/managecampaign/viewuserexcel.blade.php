@extends('bo.layouts.master')

@section('title', "Manage Campaign | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/nestable2/nestable2.min.css') }}" rel="stylesheet" />
 <!-- Sweet Alert-->
<link href="{{url('assets/libs/jquery-toast/jquery-toast.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{url('assets/libs/jquery-toast/jquery-toast.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('post_css')
<style>
    .ms-container{
        max-width: 650px;
    } 
    .InvalidRow {
        color:red;
    } 
    .ValidRow {
        color:green;
    }
    .formtext {
    font-size: 11px;
    font-style: italic;
    color: red;
    padding: 6px 0 0 0;
    margin: 0;
    line-height: 13px;
}
      
</style>
@endsection
@section('content')

   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item">Marketing</li>
                     <li class="breadcrumb-item"><a href="{{ url('marketing/managecampaign')}}">Manage Campaign</a></li>
                     <li class="breadcrumb-item active">View Users</li>
                  </ol>
               </div>
               <h4 class="page-title">View Users </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <div class="row">
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign Code: <span class="text-danger">{{ $campaign_info->PROMO_CAMPAIGN_CODE }}</span></h4>
                     </div>
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign Name: <span class="text-danger">{{ $campaign_info->PROMO_CAMPAIGN_NAME }}</span></h4>
                     </div>
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign Type: <span class="text-danger">{{ $campaign_type_id->PROMO_CAMPAIGN_TYPE }}</span></h4>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign Start Date: <span class="text-danger">{{ $campaign_info->START_DATE_TIME }}</span></h4>
                     </div>
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign End Date: <span class="text-danger">{{ $campaign_info->END_DATE_TIME }}</span></h4>
                     </div>
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Created By: <span class="text-danger">{{ $partner_type_id->PARTNER_NAME }}</span></h4>
                     </div>
                  </div>
                  <p class="text-muted font-13 mb-2"> </p>
                  <div class="row mb-1">
                     <div class="col-sm-4"></div>
                     <div class="col-sm-8">
                        <div class="text-sm-right">
                           <button type="button" class="btn btn-light mb-1" data-toggle="modal" data-target="#uploadexcel"><i class="fas fa-file-excel" style="color: #34a853;"></i> Import Excel</button>
                        </div>
                     </div>
                     <!-- end col-->
                  </div>
                  <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                    <thead>
                        <tr>
                           <th>Sr no.</th>
                           <th>Userid</th>
                           <th>Username</th>
                           <th>Email</th>
                           <th>Phone</th>
                           <th>Status</th>
                           <th>Assign Date</th>
                           <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>@php
					$i = 1;
					@endphp
					@forelse($getUserExcel as $key=> $getUserExcelData)
						<tr>
							<td>{{ $i++ }}</td>
							<td>{{ $getUserExcelData->USER_ID}}</td>
							<td>{{ $getUserExcelData->USERNAME}}</td>
							<td>{{ $getUserExcelData->EMAIL_ID}}</td>
							<td>{{ $getUserExcelData->CONTACT}}</td>
							<td>{{ $getUserExcelData->STATUS}}</td>
							<td>{{ $getUserExcelData->CREATED_DATE}}</td>
							<td>   
                              <button onclick="getData({{ $getUserExcelData->CAMPAIGN_USER_SPECIFIC_ID }})" type="button" class="action-icon bg-transparent border-0 font-15 tooltips" data-toggle="modal" data-target="#edituserss"><i class="fa fa-edit"></i><span class="tooltiptext">Edit</span></button>
                              <button id="delete_{{ $getUserExcelData->CAMPAIGN_USER_SPECIFIC_ID }}" onclick="dltData({{ $getUserExcelData->CAMPAIGN_USER_SPECIFIC_ID }})" type="button" class="action-icon bg-transparent border-0 font-15 tooltips" title="Delete" data-plugin="tippy" data-tippy-interactive="true"><i class="fa fa-trash"></i><span class="tooltiptext">Delete</span></button>
                           </td>
                        </tr>
						@empty
						<tr><td colspan="9">data not found</td></tr>
						@endforelse
                    </tbody>
                  </table>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
   </div>

<div class="modal fade" id="edituserss">
   <div class="modal-dialog">
      <div class="modal-content bg-transparent shadow-none ">
         <div class="card ">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Edit User</h5>
            </div>
            <div class="card-body">
            <form id="valid-excel-data" method="post" action="{{ url('marketing/managecampaign/updateDataExcel') }}" enctype="multipart/form-data">
			  @csrf	
				
			    <div class="form-group">
					 <label for="name">User</label>
					 <input type="hidden" name="cus_id" value="">
					 <input type="text" class="form-control" id="name" name="name" placeholder="" required />
				</div>
				<div class="text-right">
                     <button type="button" id="update_submit" class="btn btn-success waves-effect waves-light" data-toggle="modal">
                     <i class="fa fa-save mr-1"></i> Save
                     </button>
				</div>
			</form>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="uploadexcel">
   <div class="modal-dialog">
      <div class="modal-content bg-transparent shadow-none ">
         <div class="card ">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Import Excel</h5>
            </div>
            <div class="card-body">
               <form method="post" id="viewUserExcel" action="{{ url('marketing/managecampaign/addViewUserExcel') }}" enctype='multipart/form-data'>
			  @csrf	
                  <div class="form-group">
				  <input type="hidden" name="campaign_id" value="{{ $campaign_info->PROMO_CAMPAIGN_ID }}">
                     <label for="name">Excel</label>
                     <input type="file" class="form-control" id="excel_viewuser" name="excel_viewuser" accept=".xlsx, .xls, .csv"/>
					 <div id="excelError"></div>
					</div>
					
                  <div class="text-right">
				  <!--<button type="submit" class="btn btn-success waves-effect waves-light">
                     <i class="fa fa-save mr-1"></i> Save
				  </button>-->
					 <button title="Click this button to import users" data-plugin="tippy" data-tippy-arrow="true" data-tippy-animation="fade" id="saveuserExcel" type="button"  class="btn btn-success waves-effect waves-light add_field_button"><i class="fas fa-save mr-2 " ></i>Save</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
 </div>

<!---- model box for user excel-->

<div id="accordion-modal-user" class="modal fade show" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="" aria-modal="true">
 <div class="modal-dialog modal-lg">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Upload users</h5>
            </div>
            <div class="card-body" id="">
			   <table id="basic-datatable-modal" class="basic-datatable table dt-responsive nowrap w-100">
					<thead class="thead-light">
						<tr>
							<th>Sr no.</th>
							<th>Username</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody id="getUserExcelData">
					</tbody>
				</table>
               <button type="button" class="card-button btn btn-warning waves-effect waves-light mr-1" data-dismiss="modal"><i class="fa fa-upload mr-1"></i> Reupload</button>
               
			   <button type="button" id="confirmExcelUpload" name="confirmExcelUpload" class="nextBtn btn btn-secondary waves-effect waves-light" ><i class="fa fa-save mr-1"></i>Continue Anyway</button>
            </div>
         </div>
      </div>
   </div>
</div>
 
 <!---- End model box for user excel-->
@endsection
@section('js')
<script src="{{url('assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>   
<script src="{{url('assets/js/pages/form-wizard.init.js')}}"></script>
<script src="{{asset('assets/js/pages/sweet-alerts.init.js')}}"></script>
<script src="{{asset('assets/libs/sweetalert2/sweetalert2.min.js')}}"></script>
<script>
$(document).ready(function(){
	$(".modalclose").click(function(){
		$('#accordion-modal').hide();
		// alert('dasdsa');
	});
	$("input[name='excel_viewuser']").on("focus",function(){
		$("#excelError").text('');
	});
});
   
$(document).ready(function(){
	
	$( "#valid-excel-data" ).validate({
		  rules: {
			name: {
			  required: true,
			  remote: {
				url: "{{url('/marketing/managecampaign/editDataExcel')}}",
				type: "post",
				//dataType:"json",
				data: {
				  username: function() {
					return $( "#name" ).val();
				  }
				}
			  }
			}
		  },
		    messages: {
				name: {
					remote: "username doesn't exist"
				}
			}
		});
		//end edit validation
		//
		
	
////validation close
	$("#update_submit").click(function(){
		$(this).closest('form').submit();
	})
	$.ajaxSetup({
	   headers: {
		   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	   }
})
///setup close

});//close document

function getData(id){
	var dataString = 'id=' +id;
		$.ajax({
		   "url": "{{url('/marketing/managecampaign/editDataExcel')}}",
		   "method":"post",
			dataType: "json",
			data: dataString,
			success:function(data){
				$('#name').val(data.name);
				$("#edituserss").find('input[name="cus_id"]').val(id);
				$("#edituserss").find('input.error').removeClass('error');
				$("#edituserss").find('label.error').remove();
			},
			
		});
}

dltData = (id) => {
	Swal.fire({
	title: 'Are you sure?',
	text: "You won't be able to revert this!",
	type:"warning",
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	confirmButtonText: 'Yes, delete it!'
	}).then((result) => {
          if (result.value) {
			 var _el = $("#delete_" + id).closest("tr");
			$.ajax({
				
				"url":`${window.pageData.baseUrl}/marketing/managecampaign/deleteDataExcelContact`, 
				"method":"post",
				"data":{"id":id},
				"dataType":"json",
				success:function(data){
					if(data.status){
						$.NotificationApp.send("Success", data.message, 'top-right', '#5ba035', 'success');
					 _el.remove();
						//location.reload();
					}else{
						$.NotificationApp.send("error", data.message, 'top-right', '#FF0000', 'error');
					}			
				}
			});
          }
        })
}
$('#confirmExcelUpload').on('click', function(){
	$("#viewUserExcel").submit();
	$(this).prop('disabled',true);
});
$(".card-widgets,.card-button").on("click", function(){
    $('#accordion-modal-user').css('display', 'none');
});

$('#saveuserExcel').on('click', function(){

var form_data = new FormData(); 
	form_data.append("excel_viewuser", $('input[name="excel_viewuser"]')[0].files[0]);
	form_data.append("isUploadExcel", 1);
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});						
	$.ajax({
	"url": `${window.pageData.baseUrl}/marketing/managecampaign/addViewUserExcel`,
	"type": "POST",
	"data": form_data,
	"cache": false,
	"contentType": false, 
	"processData": false,
	beforeSend:function(){
		$('#saveuserExcel').text('Loading..').prop('disabled',true);
	},
	success: function (data) {
	  if(data){
		   $('#basic-datatable-modal').DataTable().destroy();
		if(data.status == 101){
			$("#excelError").html(data.message).addClass("InvalidRow");
			$('#saveuserExcel').text('Save').prop('disabled',false);
		}else if(data.status == 102){
			$("#excelError").html(data.message).addClass("InvalidRow");
			$('#saveuserExcel').text('Save').prop('disabled',false);
		}else{
			var newData = [];
			newData[0] = data;
			var htmldata = getUsersExcelData(data);
			$("#accordion-modal-user").find("table").html(htmldata);
			$('#basic-datatable-modal').DataTable();
			$('#accordion-modal-user').modal('show');
			$('#saveuserExcel').text('Save').prop('disabled',false);
		}
		
	  }else{
		$('#accordion-modal-user').modal('hide');
	  }
	}
});//ajax close
					
});

function getUsersExcelData(data){
	if (data!='') {
		var element = '';
		for (let i = 0;i<data.userStatusResponse.length;i++) {
			$('#basic-datatable tbody').html('');
			$('#basic-datatable-').DataTable().destroy();
			
			element += `<tr class="${data.userStatusResponse[i].styleClass}">
			<td>${(i+1)}</td>
			<td>${data.userStatusResponse[i].userName}</td>
			<td>${data.userStatusResponse[i].STATUS}</td>
			
		</tr>`;
		}
		$("#getUserExcelData").html(element);
		
	}
}


</script>
@endsection