@extends('bo.layouts.master')

@section('title', "Manage Campaign | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/nestable2/nestable2.min.css') }}" rel="stylesheet" />
 <!-- Sweet Alert-->
<link href="{{url('assets/libs/jquery-toast/jquery-toast.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{url('assets/libs/jquery-toast/jquery-toast.min.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.has-error input{ border: 1px #f00  solid }
.has-error span.error{ color: #f00;display:block }
span.error{ color: #f00;display:none; width: 100% }

.dropdown-menu .inner.show{ overflow-y:scroll !important; max-height:180px !important; overflow-x:hidden !important}
 .dropdown-menu .inner.show ul{ overflow:hidden !important ;max-height:100% !important }     
</style>
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					<ol class="breadcrumb m-0">
						<li class="breadcrumb-item">Marketing</a></li>
						<li class="breadcrumb-item"><a href="{{ url('marketing/managecampaign')}}">Manage Campaign </a></li>
						<li class="breadcrumb-item active">Edit Campaign</li>
					</ol>
				</div>
				<h4 class="page-title">Edit Campaign CD</h4>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
		<div class="row mb-3">
			<div class="col-lg-8">
				<h5 class="font-18">Edit Campaign </h5>
			</div>
			<div class="col-lg-4">
				<div class="text-lg-right mt-3 mt-lg-0">
					<a href="{{ url('marketing/managecampaign')}}" class="btn btn-success waves-effect waves-light"><i class="fa fa-bars mr-1"></i> View Campaign</a>
				</div>
			</div>
		</div>
		
		<div class="stepwizard">
			<ul class="stepwizard-row setup-panel nav nav-pills bg-light nav-justified  m-0 mb-3">
				<li class="stepwizard-step nav-item">
					<a href="#step-1" type="button" class=" active nav-link rounded-0 pt-2 pb-2 active"><i class=" mdi mdi-account-details mr-1"></i><span class="d-none d-sm-inline">Campaign Details</span></a>
					
				</li>
				<li class="stepwizard-step nav-item">
					<a href="#step-2" type="button" class=" btndefault nav-link rounded-0 pt-2 pb-2" disabled="disabled"><i class=" mdi mdi-account-details mr-1"></i><span class="d-none d-sm-inline">Payment & Rule Settings</span></a>
					
				</li>
				<li class="stepwizard-step nav-item">
					<a href="#step-3" type="button" class=" btndefault nav-link rounded-0 pt-2 pb-2" disabled="disabled"><i class=" mdi mdi-account-details mr-1"></i><span class="d-none d-sm-inline">Cost & User Range Settings</span></a>
					
				</li>
				
			</ul>
		</div>
		<form id="editcampaign" method="post" action="{{ url('marketing/managecampaign/editcampaign') }}" data-parsley-validate=""novalidate="">
			@csrf
				<div class="setup-content" id="step-1">
				<input type="hidden" name="promo_campaignid_edit" value="{{ $promo_campaignID }}"  />
				<div class="form-row">
					<div class="form-group col-md-4">
						<label for="campaignType">Campaign type</label>
						
						<input type="text" value="{{ $campaigntypeName[0]->PROMO_CAMPAIGN_TYPE }}" class="form-control" id="campaignType" name="campaignType" placeholder="  " readonly>
						
						<input type="hidden" value="{{ $getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID }}" class="form-control" id="campaignTypeid" name="campaignTypeid">
						
					</div>
					<div class="form-group col-md-4">
					<label for="partnername">Affiliate Name</label>
						
					<input type="text" value="{{ $partnerName[0]->PARTNER_NAME }}" class="form-control" id="partner_name" name="partner_name" placeholder="  " readonly>
					</div>
					<div class="form-group col-md-4">
						<label for="validationCustom01">Created By:</label>
						<input type="text" value="{{ Auth::user()->FULLNAME }}" class="form-control" id="created_by" name="created_by" placeholder="  " readonly>
						
					</div>
				</div> 
				<div class="form-row" id ="campaigncodeurl">
				@if($getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID != 7)
					<div class="form-group col-md-4">
						<label for="campaigncode">Campaign Code</label>
						<div class="input-group">
							<input type="text" class="form-control "id="campaigncode"name="campaigncode" placeholder="" value="{{ $getdataEditCampaign->PROMO_CAMPAIGN_CODE}}" maxlength="16" required readonly>
							<span class="error">Please enter campaign code</span>
								<div class="input-group-append" style="display:none" >
									<button class="btn btn-blue waves-effect waves-light" type="button" onclick="refreshBTN()"readonly>
										<span class="icon-refresh black-text"></span>
									</button>
								</div>
						</div>
						
					</div>
					@else
						
					@endif
					<div class="form-group col-md-8">
					
					@if($getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID == 2)
						<label for="campaignurl" id="campaignurlhead">Campaign URL</label>
						<label for="" id="default_campaignurlhead"style="display:none">Campaign Code</label>
						<div class="input-group">
						<input type="text" id="campaignurl" name="campaignurl"class="form-control " value="{{ $getdataEditCampaign->PROMO_CAMPAIGN_URL }}" placeholder="" aria-label="" readonly>
							<div class="input-group-append"id="generate-button">
								<button class="btn btn-blue waves-effect waves-light" id="ccodeGenerate" type="button" onclick="generateCampaignUrl()" readonly>Generate</button>
							</div>
							
						</div>
					@else
						
					@endif
					</div>
					
				</div>
				<div class="row">
					<div class="form-group col-md-4">
						<label for="campaignname">Campaign Name</label>
						<input type="text" class="form-control" value="{{ $getdataEditCampaign->PROMO_CAMPAIGN_NAME }}" id="campaignname" name="campaignname" maxlength="55" placeholder=" "required >
						<span class="error">Enter Campaign Name </span>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="campaign_description">Campaign Description</label>
							<input class="form-control" id="campaign_description" name="campaign_description"  maxlength="50" value="{{ $getdataEditCampaign->PROMO_CAMPAIGN_DESC }}">
						</div>
					</div>
					@if($getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID != 7)
					<div class="col-md-4" id="withdrawl_setting">
						<div class="form-group">
							<label for="campaign_description">Withdrawl Criteria</label>
							<input type="text" class="form-control" maxlength="50" id="withdrawl_criteria" name="withdrawl_criteria" value="{{ $getdataEditCampaign->WITHDRAW_CRITERIA }}" maxlength="50" rows="5" >
						</div>
					</div>
					@endif
				</div>
						
						
				<div class="row">
					<div class=" col-md-4">
						<div class="row">
						<div class="form-group col-md-4">
							<label for="campaignstatus">Campaign Status</label>
							<div>
							@if($getdataEditCampaign->PROMO_STATUS_ID == 1)
							<label class="switches">
							<input type="checkbox" name="campaignstatus" checked>
							<span class="slider round"></span>
							</label>
							@else
							<label class="switches">
							<input type="checkbox" name="campaignstatus" >
							<span class="slider round"></span>
							</label>	
							@endif
							</div>
						</div>
						<div class="form-group col-md-4">
							<label for="campaignstatus">Is Featured</label>
							<div>
							@if($getdataEditCampaign->IS_FEATURED == 1)
							<label class="switches">
							<input type="checkbox" name="campaignfeature" checked>
							<span class="slider round"></span>
							</label>
							@else
							<label class="switches">
							<input type="checkbox" name="campaignfeature" >
							<span class="slider round"></span>
							</label>	
							@endif
							</div>
						</div>
						<div class="form-group col-md-4">
							<label for="campaignstatus">Display Status</label>
							<div>
							@if($getdataEditCampaign->DISPLAY_STATUS == 1)
							<label class="switches">
							<input type="checkbox" name="display_status" checked>
							<span class="slider round"></span>
							</label>
							@else
							<label class="switches">
							<input type="checkbox" name="display_status" >
							<span class="slider round"></span>
							</label>	
							@endif
							</div>
						</div>
						
						</div>
					</div>
					@if($getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID != 7)
					<div class="col-md-4" id="remark_setting">
						<div class="form-group">
							<label for="campaign_description">Remark</label>
							<textarea class="form-control" id="campaign_remark" name="campaign_remark" placeholder="  " rows="4" >{{ $getdataEditCampaign->REMARK }}</textarea>
						</div>
					</div>
					@endif
			
					<div class="col-md-4 customDatePickerWrapper">
						<div class="form-group "> 
							<label for="startdate">Start date</label>
							<div class="input-group"> 
							<input type="text" value="{{ \Carbon\Carbon::parse($getdataEditCampaign->START_DATE_TIME)->format('d-M-Y H:i:s') }}" id="startdate" name="startdate"  class="form-control customDatePicker from" placeholder=""  required="">
							<span class="error">
							Select the campaign start date
							</span>
							</div>
						</div>
						<div class="form-group "> 
							<label for="enddate">End date</label>
							<div class="input-group">
							<input type="text" value="{{ \Carbon\Carbon::parse($getdataEditCampaign->END_DATE_TIME)->format('d-M-Y H:i:s') }}" id="disable-datepicker" id="enddate" name="enddate" class="form-control customDatePicker to" placeholder="" required="">
							<span class="error">
							Select the campaign end date
							</span>
							</div>
						</div>
					</div>
				</div>
				<button class="nextBtn btn btn-secondary waves-effect waves-light" type="button">Next <i class=" mdi mdi-skip-next  ml-1"></i></button>
				</div>
				<!--tab - 2-->
				<div class="setup-content" id="step-2">
				@if($getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID== 1 ||$getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID== 6 || $getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID== 8)
				<div id="Payment-first-deposite-tab">
					<h5 class="font-18">Campaign Type - Payment | First deposit  </h5>

					<div class="form-group">
					<label class="mb-1 d-block">Payment Type:</label>
					
					<div class="radio mb-1 d-inline-block mr-2">
						<input type="radio" id="payment_type_deposite_money" name="payment_type" value="1" <?php if($getdataEditCampaign->MONEY_OPTION==1) { echo "checked=checked"; } ?> data-parsley-multiple="">
						<label for="payment_type_deposite_money">
						Deposit Money
						</label>
					</div>
					
					<div class="radio d-inline-block mb-1">
						<input type="radio" id="payment_type_promo_money" name="payment_type"value="2" <?php if($getdataEditCampaign->MONEY_OPTION==2) { echo "checked=checked"; }?>/>
						<label for="payment_type_promo_money">
						Promo Money
						</label>
					</div>
					
					</div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input check-pay-group" id="p_promochips"name="p_promochips"<?php if($getdataEditCampaign->P_PROMO_CHIPS==1) echo "checked=checked";?>>
							<label class="custom-control-label" for="p_promochips">Promo Chips</label>
							</div>
						</div>
						<div class="form-group col-md-4">
							<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input check-pay-group" id="p_unclaimedbonus" name="p_unclaimedbonus"<?php if($getdataEditCampaign->P_UNCLAIMED_BONUS==1) echo "checked=checked";?>>
							<label class="custom-control-label" for="p_unclaimedbonus">Unclaimed Bonus</label>
							</div>
						</div>
						
						<div class="form-group col-md-4">
							<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" id="p_tournamenttickets" name="p_tournamenttickets" <?php if($getdataEditCampaign->TOURNAMENT_ID!='' && $getdataEditCampaign->TOURNAMENT_ID!='0' ) echo "checked=checked";?>>
							<label class="custom-control-label" for="p_tournamenttickets">Tournament tickets</label>
							</div>
						</div>
					</div>
					<div class="form-row">
					<div class="form-group col-md-4">
						<label for="p_promovalue">Promo (%)</label>
						@if($getdataEditCampaign->P_PROMO_VALUE!= 0)
						<input type="text" class="form-control" id="p_promovalue" name="p_promovalue"placeholder=" " value="{{ $getdataEditCampaign->P_PROMO_VALUE }}" max="100" min="0">
						@else
						<input type="text" class="form-control" id="p_promovalue" name="p_promovalue"value="" max="100" min="0">	
						@endif
					</div>
					<div class="form-group col-md-4" id="promo-amount" style="display:none">
						<label for="">Promo Amount</label>
						@if($getdataEditCampaign->P_CREDIT_DEBITCARD_VALUE!=0)
						<input type="text" class="form-control" id="promo_amount" name="promo_amount" value="{{ $getdataEditCampaign->P_CREDIT_DEBITCARD_VALUE}}" placeholder="  " />
						@else
						<input type="text" class="form-control" id="promo_amount" name="promo_amount" value="" placeholder="  " />	
						@endif
					</div>
					<div class="form-group col-md-4">
						<label for="">Netbanking (%)</label>
						@if($getdataEditCampaign->P_NETBANKING_VALUE == 0)
						<input type="text" class="form-control" id="p_netbankingvalue"name="p_netbankingvalue" value="" max="100" min="0">
						@else
						<input type="text" class="form-control" id="p_netbankingvalue"name="p_netbankingvalue" value="{{ $getdataEditCampaign->P_NETBANKING_VALUE}}" max="100" min="0">	
						@endif
					</div>
					
					<div class="form-group col-md-4 multiplecustom" id="tournament-tab" >
						<label for="">Tournament Tickets</label>
						<select class="form-control selectpicker multiselecttextoverflow" multiple data-style="btn-light" data-live-search="true" id="p_tournamentticketid" name="p_tournamentticketid[]">
						<option value="">-- Select --</option>
						
							<?php foreach($getTournamentId as $value) {   
							   $editCampaignTourID[] = $value->TOURNAMENT_ID;
							  
							}?>
						   <?php foreach($tournaments as $tournament) {
							   if(!empty($editCampaignTourID)){
									if(in_array($tournament->TOURNAMENT_ID,$editCampaignTourID)){
										echo '<option value="'.$tournament->TOURNAMENT_ID.'" selected="selected">'.$tournament->TOURNAMENT_NAME.'--'.$tournament->TOURNAMENT_START_TIME.'</option>';
									}else{
										echo '<option value="'.$tournament->TOURNAMENT_ID.'">'.$tournament->TOURNAMENT_NAME.'--'.$tournament->TOURNAMENT_START_TIME.'</option>';
									}
							   }else{
								   echo '<option value="'.$tournament->TOURNAMENT_ID.'">'.$tournament->TOURNAMENT_NAME.'--'.$tournament->TOURNAMENT_START_TIME.'</option>';
							   }
						   }
										
							?>
						
						</select>
						
					</div>
					
					</div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="">Promo Capping</label>
							@if($getdataEditCampaign->P_PROMO_CAP_VALUE!=0)
							<input type="text" class="form-control" id="p_promocapvalue"name="p_promocapvalue" placeholder="  "value="{{ $getdataEditCampaign->P_PROMO_CAP_VALUE }}" maxlength="6">
							@else
							<input type="text" class="form-control" id="p_promocapvalue"name="p_promocapvalue" placeholder="  "value="" maxlength="6">	
							@endif
						</div>
						<div class="form-group col-md-4">
							<label for="">Cradit /Dabit Card (%)</label>
							@if($getdataEditCampaign->P_CREDIT_DEBITCARD_VALUE!=0)
							<input type="text" class="form-control" id="p_creditdebitcardvalue" name="p_creditdebitcardvalue" value="{{ $getdataEditCampaign->P_CREDIT_DEBITCARD_VALUE }}" max="100" min="0">
							@else
							<input type="text" class="form-control" id="p_creditdebitcardvalue" name="p_creditdebitcardvalue"max="100" min="0">
							@endif
						</div>
						<div class="form-group col-md-4">
							<label for="">UCB Capping</label>
							@if($getdataEditCampaign->P_BONUS_CAP_VALUE!=0)
							<input type="text" class="form-control" id="p_bonuscapvalue" name="p_bonuscapvalue" value="{{ $getdataEditCampaign->P_BONUS_CAP_VALUE}}" placeholder="" maxlength="6">
							@else
							<input type="text" class="form-control" id="p_bonuscapvalue" name="p_bonuscapvalue" placeholder="" maxlength="6">
							@endif
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="m_depositamount">Minimum Deposit Amount</label>
							<input type="text" value="{{ $getdataEditCampaign->MINIMUM_DEPOSIT_AMOUNT }}" class="form-control" id="m_depositamount" name="m_depositamount" placeholder="  "  maxlength="7">
							
						</div>
						<div class="form-group col-md-4">
							<label for="">Max Deposit Amount</label>
							<input type="text" value="{{ $getdataEditCampaign->MAXIMUM_DEPOSIT_AMOUNT }}" class="form-control" id="max_depositamount"name="max_depositamount" placeholder="  "  maxlength="7">
							
						</div>
						<div class="form-group col-md-4">
							<label for="">Max Coins Required </label>
							<input type="text" class="form-control" id="coins_required" name="coins_required" value="{{ number_format($getdataEditCampaign->COINS_REQUIRED, 0, '.', "") }}"  maxlength="7">
							
						</div>
					</div>
					
				</div>
				@endif
				<!-- 3 Registration and tell a friend-->
				@if($getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID == 2 ||$getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID == 3)
				<div id ="registration-tab">
					<h5 class="font-18">Campaign Type - Registration  | Tell - a - Friend  </h5>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="mb-1 d-block">Payment Type:</label>
								<div class="radio d-inline-block mb-1">
									@if($getdataEditCampaign->PROMO_PAYMENT_TYPE_ID == 1)
									<input type="radio" id="paymenttype_promo" name="paymenttype" value="1" checked data-parsley-multiple="">
									@else
									<input type="radio" id="paymenttype_promo" name="paymenttype" value="1" data-parsley-multiple="">
									@endif
									<label for="paymenttype_promo">
									Promo 
									</label>
								</div>
								<div class="radio mb-1 d-inline-block mr-2">
									@if($getdataEditCampaign->PROMO_PAYMENT_TYPE_ID == 2)
									<input type="radio" id="paymenttype" name="paymenttype" value="2" checked data-parsley-multiple="">
									@else
									<input type="radio" id="paymenttype" name="paymenttype" value="2"  data-parsley-multiple="">
									@endif
									<label for="paymenttype">
									Bonus
									</label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<label for="">Rule</label>
							<select class="custom-select" id="promotype" name="promotype">
								@if($getdataEditCampaign->PROMO_TYPE_ID!=1)
								<option value="1" selected>Fixed</option>
								@else
									@if($getdataEditCampaign->PROMO_TYPE_ID == 1)
									<option value="1" selected>Fixed</option>
									@else
									<option value="1" >Fixed</option>	
									@endif
									@if($getdataEditCampaign->PROMO_TYPE_ID==2)
									<option value="2" selected>Percentage(%)</option>
									@else
										@if($getdataEditCampaign->PROMO_TYPE_ID!=1)
										<option value="2" >Percentage(%)</option>	
										@endif
									@endif
									
								@endif
							</select>
							
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6">
							<label for="">Promo Min value</label>
							
							@if($getdataEditCampaign->PROMO_MIN_VALUE!= '')
							<input type="text" class="form-control" id="promominvalue" name="promominvalue" placeholder="  " value="{{ $getdataEditCampaign->PROMO_MIN_VALUE }}" maxlength="5">
							@else
							<input type="text" class="form-control" id="promominvalue" name="promominvalue" placeholder="  " maxlength="5">
							@endif
						</div>
						<div class="form-group col-md-6">
							<label for="">Promo max value</label>
							@if($getdataEditCampaign->PROMO_MAX_VALUE!= '')
							<input type="text" class="form-control" id="promomaxvalue" name="promomaxvalue" placeholder="  " value="{{ $getdataEditCampaign->PROMO_MAX_VALUE }}" maxlength="5">
							@else
							<input type="text" class="form-control" id="promomaxvalue" name="promomaxvalue" placeholder="  " maxlength="5">
							@endif
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6">
							<label for="promovalue1">Promo value 1</label>
							
							<?php
							if(!empty($getdataEditCampaign->PROMO_VALUE1))
								$PromoValue11 = $getdataEditCampaign->PROMO_VALUE1;
							else
								$PromoValue11 = "";
							?>
							<input type="text" class="form-control" id="promovalue1" name="promovalue1" placeholder="  " value="{{ $PromoValue11 }}" maxlength="5">
							
						</div>
						<div class="form-group col-md-6">
							<label for="">Promo Value 2</label>
							<?php
							if(!empty($getdataEditCampaign->PROMO_VALUE2))
								$PromoValue22 = $getdataEditCampaign->PROMO_VALUE2;
							else
								$PromoValue22 = "";
							?>
							<input type="text" class="form-control" id="promovalue2" name="promovalue2" placeholder=" " value="{{ $PromoValue22 }}" maxlength="5">
							
						</div>
					</div>
				</div>
				@endif
				<!----end --->
				<!--- unique code tab--->
				
				@if($getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID == 5 || $getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID == 7 )	
				<div id="uniquecode-tab" class="">
						<div class="row">
							<div class="form-group col-md-6">
								<div class="custom-control custom-checkbox">
								@if($getdataEditCampaign->S_PROMO_CHIPS!=0)
								<input type="checkbox" class="custom-control-input" id="s_promochips" value="1" checked name="s_promochips">
								@else
								<input type="checkbox" class="custom-control-input" id="s_promochips" name="s_promochips">	
								@endif
								<label class="custom-control-label" for="s_promochips">Promo Chips</label>
								</div>
							</div>
							<div class="form-group col-md-6">
								<div class="custom-control custom-checkbox">
								@if($getdataEditCampaign->TOURNAMENT_ID!='' && $getdataEditCampaign->TOURNAMENT_ID!='0')
								<input type="checkbox" class="custom-control-input" id="s_tournamenttickets" value="1" checked name="s_tournamenttickets">
								@else
								<input type="checkbox" class="custom-control-input" id="s_tournamenttickets" name="s_tournamenttickets">
								@endif
								<label class="custom-control-label" for="s_tournamenttickets">Tournament tickets</label>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group col-md-6">
								<label for="">Promo  Amount</label>
								@php
								if(isset($getdataEditCampaign->S_PROMO_CHIP_VALUE) && !empty($getdataEditCampaign->S_PROMO_CHIP_VALUE)){
								
								 $S_PROMO_CHIP_VALUE =  $getdataEditCampaign->S_PROMO_CHIP_VALUE ;
								}else{
									 $S_PROMO_CHIP_VALUE = '' ;
								}
								@endphp
								
								<input type="text" class="form-control" id="s_promovalue" name="s_promovalue" value="{{ $S_PROMO_CHIP_VALUE }}" placeholder=" ">
							
							</div>
							<div class="form-group col-md-6" id="">
							<label for="">Tournament Tickets</label>
							<select class="custom-select"id="s_tournamentticketid" name="s_tournamentticketid">
							<option value="">-- Select --</option>
							<?php  foreach($tournaments as $tournament) {
								 if($tournament->TOURNAMENT_ID==$getdataEditCampaign->TOURNAMENT_ID)
									echo '<option value="'.$tournament->TOURNAMENT_ID.",".$tournament->TOURNAMENT_NAME.'" selected="selected">'.$tournament->TOURNAMENT_NAME.'</option>';
								 else
									echo '<option value="'.$tournament->TOURNAMENT_ID.",".$tournament->TOURNAMENT_NAME.'">'.$tournament->TOURNAMENT_NAME.'</option>';
								}
							?>
							</select>
							
							</div>
						</div>
						
				</div>
				@endif
				<!-- Refer friend setting tab-->
				@if($getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID == 2)
					<?php 
					 $typeId =  $getdataEditCampaign->PROMO_CAMPAIGN_TYPE_ID;
					 $rafCampTypeIds = array(2,5,6);
					 if(in_array($typeId,$rafCampTypeIds)){		 
						$referalUserName = $getdataEditCampaign->REFERRAL_USERNAME;
						$referalPer = $getdataEditCampaign->RAF_PERCENTAGE;
						
						if($referalUserName == ''){
						   $rafUsername   = '-';
						   $rafPercentage = '-'; 
						}else{
						   $rafUsername    = $referalUserName;
						   $rafPercentage  = $referalPer;
						}
					?>
				<div class="form-row"id="referafriendSetting" style="dispaly:none">
					<div class="form-group col-md-4">
						<label for="">RAF (%)</label>
						<input type="text" value="{{ $rafPercentage }}" class="form-control" readonly id="ref_precentage"name="ref_precentage" placeholder=" ">
						
					</div>
					<div class="form-group col-md-8">
						<label for="">Referral Username</label>
						<input type="text" class="form-control" value="{{ $rafUsername }}" id="ref_username" readonly name="ref_username" placeholder="  " >
						
					</div>
				</div>
					 <?php } ?>
				@endif
				<!-- end Refer friend setting tab-->
				<!--end tab 2-->
				<button href="javascript: void(0);" class="previousbtn btn btn-secondary waves-effect waves-light"><i class=" mdi mdi-skip-previous   mr-1"></i> Previous </button>
				<button class="nextBtn  btn btn-secondary waves-effect waves-light" type="button" >Next <i class=" mdi mdi-skip-next  ml-1"></i></button>
				</div>
				<!--tab - 3-->
				<div class="setup-content" id="step-3">
					<h5 class="font-18">Cost & User Range Settings </h5>
					<div class="row">
						<div class="form-group col-md-6">
							<label for="">Campaign Cost</label>
							<input type="text" value="{{ $getdataEditCampaign->COST }}" class="form-control" id="cost" name="cost" placeholder=" " maxlength="4">
							
						</div>
						<div class="form-group col-md-6">
							<label for="">Expected User</label>
							<input type="text" class="form-control" value="{{ $getdataEditCampaign->TOTAL_USER_EXPECTED }}" id="expecteduser" name="expecteduser" onChange="findCostPerUserValue()" placeholder=" " maxlength="4">
							
						</div>
					</div>


					<div class="row">
						<div class="form-group col-md-6">
							<label for="">Cost Per User</label>
							<input type="text" class="form-control" value="{{ $getdataEditCampaign->COST_PER_USER }}" id="costperuser" name="costperuser" placeholder="  " maxlength="4">
							
						</div>
						<!--<div class="form-group col-md-6" id="noofUniqueCode"s>
							<label for="noOfUniqueCode">No of Unique Code</label>
							<input type="text" class="form-control" id="noOfUniqueCode" name="noOfUniqueCode" placeholder="  " maxlength="5">
							
						</div>-->
					</div>

					<div class="row">
						<div class="form-group col-md-6">
							<label for="validationCustom01">Expected User Min</label>
							<input type="text" class="form-control" name="expusermin" id="expusermin" placeholder="  "value="{{ $getdataEditCampaign->EXP_USER_MIN }}" required="" maxlength="8" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
							
						</div>
						<div class="form-group col-md-6">
							<label for="validationCustom01">Expected User Max</label>
							<input type="text" class="form-control" id="expusermax" name="expusermax" value="{{ $getdataEditCampaign->EXP_USER_MAX }}" placeholder="  " required="" maxlength="8"onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
							
						</div>
					</div>
					<button href="javascript: void(0);" class="previousbtn btn btn-secondary waves-effect waves-light"><i class=" mdi mdi-skip-previous   mr-1"></i> Previous </button>
					<button type="submit" id="frmsubmitedit" name="frmsubmitedit"  value="Update" class="nextBtn btn btn-secondary waves-effect waves-light"><i class="fa fa-save mr-1"></i> Save</button>
				</div>
			</div> <!-- tab-content -->
		</div> <!-- end #progressbarwizard-->
		</form>
		</div> <!-- end card-body -->
	</div> <!-- end card-->
</div><!-- end row-->


@endsection
@section('post_js')
<script src="{{url('assets/libs/twitter-bootstrap-wizard/twitter-bootstrap-wizard.min.js')}}"></script>     
<script src="{{url('assets/js/pages/form-wizard.init.js')}}"></script>
<script src="{{ url('js/bo/editcampaign.js') }}"></script>  
@endsection