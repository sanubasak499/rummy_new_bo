@extends('bo.layouts.master')

@section('title', "Manage Campaign | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/nestable2/nestable2.min.css') }}" rel="stylesheet" />
 <!-- Sweet Alert-->
<link href="{{url('assets/libs/jquery-toast/jquery-toast.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.has-error input{ border: 1px #f00  solid }
.has-error span.error{ color: #f00;display:block }
span.error{ color: #f00;display:none; width: 100% }

    .ms-container{
        max-width: 650px;
    } 
    .InvalidRow {
        color:red;
    } 
    .ValidRow {
        color:green;
    }
    .formtext {
    font-size: 11px;
    font-style: italic;
    color: red;
    padding: 6px 0 0 0;
    margin: 0;
    line-height: 13px;
}
.dropdown-menu .inner.show{ overflow-y:scroll !important; max-height:180px !important; overflow-x:hidden !important}
 .dropdown-menu .inner.show ul{ overflow:hidden !important ;max-height:100% !important }     
</style>
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					<ol class="breadcrumb m-0">
						<li class="breadcrumb-item">Marketing</a></li>
						<li class="breadcrumb-item"><a href="{{ url('marketing/managecampaign')}}" >Manage Campaign</a></li>
						<li class="breadcrumb-item active">Create Campaign</li>
					</ol>
				</div>
				<h4 class="page-title">Create Campaign CD</h4>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-body">
		<div class="row mb-3">
			<div class="col-lg-8">
				<h5 class="font-18">Create Campaign </h5>
			</div>
			<div class="col-lg-4">
				<div class="text-lg-right mt-3 mt-lg-0">
					<a href="{{ url('marketing/managecampaign')}}" class="btn btn-dark waves-effect waves-light"><i class="fa fa-bars mr-1"></i> View Campaign</a>
				</div>
			</div>
		</div>
		
		<div class="stepwizard">
			<ul class="stepwizard-row setup-panel nav nav-pills bg-light nav-justified  m-0 mb-3">
				<li class="stepwizard-step nav-item">
					<a href="#step-1" type="button" class=" active nav-link rounded-0 pt-2 pb-2 active"><i class=" mdi mdi-account-details mr-1"></i><span class="d-none d-sm-inline">Campaign Details</span></a>
					
				</li>
				<li class="stepwizard-step nav-item">
					<a href="#step-2" type="button" class=" btndefault nav-link rounded-0 pt-2 pb-2"  disabled="disabled" ><i class=" mdi mdi-account-details mr-1"></i><span class="d-none d-sm-inline">Payment & Rule Settings</span></a>
					
				</li>
				<li class="stepwizard-step nav-item">
					<a href="#step-3" type="button" class=" btndefault nav-link rounded-0 pt-2 pb-2" disabled="disabled"><i class=" mdi mdi-account-details mr-1"></i><span class="d-none d-sm-inline">Cost & User Range Settings</span></a>
					
				</li>
				<li class="stepwizard-step nav-item">
					<a href="#step-4" type="button" class=" btndefault nav-link rounded-0 pt-2 pb-2" disabled="disabled"><i class=" mdi mdi-account-details mr-1"></i><span class="d-none d-sm-inline">Assign Users</span></a>
				</li>
			</ul>
		</div>
		<form id="createcampaign" method="post" action="{{ route('marketing.managecampaign.createcampaign') }}" data-parsley-validate=""novalidate="" enctype="multipart/form-data">
			@csrf
				<div class="setup-content" id="step-1">

				<div class="form-row">
					<div class="form-group col-md-4">
						<label for="campaignType">Campaign type<span class="text-danger">* </span></label>
						<select class="custom-select @error('campaignType') parsley-error @enderror" id="campaignType" name="campaignType" required>
						<option value="" selected="selected">-- Select --</option>
						
						@foreach($campaignType as $key => $campaign)
						@php
							$affpartnertypeid = array(2,5);
							$partner_type_id  = Auth::user()->partner->PARTNER_TYPE_ID;
						@endphp
						@if(in_array($partner_type_id,$affpartnertypeid))
							@if($campaign->PROMO_CAMPAIGN_TYPE_ID == 2)
							<option value="{{ $campaign->PROMO_CAMPAIGN_TYPE_ID}}"> {{ $campaign->PROMO_CAMPAIGN_TYPE }}</option>
							
							@endif
						@else
						
						<option value="{{$campaign->PROMO_CAMPAIGN_TYPE_ID}}">{{ $campaign->PROMO_CAMPAIGN_TYPE}}</option>
						
						@endif
						
						@endforeach
						</select>
						@error('campaignType')
							<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
						@enderror
						<div class="valid-feedback" style="color:#f1556c;font-family: Roboto, sans-serif;
						font-size: .875rem;
						font-weight: 400;">
						Please select campaign type 
						</div>
					</div>
					<div class="form-group col-md-4">
					<label for="partnername">Affiliate Name<span class="text-danger">* </span></label>
					<select class="custom-select @error('partner_name') parsley-error @enderror" id="partner_name" name="partner_name" required >
						<option value="">-- Select --</option>
						@foreach($getPartnerNames as $key=> $partnerNames)
						<option value="{{$partnerNames->PARTNER_ID}}">{{ $partnerNames->PARTNER_NAME }}</option>
						@endforeach
					</select>
					@error('partner_name')
						<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
					@enderror
					
					</div>
					<div class="form-group col-md-4">
						<label for="validationCustom01">Created By:</label>
						<input type="text" value="{{ Auth::user()->FULLNAME }}" class="form-control" id="created_by" name="created_by" placeholder="  " readonly>
						
					</div>
				</div> 
				<div class="form-row" id ="campaigncodeurl">
					<div class="form-group col-md-4">
						<label for="campaigncode">Campaign Code<span class="text-danger">* </span></label>
						<div class="input-group">
							<input type="text" class="form-control "id="campaigncode"name="campaigncode" placeholder=""  maxlength="16" required>
							<span class="error">Please enter campaign code</span>
								<div class="input-group-append" id="refresh_btn" >
									<button class="btn btn-blue waves-effect waves-light" type="button" onclick="refreshBTN()">
										<span class="icon-refresh black-text"></span>
									</button>
								</div>
						</div>
						
					</div>
					<div class="form-group col-md-8">
						<label for="campaignurl" id="campaignurlhead">Campaign URL<span class="text-danger">* </span></label>
						<label for="" id="default_campaignurlhead"style="display:none">Generate Code</label>
							<div class="input-group">
							<input type="text" id="campaignurl" name="campaignurl" class="form-control " placeholder="" aria-label="">
								<div class="input-group-append"id="generate-button">
									<button class="btn btn-blue waves-effect waves-light" id="ccodeGenerate" type="button" onclick="generateCampaignUrl()">Generate</button>
								</div>
							</div>
					</div>
				</div>
				<div class="row">
				
					<div class="form-group col-md-4">
						<label for="campaignname">Campaign Name<span class="text-danger">* </span></label>
						<input  type="text" class="form-control @error('campaignname') is-invalid @enderror" id="campaignname" name="campaignname" maxlength="55" placeholder=" " required value="{{ old('campaignname') }}" >
						<span class="error">Enter Campaign Name </span>
						@error('campaignname')
							<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
						@enderror
					</div>
				
					<div class="col-md-4">
						<div class="form-group">
							<label for="campaign_description">Campaign Description</label>
							<input type="text" class="form-control" id="campaign_description" name="campaign_description" placeholder="  " maxlength="50" rows="5" >
						</div>
					</div>	
					<div class="col-md-4" id="withdrawl_setting">
						<div class="form-group">
							<label for="withdrawl_criteria">Withdrawal Criteria</label>
							<input type="text" class="form-control" id="withdrawl_criteria" name="withdrawl_criteria" placeholder="  " maxlength="50" rows="5" >
						</div>
					</div>
				
				</div>
				<div class="row">
					<div class="col-md-4">
					<div class="row">
						<div class="form-group col-md-4">
							<label for="campaignstatus">Campaign Status</label>
							<div>
							<label class="switches">
							<input type="checkbox" name="campaignstatus" checked>
							<span class="slider round"></span>
							</label>
							</div>
						</div>
						
						<div class="form-group col-md-4">
							<label for="">Is Featured</label>
							<div>
							<label class="switches">
							<input type="checkbox" id="campaignfeature" name="campaignfeature">
							<span class="slider round"></span>
							</label>
							</div>
						</div>
						<div class="form-group col-md-4">
							<label for="">Display Status</label>
							<div>
							<label class="switches">
							<input type="checkbox" name="display_status" checked>
							<span class="slider round"></span>
							</label>
							</div>
						</div>
					</div>
					</div>
					
					<div class="col-md-4" id="remark_setting">
						<div class="form-group">
							<label for="campaign_remark">Remark</label>
							<textarea class="form-control" id="campaign_remark" name="campaign_remark" placeholder="  " rows="5" /></textarea>
						</div>
					</div>
					<div class="col-md-4">
					<div class="customDatePickerWrapper">
						<div class="form-group mb-2"> 
							<label for="startdate">Start Date<span class="text-danger">* </span></label>
							<div class="input-group"> <input type="text" id="startdate" name="startdate" class="form-control customDatePicker from dateicons @error('startdate') is-invalid @enderror" placeholder="Select Start Date"  required="" value="{{ old('startdate') }}">
							<span class="error">
							Select the campaign start date
							</span>
							
							</div>
							@error('startdate')
                                    <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                            @enderror
						</div>
						<div class="form-group "> 
							<label for="enddate">End Date<span class="text-danger"></span></label>
							<div class="input-group">
							<input type="text" id="disable-datepicker" id="enddate" name="enddate" class="form-control customDatePicker to dateicons" placeholder="Select End Date"  value="{{ old('enddate') }}">
							
							
							</div>
							
						</div>
					</div>
					</div>
				</div>
				<button class="nextBtn btn btn-secondary waves-effect waves-light" type="button" >Next <i class=" mdi mdi-skip-next  ml-1"></i></button>
				</div>
				<!--tab - 2-->
				<div class="setup-content" id="step-2">
				<div id="Payment-first-deposite-tab">
					<h5 class="font-18">Campaign Type - Payment | First deposit  </h5>

					<div class="form-group">
					<label class="mb-1 d-block">Payment Type:</label>
					<div class="radio mb-1 d-inline-block mr-2">
						<input type="radio" id="payment_type_deposite_money" name="payment_type" value="1" checked="checked" data-parsley-multiple="">
						<label for="payment_type_deposite_money">
						Deposit Money
						</label>
					</div>
					<div class="radio d-inline-block mb-1">
						<input type="radio"  id="payment_type_promo_money" name="payment_type"value="2" >
						<label for="payment_type_promo_money">
						Promo Money
						</label>
					</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input check-pay-group" id="p_promochips" name="p_promochips">
							<label class="custom-control-label " for="p_promochips">Promo Chips</label>
							</div>
						</div>
						<div class="form-group col-md-4">
							<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input check-pay-group" id="p_unclaimedbonus" name="p_unclaimedbonus">
							<label class="custom-control-label" for="p_unclaimedbonus">Unclaimed Bonus</label>
							</div>
						</div>
						<div class="form-group col-md-4">
							<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" id="p_tournamenttickets" name="p_tournamenttickets">
							<label class="custom-control-label" for="p_tournamenttickets">Tournament Tickets</label>
							</div>
						</div>
					</div>
					<div class="form-row">
					<div class="form-group col-md-4">
						<label for="p_promovalue">Promo (%)</label>
						<input type="text" class="form-control" id="p_promovalue" name="p_promovalue" max="100" min="0">
						
					</div>
					<div class="form-group col-md-4" id="promo-amount" style="display:none">
						<label for="">Promo Amount</label>
						<input type="text" class="form-control" id="promo_amount" name="promo_amount" placeholder="  " />
						
					</div>
					<div class="form-group col-md-4">
						<label for="">Netbanking (%)</label>
						<input type="text" class="form-control" id="p_netbankingvalue"name="p_netbankingvalue" max="100" min="0">
					</div>
					<div class="form-group col-md-4 multiplecustom" id="tournament-tab">
						<label for="">Tournament Tickets</label>
						<select class="form-control selectpicker multiselecttextoverflow" multiple data-style="btn-light" data-live-search="true"  data-placeholder="select" id="p_tournamentticketid" name="p_tournamentticketid[]" >
							@foreach($tournaments as $key => $tournamentNames)
							<option value="{{ $tournamentNames->TOURNAMENT_ID }}">{{ $tournamentNames->TOURNAMENT_NAME }}</option>
							@endforeach
						</select>
						
					</div>
				 
					</div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="">Promo Capping</label>
							<input type="text" class="form-control" id="p_promocapvalue"name="p_promocapvalue" placeholder="  " maxlength="6">
							
						</div>
						<div class="form-group col-md-4">
							<label for="">Cradit /Dabit Card (%)</label>
							<input type="text" class="form-control" id="p_creditdebitcardvalue" name="p_creditdebitcardvalue" max="100" min="0">
							
						</div>
						<div class="form-group col-md-4">
							<label for="">UCB Capping</label>
							<input type="text" class="form-control" id="p_bonuscapvalue" name="p_bonuscapvalue" placeholder="" maxlength="6">
							
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="m_depositamount">Minimum Deposit Amount</label>
							<input type="text" class="form-control" id="m_depositamount" name="m_depositamount" placeholder="  "  maxlength="7">
							
						</div>
						<div class="form-group col-md-4">
							<label for="">Max Deposit Amount</label>
							<input type="text" class="form-control" id="max_depositamount"name="max_depositamount" placeholder="  "  maxlength="7">
							
						</div>
						<div class="form-group col-md-4">
							<label for="">Max Coins Required </label>
							<input type="text" class="form-control" id="coins_required"name="coins_required" placeholder="  " maxlength="7">
							
						</div>
					</div>
					
				</div>
				
				<!--Registration and tell a friend-->
				<div id ="registration-tab">
					<h5 class="font-18">Campaign Type - Registration  | Tell - a - Friend  </h5>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="mb-1 d-block">Payment Type<span class="text-danger">* </span>:</label>
								<div class="radio d-inline-block mb-1">
									<input type="radio" id="paymenttype_promo" name="paymenttype" value="1" data-parsley-multiple="genders">
									<label for="paymenttype_promo">
									Promo 
									</label>
								</div>
								<div class="radio mb-1 d-inline-block mr-2">
									<input type="radio" id="paymenttype_bonus" name="paymenttype" value="2"  data-parsley-multiple="genders">
									<label for="paymenttype_bonus">
									Bonus
									</label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<label for="">Rule<span class="text-danger">* </span></label>
							<select class="custom-select" id="promotype" name="promotype">
								<option value="1">Fixed</option>
							</select>
							
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6">
							<label for="">Promo Min value</label>
							<input type="text" class="form-control" id="promominvalue" name="promominvalue" placeholder="  "  maxlength="5">
							
						</div>
						<div class="form-group col-md-6">
							<label for="">Promo max value</label>
							<input type="text" class="form-control" id="promomaxvalue" name="promomaxvalue" placeholder="  " maxlength="5">
							
						</div>
					</div>
					<div class="row">
						<div class="form-group col-md-6">
							<label for="promovalue1">Promo value 1</label>
							<input type="text" class="form-control" id="promovalue1" name="promovalue1" placeholder="  " maxlength="5">
							
						</div>
						<div class="form-group col-md-6">
							<label for="">Promo Value 2</label>
							<input type="text" class="form-control" id="promovalue2" name="promovalue2" placeholder="  " maxlength="5">
							
						</div>
					</div>
				</div>
				<!----end --->
					<!--- unique code tab--->
				<div id="uniquecode-tab" class="">
						<div class="row">
							<div class="form-group col-md-6">
								<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="s_promochips" name="s_promochips">
								<label class="custom-control-label" for="s_promochips">Promo Chips</label>
								</div>
							</div>
							<div class="form-group col-md-6">
								<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input" id="s_tournamenttickets" name="s_tournamenttickets">
								<label class="custom-control-label" for="s_tournamenttickets">Tournament Tickets</label>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group col-md-6">
								<label for="">Promo  Amount</label>
								<input type="text" class="form-control" id="s_promovalue" name="s_promovalue" placeholder="  ">
								
							</div>
							<div class="form-group col-md-6 multiplecustom" id="">
							<label for="">Tournament Tickets</label>
							<select class="form-control selectpicker multiselecttextoverflow"  data-live-search="true" id="s_tournamentticketid" name="s_tournamentticketid">
								<option value="">Select</option>
								@foreach($tournaments as $key => $tourNames)
								<option value="{{ $tourNames->TOURNAMENT_ID }}">{{ $tourNames->TOURNAMENT_NAME }}</option>
								@endforeach
							</select>
							
							</div>
						</div>
						
				</div>
				<!-- Refer friend setting tab-->
				<div class="form-row"id="referafriendSetting" style="dispaly:none">
						<div class="form-group col-md-4">
							<label for="">RAF (%)</label>
							<input type="text" class="form-control" id="ref_precentage" name="ref_precentage" max="100" min="0">
							
						</div>
						<div class="form-group col-md-8">
							<label for="">Referral Username</label>
							<input type="text" class="form-control" id="ref_username" name="ref_username" placeholder="  " >
							
						</div>
				</div>
				<!-- end Refer friend setting tab-->
				<!--end tab 2-->
				<button class="previousbtn pull-right btn btn-secondary waves-effect waves-light"><i class=" mdi mdi-skip-previous   mr-1"></i> Previous </button>
				<button class="nextBtn  btn btn-secondary waves-effect waves-light" type="button" >Next <i class=" mdi mdi-skip-next  ml-1"></i></button>
				</div>
				<!--tab - 3-->
				<div class="setup-content" id="step-3">
					<h5 class="font-18">Cost & User Range Settings </h5>
					<div class="row">
						<div class="form-group col-md-6">
							<label for="">Campaign Cost</label>
							<input type="text" class="form-control" id="cost" name="cost" placeholder=" " maxlength="4">
							
						</div>
						<div class="form-group col-md-6">
							<label for="">Expected User</label>
							<input type="text" class="form-control" id="expecteduser" name="expecteduser" onChange="findCostPerUserValue()" placeholder=" " maxlength="4">
							
						</div>
					</div>


					<div class="row">
						<div class="form-group col-md-6">
							<label for="">Cost Per User</label>
							<input type="text" class="form-control" id="costperuser" name="costperuser" placeholder="  " maxlength="4">
							
						</div>
						<div class="form-group col-md-6" id="noofUniqueCode"s>
							<label for="noOfUniqueCode">No of Unique Code</label>
							<input type="text" class="form-control" id="noOfUniqueCode" name="noOfUniqueCode" placeholder="  " maxlength="5">
							
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-6">
							<label for="validationCustom01">Expected User Min<span class="text-danger">* </span></label>
							<input type="text" class="form-control" name="expusermin" id="expusermin" placeholder="  " required="" maxlength="8"onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
							
						</div>
						<div class="form-group col-md-6">
							<label for="validationCustom01">Expected User Max<span class="text-danger">* </span></label>
							<input type="text" class="form-control" id="expusermax" name="expusermax" placeholder="  " required="" maxlength="8"onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
							
						</div>
					</div>
					<button href="javascript: void(0);" class="previousbtn btn btn-secondary waves-effect waves-light pull-right"><i class=" mdi mdi-skip-previous   mr-1"></i> Previous </button>
					<button class="nextBtn  btn btn-secondary waves-effect waves-light" type="button" >Next <i class=" mdi mdi-skip-next  ml-1"></i></button>
				</div>
				<div class="setup-content" id="step-4">
				   <div class="row mb-2" id="download_sam_for" style="">
					  <div class="form-group col-md-8">
					  <label>Assign Users</label>
						 <div class="input-group">
							<input type="file" name= "excel_camp" id="excel_camp" class="form-control">
							
						 </div>
						 <div id="excelError"></div>
					  </div>
					  <div class="col-md-4 text-md-center text-sm-left" style="margin-top: 26px">
					
					  <a href="{{ asset('/data/bo/sample-campaign-by-username.xlsx') }}"
						  type="button" class="btn btn-success waves-effect waves-light d-inline-block ">
						 <i class="fas fa-download  mr-3"></i> Download Sample Format
					 </a>
					  </div>
				   </div>
				   <div class="row mb-2" id="download_sam_for_regist" style="display:none">
					  <div class="form-group col-md-8">
					  <label>Assign Contact Numbers</label>
						<div class="input-group">
							<input type="file" name= "excel_camp_regist" id="excel_camp_regist" class="form-control" />
						</div>
						<div id="excelErrorReg"></div>
						</div>
					  <div class="col-md-4 text-md-center text-sm-left" style="margin-top: 26px">
					  <a href="{{ asset('/data/bo/sample-campaign-by-usermobile.xlsx') }}"
						  type="button" class="btn btn-success waves-effect waves-light d-inline-block ">
						 <i class="fas fa-download  mr-3"></i> Download Sample Format
						 </a>
					  </div>
				   </div>
				   <button href="javascript: void(0);" class="previousbtn btn btn-secondary waves-effect waves-light pull-right"><i class=" mdi mdi-skip-previous   mr-1"></i> Previous </button>
				   <button type="button" id="frmsubmit" class="nextBtn btn btn-secondary waves-effect waves-light" ><i class="fa fa-save mr-1"></i> Save</button>
				</div>
			</div> <!-- tab-content -->
		</div> <!-- end #progressbarwizard-->
		</form>
		<!--model for excel show data-->
<!---- model box for user excel-->

<div id="accordion-modal-user" class="modal fade show" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="" aria-modal="true">
 <div class="modal-dialog modal-lg">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Upload users</h5>
            </div>
            <div class="card-body" id="">
               <table class="basic-datatable- table dt-responsive nowrap w-100" id="basic-datatable-modal">
					<thead class="thead-light">
						<tr>
							<th>Sr no.</th>
							<th>Username</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody id="getUserExcelData">
					</tbody>
				</table>
               <button type="button" class="card-button btn btn-warning waves-effect waves-light mr-1" data-dismiss="modal"><i class="fa fa-upload mr-1"></i> Reupload</button>
               
			   <button type="button" id="confirmExcelUpload" name="confirmExcelUpload" class="nextBtn btn btn-secondary waves-effect waves-light" ><i class="fa fa-save mr-1"></i>Continue Anyway</button>
            </div>
         </div>
      </div>
   </div>
</div>
 
 <!---- End model box for user excel-->
<!---model for excel mobile number upload-->
<div id="accordion-modal-contact" class="modal fade show" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="" aria-modal="true">
 <div class="modal-dialog modal-lg">
      <div class="modal-content bg-transparent shadow-none">
        <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                  <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Upload Contact Numbers</h5>
            </div>
            <div class="card-body" id="">
               <table  class="basic-datatable- table dt-responsive nowrap w-100" id="basic-datatable-modal-contact">
					<thead class="thead-light">
						<tr>
							<th>Sr no.</th>
							<th>Contact Number</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody id="getUserExcelDataContact">
					</tbody>
				</table>
               <button type="button" class="card-button btn btn-warning waves-effect waves-light mr-1" data-dismiss="modal"><i class="fa fa-upload mr-1"></i> Reupload</button>
               
			   <button type="button" id="confirmExcelUploadContact" name="confirmExcelUploadContact" class="nextBtn btn btn-secondary waves-effect waves-light" ><i class="fa fa-save mr-1"></i>Continue Anyway</button>
            </div>
        </div>
      </div>
   </div>
</div>
<!---model for excel mobile number upload-->

</div> <!-- end card-body -->
@endsection
@section('js')
<!---<script src="{{url('assets/libs/parsleyjs/parsley.min.js')}}"></script>-->
<script src="{{asset('assets/libs/twitter-bootstrap-wizard/twitter-bootstrap-wizard.min.js')}}"></script> <script src="{{ asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
<script src="{{asset('assets/js/pages/form-wizard.init.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>

<script src="{{ asset('js/bo/createcampaign.js') }}"></script>  
<script>
function generateCampaignUrl(){
	if($('#campaignType').val() =="" || $('#campaignType').val() == null ) {
		
		$('.valid-feedback').show();
		return false;
	}
	var campaignType = $("#campaignType").val();
	var campaigncode = $("#campaigncode").val();
	$.ajax({
		   "url": "{{url('/marketing/managecampaign/getCampaignURL')}}",
		   "method":"POST",
		   "data":{"CampaignType":campaignType,"campaigncode":campaigncode},
		   "dataType":"json",
		  success:function(data){
			  $('.valid-feedback').hide();
			  $("#campaigncode").val(data.gcampaignCODE);
			  $("#campaignurl").val(data.gcampaignURL);
			}
	});
}

</script>
@endsection