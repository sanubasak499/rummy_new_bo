@extends('bo.layouts.master')

@section('title', "Manage Campaign | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/nestable2/nestable2.min.css') }}" rel="stylesheet" />
 <!-- Sweet Alert-->
<link href="{{url('assets/libs/jquery-toast/jquery-toast.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{url('assets/libs/jquery-toast/jquery-toast.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('post_css')
<style>
    .ms-container{
        max-width: 650px;
    } 
    .InvalidRow {
        color:red;
    } 
    .ValidRow {
        color:green;
    }
    .formtext {
    font-size: 11px;
    font-style: italic;
    color: red;
    padding: 6px 0 0 0;
    margin: 0;
    line-height: 13px;
}
      
</style>
@endsection
@section('content')

   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item">Marketing</li>
                     <li class="breadcrumb-item"><a href="{{ url('marketing/managecampaign')}}">Manage Campaign </a></li>
                     <li class="breadcrumb-item active">View Bulk Campaign</li>
                  </ol>
               </div>
               <h4 class="page-title">View Bulk Campaign </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <div class="row">
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign Name: <span class="text-danger">{{ $campaign_info->PROMO_CAMPAIGN_NAME }}</span></h4>
                     </div>
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign Type: <span class="text-danger">{{ $campaign_type_id->PROMO_CAMPAIGN_TYPE }}</span></h4>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign Start Date: <span class="text-danger">{{ $campaign_info->START_DATE_TIME }}</span></h4>
                     </div>
					 <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Campaign End Date: <span class="text-danger">{{ $campaign_info->END_DATE_TIME }}</span></h4>
                     </div>
                     
                     <div class="col-md-4">
                        <h4 class="font-15 mt-0"> Created By: <span class="text-danger">{{ $partner_type_id->PARTNER_NAME }}</span></h4>
                     </div>
                  </div>
                  <p class="text-muted font-13 mb-2"> </p>
                
                  <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                    <thead>
                        <tr>
                           <th>Sr no.</th>
                           <th>Campaign Name</th>
                           <th>Unique Code</th>
                           <th>Status</th>
                           <th>Created Date</th>
                         </tr>
                    </thead>
					
                    <tbody>
					@if(isset($bulk_promo_data) && !empty($bulk_promo_data))
					@php
					$i = 1;
					@endphp
					
					@foreach($bulk_promo_data as $data)
					<tr>
						<td>{{ $i++ }}</td>
						<td>{{ $data->PROMO_CAMPAIGN_NAME }}</td>
						<td>{{ $data->BULK_UNIQUE_CODE }}</td>
						@if($data->STATUS == '0')
							<td>Used</td>
						@else
							<td>{{ $data->USER_ID }}</td>
						@endif
						<td>{{ $data->CREATED_DATE}}</td>
					</tr>
					@endforeach
					@endif
                    </tbody>
                  </table>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
   </div>

@endsection
@section('js')
<script src="{{url('assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>   
<script src="{{url('assets/js/pages/form-wizard.init.js')}}"></script>
<script>
$(document).ready(function(){
	$(".modalclose").click(function(){
		$('#accordion-modal').hide();
		// alert('dasdsa');
	});
});
   
$(document).ready(function(){
	
	$( "#valid-excel-data" ).validate({
		  rules: {
			name: {
			  required: true,
			  remote: {
				url: "{{url('/marketing/managecampaign/editDataExcel')}}",
				type: "post",
				//dataType:"json",
				data: {
				  username: function() {
					return $( "#name" ).val();
				  }
				}
			  }
			}
		  },
		    messages: {
				name: {
					remote: "username doesn't exist"
				}
			}
		});
		
////validation close
	$("#update_submit").click(function(){
		$(this).closest('form').submit();
	})
	$.ajaxSetup({
	   headers: {
		   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	   }
})
///setup close

});//close document

function getData(id){
	var dataString = 'id=' +id;
		$.ajax({
		   "url": "{{url('/marketing/managecampaign/editDataExcel')}}",
		   "method":"post",
			dataType: "json",
			data: dataString,
			success:function(data){
				$('#name').val(data.name);
				$("#edituserss").find('input[name="cus_id"]').val(id);
				$("#edituserss").find('input.error').removeClass('error');
				$("#edituserss").find('label.error').remove();
			},
			
		});
}
function dltData(id){
	var dataString = 'id=' +id;
		$.ajax({
		   "url": "{{url('/marketing/managecampaign/deleteDataExcel')}}",
		   "method":"post",
			dataType: "json",
			data: dataString,
			success:function(data){

			},
			
		});
}
</script>
@endsection