@extends('bo.layouts.master')

@section('title', "FTD Report | PB BO")

@section('pre_css')
 
@endsection

@section('content')

<!-- start page title -->
<div class="row">
    <div class="col-12">
       <div class="page-title-box">
          <div class="page-title-right">
             <ol class="breadcrumb m-0">
                <li class="breadcrumb-item active">Marketing</li>
                <li class="breadcrumb-item active">FTD Report</li>
             </ol>
          </div>
          <h4 class="page-title">FTD Report</h4>
       </div>
    </div>
 </div>
<!-- end page title --> 


<div class="card">
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-sm-4">
               <h5 class="font-18">FTD Report </h5>
            </div>
         </div>
         <form id="ftdReport_form" action="{{ url('/marketing/ftdreport') }}" method="post">
             @csrf
            <div class="form-row customDatePickerWrapper">
                <div class="form-group col-lg-5 col-md-4 col-sm-4">
                    <label>From: </label>
                    <input name="date_from" type="text" class="form-control customDatePicker from  @error('date_from') is-invalid @enderror" placeholder="Select From Date" value="">
                    @error('date_from')
                        <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                    @enderror
                </div>
                <div class="form-group  col-lg-5 col-md-4 col-sm-4">
                    <label>To: </label>
                    <input name="date_to" type="text" class="form-control customDatePicker to @error('date_to') is-invalid @enderror" placeholder="Select To Date" value="">
                    @error('date_to')
                        <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                    @enderror
                </div>
                <div class="form-group col-lg-2 col-md-4 col-sm-4">
                    <label class="text-white d-none d-sm-block d-md-block">.</label>
                    <button type="submit" data-toggle="modal" data-target="#setuserflagpopup" class="w-100 btn btn-success waves-effect waves-light d-inline-block mr-1">
                       <i class="mdi mdi-file-excel mr-1"></i> Export(.xlsx)
                    </button>
                </div>
            </div>
        </form> 
    </div>
</div> <!-- end card-->

@endsection

@section('js')

<script>
    $("#ftdReport_form").validate({
        rules: {
          // simple ;rule, converted to {required:true}
          date_from: {
            required:true
          },
          // compound rule
          date_to: {
            required: true,
          }
        }
      });

</script>
@endsection