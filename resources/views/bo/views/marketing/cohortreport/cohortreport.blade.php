@extends('bo.layouts.master')

@section('title', "Cohort Report | PB BO")

@section('pre_css')
 
@endsection

@section('content')

<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a >Markting</a></li>
                     <li class="breadcrumb-item active">Cohort Report</li>
                  </ol>
               </div>
               <h4 class="page-title">
                  Cohort Report
               </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                   <form id="cohortReport_form" onsubmit="onSubmit()" action="{{ url('/marketing/cohortreport/') }}" method="post">
                    @csrf  
        
                     <div class="row  customDatePickerWrapper">
                        <div class="form-group col-md-4">
                           <label>From: </label>
                           <input name="date_from" id="date_from" type="text" class="form-control customDatePicker1 from flatpickr-input" placeholder="Select From Date" value="{{ (old('date_from'))?old('date_from'):'' }}" readonly="readonly">
                           @error('date_from')
                            <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                           @enderror
                        </div>
                        <div class="form-group col-md-4">
                           <label>To: </label>
                           <input name="date_to" id="date_to" type="text" class="form-control customDatePicker1 to flatpickr-input" placeholder="Select To Date" value="{{ (old('date_to'))?old('date_to'):'' }}" readonly="readonly">
                           @error('date_to')
                            <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                           @enderror
                        </div>
                        <div class="form-group col-md-4">
                           <label>Date Range: </label>
                           <select name="date_range" id="date_range1"  class="customselect" data-plugin="customselect" >
                              <option data-default-selected="" value="" selected="">Select Date Range</option>
                              <option value="1">Today</option>
                              <option value="2">Yesterday</option>
                              <option value="3">This Week</option>
                              <option value="4">Last Week</option>
                              <option value="5">This Month</option>
                              <option value="6">Last Month</option>
                           </select>
                        </div>
                     </div>
        
                     <button type="submit" id="submit_btn" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-save mr-1"></i> Download</button>
                     <button type="reset" class="btn btn-secondary waves-effect waves-light"><i class="fas fa-eraser mr-1"></i> Clear</button>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
         </div>
         <!-- end col -->
      </div>
     

   </div>
</div>
@endsection

@section('post_js')
<script>
    var $customDatePickerWrapper = $('.customDatePickerWrapper');
    $(document).ready(function(){
//        var $customDatePickerWrapper = $('.customDatePickerWrapper');
       var $customDatePickerFrom = $customDatePickerWrapper.find(".customDatePicker1.from");
                let flatpikcerInstanceFrom = $customDatePickerFrom.flatpickr({
                    autoclose: true,
                    dateFormat: 'd-M-Y H:i:s',
                     maxDate: new Date(),
                    enableTime: true,
                    changeMonth: true,
                    changeYear: true,
                    defaultHour: 0,
                    defaultMinute: 0,
                    defaultSecond: 0,
                    minuteIncrement: 1,
                    time_24hr: true,
                    enableSeconds: true,
                    disableMobile: "true",
                    onClose: function(selectedDates, dateStr, instance) {
                        let minDate = new Date(dateStr).setHours("00", "00", "00");

                        // $customDatePickerWrapper.find(".customDatePicker.to").flatpickr("minDate", minDate).focus();
//                        setTimeout(function(){
//                            let customDatePickerFrom = $customDatePickerWrapper.find(".customDatePicker1.to");
//                            var flatpickrFrom = customDatePickerFrom.data('flatpickr');
//                            flatpickrFrom.set("minDate", minDate);
//                            flatpickrFrom.set("maxDate", ((new Date(dateStr)).setDate((new Date(dateStr)).getDate() + 30)));
//                            flatpickrFrom.set("maxDate", ((new Date(dateStr)).setDate((new Date(dateStr)).getDate() + 30)));
//                        },1000);

                    },
                })
                $customDatePickerFrom.data('flatpickr', flatpikcerInstanceFrom);

                var $customDatePickerTo = $customDatePickerWrapper.find(".customDatePicker1.to");
                let flatpikcerInstanceTo = $customDatePickerTo.flatpickr({
                    autoclose: true,
                    dateFormat: 'd-M-Y H:i:s',
                     maxDate: new Date(),
                    enableTime: true,
                    changeMonth: true,
                    changeYear: true,
                    defaultHour: 23,
                    defaultMinute: 59,
                    minuteIncrement: 1,
                    time_24hr: true,
                    enableSeconds: true,
                    disableMobile: "true",
                    onClose: function(selectedDates, dateStr, instance) {
//                        let maxDate = new Date(dateStr).setHours("00", "00", "00");
//                        // $customDatePickerWrapper.find(".customDatePicker.from").flatpickr('maxDate', maxDate);
//                        let customDatePickerTo = $customDatePickerWrapper.find(".customDatePicker1.from");
//                        var flatpickrTo = customDatePickerTo.data('flatpickr');
//                        flatpickrTo.set("maxDate", maxDate);
                    },
                }); 
                $customDatePickerTo.data('flatpickr', flatpikcerInstanceTo);
    });
    $(document).ready(function(){
        $("#date_from,#date_to").change(function(){
            $("ul.parsley-errors-list").remove();
        });
        
        $("#date_range1").change(function(){
            var val = $(this).val();
            
            var from = $customDatePickerWrapper.find(".customDatePicker1.from");
            var to = $customDatePickerWrapper.find(".customDatePicker1.to");
            var flatpickrFromObj = from.data('flatpickr');
            var flatpickrToObj = to.data('flatpickr');
            
            if(val==='1'){
                from.val(moment().format("DD-MMM-Y 00:00:00"));
                to.val(moment().format("DD-MMM-Y 00:00:00"));
            }else if(val==='2'){
                from.val(moment().subtract(1, 'day').format("DD-MMM-Y 00:00:00"));
                to.val(moment().subtract(1, 'day').format("DD-MMM-Y 00:00:00"));
            }else if(val==='3'){
                from.val(moment().startOf('week').format("DD-MMM-Y 00:00:00"));
                to.val(moment().format("DD-MMM-Y 00:00:00"));
            }else if(val==='4'){
                from.val(moment().subtract(1, 'weeks').startOf('week').format("DD-MMM-Y 00:00:00"));
                to.val(moment().subtract(1, 'weeks').endOf('week').format("DD-MMM-Y 00:00:00"));
            }
            else if(val==='5'){
                from.val(moment().format("01-MMM-Y 00:00:00"));
                to.val(moment().format("DD-MMM-Y 00:00:00"));
            }
            else if(val==='6'){
                from.val(moment().subtract(1, 'month').startOf('month').format("DD-MMM-Y 00:00:00"));
                to.val(moment().subtract(1, 'month').endOf('month').format("DD-MMM-Y 00:00:00"));
            }
        });
    });

    function onSubmit(){
        $("ul.parsley-errors-list").remove();
        $("#submit_btn").prop('disabled', true);
            setTimeout(function(){ $("#submit_btn").prop('disabled', false); }, 10000);
        return true;
    }
    
</script>
@endsection