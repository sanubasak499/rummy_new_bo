<div style="width: 600px; background-color: #2a2a2a; display: block; margin: 0 auto -9px auto; position: relative; overflow: hidden; color: #fff; font-family: Arial, Helvetica, sans-serif;padding-top: 2px;">
    <div style="width: 554px; margin: 2px 3px 0px 3px !important; background-color: #fff; padding: 5px 20px; height: 86px; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; -ms-border-radius: 3px; -o-border-radius: 3px;">
        <img
            src="http://pokerbaazi.com/images/Home_logo.png"
            border="0"
            style="margin-top: 10px;"
        />
        <span style="float: right; font-family: Arial; font-weight: bold; display: inline-block; font-size: 18px; color: #2a2a2a; line-height: 86px;">
            info@pokerbaazi.com
        </span>
    </div>
    <span style="width: 135px; height: 33px; display: block; margin: 0px auto 20px auto;">
        <img
            src="http://pokerbaazi.com/images/dwnarw.png"
            border="0"
            style="float: left;"
        />
    </span>
    <div style="width: 100%;">
        <p style="text-align: center; font-size: 32px; font-weight: bold; margin: 5px 0 0px 0;">
            DEPOSIT SUCCESS
        </p>
        <div style="width: 554px; padding: 20px 23px;">
            <form>
                <h3 style="font-size: 26px; font--weight: bold; color: #fff;">
                    Dear {{ $data->username }},
                </h3>
                <span style="font-size: 14px; font-family: Arial; margin-top: 7px; display: block; font-weight: normal; line-height: 18px;">
                    Thank you for purchasing {{ $data->amount }} at PokerBaazi.com.
                </span>
                <br />
                <span style="font-size: 14px; font-family: Arial; margin-top: 7px; display: block; font-weight: normal; line-height: 18px;">
                    Following are your deposit details:
                </span>
                <br />
                <span style="font-size: 14px; font-family: Arial; margin-top: 7px; display: block; font-weight: normal; line-height: 18px;">
                    Merchant Name : BAAZI NETWORKS PRIVATE LIMITED
                </span>
                <span style="font-size: 14px; font-family: Arial; margin-top: 7px; display: block; font-weight: normal; line-height: 18px;">
                    Merchant Order ID: {{ $data->merchant_id }}
                </span>
                <span style="font-size: 14px; font-family: Arial; margin-top: 7px; display: block; font-weight: normal; line-height: 18px;">
                    Transaction Amount: Rs. {{ $data->amount }}
                </span>
                <span style="font-size: 14px; font-family: Arial; margin-top: 7px; display: block; font-weight: normal; line-height: 18px;">
                    Transaction Date: {{ date('d/m/Y',strtotime($data->transaction_date)) }}
                </span>
                <span style="font-size: 14px; font-family: Arial; margin-top: 7px; display: block; font-weight: normal; line-height: 18px;">
                    Transaction Time: {{ date('H:i:s',strtotime($data->transaction_date)) }}
                </span>
                <p> </p>
                <span style="font-size: 14px; font-family: Arial; margin-top: 7px; display: block; font-weight: normal; line-height: 18px;">
                    In case of any queries, please contact our banking team at
                    <a
                        href="mailto:banking@pokerbaazi.com"
                        style="font-size: 14px; color: #fff478; font-weight: bold; margin-top: 8px; display: inline-block;"
                    >
                        banking@pokerbaazi.com
                    </a>
                    . We hope you enjoy playing at PokerBaazi.com, India's most
                    trusted poker website.
                </span>
                <h4 style="font-size: 20px; font-weight: bold; margin-top: 35px; margin-bottom: 0px; color: #fff;">
                    Challenge your Skills!
                </h4>
                <span style="font-size: 14px; font-family: Arial; margin-top: 7px; display: block; font-weight: normal; line-height: 18px;">
                    Regards,
                </span>
                <span style="font-size: 14px; font-family: Arial; margin-top: 7px; display: block; font-weight: normal; line-height: 18px;">
                    PokerBaazi Team
                </span>
                <div style="width: 100%;">
                    <br />
                    <a href="https://www.pokerbaazi.com/" target="_blank">
                        <img
                            src="http://pokerbaazi.com/images/add_cash.png"
                            border="0"
                            width="20%"
                            style="margin-top:15px;margin-bottom: 0; margin-left: 10%; "
                        />
                    </a>
                    <a href="http://www.pokerbaazi.com/" target="_blank">
                        <img
                            src="http://pokerbaazi.com/images/download_now.png"
                            border="0"
                            width="20%"
                            style="margin-top:15px;margin-bottom: 0; margin-left: 5%; margin-right: 5%;"
                        />
                    </a>
                    <a href="http://www.pokerbaazi.com/" target="_blank">
                        <img
                            src="http://pokerbaazi.com/images/play_now.png"
                            border="0"
                            width="20%"
                            style="margin-top:15px;margin-bottom: 0; margin-right: 10%; "
                        />
                    </a>
                </div>
                <div style="margin: 40px auto 0 auto; text-align: center; width: 553px;">
                    <span style="font-size: 14px; font-family: Arial; margin-top: 7px; display: block; font-weight: normal; line-height: 18px; text-align: left;">
                        <br /> Disclaimer: This is an automated message.
                    </span>
                    <span style="font-size: 14px; font-family: Arial; margin-top: 7px; display: block; font-weight: normal; line-height: 18px; text-align: left;">
                        You are receiving this email because a user created an
                        account with this email address. If you are the owner of
                        this email address and did not create the PokerBaazi
                        account, just ignore this message and the account will
                        remain inactive
                    </span>
                </div>
                <div style="width: 100%;">
                    <br />
                    <table width="100%">
                            <tr>
                                <td  width="30%">
                                    <h4 style=" font-size: 20px; color: #fff;">
                                        Follow Us On
                                    </h4>
                                </td>

                                <td  width="70%">
                                    <table border="0">
                                        <tr>
                                            <td>
                                                <a
                                                    href="https://www.facebook.com/BaaziGames?fref=ts"
                                                    target="_blank"
                                                >
                                                    <img
                                                        src="http://pokerbaazi.com/images/facebook_icon_2.png"
                                                        
                                                        style="margin-left:-5px !important;"
                                                    />
                                                </a>
                                            </td>
                                            <td>   
                                                <a
                                                    href="https://twitter.com/PokerBaazi"
                                                    target="_blank"
                                                >
                                                    <img
                                                        src="http://pokerbaazi.com/images/twitter_icon_2.png"
                                                        
                                                        style="margin-left: 5%; "
                                                    />
                                                </a>
                                            </td>
                                            <td>    
                                                <a
                                                    href="https://www.youtube.com/channel/UC9i7bPNKr15Io0eRR9cqExA"
                                                    target="_blank"
                                                >
                                                    <img
                                                        src="http://pokerbaazi.com/images/youtube_icon_2.png"
                                                        
                                                        style="margin-left: 5%; "
                                                    />
                                                </a>
                                            </td>    
                                        </tr>    
                                    </table>

                                </td>
                            </tr>
                    </table>
                </div>
            </form>
        </div>
    </div>
    <img
        src="http://pokerbaazi.com/images/email-bottom.png"
        border="0"
        style="margin-bottom: 0;"
    />
</div>
