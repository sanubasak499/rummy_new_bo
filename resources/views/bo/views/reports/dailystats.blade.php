@extends('bo.layouts.master')

@section('title', "Daily Stats Report | PB BO")

@section('pre_css')


<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/nestable2/nestable2.min.css') }}" rel="stylesheet" />
 
<!-- Plugins css -->
<link href="{{ url('assets/libs/jquery-nice-select/jquery-nice-select.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet" type="text/css" />


<style>.nice-select.customselect{ width:180px}</style
>@endsection

@section('content')

<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="">Reports</a></li>
                     <li class="breadcrumb-item active">Daily Stats</li>
				</ol>
               </div>
               <h4 class="page-title">Daily Stats Report</h4>
            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-dark text-white">
                  <div class="card-widgets">
                     <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>                                   
                  </div>
                  <h5 class="card-title mb-0 text-white">Summary</h5>
               </div>
			   <div id="cardCollpase7" class="collapse show">
                  <div class="card-body">
					<form id="reportdata" enctype='multipart/form-data' method="post" action="{{ url('reports/dailystats') }}">
						@csrf
						<input type="hidden" id="submit_type" value="1" name="yesterday_report" />
						<input type="hidden" id="date" value="1" name="date" />
					</form>
					
                     <div class="row">
					 
						<div class="form-group customDatePickerWrapper col-lg-6 col-md-6 col-sm-6">
						<label>Date  </label>
						<input  id="selectdate" name="selectdate" type="text" class="form-control customDatePicker from dateicons   @error('date') is-invalid @enderror" placeholder="Date" value="{{ $selected_date ?? ''}}">
							@error('date')
								<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
							@enderror
						</div>
                        <div class="col-sm-3 mb-1 text-center">
						<label for="selectdate" class="white-text d-block">.</label>
                           <button type="button" id="get_yesterday_report" name="getdailystats" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i>Get Daily Stats Report</button>
						    
                        </div>
						
                        <div class="col-sm-3 mb-1 ">
						<label for="selectdate" class="white-text d-block">.</label>
						<!--<button type="button" id="getmaillist" data-toggle="modal" data-target="#setuserflagpopup" class="btn btn-success waves-effect waves-light d-inline-block mr-1">
                        <i class="mdi mdi-email-outline mr-1" ></i> Send Mail
                        </button>-->
                           <div class="form-group d-inline-block">
                           
                              <div class="d-inline-block">
                                 <select name="select_type" id="select_type" style="width: 200px;"  class=" customselect   formatedDateRange" data-plugin="customselect">
                                    <option data-default-selected="" value="" selected="">Export to: PDF, Excel</option>
                                    <option value="2">PDF</option>
                                    <option value="3">Excel</option>
                                 </select>
                              </div>
							  
                           </div>
                        </div>
						
                        <!-- end col-->
                     </div>
					
					@if(isset($searchData) && !empty($searchData))
                     <table  class="table dt-responsive nowrap w-100">
                        <thead>
                           <tr>
                              <th>Stakes</th>
                              <th>T.Hold'em Gr. Rev.</th>
                              <th>T. Hold'em Nt. Rev.</th>
                              <th>4 Card Gr. Rev.</th>
                              <th>4 Card Nt. Rev.</th>
                              <th>5 Card Gr. Rev.</th>
                              <th>4 Card Nt. Rev.</th>
                              <th>OFC Gr. Rev.</th>
                              <th>OFC Nt. Rev.</th>
                           </tr>
                        </thead>
                        <tbody>
						
						@foreach($searchData as $key => $search_data)
						<tr>
							<td>{{ trim($search_data->BIG_BLIND)}}</td>
							<td>{{ $search_data->Texas_Holdem_Gross_Revenue }}</td>
							<td>{{ $search_data->Taxs_Holdem_Net_Revenue }}</td>
							<td>{{ $search_data->Omaha_Gross_Revenue }}</td>
							<td>{{ $search_data->Omaha_Net_Revenue }}</td>
							<td>{{ $search_data->{'5card_Gross_Revenue'} }}</td>
							<td>{{ $search_data->{'5card_Net_Revenue'} }}</td>
							<td>{{ $search_data->Ofc_Gross_Revenue }}</td>
							<td>{{ $search_data->Ofc_Net_Revenue }}</td>
						</tr>
						@endforeach
						@endif
                        </tbody>
                     </table>
						@if(isset($countdata) && !empty($countdata))
						<div class="border-bottom my-3"></div>
                    <h5 class="font-17 mt-0 text-center"><u>Report's Summary</u></h5>
                    <div class=" my-3"></div>
					
					@foreach($countdata as $key => $reportdatacount)
					<div class="row mt-3">
                        <div class="col-md-4 mb-3">
                           <h5 class="font-15 mt-0"><u>Revenue Report</u></h5>
							<table class="b-0 w-100 d-inline">
                              <tr>
                                 <td class="py-1 text-black"><strong>Total Gross Revenue</strong></td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">{{ $reportdatacount->U_GROSS_REVENUE ?? '' }}</td>
                              </tr>
                              <tr>
                                 <td class="py-1 text-black"><strong>Total Net Revenue</strong></td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">{{ $reportdatacount->U_NET_REVENUE ?? ''}}</td>
                              </tr>
                              <tr>
                                 <td class="py-1 text-black"><strong>Total Successful Deposits</strong></td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">{{ $reportdatacount->U_TOTAL_SUCCESSFUL_DEPOSIT ?? ''}}</td>
                              </tr>
							</table>
                        </div>
                        <div class="col-md-4 mb-3">
							<h5 class="font-15 mt-0"><u>Total Withdrawals</u></h5>
							<table class="b-0 w-100 d-inline">
                              <tr>
                                 <td class="py-1 text-black"><strong>Pending</strong></td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">{{ $reportdatacount->U_WITHDRAW_PENDING_AMOUNT ?? ''}}</td>
                              </tr>
                              <tr>
                                 <td class="py-1 text-black"><strong>Approved</strong></td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">{{ $reportdatacount->U_WITHDRAW_SUCCESSFUL_AMOUNT ?? ''}}</td>
                              </tr>
                              <tr>
                                 <td class="py-1 text-black"><strong>Total </strong></td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">{{ $reportdatacount->U_WITHDRAW_TOTAL_AMOUNT ?? ''}}</td>
                              </tr>
							</table>
                        </div>
                        <div class="col-md-4 mb-3">
                           <h5 class="font-15 mt-0"><u>First Time Deposits</u></h5>
                           <table class="b-0 w-100 d-inline">
                              <tr>
                                 <td class="py-1 text-black"><strong>Total First Time Depositors</strong></td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">{{ $reportdatacount->U_TOTAL_FIRST_DEPOSIT }}</td>
                              </tr>
                               
                              <tr>
                                 <td class="py-1 text-black"><strong>Total Registration(s)</strong></td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">{{ $reportdatacount->U_NEW_REGISTRATION ?? ''}}</td>
                              </tr>                            

                           </table>
                        </div>
                     </div>
					 @endforeach
					@endif
                     <!--<div class="row">
                        <div class="col-md-6 mb-3">
                           <h5 class="font-15 mt-0">Tournaments Summary</h5>
                           <table class="b-0 w-100 d-inline">
                              <tr>
                                 <td class="py-1 text-black"><strong>Total Revenue</strong></td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">Amount</td>
                              </tr>
                              <tr>
                                 <td class="py-1 text-black"><strong>Total Tournaments</strong></td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">Count</td>
                              </tr>
                              <tr>
                                 <td class="py-1 text-black"><strong>Overlays</strong></td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">Amount</td>
                              </tr>
                              <tr>
                                 <td class="py-1 text-black"><strong>Promo Tickets Amount</strong></td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">Amount</td>
                              </tr>
                              <tr>
                                 <td class="py-1 text-black"><strong>Net Earnings Profit/Loss</strong> </td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">Amount</td>
                              </tr>
                           </table>
                        </div>
                        <div class="col-md-6 mb-3">
                           <h5 class="font-15 mt-0">Free Rolls Summary</h5>
                           <table class="b-0 w-100 d-inline">
                              <tr>
                                 <td class="py-1 text-black"><strong>Total Prizes Given</strong></td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">Amount</td>
                              </tr>

                              <tr>
                                 <td class="py-1 text-black"><strong>Total Users Played</strong> </td>
                                 <td class="p-1 text-black">:</td>
                                 <td class="p-1 text-black-50">Amount</td>
                              </tr>
                              
                           </table>
                        </div>
               
                     </div>-->

                  </div>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
   </div>
</div>


@endsection

@section('js')
<!---  Nice Select menu -->
    <script src="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.js') }}"></script>
    <script src="{{ asset('assets/libs/switchery/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
    <script src="{{ asset('assets/libs/multiselect/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
	
    <!-- Init js-->
    <script src="{{url('assets/js/pages/form-advanced.init.js') }}"></script>
	<script>
		$("#get_yesterday_report").click(function(){
			
			
			$("#date").val($("#selectdate").val());
			$("#submit_type").val('1');
			$("#reportdata").submit();
			
		});
		$("#select_type").change(function(){
			var x = $(this).val();
			if(x != '' && x != null ){
				$("#submit_type").val(x);
				$("#date").val($("#selectdate").val());
				$("#reportdata").submit();
			}

		});	
		
		$("#getmaillist").click(function(){
			$.ajax({
				   "url": "{{url('/reports/dailystats/receipientmaillist')}}",
				   "method":"POST",
				   "data":{"id":$(this).data('id')},
				   "dataType":"json",
				   success:function(data){
					  //alert(data);
					  
				}
			});
			});
		$("#ftdReport_form").validate({
        rules: {
          // simple ;rule, converted to {required:true}
          selectdate: {
            required:true
          },
          // compound rule
          // date_to: {
            // required: true,
          // }
        }
      });
			$(document).ready(function(){
				$("#selectdate").change(function(){
					var val = $("#selectdate").val();
					if(val != "" && val != null){
						$("#selectdate").removeClass("is-invalid");
						$(".parsley-errors-list").remove();
					}
				});
			});
	</script>
<!---  Nice Select menu -->
@endsection

