@extends('bo.layouts.master')

@section('title', "Tournament Report | PB BO")

@section('pre_css')
<link href="{{ url('assets/libs/bootstrap-table/bootstrap-table.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/nestable2/nestable2.min.css') }}" rel="stylesheet" />
 
<!-- Plugins css -->
<link href="{{ url('assets/libs/jquery-nice-select/jquery-nice-select.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet" type="text/css" />


<style>.nice-select.customselect{ width:180px}</style
>@endsection

@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="#">Tournaments </a></li>
                     <li class="breadcrumb-item active"> Reports</li>
                  </ol>
               </div>
               <h4 class="page-title">
                  Tournaments Report
               </h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
            
                  <form id="userSearchForm" enctype='multipart/form-data' method="post" action="
				  {{ url('reports/tournamentreport') }}">
     
					@csrf
                     <div class="row  customDatePickerWrapper">
                        <div class="form-group col-md-4">
                           <label>From: </label>
                           <input name="date_from" id="date_from" type="text" class="form-control customDatePicker from flatpickr-input @error('date_from') is-invalid @enderror" placeholder="Select From Date" value="" readonly="readonly">
						   @error('date_from')
								<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
							@enderror
                        </div>
                        <div class="form-group col-md-4">
                           <label>To: </label>
                           <input name="date_to" id="date_to" type="text" class="form-control customDatePicker to flatpickr-input @error('date_to') is-invalid @enderror" placeholder="Select To Date" value="" readonly="readonly">
						   @error('date_to')
								<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
							@enderror
                        </div>
                        <div class="form-group col-md-4">
                           <label>Date Range: </label>
                           <select name="date_range" id="date_range" class="formatedDateRange customselect" data-plugin="customselect">
                              <option data-default-selected="" value="" selected="">Select Date Range</option>
                              <option value="1">Today</option>
                              <option value="2">Yesterday</option>
                              <option value="3">This Week</option>
                              <option value="4">Last Week</option>
                            <!--  <option value="5">This Month</option>
                              <option value="6">Last Month</option>-->
                           </select>
                        </div>
                     </div>
                     <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                     <a href="{{ url('reports/tournamentreport') }}" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                  </form>
               </div>
               <!-- end card-body-->
            </div>


         </div>
         <!-- end col -->
      </div>
	  {{-- <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-dark text-white">
                  <div class="card-widgets">
                     <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>   
                     
                  </div>
                  <h5 class="card-title mb-0 text-white">Tournament Report</h5>
               </div>

                <div id="cardCollpase7" class="collapse show">
				<div class="card-body">
                  <div class="row mb-2">
                     <div class="col-sm-9">
                        <!---<h5 class="font-15 mt-0">Total Affiliates: <span class="text-danger">XX</span> </h5>--->
                     </div>
                     <div class="col-sm-3">
                        <div class="text-sm-right">
                           <button type="button" class="btn btn-light mb-1"><i class="fas fa-file-excel" style="color: #34a853;"></i> Export</button>
                        </div>
                     </div>
                  </div>
				<table data-toggle="table" data-show-columns="false" data-page-list="[10]"data-page-size="8" data-buttons-class="xs btn-light" data-pagination="true" class="table-borderless table custopwithapen">
                     <thead  class="thead-light">
                        <tr>
                           <th>ID</th>
                           <th> Name</th>
                           <th>Start  Time</th>
                           <th>Buy  In</th>
                           <th>Rebuy  In</th>
                           <th>ADDON</th>
                           <th>RG. COUNT</th>
                           <th>REBUYS COUNT</th>                         
                           <th>ADDONS COUNT</th>
                           <th>CASH TICKETS COUNT</th>
                           <th>SATELLITE TICKETS COUNT</th>
                           <th>PROMO TICKETS_COUNT</th>
                           <th>GTD PRIZE</th>
                           <th>PRIZE GIVEN</th>
                           <th>ENTRY RECEIVED</th>
                           <th>Profit/Loss</th>
                           <th>Net profit</th>
                           <th>Gross rake</th>
                           <th>Rebuy/Registration</th>
                           <th>Extra amount given</th>
                           <th>Free ticket amount</th>
                           <th>GTD X</th>
                           <th>Extra Amount X</th>
                           <th>Time</th>
                           <th>UserList</th>
                           <th>Code Tickets Count</th>
                           
                        </tr>
                     </thead>
                     <tbody>
                        
                       <tr>
                     
                        </tr>
                     </tbody>
                  </table>
                  <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" class="mt-2">
                     <div>More Records</div>
                     <div>

                     </div>
                  </div>
               </div> <!-- end card body--></div>
            </div> <!-- end card -->
         </div><!-- end col-->
      </div> --}}
 </div>
</div>



	
<style type="text/css">
.custopwithapen{ border: 0  !important }
.custopwithapen thead th {vertical-align: bottom; border-bottom: 2px solid #dee2e6 !important;}
.custopwithapen td{border-top: 1px solid #dee2e6 !important;}
</style>
​@endsection

@section('js')
<!-- Bootstrap Tables js -->
	<script src="{{ asset('assets/libs/bootstrap-table/bootstrap-table.min.js') }}"></script>
<!---  Nice Select menu -->
    <script src="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.js') }}"></script>
    <script src="{{ asset('assets/libs/switchery/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
	
    <!-- Init js-->
    <script src="{{url('assets/js/pages/form-advanced.init.js') }}"></script>
	<script src="{{url('assets/js/pages/bootstrap-tables.init.js') }}"></script>
<!---  Nice Select menu -->
<script>
$(document).ready(function(){
				$("#date_from,#date_to").change(function(){
					var val1 = $("#date_from").val();
					var val2 = $("#date_to").val();
					if(val1 != "" && val1 != null){
						$("#date_from").removeClass("is-invalid");
						$(".parsley-errors-list").remove();
					}
					if(val2 != "" && val2 != null){
						$("#date_to").removeClass("is-invalid");
						$(".parsley-errors-list").remove();
					}
				});
			});
</script>
@endsection