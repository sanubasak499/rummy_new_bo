@extends('bo.layouts.master')

@section('title', "Commission Withdraw Report | PB BO")

@section('pre_css')
<!-- <link href="{{ asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs//rwd-table/rwd-table.min.css') }}" rel="stylesheet" type="text/css" /> -->
<!-- Plugins css -->
<link href="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('post_css')
<style>
   .ms-container {
      max-width: 650px;
   }

   .completeblur {
      overflow: hidden;
   }

   .completeblur .content {
      position: relative;
   }

   .completeblur .content:after {
      position: absolute;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      content: ""
   }

   .completeblur .content .container-fluid {
      filter: blur(3px);
      -webkit-filter: blur(3px);
   }

   .completeblur .content .spinerss {
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      z-index: 999;
      display: block
   }

   .spinerss {
      display: none
   }

   /* stadus css */
   .stadus ul {
      margin: 0 -10px;
      padding: 0 0 5px 0;
   }

   .stadus ul li {
      float: left;
      padding: 2px 10px;
      font-size: 0.875rem;
      font-weight: 500;
      font-family: Roboto, sans-serif;
      color: #343a40;
      line-height: 12px;
      list-style: none
   }

   .stadus ul li+li {
      border-left: 2px #000 solid;
   }

   .mobilelessmargin {
      margin-top: 10px;
      position: relative
   }

   @media (min-width: 768px) {

      .mobilelessmargin {
         margin-top: -33px;
         ;
      }


   }
</style>

@endsection
@section('content')
<!-- start page title -->
<div id="completeblur" class="">
   <div class="content">
      <div class="spinerss">
         <div class="d-flex justify-content-center ">
            <div class="spinner-border" role="status"></div>
         </div>
      </div>
      <div class="container-fluid">
         <div class="row">
            <div class="col-12">
               <div class="page-title-box">
                  <div class="page-title-right">
                     <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="">Home</a></li>
                        <li class="breadcrumb-item active">Commission Withdrawal Report</li>
                     </ol>
                  </div>
                  <h4 class="page-title">
                     Commission Withdrawal Report
                  </h4>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-body">
                     <form id="withdrawSearchForm" action="{{ route('reports.commission_withdraw.search') }}{{ !empty($params) ? empty($params['page']) ? '' : "?page=".$params['page'] : '' }}" method="post">
                        @csrf
                        <div class="form-row align-items-center">
                           <div class="form-group col-md-2">
                              <label class="mb-1">Search By: </label>
                           </div>
                           <div class="form-group col-md-4">
                              <div class="radio radio-info form-check-inline">
                                 <input data-default-checked="" type="radio" id="search_by_username" value="username" name="search_by" checked="">
                                 <label for="search_by_username"> Username </label>
                              </div>
                              <div class="radio radio-info form-check-inline">
                                 <input type="radio" id="search_by_email" value="email" name="search_by">
                                 <label for="search_by_email"> Email </label>
                              </div>
                              <div class="radio radio-info form-check-inline">
                                 <input type="radio" id="search_by_contact_no" value="contact_no" name="search_by">
                                 <label for="search_by_contact_no"> Contact No </label>
                              </div>
                           </div>
                           <div class="form-group col-md-6">
                              <label class="sr-only" for="username">Search Input</label>
                              <input type="text" class="form-control" name="username" id="username" placeholder="Search by Username, Email & Contact No" value="{{ !empty($params) ? $params['username']  : ''  }}">
                           </div>
                        </div>
                        <div class="row  align-items-center">
                           <div class="form-group col-md-4">
                              <label>Internal Reference No: </label>

                              <input type="text" class="form-control" name="ref_no" id="ref_no" value="{{ !empty($params) ? $params['ref_no']  : ''  }}">
                           </div>
                           <div class="form-group col-md-4 multiplecustom">
                              <label>Status:</label>
                              <select name="withdraw_status[]" class="form-control selectpicker" multiple data-style="btn-light">
                                 {{-- <option value="" selected="selected">Select Status</option> --}}
                                 <option value="109" {{ !empty($params['withdraw_status']) ? in_array('109',$params['withdraw_status'])? "selected" : ''  : ''  }}>
                                    Pending</option>
                                 <option value="208" {{ !empty($params['withdraw_status']) ? in_array('208',$params['withdraw_status'])? "selected" : ''  : ''  }}>
                                    Approved</option>
                                 <option value="209" {{ !empty($params['withdraw_status']) ? in_array('209',$params['withdraw_status'])? "selected" : ''  : ''  }}>
                                    Paid</option>
                                 <option value="216" {{ !empty($params['withdraw_status']) ? in_array('216',$params['withdraw_status'])? "selected" : ''  : ''  }}>
                                    Failed</option>
                                 <option value="112" {{ !empty($params['withdraw_status']) ? in_array('112',$params['withdraw_status'])? "selected" : ''  : ''  }}>
                                    Rejected</option>
                                 <option value="254" {{ !empty($params['withdraw_status']) ? in_array('254',$params['withdraw_status'])? "selected" : ''  : ''  }}>
                                    Initiated</option>
                              </select>
                           </div>
                           <div class="form-group col-md-4">
                              <label>Transfer ID:</label>
                              <input type="text" class="form-control" name="tranfer_id" id="tranfer_id" value="{{ !empty($params) ? $params['tranfer_id']  : ''  }}">
                           </div>
                        </div>
                        <div class="row">
                           <div class="form-group col-md-4">
                              <label>Approve Type: </label>
                              <select name="approve_type" class="customselect" data-plugin="customselect">
                                 <option value="" selected="selected">Select Type</option>
                                 <option value="normal" {{ !empty($params) ? $params['approve_type'] == 'normal' ? "selected" : ''  : ''  }}>
                                    Normal</option>
                                 <option value="payu" {{ !empty($params) ? $params['approve_type'] == 'payu' ? "selected" : ''  : ''  }}>
                                    Payu</option>
                                 <option value="cashfree" {{ !empty($params) ? $params['approve_type'] == 'cashfree' ? "selected" : ''  : ''  }}>
                                    Cashfree</option>
                                 <!-- <option value="rbl" {{ !empty($params) ? $params['approve_type'] == 'rbl' ? "selected" : ''  : ''  }}>RBL</option> -->
                              </select>
                           </div>
                           <div class="form-group col-md-4">
                              <label>1st Withdrawal Status: </label>
                              <select name="first_withdraw" class="customselect" data-plugin="customselect">
                                 <option value="" selected="selected">Select 1st Withdrawal Status</option>
                                 <option value="yes" {{ !empty($params) ? $params['first_withdraw'] == 'yes' ? "selected" : ''  : ''  }}>
                                    Yes</option>
                                 <option value="no" {{ !empty($params) ? $params['first_withdraw'] == 'no' ? "selected" : ''  : ''  }}>
                                    No</option>
                              </select>
                           </div>
                           <div class="form-group col-md-4">
                              <label>
                                 Flag: </label>
                              <select name="user_flag" class="customselect" data-plugin="customselect">
                                 <option value="" selected="selected">Select Flag</option>
                                 <option value="1" {{ !empty($params) ? $params['user_flag'] == '1' ? "selected" : ''  : ''  }}>
                                    Yellow</option>
                                 <option value="2" {{ !empty($params) ? $params['user_flag'] == '2' ? "selected" : ''  : ''  }}>
                                    Green</option>
                                 <option value="3" {{ !empty($params) ? $params['user_flag'] == '3' ? "selected" : ''  : ''  }}>
                                    Red</option>
                              </select>
                           </div>


                        </div>
                        <div class="row  customDatePickerWrapper">
                           <div class="form-group col-md-4">
                              <label>From: </label>
                              <input name="date_from" type="text" class="form-control customDatePicker from" placeholder="Select From Date" value="{{ !empty($params) ? $params['date_from'] : \Carbon\Carbon::today()->setTime("00","00","00")->format('d-M-Y H:i:s') }}">

                           </div>
                           <div class="form-group col-md-4">
                              <label>To: </label>
                              <input name="date_to" type="text" class="form-control customDatePicker to" placeholder="Select To Date" value="{{ !empty($params) ? $params['date_to'] : \Carbon\Carbon::today()->setTime("23","59","59")->format('d-M-Y H:i:s') }}">
                           </div>
                           <div class="form-group col-md-4">
                              <label>Date Range: </label>
                              <select name="date_range" id="date_range" class="formatedDateRange customselect" data-plugin="customselect">
                                 <option data-default-selected="" value="" selected="">Select Date Range
                                 </option>
                                 <option value="1" {{ !empty($params) ? $params['date_range'] == '1' ? "selected" : ''  : ''  }}>
                                    Today</option>
                                 <option value="2" {{ !empty($params) ? $params['date_range'] == '2' ? "selected" : ''  : ''  }}>
                                    Yesterday</option>
                                 <option value="3" {{ !empty($params) ? $params['date_range'] == '3' ? "selected" : ''  : ''  }}>
                                    This Week</option>
                                 <option value="4" {{ !empty($params) ? $params['date_range'] == '4' ? "selected" : ''  : ''  }}>
                                    Last Week</option>
                                 <option value="5" {{ !empty($params) ? $params['date_range'] == '5' ? "selected" : ''  : ''  }}>
                                    This Month</option>
                                 <option value="6" {{ !empty($params) ? $params['date_range'] == '6' ? "selected" : ''  : ''  }}>
                                    Last Month</option>
                              </select>
                           </div>
                        </div>
                        <div class="row">


                           <div class="form-group col-md-4">
                              <label>Checking status: </label>
                              <select name="check_pass" id="check_pass" class="formatedDateRange customselect" data-plugin="customselect">
                                 <option value="" selected="selected">Select Checking status</option>
                                 <option value="1" {{ !empty($params) ? $params['check_pass'] == '1' ? "selected" : ''  : ''  }}>
                                    Unchecked</option>
                                 <option value="2" {{ !empty($params) ? $params['check_pass'] == '2' ? "selected" : ''  : ''  }}>
                                    Passed</option>
                                 <option value="3" {{ !empty($params) ? $params['check_pass'] == '3' ? "selected" : ''  : ''  }}>
                                    Not Passed</option>
                              </select>
                           </div>
                        </div>
                        <button type="submit" class="btn btn-primary waves-effect waves-light btn-hide"><i class="fas fa-search mr-1"></i> Search</button>
                        <a href="{{ route('reports.commission_withdraw.index') }}" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                     </form>

                     <div class="float-right mobilelessmargin"><button type="button" id="payuStatusUpdate" class="btn btn-primary waves-effect waves-light mb-1 mr-1"><i class="fe-check-square mr-1"></i>Payu Status Update</button>
                        <button type="button" id="cashfreeStatusUpdate" class="btn btn-primary waves-effect waves-light mb-1 mr-1"><i class="fe-check-square mr-1"></i>Cashfree Status Update</button></div>
                     <div class="clearfix "></div>
                  </div>
                  <!-- end card-body-->
                  <div class="col-sm-5">
                     <!-- <div id="payuStatusUpdateLoader" style="display:none">
                        <div class="d-flex justify-content-center hv_center">
                           <div class="spinner-border" role="status"></div>
                        </div>
                     </div> -->
                     <div class="text-sm-right">

                     </div>



                     <div id="cshfreeStatusUpdateLoader" style="display:none">
                        <div class="d-flex justify-content-center hv_center">
                           <div class="spinner-border" role="status"></div>
                        </div>
                     </div>
                     <div class="text-sm-right">

                     </div>
                  </div>
               </div>

            </div>
            <!-- end col -->
         </div>
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header bg-dark text-white">
                     <div class="card-widgets">
                        <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>
                     </div>
                     <h5 class="card-title mb-0 text-white">Commission Withdrawal Report List</h5>
                  </div>
                  <div id="cardCollpase7" class="collapse show">
                     <div class="card-body">
                        <div class="row mb-2">
                           <div class="col-sm-7">
                              <div class="stadus">
                                 <ul class="clearfix">
                                    @php
                                    $tobePaidAmount =0;
                                    if(!empty($totalToBePaid)){
                                    foreach($totalToBePaid as $totalToBePaid){
                                    $tobePaidAmount += $totalToBePaid->PAYMENT_TRANSACTION_AMOUNT;
                                    }
                                    }
                                    @endphp

                                    <li>Total of To Be Paid: <span class="text-danger">(@if(!empty($tobePaidAmount)){{ $tobePaidAmount }}
                                          @else {{ 0 }} @endif)</span></li>
                                    <li>Sum Of Selected Amount: <span class="text-danger" id="sumSelAmnt">(0)</span></li>
                                 </ul>
                              </div>
                           </div>

                           @if(!empty($tobePaidAmount) && $tobePaidAmount > 0 )
                           <div class="col-sm-5">
                              <div class="text-sm-right">
                                 <a href="{{ route('reports.commission_withdraw.getExcelExport') }}"><button type="button" class="btn btn-light mb-1"><i class="fas fa-file-excel" style="color: #34a853;"></i> Export
                                       Excel</button></a>
                              </div>
                           </div>
                           @endif
                        </div>
                        <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                           <thead>
                              <tr>
                                 <th>#</th>
                                 <th>Username</th>
                                 <th>Req. Amount</th>
                                 <th>TDS</th>
                                 <th>To Be Paid</th>
                                 <th>1<sub>st</sub></th>
                                 <th>Net Win</th>
                                 <th>Flag</th>
                                 <th>C/S</th>
                                 <th>Reward</th>
                                 <th>Date</th>
                                 <th>AD</th>
                                 <th>Pan</th>
                                 <th>Oth</th>
                                 <th>Ref No.</th>
                                 <th>Status</th>
                              </tr>
                           </thead>
                           <tbody>
                              @if(!empty($withdrawData))
                              @foreach($withdrawData as $allWithdrawData)
                              <tr>

                                 <td class="td-refNoCheckBoxSelect">
                                    <div class="checkbox checkbox-primary checkbox-single">
                                       @if(($allWithdrawData->PAYMENT_TRANSACTION_STATUS ==109 ||
                                       $allWithdrawData->PAYMENT_TRANSACTION_STATUS ==216 ) &&
                                       !empty($params['withdraw_status']) &&
                                       (in_array('109',$params['withdraw_status'])||
                                       in_array('216',$params['withdraw_status'])) &&
                                       !empty($params['approve_type']) && ($params['approve_type'] ==
                                       "normal" || $params['approve_type'] =="payu" ||
                                       $params['approve_type'] =="cashfree"))
                                       <input type="checkbox" id="refNoCheckBoxSelect" name="refNoCheckBoxSelect" class="checkbox" value="{{ $allWithdrawData->INTERNAL_REFERENCE_NO }}" aria-label="Single checkbox Two">
                                       @else
                                       <input type="checkbox" id="refNoCheckBoxSelect" class="checkbox" aria-label="Single checkbox Two" disabled>
                                       @endif
                                       <label></label>
                                 </td>

                                 <td><a href="{{ route('user.account.user_profile', encrypt($allWithdrawData->USER_ID)) }}" target="_blank">{{$allWithdrawData->USERNAME}}</a></td>
                                 <td>{{$allWithdrawData->PAYMENT_TRANSACTION_AMOUNT}}</td>
                                 <td>{{$allWithdrawData->COM_TDS ?? '0.00'}}</td>


                                 <td class="toBePaid">{{$allWithdrawData->PAYMENT_TRANSACTION_AMOUNT - $allWithdrawData->COM_TDS ?? '0.00' }}</td>
                                 <td class="td-withStatusUserId">
                                    <div class="withStatusRes" data-with-status-user-id="{{ $allWithdrawData->USER_ID }}">
                                       Loading..
                                    </div>
                                 </td>

                                 <td class="td-winUserId">
                                    <div class="winRes" data-user-id="{{ $allWithdrawData->USER_ID }}">
                                       Loading..
                                    </div>
                                 </td>

                                 <td>
                                    @if($allWithdrawData->user_flag == 2)
                                    <a href="#" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" data-cur-flag="{{ $allWithdrawData->user_flag }}" data-user-id="{{ $allWithdrawData->USER_ID }}" class="badge badge-success p-1 font-15 tooltips"><span class="tooltiptext">Set User Flag</span><i class="fe-flag"></i></a>
                                    @elseif($allWithdrawData->user_flag == 1)
                                    <a href="#" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" data-cur-flag="{{ $allWithdrawData->user_flag }}" data-user-id="{{ $allWithdrawData->USER_ID }}" class="badge badge-warning p-1 font-15 tooltips"><span class="tooltiptext">Set User Flag </span><i class="fe-flag"></i></a>
                                    @elseif($allWithdrawData->user_flag == 3)
                                    <a href="#" data-toggle="modal" title="Set User Flag" data-target="#setuserflagpopup" data-cur-flag="{{ $allWithdrawData->user_flag }}" data-user-id="{{ $allWithdrawData->USER_ID }}" class="badge badge-danger p-1 font-15 tooltips"><span class="tooltiptext">Set User Flag </span><i class="fe-flag"></i></a>
                                    @endif
                                 </td>
                                 <td class="comment-status">
                                    <div data-ref-number="{{ $allWithdrawData->INTERNAL_REFERENCE_NO }}">
                                       @if($allWithdrawData->CHECKING_STATUS == 1 || empty($allWithdrawData->CHECKING_STATUS))
                                       <input type="text" class="form-control p-1" style="width: 80px;" name="amount" placeholder="Unchecked" value="{{ $allWithdrawData->CHECKING_COMMENT }}">
                                       <a class="badge badge-warning tooltips"><i class="mdi mdi-check text-white"></i> <span class="tooltiptext">Unchecked</span></a>
                                       @elseif($allWithdrawData->CHECKING_STATUS == 2)
                                       <input type="text" class="form-control p-1" style="width: 80px;" name="amount" placeholder="Passed" value="{{ $allWithdrawData->CHECKING_COMMENT }}">
                                       <a class="badge badge-success tooltips"><i class="mdi mdi-check text-white"></i> <span class="tooltiptext">Passed</span></a>
                                       @elseif($allWithdrawData->CHECKING_STATUS == 3)
                                       <input type="text" class="form-control p-1" style="width: 80px;" name="amount" placeholder="Not Passed" value="{{ $allWithdrawData->CHECKING_COMMENT }}">
                                       <a class="badge badge-danger tooltips"><i class="mdi mdi-check text-white"></i> <span class="tooltiptext">Not Passed</span></a>
                                       @else
                                       --
                                       @endif
                                    </div>
                                 </td>
                                 <td>{{ ($allWithdrawData->REWARD_STATUS == 1) ? "LRP" : "CB" }}</td>
                                 <td>{{$allWithdrawData->PAYMENT_TRANSACTION_CREATED_ON}}</td>
                                 <td>
                                    @if($allWithdrawData->AADHAAR_STATUS ==2)
                                    <a href="#" data-toggle="modal" data-target="#aadharcardpopup" data-aadhaarno="{{ $allWithdrawData->AADHAAR_NUMBER}}" data-addhaarfront="{{ $allWithdrawData->AADHAAR_FRONT_URL}}" data-aadhaarback="{{ $allWithdrawData->AADHAAR_BACK_URL}}" title="Adhaar Status" class="badge badge-success tooltips aadhaarInfo"><i class="mdi mdi-check text-white"></i> <span class="tooltiptext">Aadhar Card</span></a>
                                    @elseif($allWithdrawData->AADHAAR_STATUS==1)
                                    <a href="#" data-toggle="modal" data-target="#aadharcardpopup" data-aadhaarno="{{ $allWithdrawData->AADHAAR_NUMBER}}" data-addhaarfront="{{ $allWithdrawData->AADHAAR_FRONT_URL}}" data-aadhaarback="{{ $allWithdrawData->AADHAAR_BACK_URL}}" title="Adhaar Status" class="badge badge-warning tooltips aadhaarInfo"><i class="mdi mdi-check text-white"></i> <span class="tooltiptext">Aadhar Card</span></a>
                                    @elseif($allWithdrawData->AADHAAR_STATUS==3)
                                    <a href="#" data-toggle="modal" data-target="#aadharcardpopup" data-aadhaarno="{{ $allWithdrawData->AADHAAR_NUMBER}}" data-addhaarfront="{{ $allWithdrawData->AADHAAR_FRONT_URL}}" data-aadhaarback="{{ $allWithdrawData->AADHAAR_BACK_URL}}" title="Adhaar Status" class="badge badge-danger tooltips aadhaarInfo"><i class="mdi mdi-check text-white"></i> <span class="tooltiptext">Aadhar Card</span></a>
                                    @else
                                    --
                                    @endif
                                 </td>
                                 <td>
                                    @if($allWithdrawData->PAN_STATUS ==2)
                                    <a href="#" data-toggle="modal" data-target="#pancardpopup" data-panno="{{ $allWithdrawData->PAN_NUMBER}}" title="Pan Status" class="badge badge-success tooltips "><i class="mdi mdi-check text-white"></i> <span class="tooltiptext">Pan Card</span></a>
                                    @elseif($allWithdrawData->PAN_STATUS==1)
                                    <a href="#" data-toggle="modal" data-target="#pancardpopup" data-panno="{{ $allWithdrawData->PAN_NUMBER}}" title="Pan Status" class="badge badge-warning tooltips "><i class="mdi mdi-check text-white"></i> <span class="tooltiptext">Pan Card</span></a>
                                    @elseif($allWithdrawData->PAN_STATUS==3)
                                    <a href="#" data-toggle="modal" data-target="#pancardpopup" data-panno="{{ $allWithdrawData->PAN_NUMBER}}" title="Pan Status" class="badge badge-danger tooltips "><i class="mdi mdi-check text-white"></i> <span class="tooltiptext">Pan Card</span></a>
                                    @else
                                    --
                                    @endif
                                 </td>
                                 <td>
                                    @if($allWithdrawData->OTHER_DOC_STATUS ==2)
                                    <a href="#" data-toggle="modal" data-target="#othercardpopup" data-otherno="{{ $allWithdrawData->OTHER_DOC_NUMBER}}" data-otherfront="{{ $allWithdrawData->OTHER_DOC_URL}}" data-otherback="{{ $allWithdrawData->OTHER_DOC_URL_BACK}}" title="Other Status" class="badge badge-success tooltips otherInfo"><i class="mdi mdi-check text-white"></i> <span class="tooltiptext">Other Doc</span></a>
                                    @elseif($allWithdrawData->OTHER_DOC_STATUS==1)
                                    <a href="#" data-toggle="modal" data-target="#othercardpopup" data-otherno="{{ $allWithdrawData->OTHER_DOC_NUMBER}}" data-otherfront="{{ $allWithdrawData->OTHER_DOC_URL}}" data-otherback="{{ $allWithdrawData->OTHER_DOC_URL_BACK}}" title="Other Status" class="badge badge-warning tooltips otherInfo"><i class="mdi mdi-check text-white"></i> <span class="tooltiptext">Other Doc</span></a>
                                    @elseif($allWithdrawData->OTHER_DOC_STATUS==3)
                                    <a href="#" data-toggle="modal" data-target="#othercardpopup" data-otherno="{{ $allWithdrawData->OTHER_DOC_NUMBER}}" data-otherfront="{{ $allWithdrawData->OTHER_DOC_URL}}" data-otherback="{{ $allWithdrawData->OTHER_DOC_URL_BACK}}" title="Other Status" class="badge badge-danger tooltips otherInfo"><i class="mdi mdi-check text-white"></i> <span class="tooltiptext">Other Doc</span></a>
                                    @else
                                    --
                                    @endif
                                 </td>
                                 <td>{{$allWithdrawData->INTERNAL_REFERENCE_NO}}</td>

                                 <td>
                                    <div class="statusBadge" data-ref-no="{{ $allWithdrawData->INTERNAL_REFERENCE_NO }}">
                                       @if($allWithdrawData->TRANSACTION_STATUS_DESCRIPTION=="Pending")
                                       <span class="badge bg-warning text-white shadow-none">{{$allWithdrawData->TRANSACTION_STATUS_DESCRIPTION}}</span>
                                       @elseif($allWithdrawData->TRANSACTION_STATUS_DESCRIPTION=="Approved"
                                       )
                                       @if($allWithdrawData->APPROVE_TYPE =="cashfree" ||
                                       $allWithdrawData->APPROVE_TYPE =="payu" ||
                                       $allWithdrawData->APPROVE_TYPE =="normal" ||
                                       $allWithdrawData->APPROVE_TYPE =="")
                                       <span class="badge bg-success  text-white shadow-none">{{$allWithdrawData->TRANSACTION_STATUS_DESCRIPTION}}</span>
                                       @endif
                                       @if( $allWithdrawData->APPROVE_TYPE =="cashfree_greenflag")
                                       <span class="badge bg-success  text-white shadow-none">{{$allWithdrawData->TRANSACTION_STATUS_DESCRIPTION}}(G)</span>
                                       @endif

                                       @elseif($allWithdrawData->TRANSACTION_STATUS_DESCRIPTION=="Rejected"
                                       || $allWithdrawData->TRANSACTION_STATUS_DESCRIPTION=="Failed" )
                                       <span class="badge bg-danger text-white shadow-none">{{$allWithdrawData->TRANSACTION_STATUS_DESCRIPTION}}</span>
                                       @else
                                       <span class="badge bg-warning text-white shadow-none">{{$allWithdrawData->TRANSACTION_STATUS_DESCRIPTION}}</span>
                                       @endif
                                    </div>
                                    @if(!empty($allWithdrawData->APPROVE_BY_PAYU_STATUS) && $allWithdrawData->APPROVE_BY_PAYU_STATUS == "1")
                                    <a href="#" id="payuStatus" title="Check Payu status" data-target="#payu-GetwayStatuspopup" data-ref-no="{{ $allWithdrawData->INTERNAL_REFERENCE_NO }}" data-user-id="{{ $allWithdrawData->USER_ID }}" data-with-type="payu" data-toggle="modal" data-plugin="tippy" data-tippy-interactive="true" class="font-19 mr-1"><b><i class="fab fa-paypal"></i></b></a>
                                    @elseif(!empty($allWithdrawData->APPROVE_BY_CASHFREE_STATUS) &&  ($allWithdrawData->APPROVE_BY_CASHFREE_STATUS == "1"))
                                    <a href="#" id="cashfreeStatus" title="Check Cashfree status" data-target="#cashfree-GetwayStatuspopup" data-ref-no="{{ $allWithdrawData->INTERNAL_REFERENCE_NO }}" data-user-id="{{ $allWithdrawData->USER_ID }}" data-with-type="cashfree" data-toggle="modal" data-plugin="tippy" data-tippy-interactive="true" class="font-19 mr-1"><i class="fas fa-money-bill-alt"></i></a>
                                    @endif
                                    <span class="inatStatus" data-status-ref-no="{{ $allWithdrawData->INTERNAL_REFERENCE_NO }}"></span>
                                 </td>
                              </tr>
                              @endforeach
                              @endif

                           </tbody>
                        </table>
                        <div class="customPaginationRender mt-2" data-form-id="#withdrawSearchForm" style="display:flex; justify-content: space-between;">
                           @if(!empty($withdrawData))
                           <div>More Records</div>
                           <div>
                              {{ $withdrawData->render("pagination::customPaginationView") }}
                           </div>
                           @endif
                        </div>
                        @if(!empty($withdrawData))
                        @if(!empty($params['withdraw_status'] ) && (in_array('109',$params['withdraw_status'])
                        || in_array('216',$params['withdraw_status'])))
                        <div class="clearfix mt-2" id="btnGroup" style="display:none">
                           <!-- check/uncheck all button -->
                           <button type="button" class="btn btn-primary waves-effect waves-light mb-1 mr-1" onclick="chkAllCheckboxes();"><i class="fe-check-square mr-1"></i>Check /
                              Uncheck ALL</button>
                           <!-- normal approve button will show if status is pending or failed and not approved by cashfree or payu -->
                           @if(\PokerBaazi::hasPermission('view', 'withdrawal_normal_approve'))
                           @if(!empty($params['withdraw_status']) &&
                           (in_array('109',$params['withdraw_status']) ||
                           in_array('216',$params['withdraw_status'])) &&
                           $params['approve_type'] !="cashfree" && $params['approve_type'] !="payu")
                           <button id="normalApprove" type="button" class="btn btn-success waves-effect waves-light mb-1 mr-1"><i class="fas fa-thumbs-up  mr-1"></i>Approve</button>
                           @endif
                           @endif
                           <!-- normal reject button will show if status is pending or failed and not approved by cashfree or payu -->
                           @if(\PokerBaazi::hasPermission('view', 'withdrawal_normal_reject'))
                           @if(!empty($params['withdraw_status']) &&
                           (in_array('109',$params['withdraw_status']) ||
                           in_array('216',$params['withdraw_status'])) &&
                           $params['approve_type'] !="cashfree" && $params['approve_type'] !="payu")
                           <button id="normalReject" type="button" class="btn btn-danger waves-effect waves-light mb-1 mr-1"><i class="fa fa-ban mr-1"></i>Reject</button>
                           @endif
                           @endif
                           <!-- cashfree approve button will show if status is pending or failed and not approved by  payu -->
                           @if(\PokerBaazi::hasPermission('view', 'withdrawal_cashfree_approve'))
                           @if(!empty($params['withdraw_status']) &&
                           (in_array('109',$params['withdraw_status']) ||
                           in_array('216',$params['withdraw_status'])) && $params['approve_type'] !="payu")
                           <button id="cashfreeApprove" type="button" class="btn btn-success waves-effect waves-light mb-1 mr-1"><i class="fas fa-thumbs-up  mr-1"></i>Cashfree Approve </button>
                           @endif
                           @endif
                           <!-- cashfree reject button will show if status  approved by cashfree -->
                           @if(\PokerBaazi::hasPermission('view', 'withdrawal_cashfree_reject'))
                           @if(!empty($params['withdraw_status'] ) &&
                           in_array('216',$params['withdraw_status']) && $params['approve_type'] =="cashfree")
                           <button id="cashfreeReject" type="button" class="btn btn-danger waves-effect waves-light mb-1 mr-1"><i class="fa fa-ban mr-1"></i>Cashfree Reject</button>
                           @endif
                           @endif
                           <!-- payu approve button will show if status is pending or failed and not approved by cashfree or payu -->
                           @if(\PokerBaazi::hasPermission('view', 'withdrawal_payu_approve'))
                           @if(!empty($params['withdraw_status']) &&
                           (in_array('109',$params['withdraw_status']) ||
                           in_array('216',$params['withdraw_status'])) &&
                           $params['approve_type'] !="cashfree")
                           <button id="payuApprove" type="button" class="btn btn-success waves-effect waves-light mb-1 mr-1"><b><i class="fab fa-paypal"></i></b>Payu Approve </button>
                           @endif
                           @endif
                           @if(\PokerBaazi::hasPermission('view', 'withdrawal_payu_reject'))
                           @if(!empty($params['withdraw_status'] ) &&
                           in_array('216',$params['withdraw_status']) && $params['approve_type'] =="payu")
                           <button id="payuReject" type="button" class="btn btn-danger waves-effect waves-light mb-1 mr-1"><i class="fa fa-ban mr-1"></i> Payu Reject</button>
                           @endif
                           @endif
                           @if(!empty($params['withdraw_status'] ) &&
                           (in_array('109',$params['withdraw_status'])))
                           <button id="passed" type="button" class="btn btn-success waves-effect waves-light mb-1 mr-1"><i class="fe-check-square mr-1"></i>Passed</button>
                           <button id="notPassed" type="button" class="btn btn-secondary waves-effect waves-light mb-1 mr-1"><i class="fa-lock fa mr-1"></i>Not Passed</button>
                           <button id="unchecked" type="button" class="btn btn-warning waves-effect waves-light mb-1 mr-1"><i class="fe-square mr-1"></i>Unchecked</button>
                           @endif
                        </div>
                        @endif
                        @endif
                        <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" class="mt-2">
                           <!-- <div>More Records</div> -->
                           <div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- end card body-->
               </div>
               <!-- end card -->
            </div>
            <!-- end col-->
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="setuserflagpopup">
   <div class="modal-dialog">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                     <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Set User Flag</h5>
            </div>
            <div class="card-body">
               <div class="form-group col-md-12">
                  <div id="statusUpdateLoader" style="display:none">
                     <div class="d-flex justify-content-center hv_center">
                        <div class="spinner-border" role="status"></div>
                     </div>
                  </div>
                  <div class="form-row align-items-center" style="min-height:70px">
                     <form id="flagForm">
                        <input type="hidden" class="userIdStatusUpdate">
                        <!-- <input type="hidden" class="userCurrentFlag"> -->
                        <div class="radio radio-info form-check-inline">
                           <input class="flag_status" type="radio" id="set_Pending" value="1" name="flag_status" checked="">
                           <label for="set_Pending"> Yellow </label>
                        </div>
                        <div class="radio radio-info form-check-inline">
                           <input class="flag_status" type="radio" id="set_Approved" value="2" name="flag_status">
                           <label for="set_Approved"> Green </label>
                        </div>
                        <div class="radio radio-info form-check-inline">
                           <input class="flag_status" type="radio" id="set_Rejected" value="3" name="flag_status">
                           <label for="set_Rejected"> Red </label>
                        </div>
                     </form>
                  </div>
               </div>
               <button type="button" id="flagUpdate" class="btn btn-success waves-effect waves-light"><i class="fa fa-save mr-1"></i> Submit</button>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- payu Getway status poup -->
<div class="modal fade" id="payu-GetwayStatuspopup">
   <div class="modal-dialog">
      <div class="modal-content  shadow-none position-relative">
         <div id="payuloader" style="display:none">
            <div class="d-flex justify-content-center hv_center">
               <div class="spinner-border" role="status"></div>
            </div>
         </div>
         <div class="card  mb-0">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                     <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18"><span class="text-success" id="getWayPoupType"></span> Payu Latest Status</h5>
            </div>
            <div class="card-body " style="position:relative">
               <div id="payu-blurr-effect" class="blureffect" style="display:none;z-index:9; position:absolute; left:0; top:0; bottom:0; right:0;"></div>
               <input type="hidden" class="refNoGetway">
               <input type="hidden" class="getwayType">
               <div id="payu-status-div">
                  <div class="form-group ">
                     <label>Status: <span id="txnStatus"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Ref No: <span id="merchantRefId"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Amount: <span id="amount"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Trans Time: <span id="txnDate"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Payu RefNo: <span id="payuTransactionRefNo"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Beneficiary Name: <span id="beneficiaryName"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Msg: <span id="msg"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Transfer Type: <span id="transferType"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Name With Bank: <span id="nameWithBank"></span></label>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- cashfree Getway status poup -->
<div class="modal fade" id="cashfree-GetwayStatuspopup">
   <div class="modal-dialog">
      <div class="modal-content  shadow-none position-relative">
         <div id="cashfreeloader" style="display:none">
            <div class="d-flex justify-content-center hv_center">
               <div class="spinner-border" role="status"></div>
            </div>
         </div>
         <div class="card  mb-0">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                     <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18"><span class="text-success" id="getWayPoupType"></span> Cashfree Latest Status</h5>
            </div>
            <div class="card-body " style="position:relative">
               <div id="cashfree-blurr-effect" class="blureffect" style="display:none;z-index:9; position:absolute; left:0; top:0; bottom:0; right:0;"></div>
               <input type="hidden" class="refNoCashfree">
               <input type="hidden" class="getwayType">
               <div id="cashfree-status-div">
                  <div class="form-group ">
                     <label>Status: <span id="cashfreestatus"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Sub Code: <span id="subCode"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Message: <span id="message"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>BankAccount: <span id="bankAccount"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Ifsc: <span id="ifsc"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Transfer Id: <span id="transferId"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Amount: <span id="pay_amount"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Date: <span id="processedOn"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>Acknowledged: <span id="acknowledged"></span></label>
                  </div>
                  <div class="form-group ">
                     <label>TransferMode: <span id="transferMode"></span></label>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade show" style="display:none" id="warningPop">
   <div class="modal-dialog">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                     <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Warning</h5>
            </div>
            <div class="card-body">
               <div class="form-group col-md-12">
                  <div id="statusUpdateLoader" style="display:none">
                     <div class="d-flex justify-content-center hv_center">
                        <div class="spinner-border" role="status"></div>
                     </div>
                  </div>
                  <div class="form-row align-items-center" style="min-height:70px">
                     <p id="warmingMsg"></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="aadharcardpopup">
   <div class="modal-dialog">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                     <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Aadhaar Details</h5>
            </div>
            <div class="card-body text-center">
               <h4 class="mt-0 mb-0">Aadhaar No. : <span class="aadhaarno"><span></h4>
               <div class="row p-2">
                  <div class="col-md-6">
                     <h4 class="mt-0 mb-2  text-left">Front</h4>
                     <img class="w-100 frontAaddhaar" />
                  </div>
                  <div class="col-md-6">
                     <h4 class="mt-0 mb-2 text-left">Back</h4>
                     <img class="w-100 backAadhaar" />
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="othercardpopup">
   <div class="modal-dialog">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                     <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Other Details</h5>
            </div>
            <div class="card-body text-center">
               <h4 class="mt-0 mb-0">Other No. : <span class="otherno"><span></h4>
               <div class="row p-2">
                  <div class="col-md-6">
                     <h4 class="mt-0 mb-2  text-left">Front</h4>
                     <img class="w-100 frontOther" />
                  </div>
                  <div class="col-md-6">
                     <h4 class="mt-0 mb-2 text-left">Back</h4>
                     <img class="w-100 backOther" />
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="pancardpopup">
   <div class="modal-dialog">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                     <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">User Pan No</h5>
            </div>
            <div class="card-body text-center">
               <h4 class="mt-0 mb-2"><span class="panno"><span></h4>
               <div class="w-50 m-auto">
                  <img class="w-100 panfront" />
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="pancardpopup">
   <div class="modal-dialog" style="transform: translate(0)">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a data-dismiss="modal">
                     <i class="mdi mdi-close closePan"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">User Pan No</h5>
            </div>
            <div class="card-body text-center">
               <h4 class="mt-0 mb-2"><span class="panno"><span></h4>
               <div class="w-50 m-auto">
                  <img class="w-100 panfront" />
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


@endsection

@section('js')
<!-- Datatables init -->


<!-- switchery toogle button -->
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.js') }}"></script>
<script src="{{ asset('assets/libs/switchery/switchery.min.js') }}"></script>
<script src="{{ asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
<!-- Init js-->
<script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>
@endsection

@section('post_js')
<script>
   //if selected any checkbox then action button will show     
   $(document).on('change', '.checkbox', function(e) {
      var len = $(document).find(".td-refNoCheckBoxSelect input:checked").length;
      if (len > 0) {
         $('#btnGroup').show();
      }
      if (len <= 0) {
         $('#btnGroup').hide();
      }
   });
   //});
   //for sum of selected amount
   $(document).ready(function() {
      $('.checkbox').change(function(e) {
         var sum = 0;
         $('#sumSelAmnt').text('');
         $("input[name='refNoCheckBoxSelect']:checked").each(function() {
            var tr_el = $(this).closest('tr');
            var combat = tr_el.find('.toBePaid').text();

            // if (!isNaN(combat) && combat.length !== 0) {
            sum = (sum * 1) + (combat * 1);
            // sum += parseFloat(combat);
            //}
         });
         $('#sumSelAmnt').text(sum.toFixed(2)); //for sum of selected amount
      });
   });
   //pass data attribute to bootstrap modal

   // Aadhaar click
   //open user flaf modal and pass value to modal
   $('#aadharcardpopup').on('show.bs.modal', function(e) {
      $relatedButton = $(e.relatedTarget);
      $('.aadhaarno').html($relatedButton.data('aadhaarno'));
      $('.frontAaddhaar').attr('src', $relatedButton.data('addhaarfront'));
      $('.backAadhaar').attr('src', $relatedButton.data('aadhaarback'));

   });

   // other click
   //open user flaf modal and pass value to modal
   $('#othercardpopup').on('show.bs.modal', function(e) {
      $relatedButton = $(e.relatedTarget);
      $('.otherno').html($relatedButton.data('otherno'));
      $('.frontOther').attr('src', $relatedButton.data('otherfront'));
      $('.backOther').attr('src', $relatedButton.data('otherback'));

   });



   //pan popup
   $('#pancardpopup').on('show.bs.modal', function(e) {
      $relatedButton = $(e.relatedTarget);
      $('.panno').html($relatedButton.data('panno'));


   });
   //check all check boxes
   var isAllCheck = false;

   function chkAllCheckboxes() {
      var chkWithdrawals = document.getElementsByClassName('checkbox');
      for (var i = 0; i < chkWithdrawals.length; i++) {
         if (chkWithdrawals[i].disabled) {
            continue;
         }
         chkWithdrawals[i].checked = !isAllCheck
      }
      isAllCheck = !isAllCheck;
      if (isAllCheck == false) {
         $("#btnGroup").hide();
      }
      var sum = 0;
      $(".checkbox").each(function() {
         var _el = $(this);
         if (_el.prop('checked') == true && _el.prop('disabled') == false) {
            var tr_el = _el.closest('tr');
            var combat = tr_el.find('.toBePaid').text();
            sum = (sum * 1) + (combat * 1);

         }
      });
      $('#sumSelAmnt').text(sum.toFixed(2)); //for sum of selected amount
   }

   $(".td-woff").find("input").change(function() {
      if ($(this).prop('checked')) {
         $(this).closest('tr').find('.td-refNoCheckBoxSelect').find("input").prop('checked', true);
      } else {
         $(this).closest('tr').find('.td-refNoCheckBoxSelect').find("input").prop('checked', false);

      }
   })



   //open user flaf modal and pass value to modal
   $('#setuserflagpopup').on('show.bs.modal', function(e) {
      $relatedButton = $(e.relatedTarget);
      $model = $(this);
      $model.find('.userIdStatusUpdate').val($relatedButton.data('userId'));
      // $model.find('.userCurrentFlag').val($relatedButton.data('curFlag'));
      var curStatus = $relatedButton.data('curFlag');
      if (curStatus == 1) {
         $('#set_Pending').prop('checked', true);
      } else if (curStatus == 2) {
         $('#set_Approved').prop('checked', true);
      } else if (curStatus == 3) {
         $('#set_Rejected').prop('checked', true);
      }

   });
   // user flag update 
   // change user flag status  
   $('#flagUpdate').click(function(e) {
      e.preventDefault();
      $("#flagUpdate").hide();
      $("#statusUpdateLoader").show();
      //   $("#blurr-effect").show();
      // var flag_status = $('.flag_status').val();
      var flag_status = $('input[name=flag_status]:checked', '#flagForm').val();
      var user_id = $('.userIdStatusUpdate').val();
      $.ajax({
         url: `${window.pageData.baseUrl}/reports/withdraw/userFlagUpdate`,
         method: "POST",
         data: {
            user_id: user_id,
            flag_status: flag_status,
         },
         success: function(response) {
            $('#app').removeClass('modal-open');
            if (response == 1) {

               $('#setuserflagpopup').hide();
               $('.modal-backdrop').hide();
               $("#statusUpdateLoader").hide();

               $('table').find(`.badge[data-user-id="${user_id}"]`).addClass('badge-warning')
                  .removeClass('badge-success').removeClass('badge-danger');
            } else if (response == 2) {

               $('#setuserflagpopup').hide();
               $('.modal-backdrop').hide();
               $('table').find(`.badge[data-user-id="${user_id}"]`).addClass('badge-success')
                  .removeClass('badge-warning').removeClass('badge-danger');

            } else if (response == 3) {

               $('#setuserflagpopup').hide();
               $('.modal-backdrop').hide();
               $('table').find(`.badge[data-user-id="${user_id}"]`).addClass('badge-danger')
                  .removeClass('badge-success').removeClass('badge-warning');

            }
         },
         error: function(error) {
            $("#statusUpdateLoader").hide();
            $("#flagUpdate").show();


         }
      });
   });



   // withdraw normal approve button action 
   $('#normalApprove').click(function(e) {
      e.preventDefault();
      var tableData = [];
      $('#completeblur').addClass('completeblur');
      var tableData = getRefNoAndWoffValueFromTable();
      $.ajax({
         url: `${window.pageData.baseUrl}/reports/commission_withdraw/action/comm_normal_approve`,
         method: "POST",
         data: {
            tableData
         },
         dataType: 'json',
         success: function(response) {
            if (response.status == "success" && response.code == 1000) {
               $('#btnGroup').hide();
               for (refValue of response.refNoArray) {
                  $('table').find(`.statusBadge[data-ref-no="${refValue}"]`).html(
                     '<span class="badge bg-success  text-white shadow-none">Approved</span>'
                  );
                  $('#completeblur').removeClass('completeblur');
                  $(`table input[value="${refValue}"]`).prop('checked', false).prop('disabled',
                     true);
                  $(`table input[value="${refValue}"]`).closest('tr').find('.td-woff input').prop(
                     'checked', false).prop('disabled', true);
               }
            } else {
               $('#completeblur').removeClass('completeblur');
               $("#warningPop").show();
               $('#warmingMsg').html(response.msg);
            }
         },
         error: function(error) {
            $('#completeblur').removeClass('completeblur');
            $("#warningPop").show();
            $('#warmingMsg').html(response.msg);
         }
      });
   });

   // withdraw normal reject button action 
   $('#normalReject').click(function(e) {
      e.preventDefault();
      var tableData = [];
      $('#completeblur').addClass('completeblur');
      var tableData = getRefNoAndWoffValueFromTable();
      $.ajax({
         url: `${window.pageData.baseUrl}/reports/commission_withdraw/action/comm_normal_reject`,
         method: "POST",
         data: {
            tableData
         },
         dataType: 'json',
         success: function(response) {
            if (response.status == "success" && response.code == 2000) {
               $('#btnGroup').hide();
               for (refValue of response.refNoArray) {
                  $('table').find(`.statusBadge[data-ref-no="${refValue}"]`).html(
                     '<span class="badge bg-danger text-white shadow-none">Rejected</span>');
                  $('#completeblur').removeClass('completeblur');
                  $(`table input[value="${refValue}"]`).prop('checked', false).prop('disabled',
                     true);
                  $(`table input[value="${refValue}"]`).closest('tr').find('.td-woff input').prop(
                     'checked', false).prop('disabled', true);
               }
            } else {
               $('#completeblur').removeClass('completeblur');
               $("#warningPop").show();
               $('#warmingMsg').html(response.msg);
            }
         },
         error: function(error) {
            $('#completeblur').removeClass('completeblur');
            $("#warningPop").show();
            $('#warmingMsg').html(response.msg);
         }
      });
   });

   // withdraw cashfree approve button action 
   $('#cashfreeApprove').click(function(e) {
      e.preventDefault();
      var tableData = [];
      $('#completeblur').addClass('completeblur');
      var tableData = getRefNoAndWoffValueFromTable();
      $.ajax({
         url: `${window.pageData.baseUrl}/reports/commission_withdraw/action/comm_withdrawal_cashfree_approve`,
         method: "POST",
         data: {
            tableData
         },
         dataType: 'json',
         success: function(response) {
            if (response.status == "failed") {
               $('#completeblur').removeClass('completeblur');
               $("#warningPop").show();
               $('#warmingMsg').html(response.msg);
               return false;

            }
            for (refValue of response) {
               if (refValue.status == "success") {
                  $('table').find(`.statusBadge[data-ref-no="${refValue.refNo}"]`).html(
                     '<span class="badge bg-success  text-white shadow-none">Cashfree Processing</span>'
                  );
                  $('table').find(`.inatStatus[data-status-ref-no="${refValue.refNo}"]`).html(
                     '<a href="#" id="cashfreeStatus" title="Check Cashfree status" data-target="#cashfree-GetwayStatuspopup" data-ref-no=' +
                     refValue.refNo +
                     '  data-with-type="cashfree" data-toggle="modal" data-plugin="tippy" data-tippy-interactive="true" class="font-19 mr-1"><i class="fas fa-money-bill-alt"></i></a>'
                  );
                  $('#completeblur').removeClass('completeblur');
                  $(`table input[value="${refValue.refNo}"]`).prop('checked', false).prop(
                     'disabled', true);
                  $(`table input[value="${refValue.refNo}"]`).closest('tr').find('.td-woff input')
                     .prop('checked', false).prop('disabled', true);
               } else {
                  $('table').find(`.statusBadge[data-ref-no="${refValue.refNo}"]`).html(
                     '<span class="badge bg-danger  text-white shadow-none">' + refValue
                     .msg + '</span>');
                  $('#completeblur').removeClass('completeblur');
                  $(`table input[value="${refValue.refNo}"]`).prop('checked', false).prop(
                     'disabled', true);
                  $(`table input[value="${refValue.refNo}"]`).closest('tr').find('.td-woff input')
                     .prop('checked', false).prop('disabled', true);
               }
            }

         },
         error: function(error) {
            $('#completeblur').removeClass('completeblur');
            $("#warningPop").show();
            $('#warmingMsg').html(response.msg);
         }
      });
   });


   // withdraw cashfreeReject button action 
   $('#cashfreeReject').click(function(e) {
      e.preventDefault();
      var tableData = [];
      $('#completeblur').addClass('completeblur');
      var tableData = getRefNoAndWoffValueFromTable();
      $.ajax({
         url: `${window.pageData.baseUrl}/reports/commission_withdraw/action/comm_withdrawal_cashfree_reject`,
         method: "POST",
         data: {
            tableData
         },
         dataType: 'json',
         success: function(response) {
            if (response.status == "success" && response.code == 2040) {
               $('#btnGroup').hide();
               for (refValue of response.refNoArray) {
                  $('table').find(`.statusBadge[data-ref-no="${refValue}"]`).html(
                     '<span class="badge bg-danger text-white shadow-none">Rejected</span>');
                  $('#completeblur').removeClass('completeblur');
                  $(`table input[value="${refValue}"]`).prop('checked', false).prop('disabled',
                     true);
                  $(`table input[value="${refValue}"]`).closest('tr').find('.td-woff input').prop(
                     'checked', false).prop('disabled', true);
               }
            } else {
               $('#completeblur').removeClass('completeblur');
               $("#warningPop").show();
               $('#warmingMsg').html(response.msg);
            }
         },
         error: function(error) {
            $('#completeblur').removeClass('completeblur');
            $("#warningPop").show();
            $('#warmingMsg').html(response.msg);
         }
      });
   });

   // withdraw payu approve button action 
   $('#payuApprove').click(function(e) {
      e.preventDefault();
      var tableData = [];
      $('#completeblur').addClass('completeblur');
      var tableData = getRefNoAndWoffValueFromTable();
      $.ajax({
         url: `${window.pageData.baseUrl}/reports/commission_withdraw/action/comm_withdrawal_payu_approve`,
         method: "POST",
         data: {
            tableData
         },
         dataType: 'json',
         success: function(response) {
            if (response.status == "success" && response.code == 3000) {
               $('#btnGroup').hide();
               for (refValue of response.refNoArray) {
                  $('table').find(`.statusBadge[data-ref-no="${refValue}"]`).html(
                     '<span class="badge bg-success  text-white shadow-none">Payu Processing</span>'
                  );
                  $('table').find(`.inatStatus[data-status-ref-no="${refValue}"]`).html(
                     '<a href="#" id="payuStatus" title="Check Payu status" data-target="#payu-GetwayStatuspopup" data-ref-no=' +
                     refValue +
                     ' data-with-type="payu" data-toggle="modal" data-plugin="tippy" data-tippy-interactive="true" class="font-19 mr-1"><b><i class="fab fa-paypal"></i></b></a>'
                  );

                  $('#completeblur').removeClass('completeblur');
                  $(`table input[value="${refValue}"]`).prop('checked', false).prop('disabled',
                     true);
                  $(`table input[value="${refValue}"]`).closest('tr').find('.td-woff input').prop(
                     'checked', false).prop('disabled', true);
               }
            } else {
               $('#completeblur').removeClass('completeblur');
               // alert(response);
               // console.log(response);
               $("#warningPop").show();
               $('#warmingMsg').html(response.msg);
            }
         },
         error: function(error) {
            $('#completeblur').removeClass('completeblur');
            $("#warningPop").show();
            $('#warmingMsg').html(response.msg);
         }
      });
   });


   // withdraw payu reject button action 
   $('#payuReject').click(function(e) {
      e.preventDefault();
      var tableData = [];
      $('#completeblur').addClass('completeblur');
      var tableData = getRefNoAndWoffValueFromTable();
      $.ajax({
         url: `${window.pageData.baseUrl}/reports/commission_withdraw/action/comm_withdrawal_payu_reject`,
         method: "POST",
         data: {
            tableData
         },
         dataType: 'json',
         success: function(response) {
            if (response.status == "success" && response.code == 2020) {
               $('#btnGroup').hide();
               for (refValue of response.refNoArray) {
                  $('table').find(`.statusBadge[data-ref-no="${refValue}"]`).html(
                     '<span class="badge bg-danger text-white shadow-none">Rejected</span>');
                  $('#completeblur').removeClass('completeblur');
                  $(`table input[value="${refValue}"]`).prop('checked', false).prop('disabled',
                     true);
                  $(`table input[value="${refValue}"]`).closest('tr').find('.td-woff input').prop(
                     'checked', false).prop('disabled', true);
               }
            } else {
               $('#completeblur').removeClass('completeblur');
               $("#warningPop").show();
               $('#warmingMsg').html(response.msg);
            }
         },
         error: function(error) {
            $('#completeblur').removeClass('completeblur');
            $("#warningPop").show();
            $('#warmingMsg').html(response.msg);
         }
      });
   });

   // withdraw passed button action 
   $('#passed').click(function(e) {
      e.preventDefault();
      var tableData = [];
      $('#completeblur').addClass('completeblur');
      var tableData = getRefNoAndWoffValueFromTable();
      $.ajax({
         url: `${window.pageData.baseUrl}/reports/commission_withdraw/action/comm_withdrawal_passed`,
         method: "POST",
         data: {
            tableData
         },
         dataType: 'json',
         success: function(response) {
            if (response.status == "success" && response.code == "7000") {
               for (refValue of response.refNoArray) {
                  $('#withdrawSearchForm').submit();
                  //$('table').find(`[data-ref-number="${refValue}"] a`).addClass('badge-success').removeClass('badge-warning').removeClass('badge-danger');
                  //$('#completeblur').removeClass('completeblur');
               }
            } else {
               $('#completeblur').removeClass('completeblur');
               // alert(response);
            }
         },
         error: function(error) {
            $('#completeblur').removeClass('completeblur');
            // alert(response);
         }
      });
   });
   // withdraw not passed button action 
   $('#notPassed').click(function(e) {
      e.preventDefault();
      var tableData = [];
      $('#completeblur').addClass('completeblur');
      var tableData = getRefNoAndWoffValueFromTable();
      $.ajax({
         url: `${window.pageData.baseUrl}/reports/commission_withdraw/action/comm_withdrawal_not_passed`,
         method: "POST",
         data: {
            tableData
         },
         dataType: 'json',
         success: function(response) {
            if (response.status == "success" && response.code == "8000") {
               for (refValue of response.refNoArray) {
                  $('#withdrawSearchForm').submit();
                  $('table').find(`.statusBadge[data-ref-no="${refValue}"]`).html(
                     '<span class="badge bg-success  text-white shadow-none">Payu Processing</span>'
                  );
                  $('#completeblur').removeClass('completeblur');
               }
            } else {
               $('#completeblur').removeClass('completeblur');
               // alert(response);
            }
         },
         error: function(error) {
            $('#completeblur').removeClass('completeblur');
            // alert(response);
         }
      });
   });

   // withdraw unchecked button action 
   $('#unchecked').click(function(e) {
      e.preventDefault();
      var tableData = [];
      $('#completeblur').addClass('completeblur');
      var tableData = getRefNoAndWoffValueFromTable();
      $.ajax({
         url: `${window.pageData.baseUrl}/reports/commission_withdraw/action/comm_withdrawal_unchecked`,
         method: "POST",
         data: {
            tableData
         },
         dataType: 'json',
         success: function(response) {
            $('#withdrawSearchForm').submit();
            if (response.status == "success" && response.code == 3000) {
               for (refValue of response.refNoArray) {
                  $('#withdrawSearchForm').submit();
                  $('table').find(`.statusBadge[data-ref-no="${refValue}"]`).html(
                     '<span class="badge bg-success  text-white shadow-none">Payu Processing</span>'
                  );
                  $('#completeblur').removeClass('completeblur');
               }
            } else {
               $('#completeblur').removeClass('completeblur');
               // alert(response);
            }
         },
         error: function(error) {
            $('#completeblur').removeClass('completeblur');
            // alert(response);
         }
      });
   });

   // get payu latest status button action 
   $('#payu-GetwayStatuspopup').on('show.bs.modal', function(e) {
      $("#payuloader").show();

      $("#txnStatus").val('');
      $("#merchantRefId").val('');
      $("#amount").val('');
      $("#txnDate").val('');
      $("#payuTransactionRefNo").val('');
      $("#beneficiaryName").val('');
      $("#msg").val('');
      $("#transferType").val('');
      $("#nameWithBank").val('');
      $("#payu-blurr-effect").show();

      $("#payu-status-div").hide();
      $relatedButton = $(e.relatedTarget);
      $model = $(this);
      $model.find('.refNoGetway').val($relatedButton.data('refNo'));
      var ref_no = $('.refNoGetway').val();
      $model.find('.getwayType').val($relatedButton.data('withType'));
      var getway_type = $('.getwayType').val();
      $.ajax({
         url: `${window.pageData.baseUrl}/reports/commission_withdraw/action/comm_get_payu_latest_status`,
         method: "POST",
         data: {
            ref_no: ref_no,
            getway_type: getway_type,
         },
         success: function(response) {
            if (response.txnStatus == "SUCCESS") {
               $("#txnStatus").addClass("badge bg-success text-white shadow-none");
            } else {
               $("#txnStatus").addClass("badge bg-danger text-white shadow-none");
            }
            $("#txnStatus").html(response.txnStatus);
            $("#merchantRefId").html(response.merchantRefId);
            $("#amount").html(response.amount);
            $("#txnDate").html(response.txnDate);
            $("#payuTransactionRefNo").html(response.payuTransactionRefNo);
            $("#beneficiaryName").html(response.beneficiaryName);
            $("#msg").html(response.msg);
            $("#transferType").html(response.transferType);
            $("#nameWithBank").html(response.nameWithBank);
            $("#payuloader").hide();
            $("#payu-blurr-effect").hide();
            $("#payu-status-div").show();

         },
         error: function(error) {
            $("#txnStatus").html('Unable to get status.Please Try gain later');
            $("#payuloader").hide();
            $("#payu-blurr-effect").hide();
            $("#payu-status-div").show();
         }
      });
   });

   // get cashfree latest status button action 
   $('#cashfree-GetwayStatuspopup').on('show.bs.modal', function(e) {
      $("#cashfreeloader").show();
      $("#cashfreestatus").val('');
      $("#subCode").val('');
      $("#message").val('');
      $("#bankAccount").val('');
      $("#ifsc").val('');
      $("#beneId").val('');
      $("#transferId").val('');
      $("#amount").val('');
      $("#processedOn").val('');
      $("#acknowledged").val('');
      $("#transferMode").val('');
      $("#cashfree-blurr-effect").show();
      $("#cashfree-status-div").hide();

      $relatedButton = $(e.relatedTarget);
      $model = $(this);
      $model.find('.refNoCashfree').val($relatedButton.data('refNo'));
      var ref_no = $('.refNoCashfree').val();
      $model.find('.getwayType').val($relatedButton.data('withType'));
      var getway_type = $('.getwayType').val();

      $.ajax({
         url: `${window.pageData.baseUrl}/reports/commission_withdraw/action/comm_get_cashfree_latest_status`,
         method: "POST",
         data: {
            ref_no: ref_no,
            getway_type: getway_type,
         },
         success: function(response) {
            if (response.status == "SUCCESS") {
               $("#cashfreestatus").addClass("badge bg-success text-white shadow-none");
            } else {
               $("#cashfreestatus").addClass("badge bg-danger text-white shadow-none");
            }

            $("#cashfreestatus").html(response.status);
            $("#subCode").html(response.subCode);
            $("#message").html(response.message);
            $("#bankAccount").html(response.bankAccount);
            $("#ifsc").html(response.ifsc);
            $("#beneId").html(response.beneId);
            $("#transferId").html(response.transferId);
            $("#pay_amount").html(response.amount);
            $("#processedOn").html(response.processedOn);
            $("#acknowledged").html(response.acknowledged);
            $("#transferMode").html(response.transferMode);
            $("#cashfree-status-div").show();
            $("#cashfreeloader").hide();
            $("#cashfree-blurr-effect").hide();
         },
         error: function(error) {
            $("#cashfreestatus").html('Unable to get status.Please Try gain later');
            $("#cashfreeloader").hide();
            $("#cashfree-blurr-effect").hide();
            $("#cashfree-status-div").show();
         }
      });
   });

   function getRefNoAndWoffValueFromTable() {
      var arr = [];
      $("input[name='refNoCheckBoxSelect']:checked").each(function() {
         var tempObj = {};
         var _el_tr = $(this).closest('tr');
         tempObj.refNo = $(this).val();
         tempObj.wo = _el_tr.find(".td-woff").find("input").prop("checked");
         tempObj.cs = _el_tr.find(".comment-status").find("input").val();
         tempObj.checkedBox = $(this).prop("checked");
         // tempObj.srno =srNO_val;
         arr.push(tempObj);
      });
      return arr;
   }

   //error modal close
   $(".card-widgets").on("click", function() {
      $('#warningPop').css('display', 'none');
   });


   //get win amount after page load
   var myWindowIsLoaded = false
   $(window).on('load', function() {
      myWindowIsLoaded = true;
      getWin();
      getUserFirstWithdrawStatus();
      if (myWindowIsLoaded) {
         $('#cardCollpase7 table').on('draw.dt', function() {
            getWin();
            getUserFirstWithdrawStatus();
         })
      }
   });

   function getWin() {
      var user_id;

      var user_id_array = [];
      $(".td-winUserId .winRes").each(function() {
         user_id = $(this).data("userId");
         user_id_array.push(user_id);
      });
      console.log(user_id_array);
      if (user_id_array != "") {
         $.ajax({
            url: `${window.pageData.baseUrl}/reports/commission_withdraw/get_user_win`,
            method: "POST",
            data: {
               user_id: user_id_array,
            },
            dataType: 'json',
            success: function(response) {
               for (userIdVal of response) {
                  $('table').find(`.winRes[data-user-id="${userIdVal.user_id}"]`).html(userIdVal
                     .winAmount);
               }
            },
            error: function(error) {

            }
         });
      }
   }

   //get user first withdraw status
   function getUserFirstWithdrawStatus() {
      var user_id;
      var user_id_array = [];
      $(".td-withStatusUserId .withStatusRes").each(function() {
         user_id = $(this).data("withStatusUserId");
         user_id_array.push(user_id);
      });
      console.log(user_id_array);
      if (user_id_array != "") {
         $.ajax({
            url: `${window.pageData.baseUrl}/reports/commission_withdraw/getUserFirstWitStatus`,
            method: "POST",
            data: {
               user_id: user_id_array,
            },
            dataType: 'json',
            success: function(response) {
               for (userIdVal of response) {
                  $('table').find(`.withStatusRes[data-with-status-user-id="${userIdVal.user_id}"]`).html(
                     userIdVal.withStatus);
               }
            },
            error: function(error) {

            }
         });
      }
   }

   $("#payuStatusUpdate").on("click", function() {
      // $('#payuStatusUpdateLoader').show();
      $('#payuStatusUpdate').hide();
      $.ajax({
         url: `${window.pageData.baseUrl}/api/payuManuallyCommWebhookCall`,
         method: "GET",

         success: function(response) {
            // $('#withdrawSearchForm').submit();
            // $('#payuStatusUpdateLoader').hide();
            $('#payuStatusUpdate').show();

         },
         error: function(error) {
            // $('#payuStatusUpdateLoader').hide();
            $('#payuStatusUpdate').show();

         }
      });
   });

   $("#cashfreeStatusUpdate").on("click", function() {
      $('#cashfreeStatusUpdateLoader').show();
      $('#cashfreeStatusUpdate').hide();
      $.ajax({
         url: `${window.pageData.baseUrl}/api/cashfreeManuallyCommWebhookCall`,
         method: "GET",

         success: function(response) {
            // $('#withdrawSearchForm').submit();
            $('#cashfreeStatusUpdateLoader').hide();
            $('#cashfreeStatusUpdate').show();

         },
         error: function(error) {
            $('#cashfreeStatusUpdateLoader').hide();
            $('#cashfreeStatusUpdate').show();

         }
      });
   });
   $(".btn-hide").click(function() {
         $(".btn-hide").hide();
      });
</script>
@endsection