@extends('bo.layouts.master')

@section('title', "PB Daily Stats Report | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/nestable2/nestable2.min.css') }}" rel="stylesheet" />
 
<!-- Plugins css -->
<link href="{{ url('assets/libs/jquery-nice-select/jquery-nice-select.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<div class="col-md-12">
<div class="col-md-4"></div>
<div class="col-md-4">

</div>
<div class="col-md-4"></div>
</div>
<div class="content">
   <div class="container-fluid">
       <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a href="">Reports</a></li>
                     <li class="breadcrumb-item active">PB Daily Stats</li>
				</ol>
               </div>
               <h4 class="page-title">PB Daily Stats Report</h4>
            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-12">
           <div class="card">
               <div class="card-header bg-dark text-white">
                  <div class="card-widgets">
                     <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>                                   
                  </div>
                  <h5 class="card-title mb-0 text-white">Summary</h5>
               </div>
                  
			   <div id="cardCollpase7" class="collapse show">
                   <div class="row">
					 
						<div class="form-group customDatePickerWrapper col-lg-4 col-md-4 col-sm-4">
						
						</div>
                        <div class="col-sm-4 mb-1 text-center">
						<form id="reportdata" enctype='multipart/form-data' method="post" action="{{ url('reports/pbdailystats') }}">
						@csrf
						<input type="hidden" id="date" value="1" name="date" />
						
                           <button type="submit" style="margin-top: 10px;" id="get_yesterday_report" name="getdailystats" class="btn btn-primary waves-effect waves-light">Download Yesterday's Report</button>
						   </form> 
                        </div>
						
                        <div class="col-sm-3 mb-1 ">
						<label for="selectdate" class="white-text d-block">.</label>
						
                           <div class="form-group d-inline-block">
                           
                             
							  
                           </div>
                        </div>
						
                        <!-- end col-->
                     </div>
				
               </div>
               </div>
               <!-- end card body-->
           
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
   </div>
</div>
@endsection

@section('js')
<!---  Nice Select menu -->
    <script src="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.js') }}"></script>
    <script src="{{ asset('assets/libs/switchery/switchery.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
	
    <!-- Init js-->
    <script src="{{url('assets/js/pages/form-advanced.init.js') }}"></script>
	
<!---  Nice Select menu -->
@endsection
