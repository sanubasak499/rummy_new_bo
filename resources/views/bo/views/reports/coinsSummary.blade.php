@extends('bo.layouts.master')
@section('title', "Coins & Reward Points Summary| PB BO")
@section('pre_css')
@endsection
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="#">Report</a></li>
                            <li class="breadcrumb-item active">Coin Summary</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Coins & Reward Points Summary</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form id="userSearchForm" method="post" action="{{ route('reports.coinsummary.coinsSummary') }}{{ !empty($params) ? empty($params['page']) ? '' : "?page=".$params['page'] : '' }}">
                            @csrf
                            <div class="form-row align-items-center">
                                <div class="form-group col-md-2">
                                    <label class="mb-1">Search By: </label>
                                </div>
                                <div class="form-group col-md-5">
                                    <div class="radio radio-info form-check-inline">
                                        <input data-default-checked="" type="radio" id="search_by_username" value="USERNAME" name="search_by" {{ !empty($params) ? $params['search_by'] == 'USERNAME' ? "checked" : ''  : ''  }} checked="">
                                        <label for="search_by_username"> Username </label>
                                    </div>
                                    <div class="radio radio-info form-check-inline">
                                        <input type="radio" id="search_by_email" value="EMAIL_ID" name="search_by" {{ !empty($params) ? $params['search_by'] == 'EMAIL_ID' ? "checked" : ''  : ''  }}>
                                        <label for="search_by_email"> Email </label>
                                    </div>
                                    <div class="radio radio-info form-check-inline">
                                        <input type="radio" id="search_by_contact_no" value="CONTACT" name="search_by" {{ !empty($params) ? $params['search_by'] == 'CONTACT' ? "checked" : ''  : ''  }}>
                                        <label for="search_by_contact_no"> Contact No </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-5">
                                    <label class="sr-only" for="search_by_value">Search Input</label>
                                    <input type="text" class="form-control" name="search_by_value" id="search_by_value" placeholder="Search by Username, Email & Contact No" value="{{ !empty($params) ? $params['search_by_value']  : ''  }}">
                                </div>
                            </div>

                            <div class="form-row customDatePickerWrapper">
                                <div class="form-group col-md-4">
                                    <label>From: </label>
                                    <input name="START_DATE_TIME" id="date_from" type="text" class="form-control customDatePicker from flatpickr-input" placeholder="Select From Date" value="{{ !empty($params) ? $params['START_DATE_TIME']  : ''  }}" readonly="readonly">
                                </div>
                                <div class="form-group col-md-4">
                                    <label>To: </label>
                                    <input name="END_DATE_TIME" id="date_to" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="{{ !empty($params) ? $params['END_DATE_TIME']  : ''  }}" readonly="readonly">
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Date Range: </label>
                                    <select name="date_range" id="date_range" class="customselect formatedDateRange" data-plugin="customselect">
                                        <option data-default-selected="" value="" selected="">Select Date Range</option>
                                        <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                                        <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                                        <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                                        <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                                        <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                                        <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                                    </select>
                                </div>
                            </div>


                            <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                            <a href="{{ route('reports.coinsummary.index') }}" class="btn btn-secondary waves-effect waves-light ml-1">
                                <i class="fas fa-eraser mr-1"></i> Clear
                            </a>
                        </form>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        @if(!empty($transactionData))
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <div class="card-widgets">
                            <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                        </div>
                        <h5 class="card-title mb-0 text-white">Coins & Reward Points Summary</h5>
                    </div>
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-4">
                                <h5 class="font-15 mt-0">Total Accumulated Coins: <span class="text-danger">@if(!empty($USER_TOT_BALANCE))({{ $USER_TOT_BALANCE }})@else 0 @endif </span></h5>
                            </div>
                        </div>
                        <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Username</th>
                                    <th>Accumulated Coins</th>
                                    <th>Current Coins Balance</th>
                                    <th>Accumulated RPs</th>
                                    <th>RPs Spent</th>
                                    <th>Balance RPs</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1; @endphp
                                @foreach($transactionData as $key => $result)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $result->USERNAME }}</td>
                                    <td>{{ $result->TRANSACTION_AMOUNT}}</td>
                                    <td>{{ $result->USER_TOT_BALANCE }}</td>
                                    <td>{{ $result->TransactionAmount }}</td>
                                    <td> {{ $result->RPCONVERTED }} </td>
                                    <td>{{ $result->userTotBalance }}</td>

                                  

                                </tr>
                                @php $i++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                        <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" style="display:flex; justify-content: space-between;" class="mt-2">
                            @if(!empty($transactionData) && (sizeof($transactionData)>0))
                            <div>More Records</div>
                            <div>
                                {{ $transactionData->render("pagination::customPaginationView") }}
                            </div>
                            @endif
                        </div>
                    </div> <!-- end card body-->
                </div> <!-- end card -->
            </div><!-- end col-->
        </div>
        @endif

    </div>
</div>
@endsection
@section('js')
@endsection