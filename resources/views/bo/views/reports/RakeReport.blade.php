@extends('bo.layouts.master')
@section('title', "Rake Report| PB BO")
@section('pre_css')

@endsection
@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="#">Reports </a></li>
                            <li class="breadcrumb-item active">Daily Rake</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Daily Rake Report</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- Error massge -->
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button> 
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <!-- end Error Massage -->
                    <div class="card-body">
                        <form id="rake-report-form" action="{{ route('reports.rake-report.exportExcel') }}" method="post">
                            @csrf
                            <div class="form-row customDatePickerWrapper">
                                <div class="form-group col-lg-8 col-md-8 col-sm-8">
                                    <label>Date  </label>
                                    <input name="date_from" type="text" class="form-control customDatePicker from  @error('date_from') is-invalid @enderror" placeholder="Date" value="">
                                    @error('date_from')
                                    <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                                    @enderror
                                </div>
<!--                                <div class="form-group  col-lg-5 col-md-4 col-sm-4">
                                    <label>To: </label>
                                    <input name="date_to" type="text" class="form-control customDatePicker to @error('date_to') is-invalid @enderror" placeholder="Select To Date" value="">
                                    @error('date_to')
                                    <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                                    @enderror
                                </div>-->
                                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                    <label class="text-white d-none d-sm-block d-md-block">.</label>
                                    <button type="submit" data-toggle="modal" data-target="#setuserflagpopup" class="w-100 btn btn-success waves-effect waves-light d-inline-block mr-1">
                                        <i class="mdi mdi-file-excel mr-1"></i>  Download Rake Report
                           
                                    </button>
                                </div>
                            </div>
                        </form> 


<!--                        <div class="row mb-2">
                            <div class="col-sm-4 mb-1">
                                <a href="{{ route('reports.rake-report.exportExcel') }}" > <button type="button"  data-toggle="modal" data-target="#setuserflagpopup" class="btn btn-success waves-effect waves-light d-inline-block mr-1">
                                        <i class="mdi mdi-file-excel mr-1" ></i> Download Yesterday's Rake Report
                                    </button></a>
                                     <button type="button" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Get Yesterday's Report</button> 
                            </div>
                        </div>-->
                            <!-- <div class="col-sm-8  mb-1 text-md-right text-sm-left"><button type="button"  data-toggle="modal" data-target="#setuserflagpopup" class="btn btn-success waves-effect waves-light d-inline-block mr-1">
                               <i class="mdi mdi-file-excel mr-1" ></i> Download Excel
                               </button>
                            </div> -->
                            <!-- end col-->

                    </div>
                </div>
                <!-- end card body-->
            </div>
            <!-- end card -->
            <!-- end col-->
        </div>
    </div>
</div>
</div>
</div>
@endsection

@section('js')

<script>
    $("#rake-report-form").validate({
        rules: {
          // simple ;rule, converted to {required:true}
          date_from: {
            required:true
          },
          // compound rule
//          date_to: {
//            required: true,
//          }
        }
      });
</script>
@endsection