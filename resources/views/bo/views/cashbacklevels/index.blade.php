@extends('bo.layouts.master')

@section('title', "Cashback Levels | PB BO")

@section('pre_css')

@endsection

@section('post_css')

@endsection

@section('content')
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a>Baazi Rewards</a></li>
                     <li class="breadcrumb-item active"> Cashback Levels</li>
                  </ol>
               </div>
               <h4 class="page-title">
                  Cashback Levels
               </h4>
            </div>
         </div>
      </div>
      <!-- end col -->
   </div>
   <div class="row">
      <div class="col-12">
         <div class="card">
            <div class="card-body">
               <div class="row mb-1">
                  <div class="col-lg-8">
                     <h5 class="font-18">Manage Cashback Levels</h5>
                  </div>
                  <div class="col-lg-4 mb-2">
                     <div class="text-lg-right mt-lg-0"><a href="#" data-toggle="modal" data-target="#addModel" class="btn btn-success waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Create Level</a></div>
                  </div>
               </div>
               <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                  <thead>
                     <tr>
                        <th>Level #</th>
                        <th>Min Reward Point</th>
                        <th>Max Reward Point</th>
                        <th>Cashback Amount</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach ($RewardCashbackLevelResult as $key => $result)
                     <tr>
                        <td>{{ $result->LEVEL_ID }} </td>
                        <td>{{ $result->MIN_REWARD_POINTS }} </td>
                        <td>{{ $result->MAX_REWARD_POINTS }} </td>
                        <td>{{ $result->CASHBACK_AMOUNT }} </td>
                        <td>
                           <a class="action-icon font-14 tooltips" href="#" data-toggle="modal" data-target="#editModel_{{ $result->LEVEL_ID }}">
                              <i class="fa fa-edit"></i><span class="tooltiptext">Edit</span>
                           </a>

                           <div class="modal fade" id="editModel_{{ $result->LEVEL_ID }}">
                              <div class="modal-dialog ">
                                 <div class="modal-content bg-transparent shadow-none">
                                    <div class="card">
                                       <div class="card-header bg-dark py-3 text-white">
                                          <div class="card-widgets">
                                             <a href="#" data-dismiss="modal">
                                                <i class="mdi mdi-close"></i>
                                             </a>
                                          </div>
                                          <h5 class="card-title mb-0 text-white font-18">Edit Cashback Levels</h5>
                                       </div>
                                       <form id="EditCashbackLevel" data-default-url="{{route('baazirewards.cashback-levels.UpdateCashbackLevel')}}" action="{{route('baazirewards.cashback-levels.UpdateCashbackLevel')}}" method="post">
                                          @csrf
                                          <div class="card-body">
                                             <input type="hidden" class="form-control" name="LEVEL_ID" id="LEVEL_ID" placeholder="Level" value="{{ $result->LEVEL_ID }}">
                                             <div class="form-group">
                                                <label>Min Reward Point: <span class="text-danger">*</span></label>
                                                <input type="number" class="form-control" name="MIN_REWARD_POINTS" id="MIN_REWARD_POINTS" placeholder="Min Reward Points" value="{{ $result->MIN_REWARD_POINTS }}" step='0.01'  min="1" required="">
                                             </div>
                                             <div class="form-group mb-3">
                                                <label>Max Reward Point: <span class="text-danger">*</span></label>
                                                <input type="number" class="form-control" name="MAX_REWARD_POINTS" id="MAX_REWARD_POINTS" placeholder="Max Reward Points" value="{{ $result->MAX_REWARD_POINTS }}" step='0.01' min="1" required="">
                                             </div>
                                             <div class="form-group mb-3">
                                                <label>Cashback Amount: <span class="text-danger">*</span></label>
                                                <input type="number" class="form-control" name="CASHBACK_AMOUNT" id="CASHBACK_AMOUNT" placeholder="Cashback amount" value="{{ $result->CASHBACK_AMOUNT }}" step='0.01' min="1" required="">
                                             </div>
                                             <button type="submit" name="Update" id="Update" class="btn btn-warning waves-effect waves-light mr-1 formActionButton"><i class="far fa-edit  mr-1"></i> Update</button>
                                             <button style="display:none" class="btn btn-primary formActionButton mr-1" type="button" disabled>
                                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                                Loading...
                                             </button>
                                             <button type="reset" id="resetModel" class="btn btn-secondary waves-effect waves-light"><i class="fas fa-eraser mr-1"></i> Clear</button>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
               <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" class="mt-2">
                  <div>More Records</div>
                  <div>
                  </div>
               </div>
            </div>
            <!-- end card body-->
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
   </div>
</div>
</div>
<div class="modal fade" id="addModel">
   <div class="modal-dialog ">
      <div class="modal-content bg-transparent shadow-none">
         <div class="card">
            <div class="card-header bg-dark py-3 text-white">
               <div class="card-widgets">
                  <a href="#" data-dismiss="modal">
                     <i class="mdi mdi-close"></i>
                  </a>
               </div>
               <h5 class="card-title mb-0 text-white font-18">Add Cashback Levels</h5>
            </div>
            <form id="AddCashbackLevel" data-default-url="{{route('baazirewards.cashback-levels.insertCashbackLevel')}}" action="{{route('baazirewards.cashback-levels.insertCashbackLevel')}}" method="post">
               @csrf
               <div class="card-body">
                  <div class="form-group">
                     <label>Min Reward Point: <span class="text-danger">*</span></label>
                     <input type="number" class="form-control" name="MIN_REWARD_POINTS" id="MIN_REWARD_POINTS" placeholder="Min Reward Points" value="" required="" step='0.01' min="1">
                  </div>
                  <div class="form-group mb-3">
                     <label>Max Reward Point: <span class="text-danger">*</span></label>
                     <input type="number" class="form-control" name="MAX_REWARD_POINTS" id="MAX_REWARD_POINTS" placeholder="Max Reward Points" value="" required="" step='0.01' min="1">
                  </div>
                  <div class="form-group mb-3">
                     <label>Cashback Amount: <span class="text-danger">*</span></label>
                     <input type="number" class="form-control" name="CASHBACK_AMOUNT" id="CASHBACK_AMOUNT" placeholder="Cashback amount" value="" required="" step='0.01' min="1">
                  </div>
                  <button type="submit" name="confirmExcelUpload" id="confirmExcelUpload" class="btn btn-success waves-effect waves-light formActionButtonvv"><i class="fa fa-save mr-1"></i> Save</button>
                  <button style="display:none" class="btn btn-primary formActionButtonvv mr-1" type="button" disabled>
                     <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                     Loading...
                  </button>
                  <button type="reset" id="reset" class="btn btn-secondary waves-effect waves-light"><i class="fas fa-eraser mr-1"></i> Clear</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection

@section('js')
<script>
   $(document).ready(function() {
      $("#reset").click(function() {
         trigger("reset");
      });

      $("#resetModel").click(function() {
         trigger("reset");
      });

      $("#EditCashbackLevel").validate({

      });

      $('#Update').on('click', (e) => {
         if ($("#EditCashbackLevel").valid()) {
            $('.formActionButton').toggle();
            $('#Update').hide();
         }
      });

      $('#confirmExcelUpload').on('click', (e) => {
         if ($("#AddCashbackLevel").valid()) {
            $('.formActionButtonvv').toggle();
            $('#confirmExcelUpload').hide();
         }
      });
   })
</script>
@endsection
@section('post_js')
@endsection