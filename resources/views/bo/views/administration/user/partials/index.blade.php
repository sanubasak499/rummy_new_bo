<ul class="sitemap permission-list">
    @foreach ($menus as $key => $item)
    <li class="permission-item" data-id="{{ $item->id }}">
        @php
        $module = $item->getTable() == "bo_admin_menus" ? $item->module : $item;
        // bo_admin_customized_admin_user_permissions
        $rolePermission = count($custom_permissions->where('module_id', $module->id)) > 0 ? $custom_permissions->where('module_id', $module->id)->first() : null;
        if(empty($rolePermission)){
            $rolePermission = count($items->where('module_id', $module->id)) > 0 ? $items->where('module_id', $module->id)->first() : null;
        }
        $rolePermissionTable = !empty($rolePermission) ? $rolePermission->getTable() : null;
        @endphp
        
        <a href="javascript: void(0);" class="text-uppercase permission-box">{{ $module->display_name }} 
            <div class="float-right d-flex justify-content-between" style="width:60px;">  
                <label class="permission-label">
                    <input data-check-box-type="view" class="permission-checkbox permission-checkbox-view" type="checkbox" name="modules[{{ $module->id }}][view]" value="1" {{ !empty($rolePermission) ? ($rolePermission->view ? 'checked' : '') : '' }}>
                </label>
                <label class="permission-label">
                    <input data-check-box-type="edit" class="permission-checkbox permission-checkbox-edit" type="checkbox" name="modules[{{ $module->id }}][edit]" value="1" {{ !empty($rolePermission) ? ($rolePermission->edit ? 'checked' : '') : '' }}>
                </label>
            </div>
        </a>
        
        @if($item->getTable() == "bo_admin_menus")
        @if(!$item->children->isEmpty())
        @include('bo.views.administration.user.partials.index', ['items' => $items ?? collect([]), 'menus'=>$item->children])
        @endif
        @endif
        @endforeach
    </li>
</ul>