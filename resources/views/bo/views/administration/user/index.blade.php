@extends('bo.layouts.master')

@section('title', "Admin Users | PB BO")

@section('pre_css')
<link rel="stylesheet" href="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.css') }}">    
@endsection

@section('content')

<!-- start page title -->
<div class="row">
    <div class="col-12">
       <div class="page-title-box">
          <div class="page-title-right">
             <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                <li class="breadcrumb-item active">Admin Users</li>
             </ol>
          </div>
          <h4 class="page-title">Admin Users</h4>
       </div>
    </div>
 </div>
<!-- end page title --> 


<div class="card">
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-sm-4">
               <h5 class="font-18">View Admin Users </h5>
            </div>
            <div class="col-sm-8">
               <div class="text-sm-right">
                  <a href="{{ route('administration.user.create') }}" class="btn btn-success waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add User</a>
               </div>
            </div>
            <!-- end col-->
         </div>
         <form id="userSearchForm" data-default-url="{{ route('administration.user.filter') }}" action="{{ route('administration.user.filter') }}{{ !empty($params) ? empty($params['page']) ? "" : "?page=".$params['page'] : '' }}" method="post">
             @csrf
            <div class="form-row align-items-center">
                <div class="form-group col-md-2">
                    <label class="mb-1">Search By: </label>
                 </div>
                <div class="form-group col-md-5">
                    <div class="radio radio-info form-check-inline">
                        <input data-default-checked type="radio" id="search_by_username" value="username" name="search_by" {{ !empty($params) ? $params['search_by'] == "username" ? "checked" : '' : "checked" }}>
                        <label for="search_by_username"> Username </label>
                    </div>
                    <div class="radio radio-info form-check-inline">
                        <input type="radio" id="search_by_email" value="email" name="search_by" {{ !empty($params) ? $params['search_by'] == "email" ? "checked" : '' : "" }}>
                        <label for="search_by_email"> Email </label>
                    </div>
                    <div class="radio radio-info form-check-inline">
                        <input type="radio" id="search_by_contact_no" value="contact_no" name="search_by" {{ !empty($params) ? $params['search_by'] == "contact_no" ? "checked" : '' : "" }}>
                        <label for="search_by_contact_no"> Contact No </label>
                    </div>
                </div>
                <div class="form-group col-md-5">
                    <label class="sr-only" for="search_by_value">Search Input</label>
                    <input type="text" class="form-control" name="search_by_value" id="search_by_value" placeholder="Search Input" value="{{ !empty($params) ? $params['search_by_value'] : '' }}">
                </div>
            </div>
             
            <div class="form-row customDatePickerWrapper">
                <div class="form-group col-md-4">
                    <label>From: </label>
                    <input name="date_from" type="text" class="form-control customDatePicker from" placeholder="Select From Date" value="{{ !empty($params) ? $params['date_from']  : ''  }}">
                </div>
                <div class="form-group col-md-4">
                    <label>To: </label>
                    <input name="date_to" type="text" class="form-control customDatePicker to" placeholder="Select To Date" value="{{ !empty($params) ? $params['date_to']  : ''  }}">
                </div>
                <div class="form-group col-md-4">
                    <label>Date Range: </label>
                    <select name="date_range" id="date_range" class="customselect formatedDateRange" data-plugin="customselect">
                        <option data-default-selected value value="" {{ !empty($params) ? $params['date_range'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                        <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                        <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                        <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                        <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                        <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                        <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                    </select>
                </div>
            </div>
             
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label>Status: </label> 
                    <select name="status" class="customselect" data-plugin="customselect">
                        <option data-default-selected value="" {{ !empty($params) ? $params['status'] == "" ? "selected" : '' :  'selected'  }}>Select</option>
                        <option value="none" {{ !empty($params) ? $params['status'] == "none" ? "selected" : ''  : ''  }}>Inactive</option>
                        <option value="1" {{ !empty($params) ? $params['status'] == 1 ? "selected" : ''  : ''  }}>Active</option>
                        <option value="2" {{ !empty($params) ? $params['status'] == 2 ? "selected" : ''  : ''  }}>Blocked</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label>Role: </label>
                    <select name="role" class="customselect" data-plugin="customselect">
                        <option data-default-selected value="" {{ !empty($params) ? $params['role'] == "" ? "selected" : '' :  'selected'  }}>Select</option>
                        @foreach ($roles as $role)
                        <option value="{{ $role->id }}" {{ !empty($params) ? $params['role'] == $role->id ? "selected" : ''  : ''  }}>{{ ucfirst($role->name) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
             
            <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
            <a href="#" class="btn btn-secondary waves-effect waves-light ml-1" data-toggle="reload" data-form-id="#userSearchForm"><i class="mdi mdi-replay mr-1"></i> Reset</a>
        </form> 
    </div>
</div> <!-- end card-->

@if(!empty($users))
<div class="card">
    <div class="card-header bg-dark text-white">
        <div class="card-widgets">
            {{-- <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a> --}}
            {{-- <a href="javascript:;"><i class="mdi mdi-plus"></i></a>                                    --}}
        </div>
        <h5 class="card-title mb-0 text-white">Users List</h5>
        </div>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col-sm-12">
               <h5 class="font-15 mt-0">Total Record: <span class="text-danger">({{ $stats['total']->total ?? 0 }}) </span>&nbsp;&nbsp;|&nbsp;&nbsp;Total Active: <span class="text-danger">({{ $stats['1']->total ?? 0 }})</span>&nbsp;&nbsp;|&nbsp;&nbsp;Total Inactive: <span class="text-danger">({{ $stats['0']->total ?? 0 }})</span>  </h5>
            </div>
         </div>
        <table id="basic-datatable" class="table dt-responsive nowrap">
            <thead>
                <tr>
                    <th>S. No.</th>
                    <th>Username</th>
                    <th>Full Name</th>
                    <th>Role</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Created Date</th>
                    <th>Account Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i=0;
                @endphp
                @foreach ($users as $key => $user)
                <tr>
                    <td data-user-id="{{ $user->id }}">{{ ++$i }}</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->FULLNAME }}</td>
                    <td>{{ ucfirst($user->role->name) }}</td>
                    <td>{{ $user->mobile }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{!!  changeDateFormate($user->created_at)  !!}</td>
                    <td>{!! $user->account_status == 1 ? '<span class="badge bg-soft-success text-success shadow-none">Active</span>' : '<span class="badge bg-soft-danger text-danger shadow-none">InActive</span>' !!}</td>
                    <td>
                        <a href="{{ route('administration.user.edit', encrypt($user->id)) }}" target="_blank" class="action-icon font-14" title="Edit" data-plugin="tippy" data-tippy-interactive="true">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="{{ route('administration.user.rolePermissions', encrypt($user->id)) }}" target="_blank" class="action-icon font-14" title="View/Edit Role & Permissions" data-plugin="tippy" data-tippy-interactive="true"> 
                            <i class="fa fa-user-lock"></i> 
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="customPaginationRender customPaginationStyle mt-2" data-form-id="#userSearchForm">
            <div>More Records</div>
            <div>
                {{ $users->render("pagination::customPaginationView") }}
            </div>
        </div>
    </div> <!-- end card body-->
</div> <!-- end card -->
@endif
@endsection

@section('js')
<script src="{{ asset('assets/libs/jquery-nice-select/jquery-nice-select.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>
@endsection