@extends('bo.layouts.master')

@section('title', "Player Search | PB BO")

@section('pre_css')
    <style>
        .toggle-password{
            position: absolute;
            top: 11px;
            right: 10px;
            z-index: 4;
        }
    </style>
@endsection

@section('content')

 <!-- start page title -->
 <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                    <li class="breadcrumb-item active">Add User</li>
                </ol>
            </div>
            <h4 class="page-title">{{ empty($user) ? 'Add' : 'Edit' }} User</h4>
        </div>
    </div>
</div>
<!-- end page title -->

<!-- Form row -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @php
                    $user = $user ?? null;
                @endphp
                <form id="{{ $user ? 'adminUserUpdate' : 'adminUserRegister' }}" action="{{ $user ? route('administration.user.update', encrypt($user->id)) : route('administration.user.store') }}" method="post">
                    @csrf
                    
                    @if($user)
                    @method('put')
                    <input type="hidden" name="old_password" value="{{ $user->password }}">
                    <input type="hidden" name="old_transcation_password" value="{{ $user->transcation_password }}">
                    @endif

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="col-form-label">First Name</label>
                            <input type="text" name="firstname" class="form-control" placeholder="Enter Firstname" value="{{ $user->firstname ?? '' }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-form-label">Last Name</label>
                            <input type="text" name="lastname" class="form-control" placeholder="Enter Lastname" value="{{ $user->lastname ?? '' }}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="col-form-label">Username</label>
                            <input type="text" name="username" class="form-control" placeholder="Username" value="{{ $user->username ?? '' }}" {{ $user ? 'readonly' : '' }}>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-form-label">Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Email" value="{{ $user->email ?? '' }}" {{ $user ? 'readonly' : '' }}>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="col-form-label">Password</label>
                            <div class="input-group">
                                <span data-toggle-password="" class="mr-3 toggle-password fa fa-fw field-icon fa-eye-slash"></span>
                                <input type="password" name="password" class="form-control w-100" placeholder="Password" value="{{ $user ? '' : '' }}">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-form-label">Transaction Password</label>
                            <div class="input-group">
                                <span data-toggle-password="" class="mr-3 toggle-password fa fa-fw field-icon fa-eye-slash"></span>
                                <input type="password" name="transcation_password" class="form-control w-100" placeholder="Password" value="{{ $user ? '' : '' }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="col-form-label">Mobile No.</label>
                            <input type="text" name="mobile" class="form-control" placeholder="Mobile Number" value="{{ $user->mobile ?? '' }}" {{ $user ? 'readonly' : '' }}>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-form-label">Role: </label>
                            <select name="role_id" class="form-control">
                                <option data-default-selected value="" {{ $user ? '' :  'selected' }}>Select</option>
                                @foreach ($roles as $role)
                                <option value="{{ $role->id }}" {{ $user ? ($user->role_id == $role->id ? "selected" : '')  : ''  }}>{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @if($user)
                    <button type="submit" class="btn btn-warning waves-effect waves-light ml-1"><i class="far fa-edit mr-1"></i> Update</button>
                    @else
                    <button type="submit" class="btn btn-success waves-effect waves-light  "><i class="fa fa-save mr-1"></i> Submit</button>
                    @endif
                    {{-- <button type="button" class="btn btn-dark waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i>Reset</button> --}}
                </form>
            </div>
            <!-- end card-body -->
        </div>
        <!-- end card-->
    </div>
    <!-- end col -->
</div>
<!-- end row -->
@endsection

@section('post_js')
<script src="{{ asset('js/bo/admin_register.js') }}"></script>
@endsection