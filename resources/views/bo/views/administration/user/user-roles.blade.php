@extends('bo.layouts.master')

@section('title', "Role & Permissions | PB BO")

@section('pre_css')
    
@endsection

@section('post_css')
    <link href="{{ asset('assets/bo/pages/role_permission/role_permission.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title">User Role & Permission</h4>
        </div>
    </div>
</div>     
<!-- end page title --> 

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            {{-- Menu role and permissions --}}
            <form action="{{ route('administration.user.rolePermissionsUpdate', encrypt($user->id)) }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="header-title">Role Permission List</h4>
                        <div class="custom-dd-empty dd card" id="menuOrderNestable">
                            <div class="card-body">
                                <ul class="sitemap">
                                    <li>
                                        <a href="javascript: void(0);" class="text-uppercase">Modules
                                            <div class="float-right d-flex justify-content-between" style="width:80px;">
                                                <span>View</span>
                                                <span>Edit</span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <hr>    
                                @include('bo.views.administration.user.partials.index', ['items'=>$permissions, 'menus'=>$menus, 'custom_permissions'=>$custom_permissions])
                                
                            </div>
                        </div>
                    </div><!-- end col -->
                
                    {{-- internal pages role and permissions --}}
                    <div class="col-md-6">
                        <h4 class="header-title">Internal Pages Permission List</h4>
                        <div class="custom-dd-empty dd card" id="menuOrderNestable">
                            <div class="card-body">
                                <ul class="sitemap">
                                    <li>
                                        <a href="javascript: void(0);" class="text-uppercase">Modules
                                            <div class="float-right d-flex justify-content-between" style="width:80px;">
                                                <span>View</span>
                                                <span>Edit</span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <hr>
                                @include('bo.views.administration.user.partials.index', ['items'=>$permissions, 'menus'=>$custom_modules, 'custom_permissions'=>$custom_permissions])
                            </div>
                        </div>
                    </div><!-- end col -->
                </div> <!-- end row -->
                <button type="submit" class="btn btn-primary">Update Permissions</button>
            </form>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>
@endsection

@section('js')
    
@endsection

@section('post_js')
<script src="{{ asset('assets/bo/pages/role_permission/role_permission.min.js') }}"></script>
@endsection