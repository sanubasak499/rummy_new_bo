@extends('bo.layouts.master')

@section('title', "Roles List | PB BO")

@section('pre_css')

@endsection

@section('content')

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                    <li class="breadcrumb-item active">Roles List</li>
                </ol>
            </div>
            <h4 class="page-title">Manage Roles </h4>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row mb-2">
                    <div class="col-sm-4">
                        <h4 class="header-title mt-1">View Role List </h4>
                    </div>
                    <div class="col-sm-8">
                        <div class="text-sm-right">
                            <a href="#addRoleModal" class="btn btn-success waves-effect waves-light" data-toggle="modal"
                                title="Add Role" data-target="#addRoleModal" title="Add Role"><i
                                    class="fa fa-plus mr-1"></i> Add Role</a>
                        </div>
                    </div><!-- end col-->
                </div>

                <table id="basic-datatable" class="table dt-responsive nowrap roleTable table-striped">
                    <thead>
                        <tr>
                            <th>S. No.</th>
                            <th>Role</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i=0;
                        @endphp
                        @foreach ($roles as $key => $role)
                        <tr class="roleRow" data-role-id="{{ $role->id }}">
                            <td data-role-id="{{ $role->id }}"> {{ ++$i }}</td>
                            <td data-name>{{ $role->name }}</td>
                            <td data-description class="ellipsisWrapper">
                                <div class="ellipsis">
                                    {{ $role->Description50Char }}
                                </div>
                            </td>
                            <td>
                                <a class="action-icon font-14" href="#editRoleModal" data-toggle="modal"  data-role-id="{{ $role->id }}" data-role-encrypted-id="{{ encrypt($role->id) }}" title="Edit" data-target="#editRoleModal" data-plugin="tippy" data-tippy-interactive="true"> 
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="{{ route('administration.role.permission.index', encrypt($role->id)) }}" target="_blank" class="action-icon font-14" title="View/Edit Role & Permissions" data-plugin="tippy" data-tippy-interactive="true"> 
                                    <i class="fa fa-user-lock"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<div class="modal fade" id="addRoleModal">
    <div class="modal-dialog">
        <div class="modal-content bg-transparent shadow-none">
            <div class="card">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal"><i class="mdi mdi-close"></i></a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18">Add Role</h5>
                </div>

                <div class="card-body">
                    <form data-form-url="{{ route('administration.role.store') }}"
                        action="{{ route('administration.role.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Role</label>
                            <input type="text" class="form-control" name="name" placeholder="Enter Role" required>
                        </div>
                        <div class="form-group mb-3">
                            <label>Description</label>
                            <textarea class="form-control" name="description" rows="5" required></textarea>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success waves-effect waves-light"><i
                                    class="fa fa-save mr-1"></i> Save</button>
                            <button type="button" class="btn btn-dark waves-effect waves-light ml-2"
                                data-toggle="reload"><i class="mdi mdi-replay mr-1"></i>Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editRoleModal">
    <div class="modal-dialog">
        <div class="modal-content bg-transparent shadow-none">
            <div class="card">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal"><i class="mdi mdi-close"></i></a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18">Edit Role</h5>
                </div>

                <div class="card-body">
                    <form data-form-index-url="{{ route('administration.role.index') }}" data-form-url="{{ route('administration.role.index') }}"
                        action="{{ route('administration.role.index') }}" method="post">
                        @csrf
                        @method('put')
                        <input type="hidden" data-default-reset name="id">
                        <div class="form-group">
                            <label for="name">Role</label>
                            <input type="text" data-default-reset class="form-control" name="name"
                                placeholder="Enter Role" required>
                        </div>
                        <div class="form-group mb-3">
                            <label>Description</label>
                            <textarea type="text" data-default-reset class="form-control" name="description" rows="5"
                                required></textarea>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success waves-effect waves-light"><i
                                    class="fa fa-save mr-1"></i> Update</button>
                            <button type="button" class="btn btn-dark waves-effect waves-light ml-2"
                                data-toggle="reload"><i class="mdi mdi-replay mr-1"></i>Reset</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('post_js')
<script>
    $(document).ready(function () {
        $('#addRoleModal').on('hidden.bs.modal', function (e) {
            $(this).find(`[data-toggle="reload"]`).trigger('click');
        });
        $('#editRoleModal').on('hidden.bs.modal', function (e) {
            $(this).find(`[name]`).val('');
        });
        $('#editRoleModal').on('show.bs.modal', function (e) {
            let $editButton = $(e.relatedTarget);
            
            var $modal = $(this);
            var $form = $modal.find('form');
            
            let $toUrl = `${$form.data('formIndexUrl')}/${$editButton.data('roleEncryptedId')}`;
            let $ajaxResponse = editRoleAjaxCall($toUrl);

            $ajaxResponse.done((response)=>{
                if(response.status == 200){
                    let data = response.data;
                    $form.find(`[name="id"]`).data('defaultReset', data.id).val(data.id);
                    $form.find(`[name="name"]`).data('defaultReset', data.name).val(data.name);
                    $form.find(`[name="description"]`).data('defaultReset', data.description).val(data.description);
                    $form.data('formUrl', $toUrl).attr('action', $toUrl);
                }else{
                    console.log(e,err);
                }
            }).fail((e,err)=>{
                console.log(e,err);
            });
        });

        var editRoleAjaxCall = function(url){
            window.pageData.ajaxRequests = window.pageData.ajaxRequests || [];
            let xhr = $.ajax({
                type: "get",
                url: `${url}/edit`,
            });

            window.pageData.ajaxRequests.push(xhr);
            for (var i = 0; i < (window.pageData.ajaxRequests.length - 1); i++) {
                window.pageData.ajaxRequests[i].abort();
            }
            return xhr;
        }
    });
</script>
@endsection