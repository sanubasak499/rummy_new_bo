@extends('bo.layouts.master')

@section('title', "Adjustment Transactions | PB BO")

@section('pre_css')
<!-- Plugins css -->
<link href="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    .multiselecttextoverflow  .dropdown-menu.show .dropdown-item{ white-space: normal; }
    .formtext {
        font-size: 11px;
        font-style: italic;
        color: 
        #999198;
        padding: 6px 0 0 0;
        margin: 0;
        line-height: 13px;
        display: none;
    }
</style>
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item">
                                <a href="{{ route('transaction.adjustment.index') }}">Transaction</a>
                            </li>
                            <li class="breadcrumb-item active">Adjustment Transactions</li>
                        </ol>
                    </div>
                    <h4 class="page-title">
                  Adjustment Transactions
               </h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form id="userSearchForm" action="{{ route('transaction.adjustment.index') }}" method="post">
                           @csrf
                            <div class="form-row align-items-center">
                                <div class="form-group col-md-2">
                                    <label class="mb-1">Search By: </label>
                                </div>
                                <div class="form-group col-md-5">
                                    <div class="radio radio-info form-check-inline">
                                        <input data-default-checked="" type="radio" id="search_by_username" value="username" name="search_by" {{ !empty($params) ? $params['search_by'] == 'username' ? "checked" : ''  : ''  }} checked="">
                                        <label for="search_by_username"> Username </label>
                                    </div>
                                    <div class="radio radio-info form-check-inline">
                                        <input type="radio" id="search_by_email" value="email" name="search_by" {{ !empty($params) ? $params['search_by'] == 'email' ? "checked" : ''  : ''  }}>
                                        <label for="search_by_email"> Email </label>
                                    </div>
                                    <div class="radio radio-info form-check-inline">
                                        <input type="radio" id="search_by_contact_no" value="contact_no" name="search_by" {{ !empty($params) ? $params['search_by'] == 'contact_no' ? "checked" : ''  : ''  }}>
                                        <label for="search_by_contact_no"> Contact No </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-5">
                                    <label class="sr-only" for="search_by_value">Search Input</label>
                                    <input type="text" class="form-control" name="search_by_value" id="search_by_value" placeholder="Search by Username, Email & Contact No" value="{{ !empty($params)? $params['search_by_value']:old('search_by_value') }}">
                                    @error('search_by_value')
                                        <label id="search_by_value-error" class="error" for="search_by_value">
                                            {{ $message }}
                                        </label>
                                    @enderror
                                </div>
                            </div>
                            <div class="row  align-items-center">
                                <div class="form-group col-md-4">
                                    <label>Reference No: </label>
                                    <input type="text" class="form-control" name="ref_no" id="ref_no" placeholder="Reference No" value="{{ !empty($params)? $params['ref_no']:old('ref_no') }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Amount:</label>
                                    <input type="text" class="form-control @error('amount') error @enderror" name="amount" id="amount" placeholder="Amount" value="{{ !empty($params)? $params['amount']:old('amount') }}">
                                    @error('amount')
                                        <label id="amount-error" class="error" for="amount">
                                            {{ $message }}
                                        </label>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Adjustment: </label>
                                    <select name="adjust_type" id="adjust_type" class="customselect" data-plugin="customselect">
                                        <option data-default-selected="" value="" selected="">Select Adjustment</option>
                                        <option value="1" {{ !empty($params) ? $params['adjust_type'] == 1 ? "selected" : ''  : ''  }}>Add</option>
                                        <option value="2" {{ !empty($params) ? $params['adjust_type'] == 2 ? "selected" : ''  : ''  }}>Deduct</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row  align-items-center">

                                <div class="form-group col-md-4 multiplecustom">
                                    <label>Balance Type:</label>
                                    <select id="balance_type" name="balance_type[]" class="form-control selectpicker" multiple data-style="btn-light">
                                        <option {{ !empty($params['balance_type']) ? in_array(21, $params['balance_type']) ? "selected" : ''  : ''  }} value="21">Promo</option>
                                        <option {{ !empty($params['balance_type']) ? in_array(22, $params['balance_type']) ? "selected" : ''  : ''  }} value="22">Deposit</option>
                                        <option {{ !empty($params['balance_type']) ? in_array(23, $params['balance_type']) ? "selected" : ''  : ''  }} value="23">Win</option>
                                        <option {{ !empty($params['balance_type']) ? in_array(88, $params['balance_type']) ? "selected" : ''  : ''  }} value="88">Technical Loss</option>
                                    </select>
                                    <p class="formtext clearfix"><i class=" mdi mdi-alert-circle mr-1"></i><i>For Technical Loss data available only till 01/Mar/2020.</i></p>
                                </div>
                                <div class="form-group col-md-4 multiplecustom">
                                    <label>Coin Type:</label>
                                    <select multiple="multiple" name="coin_type[]" class="form-control selectpicker"  data-style="btn-light">
                                        <option value="1" {{ !empty($params['coin_type']) ? in_array(1, $params['coin_type']) ? "selected" : ''  : ''  }}>Cash</option>
                                        <option value="3" {{ !empty($params['coin_type']) ? in_array(3, $params['coin_type'])  ? "selected" : ''  : ''  }}>FPP Bonus</option>
                                        <option value="12" {{ !empty($params['coin_type']) ? in_array(12, $params['coin_type']) ? "selected" : ''  : ''  }}>Reward Points</option>
                                        <option value="16" {{ !empty($params['coin_type']) ? in_array(16, $params['coin_type']) ? "selected" : ''  : ''  }}>Poker Commission</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4 multiplecustom">
                                    <label>Reason:</label>
                                    <select  name="reason[]" class="form-control selectpicker multiselecttextoverflow" multiple data-style="btn-light" data-live-search="true" >
                                    @if(!empty($reasons))
                                        @foreach($reasons as $reason)
                                            <option {{ !empty($params['reason']) ? in_array($reason->ACTIONS_REASONS_ID,$params['reason']) ? "selected" : ''  : ''  }} value="{{ $reason->ACTIONS_REASONS_ID }}" >{{ $reason->ACTIONS_REASON }}</option>
                                        @endforeach
                                    @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row  customDatePickerWrapper">
                                <div class="form-group col-md-4">
                                    <label>From: </label>
                                    <input name="date_from" id="date_from" type="text" class="form-control customDatePicker from flatpickr-input" placeholder="Select From Date" value="{{ !empty($params)? $params['date_from']:old('date_from') }}" readonly="readonly">
                                </div>
                                <div class="form-group col-md-4">
                                    <label>To: </label>
                                    <input name="date_to" id="date_to" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="{{ !empty($params)? $params['date_to']:old('date_to') }}" readonly="readonly">
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Date Range: </label>
                                    <select name="date_range" id="date_range" class="formatedDateRange customselect" data-plugin="customselect">
                                        <option data-default-selected="" value="" selected="">Select Date Range</option>
                                        <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                                        <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                                        <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                                        <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                                        <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                                        <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                <i class="fas fa-search mr-1"></i> Search
                            </button>
                            <a href="{{ route('transaction.adjustment.index') }}" class="btn btn-secondary waves-effect waves-light ml-1">
                                <i class="mdi mdi-replay mr-1"></i> Reset
                            </a>
                        </form>
                    </div>
                    <!-- end card-body-->
                </div>
            </div>
            <!-- end col -->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <!-- <div class="card-widgets">
                            <a href="javascript:;" data-toggle="reload">
                                <i class="mdi mdi-refresh"></i>
                            </a>
                            <a href="javascript:;"><i class="mdi mdi-plus"></i></a> 
                        </div> -->
                        <h5 class="card-title mb-0 text-white">Adjustment Transactions List</h5>
                    </div>
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-sm-6">
                                <h5 class="font-15 mt-0">Total Record: 
                                    <span class="text-danger">({{ !empty($totals) && $totals->totalCount!=''? $totals->totalCount:0 }}) </span>&nbsp;&nbsp;|&nbsp;&nbsp;Total Amount: 
                                    <span class="text-danger">({{ !empty($totals) && $totals->totalAmount!=''? $totals->totalAmount:0 }})</span>
                                </h5>
                            </div>
                            <div class="col-sm-6">
                                <div class="text-sm-right">

                                    <button  onclick="generateExcel();" type="button" class="btn btn-light mb-1 waves-effect waves-dark">
                                        <i class="fas fa-file-excel" style="color: #34a853;"></i> Export Excel
                                    </button>
                                </div>
                            </div>
                            <!-- end col-->
                        </div>
                        <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Username</th>
                                    <th>Reference No</th>
                                    <th>Balance Type</th>
                                    <th>Coin Type</th>
                                    <th>Amount</th>
                                    <th>Adjustment</th>
                                    <th>Adjusted By</th>
                                    <th>Transaction Date</th>
                                    <th>Reason</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataSets) && !empty($dataSets))
                                    <?php $sNo=1; ?>
                                   @forelse($dataSets as $dataSet)
                                    <tr>
                                        <td>{{$sNo++}}</td>
                                        <td>{{ $dataSet->username }}</td>
                                        <td>{{ $dataSet->Reference_No }}</td>
                                        <td>{{ ucfirst($dataSet->BALANCE_TYPE) }}</td>
                                        <td>{{ $dataSet->COIN_TYPE }}</td>
                                        <td>{{ $dataSet->AMOUNT }}</td>
                                        <td>{{ $dataSet->ADJUST_TYPE }}</td>
                                        <td>{{ $dataSet->ADJUSTED_BY }}</td>
                                        <td>{{ date('d/m/Y H:i:s',strtotime($dataSet->ADJUSTMENT_CREATED_ON))}}</td>
                                        <td>
                                            {{ $dataSet->REASON }}  
                                            @if(trim($dataSet->COMMENT)!='' && trim(strtoupper($dataSet->COMMENT))!='NA' && trim(strtoupper($dataSet->COMMENT))!='N\A')
                                                <a  title="View"  class="action-icon font-14  tooltips">
                                                    <i class="fa fa-info-circle"  onClick="showComment(`{{$dataSet->COMMENT}}`);"></i>  <span class="tooltiptext">View</span>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                   @endforeach
                                @endif    
                            </tbody>
                        </table>
                        <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" style="display:flex; justify-content: space-between;" class="mt-2">
                            <div>More Records</div>
                            <div>
                                {{ !empty($dataSets) ? $dataSets->render("pagination::customPaginationView"): '' }}
                            </div>
                        </div>
                    </div>
                    <!-- end card body-->
                </div>
                <!-- end card -->
            </div>
            <!-- end col-->
        </div>
    </div>
</div>
<div class="modal fade" id="adjustmentcontent">
    <div class="modal-dialog">
        <div class="modal-content bg-transparent shadow-none">
            <div class="card">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal">
                            <i class="mdi mdi-close"></i>
                        </a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18">Adjustment Content</h5>
                </div>
                <div class="card-body">
                    <form>
                        <div class="form-group mb-0">
                            <textarea class="form-control" name="description" rows="4" disabled>Test</textarea>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="adjustmentcontenttable">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content bg-transparent shadow-none">
            <div class="card">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal">
                            <i class="mdi mdi-close"></i>
                        </a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18">View Details</h5>
                </div>
                <div class="card-body">
                    <table class="table table-centered table-striped dt-responsive nowrap w-100" id="basic-datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Role</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>admin</td>
                                <td>product</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>production</td>
                                <td>product</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>production</td>
                                <td>product</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="aadharcardpopup">
    <div class="modal-dialog">
        <div class="modal-content bg-transparent shadow-none">
            <div class="card">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal">
                            <i class="mdi mdi-close"></i>
                        </a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18">Reason</h5>
                </div>
                <div class="card-body font-14">

                    <p id="comment"></p>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

<script src="{{asset('assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>

@endsection
@section('post_js')
<script type="text/javascript">
    $('document').ready(function(){
        $('#userSearchForm').validate({
            rules : {
                amount : {
                    number : true
                },
                search_by_value : {
                    email : {
                        depends: function(element) {
                            if($( 'input[name=search_by]:checked' ).val()=='email'){
                                return true;
                            }
                            else{
                                return false;
                            }
                        }
                    },
                    digits : {
                        depends: function(element) {
                            if($( 'input[name=search_by]:checked' ).val()=='contact_no'){
                                return true;
                            }
                            else{
                                return false;
                            }
                        }
                    }
                }
            }
        });

        showComment = (comment) => {
            $("#comment").html('');
            $("#comment").html(comment);
            $("#aadharcardpopup").modal('show');

        }

        $("#balance_type").change(function () {
            let values = $(this).val();
            values = new Set(values);
            values.has("88") ? $(".formtext").show() : $(".formtext").hide();
        });

    });

    

    generateExcel = () => {
        $("#userSearchForm").append('<input id="excelField" type="hidden" value="excel" name="excel">');
        $("#userSearchForm").submit();
        $("#excelField").remove();
    }
</script>
@endsection