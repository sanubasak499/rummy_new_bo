@extends('bo.layouts.master')
@section('title', "User Ledger| PB BO")
@section('pre_css')
<!-- Plugins css -->
<style type="text/css">
    .customselect {
        float: none;
        height: auto !important;
        line-height: 35px;
        border: 1px solid #ced4da;
        border-radius: .2rem;
    }

    .customselect ul.list {
        width: 100%
    }

    .textwarpcontainer {
        max-width: 200px
    }

    .textoverflow {
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        width: 100%;
    }

    .toggle-password {
        position: absolute;
        top: 11px;
        right: 10px;
        z-index: 4;
    }

    .multiplecustom .btn-light {
        background-color: transparent !important;
        -webkit-box-shadow: 0 1px 4px 0 rgba(0, 0, 0, .1) !important;
        box-shadow: 0 1px 4px 0 rgba(0, 0, 0, .1) !important;
        border: 1px solid #ced4da !important;
    }

    .imgcontainer {
        padding: 5px !important
    }
</style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="#">Transactions</a></li>
                        <li class="breadcrumb-item active">User Ledger</li>
                    </ol>
                </div>
                <h4 class="page-title">
                    User Ledger
                </h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form id="userSearchForm" action="{{ route('transaction.user-ledgers.UserLedgerSearch') }}{{ !empty($params) ? empty($params['page']) ? '' : "?page=".$params['page'] : '' }}" method="post">
                        @csrf
                        <div class="form-row align-items-center">
                            <div class="form-group col-md-2">
                                <label class="mb-1">Search By: </label>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="radio radio-info form-check-inline">
                                    <input data-default-checked="" type="radio" id="USERNAME" value="USERNAME" name="search_by" checked="" {{ !empty($params) ? $params['search_by'] == "USERNAME" ? "checked" : '' : "" }}>
                                    <label for="USERNAME"> Username </label>
                                </div>
                                <div class="radio radio-info form-check-inline">
                                    <input type="radio" id="EMAIL_ID" value="EMAIL_ID" name="search_by" {{ !empty($params) ? $params['search_by'] == "EMAIL_ID" ? "checked" : '' : "" }}>
                                    <label for="EMAIL_ID"> Email </label>
                                </div>
                                <div class="radio radio-info form-check-inline">
                                    <input type="radio" id="CONTACT" value="CONTACT" name="search_by" {{ !empty($params) ? $params['search_by'] == "CONTACT" ? "checked" : '' : "" }}>
                                    <label for="CONTACT"> Contact No </label>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="sr-only" for="search_by_value">Search Input</label>
                                <input type="text" class="form-control" name="search_by_value" id="search_by_value" placeholder="Search by Username, Email & Contact No" value="{{ !empty($params) ? $params['search_by_value']  : ''  }}">
                            </div>
                        </div>
                        <div class="row  align-items-center">
                            <div class="form-group col-md-4">
                                <label>Reference No: </label>
                                <input type="text" class="form-control" name="INTERNAL_REFERENCE_NO" id="ref_no" placeholder="Reference No" value="{{ !empty($params) ? $params['INTERNAL_REFERENCE_NO']  : ''  }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Amount:</label>
                                <input type="text" class="form-control" name="TRANSACTION_AMOUNT" id="amount" placeholder="Amount" value="{{ !empty($params) ? $params['TRANSACTION_AMOUNT']  : ''  }}">
                            </div>
                            <div class="form-group col-md-4 multiplecustom">
                                <label>Action Type:</label>
                                <select class="customselect" name="ACTION" id="ACTION" data-plugin="customselect">
                                    <option value="" selected="">Select</option>
                                    <option value="DEPOSIT" {{ !empty($params) && 'DEPOSIT'==$params['ACTION'] ? 'selected':'' }}>DEPOSIT</option>
                                    <option value="WITHDRAWAL" {{ !empty($params) && 'WITHDRAWAL'==$params['ACTION'] ? 'selected':'' }}>WITHDRAWAL</option>
                                </select>
                            </div>
                        </div>
                        <div class="row  customDatePickerWrapper">
                            <div class="form-group col-md-4">
                                <label>From: </label>
                                <input name="START_DATE_TIME" id="date_from" type="text" class="form-control customDatePicker from flatpickr-input" placeholder="Select From Date" value="{{ !empty($params) ? $params['START_DATE_TIME']  : ''  }}" readonly="readonly">
                            </div>
                            <div class="form-group col-md-4">
                                <label>To: </label>
                                <input name="END_DATE_TIME" id="date_to" type="text" class="form-control customDatePicker to flatpickr-input" placeholder="Select To Date" value="{{ !empty($params) ? $params['END_DATE_TIME']  : ''  }}" readonly="readonly">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Date Range: </label>
                                <select name="date_range" id="date_range" class="formatedDateRange customselect" data-plugin="customselect">
                                    <option data-default-selected="" value="" selected="">Select Date Range</option>
                                        <option value="1" {{ (1==old('date_range',$params['date_range'] ?? '')) ?'selected="selected"':''  }}>Today</option>
                                        <option value="2" {{ (2==old('date_range',$params['date_range'] ?? '')) ?'selected="selected"':''  }}>Yesterday</option>
                                        <option value="3" {{ (3==old('date_range',$params['date_range'] ?? '')) ?'selected="selected"':''  }}>This Week</option>
                                        <option value="4" {{ (4==old('date_range',$params['date_range'] ?? '')) ?'selected="selected"':''  }}>Last Week</option>
                                        <option value="5" {{ (5==old('date_range',$params['date_range'] ?? '')) ?'selected="selected"':''  }}>This Month</option>
                                        <option value="6" {{ (6==old('date_range',$params['date_range'] ?? '')) ?'selected="selected"':''  }}>Last Month</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                        <a href="{{ url('transaction/user-ledgers') }}" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                    </form>
                </div>
                <!-- end card-body-->
            </div>
        </div>
        <!-- end col -->
    </div>
    @if(!empty($userLedgersResult))
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-dark text-white">
                    <div class="card-widgets">
                        <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>
                    </div>
                    <h5 class="card-title mb-0 text-white"> User Ledger List</h5>
                </div>
                <div id="cardCollpase7" class="collapse show">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-sm-7">
                            </div>
                            <div class="col-sm-5">
                                <div class="text-sm-right">
                                    @if(!empty($userLedgersResult[0]))
                                    <a href="{{ route('transaction.user-ledgers.exportExcel')."?START_DATE_TIME=".$params['START_DATE_TIME']."&END_DATE_TIME=".$params['END_DATE_TIME']."&search_by=".$params['search_by']."&search_by_value=".$params['search_by_value']."&INTERNAL_REFERENCE_NO=".$params['INTERNAL_REFERENCE_NO']."&ACTION=".$params['ACTION']."&TRANSACTION_AMOUNT=".$params['TRANSACTION_AMOUNT']}}"> <button type="button" class="btn btn-light mb-1"><i class="fas fa-file-excel" style="color: #34a853;"></i>Export Excel</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <table id="basic-datatable" class="table dt-responsive nowrap w-100">
                            <thead>
                                <tr> 
                                    <th>#</th>
                                    <th class="no-sort">Username</th>
                                    <th class="no-sort">Action</th>
                                    <th class="no-sort">Amount</th>
                                    <th class="no-sort">Reference No</th>
                                    <th class="no-sort">Transaction<br /> Date</th>
                                    <th class="no-sort">Total<br /> Deposits</th>
                                    <th class="no-sort">Total <br />Withdrawals</th>
                                    <th class="no-sort">Total Taxable<br /> Withdrawals</th>
                                    <th class="no-sort">Eligible Withdrawal<br /> Without Tax</th>
                                    <th class="no-sort">10K<br /> Exemption</th>
                                    <th class="no-sort">Total Eligible <br />Withdrawal Without Tax</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=1; @endphp
                                @foreach($userLedgersResult as $key => $result)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $result->USERNAME }}</td>
                                    <td>{{ $result->ACTION }}</td>
                                    <td>{{ $result->TRANSACTION_AMOUNT }}</td>
                                    <td>{{ $result->INTERNAL_REFERENCE_NO }}</td>
                                    <td>{{ $result->PAYMENT_TRANSACTION_CREATED_ON }}</td>
                                    <td>{{ $result->TOTAL_DEPOSITS }}</td>
                                    <td>{{ $result->TOTAL_WITHDRAWALS }}</td>
                                    <td>{{ $result->TOTAL_TAXABLE_WITHDRAWALS }}</td>
                                    <td>{{ $result->ELIGIBLE_WITHDRAWAL_WITHOUT_TAX }}</td>
                                    <td>{{ $result->EXEMPTION_10K }}</td>
                                    <td>{{ $result->TOTAL_ELIGIBLE_WITHDRAWAL_WITHOUT_TAX }}</td>
                                </tr>
                                  @php $i++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                        <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" style="display:flex; justify-content: space-between;" class="mt-2">
                            @if(!empty($userLedgersResult))
                            <div>More Records</div>
                            <div>
                                {{ $userLedgersResult->render("pagination::customPaginationView") }}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- end card body-->
            </div>
            <!-- end card -->
        </div>
        <!-- end col-->
    </div>
    @endif
</div>
@endsection
