@extends('bo.layouts.master')

@section('title', "Welcome | PB BO")

@section('post_css')
<style>
.dashboardwraper{ text-align: center; padding: 10px ; position: relative; }.dashboardwraper img{ position:  relative;left: auto; top:0; transform: translateY(0); width: 130px; height: auto; margin:0 auto; }
@media (min-width:768px){.dashboardwraper img{ position: absolute;left: 0; top:50%; transform: translateY(-50%); width: 130px; height: auto ;margin:0 auto;}}
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 mt-2">
        <div class="card text-white bg-white text-xs-center">
            <div class="card-body">
                <div class="dashboardwraper">
                    <img src="{{ asset('assets/images/logo_lg.png') }}" alt="PokerBaazi Logo">

                    <blockquote class="card-bodyquote" style="text-align: center;">
                        <h3>Welcome <b><u><i>{{ ucwords(Auth::user()->FULLNAME) }}</i></u></b> to</h3>
                        <h1>PokerBaazi BO</h1>
                        <!-- <h6>BO Panel</h6> -->
                        <footer style="padding-top: 20px; color: #000;">One Place Stop to Manage all <cite>PokerBaazi
                                Activities</cite>
                        </footer>
                    </blockquote>
                </div>
            </div>
        </div> <!-- end card-box-->
    </div> <!-- end col -->
</div>
<!-- end row -->
@endsection

@section('js')
@endsection