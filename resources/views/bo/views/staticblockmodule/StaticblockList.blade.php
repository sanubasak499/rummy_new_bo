
@extends('bo.layouts.master')
@section('title', "Static Block Module List | PB BO")
@section('pre_css')
    <link href="{{ asset('assets/libs/custombox/custombox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('post_css')
<style>
.bannerimg{height:50px;}

.table td div{ white-space: normal;display: block;  display: -webkit-box;  max-width: 90%;  -webkit-line-clamp: 2;
-webkit-box-orient: vertical;  overflow: hidden;  text-overflow: ellipsis;}
</style>
@endsection
@section('content')

<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a >Website Management</a></li>
                     <li class="breadcrumb-item active">Static Block</li>
                  </ol>
               </div>
               <h4 class="page-title">Static Block</h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <div class="row mb-3">
                     <div class="col-sm-4">
                        <h5 class="font-18"> </h5>
                     </div>
                     <div class="col-sm-8">
                        <div class="text-sm-right">
                           <a href="{{ route('staticblockmodule.staticblock.index') }}" class="btn btn-success waves-effect waves-light" ><i class="fa fa-plus mr-1"></i> Add Static Block</a>
                        </div>
                     </div>
                     <!-- end col-->
                  </div>
                  <form id="userSearchForm" data-default-url="{{ route('staticblockmodule.staticblock.search_post') }}" action="{{ route('staticblockmodule.staticblock.search_post') }}{{ !empty($params) ? empty($params['page']) ? "" : "?page=".$params['page'] : '' }}" method="post">
                        @csrf
                     <div class="form-row">
                        <div class="form-group col-md-6">
                           <label>Block Id: </label>
                           <input type="text" class="form-control" name="BLOCK_ID" id="BLOCK_ID" class="form-control" placeholder="Enter Block Id" value="{{ !empty($params) ? $params['BLOCK_ID']  : ''  }}"  >
                        </div>
                        <div class="form-group col-md-6">
                           <label>Title: </label>
                           <input type="text" class="form-control" name="TITLE" id="TITLE" class="form-control" placeholder="Enter Title" value="{{ !empty($params) ? $params['TITLE']  : ''  }}"  >
                        </div>
                     </div>
                     <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
                     <button data-toggle="reload" data-form-id="#userSearchForm"  type="button" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a></button>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
            <!-- end card-->
         </div>
         <!-- end col -->
      </div>
      @if(!empty($StaticBlockResult))
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header bg-dark text-white">
                  <div class="card-widgets">
                     <a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>                                 
                  </div>
                  <h5 class="card-title mb-0 text-white">Static Block List</h5>
               </div>
               <div id="cardCollpase7" class="collapse show">
                  <div class="card-body">
                     
                     <table class="basic-datatable table dt-responsive nowrap"  data-order=''>
                        <thead>
                           <tr>
                              <th  width="20%">BLOCK ID</th>
                              <th  width="28%">TITLE</th>
                              <th  width="36%">DESCRIPTION</th>
                              <th  width="16%">Action</th>
                           </tr>
                        </thead>
                        <tbody>
                        @foreach ($StaticBlockResult as $key => $StaticBlockVal)
                           <tr >
                           <td ><div>{{$StaticBlockVal->BLOCK_ID}}</div></td>
                           <td ><div >{{$StaticBlockVal->TITLE}}</div></td>
                           <td ><div>{{$StaticBlockVal->DESCRIPTION}}</div></td>                              
                           <td >
                              <a  class="action-icon font-14 tooltips"  href="{{ route('staticblockmodule.staticblock.editStaticBlock', $StaticBlockVal->STATIC_BLOCK_ID) }}"  title="Edit"  >  <span class="tooltiptext">Edit</span>
                               <i class="fa fa-edit"></i>
                              </a>
                           </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     
                     <div class="customPaginationRender mt-2" data-form-id="#userSearchForm" style="display:flex; justify-content: space-between;" class="mt-2">
                        <div>More Records</div>
                        @if(!empty($StaticBlockResult))
                            <div>
                            {{ $StaticBlockResult->render("pagination::customPaginationView") }}
                            </div>
                        @endif
                        
                     </div>
                  </div>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
      </div>
      @endif
   </div>
</div>

@endsection

@section('js')
<style>
@media (max-width: 767.98px){
li.paginate_button.previous, li.paginate_button.next {
    display: inline-block;
     font-size:.875rem !important;
}}

</style>

@endsection