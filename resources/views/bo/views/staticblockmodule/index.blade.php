
@extends('bo.layouts.master')
@section('title', "Static Block | PB BO")
@section('pre_css')

    @endsection
    @section('post_css')
<style>
.select2-container .select2-selection--single{ height:auto;border: 1px solid #ced4da;}
.select2-container--default .select2-selection--single .select2-selection__rendered{    line-height: 36px;}
.select2-container--default .select2-selection--single .select2-selection__arrow b{margin-top: 2px;}
</style>


     
    @endsection

@section('content')
<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->
<div class="content">
<div >
        <!-- Start Content-->
        <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{route('staticblockmodule.staticblock.StaticblockList')}}">Website Management </a></li>
                            <li class="breadcrumb-item active">Add Static Block </li>
                        </ol>
                    </div>
                    <h4 class="page-title">Add Static Block </h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 
        <div class="row">   
            <div class="col-lg-12">
                <div class="card-box">
                    <!-- Error massge -->
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                <!-- end Error Massage -->
                <div class="row mb-3">
                            <div class="col-lg-8">
                                <h5 class="font-18"></h5>
                            </div>
                            <div class="col-lg-4">
                                <div class="text-lg-right mt-3 mt-lg-0">
                                    <a href="{{ route('staticblockmodule.staticblock.StaticblockList') }}" class="btn btn-dark waves-effect waves-light"><i class="fa fa-bars mr-1"></i>View Static Block</a>
                                </div>
                            </div>
                        </div>
                <form id="myFormId" enctype="multipart/form-data"    action="{{route('staticblockmodule.staticblock.insertStaticblock')}}" method="post" onSubmit="return StaticValidate()" >
                  @csrf
                  <div class="row">
                  <div class="form-group  col-md-6">
                                <label>Block Id : <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="BLOCK_ID" id="BLOCK_ID" class="form-control" placeholder="Enter Block Id" value="" maxlength="50" required="">
                            </div>
                  

                
                <div class="col-md-6">
                                <div class="row"></div>
                                <div class="form-group">
                                    <label>Title : <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="TITLE" id="TITLE" class="form-control"  placeholder="Enter Title" maxlength="255" required="">
                                </div>
                            </div>
                </div>

               <div class="row">
               <div class="col-md-12">
                                <div class="row"></div>
                                <div class="form-group">
                                    <label>Description : <span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="DESCRIPTION" class="form-control" id="DESCRIPTION" rows="8"  placeholder="Enter Description" required=""></textarea>
                                </div>
                            </div>

               </div>
                <button type="submit" name="submit"   id="submitbtn" class="btn btn-success waves-effect waves-light formActionButton mr-1"><i class="fa fa-save mr-1"></i> Save</button>
                <button style="display:none" class="btn btn-primary formActionButton mr-1" type="button" disabled="disabled">
                <span class="spinner-border spinner-border-sm mr-1" role="status"
                aria-hidden="true"></span>
                Loading...
                </button>
                <a href="{{ route('staticblockmodule.staticblock.index') }}" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Reset</a>
            </form>
        </div> <!-- end card-box -->  
    </div>
</div>
    <!-- end col -->
</div>
    <!-- end row -->
</div> <!-- container -->

@endsection
@section('post_js')
<script>
$(document).ready(function() {
        $("#myFormId").validate({
            rules: {
                BLOCK_ID: {
                    required: true,
                    remote: {
                        url: `${window.pageData.baseUrl}/staticblockmodule/staticblock/checkStaticBlockTitle`,
                        type: "post",
                        dataFilter: function(response) {
                            response = JSON.parse(response);
                            let result = false;
                            result = response.status == 201 ? true : `"Block Id already exist"`;
                            return result;
                        }
                    }
                }
            }
        });
    });

    function StaticValidate() {
        if ($("#myFormId").valid()) {
            $('.formActionButton').toggle();
            return true;
        }
    } 
</script>
@endsection
@section('js')
<script src="{{ asset('assets/js/pages/form-validation.init.js') }}"></script>
@endsection
