@extends('bo.layouts.master')
@section('title', "Edit Static block | PB BO")
@section('pre_css')
<!-- Plugins css -->

@endsection
@section('post_css')

@endsection

@section('content')

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="content">
    <div class="">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{route('staticblockmodule.staticblock.StaticblockList')}}">Website Management </a></li>
                                <li class="breadcrumb-item active">Edit Static Block</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Edit Static Block</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-box">
                        <!-- Error massge -->
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <ul>
                              @foreach ($errors->all() as $error)
                                 <li>{!! $error !!}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <!-- end Error Massage -->
                        <div class="row mb-3">
                            <div class="col-lg-8">
                                <h5 class="font-18"></h5>
                            </div>
                            <div class="col-lg-4">
                                <div class="text-lg-right mt-3 mt-lg-0">
                                    <a href="{{ route('staticblockmodule.staticblock.StaticblockList') }}" class="btn btn-dark waves-effect waves-light"><i class="fa fa-bars mr-1"></i>View Static Block</a>
                                </div>
                            </div>
                        </div>

                        <form id="myFormId" enctype="multipart/form-data"  action="{{route('staticblockmodule.staticblock.updateStaticBlock',['STATIC_BLOCK_ID'=>$StaticBlockResult->STATIC_BLOCK_ID])}}{{ !empty($params) ? empty($params['page']) ? "" : "?page=".$params['page'] : '' }}" method="post" onSubmit="return StaticValidate()">
                            @csrf

                            @if(isset($StaticBlockResult) && $StaticBlockResult!='' )

                            <div class="row">
                                <div class="form-group  col-md-6">
                                    <label>Block Id: <span class="text-danger">*</span></label>
                                    <input type="text" name="BLOCK_ID" id="BLOCK_ID" class="form-control" placeholder="Enter Block Id" value="{{$StaticBlockResult->BLOCK_ID}}" maxlength="50" required="">
                                </div>

                                <div class="col-md-6">
                                    <div class="row"></div>
                                    <div class="form-group">
                                        <label>Title : <span class="text-danger">*</span></label>
                                        <input type="text" name="TITLE" id="TITLE" class="form-control" placeholder="Enter Title" value="{{$StaticBlockResult->TITLE}}" maxlength="250" required="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row"></div>
                                    <div class="form-group">
                                        <label>Description : <span class="text-danger">*</span></label>
                                        <textarea name="DESCRIPTION" class="form-control" id="DESCRIPTION" rows="8" placeholder="Enter Description" required="">{{$StaticBlockResult->DESCRIPTION}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" name="submit" id="submitbtn" class="btn btn-success waves-effect waves-light formActionButton"><i class="fa fa-save mr-1"></i> Update</button>
                            <button style="display:none" class="btn btn-primary formActionButton mr-1" type="button" disabled>
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                Loading...
                            </button>
                            <a href="{{ route('staticblockmodule.staticblock.editStaticBlock',$StaticBlockResult->STATIC_BLOCK_ID) }}" class="btn btn-secondary waves-effect waves-light"><i class="mdi mdi-replay mr-1"></i> Reset</a>
                            @endif
                        </form>
                    </div> <!-- end card-box -->
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div> <!-- container -->

    @endsection
    @section('post_js')

    @endsection
    @section('js')

    <script>
        $(document).ready(function() {
            $("#myFormId").validate({

            });
        });
    </script>
    <script src="{{ asset('assets/js/pages/form-validation.init.js') }}"></script>
    @endsection