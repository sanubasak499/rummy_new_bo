
@extends('bo.layouts.master')

@section('title', "Baazi Rewards | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />

 <!-- Sweet Alert-->
 <link href="{{ url('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{url('assets/libs/jquery-toast/jquery-toast.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<style>
@media(min-width: 576px){
	.modal-dialog {
    max-width: 998px;
}
}

</style>
<div class="row">
      <div class="col-12">
         <div class="page-title-box">
            <div class="page-title-right">
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item">Baazi Rewards	</li>
                  <li class="breadcrumb-item active">Rewards Levels</li>
               </ol>
            </div>
            <h4 class="page-title">Rewards Levels</h4>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-12">
         <div class="card">
            <div class="card-body">
				<div class="row mb-3">
					<div class="col-sm-4">
					<h5 class="font-18">View Rewards Levels </h5>
					</div>
					<div class="col-sm-8">
					<div class="text-sm-right">
						<a href="{{ route('rewards.rewardConfig.addreward')}}" class="btn btn-success waves-effect waves-light"><i class="fa fa-plus mr-1"></i> Add New Reward</a>
					</div>
					</div>
					<!-- end col-->
				</div>
               <form class="" novalidate="" method="POST" id="playerLoginSearch" action="{{ route('rewards.rewardConfig.filter_rewards') }}">
                    {{ csrf_field() }}
                  <div class="form-row">
				  <div class="form-group col-md-3">
						<label for="inputPassword4" >Level Name:</label>
				  		<input name="prize_name" type="text" class="form-control" placeholder="Reward Prize Name" value="{{ !empty($params) ? $params['prize_name'] : '' }}">
                    </div>
					<div class="form-group col-md-3">
						<label for="inputPassword4" >Required Reward Points:</label>
				  		<input name="required_points" type="text" class="form-control" placeholder="Required Reward Points" value="{{ !empty($params) ? $params['required_points'] : '' }}">
                    </div>
					<div class="form-group col-md-3">
                        <label for="validationCustom01">Status:</label>
                        <select  class="customselect" data-plugin="customselect" id="reward_status" name="reward_status">
                           <option value="" {{ !empty($params) ? $params['reward_status'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                           <option value="1" {{ !empty($params) ? $params['reward_status'] == '1' ? "selected" : ''  : ''  }}>Active</option>
                           <option value="2" {{ !empty($params) ? $params['reward_status'] == '2' ? "selected" : ''  : ''  }}>Inactive</option>
                           
                        </select>
                     </div>
					 <div class="form-group col-md-3 multiplecustom" >
                           <label>Prize Type:</label>
                           <select name="prize_type[]" class="form-control selectpicker" multiple data-style="btn-light">
						   
                           <option value="REAL_PRIZE" {{ !empty($params['prize_type']) ? in_array('REAL_PRIZE',$params['prize_type']) ? "selected" : ''  : ''  }}>Real Prize</option>
                           <option value="PROMO_PRIZE" {{ !empty($params['prize_type']) ? in_array('PROMO_PRIZE',$params['prize_type']) ? "selected" : ''  : ''  }} >Promo Prize</option>
                           <option value="CUSTOM_PRIZE_NAME" {{ !empty($params['prize_type']) ? in_array('CUSTOM_PRIZE_NAME',$params['prize_type']) ? "selected" : ''  : ''  }}>Custom Prize</option>
                           </select>
                        </div>
                     
                  </div>
                  <div class="form-row customDatePickerWrapper">
					<div class="form-group col-md-3">
                        <label for="validationCustom01">Search By Date:</label>
                        <select  class="customselect" data-plugin="customselect" id="searchByDate" name="searchByDate">
                           <option value="start" {{ !empty($params) ? $params['searchByDate'] == 'start' ? "selected" : ''  : ''  }}>Start Date</option>
                           <option value="end" {{ !empty($params) ? $params['searchByDate'] == 'end' ? "selected" : ''  : ''  }}>End Date</option>
                        </select>
                     </div>
                     <div class="form-group col-md-3">
                        <label>From:</label>
                        <div class="input-group">
                           <input name="date_from" type="text" class="form-control customDatePicker from" placeholder="Select From Date" value="{{ !empty($params) ? $params['date_from'] : '' }}">
                        </div>
                     </div>
                     <div class="form-group col-md-3">
                        <label>To:</label>
                        <div class="input-group">
                        <input name="date_to" type="text" class="form-control customDatePicker to" placeholder="Select To Date" value="{{ !empty($params) ? $params['date_to'] : '' }}">
                        </div>
                     </div>
					 <div class="form-group col-md-3">
                        <label for="inputPassword4">Date Range: </label>
                        <select class="customselect formatedDateRange" data-plugin="customselect" name="date_range" id="date_range">
                           <option value="" {{ !empty($params) ? $params['date_range'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                           <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                           <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                           <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                           <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                           <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                           <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                        </select>
                     </div>
                    
                  </div>
                  <div class="button-list">
					<button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
					<a href="{{ route('rewards.rewardConfig.index') }}" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i>Reset</a>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>

@if(!empty($rewards_level))
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header bg-dark text-white">
		<div class="card-widgets">
				<a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>                                 
		</div>
            <h5 class="card-title mb-0 text-white">Reward Levels</h5>
        </div>

		<div id="cardCollpase7" class="collapse show">
			<div class="card-body">            
				
		
						<table class="basic-datatable table dt-responsive nowrap">
						<thead>
							<tr>
							@if(empty($params['prize_name']) && empty($params['required_points'])  && empty($params['prize_type']) && $params['reward_status'] == 1)
								 <th>Level</th>
							@endif
                             
                              <th>Req.  Points</th>
                              <th>Real Prize</th>
                              <th>Promo Prize</th>
                              <th>Custom Prize</th>
                              <th>Tournaments</th>
                              <th>Level Name</th>
                              <th>Level Description</th>
                              <th>Prize Image</th>
                              <th>Starts On </th>
                              <th>Ends On</th>
                              <th>Status</th>
                              <th>Action</th>
                           </tr>
						</thead>
						<tbody>    
						@foreach($rewards_level as $key => $reward)               
						<tr>
						@if(empty($params['prize_name']) && empty($params['required_points'])  && empty($params['prize_type']) && $params['reward_status'] == 1)
								 <td>{{$loop->iteration}}</td>   
							@endif							
							<td>{{$reward->REWARD_POINTS_REQUIRED}}</td>           
							<td>{{ empty($reward->REAL_PRIZE) ? 'N/A' : $reward->REAL_PRIZE }}</td>  
							<td>{{ empty($reward->PROMO_PRIZE) ? 'N/A' : $reward->PROMO_PRIZE }}</td>  
							<td>{{ empty($reward->CUSTOM_PRIZE_NAME) ? 'N/A' : $reward->CUSTOM_PRIZE_NAME }}</td>  
							<td>{{ empty($reward->TOURNAMENT_NAME) ? 'N/A' : $reward->TOURNAMENT_NAME }}</td>  
							<td>{{ empty($reward->CUSTOM_PRIZE) ? 'N/A' : $reward->CUSTOM_PRIZE }}</td>           
							<td>{{ $reward->CUSTOM_PRIZE_DESC }}</td>     
							<td>
							<div class="gal-box">
								
								<a href="{{config('poker_config.imageUrl.CDN_URL').config('poker_config.imageUrl.REWARD_IMAGE_FOLDER').$reward->CUSTOM_PRIZE_IMAGE_LINK}}" class="image-popup" title="{{ $reward->LEVEL_ID }} | {{ $reward->REWARD_POINTS_REQUIRED }} | {{ $reward->CUSTOM_PRIZE }}">
                                        <img src="{{config('poker_config.imageUrl.CDN_URL').config('poker_config.imageUrl.REWARD_IMAGE_FOLDER').$reward->CUSTOM_PRIZE_IMAGE_LINK}}" class="img-fluid" alt="work-thumbnail">
                                    </a>
                            </div> 
							
							</td>
							 
							<td>{{ $reward->START_DATE }}</td>     
							<td>{{ $reward->END_DATE }}</td>     
							  
							<td>
							@if($reward->STATUS == "1")
							<label class="switches">
								<input type="checkbox" checked data-id="{{ $reward->LEVEL_ID }}" class='status_switch'  data-color="#64b0f2" onclick="statusDisable({{$reward->LEVEL_ID}},0)"data-size="small"/>
								<span class="slider round"></span>
							</label>
							@else
							<label class="switches">
								<input type="checkbox" class='status_switch' data-id="{{ $reward->LEVEL_ID }}" data-color="#64b0f2" onclick="statusDisable({{$reward->LEVEL_ID}},1)"data-size="small"/>
								<span class="slider round"></span>
							</label>								
							@endif
							</td>		            
							<td>
								<a href="{{ url('/rewards/rewardConfig/updateRewardLevel/' . $reward->LEVEL_ID)}}" class="action-icon font-14 tooltips" > <i class="fa fa-edit"></i><div class="tooltiptext">Edit</div></a>
								<div onclick="status({{$reward->LEVEL_ID}},2)" class="action-icon font-14 tooltips"> <i class="fa fa-trash"></i><div class="tooltiptext">Trash</div></div>
								
							</td>      
						</tr> 
						@endforeach
					</tbody>
				</table>
			</div>
			</div>
		</div>     
	</div>
</div>
@endif
@endsection
@section('js')
<script src="{{ asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/gallery.init.js') }}"></script>
<script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/sweet-alerts.init.js') }}"></script>
<script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script> 
<!-- Plugins css -->
<!-- Init js-->
<script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>
  
<!-- Sweet Alerts js -->

 <script src="{{ asset('assets/libs/jquery-toast/jquery-toast.min.js') }}"></script>

<script>
$('[data-plugin="switchery"]').each(function(a,e){new Switchery($(this)[0],$(this).data())}); 

	var switchStatus = false;
		$(document).on('change',".status_switch", function() {
			if($(this).is(':checked')){
				var status = 1; 
			}else{
				var status = 0; 
			}
			$.ajax({
			   "url": "{{url('/rewards/rewardConfig/rewardlevelstatus')}}",
			   "method":"POST",
			   "data":{"id":$(this).data('id'),"status":status},
			   "dataType":"json",
			   success:function(data){
				  window.location.reload();
				   
			   }
			});
		});

function statusDisable(id,status)
{
	if (result.value) {
			$.ajax({
				"url": "{{url('/rewards/rewardConfig/rewardlevelstatus')}}",
				"method":"POST",
				"data":{"id":id,"status":status},
				"dataType":"json",
				success:function(data){
					window.location.reload();			
				}
			});
          }
}
function status(id,status)
{

	Swal.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		type:"warning",
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, delete it!'
	}).then((result) => {
          if (result.value) {
			$.ajax({
				"url": "{{url('/rewards/rewardConfig/rewardlevelstatus')}}",
				"method":"POST",
				"data":{"id":id,"status":status},
				"dataType":"json",
				success:function(data){
					window.location.reload();			
				}
			});
          }
        })
	
}

$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});
@if(session()->has('message'))
               $.toast({
					heading: 'Well Done !!',
					text: '{!! session()->get("message") !!}',
					showHideTransition: 'slide',
					icon: 'success',
					loader: true,
					position: 'top-right',
					hideAfter: 6000
				});
				{!! session()->forget('message'); !!}
				
            @endif
            @if(session()->has('errormessage'))
                $.toast({
					heading: 'Error',
					text: '{!! session()->get("errormessage") !!}',
					showHideTransition: 'fade',
					icon: 'error',
					position: 'top-right',
					hideAfter: 6000
				});
				
				{!! session()->forget('errormessage'); !!}
            @endif
</script>



@endsection