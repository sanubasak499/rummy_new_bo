
@extends('bo.layouts.master')

@section('title', "Baazi Rewards | PB BO")

@section('pre_css')
<link href="{{ asset('assets/libs/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/libs/nestable2/nestable2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
 <!-- Sweet Alert-->
<link href="{{url('assets/libs/jquery-toast/jquery-toast.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<style>{
@media (min-width: 576px)
.modal-dialog {
    max-width: 998px;
}
}

</style>
<div class="row">
      <div class="col-12">
         <div class="page-title-box">
            <div class="page-title-right">
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item">Baazi Rewards</li>
                  <li class="breadcrumb-item active">Claim Summary</li>
               </ol>
            </div>
            <h4 class="page-title">Reward Claimed Summary</h4>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-12">
         <div class="card">
            <div class="card-body">
				<div class="row mb-3">
					<div class="col-sm-4">
					<h5 class="font-18">View Claimed Levels </h5>
					</div>
					<div class="col-sm-8">
					<div class="text-sm-right">
						
					</div>
					</div>
					<!-- end col-->
				</div>
                @if(empty($params['page']))
                    @php $page = " " @endphp
                @else
                   @php $page = $params['page'] @endphp
                @endif
               <form novalidate="" method="POST" id="playerLoginSearch" action="{{ route('rewards.rewardHistory.searchclaimed') }}{{ !empty($params) ? empty($params['page']) ? '' : "?page=".$params['page'] : '' }}">
                    {{ csrf_field() }}
                  <div class="form-row">
					<div class="form-group col-md-3">
						<label for="inputPassword4" >Username:</label>
				  		<input name="username" type="text" class="form-control" placeholder="Username" value="{{ !empty($params) ? $params['username'] : '' }}">
                    </div>
					<div class="form-group col-md-3">
						<label for="inputPassword4" >Reference No:</label>
				  		<input name="INTERNAL_REFERENCE_NO" type="text" class="form-control" placeholder="Reference No." value="{{ !empty($params) ? $params['INTERNAL_REFERENCE_NO'] : '' }}">
                    </div>
                     <div class="form-group col-md-3">
					 <label for="inputPassword4" >Reward Status:</label>
                        <select  class="customselect" data-plugin="customselect" name="REWARD_STATUS" id="device_type" >
                           <option value="" {{ !empty($params) ? $params['REWARD_STATUS'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                           <option value="1" {{ !empty($params) ? $params['REWARD_STATUS'] == 1 ? "selected" : ''  : ''  }}>LRP</option>
                           <option value="2" {{ !empty($params) ? $params['REWARD_STATUS'] == 2 ? "selected" : ''  : ''  }}>CB</option>
                           <option value="3" {{ !empty($params) ? $params['REWARD_STATUS'] == 3 ? "selected" : ''  : ''  }}>Switched</option>
                        </select>
                     </div>
                     <div class="form-group col-md-3">
					 <label for="inputPassword4" >Tracking Status:</label>
                        <select  class="customselect" data-plugin="customselect" name="TRACKING_STATUS" id="TRACKING_STATUS" >
                           <option value="" {{ !empty($params) ? $params['TRACKING_STATUS'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                           <option value="1" {{ !empty($params) ? $params['TRACKING_STATUS'] == 1 ? "selected" : ''  : ''  }}>Recieved</option>
                           <option value="2" {{ !empty($params) ? $params['TRACKING_STATUS'] == 2 ? "selected" : ''  : ''  }}>Processing</option>
                           <option value="3" {{ !empty($params) ? $params['TRACKING_STATUS'] == 3 ? "selected" : ''  : ''  }}>Dispatched</option>
                           <option value="4" {{ !empty($params) ? $params['TRACKING_STATUS'] == 4 ? "selected" : ''  : ''  }}>Delivered</option>
                        </select>
                     </div>
                     
                  </div>
                  <div class="form-row customDatePickerWrapper">
					<div class="form-group col-md-3">
                        <label for="validationCustom01">Custom Price:</label>
                        <select  class="customselect" data-plugin="customselect" id="searchByDate" name="CUSTOM_PRICE_NAME">
                           <option value="" {{ !empty($params) ? $params['CUSTOM_PRICE_NAME'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                           <option value="yes" {{ !empty($params) ? $params['CUSTOM_PRICE_NAME'] == 'yes' ? "selected" : ''  : ''  }}>Yes</option>
                           <option value="no" {{ !empty($params) ? $params['CUSTOM_PRICE_NAME'] == 'no' ? "selected" : ''  : ''  }}>No</option>
                        </select>
                     </div>
                     <div class="form-group col-md-3">
                        <label>From:</label>
                        <div class="input-group">
                           <input name="date_from" type="text" class="form-control customDatePicker from" placeholder="Select From Date" value="">
                        </div>
                     </div>
                     <div class="form-group col-md-3">
                        <label>To:</label>
                        <div class="input-group">
                        <input name="date_to" type="text" class="form-control customDatePicker to" placeholder="Select To Date" value="">
                        </div>
                     </div>
                     <div class="form-group col-md-3">
                        <label for="inputPassword4">Date Range: </label>
                        <select class="customselect formatedDateRange" data-plugin="customselect" name="date_range" id="date_range">
                           <option value="" {{ !empty($params) ? $params['date_range'] == "" ? "selected" : ''  : 'selected'  }}>Select</option>
                           <option value="1" {{ !empty($params) ? $params['date_range'] == 1 ? "selected" : ''  : ''  }}>Today</option>
                           <option value="2" {{ !empty($params) ? $params['date_range'] == 2 ? "selected" : ''  : ''  }}>Yesterday</option>
                           <option value="3" {{ !empty($params) ? $params['date_range'] == 3 ? "selected" : ''  : ''  }}>This Week</option>
                           <option value="4" {{ !empty($params) ? $params['date_range'] == 4 ? "selected" : ''  : ''  }}>Last Week</option>
                           <option value="5" {{ !empty($params) ? $params['date_range'] == 5 ? "selected" : ''  : ''  }}>This Month</option>
                           <option value="6" {{ !empty($params) ? $params['date_range'] == 6 ? "selected" : ''  : ''  }}>Last Month</option>
                        </select>
                     </div>
                  </div>
                  <div class="button-list">
					<button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fas fa-search mr-1"></i> Search</button>
					<a href="{{ route('rewards.rewardHistory.claimedform') }}" class="btn btn-secondary waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i> Clear</a>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>

@if(!empty($claimed_transactions))
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header bg-dark text-white">
		<div class="card-widgets">
				<a data-toggle="collapse" href="#cardCollpase7" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class=" fas fa-angle-down"></i></a>                                 
		</div>
            <h5 class="card-title mb-0 text-white">Reward Level Transactions</h5>
        </div>

		<div id="cardCollpase7" class="collapse show">
				<div class="card-body">            
					<div class="row mb-2 mt-3">
						<div class="col-sm-7"><h5 class="font-18 "></h5></div>
							<div class="col-sm-5">
							<div class="text-sm-right">   
								@if(count($claimed_transactions) > 0)
								<a href="{{ route('rewards.rewardHistory.export')."?username=".$params['username']."&INTERNAL_REFERENCE_NO=".$params['INTERNAL_REFERENCE_NO']."&REWARD_STATUS=".$params['REWARD_STATUS']."&CUSTOM_PRICE_NAME=".$params['CUSTOM_PRICE_NAME']."&date_from=".$params['date_from']."&date_to=".$params['date_to']."&date_range=".$params['date_range']."&type=".'rewardTransactions'."&page=".$page }}"  class="btn btn-light mb-1"><i class="fas fa-file-excel mr-1" style="color: #34a853;"></i>Excel Export</a>
								
								@endif
							</div>
						</div>
					</div>
			
							<table class="basic-datatable table dt-responsive nowrap">
							<thead>
								<tr>
									<th>USERNAME</th>
									<th>POINTS CONVERTED</th>
									<th>CASHBACK</th>
									<th>REAL PRIZE</th>
									<th>PROMO PRIZE</th>
									<th>TOURNAMENT NAME</th>
									<th>CUSTOM PRIZE</th>
									<th>CURRENT STATUS</th>
									<th>ACTION</th>
									<th>ACTION DONE</th>
									<th>REFERENCE NO</th>
									<th>LEVEL ID</th>									
									<th>DATE</th>
								</tr>
							</thead>
							<tbody>    
							@foreach($claimed_transactions as $key => $transactions)               
							<tr>
								<td>{{$transactions->USERNAME}}</td>           
								<td>{{$transactions->RP_CONVERTED}}</td>           
								<td>
                                @if(!empty($transactions->REWARD_STATUS == 2))
									{{$transactions->CASHBACK_AMOUNT}}
								@else
                                    {{ "N/A" }}
                                @endif
                                </td>           
								<td>{{$transactions->REAL_PRICE}}</td>           
								<td>{{$transactions->PROMO_PRICE}}</td>           
								<td>
								@if(!empty($transactions->TOURNAMENT_NAME))
									{{$transactions->TOURNAMENT_NAME}}
								@else
									{{ "N/A" }}
								@endif
								</td>           
								<td>
                                @if($transactions->CUSTOM_PRICE_NAME)
                                    {{$transactions->CUSTOM_PRICE_NAME}}
                                @else
                                    {{ "N/A"}}
                                @endif
                                </td> 
								<td>	
								<div class="statusBadge" data-ref-no="{{ $transactions->INTERNAL_REFERENCE_NO }}">							
									@switch($transactions->REWARD_TRACKING_STATUS_ID)
										@case(1)
											<span class="badge bg-warning text-white shadow-none">Order Recieved</span>
											@break
										@case(2)
											<span class="badge bg-info text-white shadow-none">Processing Order</span>
											@break
										@case(3)
											<span class="badge bg-danger text-white shadow-none">Order Dispatched</span>
											@break 
										@case(4)
											<span class="badge bg-success text-white shadow-none">Order Delivered</span>
											@break 
										@default
											<span class="badge bg-dark shadow-none">Status Unknown</span>
									@endswitch
								</div>
								</td> 
                                @if(!empty($transactions->REWARD_TRACKING_STATUS_ID) && $transactions->REWARD_TRACKING_STATUS_ID != 4)          
								<td><a href="#"  data-toggle="modal"  title="Update Tracking Status" data-target="#trackingStatusPopup" data-ref-no="{{$transactions->INTERNAL_REFERENCE_NO}}" data-plugin="tippy" data-tippy-interactive="true" class="font-19 mr-1 action"><b><i class="mdi-refresh mdi"></i></b></a></td>
                                @else
								<td>-</td>
                                @endif
								<td>								
								@switch($transactions->REWARD_STATUS)
									@case(1)
										LRP
										@break
									@case(2)
										CB
										@break
									@case(3)
										SWITCHED
										@break 
									@default
										Unknown
								@endswitch
								</div>
								</td>  
								<td>{{$transactions->INTERNAL_REFERENCE_NO}}</td>           
								<td>{{$transactions->LEVEL_ID}}</td>
								<td>{{$transactions->CREATED_DATE}}</td>
										
							</tr> 
							@endforeach
						</tbody>
					</table>
                    <div class="customPaginationRender mt-2" data-form-id="#playerLoginSearch" style="display:flex; justify-content: space-between;">
                         @if(!empty($claimed_transactions))
                        <div>More Records</div>
                        <div>
                        {{ $claimed_transactions->render("pagination::customPaginationView") }}
                        </div>
                        @endif
                    </div>
				</div>
			</div>
		</div>     
	</div>
</div>
@endif
<div class="modal fade" id="trackingStatusPopup">
	<div class="modal-dialog">
		<div class="modal-content  shadow-none position-relative">
            <div  id="getWayloader" style="display:none"> 
                <div class="d-flex justify-content-center hv_center">
                        <div class="spinner-border" role="status"></div>
                </div>
            </div>
            <div class="card  mb-0">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <a href="#" data-dismiss="modal">
                        <i class="mdi mdi-close"></i>
                        </a>
                    </div>
                    <h5 class="card-title mb-0 text-white font-18" ><span class="text-success" id="getWayPoupType"></span> Update  Status</h5>
                </div>
                <div class="card-body " style="position:relative">
                    <div id="blurr-effect" class="blureffect" style="display:none;z-index:9; position:absolute; left:0; top:0; bottom:0; right:0;">
					<input type="hidden" class="refNoGetway">	
				</div>
                    <div id="tracking-status-div">                         
                        <div class="form-group ">
                            <label>Current Status: <span class="trackingStatus" id="trackingStatus"></span></label>
                        </div>
                    </div>                            
                    <div id="statusradio" class="form-group  multiplecustom">
						<label for="inputPassword4" >Reward Status:</label>
                        <select  class="customselect" data-plugin="customselect" name="TRACK_STATUS" id="statusDrop" >
                           <option value="">Select</option>
                           <option value="1">Order Recieved</option>
                           <option value="2">Processing Order</option>
                           <option value="3">Order Dispatched</option>
                           <option value="4">Order Delivered</option>
                        </select>
                    </div>
                    <div class="form-group " id="pwdDiv">
                        <label>Transaction Password:</label>
                        <input type="password" class="form-control" name="transPwd" id="transPwdGetway" placeholder="Transaction Password" value="">
                    </div>
                    <button type="button" id ="updateTrackingStatus" class="btn btn-warning waves-effect waves-light"><i class="far fa-edit mr-1"></i> Update</button>
                    <div style="display:none ;margin-top:15px" class="" id="getWayMessage" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script src="{{ asset('assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/gallery.init.js') }}"></script>
<script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>

<script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script> 
<script src="{{ asset('assets/libs/switchery/switchery.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>

<script src="{{ asset('assets/libs/jquery-toast/jquery-toast.min.js') }}"></script>
@endsection
@section('post_js')
<script>
    $(document).ready(function() {
		$('#trackingStatusPopup').on('show.bs.modal', function(e) {
			$("#tracking-status-div").hide();
			$("#getWayloader").show();
			$("#getWayMessage").hide();
			$relatedButton = $(e.relatedTarget);
            $model = $(this);
			$model.find('.refNoGetway').val($relatedButton.data('refNo'));
			
			var ref_no = $('.refNoGetway').val();
			$.ajax({
                url: `${window.pageData.baseUrl}/rewards/rewardHistory/getTrackingStatus`,
                method: "POST",
                data: {
                    ref_no: ref_no,
                },
                success: function(response) {
		
                    if (response.trackingStatus == 1) {
						$("#trackingStatus").addClass("badge bg-warning text-white shadow-none");
						$("#tracking-status-div").show();
						$("#trackingStatus").html("Order Recieved");
                    } else if(response.trackingStatus == 2) {
						$("#trackingStatus").addClass("badge bg-info text-white shadow-none");
						$("#tracking-status-div").show();
						$("#trackingStatus").html("Processing Order");
					} else if(response.trackingStatus == 3) {
						$("#trackingStatus").addClass("badge bg-danger text-white shadow-none");
						$("#tracking-status-div").show();
						$("#trackingStatus").html("Order Dispatched");
					} else if(response.trackingStatus == 4) {
						$("#trackingStatus").addClass("badge bg-success text-white shadow-none");
						$("#tracking-status-div").show();
						$("#trackingStatus").html("Order Delivered");
					} else{
						$("#trackingStatus").addClass("badge bg-dark text-white shadow-none");
						$("#tracking-status-div").show();
						$("#trackingStatus").html("Unknown/Status");
					}
					$("#getWayloader").hide();
                }
            });
		});

		// update getway status on databse 
        $('#updateTrackingStatus').click(function(e) {
            e.preventDefault();
            $("#getWayloader").show();
            $("#blurr-effect").show();

            var ref_no = $('.refNoGetway').val();
            var trans_pwd = $('#transPwdGetway').val();            
            var new_status = $('#statusDrop').val();

            $.ajax({
                url: `${window.pageData.baseUrl}/rewards/rewardHistory/updateTrackingStatus`,
                method: "POST",
                data: {
                    ref_no: ref_no,
					newStatus: new_status,
					trans_pwd: trans_pwd,
                },
                success: function(response) {
                    document.getElementById('getWayMessage').className = '';
                    
                    var status = response.status;
                    $("#getWayMessage").show();
                    if (status == 200) {
                        $('table').find(`.statusBadge[data-ref-no="${ref_no}"]`).html('<span class="badge bg-info text-white shadow-none">Processing Order</span>');
                        $("#getWayMessage").addClass("alert alert-success alert-dismissible bg-success text-white border-0 fade show");
                       
					} else if (status == 201) {
                        $('table').find(`.statusBadge[data-ref-no="${ref_no}"]`).html('<span class="badge bg-danger text-white shadow-none">Order Dispatched</span>');
                        $("#getWayMessage").addClass("alert alert-success alert-dismissible bg-success text-white border-0 fade show");
                        
                    }else if (status == 202) {
                        $('table').find(`.statusBadge[data-ref-no="${ref_no}"]`).html('<span class="badge bg-success text-white shadow-none">Order Delivered</span>');
                        $('table').find(`.action[data-ref-no="${ref_no}"]`).html('-');
                        $("#getWayMessage").addClass("alert alert-success alert-dismissible bg-success text-white border-0 fade show");
                        
                    }else if (status == 203) {
                        $("#getWayMessage").addClass("alert alert-success alert-dismissible bg-danger text-white border-0 fade show");
                        
                    } else if (status == 199) {
                        $("#getWayMessage").addClass("alert alert-warning alert-dismissible bg-warning text-white border-0 fade show");
                    } else {
                        $("#getWayMessage").addClass("alert alert-danger alert-dismissible bg-danger text-white border-0 fade show");
                    }
                    $("#getWayMessage").html(response.message);
                    $("#getWayloader").hide();
                    $("#blurr-effect").hide();
                },
                error: function(error) {
                    $("#getWayloader").hide();
                    $("#blurr-effect").hide();
                    $("#getWayMessage").show();
                    $("#getWayMessage").html("Something Went Wrong");
                    $("#getWayMessage").addClass("alert alert-danger alert-dismissible bg-danger text-white border-0 fade show");

                }
            });
        });

	});


</script>
@endsection
