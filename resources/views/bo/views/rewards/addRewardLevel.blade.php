@extends('bo.layouts.master')

@section('title', "Baazi Rewards | PB BO")

@section('pre_css')
	<!--File Upload css -->
<link href="{{ asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/dropify/dropify.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
<style type="text/css">
.dropify-wrapper{ overflow: visible !important;  }
.dropify-wrapper label.error{position: absolute;left: 0;top: 101%;}

.formtext{ font-size: 11px; font-style: italic; color: #999198; padding: 6px 0 0 0; margin: 0; line-height: 13px; }
</style>
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a >Baazi Rewards</a></li>
                     <li class="breadcrumb-item"><a>Rewards Levels</a></li>
                     <li class="breadcrumb-item active">Add</li>
                  </ol>
               </div>
               <h4 class="page-title">Add Reward Level</h4>
               {{Session::get('errorvalidation')}}
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
			   <div class="row mb-3">
					<div class="col-sm-4">
						<h5 class="font-18">Reward Level </h5>
					</div>
					<div class="col-sm-8">
						<div class="text-sm-right">
							<a href="{{ route('rewards.rewardConfig.index') }}" class="btn btn-dark waves-effect waves-light"><i class="fa fa-bars mr-1"></i>View List</a>
						</div>
					</div>
				<!-- end col-->
			  </div>
               <form  name="frmCreateData" id="frmCreateReward" enctype='multipart/form-data' autocomplete="off" method="post" action="{{ route('rewards.rewardConfig.addrewardlevel') }}" onSubmit="return ImageValidate()">
				@csrf
				<input type="hidden" value="" name="LEVEL_ID" >
                     <div class="row">
                        <div class="col-md-9">
                           <div class="row">
                              <div class="form-group col-md-4">
                                 <label>Required Reward Points<span class="text-danger">* </span>:</label>
                                 <input type="tel" name="REWARD_POINTS_REQUIRED"  value="{{ old('REWARD_POINTS_REQUIRED') }}" id="REWARD_POINTS_REQUIRED" class="form-control  @error('REWARD_POINTS_REQUIRED') parsley-error @enderror">
								 @error('REWARD_POINTS_REQUIRED')
									<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
								@enderror
                              </div>
                              <div class="form-group col-md-4">
                                 <label>Level Name<span class="text-danger">* </span> :</label>
                                 <input name="CUSTOM_PRIZE" type="text" class="form-control maxlengtherror @error('CUSTOM_PRIZE') parsley-error @enderror" maxlength="65" id="CUSTOM_PRIZE"  tabindex="12" value="{{ old('CUSTOM_PRIZE') }}">
								 @error('CUSTOM_PRIZE')
									<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
								@enderror
							  </div>
                              <div class="form-group col-md-4">
                                 <label >Level Description<span class="text-danger">* </span> :</label>
                                 <input  name="CUSTOM_PRIZE_DESC" type="text" class="form-control maxlengtherror  @error('CUSTOM_PRIZE_DESC') parsley-error @enderror" maxlength="130" id="CUSTOM_PRIZE_DESC"  tabindex="12" value="{{ old('CUSTOM_PRIZE_DESC') }}">
								 @error('CUSTOM_PRIZE_DESC')
									<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
								 @enderror
							  </div>
                           </div>
                           <div class="row">
                              <div class="form-group col-md-3">
                                 <label>Real Prize:  </label>
                                 <input  name="REAL_PRIZE" type="tel"  class="form-control fillone @error('REAL_PRIZE') parsley-error @enderror prize_group" id="REAL_PRIZE" tabindex="12" value="{{ old('REAL_PRIZE') }}">
                                 @error('REAL_PRIZE')
									<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
								 @enderror
								 <p class="formtext clearfix"><i>Insert any value except Blank or 0 for prize consideration</i></p>
                              </div>
                              <div class="form-group col-md-3">
                                 <label>Promo Prize:</label>
                                 <input  type="tel"  name="PROMO_PRIZE" type="text" class="form-control fillone @error('PROMO_PRIZE') parsley-error @enderror prize_group" id="PROMO_PRIZE" tabindex="12" value="{{ old('PROMO_PRIZE') }}">
								 @error('PROMO_PRIZE')
									<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
								 @enderror
								  <p class="formtext clearfix"><i>Insert any value except Blank or 0 for prize consideration</i></p>
                              </div>
                              <div class="form-group col-md-3">
                                 <label >Custom Prize:</label>
                                 <input  type="text" class="form-control fillone  @error('CUSTOM_PRIZE_NAME') parsley-error @enderror prize_group" name="CUSTOM_PRIZE_NAME" id="CUSTOM_PRIZE_NAME">
								 @error('CUSTOM_PRIZE_NAME')
									<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
								 @enderror
                                  <p class="formtext clearfix"><i>Insert prize name for prize consideration</i></p>
                              </div>
							  <div class="form-group col-md-3">
                                <label>Tournament: </label>
                                    <select name="TOURNAMENT_NAME" class="customselect fillone @error('TOURNAMENT_NAME') parsley-error @enderror prize_group" data-plugin="customselect" id="TOURNAMENT_NAME" tabindex="3">
                                        <option hidden value="">Select</option>
                                        @foreach($tournamentNames as $key => $tournamentNames)	
                                        <option value="{{$tournamentNames->TOURNAMENT_NAME}}" > {{ $tournamentNames->TOURNAMENT_NAME }}</option>
                                        @endforeach
                                    </select>
									@error('TOURNAMENT_NAME')
									<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
								 @enderror
								 <p class="formtext clearfix"><i>Select Tournament ticket for prize consideration</i></p>
                              </div>
                           </div>
                           <div class="row  customDatePickerWrapper">
                              <div class="form-group col-md-6">
                                 <label>From: </label>
                                 <input name="date_from" type="text" class="form-control customDatePicker from" placeholder="Selct From Date" value="{{ \Carbon\Carbon::today()->setTime("00","00","00")->format('d-M-Y H:i:s') }}" required>
                              </div>
                              <div class="form-group col-md-6">
                                 <label>To: </label>
                                 <input name="date_to" type="text" class="form-control customDatePicker to" placeholder="Selct From Date" value="{{ \Carbon\Carbon::today()->setTime("23","59","59")->format('d-M-Y H:i:s') }}" required>
                              </div>
							  
                           </div>
                        </div>
                        <div class="col-md-3">
							<label>Select Image<span class="text-danger">* </span> : </label>
							<div class="mt-3">
								<input type="file" class="dropify @error('CUSTOM_PRIZE_IMAGE_LINK') parsley-error @enderror"  name="CUSTOM_PRIZE_IMAGE_LINK" data-default-file=""/>
								<p class="text-muted text-center mt-2 mb-0">Reward image</p>								
							</div> 
								@error('CUSTOM_PRIZE_IMAGE_LINK')
									<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
								 @enderror
						 </div>                       
                     </div>
                        <button type="submit" name="frmSubmit"class="btn btn-success waves-effect waves-light formActionButton" value="Save"><i class="fa fa-save mr-1"></i>Save</button>&nbsp;
						<button style="display:none" class="btn btn-primary formActionButton mr-1" type="button" disabled>
							<span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
							Saving...
						</button>
				        <a href="{{ route('rewards.rewardConfig.addreward') }}" name="frmCancel" type="button" class="btn btn-dark waves-effect waves-light ml-1"><i class="fa fa-eraser mr-1"></i>Clear</a>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
            <!-- end card-->
            <div class="mb-5 clearfix"></div>
            <div class="mb-5 "></div>
         </div>
         <!-- end col -->
      </div>
   </div>
</div>

@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>

<script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script> 


<!-- File Upload js -->
<script src="{{ asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
<script src="{{ asset('assets/libs/dropify/dropify.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/form-fileuploads.init.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>

<script>


	function customPrizeInput(str){
		if($("#cpi").prop('checked')){
				$("#custom_prize_input").show();
			}else{
				$("#custom_prize_input").hide();
			}
	}
	@if(session()->has('rewardInsertmessage'))
               $.toast({
					heading: 'Well Done !!',
					text: '{!! session()->get("rewardInsertmessage") !!}',
					showHideTransition: 'slide',
					icon: 'success',
					loader: true,
					position: 'top-right',
					hideAfter: 6000
				});
				{!! session()->forget('rewardInsertmessage'); !!}
				
            @endif
            @if(session()->has('rewardInsertmessageerror'))
                $.toast({
					heading: 'Error',
					text: '{!! session()->get("rewardInsertmessageerror") !!}',
					showHideTransition: 'fade',
					icon: 'error',
					position: 'top-right',
					hideAfter: 6000
				});
				
				{!! session()->forget('rewardInsertmessageerror'); !!}
            @endif 
			@if(session()->has('rewardnotInsertmessage'))
                $.toast({
					heading: 'Error',
					text: '{!! session()->get("rewardnotInsertmessage") !!}',
					showHideTransition: 'fade',
					icon: 'error',
					position: 'top-right',
					hideAfter: 6000
				});
				
				{!! session()->forget('rewardnotInsertmessage'); !!}
            @endif
	
</script>
<script>
$("#frmCreateReward").validate({
	rules: {
		groups: {
			nameGroup: "REAL_PRIZE PROMO_PRIZE CUSTOM_PRIZE_NAME TOURNAMENT_NAME"
		},
		REAL_PRIZE: {
			require_from_group: [1, ".prize_group"]
		},
		PROMO_PRIZE: {
			require_from_group: [1, ".prize_group"]
		},
		CUSTOM_PRIZE_NAME: {
			require_from_group: [1, ".prize_group"]
		},
		TOURNAMENT_NAME: {
			require_from_group: [1, ".prize_group"]
		},				
		REWARD_POINTS_REQUIRED: {
			required: true,
			number: true
		},		
		CUSTOM_PRIZE: {
			required: true,
			maxlength: 65
		},
		CUSTOM_PRIZE_DESC: {
			required: true,
			maxlength: 135
		},
		CUSTOM_PRIZE_IMAGE_LINK: {
				required:true,
                extension: "jpg|jpeg|png|gif"
		}
	},
	messages: {
		CUSTOM_PRIZE_IMAGE_LINK:{
            required:"Reward Image is reqiured",                  
            extension:"Only jpg,jpeg,png,gif image format allowed."
        }
	}	
});

	function ImageValidate() {
       
        if ($("#frmCreateReward").valid()) {
            
			$('.formActionButton').toggle();

            return true;
        }
    } 
</script>


@endsection
