@extends('bo.layouts.master')

@section('title', "Baazi Rewards | PB BO")

@section('pre_css')

 <!--File Upload css -->
<link href="{{ asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/dropify/dropify.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<style type="text/css">
.formtext{ font-size: 11px; font-style: italic; color: #999198; padding: 6px 0 0 0; margin: 0; line-height: 13px; }
.dropify-wrapper{ overflow: visible !important;  }
.dropify-wrapper label.error{position: absolute;left: 0;top: 101%;}
</style>
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <div class="page-title-right">
                  <ol class="breadcrumb m-0">
                     <li class="breadcrumb-item"><a >Baazi Rewards</a></li>
                     <li class="breadcrumb-item"><a>Reward Levels</a></li>
                     <li class="breadcrumb-item active">Edit</li>
                  </ol>
               </div>
               <h4 class="page-title">Edit Reward Level</h4>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-body">
                <div class="row mb-3">
                        <div class="col-sm-4">
                            <h5 class="font-18">Reward Levels </h5>
                        </div>
                        <div class="col-sm-8">
                            <div class="text-sm-right">
                                <a href="{{ route('rewards.rewardConfig.index') }}" class="btn btn-dark waves-effect waves-light"><i class="fa fa-bars mr-1"></i>View List</a>
                            </div>
                        </div>
                    <!-- end col-->
                </div>
               <form  name="frmUpdateReward" id="frmUpdateReward" enctype='multipart/form-data' autocomplete="off" method="post" action="{{ route('rewards.rewardConfig.updateRewardLevel') }}">
                @csrf
                <input type="hidden" value="{{$LEVEL_ID}}" name="LEVEL_ID" >
                     <div class="row">
                        <div class="col-md-9">
                           <div class="row">
                              <div class="form-group col-md-4">
                                 <label>Required Reward Points<span class="text-danger">* </span>:  </label>
                                 <input type="text" name="REWARD_POINTS_REQUIRED" value="{{$rewardLevelData[0]->REWARD_POINTS_REQUIRED}}" id="REWARD_POINTS_REQUIRED" class="form-control @error('REWARD_POINTS_REQUIRED') parsley-error @enderror">
								 @error('REWARD_POINTS_REQUIRED')
									<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
								@enderror
                              </div>
                              <div class="form-group col-md-4">
                                 <label>Level Name<span class="text-danger">* </span>:</label>
                                 <input name="CUSTOM_PRIZE" type="text" class="form-control @error('CUSTOM_PRIZE') parsley-error @enderror maxlengtherror" id="CUSTOM_PRIZE" maxlength="65" tabindex="12" value="{{$rewardLevelData[0]->CUSTOM_PRIZE}}">
								 @error('CUSTOM_PRIZE')
									<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
								@enderror
						  	</div>
                              <div class="form-group col-md-4">
                                 <label >Level Description<span class="text-danger">* </span>:</label>
                                 <input name="CUSTOM_PRIZE_DESC" type="text" class="form-control @error('CUSTOM_PRIZE_DESC') parsley-error @enderror maxlengtherror" id="CUSTOM_PRIZE_DESC" maxlength="130" tabindex="12" value="{{$rewardLevelData[0]->CUSTOM_PRIZE_DESC}}">
								 @error('CUSTOM_PRIZE_DESC')
									<ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
								@enderror
							  </div>
                           </div>
                           <div class="row">
                              <div class="form-group col-md-3">
                                 <label>Real Prize:  </label>
                                 <input name="REAL_PRIZE" type="text" class="form-control @error('REAL_PRIZE') parsley-error @enderror prize_group" id="REAL_PRIZE" tabindex="12" value="{{$rewardLevelData[0]->REAL_PRIZE}}">
                                 @error('REAL_PRIZE')
                                    <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                                 @enderror
								 <p class="formtext clearfix"><i>Insert any value except Blank or 0 for prize consideration</i></p>
                              </div>
                              <div class="form-group col-md-3">
                                 <label>Promo Prize:</label>
                                 <input name="PROMO_PRIZE" type="text" class="form-control @error('PROMO_PRIZE') parsley-error @enderror prize_group" id="PROMO_PRIZE"  tabindex="12" value="{{$rewardLevelData[0]->PROMO_PRIZE }}">
                                 @error('PROMO_PRIZE')
                                    <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                                 @enderror
								  <p class="formtext clearfix"><i>Insert any value except Blank or 0 for prize consideration</i></p>
                              </div>                              
                              <div class="form-group col-md-3">
                                 <label >Custom Prize:</label>
                                 <input name="CUSTOM_PRIZE_NAME" type="text" class="form-control @error('CUSTOM_PRIZE_NAME') parsley-error @enderror prize_group" id="CUSTOM_PRIZE_NAME" tabindex="12" value="{{$rewardLevelData[0]->CUSTOM_PRIZE_NAME }}">
                              @error('CUSTOM_PRIZE_NAME')
                                 <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                              @enderror
								  <p class="formtext clearfix"><i>Insert prize name for prize consideration</i></p>
                              </div>
                              <div class="form-group col-md-3">
                                <label>Tournament: </label>
                                <select name="TOURNAMENT_NAME" class="customselect @error('TOURNAMENT_NAME') parsley-error @enderror prize_group" data-plugin="customselect" id="TOURNAMENT_NAME" tabindex="3">
                                    <option hidden value="">Select</option>
                                    @foreach($tournamentNames as $key => $tournamentNames)	
                                    @php($selected = '')						
                                        @if($rewardLevelData[0]->TOURNAMENT_NAME ==$tournamentNames->TOURNAMENT_NAME)
                                            @php($selected = 'selected')
                                        @endif
                                        <option {{ $selected }} value="{{$tournamentNames->TOURNAMENT_NAME}}" > {{ $tournamentNames->TOURNAMENT_NAME }}</option>
                                    @endforeach
                                </select>
                                 @error('TOURNAMENT_NAME')
                                    <ul class="parsley-errors-list filled" ><li class="parsley-required">{{ $message }}</li></ul>
                                 @enderror
                                 <p class="formtext clearfix"><i>Select Tournament ticket for prize consideration</i></p>
                              </div>
                           </div>
                           <div class="row  customDatePickerWrapper">
                              <div class="form-group col-md-6">
                                 <label>Start Date: </label>
                                 <input name="date_from" type="text" class="form-control customDatePicker from" placeholder="Selct From Date" value="{{ \Carbon\Carbon::parse($rewardLevelData[0]->START_DATE)->format('d-M-Y H:i:s') }}">
                              </div>
                              <div class="form-group col-md-6">
                                 <label>End Date: </label>
                                 <input name="date_to" type="text" class="form-control customDatePicker from" placeholder="Selct From Date" value="{{ \Carbon\Carbon::parse($rewardLevelData[0]->END_DATE)->format('d-M-Y H:i:s') }}">
                              </div>                              
                           </div>
                        </div>
                        <div class="col-md-3">
						<label>Select Image<span class="text-danger">* </span>: </label>
							<div class="mt-3">
								<input type="file" class="dropify @error('CUSTOM_PRIZE_IMAGE_LINK') parsley-error @enderror" name="CUSTOM_PRIZE_IMAGE_LINK" data-default-file="{{config('poker_config.imageUrl.CDN_URL').config('poker_config.imageUrl.REWARD_IMAGE_FOLDER').$rewardLevelData[0]->CUSTOM_PRIZE_IMAGE_LINK}}"/>
								<p class="text-muted text-center mt-2 mb-0">Reward image</p>
							</div> 
                        </div>
                     </div>
                     <button type="submit" name="frmSubmit"class="btn btn-warning waves-effect waves-light" value="Update"><i class="fa fa-edit mr-1"> Update</i></button>
                     <!-- <button type="button" class="btn btn-warning waves-effect waves-light ml-1"><i class="far fa-edit mr-1"></i> Update</button> -->
                     <a href="{{ route('rewards.rewardConfig.index') }}" class="btn btn-dark waves-effect waves-light ml-1"><i class="mdi mdi-replay mr-1"></i>Cancel</a>
                  </form>
               </div>
               <!-- end card-body-->
            </div>
            <!-- end card-->
            <div class="mb-5 clearfix"></div>
            <div class="mb-5 "></div>
         </div>
         <!-- end col -->
      </div>
   </div>
</div>

@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>

<script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script> 

<script src="{{ asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
<script src="{{ asset('assets/libs/dropify/dropify.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/form-fileuploads.init.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>

<script>

	function customPrizeInput(str){
		if($("#cpi").prop('checked')){
				$("#custom_prize_input").show();
			}else{
				$("#custom_prize_input").hide();
			}
	}
	@if(session()->has('rewardupdatemessage'))
               $.toast({
					heading: 'Well Done !!',
					text: '{!! session()->get("rewardupdatemessage") !!}',
					showHideTransition: 'slide',
					icon: 'success',
					loader: true,
					position: 'top-right',
					hideAfter: 6000
				});
				{!! session()->forget('rewardupdatemessage'); !!}
				
            @endif
            @if(session()->has('rewardupdatemessageerror'))
                $.toast({
					heading: 'Error',
					text: '{!! session()->get("rewardupdatemessageerror") !!}',
					showHideTransition: 'fade',
					icon: 'error',
					position: 'top-right',
					hideAfter: 6000
				});
				
				{!! session()->forget('rewardupdatemessageerror'); !!}
            @endif 
		
	
</script>
<script>
$("#frmUpdateReward").validate({
	rules: {
		REWARD_POINTS_REQUIRED: {
			required: true,
			number: true
		},
		CUSTOM_PRIZE: "required",
		CUSTOM_PRIZE_DESC: "required",
      CUSTOM_PRIZE_IMAGE_LINK: {
            extension: "jpg|jpeg|png|gif"
		}
	},
	messages: {
		CUSTOM_PRIZE_IMAGE_LINK:{                
            extension:"Only jpg,jpeg,png,gif image format allowed."
        }
	}	
	
});
</script>
@endsection