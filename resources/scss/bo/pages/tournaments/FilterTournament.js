/**
 * Filter Tournament 
 * 
 * We have use this class for Filter Tournament
 * 
 * Class FilterTournament
 * 
 * @category    Manage Tournament
 * @package     Tournament
 * @copyright   PokerBaazi
 * @author      Nitin Sharma<nitin.sharma@moonshinetechnology.com>
 */
class FilterTournament {
    constructor(options = {}) {
        this.options = options;
        this.initFilterTournament();
    }
    initFilterTournament() {
        const _ = this;
        _.initFilterTournamentOption();
        _.initFilterTournamentEvents();
    }

    initFilterTournamentOption() {
        let options = {};
        this.options = $.extend({}, this.options, $.isPlainObject(options) && options);
    }

    initFilterTournamentEvents() {
        const _ = this;
        const { options } = _;

        _.confirmationEvents();
        _.tournamentDetailsModalMethod();
        _.tournamentBlindStructureInfoModelMethod();
        _.tournamentPrizeInfoModalMethod();
    }

    tournamentDetailsModalMethod() {
        $('#tournamentDetailsModal').on('show.bs.modal', function(e) {
            window.pageData = window.pageData || {};
            window.pageData.tournament = window.pageData.tournament || {};
            window.pageData.tournament.tournamentDetails = window.pageData.tournament.tournamentDetails || {};

            var $relatedTarget = $(e.relatedTarget);
            var $this = $(this);
            $this.find('.mainWrapper').html('');
            $this.find(`[data-view-column]`).html('');
            $this.find('.loader, .loader div').show().css({ 'z-index': 99 });
            $this.find('.loaderWrapper').css({ 'height': '500px' });

            var $id = $relatedTarget.data('tournamentId');
            var $url = `${window.pageData.baseUrl}/tournaments/api/tournamentInfo/${$id}`;
            $.CustomFilterForm.ajaxCall($url, {}, { 'refObj': window.pageData.tournament.tournamentDetails, 'method': 'POST' })
                .then((response) => {
                    $this.find('.mainWrapper').html(response);
                    $this.find('.loader, .loader div').hide().css({ 'z-index': -1 });
                    $this.find('.loaderWrapper').css({ 'height': '0' });
                });
        });
    }

    tournamentBlindStructureInfoModelMethod() {
        $('#tournamentBlindStructureInfoModel').on('show.bs.modal', function(e) {
            window.pageData = window.pageData || {};
            window.pageData.tournament = window.pageData.tournament || {};
            window.pageData.tournament.tournamentBlindStructureInfo = window.pageData.tournament.tournamentBlindStructureInfo || {};

            let $relatedTarget = $(e.relatedTarget);
            let $this = $(this);
            $this.find(`[data-view-column]`).html('');
            $this.find('.loader, .loader div').show().css({ 'z-index': 99 });
            var $tableRef = $this.find('table');

            $.CustomDataTable.cleanDestroy($tableRef);
            $.CustomDataTable.reInit($tableRef);

            let $id = $relatedTarget.data('tournamentId');
            let $url = `${window.pageData.baseUrl}/tournaments/api/tournamentBlindStructureInfo/${$id}`;

            $.CustomFilterForm.ajaxCall($url, {}, { 'refObj': window.pageData.tournament.tournamentBlindStructureInfo, 'method': 'POST' })
                .then((response) => {
                    let $data = response.data || {};
                    let $resData = $data.tournament;
                    let $bindStructure = $data.bindStructure;

                    let $bindStructureHtml = "";

                    $.each($resData, (key, value) => {
                        $this.find(`[data-view-column="${key}"]`).text(value);
                    });

                    $this.find(`[data-view-column="BUYIN+ENTRY_FREE"]`).text(`${$resData.BUYIN}+${$resData.ENTRY_FEE}`);

                    $bindStructureHtml = $bindStructure.map((v, i) => {
                        return `
                    <tr>
                        <td>${v.STAKE_LEVELS}</td>
                        <td>${v.SMALL_BLIND}/${v.BIG_BLIND}</td>
                        <td>${v.ANTE}</td>
                        <td>${v.LEVEL_PERIOD}</td>
                    </tr>
                `
                    });

                    $.CustomDataTable.cleanDestroy($tableRef);
                    $this.find('table tbody').html($bindStructureHtml);
                    $.CustomDataTable.reInit($tableRef);

                    $this.find('.loader, .loader div').hide().css({ 'z-index': -1 });
                });
        });
    }

    tournamentPrizeInfoModalMethod() {
        $('#tournamentPrizeInfoModal').on('show.bs.modal', function(e) {
            window.pageData = window.pageData || {};
            window.pageData.tournament = window.pageData.tournament || {};
            window.pageData.tournament.tournamentPrizeInfo = window.pageData.tournament.tournamentPrizeInfo || {};

            let $relatedTarget = $(e.relatedTarget);
            let $this = $(this);
            $this.find(`[data-view-column]`).html('');
            $this.find('.loader, .loader div').show().css({ 'z-index': 99 });

            $this.find('.prizeStructureCommon table tbody').html(`<div class="p-4"></div>`);

            var $tableRef = $this.find('.prizeStructure table');
            $.CustomDataTable.cleanDestroy($tableRef);
            $.CustomDataTable.reInit($tableRef);

            $this.find('.prizeStructureCommon').hide();

            let $id = $relatedTarget.data('tournamentId');
            var $tournamentStatus = $relatedTarget.data('tournamentStatus');
            let $url = `${window.pageData.baseUrl}/tournaments/api/tournamentPrizeInfo/${$id}`;

            $.CustomFilterForm.ajaxCall($url, {}, { 'refObj': window.pageData.tournament.tournamentPrizeInfo, 'method': 'POST' })
                .then((response) => {
                    let $data = response.data || {};
                    let $tournamentPrize = $data.tournamentPrize;

                    $tournamentPrize = Object.values($tournamentPrize) || [];
                    let $dataArrayBody = [];
                    let $dataArrayHead = [];
                    if ($tournamentPrize.length > 0) {
                        if ($tournamentStatus == 6) {
                            let $dataHeadingIndex = [];
                            var rowHead = `<tr><th>Players</th>`;
                            $tournamentPrize[0].forEach(v => {
                                rowHead += `<th>${v.PLAYERS_MIN}-${v.PLAYERS_MAX}(%)</th>`;
                                $dataHeadingIndex.push(`${v.PLAYERS_MIN}-${v.PLAYERS_MAX}`);
                            });
                            rowHead += `</tr>`;
                            $dataArrayHead.push(rowHead);

                            var row = "";
                            $tournamentPrize.forEach((value, index) => {
                                value = Object.values(value) || [];
                                var row = `<tr>`;
                                let RANK_MIN = value[0].RANK_MIN;
                                let RANK_MAX = value[0].RANK_MAX;
                                let rank = RANK_MIN == RANK_MAX ? $.OtherGlobalThing.ordinal_suffix_of(RANK_MIN) : `${value[0].RANK_MIN}-${value[0].RANK_MAX}`;

                                row += `<td>${rank}</td>`;
                                let firstColumnMaxIndex = $dataHeadingIndex.indexOf(`${value[0].PLAYERS_MIN}-${value[0].PLAYERS_MAX}`);
                                row += `<td></td>`.repeat(firstColumnMaxIndex);
                                value.forEach((v, i) => {
                                    row += `<td>${v.PRIZE_PERCENTAGE}</td>`;
                                });
                                row += `</tr>`;
                                $dataArrayBody.push(row);
                            });

                            $this.find('.defaultPrizeStructure table thead').html($dataArrayHead);
                            $this.find('.defaultPrizeStructure table tbody').html($dataArrayBody);
                            $this.find('.defaultPrizeStructure').show();
                        } else {
                            $tournamentPrize.forEach((value, index) => {
                                let row = `<tr>
                                    <td>${value.RANK}</td>
                                    <td>${value.PRIZE_VALUE}</td>
                                    <td>${value.WINNER_PERCENTAGE}</td>
                                    <td>${value.PRIZE_TOURNAMENT_ID}</td>
                                    <td>${value.PRIZE_TOURNAMENT_NAME}</td>
                                </tr>`;

                                $dataArrayBody.push(row);
                            });

                            $.CustomDataTable.cleanDestroy($tableRef);
                            $this.find('.prizeStructure table tbody').html($dataArrayBody);
                            $.CustomDataTable.reInit($tableRef);
                            $this.find('.prizeStructure').show();
                        }
                    } else {
                        $this.find('.defaultPrizeStructure table tbody').html(`<tr><td>There is no record to display</td></tr>`);
                        $this.find('.defaultPrizeStructure').show();
                    }
                    $this.find('.loader, .loader div').hide().css({ 'z-index': -1 });
                });
        });
    }

    confirmationEvents() {
        const _ = this;

        $(document).on('click', ".StuckResumeTournamentEvent", (e) => {
            e.preventDefault();
            e.stopPropagation();
            let $this = $(e.target);

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Stuck & Resume it!'
            }).then((result) => {
                if (result.value) {
                    $("#StuckResumeTournamentForm").attr('action', `${window.pageData.baseUrl}/tournaments/manage-tourneys/StuckResumeTournament/${$this.data('tournamentEncryptedId')}`);
                    $("#StuckResumeTournamentForm").get(0).submit();
                }
            })
        });

        $(document).on('click', ".cancelTournamentEvent", (e) => {
            e.preventDefault();
            e.stopPropagation();
            let $this = $(e.target);

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Cancel it!'
            }).then((result) => {
                if (result.value) {
                    $("#cancelTournamentForm").attr('action', `${window.pageData.baseUrl}/tournaments/manage-tourneys/cancelTournament/${$this.data('tournamentEncryptedId')}`);
                    $("#cancelTournamentForm").get(0).submit();
                }
            })
        });
    }
}