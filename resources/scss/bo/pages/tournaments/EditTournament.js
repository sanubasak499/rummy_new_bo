/**
 * Edit Tournament 
 * 
 * We have use this class for Edit Tournament
 * inherted Tournament class to use common methods
 * 
 * Class EditTournament
 * 
 * @category    Manage Tournament
 * @package     Tournament
 * @copyright   PokerBaazi
 * @author      Nitin Sharma<nitin.sharma@moonshinetechnology.com>
 */
class EditTournament extends Tournament {
    constructor(options = {}) {
        super(options);
        this.initEditTournament();
    }
    initEditTournament() {
        const _ = this;
        _.initEditTournamentOption();
        _.initJQueryValidation();
        _.initEditTournamentEvents();
        _.initBootstrapWizard();
        _.initEditTournamentShowHideObject();
        _.setDateTimePicker();
    }
    triggerEventsOnInit() {
        $('#CATEGORY').trigger('change');
        $('#PRIVATE_TABLE').trigger('change');
        $('.coinTypeIdEvent:checked').trigger('change');
        $('#BLIND_STRUCTURE_ID').trigger('change');
        $('.RebuyAddonReEntryEvent:checked').trigger('change');
        $('.prizeStructureIdEvent:checked').trigger('change');
        $('#LATE_REGISTRATION_ALLOW').trigger('change');
    }

    initEditTournamentOption() {
        let options = {
            formId: "editTournamentForm",
            wizardId: "editTournamentWizard",
            IsCreate: false
        }
        this.options = $.extend({}, this.options, $.isPlainObject(options) && options);
    }

    initEditTournamentShowHideObject() {
        const _ = this;
        const {
            TournamentInformationSection: {
                children: {
                    CATEGORY,
                    TOURNAMENT_TYPE_ID,
                    TOURNAMENT_SUB_TYPE_ID,
                    PRIVATE_TABLE
                }
            },
            TimingSection: {

            },
            PlayerSection: {
                children: {
                    PLAYER_PER_TABLE
                }
            },
            EntryCriteriaSection: {
                children: {
                    COIN_TYPE_ID
                }
            },
            BlindStructureSection: {
                children: {
                    BLIND_STRUCTURE_ID,
                    TIMER_TOURNAMENT_END_TIME,
                    TOURNAMENT_LEVEL
                }
            },
            TimeSettingsSection,
            RebuyAddonReEntrySection: {
                children: {
                    RebuyAddonReEntry
                }
            },
            PrizeStructureSection: {
                children: {
                    PRIZE_STRUCTURE_ID,
                    NO_OF_WINNERS_CUSTOM,
                    NO_OF_WINNERS_CUSTOM_MIX
                }
            }
        } = _.formSections;

    }

    setDateTimePicker() {
        var $customDatePickerWrapper = $('.customDatePickerWrapper');

        let from = $(document).find(".customDatePicker.from");
        let fromFlatPicker = from.data('flatpickr');
        // fromFlatPicker.set('minDate', new Date());

        fromFlatPicker.set('onClose', function(selectedDates, dateStr, instance) {
            let minDate = new Date(dateStr);
            let customDatePickerFrom = $customDatePickerWrapper.find(".customDatePicker.to");
            var flatpickrFrom = customDatePickerFrom.data('flatpickr');
            flatpickrFrom.set("minDate", minDate);
        });

        let to = $(document).find(".customDatePicker.to");
        let toFlatPicker = to.data('flatpickr');
        toFlatPicker.set('minDate', new Date());

        toFlatPicker.set('onClose', function(selectedDates, dateStr, instance) {
            let maxDate = new Date(dateStr);
            // $customDatePickerWrapper.find(".customDatePicker.from").flatpickr('maxDate', maxDate);
            let customDatePickerTo = $customDatePickerWrapper.find(".customDatePicker.from");
            var flatpickrTo = customDatePickerTo.data('flatpickr');
            flatpickrTo.set("maxDate", maxDate);
        });
    }

    initEditTournamentEvents() {
        const _ = this;
        const { options, options: { wizardId, formId }, jqueryValidationForm } = _;
        $(document).on('click', '.formSubmitButtom', (e) => {
            let $this = $(e.target);
            var $valid = $(`#${formId}`).valid();
            if (!$valid) {
                jqueryValidationForm.focusInvalid();
                return false;
            } else {
                $(document).find(`[name^="WINNER_PERCENTAGE"]`).eq(0).trigger('change');
                if ($(`#${formId}`).hasClass('invalidateTotal') && (+$(`#${formId}`).find(`[name="NO_OF_WINNERS_CUSTOM"]`).val()) > 0) {
                    return false;
                } else {

                    $('.formSubmitButtom').prop('disabled', true).append(`<span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>`);
                    if ($this.attr(`name`) == "saveButton") {
                        $(`#${formId}`).append(`<input type="hidden" name="saveButton" value="saveTemplate">`);
                    }
                    $(`#${formId}`).get(0).submit();
                }
            }
        });
    }

    initJQueryValidation() {
        const _ = this;
        const { formId } = this.options;
        _.jqueryValidationForm = $(`#${formId}`).validate({
            rules: {
                TOURNAMENT_NAME: {
                    required: true,
                    minlength: 4,
                    fieldTrim: true
                },
                TOURNAMENT_TYPE_ID: {
                    required: true
                },
                SERVER_ID: {
                    required: true
                },
                TOURNAMENT_DESC: {
                    required: true,
                    fieldTrim: true
                },
                CATEGORY: {
                    required: true
                },
                TOURNAMENT_LIMIT_ID: {
                    required: true
                },
                WIN_THE_BUTTON: {
                    required: true
                },
                TOURNAMENT_SUB_TYPE_ID: {
                    required: true
                },
                TOURNAMENT_PARENT_SATELLITE_ID: {
                    required: true
                },
                TOURNAMENT_PARENT_MULTI_DAY_ID: {
                    required: true
                },
                PASSWORD: {
                    required: function(element) {
                        return $("#PRIVATE_TABLE").val() == 1;
                    }
                },
                REGISTER_START_TIME: {
                    required: true,
                    // dateAfterToday: true,
                },
                TOURNAMENT_START_TIME: {
                    required: true,
                    dateAfterToday: true,
                    tournamentTimeGreaterThenReg: true
                },
                LATE_REGISTRATION_END_TIME: {
                    required: true,
                    digits: true,
                    min: 0,
                    max: 999
                },
                PLAYER_PER_TABLE: {
                    required: true
                },
                T_MIN_PLAYERS: {
                    required: true,
                    digits: true,
                    min: 2,
                },
                T_MAX_PLAYERS: {
                    required: true,
                    digits: true,
                    min: 2,
                    checkmaxvsminplayer: true
                },
                COIN_TYPE_ID: {
                    required: true,
                },
                BUYIN: {
                    required: true,
                    digits: true
                },
                ENTRY_FEE: {
                    required: true,
                    number: true
                },
                BOUNTY_AMOUNT: {
                    required: true,
                    number: true
                },
                BOUNTY_ENTRY_FEE: {
                    required: true,
                    number: true
                },
                PROGRESSIVE_BOUNTY_PERCENTAGE: {
                    required: true,
                    number: true
                },
                BLIND_STRUCTURE_ID: {
                    required: true
                },
                TOURNAMENT_LEVEL: {
                    required: true
                },
                LEVEL_PERIOD: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                TOURNAMENT_CHIPS: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                TIMER_TOURNAMENT_END_TIME: {
                    required: true,
                    tournamentLevelCheck: true,
                    digits: true,
                    min: 1
                },
                PLAYER_HAND_TIME: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                DISCONNECT_TIME: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                EXTRA_TIME: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                LOBBY_DISPLAY_INTERVAL: {
                    required: true,
                    min: 18
                },
                PLAYER_MAX_EXTRATIME: {
                    required: true,
                },
                ADDITIONAL_EXTRATIME_LEVEL_INTERVAL: {
                    required: true,
                },
                ADDITIONAL_EXTRATIME: {
                    required: true,
                },
                REBUY_CHIPS: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                REBUY_ELIGIBLE_CHIPS: {
                    required: true,
                    digits: true,
                    greaterThanZero: true,
                    checkstarchipvsrebuychips: true
                },
                REBUY_END_TIME: {
                    required: true,
                    digits: true
                },
                REBUY_COUNT: {
                    required: true,
                },
                REBUY_IN: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                REBUY_ENTRY_FEE: {
                    required: true,
                    digits: true,
                },
                ADDON_CHIPS: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                ADDON_BREAK_TIME: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                ADDON_AMOUNT: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                ADDON_ENTRY_FEE: {
                    required: true,
                    digits: true,
                },
                PRIZE_STRUCTURE_ID: {
                    required: true,
                },
                PRIZE_STRUCTURE_ID: {
                    required: true,
                },
                PRIZE_STRUCTURE_TYPE_ID: {
                    required: true,
                },
                FIXED_PRIZE: {
                    required: true,
                },
                NO_OF_WINNERS_CUSTOM: {
                    required: true,
                    digits: true,
                    min: 1
                },
                NO_OF_WINNERS_CUSTOM_MIX: {
                    required: true,
                    digits: true,
                    min: 1
                },
                GUARENTIED_PRIZE: {
                    required: true,
                    digits: true,
                },
                SATELLITES_GUARANTEED_PLACES_PAID: {
                    required: true,
                    digits: true,
                },
                MULTIDAY_PLAYER_PERCENTAGE: {
                    required: true,
                    digits: true,
                },

            },
            errorPlacement: function(error, element) {
                $(element).removeClass('.is-invalid');
                $(element).next('.invalid-feedback').remove();
                error.insertAfter(element);
            }
        });
    }

    initBootstrapWizard() {
        const _ = this;
        const { options, options: { wizardId, formId }, jqueryValidationForm } = _;

        $(`#${wizardId}`).bootstrapWizard({
            'tabClass': options.tabClass,
            'nextSelector': options.nextSelector,
            'previousSelector': options.previousSelector,
            'firstSelector': options.firstSelector,
            'lastSelector': options.lastSelector,
            'finishSelector': options.finishSelector,
            onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index + 1;
                var $percent = ($current / $total) * 100;
                $(`#${formId}`).find('.bar').css({ width: $percent + '%' });
            },
            onTabClick: function($activeTab, $navigation, currentIndex, clickedIndex, $clickedTab) {
                if (currentIndex < clickedIndex) {
                    return false;
                } else {
                    this.showNextHideFinishButton();
                }
                if (1 == (clickedIndex + 1)) {
                    this.hidePrevButton();
                }
            },
            onTabChange: function($activeTab, $navigation, currentIndex, nextTab) {

            },
            onNext: function($activeTab, $navigation, nextIndex) {
                var $total = $navigation.find('li').length;
                var $current = nextIndex + 1;
                var $valid = $(`#${formId}`).valid();

                if (!$valid) {
                    jqueryValidationForm.focusInvalid();
                    return false;
                }

                this.showPrevButton();

                if ($total == $current) {
                    this.showFinishHideNextButton();
                }
                if (!$navigation.find(`li[data-wizard-index="${nextIndex}"]`).is(':visible')) {
                    $navigation.find(`li[data-wizard-index="${nextIndex}"]`).next().find('a').tab('show');
                    if ($total == (nextIndex + 2)) this.showFinishHideNextButton();
                    return false;
                }
            },
            onPrevious: function($activeTab, $navigation, previousIndex) {
                var $current = previousIndex + 1;
                this.showNextHideFinishButton();
                if (!$navigation.find(`li[data-wizard-index="${previousIndex}"]`).is(':visible')) {
                    $navigation.find(`li[data-wizard-index="${previousIndex}"]`).prev().find('a').tab('show');
                    return false;
                }
                if (1 == $current) {
                    this.hidePrevButton();
                }
            },
            onLast: function() {
                return false;
            },
            onFinish: function($activeTab, $navigation, lastIndex) {

            },
            onBack: function($activeTab, $navigation, formerIndex) {
                return false;
            },
            showNextHideFinishButton: function() {
                $(`#${wizardId}`).find(options.nextSelector).show();
                $(`#${wizardId}`).find(options.finishSelector).hide().prop('disabled', true);
            },
            showFinishHideNextButton: function() {
                $(`#${wizardId}`).find(options.finishSelector).show().prop('disabled', false);
                $(`#${wizardId}`).find(options.nextSelector).hide();
            },
            hidePrevButton: function() {
                $(`#${wizardId}`).find(options.previousSelector).hide().prop('disabled', true);
            },
            showPrevButton: function() {
                $(`#${wizardId}`).find(options.previousSelector).show().prop('disabled', false);
            }

        });
    }

    CategoryChangeHandle(e, obj) {
        super.CategoryChangeHandle(e, obj);
        $('#TOURNAMENT_TYPE_ID').trigger("change");
    }
    tournamentTypeIdChangeHandle(e, obj) {
        super.tournamentTypeIdChangeHandle(e, obj);
        $('#TOURNAMENT_SUB_TYPE_ID').trigger("change");
    }

    tournamentSubTypeIdChangeHandle(e, obj) {
        super.tournamentSubTypeIdChangeHandle(e, obj);
        this.resetPrizeStructures();
    }

    resetPrizeStructures() {
        super.resetPrizeStructures();
    }

    noOfWinnersCustomMarkupGenrator(index) {
        const { finishInit, options: { IsCreate }, tournamentData: { TOURNAMENT_STATUS } } = this;
        let isDisable = "",
            isReadonly = "";
        isDisable = TOURNAMENT_STATUS == 0 ? null : 'disabled';
        isReadonly = TOURNAMENT_STATUS == 0 ? null : 'readonly';

        return `<div class="form-group col-md-2">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">${index + 1}</span>
                        </div>
                        <input type="number" min="1" max="100" name="WINNER_PERCENTAGE[${index}]" class="form-control" ${isReadonly}>
                        <span class="input-group-addon bootstrap-touchspin-postfix input-group-append"><span class="btn btn-primary bootstrap-touchspin-up">%</span></span>
                    </div>
                </div>`;
    }

    noOfWinnersCustomMixMarkupGenrator(index) {
        const { finishInit, options: { IsCreate }, tournamentData: { TOURNAMENT_STATUS } } = this;
        let isDisable = "",
            isReadonly = "",
            hiddenField = "";
        isDisable = TOURNAMENT_STATUS == 0 ? null : 'disabled';
        isReadonly = TOURNAMENT_STATUS == 0 ? null : 'readonly';

        let { satalliteTournaments } = this.options;
        satalliteTournaments = JSON.parse(atob(satalliteTournaments));
        let optionString = `<option value="">Select Tournament</option>`
        satalliteTournaments.forEach((v, i) => {
            optionString += `<option value="${$.trim(v.TOURNAMENT_NAME)}">${v.TOURNAMENT_START_TIME} - (${v.TOURNAMENT_ID})${v.TOURNAMENT_NAME} - ₹${v.TOT_BUYIN}</option>`;
        });
        let random = Math.floor(Math.random() * 9999) + 1;

        return `<div class="row">
                <div class="col-md-4">
                    <div class="form-group ">
                        <div class="radio radio-info form-check-inline ml-2">
                            <input type="radio" id="amount${random}" value="1" data-target-class="customMixMarkUp${random}_" name="CUSTOM_MIX_PRIZE_TYPE[${index}]" checked ${isDisable}>
                            <label for="amount${random}"> Amount </label>
                        </div>
                        <div class="radio radio-info form-check-inline">
                            <input type="radio" id="ticket${random}"  value="8" data-target-class="customMixMarkUp${random}_" name="CUSTOM_MIX_PRIZE_TYPE[${index}]" ${isDisable}>
                            <label for="ticket${random}"> Ticket </label>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-4 customMixMarkUp${random}_1 boxhideshow">
                    <input type="text" name="PRIZE_VALUE[${index}]" class="form-control" ${isReadonly}>
                </div>
                <div class="form-group col-md-4 customMixMarkUp${random}_8 boxhideshow" style="display:none;">
                    <select class="form-control" name="PRIZE_TOURNAMENT_NAME[${index}]" ${isDisable}>${optionString}</select>
                </div>
            </div>`;
    }
}