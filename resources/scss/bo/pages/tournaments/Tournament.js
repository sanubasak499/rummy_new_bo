/**
 * Manage Tournament 
 * 
 * This File is responsible for the manange Tournament 
 * that can be Create Tournament, Edit Tournament,
 * The Tournament Class is the base class for the all 
 * other class in this class write common code which will
 * reuse in other classes
 * 
 * Class Tournament
 * 
 * @category    Manage Tournament
 * @package     Tournament
 * @copyright   PokerBaazi
 * @author      Nitin Sharma<nitin.sharma@moonshinetechnology.com>
 * Created At:  03/02/2020
 */

class Tournament {
    constructor(options = {}) {
        this.finishInit = false;
        this.options = options;
        this.init();
    }

    init() {
        this.initOptions();
        this.initShowHideObject();
        this.initEvents();
        this.jqueryValidationAdditionalMethods();
    }

    triggerAfterInit() {
        this.finishInit = true;
    }

    initOptions() {
        let options = {
            baseUrl: window.pageData.baseUrl,
            tabClass: 'nav nav-pills',
            nextSelector: '.button-next',
            previousSelector: '.button-previous',
            firstSelector: '.button-first',
            lastSelector: '.button-last',
            finishSelector: '.button-finish',
        };
        this.options = $.extend({}, this.options, $.isPlainObject(options) && options);
        const { tournament } = this.options;
        this.tournamentData = tournament ? JSON.parse(atob(this.options.tournament)) : {};
        this.finishInit = tournament ? false : true;
    }

    initShowHideObject() {
        const _ = this;
        _.formSections = {
            TournamentInformationSection: {
                id: '#TournamentInformationSection',
                prefix: 'TournamentInformation',
                children: {
                    CATEGORY: {
                        id: '#CATEGORY',
                        prefix: 'category',
                        // clickEventHandler: _.channelChartHandle,
                        changeEventHandler: _.CategoryChangeHandle,
                        commonHideClass: ".categoryCommon",
                        conditionsShowHide: {
                            show: [],
                            hide: [{
                                val: "99",
                                selector: "#BountyAmountWrapper, #BountyEntryFeeWrapper, #ProgressiveBountyPercentageWrapper"
                            }]
                        },
                        setDefaultValues: {
                            ifValue: [{
                                "99": [{
                                    selector: "#BUYIN, #ENTRY_FEE",
                                    value: 0
                                }]
                            }],
                        }
                    },
                    TOURNAMENT_TYPE_ID: {
                        id: '#TOURNAMENT_TYPE_ID',
                        prefix: "tournamentTypeId",
                        commonHideClass: ".tournamentTypeIdCommon",
                        changeEventHandler: _.tournamentTypeIdChangeHandle,
                        conditionsShowHide: {
                            show: [{
                                val: 4,
                                selector: "#TournamentChipsWrapper, #tournamentSubTypeIdWrapper, #registerStartTimeWrapper, #tournamentStartTimeWrapper"
                            }, {
                                val: 10,
                                selector: "#TournamentChipsWrapper, #BountyAmountWrapper, #BountyEntryFeeWrapper, #registerStartTimeWrapper, #tournamentStartTimeWrapper"
                            }, {}, {
                                val: 9,
                                selector: "#registerStartTimeWrapper, #tournamentStartTimeWrapper"
                            }, {
                                val: 11,
                                selector: "#TournamentChipsWrapper, #BountyAmountWrapper, #BountyEntryFeeWrapper, #ProgressiveBountyPercentageWrapper, #registerStartTimeWrapper, #tournamentStartTimeWrapper"
                            }, {
                                val: 12,
                                selector: "#TournamentChipsWrapper, #tournamentSubTypeIdWrapper, #registerStartTimeWrapper, #tournamentStartTimeWrapper"
                            }, {
                                val: 1,
                                selector: "#TournamentChipsWrapper, #tournamentSubTypeIdWrapper"
                            }],
                            hide: [{
                                val: 4,
                                selector: "#TournamentParentSatelliteIdWrapper, #TournamentParentMultiDayIdWrapper, #TimerTournamentEndTimeWrapper, #BountyAmountWrapper, #BountyEntryFeeWrapper, #ProgressiveBountyPercentageWrapper"
                            }, {
                                val: 9,
                                selector: "#TournamentParentSatelliteIdWrapper, #TournamentParentMultiDayIdWrapper, #tournamentSubTypeIdWrapper, #BountyAmountWrapper, #BountyEntryFeeWrapper, #ProgressiveBountyPercentageWrapper, #TournamentChipsWrapper, #TimerTournamentEndTimeWrapper"
                            }, {
                                val: 10,
                                selector: "#TournamentParentSatelliteIdWrapper, #TournamentParentMultiDayIdWrapper, #tournamentSubTypeIdWrapper, #ProgressiveBountyPercentageWrapper, #TimerTournamentEndTimeWrapper"
                            }, {
                                val: 11,
                                selector: "#TournamentParentSatelliteIdWrapper, #TournamentParentMultiDayIdWrapper, #tournamentSubTypeIdWrapper, #TimerTournamentEndTimeWrapper"
                            }, {
                                val: 12,
                                selector: "#TournamentParentSatelliteIdWrapper, #TournamentParentMultiDayIdWrapper, #TimerTournamentEndTimeWrapper, #BountyAmountWrapper, #BountyEntryFeeWrapper, #ProgressiveBountyPercentageWrapper"
                            }, {
                                val: 1,
                                selector: "#TournamentParentSatelliteIdWrapper, #TournamentParentMultiDayIdWrapper, #TimerTournamentEndTimeWrapper, #BountyAmountWrapper, #BountyEntryFeeWrapper, #ProgressiveBountyPercentageWrapper, #registerStartTimeWrapper, #tournamentStartTimeWrapper"
                            }]
                        },

                    },
                    TOURNAMENT_SUB_TYPE_ID: {
                        id: '#TOURNAMENT_SUB_TYPE_ID',
                        prefix: "TournamentSubTypeId",
                        commonHideClass: ".tournamentSubTypeIdCommon",
                        changeEventHandler: _.tournamentSubTypeIdChangeHandle,
                        conditionsShowHide: {
                            show: [{
                                val: 2,
                                selector: "#TournamentParentSatelliteIdWrapper"
                            }, {
                                val: 3,
                                selector: "#TournamentParentMultiDayIdWrapper"
                            }, {
                                val: 4,
                                selector: "#TimerTournamentEndTimeWrapper"
                            }],
                            hide: [{
                                val: 1,
                                selector: "#TournamentParentSatelliteIdWrapper, #TournamentParentMultiDayIdWrapper, #TimerTournamentEndTimeWrapper"
                            }, {
                                val: 2,
                                selector: "#TournamentParentMultiDayIdWrapper, #TimerTournamentEndTimeWrapper, #BountyAmountWrapper, #BountyEntryFeeWrapper, #ProgressiveBountyPercentageWrapper"
                            }, {
                                val: 3,
                                selector: "#TournamentParentSatelliteIdWrapper, #TimerTournamentEndTimeWrapper, #BountyAmountWrapper, #BountyEntryFeeWrapper, #ProgressiveBountyPercentageWrapper"
                            }, {
                                val: 4,
                                selector: "#TournamentParentSatelliteIdWrapper, #TournamentParentMultiDayIdWrapper, #BountyAmountWrapper, #BountyEntryFeeWrapper, #ProgressiveBountyPercentageWrapper"
                            }]
                        },

                    },
                    PRIVATE_TABLE: {
                        id: '#PRIVATE_TABLE',
                        prefix: "privateTable",
                        commonHideClass: ".privateTableCommon",
                        changeEventHandler: _.privateTableChangeHandle,
                        conditionsShowHide: {
                            show: [],
                            hide: [{
                                val: 0,
                                selector: "#passwordWrapper"
                            }]
                        },

                    }
                }
            },
            TimingSection: {
                id: '#TimingSection',
                prefix: 'Timing',
                children: {
                    LATE_REGISTRATION_ALLOW: {
                        id: '#LATE_REGISTRATION_ALLOW',
                        prefix: 'lateRegistrationAllow',
                        changeEventHandler: _.lateRegistrationAllowChangeHandle,
                        commonHideClass: ".lateRegistrationAllowCommon",
                    }
                }
            },
            PlayerSection: {
                id: '#PlayerSection',
                prefix: 'Player',
                children: {
                    PLAYER_PER_TABLE: {
                        id: '#PLAYER_PER_TABLE',
                        prefix: 'playerPerTable',
                        changeEventHandler: _.playerPerTableChangeHandle,
                        commonHideClass: ".playerPerTableCommon",
                    }
                }
            },
            EntryCriteriaSection: {
                id: '#EntryCriteriaSection',
                prefix: 'EntryCriteria',
                children: {
                    COIN_TYPE_ID: {
                        id: '#COIN_TYPE_ID',
                        prefix: 'coinTypeId',
                        changeEventHandler: _.coinTypeIdChangeHandle,
                        commonHideClass: ".coinTypeIdCommon",
                    }
                }
            },
            BlindStructureSection: {
                id: '#BlindStructureSection',
                prefix: 'BlindStructure',
                children: {
                    BLIND_STRUCTURE_ID: {
                        id: '#BLIND_STRUCTURE_ID',
                        prefix: 'blindStructureId',
                        changeEventHandler: _.blindStructureIdChangeHandle,
                        commonHideClass: ".blindStructureIdCommon",
                    },
                    TIMER_TOURNAMENT_END_TIME: {
                        id: '#TIMER_TOURNAMENT_END_TIME',
                        prefix: 'timerTournamentEndTime',
                        // changeEventHandler: _.timerTournamentEndTimeChangeHandle,
                        commonHideClass: ".timerTournamentEndTimeCommon",
                    },
                    TOURNAMENT_LEVEL: {
                        id: '#TOURNAMENT_LEVEL',
                        prefix: 'tournamentLevel',
                        changeEventHandler: _.tournamentLevelChangeHandle,
                        commonHideClass: ".tournamentLevelCommon",
                    }
                }
            },
            TimeSettingsSection: {},
            RebuyAddonReEntrySection: {
                id: '#RebuyAddonReEntrySection',
                prefix: 'RebuyAddonReEntry',
                children: {
                    RebuyAddonReEntry: {
                        id: '#REBUY_ADDON_RE_ENTRY_NONE, #REBUY_ADDON_RE_ENTRY_REBUY_ADDON, #REBUY_ADDON_RE_ENTRY',
                        prefix: 'RebuyAddonReEntry',
                        changeEventHandler: _.RebuyAddonReEntryChangeHandle,
                        commonHideClass: ".RebuyAddonReEntryCommon",
                        conditionsShowHide: {
                            show: [],
                            hide: [{
                                val: "9",
                                selector: "#BountyAmountWrapper, #BountyEntryFeeWrapper, #ProgressiveBountyPercentageWrapper"
                            }]
                        },

                    }
                }
            },
            PrizeStructureSection: {
                id: '#PrizeStructureSection',
                prefix: 'PrizeStructure',
                children: {
                    PRIZE_STRUCTURE_ID: {
                        id: '#PRIZE_STRUCTURE_ID_CUSTOM, #PRIZE_STRUCTURE_ID_DEFAULT, #PRIZE_STRUCTURE_ID_CUSTOM_MIX',
                        prefix: 'prizeStructureId',
                        changeEventHandler: _.prizeStructureIdChangeHandle,
                        commonHideClass: ".prizeStructureIdCommon",
                    },
                    NO_OF_WINNERS_CUSTOM: {
                        id: '#NO_OF_WINNERS_CUSTOM',
                        prefix: 'noOfWinnersCustom',
                        changeEventHandler: _.noOfWinnersCustomChangeHandle,
                        commonHideClass: ".noOfWinnersCustomCommon",
                    },
                    NO_OF_WINNERS_CUSTOM_MIX: {
                        id: '#NO_OF_WINNERS_CUSTOM_MIX',
                        prefix: 'noOfWinnersCustomMix',
                        changeEventHandler: _.noOfWinnersCustomMixChangeHandle,
                        commonHideClass: ".noOfWinnersCustomMixCommon",
                    },
                }
            }
        }
    }

    initEvents() {
        const _ = this;

        $.each(_.formSections, (index, object) => {
            if (object.hasOwnProperty('children')) {
                $.each(object.children, (ind, obj) => {
                    if (obj.hasOwnProperty('clickEventHandler')) $(document).on('click', `.${obj.prefix}Event`, e => obj.clickEventHandler.call(_, e, obj));
                    if (obj.hasOwnProperty('changeEventHandler')) $(document).on('change', `.${obj.prefix}Event`, e => obj.changeEventHandler.call(_, e, obj));
                });
            }
        });
        _.confirmationEvents();

        $(document).on('focus', 'input[type=number]', function(e) {
            $(this).on('wheel.disableScroll', function(e) {
                e.preventDefault()
            })
        })
        $(document).on('blur', 'input[type=number]', function(e) {
            $(this).off('wheel.disableScroll')
        })
        $(document).on('change', `[name^="WINNER_PERCENTAGE"]`, (e) => {
            const { formId } = this.options;
            let sum = 0;
            $('#prizeAdditionalFieldsCustomError').remove();
            $(`#${formId}`).removeClass('invalidateTotal');
            if ($(document).find(`[name^="WINNER_PERCENTAGE"]`).length > 0) {
                $(document).find(`[name^="WINNER_PERCENTAGE"]`).each((i, e) => {
                    sum += +$(e).val();
                })
                if (sum != 100) {
                    $(`#${formId}`).addClass('invalidateTotal');
                    $(`#prizeAdditionalFieldsCustom`).append(`<div id="prizeAdditionalFieldsCustomError"><label class="error">Total should equals to 100.</label></div>`)
                }
            }
        });
    }

    showHideOnConditions(e, obj) {
        $(obj.commonHideClass).hide();
        var $flag = false;
        $.each(obj.conditionsShowHide.hide, (i, v) => {
            if (v.val == $(obj.id).val()) {
                $(v.selector).hide();
                $flag = true;
            }
        });

        if (!$flag) $(obj.commonHideClass).show();

        $.each(obj.conditionsShowHide.show, (i, v) => {
            if (v.val == $(obj.id).val()) {
                $(v.selector).show();
            }
        });
    }

    setValuesOnConditions(e, obj) {
        let $this = $(obj.id);
        const { ifValue } = obj.setDefaultValues;

        // To assign values pre (Means assign values before all the condition or if conditions)
        ifValue.forEach((value, index) => {
            if (value.hasOwnProperty('*')) {
                value['*'].forEach((v, i) => {
                    this.setValueSelector(v, i);
                });
            }
        });
        ifValue.forEach((value, index) => {
            if (value.hasOwnProperty($this.val())) {
                value[$this.val()].forEach((v, i) => {
                    this.setValueSelector(v, i);
                });
            }
        });

        // To assign values post (Means assign values after all the condition or else condition)
        ifValue.forEach((value, index) => {
            if (value.hasOwnProperty('#')) {
                value['#'].forEach((v, i) => {
                    this.setValueSelector(v, i);
                });
            }
        });
    }

    /**
     * 
     * @param { string } v
     * ex: "#abc, #xyz, .all"
     * @param { int } i, index
     */
    setValueSelector(v, i) {
        const { tournamentData, finishInit } = this;
        if (Object.keys(tournamentData).length && !finishInit) {
            let selectorArray = v.selector.split(",").map(item => item.trim());
            selectorArray.forEach((val, ind) => {
                let name = $(val).attr('name');
                $(val).val(tournamentData[name]);
            });
        } else {
            $(v.selector).val(v.value);
        }
    }


    /**
     * 
     * @param { string } SelectorsString 
     * ex: "#abc, #xyz, .all"
     * @param { int|string } value 
     */
    setValueSelectorSplitString(SelectorsString, value) {
        const { tournamentData, finishInit } = this;
        if (Object.keys(tournamentData).length && !finishInit) {
            let selectorArray = SelectorsString.split(",").map(item => item.trim());
            selectorArray.forEach((val, ind) => {
                let name = $(val).attr('name');
                $(val).val(tournamentData[name]);
                $(val).data('isChanged', true);
            });
        } else {
            $(SelectorsString).val(value);
            $(SelectorsString).data('isChanged', true);
        }
    }

    CategoryChangeHandle(e, obj) {
        const { tournamentData: { CATEGORY }, finishInit } = this;

        if (CATEGORY && !finishInit) $(obj.id).val(CATEGORY);

        if (obj.hasOwnProperty('conditionsShowHide')) this.showHideOnConditions(e, obj);
        if (obj.hasOwnProperty('setDefaultValues')) this.setValuesOnConditions(e, obj);

        $('#BUYIN').attr('readonly', false);
        $('#ENTRY_FEE').attr('readonly', false);

        if ($(obj.id).val() == 99) {
            $('#BUYIN').attr('readonly', true);
            $('#ENTRY_FEE').attr('readonly', true);
        }
    }

    privateTableChangeHandle(e, obj) {
        const { tournamentData: { PRIVATE_TABLE, PASSWORD }, finishInit } = this;

        if (PRIVATE_TABLE && !finishInit) $(obj.id).val(PRIVATE_TABLE);

        if (obj.hasOwnProperty('conditionsShowHide')) this.showHideOnConditions(e, obj);
        if (obj.hasOwnProperty('setDefaultValues')) this.setValuesOnConditions(e, obj);

        if (PRIVATE_TABLE && PASSWORD && !finishInit) {
            if (PRIVATE_TABLE == 1) $("#PASSWORD").val("XXXXXXXX");
        }

    }

    tournamentTypeIdChangeHandle(e, obj) {
        const { finishInit, options: { wizardId }, tournamentData: { TOURNAMENT_TYPE_ID } } = this;
        if (TOURNAMENT_TYPE_ID && !finishInit) $(obj.id).val(TOURNAMENT_TYPE_ID);
        if (obj.hasOwnProperty('conditionsShowHide')) this.showHideOnConditions(e, obj);
        if (obj.hasOwnProperty('setDefaultValues')) this.setValuesOnConditions(e, obj);

        let tournament_type_id = $(obj.id).val();

        if (tournament_type_id == 4) { // Normal
            $(`#${wizardId}`).bootstrapWizard('display', $('#EntryCriteriaSection').data('wizardIndex'));
        } else if (tournament_type_id == 9) { // Multi-Day Main
            $(`#${wizardId}`).bootstrapWizard('hide', $('#EntryCriteriaSection').data('wizardIndex'));
        } else if (tournament_type_id == 10) { // Normal Bounty
            $(`#${wizardId}`).bootstrapWizard('display', $('#EntryCriteriaSection').data('wizardIndex'));
        } else if (tournament_type_id == 11) { // Progressive Bounty 
            $(`#${wizardId}`).bootstrapWizard('display', $('#EntryCriteriaSection').data('wizardIndex'));
        } else if (tournament_type_id == 12) { // Offline
            $(`#${wizardId}`).bootstrapWizard('display', $('#EntryCriteriaSection').data('wizardIndex'));
        } else if (tournament_type_id == 1) {
            $(`#${wizardId}`).bootstrapWizard('display', $('#EntryCriteriaSection').data('wizardIndex'));
        }
    }

    tournamentSubTypeIdChangeHandle(e, obj) {
        const { finishInit, options: { wizardId }, tournamentData: { TOURNAMENT_SUB_TYPE_ID } } = this;
        if (TOURNAMENT_SUB_TYPE_ID && !finishInit) $(obj.id).val(TOURNAMENT_SUB_TYPE_ID);

        if (obj.hasOwnProperty('conditionsShowHide')) this.showHideOnConditions(e, obj);
        if (obj.hasOwnProperty('setDefaultValues')) this.setValuesOnConditions(e, obj);

        let tournament_sub_type_id = $(obj.id).val();
        var tournament_type_id = $("#TOURNAMENT_TYPE_ID").val();

        $(`#${wizardId}`).bootstrapWizard('display', $('#EntryCriteriaSection').data('wizardIndex'));
        if (tournament_sub_type_id == 1) {
            if (tournament_type_id == 9) {
                $('#BountyAmountWrapper, #BountyEntryFeeWrapper, #ProgressiveBountyPercentageWrapper').hide();
                $(`#${wizardId}`).bootstrapWizard('hide', $('#EntryCriteriaSection').data('wizardIndex'))
            } else if (tournament_type_id == 10) {
                $('#BountyAmountWrapper, #BountyEntryFeeWrapper').show();
                $('#ProgressiveBountyPercentageWrapper').hide();
            } else if (tournament_type_id == 11) {
                $('#BountyAmountWrapper, #BountyEntryFeeWrapper, #ProgressiveBountyPercentageWrapper').show();
            } else {
                $('#BountyAmountWrapper, #BountyEntryFeeWrapper, #ProgressiveBountyPercentageWrapper').hide();
            }
        }
    }

    playerPerTableChangeHandle(e, obj) {
        const { finishInit, tournamentData: { PLAYER_PER_TABLE } } = this;

        if (PLAYER_PER_TABLE && !finishInit) {
            $(`.${obj.prefix}Event[name="PLAYER_PER_TABLE"]`).val(PLAYER_PER_TABLE);
        }
    }

    lateRegistrationAllowChangeHandle(e, obj) {
        const { finishInit, options: { wizardId }, tournamentData: { LATE_REGISTRATION_ALLOW } } = this;
        if (LATE_REGISTRATION_ALLOW && !finishInit) {
            $(`.${obj.prefix}Event[name="LATE_REGISTRATION_ALLOW"]`).prop('checked', false);
            $(`.${obj.prefix}Event[name="LATE_REGISTRATION_ALLOW"][value="${LATE_REGISTRATION_ALLOW}"]`).prop('checked', true);
        }

        let str = $(`.${obj.prefix}Event:checked`).val();
        // $('#LATE_REGISTRATION_END_TIME').val(0);
        if (str == 1) {
            $('#lateRegistrationEndTimeWrapper').show();
        } else {
            $('#lateRegistrationEndTimeWrapper').hide();
        }
    }

    RebuyAddonReEntryChangeHandle(e, obj) {
        const { finishInit, options: { wizardId, IsCreate }, tournamentData: { REBUY_ADDON_RE_ENTRY, REBUY_ELIGIBLE_CHIPS } } = this;
        if (REBUY_ADDON_RE_ENTRY && !finishInit) {
            $(`[name="REBUY_ADDON_RE_ENTRY"]`).prop('checked', false);
            $(`[name="REBUY_ADDON_RE_ENTRY"][value="${REBUY_ADDON_RE_ENTRY}"]`).prop('checked', true);
        } else {
            $('#DOUBLE_REBUYIN').prop('checked', false);
            $('#REBUY_COUNT').val(9999);
            $('#REBUY_END_TIME').val('');
        }

        let str = $(`.${obj.prefix}Event:checked`).val();

        if (IsCreate) this.setValueSelectorSplitString("#REBUY_CHIPS,#REBUY_ELIGIBLE_CHIPS,#REBUY_IN,#REBUY_ENTRY_FEE,#ADDON_CHIPS,#ADDON_BREAK_TIME,#ADDON_AMOUNT,#ADDON_ENTRY_FEE", "");

        if (str == 1) {
            $("#REBUY_ADDON_RE_ENTRY_REBUY_ADDON").prop('checked', true);
            $("#ReBuySettingsWrapper, #AddonSettingsWrapper, #DoubleReBuyInWrapper").show();
            $("#REBUY_COUNT").prop('disabled', false);
            $("#REBUY_ELIGIBLE_CHIPS").prop('readonly', false);
            $("#REBUY_ELIGIBLE_CHIPS").val("");
        } else if (str == 2) {
            $("#AddonSettingsWrapper, #DoubleReBuyInWrapper").hide();
            $("#ReBuySettingsWrapper").show();

            $("#REBUY_ADDON_RE_ENTRY_REENRY").prop('checked', true);
            $("#REBUY_ELIGIBLE_CHIPS").prop('readonly', true);
            $("#REBUY_COUNT").prop('disabled', false);
            $("#REBUY_ELIGIBLE_CHIPS").val(0);
        } else if (str == 0) {
            $("#ReBuySettingsWrapper, #AddonSettingsWrapper").hide();
            $("#DoubleReBuyInWrapper").show();

            if (IsCreate) $("#REBUY_COUNT").prop('disabled', false);
            $("#REBUY_ADDON_RE_ENTRY_NONE").prop('checked', true);
            $("#REBUY_ELIGIBLE_CHIPS").prop('readonly', false);
            $("#REBUY_ELIGIBLE_CHIPS").val("");
        }
        if (REBUY_ELIGIBLE_CHIPS && !finishInit) $("#REBUY_ELIGIBLE_CHIPS").val(REBUY_ELIGIBLE_CHIPS);
    }

    resetPrizeStructures() {
        const { finishInit, options: { wizardId, IsCreate }, tournamentData: { REBUY_ADDON_RE_ENTRY } } = this;

        var tournament_type_id = $("#TOURNAMENT_TYPE_ID").val();
        var tournament_sub_type_id = $("#TOURNAMENT_SUB_TYPE_ID").val();

        if (!finishInit) {
            $('.prizeStructureIdEvent').trigger('change');
        } else if (IsCreate) {
            $('#NO_OF_WINNERS_CUSTOM, #NO_OF_WINNERS_CUSTOM_MIX').val('').trigger('change');
            $('#prizeAdditionalFieldsCustom,#prizeAdditionalFieldsCustomMix').html('');
            $('#PRIZE_STRUCTURE_ID_DEFAULT').prop('checked', true).trigger('change');
        }

        if (tournament_sub_type_id == 2) {
            // SATELLITE TOURNAMENT
            $('#PrizeStructureWrapper, #PrizeStructureTypeWrapper, #GTDPrizePoolWrapper, #PrizePoolTypeWrapper, #QualifyingPlayersWrapper').hide();
            $('#PrizeBalanceTypeWrapper, #GTDSeatsWrapper').show();
            $(`#${wizardId}`).bootstrapWizard('display', $('#RebuyAddonReEntrySection').data('wizardIndex'));
        } else if (tournament_sub_type_id == 4) {
            // TIMER TOURNAMENT 
            $('#PrizeStructureWrapper, #PrizeStructureTypeWrapper, #PrizePoolTypeWrapper, #QualifyingPlayersWrapper, #GTDSeatsWrapper, #ReBuySettingsWrapper, #AddonSettingsWrapper').hide();
            $('#PrizeBalanceTypeWrapper, #GTDPrizePoolWrapper').show();
            $(`#${wizardId}`).bootstrapWizard('hide', $('#RebuyAddonReEntrySection').data('wizardIndex'));
        } else if (tournament_sub_type_id == 3) {
            // MULTI DAY CHILD 
            $('#PrizeStructureWrapper, #PrizeStructureTypeWrapper, #GTDPrizePoolWrapper, #PrizePoolTypeWrapper, #GTDSeatsWrapper').hide();
            $('#PrizeBalanceTypeWrapper, #QualifyingPlayersWrapper').show();
            $(`#${wizardId}`).bootstrapWizard('display', $('#RebuyAddonReEntrySection').data('wizardIndex'));
        } else {
            // NORMAL 
            $('#PrizeStructureWrapper, #PrizeStructureTypeWrapper, #PrizeBalanceTypeWrapper, #GTDPrizePoolWrapper, #PrizePoolTypeWrapper').show();
            $('#QualifyingPlayersWrapper, #GTDSeatsWrapper').hide();
            $(`#${wizardId}`).bootstrapWizard('display', $('#RebuyAddonReEntrySection').data('wizardIndex'));
        }

        if (tournament_type_id == 9) {
            // MULTI DAY MAIN
            $('#PrizeStructureWrapper, #PrizeStructureTypeWrapper, #QualifyingPlayersWrapper, #GTDSeatsWrapper, #ReBuySettingsWrapper, #AddonSettingsWrapper').hide();
            $('#PrizeBalanceTypeWrapper, #GTDPrizePoolWrapper, #PrizePoolTypeWrapper').show();
            $(`#${wizardId}`).bootstrapWizard('hide', $('#RebuyAddonReEntrySection').data('wizardIndex'));
        }

        /**
         * Call showReBuyBlock Method for values tournament_sub_type_id = 2,3,4, and all other also 
         * Call showReBuyBlock Method for values tournament_type_id = 9
         * showReBuyBlock is changed with $(`#REBUY_ADDON_RE_ENTRY_NONE`).prop('checked', true).trigger('change');
         */
        // this.showReBuyBlock(0);
        if (REBUY_ADDON_RE_ENTRY && !finishInit) {
            $(`[name="REBUY_ADDON_RE_ENTRY"]`).prop('checked', false);
            $(`[name="REBUY_ADDON_RE_ENTRY"][value="${REBUY_ADDON_RE_ENTRY}"]`).prop('checked', true);
        } else if (IsCreate) {
            $(`#REBUY_ADDON_RE_ENTRY_NONE`).prop('checked', true).trigger('change');
        }
    }

    prizeStructureIdChangeHandle(e, obj) {
        const { finishInit, options: { wizardId, IsCreate }, tournamentData: { PRIZE_STRUCTURE_ID, NO_OF_WINNERS_CUSTOM, NO_OF_WINNERS_CUSTOM_MIX, GUARENTIED_PRIZE, SATELLITES_GUARANTEED_PLACES_PAID, MULTIDAY_PLAYER_PERCENTAGE } } = this;
        if (PRIZE_STRUCTURE_ID && !finishInit) {
            $(`.${obj.prefix}Event[name="PRIZE_STRUCTURE_ID"]`).prop('checked', false);
            $(`.${obj.prefix}Event[name="PRIZE_STRUCTURE_ID"][value="${PRIZE_STRUCTURE_ID}"]`).prop('checked', true);
            $('#NO_OF_WINNERS_CUSTOM').val(NO_OF_WINNERS_CUSTOM);
            $('#NO_OF_WINNERS_CUSTOM_MIX').val(NO_OF_WINNERS_CUSTOM_MIX);
            $('#GUARENTIED_PRIZE').val(GUARENTIED_PRIZE);
            $('#SATELLITES_GUARANTEED_PLACES_PAID').val(SATELLITES_GUARANTEED_PLACES_PAID);
            $('#MULTIDAY_PLAYER_PERCENTAGE').val(MULTIDAY_PLAYER_PERCENTAGE);
        }

        let str = $(`.${obj.prefix}Event:checked`).val();

        $("#noOfWinnersCustomWrapper,#noOfWinnersCustomMixWrapper,#PrizeStructureTypeWrapper").hide();

        if (str == 1) {
            $("#noOfWinnersCustomMixWrapper,#PrizeStructureTypeWrapper,#prizeAdditionalFieldsCustomMix").hide();
            $("#noOfWinnersCustomWrapper").show();
            this.setValueSelectorSplitString('#NO_OF_WINNERS_CUSTOM_MIX, #GUARENTIED_PRIZE', '');
            $('#prizeAdditionalFieldsCustomMix').html('');
            $('#NO_OF_WINNERS_CUSTOM').trigger('change');
        } else if (str == 2) {
            $("#noOfWinnersCustomWrapper, #noOfWinnersCustomMixWrapper, #prizeAdditionalFieldsCustom, #prizeAdditionalFieldsCustomMix").hide();
            $("#PrizeStructureTypeWrapper").show()
            this.setValueSelectorSplitString('#NO_OF_WINNERS_CUSTOM, #NO_OF_WINNERS_CUSTOM_MIX', '');
            $('#NO_OF_WINNERS_CUSTOM, #NO_OF_WINNERS_CUSTOM_MIX').trigger('change');
            $('#prizeAdditionalFieldsCustom,#prizeAdditionalFieldsCustomMix').html('');
        } else if (str == 4) {
            $("#noOfWinnersCustomWrapper, #PrizeStructureTypeWrapper, #prizeAdditionalFieldsCustom").hide();
            $("#noOfWinnersCustomMixWrapper").show();
            this.setValueSelectorSplitString('#NO_OF_WINNERS_CUSTOM, #GUARENTIED_PRIZE', '');
            $('#prizeAdditionalFieldsCustomMix').html('');
            $('#NO_OF_WINNERS_CUSTOM_MIX').trigger('change');
        }
    }

    coinTypeIdChangeHandle(e, obj) {
        const { finishInit, options: { wizardId }, tournamentData: { COIN_TYPE_ID, REBUY_ADDON_RE_ENTRY, DEPOSIT_BALANCE_ALLOW, PROMO_BALANCE_ALLOW, WIN_BALANCE_ALLOW } } = this;

        if (COIN_TYPE_ID && !finishInit) {
            $(`.${obj.prefix}Event[name="COIN_TYPE_ID"]`).prop('checked', false);
            $(`.${obj.prefix}Event[name="COIN_TYPE_ID"][value="${COIN_TYPE_ID}"]`).prop('checked', true);
        }

        let str = $(`.${obj.prefix}Event:checked`).val();

        if (str == 8) {
            $('#BalanceTypeWrapper, #EntryCriteriaAmountWrapper').hide();
            $('#DEPOSIT_BALANCE_ALLOW').prop('checked', true).prop('disabled', true);
            $('#PROMO_BALANCE_ALLOW').prop('checked', true).prop('disabled', true);
            $('#WIN_BALANCE_ALLOW').prop('checked', true).prop('disabled', true);
            this.setValueSelectorSplitString('#BUYIN', '1');
            this.setValueSelectorSplitString('#ENTRY_FEE, #BOUNTY_AMOUNT, #BOUNTY_ENTRY_FEE, #PROGRESSIVE_BOUNTY_PERCENTAGE', '0');
            $(`#${wizardId}`).bootstrapWizard('hide', $('#RebuyAddonReEntrySection').data('wizardIndex'));
        } else {
            $('#BalanceTypeWrapper, #EntryCriteriaAmountWrapper').show();
            $('#DEPOSIT_BALANCE_ALLOW').prop('checked', true).prop('disabled', false);
            $('#PROMO_BALANCE_ALLOW').prop('checked', true).prop('disabled', false);
            $('#WIN_BALANCE_ALLOW').prop('checked', true).prop('disabled', false);
            this.setValueSelectorSplitString('#BUYIN, #ENTRY_FEE, #BOUNTY_AMOUNT, #BOUNTY_ENTRY_FEE, #PROGRESSIVE_BOUNTY_PERCENTAGE', '0');
            $(`#${wizardId}`).bootstrapWizard('display', $('#RebuyAddonReEntrySection').data('wizardIndex'));
            $('#CATEGORY').trigger('change');
        }
        if (REBUY_ADDON_RE_ENTRY && !finishInit) {
            $(`[name="REBUY_ADDON_RE_ENTRY"]`).prop('checked', false);
            $(`[name="REBUY_ADDON_RE_ENTRY"][value="${REBUY_ADDON_RE_ENTRY}"]`).prop('checked', true);
        } else {
            $(`#REBUY_ADDON_RE_ENTRY_NONE`).prop('checked', true).trigger('change');
        }

        if (!finishInit) {
            $(`[name="DEPOSIT_BALANCE_ALLOW"]`).prop('checked', false);
            $(`[name="PROMO_BALANCE_ALLOW"]`).prop('checked', false);
            $(`[name="WIN_BALANCE_ALLOW"]`).prop('checked', false);
            if (DEPOSIT_BALANCE_ALLOW) {
                $(`[name="DEPOSIT_BALANCE_ALLOW"][value="${DEPOSIT_BALANCE_ALLOW}"]`).prop('checked', true);
            }
            if (PROMO_BALANCE_ALLOW) {
                $(`[name="PROMO_BALANCE_ALLOW"][value="${PROMO_BALANCE_ALLOW}"]`).prop('checked', true);
            }
            if (WIN_BALANCE_ALLOW) {
                $(`[name="WIN_BALANCE_ALLOW"][value="${WIN_BALANCE_ALLOW}"]`).prop('checked', true);
            }
        }
    }

    /**
     * This function is used Fetch all the tournament blind structures
     * also get tournament blind level rebuy
     * call ajax in given function
     * @param {*} e 
     * @param {*} obj 
     */
    blindStructureIdChangeHandle(e, obj) {
        const { finishInit, options: { wizardId }, tournamentData: { BLIND_STRUCTURE_ID } } = this;

        if (BLIND_STRUCTURE_ID && !finishInit) {
            $(`.${obj.prefix}Event[name="BLIND_STRUCTURE_ID"]`).val(BLIND_STRUCTURE_ID);
        }

        let $this = $(e.target);
        let $val = $this.val();

        $('#TOURNAMENT_LEVEL').html(`<option value=""> Fetching Tournament Levels... </option>`);
        if ($.trim($val) != "") {

            let $data = {
                BLIND_STRUCTURE_ID: $val,
            }

            this.ajaxCall(`${window.pageData.baseUrl}/tournaments/api/tournamentBlindStructure`, $data, { refObj: obj })
                .then((tbsResponse) => {
                    let optionsList = `<option value=""> Select Blind Level </option>`;
                    tbsResponse.data.forEach((v, i) => {
                        optionsList += `<option value="${v.TOURNAMENT_LEVEL}">${v.TOURNAMENT_LEVEL} - ${v.SMALL_BLIND}/${v.BIG_BLIND} (${v.ANTE})</option>`
                    });
                    $('#TOURNAMENT_LEVEL').html(optionsList);
                    $('#TOURNAMENT_LEVEL').val("").trigger('change');
                });
        }
    }

    /**
     * This function is used Fetch tournamentLevelChangeHandle
     * call ajax in given function
     * @param {*} e 
     * @param {*} obj 
     */
    tournamentLevelChangeHandle(e, obj) {
        const { finishInit, options: { wizardId }, tournamentData: { TOURNAMENT_LEVEL, REBUY_END_TIME } } = this;

        if (TOURNAMENT_LEVEL && (!finishInit || !$(`.${obj.prefix}Event[name="TOURNAMENT_LEVEL"]`).data('isChanged'))) {
            $(`.${obj.prefix}Event[name="TOURNAMENT_LEVEL"]`).val(TOURNAMENT_LEVEL);
        }
        $(`.${obj.prefix}Event[name="TOURNAMENT_LEVEL"]`).data('isChanged', true);

        let $this = $(e.target);
        let $val = $this.val();
        $('#REBUY_END_TIME').html(`<option value=""> Fetching Registration Starts Hour... </option>`);

        let $data = {
            BLIND_STRUCTURE_ID: $('#BLIND_STRUCTURE_ID').val(),
            TOURNAMENT_LEVEL: $('#TOURNAMENT_LEVEL').val()
        }

        this.ajaxCall(`${window.pageData.baseUrl}/tournaments/api/tournamentBlindLevelRebuy`, $data, { refObj: obj })
            .then((tblResponse) => {
                let optionsList = `<option value=""> Select Registration Starts Hour </option>`;
                let index = 2;
                tblResponse.data.forEach((v, i) => {
                    optionsList += `<option value="${index}">${index} - ${v.SMALL_BLIND}/${v.BIG_BLIND} (${v.ANTE})</option>`;
                    index++
                });
                $('#REBUY_END_TIME').html(optionsList);
                if (REBUY_END_TIME && (!finishInit || !$(`[name="REBUY_END_TIME"]`).data('isChanged'))) {
                    $(`[name="REBUY_END_TIME"]`).val(REBUY_END_TIME);
                }
                $(`[name="REBUY_END_TIME"]`).data('isChanged', true);
            });

    }

    noOfWinnersCustomChangeHandle(e, obj) {
        const { finishInit, options: { wizardId }, tournamentData: { NO_OF_WINNERS_CUSTOM, WINNER_PERCENTAGE } } = this;
        if (NO_OF_WINNERS_CUSTOM && !finishInit) $(e.target).val(NO_OF_WINNERS_CUSTOM);

        let i = 0;
        let $val = parseInt($(e.target).val()) || 0;
        $('#prizeAdditionalFieldsCustom').html('');
        if ($val) {
            let field = null;
            let rowSection = $(`<div class="row mb-2"></div>`)
                .appendTo('#prizeAdditionalFieldsCustom');

            for (i = 0; i < $val; i++) {
                field = $(this.noOfWinnersCustomMarkupGenrator(i))
                    .appendTo(rowSection);
                if (WINNER_PERCENTAGE && !finishInit) field.find('input').val(WINNER_PERCENTAGE[i]);
            }
            $('#prizeAdditionalFieldsCustom').show();
        } else {
            $('#prizeAdditionalFieldsCustom').hide();
        }
    }

    noOfWinnersCustomMixChangeHandle(e, obj) {
        const { finishInit, options: { wizardId }, tournamentData: { NO_OF_WINNERS_CUSTOM_MIX, CUSTOM_MIX_PRIZE_TYPE, PRIZE_VALUE, PRIZE_TOURNAMENT_NAME } } = this;
        if (NO_OF_WINNERS_CUSTOM_MIX && !finishInit) $(e.target).val(NO_OF_WINNERS_CUSTOM_MIX);

        let i = 0;
        let $val = parseInt($(e.target).val()) || 0;
        $('#prizeAdditionalFieldsCustomMix').html('');
        if ($val) {
            let field = null;
            for (i = 0; i < $val; i++) {
                field = $(this.noOfWinnersCustomMixMarkupGenrator(i))
                    .appendTo('#prizeAdditionalFieldsCustomMix');
                if (CUSTOM_MIX_PRIZE_TYPE) {
                    field.find(`input[type="radio"][value="${CUSTOM_MIX_PRIZE_TYPE[i]}"]`).prop('checked', true).trigger('change');
                    field.find('input[type="text"]').val(PRIZE_VALUE[i]);
                    field.find('select').val(PRIZE_TOURNAMENT_NAME[i]);
                }
            }
            $('#prizeAdditionalFieldsCustomMix').show();
        } else {
            $('#prizeAdditionalFieldsCustomMix').hide();
        }
    }

    noOfWinnersCustomMarkupGenrator(index) {

        return `<div class="form-group col-md-2">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">${index + 1}</span>
                        </div>
                        <input type="number" min="1" max="100" name="WINNER_PERCENTAGE[${index}]" class="form-control">
                        <span class="input-group-addon bootstrap-touchspin-postfix input-group-append"><span class="btn btn-primary bootstrap-touchspin-up">%</span></span>
                    </div>
                </div>`;
    }

    noOfWinnersCustomMixMarkupGenrator(index) {
        let { satalliteTournaments } = this.options;
        satalliteTournaments = JSON.parse(atob(satalliteTournaments));
        let optionString = `<option value="">Select Tournament</option>`
        satalliteTournaments.forEach((v, i) => {
            optionString += `<option value="${$.trim(v.TOURNAMENT_NAME)}">${v.TOURNAMENT_START_TIME} - (${v.TOURNAMENT_ID})${v.TOURNAMENT_NAME} - ₹${v.TOT_BUYIN}</option>`;
        });
        let random = Math.floor(Math.random() * 9999) + 1;

        return `<div class="row">
                <div class="col-md-4">
                    <div class="form-group ">
                        <div class="radio radio-info form-check-inline ml-2">
                            <input type="radio" id="amount${random}" value="1" data-target-class="customMixMarkUp${random}_" name="CUSTOM_MIX_PRIZE_TYPE[${index}]" checked>
                            <label for="amount${random}"> Amount </label>
                        </div>
                        <div class="radio radio-info form-check-inline">
                            <input type="radio" id="ticket${random}"  value="8" data-target-class="customMixMarkUp${random}_" name="CUSTOM_MIX_PRIZE_TYPE[${index}]">
                            <label for="ticket${random}"> Ticket </label>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-4 customMixMarkUp${random}_1 boxhideshow">
                    <input type="text" name="PRIZE_VALUE[${index}]" class="form-control">
                </div>
                <div class="form-group col-md-4 customMixMarkUp${random}_8 boxhideshow" style="display:none;">
                    <select class="form-control" name="PRIZE_TOURNAMENT_NAME[${index}]">${optionString}</select>
                </div>
            </div>`;
    }

    /**
     * @param {*} $url link to call ajax
     * @param {*} $data $data is passed as json object or formData 
     * @param { method, refObj } $optional optional argument default value is post 
     */
    ajaxCall($url, $data, $optional = {}) {
        let $method = $optional.method || 'POST';
        let refObj = $optional.refObj || {};
        refObj.requestQueue = refObj.requestQueue || [];

        let xhr = $.ajax({
            url: $url,
            type: $method,
            data: $data,
            beforeSend: function(jqXHR, settings) {
                for (var i = 0; i < (refObj.requestQueue.length); i++) {
                    refObj.requestQueue[i].abort();
                }
            }
        }).fail((failCallback, errorStatus, errorMessage) => {
            if (errorStatus != "abort") {
                $.isPlainObject(failCallback.responseJSON) ? console.error(failCallback.responseJSON, errorStatus, errorMessage) : console.error(failCallback.responseText, errorStatus, errorMessage)
            }
        }).always(function(jqXHR, textStatus, errorThrown) {
            refObj.requestQueue.splice(0, refObj.requestQueue.length - 1);
        });
        refObj.requestQueue.push(xhr);

        return xhr;
    }

    /**
     * This function is used Fetch all the countTournamentBlindInfo
     * call ajax in given function
     * @param {*} e 
     * @param {*} obj 
     */
    async timerTournamentEndTimeChangeHandle(e, obj, value) {
        const { finishInit, options: { wizardId }, tournamentData: { TIMER_TOURNAMENT_END_TIME } } = this;

        if (TIMER_TOURNAMENT_END_TIME && !finishInit) {
            $(`.${obj.prefix}Event[name="TIMER_TOURNAMENT_END_TIME"]`).val(TIMER_TOURNAMENT_END_TIME);
        }

        let $this = $(e);
        let $val = $this.val();
        let $TOURNAMENT_LEVEL = null,
            $BLIND_STRUCTURE_ID = null,
            $result = false;
        if ($.trim($TOURNAMENT_LEVEL = $('#TOURNAMENT_LEVEL').val()) != "" && $.trim($BLIND_STRUCTURE_ID = $("#BLIND_STRUCTURE_ID").val()) != "") {
            let $data = {
                TOURNAMENT_LEVEL: $TOURNAMENT_LEVEL,
                BLIND_STRUCTURE_ID: $BLIND_STRUCTURE_ID
            }
            $result = new Promise((resolve, reject) => {
                this.ajaxCall(`${window.pageData.baseUrl}/tournaments/api/countTournamentBlindInfo`, $data, { refObj: obj })
                    .then((response) => {
                        if (response.status == 200) {
                            parseInt($val) <= parseInt(response.data) ? resolve(true) : reject("Timer Tournament End Level Should be less than or equal to Blind Level");
                        } else {
                            reject(reponse.message);
                        }
                    });
            });
        } else {
            $result = new Promise((resolve, reject) => {
                $('#TIMER_TOURNAMENT_END_TIME-error').remove();
                $('#TIMER_TOURNAMENT_END_TIME').addClass('error').removeClass('valid');
                $('#TIMER_TOURNAMENT_END_TIME').after(`<label id="TIMER_TOURNAMENT_END_TIME-error" class="error" for="TIMER_TOURNAMENT_END_TIME">Please Select TOURNAMENT LEVEL and BLIND STRUCTURE</label>`);
                reject("Please Select TOURNAMENT LEVEL and BLIND STRUCTURE");
            });

        }
        return $result;
    }
    jqueryValidationAdditionalMethods() {
        const _ = this;
        $.validator.addMethod("tournamentLevelCheck", function(value, element, params) {
            const obj = _.formSections.BlindStructureSection.children.TOURNAMENT_LEVEL;
            let promise = _.timerTournamentEndTimeChangeHandle(element, obj);
            promise.then((res) => {
                $('#TIMER_TOURNAMENT_END_TIME-error').remove();
                $('#TIMER_TOURNAMENT_END_TIME').addClass('valid').removeClass('error');
            }).catch((err) => {
                $('#TIMER_TOURNAMENT_END_TIME-error').remove();
                $('#TIMER_TOURNAMENT_END_TIME').addClass('error').removeClass('valid');
                $('#TIMER_TOURNAMENT_END_TIME').after(`<label id="TIMER_TOURNAMENT_END_TIME-error" class="error" for="TIMER_TOURNAMENT_END_TIME">${err}</label>`);
            })
            return "pending";
        }, 'Please Select TOURNAMENT LEVEL and BLIND STRUCTURE');

        $.validator.addMethod("greaterThanZero", function(value, element) {
            return this.optional(element) || (parseFloat(value) > 0);
        }, "Must Be Greater Than Zero");

        $.validator.addMethod("greaterThanZeroForRebuy", function(value, element) {
            let obj = _.formSections.RebuyAddonReEntrySection.children.RebuyAddonReEntry;
            let str = $(`.${obj.prefix}Event:checked`).val();

            if (str == 1) {
                return this.optional(element) || (parseFloat(value) > 0);
            }

            return true;
        }, "Must Be Greater Than Zero");

        $.validator.addMethod("checkmaxplayer", function(value, element) {
            return value <= $("#PLAYER_PER_TABLE").val();
        }, 'Players should be less than Players per table');

        $.validator.addMethod("checkmaxvsminplayer", function(value, element) {
            return parseInt(value) >= (parseInt($("#T_MIN_PLAYERS").val()));
        }, 'Maximum players should be greater than or equal to minimum player');

        $.validator.addMethod("checkstarchipvsrebuychips", function(value, element) {
            return parseInt(value) <= (parseInt($("#TOURNAMENT_CHIPS").val()));
        }, 'Player Max. Eligible Chips should be less than or equal to Starting Chips Count');

        $.validator.addMethod("dateAfterToday", function(value, element) {
            return moment(new Date(value)).isAfter(moment());
        }, 'Date time should be greater then Now');

        $.validator.addMethod("dateTimeAllowedBeforeMinute", function(value, element, params) {
            return moment(new Date(value)).isAfter(moment().subtract({ 'minutes': params }));
        }, `Date time should be greater then before :params minutes`);

        $.validator.addMethod("tournamentTimeGreaterThenReg", function(value, element) {
            return moment(new Date(value)).isSameOrAfter(new Date($('#REGISTER_START_TIME').val()));

        }, 'Tournament Date time should be after Registration Date time');
    }

    confirmationEvents() {
        const _ = this;
        $(document).on('click', ".deleteTemplete", (e) => {
            e.preventDefault();
            e.stopPropagation();
            let $this = $(e.target);

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $("#DeleteTemplateForm").attr('action', `${window.pageData.baseUrl}/tournaments/manage-tourneys/template/${$this.data('templateId')}`);
                    $("#DeleteTemplateForm").get(0).submit();
                }
            })
        });
    }
}