/**
 * Create Tournament 
 * 
 * We have use this class for Create Tournament
 * inherted Tournament class to use common methods
 * 
 * Class CreateTournament
 * 
 * @category    Manage Tournament
 * @package     Tournament
 * @copyright   PokerBaazi
 * @author      Nitin Sharma<nitin.sharma@moonshinetechnology.com>
 * Created At:  03/02/2020
 */
class CreateTournament extends Tournament {
    constructor(options = {}) {
        super(options);
        this.initCreateTournament();
    }
    initCreateTournament() {
        const _ = this;
        _.initCreateTournamentOption();
        _.initJQueryValidation();
        _.initCreateTournamentEvents();
        _.initBootstrapWizard();
        _.initCreateTournamentShowHideObject();
        _.setDateTimePicker();
    }
    triggerEventsOnInit() {
        $('#CATEGORY').trigger('change');
        $('#PRIVATE_TABLE').trigger('change');
        $('.coinTypeIdEvent:checked').trigger('change');
        $('#BLIND_STRUCTURE_ID').trigger('change');
        $('.RebuyAddonReEntryEvent:checked').trigger('change');
        $('.prizeStructureIdEvent:checked').trigger('change');
        $('#LATE_REGISTRATION_ALLOW').trigger('change');
    }
    initCreateTournamentOption() {
        let options = {
            formId: "createTournamentForm",
            wizardId: "createTournamentWizard",
            IsCreate: true
        }
        this.options = $.extend({}, this.options, $.isPlainObject(options) && options);
    }
    initCreateTournamentShowHideObject() {
        const _ = this;
        const {
            TournamentInformationSection: {
                children: {
                    CATEGORY,
                    TOURNAMENT_TYPE_ID,
                    TOURNAMENT_SUB_TYPE_ID,
                    PRIVATE_TABLE
                }
            },
            TimingSection: {

            },
            PlayerSection: {
                children: {
                    PLAYER_PER_TABLE
                }
            },
            EntryCriteriaSection: {
                children: {
                    COIN_TYPE_ID
                }
            },
            BlindStructureSection: {
                children: {
                    BLIND_STRUCTURE_ID,
                    TIMER_TOURNAMENT_END_TIME,
                    TOURNAMENT_LEVEL
                }
            },
            TimeSettingsSection,
            RebuyAddonReEntrySection: {
                children: {
                    RebuyAddonReEntry
                }
            },
            PrizeStructureSection: {
                children: {
                    PRIZE_STRUCTURE_ID,
                    NO_OF_WINNERS_CUSTOM,
                    NO_OF_WINNERS_CUSTOM_MIX
                }
            }
        } = _.formSections;

        CATEGORY.setDefaultValues = {
            ifValue: [{
                "99": [{
                    selector: "#BUYIN, #ENTRY_FEE",
                    value: 0
                }]
            }],

        };

        TOURNAMENT_TYPE_ID.setDefaultValues = {
            ifValue: [{
                // Same values set on 4, 9, 10, 11, 12
                "*": [{
                    selector: "#NO_OF_WINNERS_CUSTOM, #NO_OF_WINNERS_CUSTOM_MIX, #TournamentParentSatelliteIdWrapper, #TournamentParentMultiDayIdWrapper",
                    value: ''
                }, {
                    selector: "#TOURNAMENT_SUB_TYPE_ID",
                    value: 1
                }],
            }],

        };

        TOURNAMENT_SUB_TYPE_ID.setDefaultValues = {
            ifValue: [{
                // Same values set on 1, 2, 3, 4
                "*": [{
                    selector: "#NO_OF_WINNERS_CUSTOM, #NO_OF_WINNERS_CUSTOM_MIX",
                    value: ''
                }],
            }],

        };

        PRIVATE_TABLE.setDefaultValues = {
            ifValue: [{
                "*": [{
                    selector: "#PASSWORD",
                    value: ''
                }],
            }],

        };

        PLAYER_PER_TABLE.setDefaultValues = {
            ifValue: [{
                "*": [{
                    selector: "#T_MIN_PLAYERS",
                    value: 5
                }, {
                    selector: "#T_MAX_PLAYERS",
                    value: 1500
                }],
                "2": [{
                    selector: "#T_MIN_PLAYERS, #T_MAX_PLAYERS",
                    value: 2
                }],
            }]
        };

        RebuyAddonReEntry.setDefaultValues = {
            ifValue: [{
                "*": [{
                    selector: "#BOUNTY_AMOUNT, #BOUNTY_ENTRY_FEE, #PROGRESSIVE_BOUNTY_PERCENTAGE, #BUYIN, #ENTRY_FEE",
                    value: 0
                }]
            }],

        }
    }
    setDateTimePicker() {
        var $customDatePickerWrapper = $('.customDatePickerWrapper');

        let from = $(document).find(".customDatePicker.from");
        let fromFlatPicker = from.data('flatpickr');
        // fromFlatPicker.set('minDate', new Date());

        fromFlatPicker.set('onClose', function(selectedDates, dateStr, instance) {
            let minDate = new Date(dateStr);
            let customDatePickerFrom = $customDatePickerWrapper.find(".customDatePicker.to");
            var flatpickrFrom = customDatePickerFrom.data('flatpickr');
            flatpickrFrom.set("minDate", minDate);
        });

        let to = $(document).find(".customDatePicker.to");
        let toFlatPicker = to.data('flatpickr');
        toFlatPicker.set('minDate', new Date());

        toFlatPicker.set('onClose', function(selectedDates, dateStr, instance) {
            let maxDate = new Date(dateStr);
            // $customDatePickerWrapper.find(".customDatePicker.from").flatpickr('maxDate', maxDate);
            let customDatePickerTo = $customDatePickerWrapper.find(".customDatePicker.from");
            var flatpickrTo = customDatePickerTo.data('flatpickr');
            flatpickrTo.set("maxDate", maxDate);
        });
    }

    initCreateTournamentEvents() {
        const _ = this;
        const { options, options: { wizardId, formId }, jqueryValidationForm } = _;
        $(document).on('click', '.formSubmitButtom', (e) => {
            let $this = $(e.target);
            var $valid = $(`#${formId}`).valid();
            if (!$valid) {
                jqueryValidationForm.focusInvalid();
                return false;
            } else {
                $(document).find(`[name^="WINNER_PERCENTAGE"]`).eq(0).trigger('change');
                if ($(`#${formId}`).hasClass('invalidateTotal') && (+$(`#${formId}`).find(`[name="NO_OF_WINNERS_CUSTOM"]`).val()) > 0) {
                    return false;
                } else {

                    $('.formSubmitButtom').prop('disabled', true).append(`<span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>`);
                    if ($this.attr(`name`) == "saveButton") {
                        $(`#${formId}`).append(`<input type="hidden" name="saveButton" value="saveTemplate">`);
                    }
                    $(`#${formId}`).get(0).submit();
                }
            }
        });

        $(document).on('change', `.${_.formSections.RebuyAddonReEntrySection.children.RebuyAddonReEntry.prefix}Event`, function(e) {
            $("#REBUY_IN").val($("#BUYIN").val());
            $("#REBUY_ENTRY_FEE").val($("#ENTRY_FEE").val());
        });
    }
    initJQueryValidation() {
        const _ = this;
        const { formId } = this.options;
        _.jqueryValidationForm = $(`#${formId}`).validate({
            rules: {
                TOURNAMENT_NAME: {
                    required: true,
                    minlength: 4,
                    fieldTrim: true
                },
                TOURNAMENT_TYPE_ID: {
                    required: true
                },
                SERVER_ID: {
                    required: true
                },
                TOURNAMENT_DESC: {
                    required: true,
                    fieldTrim: true
                },
                CATEGORY: {
                    required: true
                },
                TOURNAMENT_LIMIT_ID: {
                    required: true
                },
                WIN_THE_BUTTON: {
                    required: true
                },
                TOURNAMENT_SUB_TYPE_ID: {
                    required: true
                },
                TOURNAMENT_PARENT_SATELLITE_ID: {
                    required: true
                },
                TOURNAMENT_PARENT_MULTI_DAY_ID: {
                    required: true
                },
                PASSWORD: {
                    required: function(element) {
                        return $("#PRIVATE_TABLE").val() == 1;
                    }
                },
                REGISTER_START_TIME: {
                    required: true,
                    // dateAfterToday: true,
                    // dateTimeAllowedBeforeMinute: 30,
                },
                TOURNAMENT_START_TIME: {
                    required: true,
                    dateAfterToday: true,
                    tournamentTimeGreaterThenReg: true
                },
                LATE_REGISTRATION_END_TIME: {
                    required: true,
                    digits: true,
                    min: 0,
                    max: 999
                },
                PLAYER_PER_TABLE: {
                    required: true
                },
                T_MIN_PLAYERS: {
                    required: true,
                    digits: true,
                    min: 2,
                },
                T_MAX_PLAYERS: {
                    required: true,
                    digits: true,
                    min: 2,
                    checkmaxvsminplayer: true
                },
                COIN_TYPE_ID: {
                    required: true,
                },
                BUYIN: {
                    required: true,
                    digits: true
                },
                ENTRY_FEE: {
                    required: true,
                    number: true
                },
                BOUNTY_AMOUNT: {
                    required: true,
                    number: true
                },
                BOUNTY_ENTRY_FEE: {
                    required: true,
                    number: true
                },
                PROGRESSIVE_BOUNTY_PERCENTAGE: {
                    required: true,
                    number: true
                },
                BLIND_STRUCTURE_ID: {
                    required: true
                },
                TOURNAMENT_LEVEL: {
                    required: true
                },
                LEVEL_PERIOD: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                TOURNAMENT_CHIPS: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                TIMER_TOURNAMENT_END_TIME: {
                    required: true,
                    tournamentLevelCheck: true,
                    digits: true,
                    min: 1
                },
                PLAYER_HAND_TIME: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                DISCONNECT_TIME: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                EXTRA_TIME: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                LOBBY_DISPLAY_INTERVAL: {
                    required: true,
                    min: 2
                },
                PLAYER_MAX_EXTRATIME: {
                    required: true,
                },
                ADDITIONAL_EXTRATIME_LEVEL_INTERVAL: {
                    required: true,
                },
                ADDITIONAL_EXTRATIME: {
                    required: true,
                },
                REBUY_CHIPS: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                REBUY_ELIGIBLE_CHIPS: {
                    required: true,
                    digits: true,
                    greaterThanZeroForRebuy: true,
                    checkstarchipvsrebuychips: true
                },
                REBUY_END_TIME: {
                    required: true,
                    digits: true
                },
                REBUY_COUNT: {
                    required: true,
                },
                REBUY_IN: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                REBUY_ENTRY_FEE: {
                    required: true,
                    digits: true,
                },
                ADDON_CHIPS: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                ADDON_BREAK_TIME: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                ADDON_AMOUNT: {
                    required: true,
                    digits: true,
                    greaterThanZero: true
                },
                ADDON_ENTRY_FEE: {
                    required: true,
                    digits: true,
                },
                PRIZE_STRUCTURE_ID: {
                    required: true,
                },
                PRIZE_STRUCTURE_ID: {
                    required: true,
                },
                PRIZE_STRUCTURE_TYPE_ID: {
                    required: true,
                },
                FIXED_PRIZE: {
                    required: true,
                },
                NO_OF_WINNERS_CUSTOM: {
                    required: true,
                    digits: true,
                    min: 1
                },
                NO_OF_WINNERS_CUSTOM_MIX: {
                    required: true,
                    digits: true,
                    min: 1
                },
                GUARENTIED_PRIZE: {
                    required: true,
                    digits: true,
                },
                SATELLITES_GUARANTEED_PLACES_PAID: {
                    required: true,
                    digits: true,
                },
                MULTIDAY_PLAYER_PERCENTAGE: {
                    required: true,
                    digits: true,
                },

            },
            errorPlacement: function(error, element) {
                $(element).removeClass('.is-invalid');
                $(element).next('.invalid-feedback').remove();
                error.insertAfter(element);
            }
        });
    }
    initBootstrapWizard() {
        const _ = this;
        const { options, options: { wizardId, formId }, jqueryValidationForm } = _;

        $(`#${wizardId}`).bootstrapWizard({
            'tabClass': options.tabClass,
            'nextSelector': options.nextSelector,
            'previousSelector': options.previousSelector,
            'firstSelector': options.firstSelector,
            'lastSelector': options.lastSelector,
            'finishSelector': options.finishSelector,
            onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index + 1;
                var $percent = ($current / $total) * 100;
                $(`#${formId}`).find('.bar').css({ width: $percent + '%' });
            },
            onTabClick: function($activeTab, $navigation, currentIndex, clickedIndex, $clickedTab) {
                if (currentIndex < clickedIndex) {
                    return false;
                } else {
                    this.showNextHideFinishButton();
                }
                if (1 == (clickedIndex + 1)) {
                    this.hidePrevButton();
                }
            },
            onTabChange: function($activeTab, $navigation, currentIndex, nextTab) {

            },
            onNext: function($activeTab, $navigation, nextIndex) {
                var $total = $navigation.find('li').length;
                var $current = nextIndex + 1;
                var $valid = $(`#${formId}`).valid();

                if (!$valid) {
                    jqueryValidationForm.focusInvalid();
                    return false;
                }

                this.showPrevButton();

                if ($total == $current) {
                    this.showFinishHideNextButton();
                }
                if (!$navigation.find(`li[data-wizard-index="${nextIndex}"]`).is(':visible')) {
                    $navigation.find(`li[data-wizard-index="${nextIndex}"]`).next().find('a').tab('show');
                    if ($total == (nextIndex + 2)) this.showFinishHideNextButton();
                    return false;
                }
            },
            onPrevious: function($activeTab, $navigation, previousIndex) {
                var $current = previousIndex + 1;
                this.showNextHideFinishButton();
                if (!$navigation.find(`li[data-wizard-index="${previousIndex}"]`).is(':visible')) {
                    $navigation.find(`li[data-wizard-index="${previousIndex}"]`).prev().find('a').tab('show');
                    return false;
                }
                if (1 == $current) {
                    this.hidePrevButton();
                }
            },
            onLast: function() {
                return false;
            },
            onFinish: function($activeTab, $navigation, lastIndex) {

            },
            onBack: function($activeTab, $navigation, formerIndex) {
                return false;
            },
            showNextHideFinishButton: function() {
                $(`#${wizardId}`).find(options.nextSelector).show();
                $(`#${wizardId}`).find(options.finishSelector).hide().prop('disabled', true);
            },
            showFinishHideNextButton: function() {
                $(`#${wizardId}`).find(options.finishSelector).show().prop('disabled', false);
                $(`#${wizardId}`).find(options.nextSelector).hide();
            },
            hidePrevButton: function() {
                $(`#${wizardId}`).find(options.previousSelector).hide().prop('disabled', true);
            },
            showPrevButton: function() {
                $(`#${wizardId}`).find(options.previousSelector).show().prop('disabled', false);
            }

        });
    }
    CategoryChangeHandle(e, obj) {
        super.CategoryChangeHandle(e, obj);
        $('#TOURNAMENT_TYPE_ID').trigger("change");
    }

    tournamentTypeIdChangeHandle(e, obj) {
        super.tournamentTypeIdChangeHandle(e, obj);
        $('#TOURNAMENT_SUB_TYPE_ID').trigger("change");
    }

    tournamentSubTypeIdChangeHandle(e, obj) {
        super.tournamentSubTypeIdChangeHandle(e, obj);
        this.resetPrizeStructures();
    }

    playerPerTableChangeHandle(e, obj) {
        super.playerPerTableChangeHandle(e, obj);
        let playerPerTable = $(`.${obj.prefix}Event[name="PLAYER_PER_TABLE"]`).val();

        if (obj.hasOwnProperty('setDefaultValues')) this.setValuesOnConditions(e, obj);

        if (playerPerTable == 2) {
            $('#T_MIN_PLAYERS').attr('readonly', true);
            $('#T_MAX_PLAYERS').attr('readonly', true);
        } else {
            $('#T_MIN_PLAYERS').attr('readonly', false);
            $('#T_MAX_PLAYERS').attr('readonly', false);
        }
    }

    resetPrizeStructures() {
        super.resetPrizeStructures();
    }
}