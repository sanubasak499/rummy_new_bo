(function($, global) {
    'use strict';

    class Permission {
        constructor(data = {}) {

        }

        initEvents() {
            var _ = this;
            // $(document).on('change', '.permission-checkbox', e => _.handlePermissionChange(e));
            $(document).on('change', '.permission-checkbox-edit', e => _.handlePermissionEdit(e));
            $(document).on('change', '.permission-checkbox-view', e => _.handlePermissionView(e));
        }

        handlePermissionChange(e) {
            var _ = this;
            var $e = $(e.target);
            var $p = $e.closest('.permission-list');

            let $parentSibling = $p.siblings('.permission-box');
            if ($p.find('.permission-checkbox:checked').length == 0) {
                $parentSibling.find('.permission-checkbox').prop('checked', false);
            } else if ($p.find('.permission-checkbox:checked').length > 0) {
                $parentSibling.find('.permission-checkbox-view').prop('checked', true);
            }
        }
        handlePermissionEdit(e) {
            var _ = this;
            var $e = $(e.target);
            var $p = $e.closest('.permission-box');
            var $pl = $p.siblings('.permission-list');
            if ($e.prop('checked') == true) {
                $p.find('.permission-checkbox-view').prop('checked', true);
                $pl.find('.permission-checkbox').prop('checked', true);
            } else {
                $pl.find('.permission-checkbox-edit').prop('checked', false);
            }
            _.handlePermissionChange(e);
        }
        handlePermissionView(e) {
            var _ = this;
            var $e = $(e.target);
            var $p = $e.closest('.permission-box');
            var $pl = $p.siblings('.permission-list');
            if ($e.prop('checked') == false) {
                $p.find('.permission-checkbox-edit').prop('checked', false);
                $pl.find('.permission-checkbox').prop('checked', false);
            } else {
                $pl.find('.permission-checkbox-view').prop('checked', true);
            }
            _.handlePermissionChange(e);
        }
        init() {
            this.initEvents();
        }
    }

    $.Permission = new Permission;
}(window.jQuery, window));

(function($) {
    "use strict";
    $.Permission.init();
}(window.jQuery));