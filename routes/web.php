<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// Auth::routes();
//Admin routes starts
Route::group(['namespace' => 'bo'], function () {
    // Only want login logout
    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login');
        Route::post('logout', 'LoginController@logout')->name('logout');
    });
}); //routes are not have admin middleware

// Start Rounting bo from here
Route::group(['namespace' => 'bo', 'middleware' => ['auth']], function () {
    /**
     * Web Icons
     * @example fontawesome
     * @author Nitin Sharma
     */
    Route::group(['prefix' => 'icons', 'as' => 'icons.'], function () {
        Route::view('fontawesome', 'bo.views.commonview.icons')->name('fontawesome');
    }); 

    /**
     * Web Api this api are used for global ajax calls
     * to use \Auth due to csrf token
     * @author Nitin Sharma
     */

    Route::group(['namespace' => 'Api', 'prefix' => 'WebApi', 'as' => 'webApi.'], function () {
        Route::post('isAdminUserNameExist', 'WebApiController@isAdminUserNameExist')->name('isAdminUserNameExist');
        Route::post('isAdminEmailExist', 'WebApiController@isAdminEmailExist')->name('isAdminEmailExist');
        Route::post('isAdminMobileExist', 'WebApiController@isAdminMobileExist')->name('isAdminMobileExist');
        Route::post('isModuleKeyExist', 'WebApiController@isModuleKeyExist')->name('isModuleKeyExist');
        Route::post('excelToArray', 'WebApiController@excelToArray')->name('excelToArray');
    });

    /**
     * @author Nitin Sharma
     */
    Route::group(['namespace' => 'dashboard'], function () {
        Route::view('/', 'bo.views.dashboard.welcome')->name('index');
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    });

    /**
     * Administration routes
     * Having Add Admins, Roles etc
     * @author Nitin sharma
     */
    Route::group(['namespace' => 'administration', 'prefix' => 'administration', 'as' => 'administration.'], function () {
        /**
         * Admin User Add, Edit view and other related routes
         * @author Nitin Sharma
         */
        Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
            // Route::get('search', 'AccountController@search')->name('search');
            Route::get('/', 'UserController@index')->name('index');
            Route::post('/', 'UserController@index')->name('filter');
            Route::get('/add-user', 'UserController@create')->name('create');
            Route::post('/add-user', 'UserController@store')->name('store');
            Route::get('/permissions/{id}', 'UserController@rolePermissions')->name('rolePermissions');
            Route::post('/permissions/{id}', 'UserController@rolePermissionsUpdate')->name('rolePermissionsUpdate');
            Route::get('/{id}', 'UserController@show')->name('show');
            Route::get('/{id}/edit', 'UserController@edit')->name('edit');
            Route::put('/{id}', 'UserController@update')->name('update');
        });

        /**
         * Role and Role Permission routes
         * @author Nitin Sharma
         */
        Route::group(['prefix' => 'role', 'as' => 'role.'], function () {
            Route::get('/', "RoleController@index")->name('index');
            Route::post('/', "RoleController@store")->name('store');
            Route::get('/{id}/edit', "RoleController@edit")->name('edit');
            Route::put('/{id}', "RoleController@update")->name('update');
            Route::group(['prefix' => 'permission', 'as' => 'permission.'], function () {
                Route::get('/{role_id}', "RolePermissionController@index")->name('index');
                Route::get('/show/{role_id}', "RolePermissionController@show")->name('show');
                Route::post('{role_id}', "RolePermissionController@update")->name('update');
            });
        });
    });

    /**
     * Author: Kamlesh Kumar
     * @author Nitin Sharma
     * Purpose: Player Search and Player/User profile routes
     * Created Date: 17/01/2020
     */
    Route::group(['namespace' => 'user', 'prefix' => 'user', 'as' => 'user.'], function () {
        Route::group(['namespace' => 'account', 'prefix' => 'account', 'as' => 'account.'], function () {
            Route::get('search', 'AccountController@index')->name('index');
            Route::post('search', 'AccountController@searchResult')->name('search_post');
            Route::get('detail/{user_id}', 'AccountController@userProfile')->name('user_profile');
            Route::post('responsiblegaming', 'AccountController@responsibleGaming')->name('responsible_gaming');
            Route::post('updateuserprofile', 'AccountController@updateuserprofile')->name('update_user');
            Route::post('getcity', 'AccountController@getCity')->name('get_city');
            Route::get('excelexport', 'AccountController@excelexport')->name('excel_export');
            Route::post('twostepverification','AccountController@twostepverification')->name('twostepverification');
            Route::post('updatepasswordexpiry','AccountController@updatepasswordexpiry')->name('updatepasswordexpiry');

            /**
             * Author: Kamlesh Kumar
             * Purpose: Player Search and Player/User profile routes
             * Created Date: 30/06/2020
             */
            Route::post('resetplayerledger', 'PlayerLedgerController@index')->name('resetplayerledger');
        });
    });

    /**
     * Author: Dharminder Singh
     * Purpose: Player Login Search and Player tracking
     * Created Date: 15/01/2020
     */
    Route::group(['namespace' => 'user', 'prefix' => 'user', 'as' => 'user.'], function () {
        //player login routes start
        Route::group(['namespace' => 'account', 'prefix' => 'player-login-search', 'as' => 'player_login_search.'], function () {
            Route::get('/', 'PlayerLoginController@index')->name('index');
            Route::post('/', 'PlayerLoginController@index')->name('search_user');
            Route::get('/export', 'PlayerLoginController@export')->name('export');
        });
        //player login routes end
    });

    Route::group(['namespace' => 'users', 'prefix'=>'users', 'as'=>'users.'], function(){
        Route::group(['prefix'=>'adjustcoins', 'as'=>'adjustcoins.'], function(){
            Route::get('/','AdjustCoins@AdjustCoins')->name('index');
            Route::post('/','AdjustCoins@AdjustCoins')->name('store');
            Route::POST('balanceadjust','AdjustCoins@balanceAdjust')->name('balanceadjust');
            Route::POST('adjustcoinexcel','AdjustCoins@adjustCoinExcel')->name('adjustcoinexcel');
            Route::POST('insertadjustexcel', 'AdjustCoins@insertAdjustExcel')->name('insertadjustexcel');
        });
    });

    /**
     * Author: Dharminder Singh
     * Purpose: Rewards configuration and Claimed rewards all details
     * Created Date: 01/02/2020
     */
    Route::group(['namespace' => 'rewards', 'prefix' => 'rewards', 'as' => 'rewards.'], function () {

        Route::group(['prefix'=>'rewardConfig', 'as'=>'rewardConfig.'],function(){

            Route::get('/', 'RewardLevelController@index')->name('index');
            Route::post('/', 'RewardLevelController@index')->name('filter_rewards');
            Route::get('/addRewardLevel', 'RewardLevelController@addrewardlevel')->name('addreward');
            Route::post('/addRewardLevel', 'RewardLevelController@updateRewardLevelfields')->name('addrewardlevel');
            Route::get('/updateRewardLevel/{reward_id}', 'RewardLevelController@updateRewardLevel')->name('updateRewardLevel');
            Route::post('/updateRewardLevel', 'RewardLevelController@editRewardLevelfields')->name('updateRewardLevel');
            Route::post('/rewardlevelstatus', 'RewardLevelController@rewardlevelstatus')->name('rewardlevelstatus');
        });
        Route::group(['prefix'=>'rewardHistory', 'as'=>'rewardHistory.'],function(){
            Route::get('/', 'RewardTransactionHistory@index')->name('claimedform');
            Route::post('/', 'RewardTransactionHistory@index')->name('searchclaimed');
            Route::get('/export', 'RewardTransactionHistory@export')->name('export');
            Route::post('/getTrackingStatus', 'RewardTransactionHistory@getTrackingStatusByRefNo')->name('getTrackingStatus');
            Route::post('/updateTrackingStatus', 'RewardTransactionHistory@updateTrackingStatusByRefNo')->name('updateTrackingStatus');
        });
    });



    /**
     * Author: Dharminder Singh
     * Purpose: Player Login Search and Player tracking
     * Created Date: 15/01/2020
     */
    Route::group(['namespace' => 'user', 'prefix' => 'user', 'as' => 'user.'], function () {
        //player login routes start
        Route::group(['namespace' => 'account', 'prefix' => 'player-login-search', 'as' => 'player_login_search.'], function () {
            Route::get('/', 'PlayerLoginController@index')->name('index');
            Route::post('/', 'PlayerLoginController@index')->name('search_user');
            Route::get('/export', 'PlayerLoginController@export')->name('export');
        });
        //player login routes end
    });

    /**
     * Games Module Routes
     * @author Nitin Sharma
     */
    Route::group(['namespace' => 'games\poker', 'prefix' => 'games/poker', 'as' => 'games.poker.'], function () {
        // games routes
        Route::group(['namespace' => 'game', 'prefix' => 'game', 'as' => 'game.'], function () {
            // Ofc Game Routes
            Route::group(['as' => 'ofc.'], function () {
                Route::get('viewofcgames', 'OFCGameController@index')->name('index');
                Route::post('viewofcgames', 'OFCGameController@viewofcgames')->name('viewofcgames');
                Route::get('viewofcgames/{play_game_id}', 'OFCGameController@ViewOfcDetail')->name('ofc_game_detail');
            });
        });

        // tournament routes
        Route::group(['namespace' => 'tournament', 'prefix' => 'tournament', 'as' => 'tournament.'], function () {
            Route::get('registerplayer', 'TournamentRegisterPlayerController@index');
            Route::post('registerplayer', 'TournamentRegisterPlayerController@registerPlayer')->name('registerplayer');
            Route::post('registerplayerExcel', 'TournamentRegisterPlayerController@registerplayerExcel')->name('registerplayerExcel');
            Route::post('checkUserName', 'TournamentRegisterPlayerController@checkUserNameExist')->name('checkUserName');
        });
    });

    /**
     * Author: Basant Swain
     * Purpose: All HelpDesk  related routes, contains transaction,payment routes.
     * Created Date: 16/12/2019
     */
    Route::group(['namespace' => 'helpdesk', 'prefix' => 'helpdesk', 'as' => 'helpdesk.'], function () {
        Route::get('transcation-search', 'TransactionSearchController@index')->name('transaction_search.index');
        Route::post('transcation-search', 'TransactionSearchController@index')->name('transaction_search.search');
        Route::post('getPromoDetails', 'TransactionSearchController@promoCodeInfo')->name('getPromoDetails');
        Route::post('getBountyDetails', 'TransactionSearchController@bountyInfo')->name('getBountyDetails');
        Route::get('getExcelExport', 'TransactionSearchController@excelExport')->name('getExcelExport');

        //payment search routes
        Route::group(['prefix' => 'payment_search', 'as' => 'payment_search.'], function () {
            Route::get('/', 'PaymentSearchController@index')->name('index');
            Route::post('/', 'PaymentSearchController@index')->name('search');
            Route::post('/changePaymentStatus','PaymentSearchController@changePaymentStatus')->name('changePaymentStatus');
            Route::post('/getCurrentGetwayStatus','PaymentSearchController@getAllPaymentGetWayStatus')->name('getCurrentGetwayStatus');
            Route::get('/payExcelExport','PaymentSearchController@paymentExcelExport')->name('payExcelExport');
            Route::get('/invoiceDownload','PaymentSearchController@downloadInvoice')->name('invoiceDownload');
            Route::post('/invoiceDownload','PaymentSearchController@downloadInvoice')->name('invoiceDownload');
        });
		/**
         * Author: Prity Dagar
         * Purpose: Kyc add, view and update related routes.
         * Created Date: 08/10/2020
         */
        Route::group(['prefix' => 'kyc', 'as' => 'kyc.'], function () {
            Route::get('/', 'KycController@index')->name('index');
            Route::post('/', 'KycController@index')->name('search');
			Route::post('viewKycImg', 'KycController@viewKycImg');
			Route::post('viewUserBankDetails', 'KycController@viewUserBankDetails');
			Route::post('viewKycStatus', 'KycController@viewKycStatus');
			Route::post('viewBankStatus', 'KycController@viewBankStatus');
			Route::post('updateKycStatus', 'KycController@updateKycStatus');
			Route::post('updateBankStatus', 'KycController@updateBankStatus');
			Route::match(['get', 'post'],'addkyc', 'KycController@addKycDetais')->name('addkyc');
            Route::get('search-username', 'KycController@searchUserByUserName')->name('search.username');
            Route::post('fetch-details', 'KycController@fetchDetailsFromInVoid')->name('kyc.fetch-details');
            Route::post('approve', 'KycController@approveUserKyc')->name('kyc.approve');
            Route::post('reject', 'KycController@rejectUserKyc')->name('kyc.reject');
            Route::get('edit/{userName}/{kycId}/{docType}', 'KycController@editKycDetails')->name('kyc.edit');

        });
    });

    /**
     * @author Nitin Sharma
     * Purpose: All marketing related routes, contains Analytics routes.
     * Created Date: 16/12/2019
     */
    Route::group(['namespace' => 'marketing', 'prefix' => 'marketing', 'as' => 'marketing.'], function () {
        /**
         * @author Nitin Sharma
         * Purpose: Analytics routes.
         * Created Date: 16/12/2019
         */
        Route::group(['prefix' => 'analytics', 'as' => 'analytics.'], function () {
            Route::get('/', 'AnalyticsController@index')->name('index');
            Route::post('/channelChart', 'AnalyticsController@channelChart')->name('channelChart');
            Route::post('/campaignChart', 'AnalyticsController@campaignChart')->name('campaignChart');
        });

        /**
         * Author: Nitesh Kumar Jha
         * Purpose: Get all the Manage app image deaitls details
         * Created Date: 03-02-2020
         */
        Route::group(['prefix'=>'appimage', 'as'=>'appimage.'], function(){
            Route::get('/', 'AppImageController@index')->name('index');
            Route::get('search', 'AppImageController@appImagesList')->name('appImagesList');
            Route::post('search', 'AppImageController@searchResult')->name('search_post');
            Route::post('insertAppImage', 'AppImageController@insertAppImage')->name('insertAppImage');
            Route::get('editappimage/{APP_IMAGES_ID}', 'AppImageController@editappimage')->name('editappimage');
            Route::post('updateAppImages/{APP_IMAGES_ID}', 'AppImageController@updateAppImages')->name('updateAppImages');
            Route::post('AppImagesChangeStatus', 'AppImageController@AppImagesChangeStatus')->name('AppImagesChangeStatus');
        });
        /**
         * Author: Brijesh Kumar
         * Purpose: FTD Report.
         * Created Date: 27/01/2020
         */
        Route::group(['prefix' => 'ftdreport', 'as' => 'FTDreport.'], function () {
            Route::get('/', 'FTDReportController@index')->name('index');
            Route::post('/', 'FTDReportController@FTDExportXlsx')->name('ftdexportxlsx');
        });

        /**
         * Author: Brijesh Kumar
         * Purpose: Cohort Report.
         * Created Date: 28/02/2020
         */
        Route::group(['prefix' => 'cohortreport', 'as' => 'cohortreport.'], function () {
            Route::get('/', 'CohortReportController@index')->name('index');
            Route::post('/', 'CohortReportController@CohortXportXlsx')->name('cohortexportxlsx');
        });

		/* manage campaign
        * Author: Prity Dagar
        * Purpose: All manage campaign routes .
        */
		Route::group(['prefix'=>'managecampaign', 'as'=>'managecampaign.'], function(){
			Route::get('/', 'ManageCampaignController@index')->name('index');
			Route::post('/', 'ManageCampaignController@index');
			Route::get('createcampaign', 'ManageCampaignController@createCampaign')->name('createcampaign');
			Route::post('createcampaign', 'ManageCampaignController@addCampaign');
			Route::get('{id}/editcampaign', 'ManageCampaignController@editCampaign')->name('editcampaign');
			Route::post('editcampaign', 'ManageCampaignController@updateCampaign');
			Route::get('viewuserexcel/{id}', 'ManageCampaignController@viewUserExcel')->name('viewuserexcel');
			Route::post('getCampaignURL', 'ManageCampaignController@getCampaignURL');
			Route::post('editDataExcel', 'ManageCampaignController@editDataExcel');
			Route::post('updateDataExcel', 'ManageCampaignController@updateDataExcel');
			Route::post('deleteDataExcel', 'ManageCampaignController@deleteDataExcel');
			Route::post('addViewUserExcel', 'ManageCampaignController@addViewUserExcel');
			Route::get('viewbulkcode/{id}', 'ManageCampaignController@viewbulkcode')->name('viewbulkcode');
			Route::post('changeCampaignStatus', 'ManageCampaignController@changeCampaignStatus');
			Route::get('viewcontactexcel/{id}', 'ManageCampaignController@viewContactExcel')->name('viewcontactexcel');
			Route::post('editDataExcelContact', 'ManageCampaignController@editDataExcelContact');
			Route::post('updateDataExcelContact', 'ManageCampaignController@updateDataExcelContact');
			Route::post('deleteDataExcelContact', 'ManageCampaignController@deleteDataExcelContact');
			Route::post('addViewUserExcelContact', 'ManageCampaignController@addViewUserExcelContact');
			Route::post('deleteDataPromoCampaign', 'ManageCampaignController@deleteDataPromoCampaign');
			Route::post('chkUserValid', 'ManageCampaignController@chkUserValid');
			Route::post('chkUsersExcelValid', 'ManageCampaignController@chkUsersExcelValid');
			Route::post('chkExcelValidContact', 'ManageCampaignController@chkExcelValidContact');
        });

        /**
        * Author: Kamlesh Kumar
        * Purpose: User Contest.
        * Created Date: 30/03/2020
        */
        Route::group(['prefix'=>'contest', 'as'=>'contest.'], function(){
            Route::get('/managecontest', 'UserContestController@index')->name('index');
            Route::post('/managecontest', 'UserContestController@index')->name('index');
            Route::get('managecontest/createcontest', 'UserContestController@createcontest')->name('createcontest');
            Route::post('managecontest/createcontest', 'UserContestController@createcontest')->name('createcontest');
            Route::get('managecontest/edit_contest/{id}', 'UserContestController@edit_contest')->name('edit_contest');
            Route::post('managecontest/update_contest/{id}', 'UserContestController@update_contest')->name('update_contest');
            Route::post('/getContestModelData','UserContestController@getContestModelData')->name('getContestModelData');
            Route::post('/updateStatus','UserContestController@updateStatus')->name('updateStatus');

            /**
            * Author: Kamlesh Kumar
            * Purpose: Manage Contest Question.
            * Created Date: 02/04/2020
            */
            Route::get('/manage-questions', 'ManageQuestionController@index')->name('manage-questions');
            Route::post('/manage-questions', 'ManageQuestionController@index')->name('manage-questions');
            Route::get('manage-questions/create_questions', 'ManageQuestionController@create_questions')->name('create_questions');
            Route::post('/getContestTitle', 'ManageQuestionController@getContestTitle')->name('getContestTitle');
            Route::post('/insert_ques', 'ManageQuestionController@insert_ques')->name('insert_ques');
            Route::post('/updateQuesStatus', 'ManageQuestionController@updateQuesStatus')->name('updateQuesStatus');
            Route::post('/getQuestionModelData','ManageQuestionController@getQuestionModelData')->name('getQuestionModelData');
            Route::post('/updatequestion', 'ManageQuestionController@updatequestion')->name('updatequestion');
            Route::match(array('GET','POST'),'/viewparticipants', 'ManageQuestionController@viewparticipants')->name('viewparticipants');
            Route::get('getExcelExport','ManageQuestionController@ExcelExport')->name('getExcelExport');
        });
		/* manage campaign
        * Author: Prity Dagar
        * Purpose: All manage campaign routes .
        */
		Route::group(['prefix'=>'managecampaign', 'as'=>'managecampaign.'], function(){
			Route::get('/', 'ManageCampaignController@index')->name('index');
			Route::post('/', 'ManageCampaignController@index');
			Route::get('createcampaign', 'ManageCampaignController@createCampaign')->name('createcampaign');
			Route::post('createcampaign', 'ManageCampaignController@addCampaign');
			Route::get('{id}/editcampaign', 'ManageCampaignController@editCampaign')->name('editcampaign');
			Route::post('editcampaign', 'ManageCampaignController@updateCampaign');
			Route::get('viewuserexcel/{id}', 'ManageCampaignController@viewUserExcel')->name('viewuserexcel');
			Route::post('getCampaignURL', 'ManageCampaignController@getCampaignURL');
			Route::post('editDataExcel', 'ManageCampaignController@editDataExcel');
			Route::post('updateDataExcel', 'ManageCampaignController@updateDataExcel');
			Route::post('deleteDataExcel', 'ManageCampaignController@deleteDataExcel');
			Route::post('addViewUserExcel', 'ManageCampaignController@addViewUserExcel');
			Route::get('viewbulkcode/{id}', 'ManageCampaignController@viewbulkcode')->name('viewbulkcode');
			Route::post('changeCampaignStatus', 'ManageCampaignController@changeCampaignStatus');
			Route::get('viewcontactexcel/{id}', 'ManageCampaignController@viewContactExcel')->name('viewcontactexcel');
			Route::post('editDataExcelContact', 'ManageCampaignController@editDataExcelContact');
			Route::post('updateDataExcelContact', 'ManageCampaignController@updateDataExcelContact');
			Route::post('deleteDataExcelContact', 'ManageCampaignController@deleteDataExcelContact');
			Route::post('addViewUserExcelContact', 'ManageCampaignController@addViewUserExcelContact');
			Route::post('deleteDataPromoCampaign', 'ManageCampaignController@deleteDataPromoCampaign');
			Route::post('chkUserValid', 'ManageCampaignController@chkUserValid');
			Route::post('chkUsersExcelValid', 'ManageCampaignController@chkUsersExcelValid');
			Route::post('chkExcelValidContact', 'ManageCampaignController@chkExcelValidContact');
		});
    });

    /**
     * Setting Routes
     * Including Menu, Modules
     */
    Route::group(['namespace' => 'settings', 'prefix' => 'settings', 'as' => 'settings.'], function () {
        Route::group(['prefix' => 'menu', 'as' => 'menu.', 'middleware' => 'canAccess:view,menu'], function () {
            Route::get('/', 'MenuController@index')->name('index');
            Route::middleware('canAccess:edit,menu')->post('/order', 'MenuController@order')->name('order');
            Route::middleware('canAccess:edit,menu')->post('/store/{id?}', 'MenuController@store')->name('store');
        });

        Route::group(['prefix' => 'module', 'as' => 'module.', 'middleware' => 'canAccess:view,module'], function () {
            Route::get('/', 'ModuleController@index')->name('index');
            Route::middleware('canAccess:edit,module')->post('/store/{id?}', 'ModuleController@store')->name('store');
        });

        /**
         * @author Nitesh Sharma
         * Bannner Routes
         */
        Route::group(['prefix' => 'banner', 'as' => 'banner.'], function () {
            Route::get('/', 'ManegeBannerController@index')->name('index');
            Route::get('managebanner', 'ManegeBannerController@managebanner')->name('managebanner');
            Route::post('insertbanner', 'ManegeBannerController@insertbanner')->name('insertbanner');
            Route::get('editappbanner/{APP_BANNER_ID}', 'ManegeBannerController@editappbanner')->name('editappbanner');
            Route::post('updateAppBanner/{APP_BANNER_ID}', 'ManegeBannerController@updateAppBanner')->name('updateAppBanner');
            Route::post('bannerChangeStatus', 'ManegeBannerController@bannerChangeStatus')->name('bannerChangeStatus');
        });

        /**
         * Author: Hitesh Aloney
         * Reason Routes
         */
        Route::group(['prefix' => 'reason-action', 'as' => 'reason-action.'], function () {
            Route::get('/', 'reason\ManageReasonController@index')->name('index');
            Route::get('/{id}', 'reason\ManageReasonController@show')->name('show');
            Route::patch('/{id}', 'reason\ManageReasonController@update')->name('update');
            Route::post('/', 'reason\ManageReasonController@store')->name('store');
        });

        Route::group(['prefix' => 'reason-category', 'as' => 'reason-category.'], function () {
            Route::get('/', 'reason\ManageReasonCategoryController@index')->name('index');
            Route::get('/{id}', 'reason\ManageReasonCategoryController@show')->name('show');
            Route::patch('/{id}', 'reason\ManageReasonCategoryController@update')->name('update');
            Route::post('/', 'reason\ManageReasonCategoryController@store')->name('store');
        });
    });

    /**
     * Live Tournament Routes
     */
    Route::group(['namespace' => 'livetournament', 'prefix' => 'livetournament', 'as' => 'livetournament.'], function () {
        Route::group(['prefix' => 'tournament', 'as' => 'tournament.'], function () {
            Route::get('search', 'LiveTournamentController@index')->name('index');
            Route::post('search', 'LiveTournamentController@searchResult')->name('search_post');
            Route::get('liveTournamentDetails/{TOURNAMENT_ID}', 'LiveTournamentController@liveTournamentDetails')->name('liveTournamentDetails');
            Route::get('lateregistrationClosed/{TOURNAMENT_ID}', 'LiveTournamentController@lateregistrationClosed')->name('lateregistrationClosed');
            Route::get('updateRebuy/{tid}/{id}', 'LiveTournamentController@updateRebuy')->name('updateRebuy');
            Route::get('updateUnRegistration/{tid}/{id}', 'LiveTournamentController@updateUnRegistration')->name('updateUnRegistration');
            Route::post('updateAssignTable', 'LiveTournamentController@updateAssignTable')->name('updateAssignTable');
            Route::get('exportExcel', 'LiveTournamentController@exportExcel')->name('exportExcel');
        });

        #Live Entries
        Route::group(['prefix' => 'live-entries', 'as' => 'live_entries.'], function () {
            Route::get('/', 'LiveTournamentEntriesController@index')->name('index');
            Route::post('/', 'LiveTournamentEntriesController@store')->name('store');
            Route::post('ajax-live-entries-getuser', 'LiveTournamentEntriesController@ajaxGetUserStatus')->name('ajaxGetUserStatus');
        });
    });

    /**
     * Author: Nitesh Kumar
     * Rake reports
     * Created date:1-1-2020 To 3-1-2020
     */
    Route::group(['namespace' => 'reports', 'prefix' => 'reports', 'as' => 'reports.'], function () {

        Route::group(['prefix' => 'baazitds', 'as' => 'baazitds.'], function () {
            Route::get('Search', 'BaazitdsSearchController@index')->name('index');
            Route::post('Search', 'BaazitdsSearchController@BaaziTdsSearch')->name('BaaziTdsSearch');
            Route::get('exportExcel', 'BaazitdsSearchController@exportExcel')->name('exportExcel');
        });

        Route::group(['prefix' => 'rake-report', 'as' => 'rake-report.'], function () {
            Route::get('/', 'RakeReportController@index')->name('index');
            Route::post('exportExcel', 'RakeReportController@exportExcel')->name('exportExcel');
        });

        /* daily stats report start */
        Route::group(['prefix' => 'dailystats', 'as' => 'dailystats.'], function () {
            Route::match(['get', 'post'], '/', 'DailyReportController@index')->name('index');
            Route::match(['get', 'post'], 'excel', 'DailyReportController@excelExportDailyReport');
            Route::get('/exportpdf/pdf', 'DailyReportController@export_daily_pdf');
            Route::post('receipientmaillist', 'DailyReportController@ReceipientMailList');
        });
        /*daily stats report end*/

        Route::group(['prefix'=>'coinsummary', 'as'=>'coinsummary.'], function(){
            Route::get('/', 'CoinsSummaryController@index')->name('index');
            Route::post('/', 'CoinsSummaryController@coinsSummary')->name('coinsSummary');
            Route::get('exportExcel','CoinsSummaryController@exportExcel')->name('exportExcel');
        });

        /*withdraw report start*/
        /**
         * Author: Basanta Swain
         * Purpose: Withdraw Approve and reject.
         * Created Date: 30/01/2020
         */
        Route::group(['prefix' => 'withdraw', 'as' => 'withdraw.'], function () {
            Route::get('/', 'WithdrawReportController@index')->name('index');
            Route::post('/', 'WithdrawReportController@index')->name('search');
            Route::post('/userFlagUpdate', 'WithdrawReportController@userFlagUpdate')->name('userFlagUpdate');
            Route::get('/getExcelExport','WithdrawReportController@withdrawExcelExport')->name('getExcelExport');
            Route::post('/getUserFirstWitStatus','WithdrawReportController@getUserFirstWithdrawStatus')->name('getUserFirstWitStatus');
            Route::post('/get_user_win','WithdrawReportController@getUserWin')->name('getUserFiget_user_winrstWitStatus');
            Route::post('/get_private_table','WithdrawReportController@privateTableStatus')->name('get_private_table');
            // Route::get('/update_payu_status','WithdrawReportController@payuManuallyWebhookCall')->name('update_payu_status_db');
            // Route::get('/update_cashfree_status','WithdrawReportController@cashfreeManuallyWebhookCall')->name('update_cashfree_status_db');

            Route::group(['prefix'=>'action', 'as'=>'action.'], function(){
                Route::middleware('canAccess:view,withdrawal_normal_approve')->post('/normal_approve', 'WithdrawReportController@withdrawNormalApprove')->name('normal_approve');
                Route::middleware('canAccess:view,withdrawal_normal_reject')->post('/normal_reject', 'WithdrawReportController@withdrawNormalReject')->name('normal_reject');
                Route::middleware('canAccess:view,withdrawal_cashfree_approve')->post('/withdrawal_cashfree_approve', 'WithdrawReportController@withdrawCashfreeApprove')->name('withdrawal_cashfree_approve');
                Route::middleware('canAccess:view,withdrawal_cashfree_reject')->post('/withdrawal_cashfree_reject', 'WithdrawReportController@withdrawCashfreeReject')->name('withdrawal_cashfree_reject');
                Route::middleware('canAccess:view,withdrawal_payu_approve')->post('/withdrawal_payu_approve', 'WithdrawReportController@withdrawPayuApprove')->name('withdrawal_payu_approve');
                Route::middleware('canAccess:view,withdrawal_payu_reject')->post('/withdrawal_payu_reject', 'WithdrawReportController@withdrawPayuReject')->name('withdrawal_payu_reject');
                Route::middleware('canAccess:view,withdrawal_passed')->post('/withdrawal_passed', 'WithdrawReportController@withdrawPassed')->name('withdrawal_passed');
                Route::middleware('canAccess:view,withdrawal_not_passed')->post('/withdrawal_not_passed', 'WithdrawReportController@withdrawNotPassed')->name('withdrawal_not_passed');
                Route::middleware('canAccess:view,withdrawal_unchecked')->post('/withdrawal_unchecked', 'WithdrawReportController@withdrawUnchecked')->name('withdrawal_unchecked');
                Route::middleware('canAccess:view,get_payu_latest_status')->post('/get_payu_latest_status', 'WithdrawReportController@getLatestPayuStatus')->name('get_payu_latest_status');
                Route::middleware('canAccess:view,get_cashfree_latest_status')->post('/get_cashfree_latest_status', 'WithdrawReportController@getLatestCashfreeStatus')->name('get_cashfree_latest_status');



            });
        });
        /*withdraw report end*/


        /*commission withdraw report start*/
        /**
         * Author: Basanta Swain
         * Purpose: commission Withdraw Approve and reject.
         * Created Date: 07/10/2020
         */
        Route::group(['prefix' => 'commission_withdraw', 'as' => 'commission_withdraw.'], function () {
            Route::get('/', 'CommissionWithdrawReportController@index')->name('index');
            Route::post('/', 'CommissionWithdrawReportController@index')->name('search');
            Route::post('/userFlagUpdate', 'CommissionWithdrawReportController@userFlagUpdate')->name('userFlagUpdate');
            Route::get('/getExcelExport','CommissionWithdrawReportController@withdrawExcelExport')->name('getExcelExport');
            Route::post('/getUserFirstWitStatus','CommissionWithdrawReportController@getUserFirstWithdrawStatus')->name('getUserFirstWitStatus');
            Route::post('/get_user_win','CommissionWithdrawReportController@getUserWin')->name('getUserFiget_user_winrstWitStatus');


            Route::group(['prefix'=>'action', 'as'=>'action.'], function(){
                Route::middleware('canAccess:view,comm_withdrawal_normal_approve')->post('/comm_normal_approve', 'CommissionWithdrawReportController@commWithdrawNormalApprove')->name('normal_approve');
                Route::middleware('canAccess:view,comm_withdrawal_normal_reject')->post('/comm_normal_reject', 'CommissionWithdrawReportController@commWithdrawNormalReject')->name('normal_reject');
                Route::middleware('canAccess:view,comm_withdrawal_cashfree_approve')->post('/comm_withdrawal_cashfree_approve', 'CommissionWithdrawReportController@withdrawCashfreeApprove')->name('withdrawal_cashfree_approve');
                Route::middleware('canAccess:view,comm_withdrawal_cashfree_reject')->post('/comm_withdrawal_cashfree_reject', 'CommissionWithdrawReportController@withdrawCashfreeReject')->name('withdrawal_cashfree_reject');
                Route::middleware('canAccess:view,comm_withdrawal_payu_approve')->post('/comm_withdrawal_payu_approve', 'CommissionWithdrawReportController@withdrawPayuApprove')->name('withdrawal_payu_approve');
                Route::middleware('canAccess:view,comm_withdrawal_payu_reject')->post('/comm_withdrawal_payu_reject', 'CommissionWithdrawReportController@withdrawPayuReject')->name('withdrawal_payu_reject');
                Route::middleware('canAccess:view,comm_withdrawal_passed')->post('/comm_withdrawal_passed', 'CommissionWithdrawReportController@withdrawPassed')->name('withdrawal_passed');
                Route::middleware('canAccess:view,comm_withdrawal_not_passed')->post('/comm_withdrawal_not_passed', 'CommissionWithdrawReportController@withdrawNotPassed')->name('withdrawal_not_passed');
                Route::middleware('canAccess:view,comm_withdrawal_unchecked')->post('/comm_withdrawal_unchecked', 'CommissionWithdrawReportController@withdrawUnchecked')->name('withdrawal_unchecked');
                Route::middleware('canAccess:view,comm_get_payu_latest_status')->post('/comm_get_payu_latest_status', 'CommissionWithdrawReportController@getLatestPayuStatus')->name('get_payu_latest_status');
                Route::middleware('canAccess:view,comm_get_cashfree_latest_status')->post('/comm_get_cashfree_latest_status', 'CommissionWithdrawReportController@getLatestCashfreeStatus')->name('get_cashfree_latest_status');



            });
        });
        /*Commission withdraw report end*/

        /**
         * pbdaily stats report start
         * Author: Prity Dagar
         * Purpose: All PB Daily Stats related routes.
         */
        Route::group(['prefix' => 'pbdailystats', 'as' => 'pbdailystats.'], function () {
            //Route::match(['get','post'],'/','PBDailyReportController@index')->name('index');
            Route::get('/exportpdf/pbdailystatspdf', 'PBDailyReportController@export_pbdaily_pdf');
        });
        /*pbdaily stats report end*/

		/**
         * for testign pbdaily stats report
         */
        Route::group(['prefix' => 'testpbdailystats', 'as' => 'testpbdailystats.'], function () {
			//Route::match(['get','post'],'/','TestPBDailyReportController@index')->name('index');
            Route::get('/exportpdf/testpbdailystats', 'TestPBDailyReportController@export_testpbdaily_pdf');
        });
        /*testpbdaily stats report end*/

		/**
        * Author: Prity Dagar
        * Landing Page Report Automation related routes
		* Created Date: 17/08/2020
        */
		Route::group(['prefix' => 'landingpagereport', 'as' => 'landingpagereport.'], function () {
			Route::match(['get','post'],'/','LandingPageReportAutomationController@index')->name('index');
		});
		/**
        * Author: Prity Dagar
        * Tournament Report Related routes
		* Created Date: 10/09/2020
        */
		Route::group(['prefix'=>'tournamentreport', 'as'=>'tournamentreport.'], function(){
            Route::match(['get', 'post'],'/', 'TournamentReportController@index')->name('index');
           // Route::post('/', 'TournamentReportController@UserLedgerSearch');
            //Route::get('exportExcel','TournamentReportController@exportExcel')->name('exportExcel');
        });
    });


    /**
     * Author: Hitesh Aloney
     * Purpose: All transaction related routes.
     * Created Date: 27/01/2020
     */
    Route::group(['namespace' => 'transaction', 'prefix' => 'transaction', 'as' => 'transaction.'], function () {
        Route::group(['prefix' => 'adjustment', 'as' => 'adjustment.'], function () {
            Route::get('/', 'AdjustmentTransactionsController@index')->name('index');
            Route::post('/', 'AdjustmentTransactionsController@index')->name('index');
        });

        /**
         * Author: Tapesh Chauhan
         * Purpose:  Affiliate Transactions .
         * Created Date: 2/3/2020
         */
        Route::group(['prefix'=>'affiliates-transaction', 'as'=>'affiliates_transaction.'], function(){
            Route::get('/', 'AffiliatesTransactionController@index')->name('index');
            Route::post('/', 'AffiliatesTransactionController@index')->name('search');
            Route::get('/getExcelExport', 'AffiliatesTransactionController@excelExport')->name('getExcelExport');
        });

        /*daily stats report start*/
        Route::group(['prefix'=>'dailystats', 'as'=>'dailystats.'], function(){
            Route::match(['get','post'],'/','DailyReportController@index')->name('index');
            Route::match(['get','post'],'excel','DailyReportController@excelExportDailyReport');
            Route::get('/exportpdf/pdf','DailyReportController@export_daily_pdf');
            Route::post('receipientmaillist','DailyReportController@ReceipientMailList');
        });
        /*daily stats report end*/

        /**
         * Author: Dharminder Singh
         * Purpose: User Locked Amount Related Routes.
         * Created Date: 24/03/2020
         */
        Route::group(['prefix'=>'lockedamount', 'as'=>'lockedamount.'], function(){
            Route::get('/', 'LockedAmountController@index')->name('index');
            Route::post('/', 'LockedAmountController@index')->name('search_filter');
            Route::post('updateLockedAmount', 'LockedAmountController@updateLockedAmount')->name('updateLockedAmount');
        });

        /**
         * Author: Nitesh Kumar Jha
         * Purpose: All user ledgers related routes.
         * Created Date: 6/04/2020
         */
        Route::group(['prefix'=>'user-ledgers', 'as'=>'user-ledgers.'], function(){
            Route::get('/', 'UserLedgersController@index')->name('index');
            Route::post('/', 'UserLedgersController@UserLedgerSearch')->name('UserLedgerSearch');
            Route::get('exportExcel','UserLedgersController@exportExcel')->name('exportExcel');
        });

    });


    /**
     * @author Nitin Sharma
     * Purpose: Manage tournaments
     * Created Date: 03/02/2019
     */
    Route::group(['namespace'=>'tournaments', 'prefix'=>'tournaments', 'as'=>'tournaments.'], function(){
        /**
         * @author Nitin Sharma
         * Purpose: Tournament Common Api
         * Created Date: 03/02/2019
         */
        Route::group(['prefix'=>'api', 'as'=>'api.'], function(){
            Route::post('/tournamentBlindStructure', 'TournamentBaseController@tournamentBlindStructure')->name('tournamentBlindStructure');
            Route::post('/tournamentBlindLevelRebuy', 'TournamentBaseController@tournamentBlindLevelRebuy')->name('tournamentBlindLevelRebuy');
            Route::post('/countTournamentBlindInfo', 'TournamentBaseController@countTournamentBlindInfo')->name('countTournamentBlindInfo');

            Route::post('/tournamentInfo/{tournamentId}', 'TournamentBaseController@tournamentInfo')->name('tournamentInfo');
            Route::post('/tournamentBlindStructureInfo/{tournamentId}', 'TournamentBaseController@tournamentBlindStructureInfo')->name('tournamentBlindStructureInfo');
            Route::post('/tournamentUserRanks/{tournamentId}', 'TournamentBaseController@tournamentUserRanks')->name('tournamentUserRanks');
            Route::post('/tournamentPrizeInfo/{tournamentId}', 'TournamentBaseController@tournamentPrizeInfo')->name('tournamentPrizeInfo');
        });

        /**
         * @author Nitin Sharma
         * Purpose: Manage Tourneys
         * Created Date: 03/02/2019
         */
        Route::group(['prefix'=>'manage-tourneys', 'as'=>'manage-tourneys.'], function(){
            Route::get('/', 'ManageTournamentsController@index')->name('index');
            Route::post('/', 'ManageTournamentsController@index')->name('filter');
            Route::post('/store', 'ManageTournamentsController@store')->name('store');
            Route::get('/{tournamentId}/edit', 'ManageTournamentsController@edit')->name('edit');
            Route::get('/{tournamentId}/clone', 'ManageTournamentsController@clone')->name('clone');
            Route::put('/{tournamentId}', 'ManageTournamentsController@update')->name('update');
            Route::delete('/{tournamentId}', 'ManageTournamentsController@destroy')->name('destroy');

            Route::put('StuckResumeTournament/{tournamentId}', 'ManageTournamentsController@StuckResumeTournament')->name('StuckResumeTournament');
            Route::delete('cancelTournament/{tournamentId}', 'ManageTournamentsController@cancelTournament')->name('cancelTournament');

            Route::get('/create/{template_id?}','ManageTournamentsController@create')->name('create');
            Route::delete('/template/{template_id?}','ManageTournamentsController@deleteTemplate')->name('template_delete');

            Route::get('/rank/{tournamentId}','ManageTournamentsController@rank')->name('rank');
            Route::get('/rankExport/{tournamentId}','ManageTournamentsController@rankExport')->name('rankExport');
            Route::get('/userRegistered/{tournamentId}','ManageTournamentsController@userRegistered')->name('userRegistered');
            Route::get('/userRegisteredExport/{tournamentId}','ManageTournamentsController@userRegisteredExport')->name('userRegisteredExport');
        });
    });

    /**
     * Author: Hitesh Aloney
     * Purpose: All Invoice related routes.
     * Created Date: 30/01/2020
     */
    Route::group(['namespace' => 'invoice', 'prefix' => 'invoice', 'as' => 'invoice.'], function () {
        Route::group(['prefix' => 'generate', 'as' => 'generate.'], function () {
            Route::get('/', 'CreateInvoiceController@index')->name('index');
            Route::post('/', 'CreateInvoiceController@index')->name('index');
        });
    });

    /**
     * Author Nitesh Kumar jha
     * Purpose: Get all Tax Report For Affiliate
     * Created Date 17/02/20120
     */
    Route::group(['namespace' => 'affiliate', 'prefix' => 'affiliate', 'as' => 'affiliate.'], function () {
        Route::group(['prefix' => 'affiliateTaxSummary', 'as' => 'affiliateTaxSummary.'], function () {
            Route::get('/', 'affiliateBaaziTdsSearchController@index')->name('index');
            Route::post('/', 'affiliateBaaziTdsSearchController@affiliateTdsSearch')->name('affiliateTdsSearch');
            Route::get('exportExcel', 'affiliateBaaziTdsSearchController@exportExcel')->name('exportExcel');
        });
    });

    /**
     * Author: Hitesh Aloney
     * Purpose: All transaction related routes.
     * Created Date: 27/01/2020
     */
    Route::group(['namespace'=>'transaction', 'prefix'=>'transaction', 'as'=>'transaction.'], function(){
        Route::group(['prefix'=>'adjustment', 'as'=>'adjustment.'], function(){
            Route::get('/', 'AdjustmentTransactionsController@index')->name('index');
            Route::post('/', 'AdjustmentTransactionsController@index')->name('index');
        });
    });

    Route::group(['namespace' => 'users', 'prefix' => 'users', 'as' => 'users.'], function () {
        Route::group(['prefix' => 'adjustcoins', 'as' => 'adjustcoins.'], function () {
            Route::get('/', 'AdjustCoins@AdjustCoins')->name('index');
            Route::post('/', 'AdjustCoins@AdjustCoins')->name('store');
            Route::POST('balanceadjust', 'AdjustCoins@balanceAdjust')->name('balanceadjust');
            Route::POST('adjustcoinexcel', 'AdjustCoins@adjustCoinExcel')->name('adjustcoinexcel');
            Route::POST('insertadjustexcel', 'AdjustCoins@insertAdjustExcel')->name('insertadjustexcel');
        });

        /**
         * Author: Ajjay Aroraa
         * Purpose: Bulk Password Expiry and Bulk 2 Step Verification Update
         * Created Date: 08/07/2020
         *
         * Password Expiry and 2 Step Verification Update Routes Start
         */
        Route::group(['prefix' => 'passwordexpiry', 'as' => 'passwordexpiry.'], function () {
            Route::get('/', 'PasswordExpiry@index')->name('index');
            Route::POST('verifydata', 'PasswordExpiry@verifydata')->name('verifydata');
            Route::POST('verifydata/update', 'PasswordExpiry@update')->name('verifydata/update');
        });
        /* Password Expiry and 2 Step Verification Update Routes End */
    });

    /**
     * Author: Nitesh kumar jha
     * Purpose: create segments , list segments , export excel and change segment type status
     * Created Date: 27/02/2020
     */
    Route::group(['namespace' => 'notification', 'prefix' => 'notification', 'as' => 'notification.'], function () {
        Route::group(['prefix' => 'manage-segments', 'as' => 'manage-segments.'], function () {
            Route::get('/', 'ManageSegmentsController@index')->name('index');
            Route::post('/', 'ManageSegmentsController@search')->name('search');
            Route::get('createSegments', 'ManageSegmentsController@createSegments')->name('createSegments');
            Route::post('insertCreateSegments', 'ManageSegmentsController@insertCreateSegments')->name('insertCreateSegments');
            Route::post('manageSegmentsChangeStatus', 'ManageSegmentsController@manageSegmentsChangeStatus')->name('manageSegmentsChangeStatus');
            Route::get('exportExcel/{SEGMENT_ID}', 'ManageSegmentsController@exportExcel')->name('exportExcel');
            Route::post('chaakeSegmentTitle', 'ManageSegmentsController@chaakeSegmentTitle')->name('chaakeSegmentTitle');
            Route::POST('SegmentMatchExcel', 'ManageSegmentsController@SegmentMatchExcel')->name('SegmentMatchExcel');
        });

        /**
         * Author: Hitesh Aloney
         * Purpose: All push notification related routes.
         * Created Date: 04/03/2020
         */
        Route::group(['prefix' => 'push-notification', 'as' => 'push-notification.'], function () {
            Route::match(['get', 'post'], '/', "ManagePushNotificationController@index")->name('index');
            Route::get('/create', 'ManagePushNotificationController@create')->name('create');
            Route::post('/review-excel', 'ManagePushNotificationController@reviewExcel')->name('review-excel');
            Route::post('/store', 'ManagePushNotificationController@store')->name('store');
            Route::get('/{id}/edit', 'ManagePushNotificationController@edit')->name('edit');
            Route::get('/{id}/show', "ManagePushNotificationController@show")->name('show');
            Route::get('exportExcel/{id}/{seg_id?}', "ManagePushNotificationController@exportExcel")->name('exportExcel');
        });
    });

    /**
     * Author: Kamlesh Kumar
     * Purpose: All Pay By Cash And NEFT Releted Route.
     * Created Date: 18/02/2020
     */
    Route::group(['namespace'=>'transaction', 'prefix'=>'transaction', 'as'=>'transaction.'], function(){
        Route::group(['prefix'=>'paybycashneft', 'as'=>'paybycashneft.'], function(){
            Route::get('/', 'PayByCashNeft@index')->name('index');
            Route::post('checkUserDepositBalance','PayByCashNeft@checkUserDepositBalance')->name('checkUserDepositBalance');
            Route::post('createPayByCash','PayByCashNeft@createPayByCash')->name('createPayByCash');
            Route::post('paybycashneftexcel','PayByCashNeft@paybycashneftexcel')->name('paybycashneftexcel');
            Route::post('insertpaybycashneftexcel','PayByCashNeft@insertpaybycashneftexcel')->name('insertpaybycashneftexcel');
        });
    });


    Route::group(['namespace'=>'baazirewards', 'prefix'=>'baazirewards', 'as'=>'baazirewards.'], function(){
        Route::group(['prefix'=>'cashback-levels', 'as'=>'cashback-levels.'], function(){
            Route::get('/', 'CashbackLevelsController@index')->name('index');
            Route::post('insertCashbackLevel', 'CashbackLevelsController@insertCashbackLevel')->name('insertCashbackLevel');
            Route::post('UpdateCashbackLevel', 'CashbackLevelsController@UpdateCashbackLevel')->name('UpdateCashbackLevel');
        });
    });

    /**
     * Author: Hitesh Aloney
     * Purpose: All Leaderboard related routes.
     * Created Date: 18/03/2020
     */
    Route::group(['namespace'=>'leaderboard', 'prefix'=>'leaderboard', 'as'=>'leaderboard.'], function(){
        Route::group(['prefix'=>'manage-leaderboard','as'=>'manage-leaderboard.'], function(){
           Route::get('/','ManageLeaderboardController@index')->name('index');
           Route::post('/search','ManageLeaderboardController@search')->name('search');
           Route::get('/create','ManageLeaderboardController@create')->name('create');
           Route::get('/{id}/{action}','ManageLeaderboardController@edit')->name('edit');
           Route::post('/','ManageLeaderboardController@store')->name('store');
           Route::patch('/{id}','ManageLeaderboardController@update')->name('update');
           Route::get('/{id}','ManageLeaderboardController@show')->name('show');
        });

        Route::group(['prefix'=>'manage-mapping','as'=>'manage-mapping.'], function(){
           Route::get('/','ManageMappingController@index')->name('index');
           Route::post('/','ManageMappingController@store')->name('store');
           Route::delete('/{id}','ManageMappingController@destroy')->name('destroy');
           Route::post('/search','ManageMappingController@search')->name('search');
        });

    });

    /**
     * Author: Kamlesh Kumar
     * Purpose: All Responsible Gaming Releted Routes
     * Created Date: 16/04/2020
     */
    Route::group(['namespace'=>'responsiblegaming', 'prefix'=>'responsiblegaming', 'as'=>'responsiblegaming.'], function(){
        Route::group(['prefix'=>'pokerbreak', 'as'=>'pokerbreak.'], function(){
            Route::match(['get','post'],'/','ResponsibleGamingController@index')->name('pokerbreak');
            Route::post('/show','ResponsibleGamingController@show')->name('show');
            Route::post('/updatePokerBreak','ResponsibleGamingController@update')->name('updatePokerBreak');
            Route::get('/getExcelExport', 'ResponsibleGamingController@excelExportforPokerBreak')->name('getExcelExport');
        });
        Route::match(['get','post'],'/showAllLogs','ResponsibleGamingController@showAllLogs')->name('showalllogs');
        Route::match(['get','post'],'/addusername','ResponsibleGamingController@addusername')->name('adduser');
        Route::match(['get','post'],'/ofclimit','ResponsibleGamingController@ofclimit')->name('ofclimit');
        Route::post('showofctable','ResponsibleGamingController@showofctable')->name('showofctable');
        Route::post('/updateofclimit','ResponsibleGamingController@updateofclimit')->name('updateofclimit');
        Route::match(['get','post'],'/cashtablelimit','ResponsibleGamingController@cashtablelimit')->name('cashtablelimit');
        Route::post('showcashtable','ResponsibleGamingController@showcashtable')->name('showcashtablemodel');
        Route::post('/updatecashlimit','ResponsibleGamingController@updatecashlimit')->name('updatecashlimit');
        Route::match(['get','post'],'/allrequest','ResponsibleGamingController@allrequest')->name('allrequest');
        Route::get('/getExcelExport', 'ResponsibleGamingController@excelExport')->name('getExcelExport');
        Route::post('/updaterequest','ResponsibleGamingController@updaterequest')->name('updaterequest');
        Route::group(['prefix'=>'depositlimit', 'as'=>'depositlimit.'], function(){
            Route::get('edit/{user_id}', 'DepositLimitController@index')->name('editDepositLimit');
            Route::post('update/{user_id}', 'DepositLimitController@store')->name('updateDepositLimit');
        });

    });

    Route::group(['namespace' => 'staticblockmodule', 'prefix' => 'staticblockmodule', 'as' => 'staticblockmodule.'], function () {
        /**
         * Author: Nitesh Kumar Jha
         * Purpose: Get all website management related static block module url
         * Created Date: 09-09-2020
         */
        Route::group(['prefix'=>'staticblock', 'as'=>'staticblock.'], function(){
            Route::get('/', 'StaticblockController@index')->name('index');
            Route::post('insertStaticblock', 'StaticblockController@insertStaticblock')->name('insertStaticblock');
            Route::post('checkStaticBlockTitle', 'StaticblockController@checkStaticBlockTitle')->name('checkStaticBlockTitle');
            Route::get('search', 'StaticblockController@StaticblockList')->name('StaticblockList');
            Route::post('search', 'StaticblockController@searchResult')->name('search_post');
            Route::get('editStaticBlock/{STATIC_BLOCK_ID}', 'StaticblockController@editStaticBlock')->name('editStaticBlock');
            Route::post('updateStaticBlock/{STATIC_BLOCK_ID}', 'StaticblockController@updateStaticBlock')->name('updateStaticBlock');

        });
    });

    Route::group(['namespace'=>'contest', 'prefix'=>'contest', 'as'=>'contest.'], function(){
        Route::group(['prefix'=>'ticket-ledgers', 'as'=>'ticket-ledgers.'], function(){
            Route::get('/', 'TicketLedgerController@index')->name('index');
            Route::post('/', 'TicketLedgerController@TicketLedgerSearch')->name('TicketLedgerSearch');
            Route::get('exportExcel','TicketLedgerController@exportExcel')->name('exportExcel');
        });
    });
});

Route::group(['middleware' => ['web', 'checkserverip']], function () {
    Route::get('/mailxls/{days?}', 'bo\marketing\CohortReportController@scheduleCRONmailCohortReport')->where('days', '[0-9]+')->name('mailxls');
    Route::get('cron-mail-pb-stats-report', 'bo\reports\PBDailyReportController@index')->name('cron-mail-pb-stats-report');
    Route::get('newtest-mail-pb-stats-report', 'bo\reports\TestPBDailyReportController@index')->name('newtest-mail-pb-stats-report'); // for testing pb-daily-stats
    Route::get('PB-LP-REGISTRATION', 'bo\reports\LandingPageReportAutomationController@index')->name('PB-LP-REGISTRATION');
    Route::get('PB-LP-FTD', 'bo\reports\LandingPageReportAutomationController@pbLFTD')->name('PB-LP-FTD');
    Route::get('PB-LP-REVENUE', 'bo\reports\LandingPageReportAutomationController@pbLPREVENUE')->name('PB-LP-REVENUE');
    Route::get('weekly-performance-report', 'bo\reports\WeeklyPerformanceReportAutomationController@index')->name('weekly-performance-report');

    Route::group(['namespace' => 'bo\marketing'], function () {
        Route::get('exportEverydayFTDWise', 'DMReportAutomationController@exportEverydayFTDWise')->name('exportEverydayFTDWise');
        Route::get('exportTotalRegistration', 'DMReportAutomationController@exportTotalRegistration')->name('exportTotalRegistration');
        Route::get('exportFTDday0day7day30', 'DMReportAutomationController@exportFTDday0day7day30')->name('exportFTDday0day7day30');
        Route::get('exportSignUpCode', 'DMReportAutomationController@exportSignUpCode')->name('exportSignUpCode');
    });

    Route::get('cron-mail-poker-partner-report', 'bo\reports\PartnerReportAutomationController@index')->name('cron-mail-poker-partner-report');
});


