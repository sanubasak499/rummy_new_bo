<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//for branch webhook route

Route::post('v1/branckwebhook', 'bo\marketing\BranchWebhookController@insertWebhookData');
Route::post('v1/appInstallbranchwebhook', 'bo\marketing\BranchWebhookController@insertAppWebhookData');

/*cashfree and payu cashout webhook*/
/**
 * Author: Basanta Swain
 * Purpose: Withdraw Approve and reject.
 * Created Date: 11/03/2020
 */
Route::post('transferSuccessPayuCashoutWebhook','bo\reports\WithdrawReportController@transferSuccessPayuCashoutWebhook')->name('transferSuccessPayuCashoutWebhook');
Route::post('transferSuccessCashfreeCashoutWebhook','bo\reports\WithdrawReportController@transferSuccessCashfreeCashoutWebhook')->name('transferSuccessCashfreeCashoutWebhook');

/**
 * Author: Basanta Swain
 * Purpose: Withdraw gateway status update by cron.
 * Created Date: 22/09/2020
 */

Route::get('payuManuallyWebhookCall','bo\reports\WithdrawReportController@payuManuallyWebhookCall')->name('payuManuallyWebhookCall');
Route::get('cashfreeManuallyWebhookCall','bo\reports\WithdrawReportController@cashfreeManuallyWebhookCall')->name('cashfreeManuallyWebhookCall');

Route::get('payuManuallyCommWebhookCall','bo\reports\CommissionWithdrawReportController@payuManuallyCommWebhookCall')->name('payuManuallyCommWebhookCall');
Route::get('cashfreeManuallyCommWebhookCall','bo\reports\CommissionWithdrawReportController@cashfreeManuallyCommWebhookCall')->name('cashfreeManuallyCommWebhookCall');

